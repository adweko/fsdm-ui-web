PROCEDURE "sap.fsdm.procedures::ClaimItemDelete" (IN ROW "sap.fsdm.tabletypes::ClaimItemTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ID=' || TO_VARCHAR("ID") || ' ' ||
                '_Subclaim._Claimant._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_Subclaim._Claimant._BusinessPartner.BusinessPartnerID") || ' ' ||
                '_Subclaim._Claimant._Claim.ID=' || TO_VARCHAR("_Subclaim._Claimant._Claim.ID") || ' ' ||
                '_Subclaim._Claimant._Claim.IDSystem=' || TO_VARCHAR("_Subclaim._Claimant._Claim.IDSystem") || ' ' ||
                '_Subclaim._InsuranceCoverage.ID=' || TO_VARCHAR("_Subclaim._InsuranceCoverage.ID") || ' ' ||
                '_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID=' || TO_VARCHAR("_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID") || ' ' ||
                '_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem=' || TO_VARCHAR("_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem") || ' ' ||
                '_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID=' || TO_VARCHAR("_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID") || ' ' ||
                '_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem=' || TO_VARCHAR("_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem") || ' ' ||
                '_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                        "IN"."_Subclaim._Claimant._Claim.ID",
                        "IN"."_Subclaim._Claimant._Claim.IDSystem",
                        "IN"."_Subclaim._InsuranceCoverage.ID",
                        "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                        "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                        "IN"."_Subclaim._Claimant._Claim.ID",
                        "IN"."_Subclaim._Claimant._Claim.IDSystem",
                        "IN"."_Subclaim._InsuranceCoverage.ID",
                        "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                        "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ID",
                        "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                        "_Subclaim._Claimant._Claim.ID",
                        "_Subclaim._Claimant._Claim.IDSystem",
                        "_Subclaim._InsuranceCoverage.ID",
                        "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                                    "IN"."_Subclaim._Claimant._Claim.ID",
                                    "IN"."_Subclaim._Claimant._Claim.IDSystem",
                                    "IN"."_Subclaim._InsuranceCoverage.ID",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                                    "IN"."_Subclaim._Claimant._Claim.ID",
                                    "IN"."_Subclaim._Claimant._Claim.IDSystem",
                                    "IN"."_Subclaim._InsuranceCoverage.ID",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                                    "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" is null and
            "_Subclaim._Claimant._Claim.ID" is null and
            "_Subclaim._Claimant._Claim.IDSystem" is null and
            "_Subclaim._InsuranceCoverage.ID" is null and
            "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ClaimItem" (
        "ID",
        "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
        "_Subclaim._Claimant._Claim.ID",
        "_Subclaim._Claimant._Claim.IDSystem",
        "_Subclaim._InsuranceCoverage.ID",
        "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
        "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_InjuredPerson._IndividualPerson.BusinessPartnerID",
        "_InjuredPerson._PersonalInjury.LossCategory",
        "_InjuredPerson._PersonalInjury._Claim.ID",
        "_InjuredPerson._PersonalInjury._Claim.IDSystem",
        "_LossObject.LossObjectID",
        "_LossObject._PropertyLoss.LossCategory",
        "_LossObject._PropertyLoss._Claim.ID",
        "_LossObject._PropertyLoss._Claim.IDSystem",
        "ClaimedAmount",
        "RejectionReason",
        "RiskCoverage",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID" ,
            "OLD__Subclaim._Claimant._BusinessPartner.BusinessPartnerID" as "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
            "OLD__Subclaim._Claimant._Claim.ID" as "_Subclaim._Claimant._Claim.ID" ,
            "OLD__Subclaim._Claimant._Claim.IDSystem" as "_Subclaim._Claimant._Claim.IDSystem" ,
            "OLD__Subclaim._InsuranceCoverage.ID" as "_Subclaim._InsuranceCoverage.ID" ,
            "OLD__Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" as "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "OLD__Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" as "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
            "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "OLD__Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__InjuredPerson._IndividualPerson.BusinessPartnerID" as "_InjuredPerson._IndividualPerson.BusinessPartnerID" ,
            "OLD__InjuredPerson._PersonalInjury.LossCategory" as "_InjuredPerson._PersonalInjury.LossCategory" ,
            "OLD__InjuredPerson._PersonalInjury._Claim.ID" as "_InjuredPerson._PersonalInjury._Claim.ID" ,
            "OLD__InjuredPerson._PersonalInjury._Claim.IDSystem" as "_InjuredPerson._PersonalInjury._Claim.IDSystem" ,
            "OLD__LossObject.LossObjectID" as "_LossObject.LossObjectID" ,
            "OLD__LossObject._PropertyLoss.LossCategory" as "_LossObject._PropertyLoss.LossCategory" ,
            "OLD__LossObject._PropertyLoss._Claim.ID" as "_LossObject._PropertyLoss._Claim.ID" ,
            "OLD__LossObject._PropertyLoss._Claim.IDSystem" as "_LossObject._PropertyLoss._Claim.IDSystem" ,
            "OLD_ClaimedAmount" as "ClaimedAmount" ,
            "OLD_RejectionReason" as "RejectionReason" ,
            "OLD_RiskCoverage" as "RiskCoverage" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                        "OLD"."_Subclaim._Claimant._Claim.ID",
                        "OLD"."_Subclaim._Claimant._Claim.IDSystem",
                        "OLD"."_Subclaim._InsuranceCoverage.ID",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" AS "OLD__Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Subclaim._Claimant._Claim.ID" AS "OLD__Subclaim._Claimant._Claim.ID" ,
                "OLD"."_Subclaim._Claimant._Claim.IDSystem" AS "OLD__Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage.ID" AS "OLD__Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_InjuredPerson._IndividualPerson.BusinessPartnerID" AS "OLD__InjuredPerson._IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InjuredPerson._PersonalInjury.LossCategory" AS "OLD__InjuredPerson._PersonalInjury.LossCategory" ,
                "OLD"."_InjuredPerson._PersonalInjury._Claim.ID" AS "OLD__InjuredPerson._PersonalInjury._Claim.ID" ,
                "OLD"."_InjuredPerson._PersonalInjury._Claim.IDSystem" AS "OLD__InjuredPerson._PersonalInjury._Claim.IDSystem" ,
                "OLD"."_LossObject.LossObjectID" AS "OLD__LossObject.LossObjectID" ,
                "OLD"."_LossObject._PropertyLoss.LossCategory" AS "OLD__LossObject._PropertyLoss.LossCategory" ,
                "OLD"."_LossObject._PropertyLoss._Claim.ID" AS "OLD__LossObject._PropertyLoss._Claim.ID" ,
                "OLD"."_LossObject._PropertyLoss._Claim.IDSystem" AS "OLD__LossObject._PropertyLoss._Claim.IDSystem" ,
                "OLD"."ClaimedAmount" AS "OLD_ClaimedAmount" ,
                "OLD"."RejectionReason" AS "OLD_RejectionReason" ,
                "OLD"."RiskCoverage" AS "OLD_RiskCoverage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClaimItem" as "OLD"
            on
                      "IN"."ID" = "OLD"."ID" and
                      "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
                      "IN"."_Subclaim._Claimant._Claim.ID" = "OLD"."_Subclaim._Claimant._Claim.ID" and
                      "IN"."_Subclaim._Claimant._Claim.IDSystem" = "OLD"."_Subclaim._Claimant._Claim.IDSystem" and
                      "IN"."_Subclaim._InsuranceCoverage.ID" = "OLD"."_Subclaim._InsuranceCoverage.ID" and
                      "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                      "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
                      "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                      "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                      "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ClaimItem" (
        "ID",
        "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
        "_Subclaim._Claimant._Claim.ID",
        "_Subclaim._Claimant._Claim.IDSystem",
        "_Subclaim._InsuranceCoverage.ID",
        "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
        "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_InjuredPerson._IndividualPerson.BusinessPartnerID",
        "_InjuredPerson._PersonalInjury.LossCategory",
        "_InjuredPerson._PersonalInjury._Claim.ID",
        "_InjuredPerson._PersonalInjury._Claim.IDSystem",
        "_LossObject.LossObjectID",
        "_LossObject._PropertyLoss.LossCategory",
        "_LossObject._PropertyLoss._Claim.ID",
        "_LossObject._PropertyLoss._Claim.IDSystem",
        "ClaimedAmount",
        "RejectionReason",
        "RiskCoverage",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID",
            "OLD__Subclaim._Claimant._BusinessPartner.BusinessPartnerID" as "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
            "OLD__Subclaim._Claimant._Claim.ID" as "_Subclaim._Claimant._Claim.ID",
            "OLD__Subclaim._Claimant._Claim.IDSystem" as "_Subclaim._Claimant._Claim.IDSystem",
            "OLD__Subclaim._InsuranceCoverage.ID" as "_Subclaim._InsuranceCoverage.ID",
            "OLD__Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" as "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD__Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" as "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
            "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD__Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__InjuredPerson._IndividualPerson.BusinessPartnerID" as "_InjuredPerson._IndividualPerson.BusinessPartnerID",
            "OLD__InjuredPerson._PersonalInjury.LossCategory" as "_InjuredPerson._PersonalInjury.LossCategory",
            "OLD__InjuredPerson._PersonalInjury._Claim.ID" as "_InjuredPerson._PersonalInjury._Claim.ID",
            "OLD__InjuredPerson._PersonalInjury._Claim.IDSystem" as "_InjuredPerson._PersonalInjury._Claim.IDSystem",
            "OLD__LossObject.LossObjectID" as "_LossObject.LossObjectID",
            "OLD__LossObject._PropertyLoss.LossCategory" as "_LossObject._PropertyLoss.LossCategory",
            "OLD__LossObject._PropertyLoss._Claim.ID" as "_LossObject._PropertyLoss._Claim.ID",
            "OLD__LossObject._PropertyLoss._Claim.IDSystem" as "_LossObject._PropertyLoss._Claim.IDSystem",
            "OLD_ClaimedAmount" as "ClaimedAmount",
            "OLD_RejectionReason" as "RejectionReason",
            "OLD_RiskCoverage" as "RiskCoverage",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
                        "OLD"."_Subclaim._Claimant._Claim.ID",
                        "OLD"."_Subclaim._Claimant._Claim.IDSystem",
                        "OLD"."_Subclaim._InsuranceCoverage.ID",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" AS "OLD__Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Subclaim._Claimant._Claim.ID" AS "OLD__Subclaim._Claimant._Claim.ID" ,
                "OLD"."_Subclaim._Claimant._Claim.IDSystem" AS "OLD__Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage.ID" AS "OLD__Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_InjuredPerson._IndividualPerson.BusinessPartnerID" AS "OLD__InjuredPerson._IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InjuredPerson._PersonalInjury.LossCategory" AS "OLD__InjuredPerson._PersonalInjury.LossCategory" ,
                "OLD"."_InjuredPerson._PersonalInjury._Claim.ID" AS "OLD__InjuredPerson._PersonalInjury._Claim.ID" ,
                "OLD"."_InjuredPerson._PersonalInjury._Claim.IDSystem" AS "OLD__InjuredPerson._PersonalInjury._Claim.IDSystem" ,
                "OLD"."_LossObject.LossObjectID" AS "OLD__LossObject.LossObjectID" ,
                "OLD"."_LossObject._PropertyLoss.LossCategory" AS "OLD__LossObject._PropertyLoss.LossCategory" ,
                "OLD"."_LossObject._PropertyLoss._Claim.ID" AS "OLD__LossObject._PropertyLoss._Claim.ID" ,
                "OLD"."_LossObject._PropertyLoss._Claim.IDSystem" AS "OLD__LossObject._PropertyLoss._Claim.IDSystem" ,
                "OLD"."ClaimedAmount" AS "OLD_ClaimedAmount" ,
                "OLD"."RejectionReason" AS "OLD_RejectionReason" ,
                "OLD"."RiskCoverage" AS "OLD_RiskCoverage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClaimItem" as "OLD"
            on
                                                "IN"."ID" = "OLD"."ID" and
                                                "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
                                                "IN"."_Subclaim._Claimant._Claim.ID" = "OLD"."_Subclaim._Claimant._Claim.ID" and
                                                "IN"."_Subclaim._Claimant._Claim.IDSystem" = "OLD"."_Subclaim._Claimant._Claim.IDSystem" and
                                                "IN"."_Subclaim._InsuranceCoverage.ID" = "OLD"."_Subclaim._InsuranceCoverage.ID" and
                                                "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                                                "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
                                                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                                                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                                                "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ClaimItem"
    where (
        "ID",
        "_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
        "_Subclaim._Claimant._Claim.ID",
        "_Subclaim._Claimant._Claim.IDSystem",
        "_Subclaim._InsuranceCoverage.ID",
        "_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
        "_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ID",
            "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID",
            "OLD"."_Subclaim._Claimant._Claim.ID",
            "OLD"."_Subclaim._Claimant._Claim.IDSystem",
            "OLD"."_Subclaim._InsuranceCoverage.ID",
            "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem",
            "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ClaimItem" as "OLD"
        on
                                       "IN"."ID" = "OLD"."ID" and
                                       "IN"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
                                       "IN"."_Subclaim._Claimant._Claim.ID" = "OLD"."_Subclaim._Claimant._Claim.ID" and
                                       "IN"."_Subclaim._Claimant._Claim.IDSystem" = "OLD"."_Subclaim._Claimant._Claim.IDSystem" and
                                       "IN"."_Subclaim._InsuranceCoverage.ID" = "OLD"."_Subclaim._InsuranceCoverage.ID" and
                                       "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                                       "IN"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
                                       "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                                       "IN"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                                       "IN"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
