PROCEDURE "sap.fsdm.procedures::EndOfDayListedDerivativePriceObservationDelete" (IN ROW "sap.fsdm.tabletypes::EndOfDayListedDerivativePriceObservationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ExpirationDate=' || TO_VARCHAR("ExpirationDate") || ' ' ||
                'PriceDataProvider=' || TO_VARCHAR("PriceDataProvider") || ' ' ||
                'PriceSeriesType=' || TO_VARCHAR("PriceSeriesType") || ' ' ||
                'PutOrCall=' || TO_VARCHAR("PutOrCall") || ' ' ||
                'StrikePrice=' || TO_VARCHAR("StrikePrice") || ' ' ||
                '_Exchange.MarketIdentifierCode=' || TO_VARCHAR("_Exchange.MarketIdentifierCode") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ExpirationDate",
                        "IN"."PriceDataProvider",
                        "IN"."PriceSeriesType",
                        "IN"."PutOrCall",
                        "IN"."StrikePrice",
                        "IN"."_Exchange.MarketIdentifierCode",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ExpirationDate",
                        "IN"."PriceDataProvider",
                        "IN"."PriceSeriesType",
                        "IN"."PutOrCall",
                        "IN"."StrikePrice",
                        "IN"."_Exchange.MarketIdentifierCode",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ExpirationDate",
                        "PriceDataProvider",
                        "PriceSeriesType",
                        "PutOrCall",
                        "StrikePrice",
                        "_Exchange.MarketIdentifierCode",
                        "_FinancialInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ExpirationDate",
                                    "IN"."PriceDataProvider",
                                    "IN"."PriceSeriesType",
                                    "IN"."PutOrCall",
                                    "IN"."StrikePrice",
                                    "IN"."_Exchange.MarketIdentifierCode",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ExpirationDate",
                                    "IN"."PriceDataProvider",
                                    "IN"."PriceSeriesType",
                                    "IN"."PutOrCall",
                                    "IN"."StrikePrice",
                                    "IN"."_Exchange.MarketIdentifierCode",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ExpirationDate" is null and
            "PriceDataProvider" is null and
            "PriceSeriesType" is null and
            "PutOrCall" is null and
            "StrikePrice" is null and
            "_Exchange.MarketIdentifierCode" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::EndOfDayListedDerivativePriceObservation" (
        "ExpirationDate",
        "PriceDataProvider",
        "PriceSeriesType",
        "PutOrCall",
        "StrikePrice",
        "_Exchange.MarketIdentifierCode",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CommodityReferencePrice.ReferencePriceID",
        "Close",
        "Currency",
        "EndOfDayListedDerivativePriceObservationCategory",
        "High",
        "Low",
        "Open",
        "OpenInterest",
        "PriceNotationForm",
        "StrikePriceCurrency",
        "Volume",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ExpirationDate" as "ExpirationDate" ,
            "OLD_PriceDataProvider" as "PriceDataProvider" ,
            "OLD_PriceSeriesType" as "PriceSeriesType" ,
            "OLD_PutOrCall" as "PutOrCall" ,
            "OLD_StrikePrice" as "StrikePrice" ,
            "OLD__Exchange.MarketIdentifierCode" as "_Exchange.MarketIdentifierCode" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__CommodityReferencePrice.ReferencePriceID" as "_CommodityReferencePrice.ReferencePriceID" ,
            "OLD_Close" as "Close" ,
            "OLD_Currency" as "Currency" ,
            "OLD_EndOfDayListedDerivativePriceObservationCategory" as "EndOfDayListedDerivativePriceObservationCategory" ,
            "OLD_High" as "High" ,
            "OLD_Low" as "Low" ,
            "OLD_Open" as "Open" ,
            "OLD_OpenInterest" as "OpenInterest" ,
            "OLD_PriceNotationForm" as "PriceNotationForm" ,
            "OLD_StrikePriceCurrency" as "StrikePriceCurrency" ,
            "OLD_Volume" as "Volume" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ExpirationDate",
                        "OLD"."PriceDataProvider",
                        "OLD"."PriceSeriesType",
                        "OLD"."PutOrCall",
                        "OLD"."StrikePrice",
                        "OLD"."_Exchange.MarketIdentifierCode",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ExpirationDate" AS "OLD_ExpirationDate" ,
                "OLD"."PriceDataProvider" AS "OLD_PriceDataProvider" ,
                "OLD"."PriceSeriesType" AS "OLD_PriceSeriesType" ,
                "OLD"."PutOrCall" AS "OLD_PutOrCall" ,
                "OLD"."StrikePrice" AS "OLD_StrikePrice" ,
                "OLD"."_Exchange.MarketIdentifierCode" AS "OLD__Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_CommodityReferencePrice.ReferencePriceID" AS "OLD__CommodityReferencePrice.ReferencePriceID" ,
                "OLD"."Close" AS "OLD_Close" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."EndOfDayListedDerivativePriceObservationCategory" AS "OLD_EndOfDayListedDerivativePriceObservationCategory" ,
                "OLD"."High" AS "OLD_High" ,
                "OLD"."Low" AS "OLD_Low" ,
                "OLD"."Open" AS "OLD_Open" ,
                "OLD"."OpenInterest" AS "OLD_OpenInterest" ,
                "OLD"."PriceNotationForm" AS "OLD_PriceNotationForm" ,
                "OLD"."StrikePriceCurrency" AS "OLD_StrikePriceCurrency" ,
                "OLD"."Volume" AS "OLD_Volume" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EndOfDayListedDerivativePriceObservation" as "OLD"
            on
                      "IN"."ExpirationDate" = "OLD"."ExpirationDate" and
                      "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                      "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                      "IN"."PutOrCall" = "OLD"."PutOrCall" and
                      "IN"."StrikePrice" = "OLD"."StrikePrice" and
                      "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::EndOfDayListedDerivativePriceObservation" (
        "ExpirationDate",
        "PriceDataProvider",
        "PriceSeriesType",
        "PutOrCall",
        "StrikePrice",
        "_Exchange.MarketIdentifierCode",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CommodityReferencePrice.ReferencePriceID",
        "Close",
        "Currency",
        "EndOfDayListedDerivativePriceObservationCategory",
        "High",
        "Low",
        "Open",
        "OpenInterest",
        "PriceNotationForm",
        "StrikePriceCurrency",
        "Volume",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ExpirationDate" as "ExpirationDate",
            "OLD_PriceDataProvider" as "PriceDataProvider",
            "OLD_PriceSeriesType" as "PriceSeriesType",
            "OLD_PutOrCall" as "PutOrCall",
            "OLD_StrikePrice" as "StrikePrice",
            "OLD__Exchange.MarketIdentifierCode" as "_Exchange.MarketIdentifierCode",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__CommodityReferencePrice.ReferencePriceID" as "_CommodityReferencePrice.ReferencePriceID",
            "OLD_Close" as "Close",
            "OLD_Currency" as "Currency",
            "OLD_EndOfDayListedDerivativePriceObservationCategory" as "EndOfDayListedDerivativePriceObservationCategory",
            "OLD_High" as "High",
            "OLD_Low" as "Low",
            "OLD_Open" as "Open",
            "OLD_OpenInterest" as "OpenInterest",
            "OLD_PriceNotationForm" as "PriceNotationForm",
            "OLD_StrikePriceCurrency" as "StrikePriceCurrency",
            "OLD_Volume" as "Volume",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ExpirationDate",
                        "OLD"."PriceDataProvider",
                        "OLD"."PriceSeriesType",
                        "OLD"."PutOrCall",
                        "OLD"."StrikePrice",
                        "OLD"."_Exchange.MarketIdentifierCode",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ExpirationDate" AS "OLD_ExpirationDate" ,
                "OLD"."PriceDataProvider" AS "OLD_PriceDataProvider" ,
                "OLD"."PriceSeriesType" AS "OLD_PriceSeriesType" ,
                "OLD"."PutOrCall" AS "OLD_PutOrCall" ,
                "OLD"."StrikePrice" AS "OLD_StrikePrice" ,
                "OLD"."_Exchange.MarketIdentifierCode" AS "OLD__Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_CommodityReferencePrice.ReferencePriceID" AS "OLD__CommodityReferencePrice.ReferencePriceID" ,
                "OLD"."Close" AS "OLD_Close" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."EndOfDayListedDerivativePriceObservationCategory" AS "OLD_EndOfDayListedDerivativePriceObservationCategory" ,
                "OLD"."High" AS "OLD_High" ,
                "OLD"."Low" AS "OLD_Low" ,
                "OLD"."Open" AS "OLD_Open" ,
                "OLD"."OpenInterest" AS "OLD_OpenInterest" ,
                "OLD"."PriceNotationForm" AS "OLD_PriceNotationForm" ,
                "OLD"."StrikePriceCurrency" AS "OLD_StrikePriceCurrency" ,
                "OLD"."Volume" AS "OLD_Volume" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EndOfDayListedDerivativePriceObservation" as "OLD"
            on
                                                "IN"."ExpirationDate" = "OLD"."ExpirationDate" and
                                                "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                                                "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                                                "IN"."PutOrCall" = "OLD"."PutOrCall" and
                                                "IN"."StrikePrice" = "OLD"."StrikePrice" and
                                                "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                                                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::EndOfDayListedDerivativePriceObservation"
    where (
        "ExpirationDate",
        "PriceDataProvider",
        "PriceSeriesType",
        "PutOrCall",
        "StrikePrice",
        "_Exchange.MarketIdentifierCode",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ExpirationDate",
            "OLD"."PriceDataProvider",
            "OLD"."PriceSeriesType",
            "OLD"."PutOrCall",
            "OLD"."StrikePrice",
            "OLD"."_Exchange.MarketIdentifierCode",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::EndOfDayListedDerivativePriceObservation" as "OLD"
        on
                                       "IN"."ExpirationDate" = "OLD"."ExpirationDate" and
                                       "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                                       "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                                       "IN"."PutOrCall" = "OLD"."PutOrCall" and
                                       "IN"."StrikePrice" = "OLD"."StrikePrice" and
                                       "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                                       "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
