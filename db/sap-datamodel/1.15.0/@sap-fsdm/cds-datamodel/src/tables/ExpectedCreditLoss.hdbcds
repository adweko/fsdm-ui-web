namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"ResultGroup";
using "sap"."fsdm"::"MaturityBand";

entity "ExpectedCreditLoss" {
    key "CalculationMethod"                       : String(100);
    key "RoleOfCurrency"                          : String(40);
    key "_FinancialContract"                      : association to FinancialContract {
                                                                                       FinancialContractID,
                                                                                       IDSystem
                                                                                     }                           not null;
    key "_FinancialInstrument"                    : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_ResultGroup"                            : association to ResultGroup {
                                                                                 ResultDataProvider,
                                                                                 ResultGroupID
                                                                               }                                 not null;
    key "_TimeBucket"                             : association to MaturityBand {
                                                                                  MaturityBandID,
                                                                                  TimeBucketID
                                                                                }                                not null;
    key "BusinessValidFrom"                       : LocalDate;
    key "BusinessValidTo"                         : LocalDate;
        "SystemValidFrom"                         : UTCTimestamp                                                 not null;
        "SystemValidTo"                           : UTCTimestamp                                                 not null;
        "Currency"                                : String(3);
        "CurrentExposureAmount"                   : Decimal(34, 6);
        "EffectiveExpectedPositiveExposureAmount" : Decimal(34, 6);
        "ExpectedCreditGainOrLossAmount"          : Decimal(34, 6);
        "ExpectedPositiveExposureAmount"          : Decimal(34, 6);
        "LostCashFlowsAmount"                     : Decimal(34, 6);
        "PastDueAmount"                           : Decimal(34, 6);
        "RecoveredAmount"                         : Decimal(34, 6);
        "TimeBucketEndDate"                       : LocalDate;
        "TimeBucketStartDate"                     : LocalDate;
        "SourceSystemID"                          : String(128);
        "ChangeTimestampInSourceSystem"           : UTCTimestamp;
        "ChangingUserInSourceSystem"              : String(128);
        "ChangingProcessType"                     : String(40);
        "ChangingProcessID"                       : String(128);
}
technical configuration {
    column store;
};

entity "ExpectedCreditLoss_Historical" {
    "CalculationMethod"                       : String(100)                                                  not null;
    "RoleOfCurrency"                          : String(40)                                                   not null;
    "_FinancialContract"                      : association to FinancialContract {
                                                                                   FinancialContractID,
                                                                                   IDSystem
                                                                                 }                           not null;
    "_FinancialInstrument"                    : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_ResultGroup"                            : association to ResultGroup {
                                                                             ResultDataProvider,
                                                                             ResultGroupID
                                                                           }                                 not null;
    "_TimeBucket"                             : association to MaturityBand {
                                                                              MaturityBandID,
                                                                              TimeBucketID
                                                                            }                                not null;
    "BusinessValidFrom"                       : LocalDate                                                    not null;
    "BusinessValidTo"                         : LocalDate                                                    not null;
    "SystemValidFrom"                         : UTCTimestamp                                                 not null;
    "SystemValidTo"                           : UTCTimestamp                                                 not null;
    "Currency"                                : String(3);
    "CurrentExposureAmount"                   : Decimal(34, 6);
    "EffectiveExpectedPositiveExposureAmount" : Decimal(34, 6);
    "ExpectedCreditGainOrLossAmount"          : Decimal(34, 6);
    "ExpectedPositiveExposureAmount"          : Decimal(34, 6);
    "LostCashFlowsAmount"                     : Decimal(34, 6);
    "PastDueAmount"                           : Decimal(34, 6);
    "RecoveredAmount"                         : Decimal(34, 6);
    "TimeBucketEndDate"                       : LocalDate;
    "TimeBucketStartDate"                     : LocalDate;
    "SourceSystemID"                          : String(128);
    "ChangeTimestampInSourceSystem"           : UTCTimestamp;
    "ChangingUserInSourceSystem"              : String(128);
    "ChangingProcessType"                     : String(40);
    "ChangingProcessID"                       : String(128);
}
technical configuration {
    column store;
};