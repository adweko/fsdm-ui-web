PROCEDURE "sap.fsdm.procedures::EquityLinkedInstrumentUnderlyingAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::EquityLinkedInstrumentUnderlyingAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::EquityLinkedInstrumentUnderlyingAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EquityLinkedInstrumentUnderlyingAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_EquityLinkedInstrument.FinancialInstrumentID=' || TO_VARCHAR("_EquityLinkedInstrument.FinancialInstrumentID") || ' ' ||
                '_UnderlyingIndex.IndexID=' || TO_VARCHAR("_UnderlyingIndex.IndexID") || ' ' ||
                '_UnderlyingInstrument.FinancialInstrumentID=' || TO_VARCHAR("_UnderlyingInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_EquityLinkedInstrument.FinancialInstrumentID",
                        "IN"."_UnderlyingIndex.IndexID",
                        "IN"."_UnderlyingInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_EquityLinkedInstrument.FinancialInstrumentID",
                        "IN"."_UnderlyingIndex.IndexID",
                        "IN"."_UnderlyingInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_EquityLinkedInstrument.FinancialInstrumentID",
                        "_UnderlyingIndex.IndexID",
                        "_UnderlyingInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_EquityLinkedInstrument.FinancialInstrumentID",
                                    "IN"."_UnderlyingIndex.IndexID",
                                    "IN"."_UnderlyingInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_EquityLinkedInstrument.FinancialInstrumentID",
                                    "IN"."_UnderlyingIndex.IndexID",
                                    "IN"."_UnderlyingInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_EquityLinkedInstrument.FinancialInstrumentID" is null and
            "_UnderlyingIndex.IndexID" is null and
            "_UnderlyingInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_EquityLinkedInstrument.FinancialInstrumentID",
            "_UnderlyingIndex.IndexID",
            "_UnderlyingInstrument.FinancialInstrumentID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::EquityLinkedInstrumentUnderlyingAssignment" WHERE
            (
            "_EquityLinkedInstrument.FinancialInstrumentID" ,
            "_UnderlyingIndex.IndexID" ,
            "_UnderlyingInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_EquityLinkedInstrument.FinancialInstrumentID",
            "OLD"."_UnderlyingIndex.IndexID",
            "OLD"."_UnderlyingInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::EquityLinkedInstrumentUnderlyingAssignment" as "OLD"
        on
                              "IN"."_EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" and
                              "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" and
                              "IN"."_UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_UnderlyingInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_EquityLinkedInstrument.FinancialInstrumentID",
        "_UnderlyingIndex.IndexID",
        "_UnderlyingInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_ReferenceExchange.MarketIdentifierCode",
        "ComponentWeight",
        "DeterminationDate",
        "InitialReferenceValue",
        "ReferenceValueCurrency",
        "ReferenceValueType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__EquityLinkedInstrument.FinancialInstrumentID" as "_EquityLinkedInstrument.FinancialInstrumentID" ,
            "OLD__UnderlyingIndex.IndexID" as "_UnderlyingIndex.IndexID" ,
            "OLD__UnderlyingInstrument.FinancialInstrumentID" as "_UnderlyingInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__ReferenceExchange.MarketIdentifierCode" as "_ReferenceExchange.MarketIdentifierCode" ,
            "OLD_ComponentWeight" as "ComponentWeight" ,
            "OLD_DeterminationDate" as "DeterminationDate" ,
            "OLD_InitialReferenceValue" as "InitialReferenceValue" ,
            "OLD_ReferenceValueCurrency" as "ReferenceValueCurrency" ,
            "OLD_ReferenceValueType" as "ReferenceValueType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_EquityLinkedInstrument.FinancialInstrumentID",
                        "OLD"."_UnderlyingIndex.IndexID",
                        "OLD"."_UnderlyingInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" AS "OLD__EquityLinkedInstrument.FinancialInstrumentID" ,
                "OLD"."_UnderlyingIndex.IndexID" AS "OLD__UnderlyingIndex.IndexID" ,
                "OLD"."_UnderlyingInstrument.FinancialInstrumentID" AS "OLD__UnderlyingInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ReferenceExchange.MarketIdentifierCode" AS "OLD__ReferenceExchange.MarketIdentifierCode" ,
                "OLD"."ComponentWeight" AS "OLD_ComponentWeight" ,
                "OLD"."DeterminationDate" AS "OLD_DeterminationDate" ,
                "OLD"."InitialReferenceValue" AS "OLD_InitialReferenceValue" ,
                "OLD"."ReferenceValueCurrency" AS "OLD_ReferenceValueCurrency" ,
                "OLD"."ReferenceValueType" AS "OLD_ReferenceValueType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EquityLinkedInstrumentUnderlyingAssignment" as "OLD"
            on
                                      "IN"."_EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" and
                                      "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" and
                                      "IN"."_UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_UnderlyingInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__EquityLinkedInstrument.FinancialInstrumentID" as "_EquityLinkedInstrument.FinancialInstrumentID",
            "OLD__UnderlyingIndex.IndexID" as "_UnderlyingIndex.IndexID",
            "OLD__UnderlyingInstrument.FinancialInstrumentID" as "_UnderlyingInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__ReferenceExchange.MarketIdentifierCode" as "_ReferenceExchange.MarketIdentifierCode",
            "OLD_ComponentWeight" as "ComponentWeight",
            "OLD_DeterminationDate" as "DeterminationDate",
            "OLD_InitialReferenceValue" as "InitialReferenceValue",
            "OLD_ReferenceValueCurrency" as "ReferenceValueCurrency",
            "OLD_ReferenceValueType" as "ReferenceValueType",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_EquityLinkedInstrument.FinancialInstrumentID",
                        "OLD"."_UnderlyingIndex.IndexID",
                        "OLD"."_UnderlyingInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" AS "OLD__EquityLinkedInstrument.FinancialInstrumentID" ,
                "OLD"."_UnderlyingIndex.IndexID" AS "OLD__UnderlyingIndex.IndexID" ,
                "OLD"."_UnderlyingInstrument.FinancialInstrumentID" AS "OLD__UnderlyingInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ReferenceExchange.MarketIdentifierCode" AS "OLD__ReferenceExchange.MarketIdentifierCode" ,
                "OLD"."ComponentWeight" AS "OLD_ComponentWeight" ,
                "OLD"."DeterminationDate" AS "OLD_DeterminationDate" ,
                "OLD"."InitialReferenceValue" AS "OLD_InitialReferenceValue" ,
                "OLD"."ReferenceValueCurrency" AS "OLD_ReferenceValueCurrency" ,
                "OLD"."ReferenceValueType" AS "OLD_ReferenceValueType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EquityLinkedInstrumentUnderlyingAssignment" as "OLD"
            on
               "IN"."_EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" and
               "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" and
               "IN"."_UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_UnderlyingInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
