# FX Translation

An example showing how to use the [HANA FX conversion](https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/latest/en-US/d22d746ed2951014bb7fb0114ffdaf96.html).

## Background

For reporting and other purposes, monetary values that are stored in the data model are either displayed in their original currency or translated into a reporting or target currency. This can be on an aggregated or an item level.
Technically, translated values are needed in SQL views or other SQL constructs as well as in graphical HANA calculation views.

## Currency Translation in SAP Financial Services Data Management

In SAP Financial Services Data Management, we propose to use the built-in [HANA currency conversion](https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/latest/en-US/d22d746ed2951014bb7fb0114ffdaf96.html).

To use the [HANA currency conversion](https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/latest/en-US/d22d746ed2951014bb7fb0114ffdaf96.html), the values in FSDM need to be adapted in views.

There are two different methods of [HANA currency conversion](https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/latest/en-US/d22d746ed2951014bb7fb0114ffdaf96.html): The ERP method and the Banking method. We recommend that you use the Banking method for SAP Financial Services Data Management. This banking method is described in this proposal. For more information about the Banking method, see SAP Help Portal: 
[Currency Translation](https://help.sap.com/viewer/ab2cb3cae60b45dd861f79c60f4cbdcc/latest/en-US/4e20dd7e0a6d20cee10000000a42189c.html).

Note that while the HANA FX conversion uses the rates and configuration information in the style used by ABAP applications, it is not restricted to the data types used by ABAP. In particular:

- HANA date types can be used (thus, for example, an SQL date can be used instead of DATS style dates).
- More precise rates can be used.

![FX Conversion](img/fsdm-fx-conversion.png)

## Structure of this repository

This repository contains the following folders:

## [currency-conversion-views](currency-conversion-views/)

The folder [`currency-conversion-views`](currency-conversion-views/) contains views that can be used in the [HANA currency conversion](https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/latest/en-US/d22d746ed2951014bb7fb0114ffdaf96.html).

- [`ba1_f4_fxrates.hdbview`](currency-conversion-views/ba1_f4_fxrates.hdbview): A view that translates the entries of entity `EndOfDayExchangeRateObservation` so they can be used in the HANA currency translation. For more information, check the comments in view [`ba1_f4_fxrates.hdbview`](currency-conversion-views/ba1_f4_fxrates.hdbview).
- [`ba1_f4_fxrttyp.hdbview`](currency-conversion-views/ba1_f4_fxrttyp.hdbview): A view with configuration information. The content of this view needs to be adapted. For more information, check the comments in view [`ba1_f4_fxrttyp.hdbview`](currency-conversion-views/ba1_f4_fxrttyp.hdbview)
- [`map_pricedataprovider_to_mdcode.hdbview`](currency-conversion-views/): This view suggests a translation of field `PriceDataProvider` to `MDCODE` (master data area). For more information, check the comments in view [`map_pricedataprovider_to_mdcode.hdbview`](currency-conversion-views/)
- [`ba1_tf4_fxcvfct.hdbview`](currency-conversion-views/ba1_tf4_fxcvfct.hdbview): This view is needed in order to call the currency translation. In many cases, this view does not need to be adapted. For more information, check the comments in view [`ba1_tf4_fxcvfct.hdbview`](currency-conversion-views/ba1_tf4_fxcvfct.hdbview)
- [`tcurn.hdbview`](currency-conversion-views/tcurn.hdbview): This view is needed in order to use the currency translation in a calculation view. In most cases, this view does not need to be adapted. For more information, check the comments in view [`tcurn.hdbview`](currency-conversion-views/tcurn.hdbview)
- [`tcurx.hdbview`](currency-conversion-views/tcurx.hdbview): This view is needed in order to call the currency translation. In most cases, this view does not need to be adapted. For more information, check the comments in view [`tcurx.hdbview`](currency-conversion-views/tcurx.hdbview)

## [example-conversions](example-conversions)

This folder contains some examples how to use the currency translation on data. In order to load the necesary data, look at the chapter [data-load](#data-load)

- [`MinimalBankingConversion.hdbview`](example-conversions/MinimalBankingConversion.hdbview): A synthetic example that shows how the function `CONVERT_CURRENCY` can be used.
- [`BankingConversionAllOptions.hdbview`](example-conversions/BankingConversionAllOptions.hdbview): A synthetic example that shows all the options for `CONVERT_CURRENCY`.
- [`IncomeInformationPerPropertyView.hdbview`](example-conversions/IncomeInformationPerPropertyView.hdbview): A view that shows a conversion on top of a FDSM table. In reality, the read interface should be used on top of the table.
- [`IncomeInformationPerProperty.hdbcalculationview`](example-conversions/IncomeInformationPerProperty.hdbcalculationview): A Calculation View that shows how the currency translation can be used. You can use the preview functionality to look at the converted data (in `EUR`).

## [data-load](data-load)

The two tables `CurrencyCode` and `EndOfDayExchangeRateObervation` are directly used in the currency translation. `CurrencyCode` has default values and can be extended via a CSV file. `EndOfDayExchangeRateObervation` is usually loaded via the write interface.

This folder contains two staging tables with data that is loaed via CSV. These tables can be loaded by executing the following code.

To load some sample FX Data (taken from the ECB website), call the following on a SQL command line:

```[sql]
do begin
   declare tab_in_proc "sap.fsdm.tabletypes::EndOfDayExchangeRateObservationTT";
   declare tab_input "sap.fsdm.tabletypes::EndOfDayExchangeRateObservationTT";
   
    tab_input = SELECT
		"BaseCurrency",
		"ExchangeRateType",
		"PriceDataProvider",
		"QuotationType",
		"QuoteCurrency",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Close",
		"High",
		"Low",
		"Open",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM "sap.fsdm.data-load-example::EndOfDayExchangeRateObservation";

	:tab_in_proc.(
		"BaseCurrency",
		"ExchangeRateType",
		"PriceDataProvider",
		"QuotationType",
		"QuoteCurrency",
		"BusinessValidFrom",
		"BusinessValidTo",
		"Close",
		"High",
		"Low",
		"Open",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
    ).insert(:tab_input);
    call "sap.fsdm.procedures::EndOfDayExchangeRateObservationLoad"(:tab_in_proc);
end;    
```

The table `IncomeInformation` is used in two example conversions. You can load some sample data (e.g., in a project without real data in WebIDE) by loading data from [`sap.fsdm.data-load-example::IncomeInformation`](data-load/IncomeInformation.hdbcds) using the following code:

```[sql]
do begin
   declare tab_in_proc "sap.fsdm.tabletypes::IncomeInformationTT";
   declare tab_input "sap.fsdm.tabletypes::IncomeInformationTT";
   
    tab_input = SELECT
		"IncomeInformationType",
		"RentedPropertyType",
		"_PhysicalAsset_PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"GrossOperatingIncome",
		"GrossOperatingIncomeCurrency",
		"IncomePeriodLength",
		"IncomePeriodTimeUnit",
		"NetRentIncome",
		"NetRentIncomeCurrency",
		"OperatingExpenses",
		"OperatingExpensesCurrency",
		"VacancyRate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
	FROM "sap.fsdm.data-load-example::IncomeInformation";
	:tab_in_proc.(
		"IncomeInformationType",
		"RentedPropertyType",
		"_PhysicalAsset.PhysicalAssetID",
		"BusinessValidFrom",
		"BusinessValidTo",
		"GrossOperatingIncome",
		"GrossOperatingIncomeCurrency",
		"IncomePeriodLength",
		"IncomePeriodTimeUnit",
		"NetRentIncome",
		"NetRentIncomeCurrency",
		"OperatingExpenses",
		"OperatingExpensesCurrency",
		"VacancyRate",
		"SourceSystemID",
		"ChangeTimestampInSourceSystem",
		"ChangingUserInSourceSystem",
		"ChangingProcessType",
		"ChangingProcessID"
    ).insert(:tab_input);
    call "sap.fsdm.procedures::IncomeInformationLoad"(:tab_in_proc);
end;    
```