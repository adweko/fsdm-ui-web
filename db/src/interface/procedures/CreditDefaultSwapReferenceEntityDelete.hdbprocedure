PROCEDURE "sap.fsdm.procedures::CreditDefaultSwapReferenceEntityDelete" (IN ROW "sap.fsdm.tabletypes::CreditDefaultSwapReferenceEntityTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Basket.FinancialInstrumentID=' || TO_VARCHAR("_Basket.FinancialInstrumentID") || ' ' ||
                '_ProtectionLeg._CreditDefaultSwap.FinancialContractID=' || TO_VARCHAR("_ProtectionLeg._CreditDefaultSwap.FinancialContractID") || ' ' ||
                '_ProtectionLeg._CreditDefaultSwap.IDSystem=' || TO_VARCHAR("_ProtectionLeg._CreditDefaultSwap.IDSystem") || ' ' ||
                '_ReferenceEntity.BusinessPartnerID=' || TO_VARCHAR("_ReferenceEntity.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Basket.FinancialInstrumentID",
                        "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                        "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
                        "IN"."_ReferenceEntity.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Basket.FinancialInstrumentID",
                        "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                        "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
                        "IN"."_ReferenceEntity.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Basket.FinancialInstrumentID",
                        "_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                        "_ProtectionLeg._CreditDefaultSwap.IDSystem",
                        "_ReferenceEntity.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Basket.FinancialInstrumentID",
                                    "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                                    "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
                                    "IN"."_ReferenceEntity.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Basket.FinancialInstrumentID",
                                    "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                                    "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
                                    "IN"."_ReferenceEntity.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Basket.FinancialInstrumentID" is null and
            "_ProtectionLeg._CreditDefaultSwap.FinancialContractID" is null and
            "_ProtectionLeg._CreditDefaultSwap.IDSystem" is null and
            "_ReferenceEntity.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CreditDefaultSwapReferenceEntity" (
        "_Basket.FinancialInstrumentID",
        "_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
        "_ProtectionLeg._CreditDefaultSwap.IDSystem",
        "_ReferenceEntity.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "IsEntityExcluded",
        "ReferenceEntityPairCode",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Basket.FinancialInstrumentID" as "_Basket.FinancialInstrumentID" ,
            "OLD__ProtectionLeg._CreditDefaultSwap.FinancialContractID" as "_ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
            "OLD__ProtectionLeg._CreditDefaultSwap.IDSystem" as "_ProtectionLeg._CreditDefaultSwap.IDSystem" ,
            "OLD__ReferenceEntity.BusinessPartnerID" as "_ReferenceEntity.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_IsEntityExcluded" as "IsEntityExcluded" ,
            "OLD_ReferenceEntityPairCode" as "ReferenceEntityPairCode" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Basket.FinancialInstrumentID",
                        "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                        "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
                        "OLD"."_ReferenceEntity.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_Basket.FinancialInstrumentID" AS "OLD__Basket.FinancialInstrumentID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" AS "OLD__ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" AS "OLD__ProtectionLeg._CreditDefaultSwap.IDSystem" ,
                "OLD"."_ReferenceEntity.BusinessPartnerID" AS "OLD__ReferenceEntity.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."IsEntityExcluded" AS "OLD_IsEntityExcluded" ,
                "OLD"."ReferenceEntityPairCode" AS "OLD_ReferenceEntityPairCode" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditDefaultSwapReferenceEntity" as "OLD"
            on
                      "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
                      "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" = "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" and
                      "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem" = "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" and
                      "IN"."_ReferenceEntity.BusinessPartnerID" = "OLD"."_ReferenceEntity.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CreditDefaultSwapReferenceEntity" (
        "_Basket.FinancialInstrumentID",
        "_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
        "_ProtectionLeg._CreditDefaultSwap.IDSystem",
        "_ReferenceEntity.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "IsEntityExcluded",
        "ReferenceEntityPairCode",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Basket.FinancialInstrumentID" as "_Basket.FinancialInstrumentID",
            "OLD__ProtectionLeg._CreditDefaultSwap.FinancialContractID" as "_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
            "OLD__ProtectionLeg._CreditDefaultSwap.IDSystem" as "_ProtectionLeg._CreditDefaultSwap.IDSystem",
            "OLD__ReferenceEntity.BusinessPartnerID" as "_ReferenceEntity.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_IsEntityExcluded" as "IsEntityExcluded",
            "OLD_ReferenceEntityPairCode" as "ReferenceEntityPairCode",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Basket.FinancialInstrumentID",
                        "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
                        "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
                        "OLD"."_ReferenceEntity.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Basket.FinancialInstrumentID" AS "OLD__Basket.FinancialInstrumentID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" AS "OLD__ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" AS "OLD__ProtectionLeg._CreditDefaultSwap.IDSystem" ,
                "OLD"."_ReferenceEntity.BusinessPartnerID" AS "OLD__ReferenceEntity.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."IsEntityExcluded" AS "OLD_IsEntityExcluded" ,
                "OLD"."ReferenceEntityPairCode" AS "OLD_ReferenceEntityPairCode" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditDefaultSwapReferenceEntity" as "OLD"
            on
                                                "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
                                                "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" = "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" and
                                                "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem" = "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" and
                                                "IN"."_ReferenceEntity.BusinessPartnerID" = "OLD"."_ReferenceEntity.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CreditDefaultSwapReferenceEntity"
    where (
        "_Basket.FinancialInstrumentID",
        "_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
        "_ProtectionLeg._CreditDefaultSwap.IDSystem",
        "_ReferenceEntity.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_Basket.FinancialInstrumentID",
            "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID",
            "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem",
            "OLD"."_ReferenceEntity.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditDefaultSwapReferenceEntity" as "OLD"
        on
                                       "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
                                       "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" = "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" and
                                       "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem" = "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" and
                                       "IN"."_ReferenceEntity.BusinessPartnerID" = "OLD"."_ReferenceEntity.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
