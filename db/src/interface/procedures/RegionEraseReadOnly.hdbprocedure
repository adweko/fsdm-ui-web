PROCEDURE "sap.fsdm.procedures::RegionEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RegionTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RegionTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RegionTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Region" is null and
            "_ReinsuranceCoverage.CoverageID" is null and
            "_ReinsuranceCoverage.SectionID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Region" ,
                "_ReinsuranceCoverage.CoverageID" ,
                "_ReinsuranceCoverage.SectionID" ,
                "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Region" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Region" "OLD"
            on
                "IN"."Region" = "OLD"."Region" and
                "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Region" ,
            "_ReinsuranceCoverage.CoverageID" ,
            "_ReinsuranceCoverage.SectionID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Region" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Region_Historical" "OLD"
            on
                "IN"."Region" = "OLD"."Region" and
                "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        );

END
