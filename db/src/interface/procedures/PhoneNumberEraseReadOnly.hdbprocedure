PROCEDURE "sap.fsdm.procedures::PhoneNumberEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PhoneNumberTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PhoneNumberTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PhoneNumberTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PhoneNumberType" is null and
            "SequenceNumber" is null and
            "ASSOC_OrganizationalUnit.IDSystem" is null and
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" is null and
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_Partner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "PhoneNumberType" ,
                "SequenceNumber" ,
                "ASSOC_OrganizationalUnit.IDSystem" ,
                "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "ASSOC_Partner.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."PhoneNumberType" ,
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Partner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PhoneNumber" "OLD"
            on
                "IN"."PhoneNumberType" = "OLD"."PhoneNumberType" and
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "PhoneNumberType" ,
            "SequenceNumber" ,
            "ASSOC_OrganizationalUnit.IDSystem" ,
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "ASSOC_Partner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."PhoneNumberType" ,
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Partner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PhoneNumber_Historical" "OLD"
            on
                "IN"."PhoneNumberType" = "OLD"."PhoneNumberType" and
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
        );

END
