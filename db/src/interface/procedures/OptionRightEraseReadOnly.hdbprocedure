PROCEDURE "sap.fsdm.procedures::OptionRightEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::OptionRightTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::OptionRightTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::OptionRightTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_TrancheInSyndication.TrancheSequenceNumber" is null and
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_TrancheInSyndication.TrancheSequenceNumber" ,
                "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::OptionRight" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_TrancheInSyndication.TrancheSequenceNumber" ,
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::OptionRight_Historical" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" 
        );

END
