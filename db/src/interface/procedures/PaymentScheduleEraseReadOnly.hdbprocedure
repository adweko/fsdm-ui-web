PROCEDURE "sap.fsdm.procedures::PaymentScheduleEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PaymentScheduleTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PaymentScheduleTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PaymentScheduleTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_Receivable.ReceivableID" is null and
            "_CreditCardLoan.ID" is null and
            "_CreditCardLoan._CreditCardAgreement.FinancialContractID" is null and
            "_CreditCardLoan._CreditCardAgreement.IDSystem" is null and
            "_DebtInstrument.FinancialInstrumentID" is null and
            "_InflationOptionComponent.ComponentNumber" is null and
            "_InflationOptionComponent._InflationOption.FinancialContractID" is null and
            "_InflationOptionComponent._InflationOption.IDSystem" is null and
            "_InterestRateOptionComponent.ComponentNumber" is null and
            "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" is null and
            "_InterestRateOptionComponent._InterestRateOption.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_Receivable.ReceivableID" ,
                "_CreditCardLoan.ID" ,
                "_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "_CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "_DebtInstrument.FinancialInstrumentID" ,
                "_InflationOptionComponent.ComponentNumber" ,
                "_InflationOptionComponent._InflationOption.FinancialContractID" ,
                "_InflationOptionComponent._InflationOption.IDSystem" ,
                "_InterestRateOptionComponent.ComponentNumber" ,
                "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "_InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_Receivable.ReceivableID" ,
                "OLD"."_CreditCardLoan.ID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."_DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_InflationOptionComponent.ComponentNumber" ,
                "OLD"."_InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."_InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."_InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."_InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."_InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PaymentSchedule" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_Receivable.ReceivableID" and
                "IN"."_CreditCardLoan.ID" = "OLD"."_CreditCardLoan.ID" and
                "IN"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."_CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."_DebtInstrument.FinancialInstrumentID" = "OLD"."_DebtInstrument.FinancialInstrumentID" and
                "IN"."_InflationOptionComponent.ComponentNumber" = "OLD"."_InflationOptionComponent.ComponentNumber" and
                "IN"."_InflationOptionComponent._InflationOption.FinancialContractID" = "OLD"."_InflationOptionComponent._InflationOption.FinancialContractID" and
                "IN"."_InflationOptionComponent._InflationOption.IDSystem" = "OLD"."_InflationOptionComponent._InflationOption.IDSystem" and
                "IN"."_InterestRateOptionComponent.ComponentNumber" = "OLD"."_InterestRateOptionComponent.ComponentNumber" and
                "IN"."_InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                "IN"."_InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."_InterestRateOptionComponent._InterestRateOption.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_Receivable.ReceivableID" ,
            "_CreditCardLoan.ID" ,
            "_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "_CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "_DebtInstrument.FinancialInstrumentID" ,
            "_InflationOptionComponent.ComponentNumber" ,
            "_InflationOptionComponent._InflationOption.FinancialContractID" ,
            "_InflationOptionComponent._InflationOption.IDSystem" ,
            "_InterestRateOptionComponent.ComponentNumber" ,
            "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
            "_InterestRateOptionComponent._InterestRateOption.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_Receivable.ReceivableID" ,
                "OLD"."_CreditCardLoan.ID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."_DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_InflationOptionComponent.ComponentNumber" ,
                "OLD"."_InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."_InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."_InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."_InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."_InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PaymentSchedule_Historical" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_Receivable.ReceivableID" and
                "IN"."_CreditCardLoan.ID" = "OLD"."_CreditCardLoan.ID" and
                "IN"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."_CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."_CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."_DebtInstrument.FinancialInstrumentID" = "OLD"."_DebtInstrument.FinancialInstrumentID" and
                "IN"."_InflationOptionComponent.ComponentNumber" = "OLD"."_InflationOptionComponent.ComponentNumber" and
                "IN"."_InflationOptionComponent._InflationOption.FinancialContractID" = "OLD"."_InflationOptionComponent._InflationOption.FinancialContractID" and
                "IN"."_InflationOptionComponent._InflationOption.IDSystem" = "OLD"."_InflationOptionComponent._InflationOption.IDSystem" and
                "IN"."_InterestRateOptionComponent.ComponentNumber" = "OLD"."_InterestRateOptionComponent.ComponentNumber" and
                "IN"."_InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."_InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                "IN"."_InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."_InterestRateOptionComponent._InterestRateOption.IDSystem" 
        );

END
