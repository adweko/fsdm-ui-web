PROCEDURE "sap.fsdm.procedures::StandardProlongationOptionListEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::StandardProlongationOptionTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::StandardProlongationOptionTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::StandardProlongationOptionTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" is null and
            "ASSOC_StandardFixedTermDeposit.IDSystem" is null and
            "ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" is null and
            "ASSOC_StandardFixedTermDeposit.PricingScheme" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" ,
                "ASSOC_StandardFixedTermDeposit.IDSystem" ,
                "ASSOC_StandardFixedTermDeposit.PricingScheme" ,
                "ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.IDSystem" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.PricingScheme" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::StandardProlongationOption" "OLD"
            on
                "IN"."ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" and
                "IN"."ASSOC_StandardFixedTermDeposit.IDSystem" = "OLD"."ASSOC_StandardFixedTermDeposit.IDSystem" and
                "IN"."ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" = "OLD"."ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" and
                "IN"."ASSOC_StandardFixedTermDeposit.PricingScheme" = "OLD"."ASSOC_StandardFixedTermDeposit.PricingScheme" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" ,
            "ASSOC_StandardFixedTermDeposit.IDSystem" ,
            "ASSOC_StandardFixedTermDeposit.PricingScheme" ,
            "ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.IDSystem" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.PricingScheme" ,
                "OLD"."ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::StandardProlongationOption_Historical" "OLD"
            on
                "IN"."ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardFixedTermDeposit.ASSOC_Company.BusinessPartnerID" and
                "IN"."ASSOC_StandardFixedTermDeposit.IDSystem" = "OLD"."ASSOC_StandardFixedTermDeposit.IDSystem" and
                "IN"."ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" = "OLD"."ASSOC_StandardFixedTermDeposit.FinancialStandardProductID" and
                "IN"."ASSOC_StandardFixedTermDeposit.PricingScheme" = "OLD"."ASSOC_StandardFixedTermDeposit.PricingScheme" 
        );

END
