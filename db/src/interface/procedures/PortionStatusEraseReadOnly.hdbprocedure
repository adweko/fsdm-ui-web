PROCEDURE "sap.fsdm.procedures::PortionStatusEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PortionStatusTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PortionStatusTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PortionStatusTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PortionStatusCategory" is null and
            "PortionStatusType" is null and
            "_CollateralPortion.PortionNumber" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "PortionStatusCategory" ,
                "PortionStatusType" ,
                "_CollateralPortion.PortionNumber" ,
                "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."PortionStatusCategory" ,
                "OLD"."PortionStatusType" ,
                "OLD"."_CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PortionStatus" "OLD"
            on
                "IN"."PortionStatusCategory" = "OLD"."PortionStatusCategory" and
                "IN"."PortionStatusType" = "OLD"."PortionStatusType" and
                "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "PortionStatusCategory" ,
            "PortionStatusType" ,
            "_CollateralPortion.PortionNumber" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."PortionStatusCategory" ,
                "OLD"."PortionStatusType" ,
                "OLD"."_CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PortionStatus_Historical" "OLD"
            on
                "IN"."PortionStatusCategory" = "OLD"."PortionStatusCategory" and
                "IN"."PortionStatusType" = "OLD"."PortionStatusType" and
                "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
        );

END
