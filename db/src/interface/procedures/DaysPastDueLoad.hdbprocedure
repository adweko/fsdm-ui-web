PROCEDURE "sap.fsdm.procedures::DaysPastDueLoad" (IN ROW "sap.fsdm.tabletypes::DaysPastDueTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PastDueType=' || TO_VARCHAR("PastDueType") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                'ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency=' || TO_VARCHAR("ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency") || ' ' ||
                'ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID=' || TO_VARCHAR("ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID") || ' ' ||
                'ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem=' || TO_VARCHAR("ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_FinancialInstrumentDaysPastDue.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrumentDaysPastDue.FinancialInstrumentID") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PastDueType",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PastDueType",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PastDueType",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "_BusinessPartner.BusinessPartnerID",
                        "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PastDueType",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                    "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PastDueType",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                    "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::DaysPastDue" (
        "PastDueType",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_BusinessPartner.BusinessPartnerID",
        "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DaysPastDue",
        "DaysPastDueCategory",
        "PastDueFrom",
        "PastDueSince",
        "PastDueTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PastDueType" as "PastDueType" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "OLD__FinancialInstrumentDaysPastDue.FinancialInstrumentID" as "_FinancialInstrumentDaysPastDue.FinancialInstrumentID" ,
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_DaysPastDue" as "DaysPastDue" ,
            "OLD_DaysPastDueCategory" as "DaysPastDueCategory" ,
            "OLD_PastDueFrom" as "PastDueFrom" ,
            "OLD_PastDueSince" as "PastDueSince" ,
            "OLD_PastDueTo" as "PastDueTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."PastDueType",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."PastDueType" as "OLD_PastDueType",
                                "OLD"."ASSOC_FinancialContract.FinancialContractID" as "OLD_ASSOC_FinancialContract.FinancialContractID",
                                "OLD"."ASSOC_FinancialContract.IDSystem" as "OLD_ASSOC_FinancialContract.IDSystem",
                                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                                "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                                "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" as "OLD__FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."DaysPastDue" as "OLD_DaysPastDue",
                                "OLD"."DaysPastDueCategory" as "OLD_DaysPastDueCategory",
                                "OLD"."PastDueFrom" as "OLD_PastDueFrom",
                                "OLD"."PastDueSince" as "OLD_PastDueSince",
                                "OLD"."PastDueTo" as "OLD_PastDueTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::DaysPastDue" as "OLD"
            on
                ifnull( "IN"."PastDueType", '') = "OLD"."PastDueType" and
                ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '') = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '') = "OLD"."ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '') = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '') = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '') = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '') = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID", '') = "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::DaysPastDue" (
        "PastDueType",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_BusinessPartner.BusinessPartnerID",
        "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DaysPastDue",
        "DaysPastDueCategory",
        "PastDueFrom",
        "PastDueSince",
        "PastDueTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PastDueType" as "PastDueType",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
            "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "OLD__FinancialInstrumentDaysPastDue.FinancialInstrumentID" as "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DaysPastDue" as "DaysPastDue",
            "OLD_DaysPastDueCategory" as "DaysPastDueCategory",
            "OLD_PastDueFrom" as "PastDueFrom",
            "OLD_PastDueSince" as "PastDueSince",
            "OLD_PastDueTo" as "PastDueTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."PastDueType",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."PastDueType" as "OLD_PastDueType",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID" as "OLD_ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem" as "OLD_ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" as "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
                        "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" as "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" as "OLD_ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
                        "OLD"."_BusinessPartner.BusinessPartnerID" as "OLD__BusinessPartner.BusinessPartnerID",
                        "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" as "OLD__FinancialInstrumentDaysPastDue.FinancialInstrumentID",
                        "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                        "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."DaysPastDue" as "OLD_DaysPastDue",
                        "OLD"."DaysPastDueCategory" as "OLD_DaysPastDueCategory",
                        "OLD"."PastDueFrom" as "OLD_PastDueFrom",
                        "OLD"."PastDueSince" as "OLD_PastDueSince",
                        "OLD"."PastDueTo" as "OLD_PastDueTo",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::DaysPastDue" as "OLD"
            on
                ifnull( "IN"."PastDueType", '' ) = "OLD"."PastDueType" and
                ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '' ) = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '' ) = "OLD"."ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '' ) = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '' ) = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '' ) = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_BusinessPartner.BusinessPartnerID" and
                ifnull( "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '' ) = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '' ) = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::DaysPastDue"
    where (
        "PastDueType",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_BusinessPartner.BusinessPartnerID",
        "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PastDueType",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
            "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::DaysPastDue" as "OLD"
        on
           ifnull( "IN"."PastDueType", '' ) = "OLD"."PastDueType" and
           ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '' ) = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
           ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '' ) = "OLD"."ASSOC_FinancialContract.IDSystem" and
           ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '' ) = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
           ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '' ) = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
           ifnull( "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '' ) = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
           ifnull( "IN"."_BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_BusinessPartner.BusinessPartnerID" and
           ifnull( "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" and
           ifnull( "IN"."_TimeBucket.MaturityBandID", '' ) = "OLD"."_TimeBucket.MaturityBandID" and
           ifnull( "IN"."_TimeBucket.TimeBucketID", '' ) = "OLD"."_TimeBucket.TimeBucketID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::DaysPastDue" (
        "PastDueType",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
        "_BusinessPartner.BusinessPartnerID",
        "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DaysPastDue",
        "DaysPastDueCategory",
        "PastDueFrom",
        "PastDueSince",
        "PastDueTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "PastDueType", '' ) as "PastDueType",
            ifnull( "ASSOC_FinancialContract.FinancialContractID", '' ) as "ASSOC_FinancialContract.FinancialContractID",
            ifnull( "ASSOC_FinancialContract.IDSystem", '' ) as "ASSOC_FinancialContract.IDSystem",
            ifnull( "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency", '' ) as "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency",
            ifnull( "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID", '' ) as "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID",
            ifnull( "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem", '' ) as "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem",
            ifnull( "_BusinessPartner.BusinessPartnerID", '' ) as "_BusinessPartner.BusinessPartnerID",
            ifnull( "_FinancialInstrumentDaysPastDue.FinancialInstrumentID", '' ) as "_FinancialInstrumentDaysPastDue.FinancialInstrumentID",
            ifnull( "_TimeBucket.MaturityBandID", '' ) as "_TimeBucket.MaturityBandID",
            ifnull( "_TimeBucket.TimeBucketID", '' ) as "_TimeBucket.TimeBucketID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "DaysPastDue"  ,
            "DaysPastDueCategory"  ,
            "PastDueFrom"  ,
            "PastDueSince"  ,
            "PastDueTo"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END