sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/table/Table",
    "sap/ui/table/Column",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/routing/History"


], function (Controller, Table, Column, JSONModel, History) {
    "use strict";
    return Controller.extend("sap.ui.hana.controller.TableElements", {
        onInit: function () {
            var oRouter = this.getOwnerComponent().getRouter();
            oRouter.getRoute("tableElements").attachPatternMatched(this._onObjectMatched, this);

        },
        _onObjectMatched: function (oEvent) {
        	
            var oModel = new JSONModel();
            var oModelKey = new JSONModel();
           
            this.getView().setModel(oModel, "main");
            
            this.getView().setModel(oModelKey, "keyValues");
            
            oModelKey.loadData("/selectData.xsjs",{name:"keyValues",data:sessionStorage.getItem("tableName")});
            this.byId("pageHeader").setTitle(sessionStorage.getItem("tableName"));
             
            this.chosenAttributes = oEvent.getParameter("arguments").parameter.split(", ");
            this.chosenAttributes.sort();
             
            oModel.loadData("/selectData.xsjs",{name:"tableData",data: sessionStorage.getItem("tableName")});
          
            var oTable = this.getView().byId("idMyTable");
            oTable.setModel(oModel);
            oTable.bindRows("/");
    
            this.chosenAttributes.forEach(function(v) {
                var cellElement = '{' + v + '}';
                oTable.addColumn(new sap.ui.table.Column({
                    label: v,
                    minWidth: 120,
                    template: new sap.m.Input({value: cellElement})
                }));
            });
        },
        onPressDeleteRow: function(){
        	var keyValues = this.getView().getModel("keyValues").getData();
        	
        	var oItem= this.getView().byId("idMyTable");
    		var oEntry = oItem.getSelectedIndex();
    		var arrayOfValues = [];	
    		var arrayOfKeys =[];
    		keyValues.forEach(function(v){
    			arrayOfKeys.push(v.COLUMN_NAME);
    			arrayOfValues.push(oItem.getContextByIndex(oEntry).getObject(v.COLUMN_NAME));
    			});
    		var oModel = this.getView().getModel("main");

    		var payload = {};
            payload.name = "deleteChosenRow";
            payload.values = arrayOfValues;
            payload.keyValues = arrayOfKeys;
            payload.tableName = sessionStorage.getItem("tableName");
            
            var insertData = JSON.stringify(payload);
            
    		$.ajax({
               type: "POST",
               url: "/manipulateData.xsjs",
               contentType: "application/json",
               data: insertData,
               dataType: "json",
               crossDomain: true,
               statusCode: {
    				200: function(data) {
    					
    					oModel.loadData("/selectData.xsjs",{name:"tableData",data: sessionStorage.getItem("tableName")});
    					sap.m.MessageToast.show(data.statusText,{ my: "center top"});         
    				}
               }
               
    		});
        },
        onPressNewRow: function () {
        	
        	this.getView().byId("idMyTable").getModel().refresh(true);
            var oTable = this.getView().byId("idMyTable");
            oTable.destroyColumns();
        	var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

            oRouter.navTo("tableNewRow", { parameter: sessionStorage.getItem("tableName") } );
            
        },
        onNavBack: function () {

            this.getView().byId("idMyTable").getModel().refresh(true);
            var oTable = this.getView().byId("idMyTable");
            oTable.destroyColumns();
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = this.getOwnerComponent().getRouter();
                oRouter.navTo("tableColumns", {}, true);
            }
        }


    });
});