namespace "sap"."fsdm";

using "sap"."fsdm"::"AccountingSystem";
using "sap"."fsdm"::"Collection";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"Trade";

entity "CollateralPoolAssetAssignment" {
    key "CollectionAssignmentType"          : String(100)                                                  default '';
    key "LotID"                             : String(128)                                                  default '';
    key "_AccountingSystem"                 : association to AccountingSystem { AccountingSystemID }       not null;
    key "_CollateralPool"                   : association to Collection {
                                                                          CollectionID,
                                                                          IDSystem,
                                                                          _Client
                                                                        }                                  not null;
    key "_FinancialContract"                : association to FinancialContract {
                                                                                 FinancialContractID,
                                                                                 IDSystem
                                                                               }                           not null;
    key "_FinancialInstrument"              : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_SecuritiesAccount"                : association to FinancialContract {
                                                                                 FinancialContractID,
                                                                                 IDSystem
                                                                               }                           not null;
    key "_Trade"                            : association to Trade {
                                                                     IDSystem,
                                                                     TradeID
                                                                   }                                       not null;
    key "BusinessValidFrom"                 : LocalDate;
    key "BusinessValidTo"                   : LocalDate;
        "SystemValidFrom"                   : UTCTimestamp                                                 not null;
        "SystemValidTo"                     : UTCTimestamp                                                 not null;
        "AmountAssignedToPool"              : Decimal(34, 6);
        "AssetAssignmentCategory"           : String(100);
        "CollectionAssetAssignmentCategory" : String(40);
        "CurrencyOfAmountAssignedToPool"    : String(3);
        "HedgeRatio"                        : Decimal(15, 11);
        "LongShort"                         : String(10);
        "PoolCoverageType"                  : String(100);
        "ShareAssignedToPool"               : Decimal(15, 11);
        "SourceSystemID"                    : String(128);
        "ChangeTimestampInSourceSystem"     : UTCTimestamp;
        "ChangingUserInSourceSystem"        : String(128);
        "ChangingProcessType"               : String(40);
        "ChangingProcessID"                 : String(128);
}
technical configuration {
    column store;
};

entity "CollateralPoolAssetAssignment_Historical" {
    "CollectionAssignmentType"          : String(100)                                                  default '' not null;
    "LotID"                             : String(128)                                                  default '' not null;
    "_AccountingSystem"                 : association to AccountingSystem { AccountingSystemID }       not null;
    "_CollateralPool"                   : association to Collection {
                                                                      CollectionID,
                                                                      IDSystem,
                                                                      _Client
                                                                    }                                  not null;
    "_FinancialContract"                : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    "_FinancialInstrument"              : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_SecuritiesAccount"                : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    "_Trade"                            : association to Trade {
                                                                 IDSystem,
                                                                 TradeID
                                                               }                                       not null;
    "BusinessValidFrom"                 : LocalDate                                                    not null;
    "BusinessValidTo"                   : LocalDate                                                    not null;
    "SystemValidFrom"                   : UTCTimestamp                                                 not null;
    "SystemValidTo"                     : UTCTimestamp                                                 not null;
    "AmountAssignedToPool"              : Decimal(34, 6);
    "AssetAssignmentCategory"           : String(100);
    "CollectionAssetAssignmentCategory" : String(40);
    "CurrencyOfAmountAssignedToPool"    : String(3);
    "HedgeRatio"                        : Decimal(15, 11);
    "LongShort"                         : String(10);
    "PoolCoverageType"                  : String(100);
    "ShareAssignedToPool"               : Decimal(15, 11);
    "SourceSystemID"                    : String(128);
    "ChangeTimestampInSourceSystem"     : UTCTimestamp;
    "ChangingUserInSourceSystem"        : String(128);
    "ChangingProcessType"               : String(40);
    "ChangingProcessID"                 : String(128);
}
technical configuration {
    column store;
};