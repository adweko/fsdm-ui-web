PROCEDURE "sap.fsdm.procedures::ExercisePeriodErase" (IN ROW "sap.fsdm.tabletypes::ExercisePeriodTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "StartDate" is null and
            "_Option.FinancialContractID" is null and
            "_Option.IDSystem" is null and
            "_Swaption.FinancialContractID" is null and
            "_Swaption.IDSystem" is null and
            "_Warrant.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ExercisePeriod"
        WHERE
        (            "StartDate" ,
            "_Option.FinancialContractID" ,
            "_Option.IDSystem" ,
            "_Swaption.FinancialContractID" ,
            "_Swaption.IDSystem" ,
            "_Warrant.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."StartDate" ,
                "OLD"."_Option.FinancialContractID" ,
                "OLD"."_Option.IDSystem" ,
                "OLD"."_Swaption.FinancialContractID" ,
                "OLD"."_Swaption.IDSystem" ,
                "OLD"."_Warrant.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::ExercisePeriod" "OLD"
            on
            "IN"."StartDate" = "OLD"."StartDate" and
            "IN"."_Option.FinancialContractID" = "OLD"."_Option.FinancialContractID" and
            "IN"."_Option.IDSystem" = "OLD"."_Option.IDSystem" and
            "IN"."_Swaption.FinancialContractID" = "OLD"."_Swaption.FinancialContractID" and
            "IN"."_Swaption.IDSystem" = "OLD"."_Swaption.IDSystem" and
            "IN"."_Warrant.FinancialInstrumentID" = "OLD"."_Warrant.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ExercisePeriod_Historical"
        WHERE
        (
            "StartDate" ,
            "_Option.FinancialContractID" ,
            "_Option.IDSystem" ,
            "_Swaption.FinancialContractID" ,
            "_Swaption.IDSystem" ,
            "_Warrant.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."StartDate" ,
                "OLD"."_Option.FinancialContractID" ,
                "OLD"."_Option.IDSystem" ,
                "OLD"."_Swaption.FinancialContractID" ,
                "OLD"."_Swaption.IDSystem" ,
                "OLD"."_Warrant.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::ExercisePeriod_Historical" "OLD"
            on
                "IN"."StartDate" = "OLD"."StartDate" and
                "IN"."_Option.FinancialContractID" = "OLD"."_Option.FinancialContractID" and
                "IN"."_Option.IDSystem" = "OLD"."_Option.IDSystem" and
                "IN"."_Swaption.FinancialContractID" = "OLD"."_Swaption.FinancialContractID" and
                "IN"."_Swaption.IDSystem" = "OLD"."_Swaption.IDSystem" and
                "IN"."_Warrant.FinancialInstrumentID" = "OLD"."_Warrant.FinancialInstrumentID" 
        );

END
