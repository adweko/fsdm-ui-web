PROCEDURE "sap.fsdm.procedures::SafeDepositLockerDelReadOnly" (IN ROW "sap.fsdm.tabletypes::SafeDepositLockerTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::SafeDepositLockerTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SafeDepositLockerTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'LockerID=' || TO_VARCHAR("LockerID") || ' ' ||
                '_SafeDepositLockerBranch.BankingChannelID=' || TO_VARCHAR("_SafeDepositLockerBranch.BankingChannelID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."LockerID",
                        "IN"."_SafeDepositLockerBranch.BankingChannelID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."LockerID",
                        "IN"."_SafeDepositLockerBranch.BankingChannelID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "LockerID",
                        "_SafeDepositLockerBranch.BankingChannelID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."LockerID",
                                    "IN"."_SafeDepositLockerBranch.BankingChannelID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."LockerID",
                                    "IN"."_SafeDepositLockerBranch.BankingChannelID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "LockerID" is null and
            "_SafeDepositLockerBranch.BankingChannelID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "LockerID",
            "_SafeDepositLockerBranch.BankingChannelID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::SafeDepositLocker" WHERE
            (
            "LockerID" ,
            "_SafeDepositLockerBranch.BankingChannelID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."LockerID",
            "OLD"."_SafeDepositLockerBranch.BankingChannelID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SafeDepositLocker" as "OLD"
        on
                              "IN"."LockerID" = "OLD"."LockerID" and
                              "IN"."_SafeDepositLockerBranch.BankingChannelID" = "OLD"."_SafeDepositLockerBranch.BankingChannelID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "LockerID",
        "_SafeDepositLockerBranch.BankingChannelID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Description",
        "LockerType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_LockerID" as "LockerID" ,
            "OLD__SafeDepositLockerBranch.BankingChannelID" as "_SafeDepositLockerBranch.BankingChannelID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Description" as "Description" ,
            "OLD_LockerType" as "LockerType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."LockerID",
                        "OLD"."_SafeDepositLockerBranch.BankingChannelID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."LockerID" AS "OLD_LockerID" ,
                "OLD"."_SafeDepositLockerBranch.BankingChannelID" AS "OLD__SafeDepositLockerBranch.BankingChannelID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."LockerType" AS "OLD_LockerType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SafeDepositLocker" as "OLD"
            on
                                      "IN"."LockerID" = "OLD"."LockerID" and
                                      "IN"."_SafeDepositLockerBranch.BankingChannelID" = "OLD"."_SafeDepositLockerBranch.BankingChannelID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_LockerID" as "LockerID",
            "OLD__SafeDepositLockerBranch.BankingChannelID" as "_SafeDepositLockerBranch.BankingChannelID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Description" as "Description",
            "OLD_LockerType" as "LockerType",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."LockerID",
                        "OLD"."_SafeDepositLockerBranch.BankingChannelID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."LockerID" AS "OLD_LockerID" ,
                "OLD"."_SafeDepositLockerBranch.BankingChannelID" AS "OLD__SafeDepositLockerBranch.BankingChannelID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."LockerType" AS "OLD_LockerType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SafeDepositLocker" as "OLD"
            on
               "IN"."LockerID" = "OLD"."LockerID" and
               "IN"."_SafeDepositLockerBranch.BankingChannelID" = "OLD"."_SafeDepositLockerBranch.BankingChannelID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
