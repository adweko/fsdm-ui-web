PROCEDURE "sap.fsdm.procedures::PriceGainErase" (IN ROW "sap.fsdm.tabletypes::PriceGainTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "LotID" is null and
            "PriceGainCalculationMethod" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null and
            "_SettlementItem.IDSystem" is null and
            "_SettlementItem.ItemNumber" is null and
            "_SettlementItem.SettlementID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::PriceGain"
        WHERE
        (            "LotID" ,
            "PriceGainCalculationMethod" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_SettlementItem.IDSystem" ,
            "_SettlementItem.ItemNumber" ,
            "_SettlementItem.SettlementID" 
        ) in
        (
            select                 "OLD"."LotID" ,
                "OLD"."PriceGainCalculationMethod" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_SettlementItem.IDSystem" ,
                "OLD"."_SettlementItem.ItemNumber" ,
                "OLD"."_SettlementItem.SettlementID" 
            from :ROW "IN"
            inner join "sap.fsdm::PriceGain" "OLD"
            on
            "IN"."LotID" = "OLD"."LotID" and
            "IN"."PriceGainCalculationMethod" = "OLD"."PriceGainCalculationMethod" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
            "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
            "IN"."_SettlementItem.IDSystem" = "OLD"."_SettlementItem.IDSystem" and
            "IN"."_SettlementItem.ItemNumber" = "OLD"."_SettlementItem.ItemNumber" and
            "IN"."_SettlementItem.SettlementID" = "OLD"."_SettlementItem.SettlementID" 
        );

        --delete data from history table
        delete from "sap.fsdm::PriceGain_Historical"
        WHERE
        (
            "LotID" ,
            "PriceGainCalculationMethod" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_SettlementItem.IDSystem" ,
            "_SettlementItem.ItemNumber" ,
            "_SettlementItem.SettlementID" 
        ) in
        (
            select
                "OLD"."LotID" ,
                "OLD"."PriceGainCalculationMethod" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_SettlementItem.IDSystem" ,
                "OLD"."_SettlementItem.ItemNumber" ,
                "OLD"."_SettlementItem.SettlementID" 
            from :ROW "IN"
            inner join "sap.fsdm::PriceGain_Historical" "OLD"
            on
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."PriceGainCalculationMethod" = "OLD"."PriceGainCalculationMethod" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                "IN"."_SettlementItem.IDSystem" = "OLD"."_SettlementItem.IDSystem" and
                "IN"."_SettlementItem.ItemNumber" = "OLD"."_SettlementItem.ItemNumber" and
                "IN"."_SettlementItem.SettlementID" = "OLD"."_SettlementItem.SettlementID" 
        );

END
