PROCEDURE "sap.fsdm.procedures::EffectiveInterestRateEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::EffectiveInterestRateTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::EffectiveInterestRateTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::EffectiveInterestRateTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "EffectiveRateCalculationMethod" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" is null and
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "EffectiveRateCalculationMethod" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "_Collection._Client.BusinessPartnerID" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."EffectiveRateCalculationMethod" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EffectiveInterestRate" "OLD"
            on
                "IN"."EffectiveRateCalculationMethod" = "OLD"."EffectiveRateCalculationMethod" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "EffectiveRateCalculationMethod" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."EffectiveRateCalculationMethod" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EffectiveInterestRate_Historical" "OLD"
            on
                "IN"."EffectiveRateCalculationMethod" = "OLD"."EffectiveRateCalculationMethod" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.PositionCurrency" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCcyOfMultiCcyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
