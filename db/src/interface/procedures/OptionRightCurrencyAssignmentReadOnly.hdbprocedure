PROCEDURE "sap.fsdm.procedures::OptionRightCurrencyAssignmentReadOnly" (IN ROW "sap.fsdm.tabletypes::OptionRightCurrencyAssignmentTT", OUT CURR_DEL "sap.fsdm.tabletypes::OptionRightCurrencyAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::OptionRightCurrencyAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Currency.CurrencyCode=' || TO_VARCHAR("_Currency.CurrencyCode") || ' ' ||
                '_OptionRight.SequenceNumber=' || TO_VARCHAR("_OptionRight.SequenceNumber") || ' ' ||
                '_OptionRight._FinancialContract.FinancialContractID=' || TO_VARCHAR("_OptionRight._FinancialContract.FinancialContractID") || ' ' ||
                '_OptionRight._FinancialContract.IDSystem=' || TO_VARCHAR("_OptionRight._FinancialContract.IDSystem") || ' ' ||
                '_OptionRight._FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_OptionRight._FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_OptionRight._TrancheInSyndication.TrancheSequenceNumber=' || TO_VARCHAR("_OptionRight._TrancheInSyndication.TrancheSequenceNumber") || ' ' ||
                '_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID") || ' ' ||
                '_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem=' || TO_VARCHAR("_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Currency.CurrencyCode",
                        "IN"."_OptionRight.SequenceNumber",
                        "IN"."_OptionRight._FinancialContract.FinancialContractID",
                        "IN"."_OptionRight._FinancialContract.IDSystem",
                        "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
                        "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Currency.CurrencyCode",
                        "IN"."_OptionRight.SequenceNumber",
                        "IN"."_OptionRight._FinancialContract.FinancialContractID",
                        "IN"."_OptionRight._FinancialContract.IDSystem",
                        "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
                        "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Currency.CurrencyCode",
                        "_OptionRight.SequenceNumber",
                        "_OptionRight._FinancialContract.FinancialContractID",
                        "_OptionRight._FinancialContract.IDSystem",
                        "_OptionRight._FinancialInstrument.FinancialInstrumentID",
                        "_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Currency.CurrencyCode",
                                    "IN"."_OptionRight.SequenceNumber",
                                    "IN"."_OptionRight._FinancialContract.FinancialContractID",
                                    "IN"."_OptionRight._FinancialContract.IDSystem",
                                    "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Currency.CurrencyCode",
                                    "IN"."_OptionRight.SequenceNumber",
                                    "IN"."_OptionRight._FinancialContract.FinancialContractID",
                                    "IN"."_OptionRight._FinancialContract.IDSystem",
                                    "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "_Currency.CurrencyCode",
        "_OptionRight.SequenceNumber",
        "_OptionRight._FinancialContract.FinancialContractID",
        "_OptionRight._FinancialContract.IDSystem",
        "_OptionRight._FinancialInstrument.FinancialInstrumentID",
        "_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
        "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::OptionRightCurrencyAssignment" WHERE
        (            "_Currency.CurrencyCode" ,
            "_OptionRight.SequenceNumber" ,
            "_OptionRight._FinancialContract.FinancialContractID" ,
            "_OptionRight._FinancialContract.IDSystem" ,
            "_OptionRight._FinancialInstrument.FinancialInstrumentID" ,
            "_OptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."_Currency.CurrencyCode",
            "OLD"."_OptionRight.SequenceNumber",
            "OLD"."_OptionRight._FinancialContract.FinancialContractID",
            "OLD"."_OptionRight._FinancialContract.IDSystem",
            "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
            "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
            "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::OptionRightCurrencyAssignment" as "OLD"
            on
               ifnull( "IN"."_Currency.CurrencyCode",'' ) = "OLD"."_Currency.CurrencyCode" and
               ifnull( "IN"."_OptionRight.SequenceNumber",-1 ) = "OLD"."_OptionRight.SequenceNumber" and
               ifnull( "IN"."_OptionRight._FinancialContract.FinancialContractID",'' ) = "OLD"."_OptionRight._FinancialContract.FinancialContractID" and
               ifnull( "IN"."_OptionRight._FinancialContract.IDSystem",'' ) = "OLD"."_OptionRight._FinancialContract.IDSystem" and
               ifnull( "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",-1 ) = "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" and
               ifnull( "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",'' ) = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
               ifnull( "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",'' ) = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "_Currency.CurrencyCode",
        "_OptionRight.SequenceNumber",
        "_OptionRight._FinancialContract.FinancialContractID",
        "_OptionRight._FinancialContract.IDSystem",
        "_OptionRight._FinancialInstrument.FinancialInstrumentID",
        "_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
        "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "_Currency.CurrencyCode", '' ) as "_Currency.CurrencyCode",
                    ifnull( "_OptionRight.SequenceNumber", -1 ) as "_OptionRight.SequenceNumber",
                    ifnull( "_OptionRight._FinancialContract.FinancialContractID", '' ) as "_OptionRight._FinancialContract.FinancialContractID",
                    ifnull( "_OptionRight._FinancialContract.IDSystem", '' ) as "_OptionRight._FinancialContract.IDSystem",
                    ifnull( "_OptionRight._FinancialInstrument.FinancialInstrumentID", '' ) as "_OptionRight._FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_OptionRight._TrancheInSyndication.TrancheSequenceNumber", -1 ) as "_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                    ifnull( "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID", '' ) as "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                    ifnull( "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem", '' ) as "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD__Currency.CurrencyCode" as "_Currency.CurrencyCode" ,
                    "OLD__OptionRight.SequenceNumber" as "_OptionRight.SequenceNumber" ,
                    "OLD__OptionRight._FinancialContract.FinancialContractID" as "_OptionRight._FinancialContract.FinancialContractID" ,
                    "OLD__OptionRight._FinancialContract.IDSystem" as "_OptionRight._FinancialContract.IDSystem" ,
                    "OLD__OptionRight._FinancialInstrument.FinancialInstrumentID" as "_OptionRight._FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__OptionRight._TrancheInSyndication.TrancheSequenceNumber" as "_OptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
                    "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                    "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."_Currency.CurrencyCode",
                        "IN"."_OptionRight.SequenceNumber",
                        "IN"."_OptionRight._FinancialContract.FinancialContractID",
                        "IN"."_OptionRight._FinancialContract.IDSystem",
                        "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
                        "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."_Currency.CurrencyCode" as "OLD__Currency.CurrencyCode",
                                "OLD"."_OptionRight.SequenceNumber" as "OLD__OptionRight.SequenceNumber",
                                "OLD"."_OptionRight._FinancialContract.FinancialContractID" as "OLD__OptionRight._FinancialContract.FinancialContractID",
                                "OLD"."_OptionRight._FinancialContract.IDSystem" as "OLD__OptionRight._FinancialContract.IDSystem",
                                "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" as "OLD__OptionRight._FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" as "OLD__OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                                "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" as "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::OptionRightCurrencyAssignment" as "OLD"
            on
                ifnull( "IN"."_Currency.CurrencyCode", '') = "OLD"."_Currency.CurrencyCode" and
                ifnull( "IN"."_OptionRight.SequenceNumber", -1) = "OLD"."_OptionRight.SequenceNumber" and
                ifnull( "IN"."_OptionRight._FinancialContract.FinancialContractID", '') = "OLD"."_OptionRight._FinancialContract.FinancialContractID" and
                ifnull( "IN"."_OptionRight._FinancialContract.IDSystem", '') = "OLD"."_OptionRight._FinancialContract.IDSystem" and
                ifnull( "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber", -1) = "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" and
                ifnull( "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID", '') = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                ifnull( "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem", '') = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD__Currency.CurrencyCode" as "_Currency.CurrencyCode",
            "OLD__OptionRight.SequenceNumber" as "_OptionRight.SequenceNumber",
            "OLD__OptionRight._FinancialContract.FinancialContractID" as "_OptionRight._FinancialContract.FinancialContractID",
            "OLD__OptionRight._FinancialContract.IDSystem" as "_OptionRight._FinancialContract.IDSystem",
            "OLD__OptionRight._FinancialInstrument.FinancialInstrumentID" as "_OptionRight._FinancialInstrument.FinancialInstrumentID",
            "OLD__OptionRight._TrancheInSyndication.TrancheSequenceNumber" as "_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
            "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."_Currency.CurrencyCode",
                        "IN"."_OptionRight.SequenceNumber",
                        "IN"."_OptionRight._FinancialContract.FinancialContractID",
                        "IN"."_OptionRight._FinancialContract.IDSystem",
                        "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID",
                        "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."_Currency.CurrencyCode" as "OLD__Currency.CurrencyCode",
                        "OLD"."_OptionRight.SequenceNumber" as "OLD__OptionRight.SequenceNumber",
                        "OLD"."_OptionRight._FinancialContract.FinancialContractID" as "OLD__OptionRight._FinancialContract.FinancialContractID",
                        "OLD"."_OptionRight._FinancialContract.IDSystem" as "OLD__OptionRight._FinancialContract.IDSystem",
                        "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" as "OLD__OptionRight._FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" as "OLD__OptionRight._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" as "OLD__OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::OptionRightCurrencyAssignment" as "OLD"
            on
                ifnull("IN"."_Currency.CurrencyCode", '') = "OLD"."_Currency.CurrencyCode" and
                ifnull("IN"."_OptionRight.SequenceNumber", -1) = "OLD"."_OptionRight.SequenceNumber" and
                ifnull("IN"."_OptionRight._FinancialContract.FinancialContractID", '') = "OLD"."_OptionRight._FinancialContract.FinancialContractID" and
                ifnull("IN"."_OptionRight._FinancialContract.IDSystem", '') = "OLD"."_OptionRight._FinancialContract.IDSystem" and
                ifnull("IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber", -1) = "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" and
                ifnull("IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID", '') = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                ifnull("IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem", '') = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
