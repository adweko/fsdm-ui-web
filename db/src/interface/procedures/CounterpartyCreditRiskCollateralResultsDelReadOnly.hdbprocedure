PROCEDURE "sap.fsdm.procedures::CounterpartyCreditRiskCollateralResultsDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CounterpartyCreditRiskCollateralResultsTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CounterpartyCreditRiskCollateralResultsTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CounterpartyCreditRiskCollateralResultsTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'MarginedNettingSet=' || TO_VARCHAR("MarginedNettingSet") || ' ' ||
                '_CollateralPortion.PortionNumber=' || TO_VARCHAR("_CollateralPortion.PortionNumber") || ' ' ||
                '_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID=' || TO_VARCHAR("_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID") || ' ' ||
                '_CollateralPortion.ASSOC_CollateralAgreement.IDSystem=' || TO_VARCHAR("_CollateralPortion.ASSOC_CollateralAgreement.IDSystem") || ' ' ||
                '_ExchangeTradedNettingSet.FinancialInstrumentID=' || TO_VARCHAR("_ExchangeTradedNettingSet.FinancialInstrumentID") || ' ' ||
                '_FinancialContractNettingSet.FinancialContractID=' || TO_VARCHAR("_FinancialContractNettingSet.FinancialContractID") || ' ' ||
                '_FinancialContractNettingSet.IDSystem=' || TO_VARCHAR("_FinancialContractNettingSet.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."MarginedNettingSet",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."MarginedNettingSet",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "MarginedNettingSet",
                        "_CollateralPortion.PortionNumber",
                        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "_FinancialContractNettingSet.FinancialContractID",
                        "_FinancialContractNettingSet.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."MarginedNettingSet",
                                    "IN"."_CollateralPortion.PortionNumber",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."MarginedNettingSet",
                                    "IN"."_CollateralPortion.PortionNumber",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "MarginedNettingSet" is null and
            "_CollateralPortion.PortionNumber" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null and
            "_ExchangeTradedNettingSet.FinancialInstrumentID" is null and
            "_FinancialContractNettingSet.FinancialContractID" is null and
            "_FinancialContractNettingSet.IDSystem" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" is null and
            "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "MarginedNettingSet",
            "_CollateralPortion.PortionNumber",
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "_ExchangeTradedNettingSet.FinancialInstrumentID",
            "_FinancialContractNettingSet.FinancialContractID",
            "_FinancialContractNettingSet.IDSystem",
            "_ResultGroup.ResultDataProvider",
            "_ResultGroup.ResultGroupID",
            "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CounterpartyCreditRiskCollateralResults" WHERE
            (
            "MarginedNettingSet" ,
            "_CollateralPortion.PortionNumber" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
            "_FinancialContractNettingSet.FinancialContractID" ,
            "_FinancialContractNettingSet.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
            "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."MarginedNettingSet",
            "OLD"."_CollateralPortion.PortionNumber",
            "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD"."_FinancialContractNettingSet.FinancialContractID",
            "OLD"."_FinancialContractNettingSet.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CounterpartyCreditRiskCollateralResults" as "OLD"
        on
                              "IN"."MarginedNettingSet" = "OLD"."MarginedNettingSet" and
                              "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                              "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                              "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                              "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID" = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                              "IN"."_FinancialContractNettingSet.FinancialContractID" = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                              "IN"."_FinancialContractNettingSet.IDSystem" = "OLD"."_FinancialContractNettingSet.IDSystem" and
                              "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                              "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                              "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                              "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "MarginedNettingSet",
        "_CollateralPortion.PortionNumber",
        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CollateralAmount",
        "CollateralAmountCurrency",
        "Eligible",
        "MarginType",
        "PostedOrReceived",
        "SegregatedAccount",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_MarginedNettingSet" as "MarginedNettingSet" ,
            "OLD__CollateralPortion.PortionNumber" as "_CollateralPortion.PortionNumber" ,
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
            "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID" ,
            "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CollateralAmount" as "CollateralAmount" ,
            "OLD_CollateralAmountCurrency" as "CollateralAmountCurrency" ,
            "OLD_Eligible" as "Eligible" ,
            "OLD_MarginType" as "MarginType" ,
            "OLD_PostedOrReceived" as "PostedOrReceived" ,
            "OLD_SegregatedAccount" as "SegregatedAccount" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."MarginedNettingSet",
                        "OLD"."_CollateralPortion.PortionNumber",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "OLD"."_FinancialContractNettingSet.FinancialContractID",
                        "OLD"."_FinancialContractNettingSet.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."MarginedNettingSet" AS "OLD_MarginedNettingSet" ,
                "OLD"."_CollateralPortion.PortionNumber" AS "OLD__CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" AS "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" ,
                "OLD"."_FinancialContractNettingSet.FinancialContractID" AS "OLD__FinancialContractNettingSet.FinancialContractID" ,
                "OLD"."_FinancialContractNettingSet.IDSystem" AS "OLD__FinancialContractNettingSet.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" AS "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" AS "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CollateralAmount" AS "OLD_CollateralAmount" ,
                "OLD"."CollateralAmountCurrency" AS "OLD_CollateralAmountCurrency" ,
                "OLD"."Eligible" AS "OLD_Eligible" ,
                "OLD"."MarginType" AS "OLD_MarginType" ,
                "OLD"."PostedOrReceived" AS "OLD_PostedOrReceived" ,
                "OLD"."SegregatedAccount" AS "OLD_SegregatedAccount" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskCollateralResults" as "OLD"
            on
                                      "IN"."MarginedNettingSet" = "OLD"."MarginedNettingSet" and
                                      "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                                      "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                                      "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                                      "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID" = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                                      "IN"."_FinancialContractNettingSet.FinancialContractID" = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                                      "IN"."_FinancialContractNettingSet.IDSystem" = "OLD"."_FinancialContractNettingSet.IDSystem" and
                                      "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                      "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                                      "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                                      "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_MarginedNettingSet" as "MarginedNettingSet",
            "OLD__CollateralPortion.PortionNumber" as "_CollateralPortion.PortionNumber",
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID",
            "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CollateralAmount" as "CollateralAmount",
            "OLD_CollateralAmountCurrency" as "CollateralAmountCurrency",
            "OLD_Eligible" as "Eligible",
            "OLD_MarginType" as "MarginType",
            "OLD_PostedOrReceived" as "PostedOrReceived",
            "OLD_SegregatedAccount" as "SegregatedAccount",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."MarginedNettingSet",
                        "OLD"."_CollateralPortion.PortionNumber",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "OLD"."_FinancialContractNettingSet.FinancialContractID",
                        "OLD"."_FinancialContractNettingSet.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."MarginedNettingSet" AS "OLD_MarginedNettingSet" ,
                "OLD"."_CollateralPortion.PortionNumber" AS "OLD__CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" AS "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" ,
                "OLD"."_FinancialContractNettingSet.FinancialContractID" AS "OLD__FinancialContractNettingSet.FinancialContractID" ,
                "OLD"."_FinancialContractNettingSet.IDSystem" AS "OLD__FinancialContractNettingSet.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" AS "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" AS "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CollateralAmount" AS "OLD_CollateralAmount" ,
                "OLD"."CollateralAmountCurrency" AS "OLD_CollateralAmountCurrency" ,
                "OLD"."Eligible" AS "OLD_Eligible" ,
                "OLD"."MarginType" AS "OLD_MarginType" ,
                "OLD"."PostedOrReceived" AS "OLD_PostedOrReceived" ,
                "OLD"."SegregatedAccount" AS "OLD_SegregatedAccount" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskCollateralResults" as "OLD"
            on
               "IN"."MarginedNettingSet" = "OLD"."MarginedNettingSet" and
               "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
               "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
               "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
               "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID" = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
               "IN"."_FinancialContractNettingSet.FinancialContractID" = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
               "IN"."_FinancialContractNettingSet.IDSystem" = "OLD"."_FinancialContractNettingSet.IDSystem" and
               "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
               "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
               "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
               "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
