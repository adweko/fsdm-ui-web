PROCEDURE "sap.fsdm.procedures::FinancialStatementHierarchyErase" (IN ROW "sap.fsdm.tabletypes::FinancialStatementHierarchyTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "FinancialStatementHierarchyID" is null and
            "_Child.FinancialStatementSegmentID" is null and
            "_Child._FinancialStatementStructure.FinancialStatementStructureID" is null and
            "_Child._GLAccount.GLAccount" is null and
            "_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" is null and
            "_Child._SubledgerAccount.SubledgerAccount" is null and
            "_Parent.FinancialStatementSegmentID" is null and
            "_Parent._FinancialStatementStructure.FinancialStatementStructureID" is null and
            "_Parent._GLAccount.GLAccount" is null and
            "_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" is null and
            "_Parent._SubledgerAccount.SubledgerAccount" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::FinancialStatementHierarchy"
        WHERE
        (            "FinancialStatementHierarchyID" ,
            "_Child.FinancialStatementSegmentID" ,
            "_Child._FinancialStatementStructure.FinancialStatementStructureID" ,
            "_Child._GLAccount.GLAccount" ,
            "_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_Child._SubledgerAccount.SubledgerAccount" ,
            "_Parent.FinancialStatementSegmentID" ,
            "_Parent._FinancialStatementStructure.FinancialStatementStructureID" ,
            "_Parent._GLAccount.GLAccount" ,
            "_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_Parent._SubledgerAccount.SubledgerAccount" 
        ) in
        (
            select                 "OLD"."FinancialStatementHierarchyID" ,
                "OLD"."_Child.FinancialStatementSegmentID" ,
                "OLD"."_Child._FinancialStatementStructure.FinancialStatementStructureID" ,
                "OLD"."_Child._GLAccount.GLAccount" ,
                "OLD"."_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_Child._SubledgerAccount.SubledgerAccount" ,
                "OLD"."_Parent.FinancialStatementSegmentID" ,
                "OLD"."_Parent._FinancialStatementStructure.FinancialStatementStructureID" ,
                "OLD"."_Parent._GLAccount.GLAccount" ,
                "OLD"."_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_Parent._SubledgerAccount.SubledgerAccount" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialStatementHierarchy" "OLD"
            on
            "IN"."FinancialStatementHierarchyID" = "OLD"."FinancialStatementHierarchyID" and
            "IN"."_Child.FinancialStatementSegmentID" = "OLD"."_Child.FinancialStatementSegmentID" and
            "IN"."_Child._FinancialStatementStructure.FinancialStatementStructureID" = "OLD"."_Child._FinancialStatementStructure.FinancialStatementStructureID" and
            "IN"."_Child._GLAccount.GLAccount" = "OLD"."_Child._GLAccount.GLAccount" and
            "IN"."_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" and
            "IN"."_Child._SubledgerAccount.SubledgerAccount" = "OLD"."_Child._SubledgerAccount.SubledgerAccount" and
            "IN"."_Parent.FinancialStatementSegmentID" = "OLD"."_Parent.FinancialStatementSegmentID" and
            "IN"."_Parent._FinancialStatementStructure.FinancialStatementStructureID" = "OLD"."_Parent._FinancialStatementStructure.FinancialStatementStructureID" and
            "IN"."_Parent._GLAccount.GLAccount" = "OLD"."_Parent._GLAccount.GLAccount" and
            "IN"."_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" and
            "IN"."_Parent._SubledgerAccount.SubledgerAccount" = "OLD"."_Parent._SubledgerAccount.SubledgerAccount" 
        );

        --delete data from history table
        delete from "sap.fsdm::FinancialStatementHierarchy_Historical"
        WHERE
        (
            "FinancialStatementHierarchyID" ,
            "_Child.FinancialStatementSegmentID" ,
            "_Child._FinancialStatementStructure.FinancialStatementStructureID" ,
            "_Child._GLAccount.GLAccount" ,
            "_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_Child._SubledgerAccount.SubledgerAccount" ,
            "_Parent.FinancialStatementSegmentID" ,
            "_Parent._FinancialStatementStructure.FinancialStatementStructureID" ,
            "_Parent._GLAccount.GLAccount" ,
            "_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_Parent._SubledgerAccount.SubledgerAccount" 
        ) in
        (
            select
                "OLD"."FinancialStatementHierarchyID" ,
                "OLD"."_Child.FinancialStatementSegmentID" ,
                "OLD"."_Child._FinancialStatementStructure.FinancialStatementStructureID" ,
                "OLD"."_Child._GLAccount.GLAccount" ,
                "OLD"."_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_Child._SubledgerAccount.SubledgerAccount" ,
                "OLD"."_Parent.FinancialStatementSegmentID" ,
                "OLD"."_Parent._FinancialStatementStructure.FinancialStatementStructureID" ,
                "OLD"."_Parent._GLAccount.GLAccount" ,
                "OLD"."_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_Parent._SubledgerAccount.SubledgerAccount" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialStatementHierarchy_Historical" "OLD"
            on
                "IN"."FinancialStatementHierarchyID" = "OLD"."FinancialStatementHierarchyID" and
                "IN"."_Child.FinancialStatementSegmentID" = "OLD"."_Child.FinancialStatementSegmentID" and
                "IN"."_Child._FinancialStatementStructure.FinancialStatementStructureID" = "OLD"."_Child._FinancialStatementStructure.FinancialStatementStructureID" and
                "IN"."_Child._GLAccount.GLAccount" = "OLD"."_Child._GLAccount.GLAccount" and
                "IN"."_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_Child._GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_Child._SubledgerAccount.SubledgerAccount" = "OLD"."_Child._SubledgerAccount.SubledgerAccount" and
                "IN"."_Parent.FinancialStatementSegmentID" = "OLD"."_Parent.FinancialStatementSegmentID" and
                "IN"."_Parent._FinancialStatementStructure.FinancialStatementStructureID" = "OLD"."_Parent._FinancialStatementStructure.FinancialStatementStructureID" and
                "IN"."_Parent._GLAccount.GLAccount" = "OLD"."_Parent._GLAccount.GLAccount" and
                "IN"."_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_Parent._GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_Parent._SubledgerAccount.SubledgerAccount" = "OLD"."_Parent._SubledgerAccount.SubledgerAccount" 
        );

END
