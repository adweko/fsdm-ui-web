PROCEDURE "sap.fsdm.procedures::CommunicationExclusionsDelete" (IN ROW "sap.fsdm.tabletypes::CommunicationExclusionsTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ASSOC_CommunicatingCompany.BusinessPartnerID=' || TO_VARCHAR("ASSOC_CommunicatingCompany.BusinessPartnerID") || ' ' ||
                'ASSOC_Partner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_Partner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID",
                        "IN"."ASSOC_Partner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID",
                        "IN"."ASSOC_Partner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ASSOC_CommunicatingCompany.BusinessPartnerID",
                        "ASSOC_Partner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID",
                                    "IN"."ASSOC_Partner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID",
                                    "IN"."ASSOC_Partner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_CommunicatingCompany.BusinessPartnerID" is null and
            "ASSOC_Partner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CommunicationExclusions" (
        "ASSOC_CommunicatingCompany.BusinessPartnerID",
        "ASSOC_Partner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "NoEmailCommunication",
        "NoTextMessageCommunication",
        "NoVoiceCommunication",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ASSOC_CommunicatingCompany.BusinessPartnerID" as "ASSOC_CommunicatingCompany.BusinessPartnerID" ,
            "OLD_ASSOC_Partner.BusinessPartnerID" as "ASSOC_Partner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_NoEmailCommunication" as "NoEmailCommunication" ,
            "OLD_NoTextMessageCommunication" as "NoTextMessageCommunication" ,
            "OLD_NoVoiceCommunication" as "NoVoiceCommunication" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID",
                        "OLD"."ASSOC_Partner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID" AS "OLD_ASSOC_CommunicatingCompany.BusinessPartnerID" ,
                "OLD"."ASSOC_Partner.BusinessPartnerID" AS "OLD_ASSOC_Partner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."NoEmailCommunication" AS "OLD_NoEmailCommunication" ,
                "OLD"."NoTextMessageCommunication" AS "OLD_NoTextMessageCommunication" ,
                "OLD"."NoVoiceCommunication" AS "OLD_NoVoiceCommunication" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CommunicationExclusions" as "OLD"
            on
                      "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID" = "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID" and
                      "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CommunicationExclusions" (
        "ASSOC_CommunicatingCompany.BusinessPartnerID",
        "ASSOC_Partner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "NoEmailCommunication",
        "NoTextMessageCommunication",
        "NoVoiceCommunication",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ASSOC_CommunicatingCompany.BusinessPartnerID" as "ASSOC_CommunicatingCompany.BusinessPartnerID",
            "OLD_ASSOC_Partner.BusinessPartnerID" as "ASSOC_Partner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_NoEmailCommunication" as "NoEmailCommunication",
            "OLD_NoTextMessageCommunication" as "NoTextMessageCommunication",
            "OLD_NoVoiceCommunication" as "NoVoiceCommunication",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID",
                        "OLD"."ASSOC_Partner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID" AS "OLD_ASSOC_CommunicatingCompany.BusinessPartnerID" ,
                "OLD"."ASSOC_Partner.BusinessPartnerID" AS "OLD_ASSOC_Partner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."NoEmailCommunication" AS "OLD_NoEmailCommunication" ,
                "OLD"."NoTextMessageCommunication" AS "OLD_NoTextMessageCommunication" ,
                "OLD"."NoVoiceCommunication" AS "OLD_NoVoiceCommunication" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CommunicationExclusions" as "OLD"
            on
                                                "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID" = "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID" and
                                                "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CommunicationExclusions"
    where (
        "ASSOC_CommunicatingCompany.BusinessPartnerID",
        "ASSOC_Partner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID",
            "OLD"."ASSOC_Partner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CommunicationExclusions" as "OLD"
        on
                                       "IN"."ASSOC_CommunicatingCompany.BusinessPartnerID" = "OLD"."ASSOC_CommunicatingCompany.BusinessPartnerID" and
                                       "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
