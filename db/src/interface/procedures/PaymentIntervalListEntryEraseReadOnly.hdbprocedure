PROCEDURE "sap.fsdm.procedures::PaymentIntervalListEntryEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PaymentIntervalListEntryTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PaymentIntervalListEntryTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PaymentIntervalListEntryTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PaymentIntervalEndDate" is null and
            "PaymentIntervalStartDate" is null and
            "ASSOC_PaymentIntervalList.SequenceNumber" is null and
            "ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" is null and
            "ASSOC_PaymentIntervalList._CreditCardLoan.ID" is null and
            "ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" is null and
            "ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" is null and
            "ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" is null and
            "ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" is null and
            "ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" is null and
            "ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" is null and
            "ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" is null and
            "ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" is null and
            "ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "PaymentIntervalEndDate" ,
                "PaymentIntervalStartDate" ,
                "ASSOC_PaymentIntervalList.SequenceNumber" ,
                "ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" ,
                "ASSOC_PaymentIntervalList._CreditCardLoan.ID" ,
                "ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" ,
                "ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" ,
                "ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" ,
                "ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" ,
                "ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."PaymentIntervalEndDate" ,
                "OLD"."PaymentIntervalStartDate" ,
                "OLD"."ASSOC_PaymentIntervalList.SequenceNumber" ,
                "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" ,
                "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan.ID" ,
                "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PaymentIntervalListEntry" "OLD"
            on
                "IN"."PaymentIntervalEndDate" = "OLD"."PaymentIntervalEndDate" and
                "IN"."PaymentIntervalStartDate" = "OLD"."PaymentIntervalStartDate" and
                "IN"."ASSOC_PaymentIntervalList.SequenceNumber" = "OLD"."ASSOC_PaymentIntervalList.SequenceNumber" and
                "IN"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" and
                "IN"."ASSOC_PaymentIntervalList._CreditCardLoan.ID" = "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan.ID" and
                "IN"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" and
                "IN"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" = "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" and
                "IN"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" = "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" and
                "IN"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" = "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" and
                "IN"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "PaymentIntervalEndDate" ,
            "PaymentIntervalStartDate" ,
            "ASSOC_PaymentIntervalList.SequenceNumber" ,
            "ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" ,
            "ASSOC_PaymentIntervalList._CreditCardLoan.ID" ,
            "ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" ,
            "ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" ,
            "ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" ,
            "ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" ,
            "ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" ,
            "ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
            "ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."PaymentIntervalEndDate" ,
                "OLD"."PaymentIntervalStartDate" ,
                "OLD"."ASSOC_PaymentIntervalList.SequenceNumber" ,
                "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" ,
                "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan.ID" ,
                "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PaymentIntervalListEntry_Historical" "OLD"
            on
                "IN"."PaymentIntervalEndDate" = "OLD"."PaymentIntervalEndDate" and
                "IN"."PaymentIntervalStartDate" = "OLD"."PaymentIntervalStartDate" and
                "IN"."ASSOC_PaymentIntervalList.SequenceNumber" = "OLD"."ASSOC_PaymentIntervalList.SequenceNumber" and
                "IN"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" and
                "IN"."ASSOC_PaymentIntervalList._CreditCardLoan.ID" = "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan.ID" and
                "IN"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" and
                "IN"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" and
                "IN"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" = "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" and
                "IN"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" = "OLD"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" and
                "IN"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" = "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" and
                "IN"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                "IN"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" 
        );

END
