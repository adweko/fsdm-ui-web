PROCEDURE "sap.fsdm.procedures::ClassificationOfTradeDelete" (IN ROW "sap.fsdm.tabletypes::ClassificationOfTradeTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                '_TradeClass.TradeClass=' || TO_VARCHAR("_TradeClass.TradeClass") || ' ' ||
                '_TradeClass.TradeClassificationSystem=' || TO_VARCHAR("_TradeClass.TradeClassificationSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID",
                        "IN"."_TradeClass.TradeClass",
                        "IN"."_TradeClass.TradeClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID",
                        "IN"."_TradeClass.TradeClass",
                        "IN"."_TradeClass.TradeClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Trade.IDSystem",
                        "_Trade.TradeID",
                        "_TradeClass.TradeClass",
                        "_TradeClass.TradeClassificationSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID",
                                    "IN"."_TradeClass.TradeClass",
                                    "IN"."_TradeClass.TradeClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID",
                                    "IN"."_TradeClass.TradeClass",
                                    "IN"."_TradeClass.TradeClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null and
            "_TradeClass.TradeClass" is null and
            "_TradeClass.TradeClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ClassificationOfTrade" (
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "_TradeClass.TradeClass",
        "_TradeClass.TradeClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "OLD__TradeClass.TradeClass" as "_TradeClass.TradeClass" ,
            "OLD__TradeClass.TradeClassificationSystem" as "_TradeClass.TradeClassificationSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."_TradeClass.TradeClass",
                        "OLD"."_TradeClass.TradeClassificationSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."_TradeClass.TradeClass" AS "OLD__TradeClass.TradeClass" ,
                "OLD"."_TradeClass.TradeClassificationSystem" AS "OLD__TradeClass.TradeClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClassificationOfTrade" as "OLD"
            on
                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                      "IN"."_TradeClass.TradeClass" = "OLD"."_TradeClass.TradeClass" and
                      "IN"."_TradeClass.TradeClassificationSystem" = "OLD"."_TradeClass.TradeClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ClassificationOfTrade" (
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "_TradeClass.TradeClass",
        "_TradeClass.TradeClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "OLD__TradeClass.TradeClass" as "_TradeClass.TradeClass",
            "OLD__TradeClass.TradeClassificationSystem" as "_TradeClass.TradeClassificationSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."_TradeClass.TradeClass",
                        "OLD"."_TradeClass.TradeClassificationSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."_TradeClass.TradeClass" AS "OLD__TradeClass.TradeClass" ,
                "OLD"."_TradeClass.TradeClassificationSystem" AS "OLD__TradeClass.TradeClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClassificationOfTrade" as "OLD"
            on
                                                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                                                "IN"."_TradeClass.TradeClass" = "OLD"."_TradeClass.TradeClass" and
                                                "IN"."_TradeClass.TradeClassificationSystem" = "OLD"."_TradeClass.TradeClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ClassificationOfTrade"
    where (
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "_TradeClass.TradeClass",
        "_TradeClass.TradeClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."_TradeClass.TradeClass",
            "OLD"."_TradeClass.TradeClassificationSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ClassificationOfTrade" as "OLD"
        on
                                       "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                       "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                                       "IN"."_TradeClass.TradeClass" = "OLD"."_TradeClass.TradeClass" and
                                       "IN"."_TradeClass.TradeClassificationSystem" = "OLD"."_TradeClass.TradeClassificationSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
