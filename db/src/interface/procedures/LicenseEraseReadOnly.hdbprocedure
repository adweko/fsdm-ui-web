PROCEDURE "sap.fsdm.procedures::LicenseEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::LicenseTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::LicenseTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::LicenseTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Authority" is null and
            "IDSystem" is null and
            "SubjectOfLicenseIdentifier" is null and
            "_BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Authority" ,
                "IDSystem" ,
                "SubjectOfLicenseIdentifier" ,
                "_BusinessPartner.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Authority" ,
                "OLD"."IDSystem" ,
                "OLD"."SubjectOfLicenseIdentifier" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::License" "OLD"
            on
                "IN"."Authority" = "OLD"."Authority" and
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."SubjectOfLicenseIdentifier" = "OLD"."SubjectOfLicenseIdentifier" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Authority" ,
            "IDSystem" ,
            "SubjectOfLicenseIdentifier" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Authority" ,
                "OLD"."IDSystem" ,
                "OLD"."SubjectOfLicenseIdentifier" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::License_Historical" "OLD"
            on
                "IN"."Authority" = "OLD"."Authority" and
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."SubjectOfLicenseIdentifier" = "OLD"."SubjectOfLicenseIdentifier" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        );

END
