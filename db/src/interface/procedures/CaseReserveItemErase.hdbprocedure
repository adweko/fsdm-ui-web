PROCEDURE "sap.fsdm.procedures::CaseReserveItemErase" (IN ROW "sap.fsdm.tabletypes::CaseReserveItemTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ReserveChangeDate" is null and
            "_CaseReserve.Type" is null and
            "_CaseReserve._Claim.ID" is null and
            "_CaseReserve._Claim.IDSystem" is null and
            "_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" is null and
            "_CaseReserve._Subclaim._Claimant._Claim.ID" is null and
            "_CaseReserve._Subclaim._Claimant._Claim.IDSystem" is null and
            "_CaseReserve._Subclaim._InsuranceCoverage.ID" is null and
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CaseReserveItem"
        WHERE
        (            "ReserveChangeDate" ,
            "_CaseReserve.Type" ,
            "_CaseReserve._Claim.ID" ,
            "_CaseReserve._Claim.IDSystem" ,
            "_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
            "_CaseReserve._Subclaim._Claimant._Claim.ID" ,
            "_CaseReserve._Subclaim._Claimant._Claim.IDSystem" ,
            "_CaseReserve._Subclaim._InsuranceCoverage.ID" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        ) in
        (
            select                 "OLD"."ReserveChangeDate" ,
                "OLD"."_CaseReserve.Type" ,
                "OLD"."_CaseReserve._Claim.ID" ,
                "OLD"."_CaseReserve._Claim.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_CaseReserve._Subclaim._Claimant._Claim.ID" ,
                "OLD"."_CaseReserve._Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            from :ROW "IN"
            inner join "sap.fsdm::CaseReserveItem" "OLD"
            on
            "IN"."ReserveChangeDate" = "OLD"."ReserveChangeDate" and
            "IN"."_CaseReserve.Type" = "OLD"."_CaseReserve.Type" and
            "IN"."_CaseReserve._Claim.ID" = "OLD"."_CaseReserve._Claim.ID" and
            "IN"."_CaseReserve._Claim.IDSystem" = "OLD"."_CaseReserve._Claim.IDSystem" and
            "IN"."_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
            "IN"."_CaseReserve._Subclaim._Claimant._Claim.ID" = "OLD"."_CaseReserve._Subclaim._Claimant._Claim.ID" and
            "IN"."_CaseReserve._Subclaim._Claimant._Claim.IDSystem" = "OLD"."_CaseReserve._Subclaim._Claimant._Claim.IDSystem" and
            "IN"."_CaseReserve._Subclaim._InsuranceCoverage.ID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage.ID" and
            "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
            "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
            "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
            "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
            "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        delete from "sap.fsdm::CaseReserveItem_Historical"
        WHERE
        (
            "ReserveChangeDate" ,
            "_CaseReserve.Type" ,
            "_CaseReserve._Claim.ID" ,
            "_CaseReserve._Claim.IDSystem" ,
            "_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
            "_CaseReserve._Subclaim._Claimant._Claim.ID" ,
            "_CaseReserve._Subclaim._Claimant._Claim.IDSystem" ,
            "_CaseReserve._Subclaim._InsuranceCoverage.ID" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        ) in
        (
            select
                "OLD"."ReserveChangeDate" ,
                "OLD"."_CaseReserve.Type" ,
                "OLD"."_CaseReserve._Claim.ID" ,
                "OLD"."_CaseReserve._Claim.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_CaseReserve._Subclaim._Claimant._Claim.ID" ,
                "OLD"."_CaseReserve._Subclaim._Claimant._Claim.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage.ID" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            from :ROW "IN"
            inner join "sap.fsdm::CaseReserveItem_Historical" "OLD"
            on
                "IN"."ReserveChangeDate" = "OLD"."ReserveChangeDate" and
                "IN"."_CaseReserve.Type" = "OLD"."_CaseReserve.Type" and
                "IN"."_CaseReserve._Claim.ID" = "OLD"."_CaseReserve._Claim.ID" and
                "IN"."_CaseReserve._Claim.IDSystem" = "OLD"."_CaseReserve._Claim.IDSystem" and
                "IN"."_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" = "OLD"."_CaseReserve._Subclaim._Claimant._BusinessPartner.BusinessPartnerID" and
                "IN"."_CaseReserve._Subclaim._Claimant._Claim.ID" = "OLD"."_CaseReserve._Subclaim._Claimant._Claim.ID" and
                "IN"."_CaseReserve._Subclaim._Claimant._Claim.IDSystem" = "OLD"."_CaseReserve._Subclaim._Claimant._Claim.IDSystem" and
                "IN"."_CaseReserve._Subclaim._InsuranceCoverage.ID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage.ID" and
                "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_CaseReserve._Subclaim._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
