PROCEDURE "sap.fsdm.procedures::CededRiskCurrencyDelete" (IN ROW "sap.fsdm.tabletypes::CededRiskCurrencyTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Currency=' || TO_VARCHAR("Currency") || ' ' ||
                '_ReinsuranceCoverage.CoverageID=' || TO_VARCHAR("_ReinsuranceCoverage.CoverageID") || ' ' ||
                '_ReinsuranceCoverage.SectionID=' || TO_VARCHAR("_ReinsuranceCoverage.SectionID") || ' ' ||
                '_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID=' || TO_VARCHAR("_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID") || ' ' ||
                '_ReinsuranceCoverage._ReinsuranceContract.IDSystem=' || TO_VARCHAR("_ReinsuranceCoverage._ReinsuranceContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Currency",
                        "IN"."_ReinsuranceCoverage.CoverageID",
                        "IN"."_ReinsuranceCoverage.SectionID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Currency",
                        "IN"."_ReinsuranceCoverage.CoverageID",
                        "IN"."_ReinsuranceCoverage.SectionID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Currency",
                        "_ReinsuranceCoverage.CoverageID",
                        "_ReinsuranceCoverage.SectionID",
                        "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Currency",
                                    "IN"."_ReinsuranceCoverage.CoverageID",
                                    "IN"."_ReinsuranceCoverage.SectionID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Currency",
                                    "IN"."_ReinsuranceCoverage.CoverageID",
                                    "IN"."_ReinsuranceCoverage.SectionID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Currency" is null and
            "_ReinsuranceCoverage.CoverageID" is null and
            "_ReinsuranceCoverage.SectionID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CededRiskCurrency" (
        "Currency",
        "_ReinsuranceCoverage.CoverageID",
        "_ReinsuranceCoverage.SectionID",
        "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
        "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DefaultIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Currency" as "Currency" ,
            "OLD__ReinsuranceCoverage.CoverageID" as "_ReinsuranceCoverage.CoverageID" ,
            "OLD__ReinsuranceCoverage.SectionID" as "_ReinsuranceCoverage.SectionID" ,
            "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" as "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
            "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" as "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_DefaultIndicator" as "DefaultIndicator" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Currency",
                        "OLD"."_ReinsuranceCoverage.CoverageID",
                        "OLD"."_ReinsuranceCoverage.SectionID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" AS "OLD__ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" AS "OLD__ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DefaultIndicator" AS "OLD_DefaultIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CededRiskCurrency" as "OLD"
            on
                      "IN"."Currency" = "OLD"."Currency" and
                      "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                      "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                      "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                      "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CededRiskCurrency" (
        "Currency",
        "_ReinsuranceCoverage.CoverageID",
        "_ReinsuranceCoverage.SectionID",
        "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
        "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DefaultIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Currency" as "Currency",
            "OLD__ReinsuranceCoverage.CoverageID" as "_ReinsuranceCoverage.CoverageID",
            "OLD__ReinsuranceCoverage.SectionID" as "_ReinsuranceCoverage.SectionID",
            "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" as "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
            "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" as "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DefaultIndicator" as "DefaultIndicator",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Currency",
                        "OLD"."_ReinsuranceCoverage.CoverageID",
                        "OLD"."_ReinsuranceCoverage.SectionID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" AS "OLD__ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" AS "OLD__ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DefaultIndicator" AS "OLD_DefaultIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CededRiskCurrency" as "OLD"
            on
                                                "IN"."Currency" = "OLD"."Currency" and
                                                "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                                                "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                                                "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                                                "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CededRiskCurrency"
    where (
        "Currency",
        "_ReinsuranceCoverage.CoverageID",
        "_ReinsuranceCoverage.SectionID",
        "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
        "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Currency",
            "OLD"."_ReinsuranceCoverage.CoverageID",
            "OLD"."_ReinsuranceCoverage.SectionID",
            "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
            "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CededRiskCurrency" as "OLD"
        on
                                       "IN"."Currency" = "OLD"."Currency" and
                                       "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                                       "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                                       "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                                       "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
