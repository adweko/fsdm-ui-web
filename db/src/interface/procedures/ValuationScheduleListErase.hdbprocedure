PROCEDURE "sap.fsdm.procedures::ValuationScheduleListErase" (IN ROW "sap.fsdm.tabletypes::ValuationScheduleTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" is null and
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" is null and
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" is null and
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ValuationSchedule"
        WHERE
        (            "SequenceNumber" ,
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
        ) in
        (
            select                 "OLD"."SequenceNumber" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
            from :ROW "IN"
            inner join "sap.fsdm::ValuationSchedule" "OLD"
            on
            "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
            "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" and
            "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
            "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ValuationSchedule_Historical"
        WHERE
        (
            "SequenceNumber" ,
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
        ) in
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
            from :ROW "IN"
            inner join "sap.fsdm::ValuationSchedule_Historical" "OLD"
            on
                "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" and
                "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" 
        );

END
