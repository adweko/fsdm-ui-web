view "sap.fsdm.SQLViews::Interest_View" 
as select
      "SequenceNumber" ,
      "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
      "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
      "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
      "ASSOC_FinancialContract.FinancialContractID" ,
      "ASSOC_FinancialContract.IDSystem" ,
      "_DebtInstrument.FinancialInstrumentID" ,
      "_OptionOfReferenceRateSpecification.ComponentNumber" ,
      "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
      "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
      "_OptionOfStrikeSpecification.ComponentNumber" ,
      "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
      "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
      "_Trade.IDSystem" ,
      "_Trade.TradeID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "ASSOC_ReferenceRateID.ReferenceRateID" ,
      "AnnuityAmount" ,
      "AnnuityAmountCurrency" ,
      "AveragedReferenceRateCalculationWeightingMethod" ,
      "BusinessDayConvention" ,
      "CompoundingConvention" ,
      "ConditionAcceptanceDate" ,
      "ConditionFixedPeriodEndDate" ,
      "ConditionFixedPeriodStartDate" ,
      "CutoffRelativeToDate" ,
      "DayCountConvention" ,
      "DayOfMonthOfInterestPayment" ,
      "DayOfMonthOfInterestPeriodEnd" ,
      "DayOfMonthOfReset" ,
      "DueDateScheduleIsIndependent" ,
      "DueScheduleBusinessCalendar" ,
      "DueScheduleBusinessDayConvention" ,
      "DueScheduleManualBusinessDayOffset" ,
      "DueSchedulePeriodLength" ,
      "DueSchedulePeriodTimeUnit" ,
      "FirstAnnuityPeriodStartDate" ,
      "FirstDueDate" ,
      "FirstInterestPeriodEndDate" ,
      "FirstInterestPeriodStartDate" ,
      "FirstRegularFloatingRateResetDate" ,
      "FixedRate" ,
      "FixingRateSpecificationCategory" ,
      "FloatingRateMax" ,
      "FloatingRateMin" ,
      "InstallmentAmount" ,
      "InstallmentAmountCurrency" ,
      "InstallmentInterestAmount" ,
      "InstallmentInterestAmountCurrency" ,
      "InterestBusinessCalendar" ,
      "InterestCalculationBaseType" ,
      "InterestCategory" ,
      "InterestCurrency" ,
      "InterestInAdvance" ,
      "InterestIsCompounded" ,
      "InterestPaymentPrecision" ,
      "InterestPaymentRoundingMethod" ,
      "InterestPeriodLength" ,
      "InterestPeriodTimeUnit" ,
      "InterestScheduleType" ,
      "InterestSpecificationCategory" ,
      "InterestSubPeriodLength" ,
      "InterestSubPeriodSpecificationCategory" ,
      "InterestSubPeriodTimeUnit" ,
      "InterestSubtype" ,
      "InterestType" ,
      "LastInterestPeriodEndDate" ,
      "LifecycleStatus" ,
      "LifecycleStatusChangeDate" ,
      "LifecycleStatusReason" ,
      "ManualBusinessDayOffset" ,
      "NumberOfInstallments" ,
      "PayingOrReceiving" ,
      "PeriodEndDueDateLag" ,
      "PeriodEndDueDateLagTimeUnit" ,
      "PreconditionApplies" ,
      "ReferenceRateFactor" ,
      "ReferenceRateFormula" ,
      "RelativeToInterestPeriodStartOrEnd" ,
      "ResetAtMonthUltimo" ,
      "ResetBusinessCalendar" ,
      "ResetBusinessDayConvention" ,
      "ResetCutoffLength" ,
      "ResetCutoffTimeUnit" ,
      "ResetInArrears" ,
      "ResetLagLength" ,
      "ResetLagTimeUnit" ,
      "ResetManualBusinessDayOffset" ,
      "ResetPeriodLength" ,
      "ResetPeriodTimeUnit" ,
      "ResetPrecision" ,
      "ResetRounding" ,
      "RoleOfPayer" ,
      "ScaleApplies" ,
      "Spread" ,
      "TotalInstallmentAmount" ,
      "TotalInstallmentInterestAmount" ,
      "VariableRateMax" ,
      "VariableRateMin" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::Interest"
with associations
(
	join "sap.fsdm.SQLViews::Barrier_View" as "_Barrier__Interest"
	  on "_Trade.IDSystem" = "_Barrier__Interest"."_Interest._Trade.IDSystem" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_Barrier__Interest"."_Interest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_Barrier__Interest"."_Interest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_Barrier__Interest"."_Interest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_Barrier__Interest"."_Interest.ASSOC_FinancialContract.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_Barrier__Interest"."_Interest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_Barrier__Interest"."_Interest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_Barrier__Interest"."_Interest._DebtInstrument.FinancialInstrumentID" AND 
	     "SequenceNumber" = "_Barrier__Interest"."_Interest.SequenceNumber" AND 
	     "_Trade.TradeID" = "_Barrier__Interest"."_Interest._Trade.TradeID" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_Barrier__Interest"."_Interest._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_Barrier__Interest"."_Interest.ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_Barrier__Interest"."_Interest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "_OptionOfReferenceRateSpecification.ComponentNumber" = "_Barrier__Interest"."_Interest._OptionOfReferenceRateSpecification.ComponentNumber" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_Barrier__Interest"."_Interest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CashFlowStream_View" as "_CashFlowStream_ASSOC_InterestSpecification"
	  on "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification.ComponentNumber" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "_Trade.IDSystem" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._Trade.IDSystem" AND 
	     "_Trade.TradeID" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._Trade.TradeID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "SequenceNumber" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.SequenceNumber" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_CashFlowStream_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID"
	     ,
	join "sap.fsdm.SQLViews::PositionCurrencyOfMultiCurrencyContract_View" as "ASSOC_CcyOfMultiCcyAccnt"
	  on "ASSOC_CcyOfMultiCcyAccnt"."ASSOC_MultiCcyAccnt.IDSystem" = "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt"."ASSOC_MultiCcyAccnt.FinancialContractID" = "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt"."PositionCurrency" = "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "ASSOC_FinancialContract"
	  on "ASSOC_FinancialContract"."FinancialContractID" = "ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_FinancialContract"."IDSystem" = "ASSOC_FinancialContract.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::ReferenceRate_View" as "ASSOC_ReferenceRateID"
	  on "ASSOC_ReferenceRateID"."ReferenceRateID" = "ASSOC_ReferenceRateID.ReferenceRateID"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_DebtInstrument"
	  on "_DebtInstrument"."FinancialInstrumentID" = "_DebtInstrument.FinancialInstrumentID"
	     ,
	join "sap.fsdm.SQLViews::InterestRateOptionComponent_View" as "_OptionOfReferenceRateSpecification"
	  on "_OptionOfReferenceRateSpecification"."_InterestRateOption.IDSystem" = "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification"."_InterestRateOption.FinancialContractID" = "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "_OptionOfReferenceRateSpecification"."ComponentNumber" = "_OptionOfReferenceRateSpecification.ComponentNumber"
	     ,
	join "sap.fsdm.SQLViews::InterestRateOptionComponent_View" as "_OptionOfStrikeSpecification"
	  on "_OptionOfStrikeSpecification"."ComponentNumber" = "_OptionOfStrikeSpecification.ComponentNumber" AND 
	     "_OptionOfStrikeSpecification"."_InterestRateOption.FinancialContractID" = "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "_OptionOfStrikeSpecification"."_InterestRateOption.IDSystem" = "_OptionOfStrikeSpecification._InterestRateOption.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Trade_View" as "_Trade"
	  on "_Trade"."TradeID" = "_Trade.TradeID" AND 
	     "_Trade"."IDSystem" = "_Trade.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::InterestPaymentListEntry_View" as "_InterestPaymentListEntry_ASSOC_InterestPaymentList"
	  on "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList.ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList.ASSOC_FinancialContract.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification.ComponentNumber" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._OptionOfReferenceRateSpecification.ComponentNumber" AND 
	     "SequenceNumber" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList.SequenceNumber" AND 
	     "_Trade.TradeID" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._Trade.TradeID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._DebtInstrument.FinancialInstrumentID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "_Trade.IDSystem" = "_InterestPaymentListEntry_ASSOC_InterestPaymentList"."ASSOC_InterestPaymentList._Trade.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Locking_View" as "_Locking__LockingOfInterest"
	  on "_Trade.TradeID" = "_Locking__LockingOfInterest"."_LockingOfInterest._Trade.TradeID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_Locking__LockingOfInterest"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_Locking__LockingOfInterest"."_LockingOfInterest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_Locking__LockingOfInterest"."_LockingOfInterest.ASSOC_FinancialContract.FinancialContractID" AND 
	     "_OptionOfReferenceRateSpecification.ComponentNumber" = "_Locking__LockingOfInterest"."_LockingOfInterest._OptionOfReferenceRateSpecification.ComponentNumber" AND 
	     "_Trade.IDSystem" = "_Locking__LockingOfInterest"."_LockingOfInterest._Trade.IDSystem" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_Locking__LockingOfInterest"."_LockingOfInterest.ASSOC_FinancialContract.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_Locking__LockingOfInterest"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_Locking__LockingOfInterest"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_Locking__LockingOfInterest"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_Locking__LockingOfInterest"."_LockingOfInterest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_Locking__LockingOfInterest"."_LockingOfInterest._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_Locking__LockingOfInterest"."_LockingOfInterest._DebtInstrument.FinancialInstrumentID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_Locking__LockingOfInterest"."_LockingOfInterest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "SequenceNumber" = "_Locking__LockingOfInterest"."_LockingOfInterest.SequenceNumber"
	     ,
	join "sap.fsdm.SQLViews::ScaleInterval_View" as "_ScaleInterval_ASSOC_InterestSpecification"
	  on "_OptionOfReferenceRateSpecification.ComponentNumber" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "_Trade.TradeID" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._Trade.TradeID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "_Trade.IDSystem" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._Trade.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "SequenceNumber" = "_ScaleInterval_ASSOC_InterestSpecification"."ASSOC_InterestSpecification.SequenceNumber"
	     ,
	join "sap.fsdm.SQLViews::SetOfPreconditions_View" as "_SetOfPreconditions_ASSOC_Interest"
	  on "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest.ASSOC_FinancialContract.IDSystem" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest.ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "_Trade.TradeID" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._Trade.TradeID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "SequenceNumber" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest.SequenceNumber" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._DebtInstrument.FinancialInstrumentID" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "_Trade.IDSystem" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._Trade.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification.ComponentNumber" = "_SetOfPreconditions_ASSOC_Interest"."ASSOC_Interest._OptionOfReferenceRateSpecification.ComponentNumber"
	     ,
	join "sap.fsdm.SQLViews::WaterfallAssignment_View" as "_WaterfallAssignment__Interest"
	  on "_OptionOfReferenceRateSpecification.ComponentNumber" = "_WaterfallAssignment__Interest"."_Interest._OptionOfReferenceRateSpecification.ComponentNumber" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "_WaterfallAssignment__Interest"."_Interest.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "_WaterfallAssignment__Interest"."_Interest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" AND 
	     "SequenceNumber" = "_WaterfallAssignment__Interest"."_Interest.SequenceNumber" AND 
	     "_Trade.TradeID" = "_WaterfallAssignment__Interest"."_Interest._Trade.TradeID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "_WaterfallAssignment__Interest"."_Interest._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_WaterfallAssignment__Interest"."_Interest.ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_WaterfallAssignment__Interest"."_Interest.ASSOC_FinancialContract.IDSystem" AND 
	     "_Trade.IDSystem" = "_WaterfallAssignment__Interest"."_Interest._Trade.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "_WaterfallAssignment__Interest"."_Interest._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "_WaterfallAssignment__Interest"."_Interest._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" AND 
	     "ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "_WaterfallAssignment__Interest"."_Interest.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" AND 
	     "_OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "_WaterfallAssignment__Interest"."_Interest._OptionOfStrikeSpecification._InterestRateOption.IDSystem" AND 
	     "_OptionOfStrikeSpecification.ComponentNumber" = "_WaterfallAssignment__Interest"."_Interest._OptionOfStrikeSpecification.ComponentNumber" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_WaterfallAssignment__Interest"."_Interest._DebtInstrument.FinancialInstrumentID"
	     
);