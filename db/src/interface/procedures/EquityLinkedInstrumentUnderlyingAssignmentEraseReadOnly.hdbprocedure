PROCEDURE "sap.fsdm.procedures::EquityLinkedInstrumentUnderlyingAssignmentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::EquityLinkedInstrumentUnderlyingAssignmentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::EquityLinkedInstrumentUnderlyingAssignmentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::EquityLinkedInstrumentUnderlyingAssignmentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_EquityLinkedInstrument.FinancialInstrumentID" is null and
            "_UnderlyingIndex.IndexID" is null and
            "_UnderlyingInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_EquityLinkedInstrument.FinancialInstrumentID" ,
                "_UnderlyingIndex.IndexID" ,
                "_UnderlyingInstrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" ,
                "OLD"."_UnderlyingIndex.IndexID" ,
                "OLD"."_UnderlyingInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EquityLinkedInstrumentUnderlyingAssignment" "OLD"
            on
                "IN"."_EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" and
                "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" and
                "IN"."_UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_UnderlyingInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_EquityLinkedInstrument.FinancialInstrumentID" ,
            "_UnderlyingIndex.IndexID" ,
            "_UnderlyingInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" ,
                "OLD"."_UnderlyingIndex.IndexID" ,
                "OLD"."_UnderlyingInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EquityLinkedInstrumentUnderlyingAssignment_Historical" "OLD"
            on
                "IN"."_EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrument.FinancialInstrumentID" and
                "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" and
                "IN"."_UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_UnderlyingInstrument.FinancialInstrumentID" 
        );

END
