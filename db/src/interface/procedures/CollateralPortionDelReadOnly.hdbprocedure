PROCEDURE "sap.fsdm.procedures::CollateralPortionDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CollateralPortionTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CollateralPortionTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CollateralPortionTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PortionNumber=' || TO_VARCHAR("PortionNumber") || ' ' ||
                'ASSOC_CollateralAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_CollateralAgreement.FinancialContractID") || ' ' ||
                'ASSOC_CollateralAgreement.IDSystem=' || TO_VARCHAR("ASSOC_CollateralAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PortionNumber",
                        "IN"."ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_CollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PortionNumber",
                        "IN"."ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_CollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PortionNumber",
                        "ASSOC_CollateralAgreement.FinancialContractID",
                        "ASSOC_CollateralAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PortionNumber",
                                    "IN"."ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_CollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PortionNumber",
                                    "IN"."ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_CollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PortionNumber" is null and
            "ASSOC_CollateralAgreement.FinancialContractID" is null and
            "ASSOC_CollateralAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "PortionNumber",
            "ASSOC_CollateralAgreement.FinancialContractID",
            "ASSOC_CollateralAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CollateralPortion" WHERE
            (
            "PortionNumber" ,
            "ASSOC_CollateralAgreement.FinancialContractID" ,
            "ASSOC_CollateralAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."PortionNumber",
            "OLD"."ASSOC_CollateralAgreement.FinancialContractID",
            "OLD"."ASSOC_CollateralAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CollateralPortion" as "OLD"
        on
                              "IN"."PortionNumber" = "OLD"."PortionNumber" and
                              "IN"."ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralAgreement.FinancialContractID" and
                              "IN"."ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "PortionNumber",
        "ASSOC_CollateralAgreement.FinancialContractID",
        "ASSOC_CollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID",
        "_Creditor.BusinessPartnerID",
        "AgreedCoverageEndDate",
        "AgreedCoverageStartDate",
        "CollateralPortionConsideredInBusinessPartnerRating",
        "CollateralPortionCoverageCategory",
        "CoveredPercentageOfObligations",
        "MaximumCollateralPortionAmount",
        "MaximumCollateralPortionAmountCurrency",
        "PercentageOfTotalCollateralForPortion",
        "PortionName",
        "RankAgainstOtherPortions",
        "RoleOfCreditor",
        "ValueDatedCollateralPortionAmount",
        "ValueDatedCollateralPortionAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_PortionNumber" as "PortionNumber" ,
            "OLD_ASSOC_CollateralAgreement.FinancialContractID" as "ASSOC_CollateralAgreement.FinancialContractID" ,
            "OLD_ASSOC_CollateralAgreement.IDSystem" as "ASSOC_CollateralAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" as "ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" ,
            "OLD__Creditor.BusinessPartnerID" as "_Creditor.BusinessPartnerID" ,
            "OLD_AgreedCoverageEndDate" as "AgreedCoverageEndDate" ,
            "OLD_AgreedCoverageStartDate" as "AgreedCoverageStartDate" ,
            "OLD_CollateralPortionConsideredInBusinessPartnerRating" as "CollateralPortionConsideredInBusinessPartnerRating" ,
            "OLD_CollateralPortionCoverageCategory" as "CollateralPortionCoverageCategory" ,
            "OLD_CoveredPercentageOfObligations" as "CoveredPercentageOfObligations" ,
            "OLD_MaximumCollateralPortionAmount" as "MaximumCollateralPortionAmount" ,
            "OLD_MaximumCollateralPortionAmountCurrency" as "MaximumCollateralPortionAmountCurrency" ,
            "OLD_PercentageOfTotalCollateralForPortion" as "PercentageOfTotalCollateralForPortion" ,
            "OLD_PortionName" as "PortionName" ,
            "OLD_RankAgainstOtherPortions" as "RankAgainstOtherPortions" ,
            "OLD_RoleOfCreditor" as "RoleOfCreditor" ,
            "OLD_ValueDatedCollateralPortionAmount" as "ValueDatedCollateralPortionAmount" ,
            "OLD_ValueDatedCollateralPortionAmountCurrency" as "ValueDatedCollateralPortionAmountCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PortionNumber",
                        "OLD"."ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."PortionNumber" AS "OLD_PortionNumber" ,
                "OLD"."ASSOC_CollateralAgreement.FinancialContractID" AS "OLD_ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralAgreement.IDSystem" AS "OLD_ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" AS "OLD_ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" ,
                "OLD"."_Creditor.BusinessPartnerID" AS "OLD__Creditor.BusinessPartnerID" ,
                "OLD"."AgreedCoverageEndDate" AS "OLD_AgreedCoverageEndDate" ,
                "OLD"."AgreedCoverageStartDate" AS "OLD_AgreedCoverageStartDate" ,
                "OLD"."CollateralPortionConsideredInBusinessPartnerRating" AS "OLD_CollateralPortionConsideredInBusinessPartnerRating" ,
                "OLD"."CollateralPortionCoverageCategory" AS "OLD_CollateralPortionCoverageCategory" ,
                "OLD"."CoveredPercentageOfObligations" AS "OLD_CoveredPercentageOfObligations" ,
                "OLD"."MaximumCollateralPortionAmount" AS "OLD_MaximumCollateralPortionAmount" ,
                "OLD"."MaximumCollateralPortionAmountCurrency" AS "OLD_MaximumCollateralPortionAmountCurrency" ,
                "OLD"."PercentageOfTotalCollateralForPortion" AS "OLD_PercentageOfTotalCollateralForPortion" ,
                "OLD"."PortionName" AS "OLD_PortionName" ,
                "OLD"."RankAgainstOtherPortions" AS "OLD_RankAgainstOtherPortions" ,
                "OLD"."RoleOfCreditor" AS "OLD_RoleOfCreditor" ,
                "OLD"."ValueDatedCollateralPortionAmount" AS "OLD_ValueDatedCollateralPortionAmount" ,
                "OLD"."ValueDatedCollateralPortionAmountCurrency" AS "OLD_ValueDatedCollateralPortionAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPortion" as "OLD"
            on
                                      "IN"."PortionNumber" = "OLD"."PortionNumber" and
                                      "IN"."ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralAgreement.FinancialContractID" and
                                      "IN"."ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_PortionNumber" as "PortionNumber",
            "OLD_ASSOC_CollateralAgreement.FinancialContractID" as "ASSOC_CollateralAgreement.FinancialContractID",
            "OLD_ASSOC_CollateralAgreement.IDSystem" as "ASSOC_CollateralAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" as "ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID",
            "OLD__Creditor.BusinessPartnerID" as "_Creditor.BusinessPartnerID",
            "OLD_AgreedCoverageEndDate" as "AgreedCoverageEndDate",
            "OLD_AgreedCoverageStartDate" as "AgreedCoverageStartDate",
            "OLD_CollateralPortionConsideredInBusinessPartnerRating" as "CollateralPortionConsideredInBusinessPartnerRating",
            "OLD_CollateralPortionCoverageCategory" as "CollateralPortionCoverageCategory",
            "OLD_CoveredPercentageOfObligations" as "CoveredPercentageOfObligations",
            "OLD_MaximumCollateralPortionAmount" as "MaximumCollateralPortionAmount",
            "OLD_MaximumCollateralPortionAmountCurrency" as "MaximumCollateralPortionAmountCurrency",
            "OLD_PercentageOfTotalCollateralForPortion" as "PercentageOfTotalCollateralForPortion",
            "OLD_PortionName" as "PortionName",
            "OLD_RankAgainstOtherPortions" as "RankAgainstOtherPortions",
            "OLD_RoleOfCreditor" as "RoleOfCreditor",
            "OLD_ValueDatedCollateralPortionAmount" as "ValueDatedCollateralPortionAmount",
            "OLD_ValueDatedCollateralPortionAmountCurrency" as "ValueDatedCollateralPortionAmountCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PortionNumber",
                        "OLD"."ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PortionNumber" AS "OLD_PortionNumber" ,
                "OLD"."ASSOC_CollateralAgreement.FinancialContractID" AS "OLD_ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralAgreement.IDSystem" AS "OLD_ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" AS "OLD_ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" ,
                "OLD"."_Creditor.BusinessPartnerID" AS "OLD__Creditor.BusinessPartnerID" ,
                "OLD"."AgreedCoverageEndDate" AS "OLD_AgreedCoverageEndDate" ,
                "OLD"."AgreedCoverageStartDate" AS "OLD_AgreedCoverageStartDate" ,
                "OLD"."CollateralPortionConsideredInBusinessPartnerRating" AS "OLD_CollateralPortionConsideredInBusinessPartnerRating" ,
                "OLD"."CollateralPortionCoverageCategory" AS "OLD_CollateralPortionCoverageCategory" ,
                "OLD"."CoveredPercentageOfObligations" AS "OLD_CoveredPercentageOfObligations" ,
                "OLD"."MaximumCollateralPortionAmount" AS "OLD_MaximumCollateralPortionAmount" ,
                "OLD"."MaximumCollateralPortionAmountCurrency" AS "OLD_MaximumCollateralPortionAmountCurrency" ,
                "OLD"."PercentageOfTotalCollateralForPortion" AS "OLD_PercentageOfTotalCollateralForPortion" ,
                "OLD"."PortionName" AS "OLD_PortionName" ,
                "OLD"."RankAgainstOtherPortions" AS "OLD_RankAgainstOtherPortions" ,
                "OLD"."RoleOfCreditor" AS "OLD_RoleOfCreditor" ,
                "OLD"."ValueDatedCollateralPortionAmount" AS "OLD_ValueDatedCollateralPortionAmount" ,
                "OLD"."ValueDatedCollateralPortionAmountCurrency" AS "OLD_ValueDatedCollateralPortionAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPortion" as "OLD"
            on
               "IN"."PortionNumber" = "OLD"."PortionNumber" and
               "IN"."ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralAgreement.FinancialContractID" and
               "IN"."ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
