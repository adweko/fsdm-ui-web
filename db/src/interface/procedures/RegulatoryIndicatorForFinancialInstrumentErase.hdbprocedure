PROCEDURE "sap.fsdm.procedures::RegulatoryIndicatorForFinancialInstrumentErase" (IN ROW "sap.fsdm.tabletypes::RegulatoryIndicatorForFinancialInstrumentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Criterion" is null and
            "Regulation" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::RegulatoryIndicatorForFinancialInstrument"
        WHERE
        (            "Criterion" ,
            "Regulation" ,
            "_FinancialInstrument.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."Criterion" ,
                "OLD"."Regulation" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForFinancialInstrument" "OLD"
            on
            "IN"."Criterion" = "OLD"."Criterion" and
            "IN"."Regulation" = "OLD"."Regulation" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::RegulatoryIndicatorForFinancialInstrument_Historical"
        WHERE
        (
            "Criterion" ,
            "Regulation" ,
            "_FinancialInstrument.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."Criterion" ,
                "OLD"."Regulation" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::RegulatoryIndicatorForFinancialInstrument_Historical" "OLD"
            on
                "IN"."Criterion" = "OLD"."Criterion" and
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
