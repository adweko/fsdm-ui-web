PROCEDURE "sap.fsdm.procedures::FloatingHolidayEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FloatingHolidayTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FloatingHolidayTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FloatingHolidayTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "HolidayDate" is null and
            "_BusinessCalendar.BusinessCalendar" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "HolidayDate" ,
                "_BusinessCalendar.BusinessCalendar" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."HolidayDate" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FloatingHoliday" "OLD"
            on
                "IN"."HolidayDate" = "OLD"."HolidayDate" and
                "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "HolidayDate" ,
            "_BusinessCalendar.BusinessCalendar" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."HolidayDate" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FloatingHoliday_Historical" "OLD"
            on
                "IN"."HolidayDate" = "OLD"."HolidayDate" and
                "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        );

END
