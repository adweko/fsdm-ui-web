PROCEDURE "sap.fsdm.procedures::ContractClassCoveredByMasterAgreementDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ContractClassCoveredByMasterAgreementTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ContractClassCoveredByMasterAgreementTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ContractClassCoveredByMasterAgreementTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_MasterAgreement.FinancialContractID=' || TO_VARCHAR("_MasterAgreement.FinancialContractID") || ' ' ||
                '_MasterAgreement.IDSystem=' || TO_VARCHAR("_MasterAgreement.IDSystem") || ' ' ||
                '_ProductClass.ProductClass=' || TO_VARCHAR("_ProductClass.ProductClass") || ' ' ||
                '_ProductClass.ProductClassificationSystem=' || TO_VARCHAR("_ProductClass.ProductClassificationSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_ProductClass.ProductClass",
                        "IN"."_ProductClass.ProductClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_ProductClass.ProductClass",
                        "IN"."_ProductClass.ProductClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_MasterAgreement.FinancialContractID",
                        "_MasterAgreement.IDSystem",
                        "_ProductClass.ProductClass",
                        "_ProductClass.ProductClassificationSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_MasterAgreement.FinancialContractID",
                                    "IN"."_MasterAgreement.IDSystem",
                                    "IN"."_ProductClass.ProductClass",
                                    "IN"."_ProductClass.ProductClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_MasterAgreement.FinancialContractID",
                                    "IN"."_MasterAgreement.IDSystem",
                                    "IN"."_ProductClass.ProductClass",
                                    "IN"."_ProductClass.ProductClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_MasterAgreement.FinancialContractID" is null and
            "_MasterAgreement.IDSystem" is null and
            "_ProductClass.ProductClass" is null and
            "_ProductClass.ProductClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_MasterAgreement.FinancialContractID",
            "_MasterAgreement.IDSystem",
            "_ProductClass.ProductClass",
            "_ProductClass.ProductClassificationSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::ContractClassCoveredByMasterAgreement" WHERE
            (
            "_MasterAgreement.FinancialContractID" ,
            "_MasterAgreement.IDSystem" ,
            "_ProductClass.ProductClass" ,
            "_ProductClass.ProductClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_MasterAgreement.FinancialContractID",
            "OLD"."_MasterAgreement.IDSystem",
            "OLD"."_ProductClass.ProductClass",
            "OLD"."_ProductClass.ProductClassificationSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ContractClassCoveredByMasterAgreement" as "OLD"
        on
                              "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                              "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                              "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
                              "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_MasterAgreement.FinancialContractID",
        "_MasterAgreement.IDSystem",
        "_ProductClass.ProductClass",
        "_ProductClass.ProductClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__MasterAgreement.FinancialContractID" as "_MasterAgreement.FinancialContractID" ,
            "OLD__MasterAgreement.IDSystem" as "_MasterAgreement.IDSystem" ,
            "OLD__ProductClass.ProductClass" as "_ProductClass.ProductClass" ,
            "OLD__ProductClass.ProductClassificationSystem" as "_ProductClass.ProductClassificationSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_MasterAgreement.FinancialContractID",
                        "OLD"."_MasterAgreement.IDSystem",
                        "OLD"."_ProductClass.ProductClass",
                        "OLD"."_ProductClass.ProductClassificationSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_MasterAgreement.FinancialContractID" AS "OLD__MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" AS "OLD__MasterAgreement.IDSystem" ,
                "OLD"."_ProductClass.ProductClass" AS "OLD__ProductClass.ProductClass" ,
                "OLD"."_ProductClass.ProductClassificationSystem" AS "OLD__ProductClass.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ContractClassCoveredByMasterAgreement" as "OLD"
            on
                                      "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                                      "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                                      "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
                                      "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__MasterAgreement.FinancialContractID" as "_MasterAgreement.FinancialContractID",
            "OLD__MasterAgreement.IDSystem" as "_MasterAgreement.IDSystem",
            "OLD__ProductClass.ProductClass" as "_ProductClass.ProductClass",
            "OLD__ProductClass.ProductClassificationSystem" as "_ProductClass.ProductClassificationSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_MasterAgreement.FinancialContractID",
                        "OLD"."_MasterAgreement.IDSystem",
                        "OLD"."_ProductClass.ProductClass",
                        "OLD"."_ProductClass.ProductClassificationSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_MasterAgreement.FinancialContractID" AS "OLD__MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" AS "OLD__MasterAgreement.IDSystem" ,
                "OLD"."_ProductClass.ProductClass" AS "OLD__ProductClass.ProductClass" ,
                "OLD"."_ProductClass.ProductClassificationSystem" AS "OLD__ProductClass.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ContractClassCoveredByMasterAgreement" as "OLD"
            on
               "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
               "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
               "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
               "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
