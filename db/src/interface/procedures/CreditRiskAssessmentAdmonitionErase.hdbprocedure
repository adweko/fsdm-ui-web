PROCEDURE "sap.fsdm.procedures::CreditRiskAssessmentAdmonitionErase" (IN ROW "sap.fsdm.tabletypes::CreditRiskAssessmentAdmonitionTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AdmonitionDate" is null and
            "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" is null and
            "_DataRequestInformation.CreditRiskAssessmentDate" is null and
            "_DataRequestInformation._BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CreditRiskAssessmentAdmonition"
        WHERE
        (            "AdmonitionDate" ,
            "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" ,
            "_DataRequestInformation.CreditRiskAssessmentDate" ,
            "_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."AdmonitionDate" ,
                "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" ,
                "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" ,
                "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskAssessmentAdmonition" "OLD"
            on
            "IN"."AdmonitionDate" = "OLD"."AdmonitionDate" and
            "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" = "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" and
            "IN"."_DataRequestInformation.CreditRiskAssessmentDate" = "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" and
            "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" = "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
        );

        --delete data from history table
        delete from "sap.fsdm::CreditRiskAssessmentAdmonition_Historical"
        WHERE
        (
            "AdmonitionDate" ,
            "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" ,
            "_DataRequestInformation.CreditRiskAssessmentDate" ,
            "_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."AdmonitionDate" ,
                "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" ,
                "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" ,
                "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskAssessmentAdmonition_Historical" "OLD"
            on
                "IN"."AdmonitionDate" = "OLD"."AdmonitionDate" and
                "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" = "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" and
                "IN"."_DataRequestInformation.CreditRiskAssessmentDate" = "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" and
                "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" = "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
        );

END
