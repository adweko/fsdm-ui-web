PROCEDURE "sap.fsdm.procedures::ValueAtRiskBacktestingResultLoad" (IN ROW "sap.fsdm.tabletypes::ValueAtRiskBacktestingResultTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ProfitAndLossValueType=' || TO_VARCHAR("ProfitAndLossValueType") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_ValueAtRiskResult.RiskProvisionScenario=' || TO_VARCHAR("_ValueAtRiskResult.RiskProvisionScenario") || ' ' ||
                '_ValueAtRiskResult.RoleOfCurrency=' || TO_VARCHAR("_ValueAtRiskResult.RoleOfCurrency") || ' ' ||
                '_ValueAtRiskResult._ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ValueAtRiskResult._ResultGroup.ResultDataProvider") || ' ' ||
                '_ValueAtRiskResult._ResultGroup.ResultGroupID=' || TO_VARCHAR("_ValueAtRiskResult._ResultGroup.ResultGroupID") || ' ' ||
                '_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ProfitAndLossValueType",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                        "IN"."_ValueAtRiskResult.RoleOfCurrency",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ProfitAndLossValueType",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                        "IN"."_ValueAtRiskResult.RoleOfCurrency",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ProfitAndLossValueType",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_ValueAtRiskResult.RiskProvisionScenario",
                        "_ValueAtRiskResult.RoleOfCurrency",
                        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ProfitAndLossValueType",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                                    "IN"."_ValueAtRiskResult.RoleOfCurrency",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ProfitAndLossValueType",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                                    "IN"."_ValueAtRiskResult.RoleOfCurrency",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                                    "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                                    "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::ValueAtRiskBacktestingResult" (
        "ProfitAndLossValueType",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_ValueAtRiskResult.RiskProvisionScenario",
        "_ValueAtRiskResult.RoleOfCurrency",
        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ExceptionRate",
        "NumberOfComparisons",
        "NumberOfExceptions",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ProfitAndLossValueType" as "ProfitAndLossValueType" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__ValueAtRiskResult.RiskProvisionScenario" as "_ValueAtRiskResult.RiskProvisionScenario" ,
            "OLD__ValueAtRiskResult.RoleOfCurrency" as "_ValueAtRiskResult.RoleOfCurrency" ,
            "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider" as "_ValueAtRiskResult._ResultGroup.ResultDataProvider" ,
            "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID" as "_ValueAtRiskResult._ResultGroup.ResultGroupID" ,
            "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" as "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ExceptionRate" as "ExceptionRate" ,
            "OLD_NumberOfComparisons" as "NumberOfComparisons" ,
            "OLD_NumberOfExceptions" as "NumberOfExceptions" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ProfitAndLossValueType",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                        "IN"."_ValueAtRiskResult.RoleOfCurrency",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ProfitAndLossValueType" as "OLD_ProfitAndLossValueType",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_ValueAtRiskResult.RiskProvisionScenario" as "OLD__ValueAtRiskResult.RiskProvisionScenario",
                                "OLD"."_ValueAtRiskResult.RoleOfCurrency" as "OLD__ValueAtRiskResult.RoleOfCurrency",
                                "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" as "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider",
                                "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" as "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID",
                                "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" as "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ExceptionRate" as "OLD_ExceptionRate",
                                "OLD"."NumberOfComparisons" as "OLD_NumberOfComparisons",
                                "OLD"."NumberOfExceptions" as "OLD_NumberOfExceptions",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ValueAtRiskBacktestingResult" as "OLD"
            on
                ifnull( "IN"."ProfitAndLossValueType", '') = "OLD"."ProfitAndLossValueType" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_ValueAtRiskResult.RiskProvisionScenario", '') = "OLD"."_ValueAtRiskResult.RiskProvisionScenario" and
                ifnull( "IN"."_ValueAtRiskResult.RoleOfCurrency", '') = "OLD"."_ValueAtRiskResult.RoleOfCurrency" and
                ifnull( "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider", '') = "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID", '') = "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" and
                ifnull( "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID", '') = "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ValueAtRiskBacktestingResult" (
        "ProfitAndLossValueType",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_ValueAtRiskResult.RiskProvisionScenario",
        "_ValueAtRiskResult.RoleOfCurrency",
        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ExceptionRate",
        "NumberOfComparisons",
        "NumberOfExceptions",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ProfitAndLossValueType" as "ProfitAndLossValueType",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__ValueAtRiskResult.RiskProvisionScenario" as "_ValueAtRiskResult.RiskProvisionScenario",
            "OLD__ValueAtRiskResult.RoleOfCurrency" as "_ValueAtRiskResult.RoleOfCurrency",
            "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider" as "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
            "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID" as "_ValueAtRiskResult._ResultGroup.ResultGroupID",
            "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" as "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ExceptionRate" as "ExceptionRate",
            "OLD_NumberOfComparisons" as "NumberOfComparisons",
            "OLD_NumberOfExceptions" as "NumberOfExceptions",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ProfitAndLossValueType",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult.RiskProvisionScenario",
                        "IN"."_ValueAtRiskResult.RoleOfCurrency",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."ProfitAndLossValueType" as "OLD_ProfitAndLossValueType",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_ValueAtRiskResult.RiskProvisionScenario" as "OLD__ValueAtRiskResult.RiskProvisionScenario",
                        "OLD"."_ValueAtRiskResult.RoleOfCurrency" as "OLD__ValueAtRiskResult.RoleOfCurrency",
                        "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" as "OLD__ValueAtRiskResult._ResultGroup.ResultDataProvider",
                        "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" as "OLD__ValueAtRiskResult._ResultGroup.ResultGroupID",
                        "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" as "OLD__ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."ExceptionRate" as "OLD_ExceptionRate",
                        "OLD"."NumberOfComparisons" as "OLD_NumberOfComparisons",
                        "OLD"."NumberOfExceptions" as "OLD_NumberOfExceptions",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ValueAtRiskBacktestingResult" as "OLD"
            on
                ifnull( "IN"."ProfitAndLossValueType", '' ) = "OLD"."ProfitAndLossValueType" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_ValueAtRiskResult.RiskProvisionScenario", '' ) = "OLD"."_ValueAtRiskResult.RiskProvisionScenario" and
                ifnull( "IN"."_ValueAtRiskResult.RoleOfCurrency", '' ) = "OLD"."_ValueAtRiskResult.RoleOfCurrency" and
                ifnull( "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider", '' ) = "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID", '' ) = "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" and
                ifnull( "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID", '' ) = "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::ValueAtRiskBacktestingResult"
    where (
        "ProfitAndLossValueType",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_ValueAtRiskResult.RiskProvisionScenario",
        "_ValueAtRiskResult.RoleOfCurrency",
        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ProfitAndLossValueType",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_ValueAtRiskResult.RiskProvisionScenario",
            "OLD"."_ValueAtRiskResult.RoleOfCurrency",
            "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider",
            "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID",
            "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ValueAtRiskBacktestingResult" as "OLD"
        on
           ifnull( "IN"."ProfitAndLossValueType", '' ) = "OLD"."ProfitAndLossValueType" and
           ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
           ifnull( "IN"."_ValueAtRiskResult.RiskProvisionScenario", '' ) = "OLD"."_ValueAtRiskResult.RiskProvisionScenario" and
           ifnull( "IN"."_ValueAtRiskResult.RoleOfCurrency", '' ) = "OLD"."_ValueAtRiskResult.RoleOfCurrency" and
           ifnull( "IN"."_ValueAtRiskResult._ResultGroup.ResultDataProvider", '' ) = "OLD"."_ValueAtRiskResult._ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ValueAtRiskResult._ResultGroup.ResultGroupID", '' ) = "OLD"."_ValueAtRiskResult._ResultGroup.ResultGroupID" and
           ifnull( "IN"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID", '' ) = "OLD"."_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::ValueAtRiskBacktestingResult" (
        "ProfitAndLossValueType",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_ValueAtRiskResult.RiskProvisionScenario",
        "_ValueAtRiskResult.RoleOfCurrency",
        "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
        "_ValueAtRiskResult._ResultGroup.ResultGroupID",
        "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ExceptionRate",
        "NumberOfComparisons",
        "NumberOfExceptions",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "ProfitAndLossValueType", '' ) as "ProfitAndLossValueType",
            ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
            ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
            ifnull( "_ValueAtRiskResult.RiskProvisionScenario", '' ) as "_ValueAtRiskResult.RiskProvisionScenario",
            ifnull( "_ValueAtRiskResult.RoleOfCurrency", '' ) as "_ValueAtRiskResult.RoleOfCurrency",
            ifnull( "_ValueAtRiskResult._ResultGroup.ResultDataProvider", '' ) as "_ValueAtRiskResult._ResultGroup.ResultDataProvider",
            ifnull( "_ValueAtRiskResult._ResultGroup.ResultGroupID", '' ) as "_ValueAtRiskResult._ResultGroup.ResultGroupID",
            ifnull( "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID", '' ) as "_ValueAtRiskResult._RiskReportingNode.RiskReportingNodeID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "ExceptionRate"  ,
            "NumberOfComparisons"  ,
            "NumberOfExceptions"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END