namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"PositionCurrencyOfMultiCurrencyContract";
using "sap"."fsdm"::"BusinessPartner";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"MaturityBand";

entity "DaysPastDue" {
    key "PastDueType"                                   : String(40)                                                   default '';
    key "ASSOC_FinancialContract"                       : association to FinancialContract {
                                                                                             FinancialContractID,
                                                                                             IDSystem
                                                                                           }                           not null;
    key "ASSOC_PositionCurrencyOfMultiCurrencyContract" : association to PositionCurrencyOfMultiCurrencyContract {
                                                                                                                   PositionCurrency,
                                                                                                                   ASSOC_MultiCcyAccnt
                                                                                                                 }     not null;
    key "_BusinessPartner"                              : association to BusinessPartner { BusinessPartnerID }         not null;
    key "_FinancialInstrumentDaysPastDue"               : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_TimeBucket"                                   : association to MaturityBand {
                                                                                        MaturityBandID,
                                                                                        TimeBucketID
                                                                                      }                                not null;
    key "BusinessValidFrom"                             : LocalDate;
    key "BusinessValidTo"                               : LocalDate;
        "SystemValidFrom"                               : UTCTimestamp                                                 not null;
        "SystemValidTo"                                 : UTCTimestamp                                                 not null;
        "DaysPastDue"                                   : Integer;
        "DaysPastDueCategory"                           : String(30);
        "PastDueFrom"                                   : LocalDate;
        "PastDueSince"                                  : LocalDate;
        "PastDueTo"                                     : LocalDate;
        "SourceSystemID"                                : String(128);
        "ChangeTimestampInSourceSystem"                 : UTCTimestamp;
        "ChangingUserInSourceSystem"                    : String(128);
        "ChangingProcessType"                           : String(40);
        "ChangingProcessID"                             : String(128);
}
technical configuration {
    column store;
};

entity "DaysPastDue_Historical" {
    "PastDueType"                                   : String(40)                                                   default '' not null;
    "ASSOC_FinancialContract"                       : association to FinancialContract {
                                                                                         FinancialContractID,
                                                                                         IDSystem
                                                                                       }                           not null;
    "ASSOC_PositionCurrencyOfMultiCurrencyContract" : association to PositionCurrencyOfMultiCurrencyContract {
                                                                                                               PositionCurrency,
                                                                                                               ASSOC_MultiCcyAccnt
                                                                                                             }     not null;
    "_BusinessPartner"                              : association to BusinessPartner { BusinessPartnerID }         not null;
    "_FinancialInstrumentDaysPastDue"               : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_TimeBucket"                                   : association to MaturityBand {
                                                                                    MaturityBandID,
                                                                                    TimeBucketID
                                                                                  }                                not null;
    "BusinessValidFrom"                             : LocalDate                                                    not null;
    "BusinessValidTo"                               : LocalDate                                                    not null;
    "SystemValidFrom"                               : UTCTimestamp                                                 not null;
    "SystemValidTo"                                 : UTCTimestamp                                                 not null;
    "DaysPastDue"                                   : Integer;
    "DaysPastDueCategory"                           : String(30);
    "PastDueFrom"                                   : LocalDate;
    "PastDueSince"                                  : LocalDate;
    "PastDueTo"                                     : LocalDate;
    "SourceSystemID"                                : String(128);
    "ChangeTimestampInSourceSystem"                 : UTCTimestamp;
    "ChangingUserInSourceSystem"                    : String(128);
    "ChangingProcessType"                           : String(40);
    "ChangingProcessID"                             : String(128);
}
technical configuration {
    column store;
};