PROCEDURE "sap.fsdm.procedures::PlannedEarlyDisbursementDelReadOnly" (IN ROW "sap.fsdm.tabletypes::PlannedEarlyDisbursementTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::PlannedEarlyDisbursementTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::PlannedEarlyDisbursementTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DisbursementDifferentiationCriterium=' || TO_VARCHAR("DisbursementDifferentiationCriterium") || ' ' ||
                'PlannedDisbursementDate=' || TO_VARCHAR("PlannedDisbursementDate") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DisbursementDifferentiationCriterium",
                        "IN"."PlannedDisbursementDate",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DisbursementDifferentiationCriterium",
                        "IN"."PlannedDisbursementDate",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DisbursementDifferentiationCriterium",
                        "PlannedDisbursementDate",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DisbursementDifferentiationCriterium",
                                    "IN"."PlannedDisbursementDate",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DisbursementDifferentiationCriterium",
                                    "IN"."PlannedDisbursementDate",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "DisbursementDifferentiationCriterium" is null and
            "PlannedDisbursementDate" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "DisbursementDifferentiationCriterium",
            "PlannedDisbursementDate",
            "_FinancialContract.FinancialContractID",
            "_FinancialContract.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::PlannedEarlyDisbursement" WHERE
            (
            "DisbursementDifferentiationCriterium" ,
            "PlannedDisbursementDate" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."DisbursementDifferentiationCriterium",
            "OLD"."PlannedDisbursementDate",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PlannedEarlyDisbursement" as "OLD"
        on
                              "IN"."DisbursementDifferentiationCriterium" = "OLD"."DisbursementDifferentiationCriterium" and
                              "IN"."PlannedDisbursementDate" = "OLD"."PlannedDisbursementDate" and
                              "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                              "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "DisbursementDifferentiationCriterium",
        "PlannedDisbursementDate",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DisbursementProbability",
        "PlannedDisbursementAmount",
        "PlannedDisbursementAmountCurrency",
        "Status",
        "StatusChangeDate",
        "StatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_DisbursementDifferentiationCriterium" as "DisbursementDifferentiationCriterium" ,
            "OLD_PlannedDisbursementDate" as "PlannedDisbursementDate" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_DisbursementProbability" as "DisbursementProbability" ,
            "OLD_PlannedDisbursementAmount" as "PlannedDisbursementAmount" ,
            "OLD_PlannedDisbursementAmountCurrency" as "PlannedDisbursementAmountCurrency" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeDate" as "StatusChangeDate" ,
            "OLD_StatusReason" as "StatusReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."DisbursementDifferentiationCriterium",
                        "OLD"."PlannedDisbursementDate",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."DisbursementDifferentiationCriterium" AS "OLD_DisbursementDifferentiationCriterium" ,
                "OLD"."PlannedDisbursementDate" AS "OLD_PlannedDisbursementDate" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DisbursementProbability" AS "OLD_DisbursementProbability" ,
                "OLD"."PlannedDisbursementAmount" AS "OLD_PlannedDisbursementAmount" ,
                "OLD"."PlannedDisbursementAmountCurrency" AS "OLD_PlannedDisbursementAmountCurrency" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusReason" AS "OLD_StatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PlannedEarlyDisbursement" as "OLD"
            on
                                      "IN"."DisbursementDifferentiationCriterium" = "OLD"."DisbursementDifferentiationCriterium" and
                                      "IN"."PlannedDisbursementDate" = "OLD"."PlannedDisbursementDate" and
                                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_DisbursementDifferentiationCriterium" as "DisbursementDifferentiationCriterium",
            "OLD_PlannedDisbursementDate" as "PlannedDisbursementDate",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DisbursementProbability" as "DisbursementProbability",
            "OLD_PlannedDisbursementAmount" as "PlannedDisbursementAmount",
            "OLD_PlannedDisbursementAmountCurrency" as "PlannedDisbursementAmountCurrency",
            "OLD_Status" as "Status",
            "OLD_StatusChangeDate" as "StatusChangeDate",
            "OLD_StatusReason" as "StatusReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."DisbursementDifferentiationCriterium",
                        "OLD"."PlannedDisbursementDate",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."DisbursementDifferentiationCriterium" AS "OLD_DisbursementDifferentiationCriterium" ,
                "OLD"."PlannedDisbursementDate" AS "OLD_PlannedDisbursementDate" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DisbursementProbability" AS "OLD_DisbursementProbability" ,
                "OLD"."PlannedDisbursementAmount" AS "OLD_PlannedDisbursementAmount" ,
                "OLD"."PlannedDisbursementAmountCurrency" AS "OLD_PlannedDisbursementAmountCurrency" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusReason" AS "OLD_StatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PlannedEarlyDisbursement" as "OLD"
            on
               "IN"."DisbursementDifferentiationCriterium" = "OLD"."DisbursementDifferentiationCriterium" and
               "IN"."PlannedDisbursementDate" = "OLD"."PlannedDisbursementDate" and
               "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
               "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
