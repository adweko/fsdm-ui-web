PROCEDURE "sap.fsdm.procedures::StandardFeeDelete" (IN ROW "sap.fsdm.tabletypes::StandardFeeTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_StandardProduct.FinancialStandardProductID=' || TO_VARCHAR("ASSOC_StandardProduct.FinancialStandardProductID") || ' ' ||
                'ASSOC_StandardProduct.IDSystem=' || TO_VARCHAR("ASSOC_StandardProduct.IDSystem") || ' ' ||
                'ASSOC_StandardProduct.PricingScheme=' || TO_VARCHAR("ASSOC_StandardProduct.PricingScheme") || ' ' ||
                'ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID=' || TO_VARCHAR("ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_StandardProduct.IDSystem",
                        "IN"."ASSOC_StandardProduct.PricingScheme",
                        "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_StandardProduct.IDSystem",
                        "IN"."ASSOC_StandardProduct.PricingScheme",
                        "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_StandardProduct.FinancialStandardProductID",
                        "ASSOC_StandardProduct.IDSystem",
                        "ASSOC_StandardProduct.PricingScheme",
                        "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_StandardProduct.IDSystem",
                                    "IN"."ASSOC_StandardProduct.PricingScheme",
                                    "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_StandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_StandardProduct.IDSystem",
                                    "IN"."ASSOC_StandardProduct.PricingScheme",
                                    "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_StandardProduct.FinancialStandardProductID" is null and
            "ASSOC_StandardProduct.IDSystem" is null and
            "ASSOC_StandardProduct.PricingScheme" is null and
            "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::StandardFee" (
        "SequenceNumber",
        "ASSOC_StandardProduct.FinancialStandardProductID",
        "ASSOC_StandardProduct.IDSystem",
        "ASSOC_StandardProduct.PricingScheme",
        "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AssessmentBase",
        "BusinessCalendar",
        "BusinessDayConvention",
        "DayOfMonthOfFeeBillingDate",
        "EventDescription",
        "EventTriggerType",
        "FeeAmount",
        "FeeCalculationMethod",
        "FeeCurrency",
        "FeeSubtype",
        "FeeTruncatedForStubPeriods",
        "FeeType",
        "Percentage",
        "PeriodUntilFeeValidityEnd",
        "PeriodUntilFeeValidityStart",
        "PeriodUntilFirstFeeBillingDate",
        "PeriodUntilLastFeeBillingDate",
        "PreconditionApplies",
        "ReccurringFeePeriodTimeUnit",
        "RecurringFeePeriodLength",
        "RoleOfPayer",
        "ScaleApplies",
        "StandardFeeCategory",
        "TimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_StandardProduct.FinancialStandardProductID" as "ASSOC_StandardProduct.FinancialStandardProductID" ,
            "OLD_ASSOC_StandardProduct.IDSystem" as "ASSOC_StandardProduct.IDSystem" ,
            "OLD_ASSOC_StandardProduct.PricingScheme" as "ASSOC_StandardProduct.PricingScheme" ,
            "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AssessmentBase" as "AssessmentBase" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_DayOfMonthOfFeeBillingDate" as "DayOfMonthOfFeeBillingDate" ,
            "OLD_EventDescription" as "EventDescription" ,
            "OLD_EventTriggerType" as "EventTriggerType" ,
            "OLD_FeeAmount" as "FeeAmount" ,
            "OLD_FeeCalculationMethod" as "FeeCalculationMethod" ,
            "OLD_FeeCurrency" as "FeeCurrency" ,
            "OLD_FeeSubtype" as "FeeSubtype" ,
            "OLD_FeeTruncatedForStubPeriods" as "FeeTruncatedForStubPeriods" ,
            "OLD_FeeType" as "FeeType" ,
            "OLD_Percentage" as "Percentage" ,
            "OLD_PeriodUntilFeeValidityEnd" as "PeriodUntilFeeValidityEnd" ,
            "OLD_PeriodUntilFeeValidityStart" as "PeriodUntilFeeValidityStart" ,
            "OLD_PeriodUntilFirstFeeBillingDate" as "PeriodUntilFirstFeeBillingDate" ,
            "OLD_PeriodUntilLastFeeBillingDate" as "PeriodUntilLastFeeBillingDate" ,
            "OLD_PreconditionApplies" as "PreconditionApplies" ,
            "OLD_ReccurringFeePeriodTimeUnit" as "ReccurringFeePeriodTimeUnit" ,
            "OLD_RecurringFeePeriodLength" as "RecurringFeePeriodLength" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD_ScaleApplies" as "ScaleApplies" ,
            "OLD_StandardFeeCategory" as "StandardFeeCategory" ,
            "OLD_TimeUnit" as "TimeUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_StandardProduct.IDSystem",
                        "OLD"."ASSOC_StandardProduct.PricingScheme",
                        "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardProduct.IDSystem" AS "OLD_ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardProduct.PricingScheme" AS "OLD_ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AssessmentBase" AS "OLD_AssessmentBase" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."DayOfMonthOfFeeBillingDate" AS "OLD_DayOfMonthOfFeeBillingDate" ,
                "OLD"."EventDescription" AS "OLD_EventDescription" ,
                "OLD"."EventTriggerType" AS "OLD_EventTriggerType" ,
                "OLD"."FeeAmount" AS "OLD_FeeAmount" ,
                "OLD"."FeeCalculationMethod" AS "OLD_FeeCalculationMethod" ,
                "OLD"."FeeCurrency" AS "OLD_FeeCurrency" ,
                "OLD"."FeeSubtype" AS "OLD_FeeSubtype" ,
                "OLD"."FeeTruncatedForStubPeriods" AS "OLD_FeeTruncatedForStubPeriods" ,
                "OLD"."FeeType" AS "OLD_FeeType" ,
                "OLD"."Percentage" AS "OLD_Percentage" ,
                "OLD"."PeriodUntilFeeValidityEnd" AS "OLD_PeriodUntilFeeValidityEnd" ,
                "OLD"."PeriodUntilFeeValidityStart" AS "OLD_PeriodUntilFeeValidityStart" ,
                "OLD"."PeriodUntilFirstFeeBillingDate" AS "OLD_PeriodUntilFirstFeeBillingDate" ,
                "OLD"."PeriodUntilLastFeeBillingDate" AS "OLD_PeriodUntilLastFeeBillingDate" ,
                "OLD"."PreconditionApplies" AS "OLD_PreconditionApplies" ,
                "OLD"."ReccurringFeePeriodTimeUnit" AS "OLD_ReccurringFeePeriodTimeUnit" ,
                "OLD"."RecurringFeePeriodLength" AS "OLD_RecurringFeePeriodLength" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."ScaleApplies" AS "OLD_ScaleApplies" ,
                "OLD"."StandardFeeCategory" AS "OLD_StandardFeeCategory" ,
                "OLD"."TimeUnit" AS "OLD_TimeUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardFee" as "OLD"
            on
                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                      "IN"."ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" and
                      "IN"."ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardProduct.IDSystem" and
                      "IN"."ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardProduct.PricingScheme" and
                      "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::StandardFee" (
        "SequenceNumber",
        "ASSOC_StandardProduct.FinancialStandardProductID",
        "ASSOC_StandardProduct.IDSystem",
        "ASSOC_StandardProduct.PricingScheme",
        "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AssessmentBase",
        "BusinessCalendar",
        "BusinessDayConvention",
        "DayOfMonthOfFeeBillingDate",
        "EventDescription",
        "EventTriggerType",
        "FeeAmount",
        "FeeCalculationMethod",
        "FeeCurrency",
        "FeeSubtype",
        "FeeTruncatedForStubPeriods",
        "FeeType",
        "Percentage",
        "PeriodUntilFeeValidityEnd",
        "PeriodUntilFeeValidityStart",
        "PeriodUntilFirstFeeBillingDate",
        "PeriodUntilLastFeeBillingDate",
        "PreconditionApplies",
        "ReccurringFeePeriodTimeUnit",
        "RecurringFeePeriodLength",
        "RoleOfPayer",
        "ScaleApplies",
        "StandardFeeCategory",
        "TimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_StandardProduct.FinancialStandardProductID" as "ASSOC_StandardProduct.FinancialStandardProductID",
            "OLD_ASSOC_StandardProduct.IDSystem" as "ASSOC_StandardProduct.IDSystem",
            "OLD_ASSOC_StandardProduct.PricingScheme" as "ASSOC_StandardProduct.PricingScheme",
            "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AssessmentBase" as "AssessmentBase",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_DayOfMonthOfFeeBillingDate" as "DayOfMonthOfFeeBillingDate",
            "OLD_EventDescription" as "EventDescription",
            "OLD_EventTriggerType" as "EventTriggerType",
            "OLD_FeeAmount" as "FeeAmount",
            "OLD_FeeCalculationMethod" as "FeeCalculationMethod",
            "OLD_FeeCurrency" as "FeeCurrency",
            "OLD_FeeSubtype" as "FeeSubtype",
            "OLD_FeeTruncatedForStubPeriods" as "FeeTruncatedForStubPeriods",
            "OLD_FeeType" as "FeeType",
            "OLD_Percentage" as "Percentage",
            "OLD_PeriodUntilFeeValidityEnd" as "PeriodUntilFeeValidityEnd",
            "OLD_PeriodUntilFeeValidityStart" as "PeriodUntilFeeValidityStart",
            "OLD_PeriodUntilFirstFeeBillingDate" as "PeriodUntilFirstFeeBillingDate",
            "OLD_PeriodUntilLastFeeBillingDate" as "PeriodUntilLastFeeBillingDate",
            "OLD_PreconditionApplies" as "PreconditionApplies",
            "OLD_ReccurringFeePeriodTimeUnit" as "ReccurringFeePeriodTimeUnit",
            "OLD_RecurringFeePeriodLength" as "RecurringFeePeriodLength",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD_ScaleApplies" as "ScaleApplies",
            "OLD_StandardFeeCategory" as "StandardFeeCategory",
            "OLD_TimeUnit" as "TimeUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_StandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_StandardProduct.IDSystem",
                        "OLD"."ASSOC_StandardProduct.PricingScheme",
                        "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardProduct.IDSystem" AS "OLD_ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardProduct.PricingScheme" AS "OLD_ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AssessmentBase" AS "OLD_AssessmentBase" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."DayOfMonthOfFeeBillingDate" AS "OLD_DayOfMonthOfFeeBillingDate" ,
                "OLD"."EventDescription" AS "OLD_EventDescription" ,
                "OLD"."EventTriggerType" AS "OLD_EventTriggerType" ,
                "OLD"."FeeAmount" AS "OLD_FeeAmount" ,
                "OLD"."FeeCalculationMethod" AS "OLD_FeeCalculationMethod" ,
                "OLD"."FeeCurrency" AS "OLD_FeeCurrency" ,
                "OLD"."FeeSubtype" AS "OLD_FeeSubtype" ,
                "OLD"."FeeTruncatedForStubPeriods" AS "OLD_FeeTruncatedForStubPeriods" ,
                "OLD"."FeeType" AS "OLD_FeeType" ,
                "OLD"."Percentage" AS "OLD_Percentage" ,
                "OLD"."PeriodUntilFeeValidityEnd" AS "OLD_PeriodUntilFeeValidityEnd" ,
                "OLD"."PeriodUntilFeeValidityStart" AS "OLD_PeriodUntilFeeValidityStart" ,
                "OLD"."PeriodUntilFirstFeeBillingDate" AS "OLD_PeriodUntilFirstFeeBillingDate" ,
                "OLD"."PeriodUntilLastFeeBillingDate" AS "OLD_PeriodUntilLastFeeBillingDate" ,
                "OLD"."PreconditionApplies" AS "OLD_PreconditionApplies" ,
                "OLD"."ReccurringFeePeriodTimeUnit" AS "OLD_ReccurringFeePeriodTimeUnit" ,
                "OLD"."RecurringFeePeriodLength" AS "OLD_RecurringFeePeriodLength" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."ScaleApplies" AS "OLD_ScaleApplies" ,
                "OLD"."StandardFeeCategory" AS "OLD_StandardFeeCategory" ,
                "OLD"."TimeUnit" AS "OLD_TimeUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardFee" as "OLD"
            on
                                                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                                "IN"."ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" and
                                                "IN"."ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardProduct.IDSystem" and
                                                "IN"."ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardProduct.PricingScheme" and
                                                "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::StandardFee"
    where (
        "SequenceNumber",
        "ASSOC_StandardProduct.FinancialStandardProductID",
        "ASSOC_StandardProduct.IDSystem",
        "ASSOC_StandardProduct.PricingScheme",
        "ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_StandardProduct.FinancialStandardProductID",
            "OLD"."ASSOC_StandardProduct.IDSystem",
            "OLD"."ASSOC_StandardProduct.PricingScheme",
            "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::StandardFee" as "OLD"
        on
                                       "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                       "IN"."ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardProduct.FinancialStandardProductID" and
                                       "IN"."ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardProduct.IDSystem" and
                                       "IN"."ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardProduct.PricingScheme" and
                                       "IN"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
