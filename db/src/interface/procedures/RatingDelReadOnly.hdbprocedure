PROCEDURE "sap.fsdm.procedures::RatingDelReadOnly" (IN ROW "sap.fsdm.tabletypes::RatingTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::RatingTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::RatingTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IsFxRating=' || TO_VARCHAR("IsFxRating") || ' ' ||
                'RatingAgency=' || TO_VARCHAR("RatingAgency") || ' ' ||
                'RatingCategory=' || TO_VARCHAR("RatingCategory") || ' ' ||
                'RatingMethod=' || TO_VARCHAR("RatingMethod") || ' ' ||
                'RatingStatus=' || TO_VARCHAR("RatingStatus") || ' ' ||
                'TimeHorizon=' || TO_VARCHAR("TimeHorizon") || ' ' ||
                'ASSOC_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartner.BusinessPartnerID") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                'ASSOC_GeographicalRegion.GeographicalStructureID=' || TO_VARCHAR("ASSOC_GeographicalRegion.GeographicalStructureID") || ' ' ||
                'ASSOC_GeographicalRegion.GeographicalUnitID=' || TO_VARCHAR("ASSOC_GeographicalRegion.GeographicalUnitID") || ' ' ||
                '_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_Security.FinancialInstrumentID=' || TO_VARCHAR("_Security.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IsFxRating",
                        "IN"."RatingAgency",
                        "IN"."RatingCategory",
                        "IN"."RatingMethod",
                        "IN"."RatingStatus",
                        "IN"."TimeHorizon",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_GeographicalRegion.GeographicalStructureID",
                        "IN"."ASSOC_GeographicalRegion.GeographicalUnitID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_Security.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IsFxRating",
                        "IN"."RatingAgency",
                        "IN"."RatingCategory",
                        "IN"."RatingMethod",
                        "IN"."RatingStatus",
                        "IN"."TimeHorizon",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_GeographicalRegion.GeographicalStructureID",
                        "IN"."ASSOC_GeographicalRegion.GeographicalUnitID",
                        "IN"."_PhysicalAsset.PhysicalAssetID",
                        "IN"."_Security.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IsFxRating",
                        "RatingAgency",
                        "RatingCategory",
                        "RatingMethod",
                        "RatingStatus",
                        "TimeHorizon",
                        "ASSOC_BusinessPartner.BusinessPartnerID",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "ASSOC_GeographicalRegion.GeographicalStructureID",
                        "ASSOC_GeographicalRegion.GeographicalUnitID",
                        "_PhysicalAsset.PhysicalAssetID",
                        "_Security.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IsFxRating",
                                    "IN"."RatingAgency",
                                    "IN"."RatingCategory",
                                    "IN"."RatingMethod",
                                    "IN"."RatingStatus",
                                    "IN"."TimeHorizon",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_GeographicalRegion.GeographicalStructureID",
                                    "IN"."ASSOC_GeographicalRegion.GeographicalUnitID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_Security.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IsFxRating",
                                    "IN"."RatingAgency",
                                    "IN"."RatingCategory",
                                    "IN"."RatingMethod",
                                    "IN"."RatingStatus",
                                    "IN"."TimeHorizon",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_GeographicalRegion.GeographicalStructureID",
                                    "IN"."ASSOC_GeographicalRegion.GeographicalUnitID",
                                    "IN"."_PhysicalAsset.PhysicalAssetID",
                                    "IN"."_Security.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "IsFxRating" is null and
            "RatingAgency" is null and
            "RatingCategory" is null and
            "RatingMethod" is null and
            "RatingStatus" is null and
            "TimeHorizon" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_GeographicalRegion.GeographicalStructureID" is null and
            "ASSOC_GeographicalRegion.GeographicalUnitID" is null and
            "_PhysicalAsset.PhysicalAssetID" is null and
            "_Security.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "IsFxRating",
            "RatingAgency",
            "RatingCategory",
            "RatingMethod",
            "RatingStatus",
            "TimeHorizon",
            "ASSOC_BusinessPartner.BusinessPartnerID",
            "ASSOC_FinancialContract.FinancialContractID",
            "ASSOC_FinancialContract.IDSystem",
            "ASSOC_GeographicalRegion.GeographicalStructureID",
            "ASSOC_GeographicalRegion.GeographicalUnitID",
            "_PhysicalAsset.PhysicalAssetID",
            "_Security.FinancialInstrumentID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Rating" WHERE
            (
            "IsFxRating" ,
            "RatingAgency" ,
            "RatingCategory" ,
            "RatingMethod" ,
            "RatingStatus" ,
            "TimeHorizon" ,
            "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "_Security.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."IsFxRating",
            "OLD"."RatingAgency",
            "OLD"."RatingCategory",
            "OLD"."RatingMethod",
            "OLD"."RatingStatus",
            "OLD"."TimeHorizon",
            "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID",
            "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID",
            "OLD"."_PhysicalAsset.PhysicalAssetID",
            "OLD"."_Security.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Rating" as "OLD"
        on
                              "IN"."IsFxRating" = "OLD"."IsFxRating" and
                              "IN"."RatingAgency" = "OLD"."RatingAgency" and
                              "IN"."RatingCategory" = "OLD"."RatingCategory" and
                              "IN"."RatingMethod" = "OLD"."RatingMethod" and
                              "IN"."RatingStatus" = "OLD"."RatingStatus" and
                              "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                              "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                              "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                              "IN"."ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" and
                              "IN"."ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" and
                              "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                              "IN"."_Security.FinancialInstrumentID" = "OLD"."_Security.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "IsFxRating",
        "RatingAgency",
        "RatingCategory",
        "RatingMethod",
        "RatingStatus",
        "TimeHorizon",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_GeographicalRegion.GeographicalStructureID",
        "ASSOC_GeographicalRegion.GeographicalUnitID",
        "_PhysicalAsset.PhysicalAssetID",
        "_Security.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_EmployeeResponsibleForRating.BusinessPartnerID",
        "_EmployeeApprovingTheRating.BusinessPartnerID",
        "_OverridingRating.IsFxRating",
        "_OverridingRating.RatingAgency",
        "_OverridingRating.RatingCategory",
        "_OverridingRating.RatingMethod",
        "_OverridingRating.RatingStatus",
        "_OverridingRating.TimeHorizon",
        "_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID",
        "_OverridingRating.ASSOC_FinancialContract.FinancialContractID",
        "_OverridingRating.ASSOC_FinancialContract.IDSystem",
        "_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID",
        "_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID",
        "_OverridingRating._PhysicalAsset.PhysicalAssetID",
        "_OverridingRating._Security.FinancialInstrumentID",
        "AcquisitionDate",
        "ApprovalDate",
        "Commissioned",
        "CreationDate",
        "Endorsed",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "PublicationDate",
        "Rating",
        "RatingBasedOnMarginsOfConservatism",
        "RatingComment",
        "RatingDescription",
        "RatingMethodVersion",
        "RatingOutlook",
        "RatingRank",
        "RatingStatusChangeDate",
        "RatingStatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_IsFxRating" as "IsFxRating" ,
            "OLD_RatingAgency" as "RatingAgency" ,
            "OLD_RatingCategory" as "RatingCategory" ,
            "OLD_RatingMethod" as "RatingMethod" ,
            "OLD_RatingStatus" as "RatingStatus" ,
            "OLD_TimeHorizon" as "TimeHorizon" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD_ASSOC_GeographicalRegion.GeographicalStructureID" as "ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "OLD_ASSOC_GeographicalRegion.GeographicalUnitID" as "ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID" ,
            "OLD__Security.FinancialInstrumentID" as "_Security.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" as "ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" ,
            "OLD__EmployeeApprovingTheRating.BusinessPartnerID" as "_EmployeeApprovingTheRating.BusinessPartnerID" ,
            "OLD__OverridingRating.IsFxRating" as "_OverridingRating.IsFxRating" ,
            "OLD__OverridingRating.RatingAgency" as "_OverridingRating.RatingAgency" ,
            "OLD__OverridingRating.RatingCategory" as "_OverridingRating.RatingCategory" ,
            "OLD__OverridingRating.RatingMethod" as "_OverridingRating.RatingMethod" ,
            "OLD__OverridingRating.RatingStatus" as "_OverridingRating.RatingStatus" ,
            "OLD__OverridingRating.TimeHorizon" as "_OverridingRating.TimeHorizon" ,
            "OLD__OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" as "_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD__OverridingRating.ASSOC_FinancialContract.FinancialContractID" as "_OverridingRating.ASSOC_FinancialContract.FinancialContractID" ,
            "OLD__OverridingRating.ASSOC_FinancialContract.IDSystem" as "_OverridingRating.ASSOC_FinancialContract.IDSystem" ,
            "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" as "_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" as "_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "OLD__OverridingRating._PhysicalAsset.PhysicalAssetID" as "_OverridingRating._PhysicalAsset.PhysicalAssetID" ,
            "OLD__OverridingRating._Security.FinancialInstrumentID" as "_OverridingRating._Security.FinancialInstrumentID" ,
            "OLD_AcquisitionDate" as "AcquisitionDate" ,
            "OLD_ApprovalDate" as "ApprovalDate" ,
            "OLD_Commissioned" as "Commissioned" ,
            "OLD_CreationDate" as "CreationDate" ,
            "OLD_Endorsed" as "Endorsed" ,
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA" ,
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB" ,
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC" ,
            "OLD_PublicationDate" as "PublicationDate" ,
            "OLD_Rating" as "Rating" ,
            "OLD_RatingBasedOnMarginsOfConservatism" as "RatingBasedOnMarginsOfConservatism" ,
            "OLD_RatingComment" as "RatingComment" ,
            "OLD_RatingDescription" as "RatingDescription" ,
            "OLD_RatingMethodVersion" as "RatingMethodVersion" ,
            "OLD_RatingOutlook" as "RatingOutlook" ,
            "OLD_RatingRank" as "RatingRank" ,
            "OLD_RatingStatusChangeDate" as "RatingStatusChangeDate" ,
            "OLD_RatingStatusReason" as "RatingStatusReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."IsFxRating",
                        "OLD"."RatingAgency",
                        "OLD"."RatingCategory",
                        "OLD"."RatingMethod",
                        "OLD"."RatingStatus",
                        "OLD"."TimeHorizon",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID",
                        "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID",
                        "OLD"."_PhysicalAsset.PhysicalAssetID",
                        "OLD"."_Security.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."IsFxRating" AS "OLD_IsFxRating" ,
                "OLD"."RatingAgency" AS "OLD_RatingAgency" ,
                "OLD"."RatingCategory" AS "OLD_RatingCategory" ,
                "OLD"."RatingMethod" AS "OLD_RatingMethod" ,
                "OLD"."RatingStatus" AS "OLD_RatingStatus" ,
                "OLD"."TimeHorizon" AS "OLD_TimeHorizon" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" AS "OLD_ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" AS "OLD_ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" AS "OLD__PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Security.FinancialInstrumentID" AS "OLD__Security.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" AS "OLD_ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" ,
                "OLD"."_EmployeeApprovingTheRating.BusinessPartnerID" AS "OLD__EmployeeApprovingTheRating.BusinessPartnerID" ,
                "OLD"."_OverridingRating.IsFxRating" AS "OLD__OverridingRating.IsFxRating" ,
                "OLD"."_OverridingRating.RatingAgency" AS "OLD__OverridingRating.RatingAgency" ,
                "OLD"."_OverridingRating.RatingCategory" AS "OLD__OverridingRating.RatingCategory" ,
                "OLD"."_OverridingRating.RatingMethod" AS "OLD__OverridingRating.RatingMethod" ,
                "OLD"."_OverridingRating.RatingStatus" AS "OLD__OverridingRating.RatingStatus" ,
                "OLD"."_OverridingRating.TimeHorizon" AS "OLD__OverridingRating.TimeHorizon" ,
                "OLD"."_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD__OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_OverridingRating.ASSOC_FinancialContract.FinancialContractID" AS "OLD__OverridingRating.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_OverridingRating.ASSOC_FinancialContract.IDSystem" AS "OLD__OverridingRating.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" AS "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" AS "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_OverridingRating._PhysicalAsset.PhysicalAssetID" AS "OLD__OverridingRating._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_OverridingRating._Security.FinancialInstrumentID" AS "OLD__OverridingRating._Security.FinancialInstrumentID" ,
                "OLD"."AcquisitionDate" AS "OLD_AcquisitionDate" ,
                "OLD"."ApprovalDate" AS "OLD_ApprovalDate" ,
                "OLD"."Commissioned" AS "OLD_Commissioned" ,
                "OLD"."CreationDate" AS "OLD_CreationDate" ,
                "OLD"."Endorsed" AS "OLD_Endorsed" ,
                "OLD"."MarginOfConservatismCategoryA" AS "OLD_MarginOfConservatismCategoryA" ,
                "OLD"."MarginOfConservatismCategoryB" AS "OLD_MarginOfConservatismCategoryB" ,
                "OLD"."MarginOfConservatismCategoryC" AS "OLD_MarginOfConservatismCategoryC" ,
                "OLD"."PublicationDate" AS "OLD_PublicationDate" ,
                "OLD"."Rating" AS "OLD_Rating" ,
                "OLD"."RatingBasedOnMarginsOfConservatism" AS "OLD_RatingBasedOnMarginsOfConservatism" ,
                "OLD"."RatingComment" AS "OLD_RatingComment" ,
                "OLD"."RatingDescription" AS "OLD_RatingDescription" ,
                "OLD"."RatingMethodVersion" AS "OLD_RatingMethodVersion" ,
                "OLD"."RatingOutlook" AS "OLD_RatingOutlook" ,
                "OLD"."RatingRank" AS "OLD_RatingRank" ,
                "OLD"."RatingStatusChangeDate" AS "OLD_RatingStatusChangeDate" ,
                "OLD"."RatingStatusReason" AS "OLD_RatingStatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Rating" as "OLD"
            on
                                      "IN"."IsFxRating" = "OLD"."IsFxRating" and
                                      "IN"."RatingAgency" = "OLD"."RatingAgency" and
                                      "IN"."RatingCategory" = "OLD"."RatingCategory" and
                                      "IN"."RatingMethod" = "OLD"."RatingMethod" and
                                      "IN"."RatingStatus" = "OLD"."RatingStatus" and
                                      "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                                      "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                      "IN"."ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" and
                                      "IN"."ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" and
                                      "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                                      "IN"."_Security.FinancialInstrumentID" = "OLD"."_Security.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_IsFxRating" as "IsFxRating",
            "OLD_RatingAgency" as "RatingAgency",
            "OLD_RatingCategory" as "RatingCategory",
            "OLD_RatingMethod" as "RatingMethod",
            "OLD_RatingStatus" as "RatingStatus",
            "OLD_TimeHorizon" as "TimeHorizon",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD_ASSOC_GeographicalRegion.GeographicalStructureID" as "ASSOC_GeographicalRegion.GeographicalStructureID",
            "OLD_ASSOC_GeographicalRegion.GeographicalUnitID" as "ASSOC_GeographicalRegion.GeographicalUnitID",
            "OLD__PhysicalAsset.PhysicalAssetID" as "_PhysicalAsset.PhysicalAssetID",
            "OLD__Security.FinancialInstrumentID" as "_Security.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" as "ASSOC_EmployeeResponsibleForRating.BusinessPartnerID",
            "OLD__EmployeeApprovingTheRating.BusinessPartnerID" as "_EmployeeApprovingTheRating.BusinessPartnerID",
            "OLD__OverridingRating.IsFxRating" as "_OverridingRating.IsFxRating",
            "OLD__OverridingRating.RatingAgency" as "_OverridingRating.RatingAgency",
            "OLD__OverridingRating.RatingCategory" as "_OverridingRating.RatingCategory",
            "OLD__OverridingRating.RatingMethod" as "_OverridingRating.RatingMethod",
            "OLD__OverridingRating.RatingStatus" as "_OverridingRating.RatingStatus",
            "OLD__OverridingRating.TimeHorizon" as "_OverridingRating.TimeHorizon",
            "OLD__OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" as "_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD__OverridingRating.ASSOC_FinancialContract.FinancialContractID" as "_OverridingRating.ASSOC_FinancialContract.FinancialContractID",
            "OLD__OverridingRating.ASSOC_FinancialContract.IDSystem" as "_OverridingRating.ASSOC_FinancialContract.IDSystem",
            "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" as "_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID",
            "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" as "_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID",
            "OLD__OverridingRating._PhysicalAsset.PhysicalAssetID" as "_OverridingRating._PhysicalAsset.PhysicalAssetID",
            "OLD__OverridingRating._Security.FinancialInstrumentID" as "_OverridingRating._Security.FinancialInstrumentID",
            "OLD_AcquisitionDate" as "AcquisitionDate",
            "OLD_ApprovalDate" as "ApprovalDate",
            "OLD_Commissioned" as "Commissioned",
            "OLD_CreationDate" as "CreationDate",
            "OLD_Endorsed" as "Endorsed",
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA",
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB",
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC",
            "OLD_PublicationDate" as "PublicationDate",
            "OLD_Rating" as "Rating",
            "OLD_RatingBasedOnMarginsOfConservatism" as "RatingBasedOnMarginsOfConservatism",
            "OLD_RatingComment" as "RatingComment",
            "OLD_RatingDescription" as "RatingDescription",
            "OLD_RatingMethodVersion" as "RatingMethodVersion",
            "OLD_RatingOutlook" as "RatingOutlook",
            "OLD_RatingRank" as "RatingRank",
            "OLD_RatingStatusChangeDate" as "RatingStatusChangeDate",
            "OLD_RatingStatusReason" as "RatingStatusReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."IsFxRating",
                        "OLD"."RatingAgency",
                        "OLD"."RatingCategory",
                        "OLD"."RatingMethod",
                        "OLD"."RatingStatus",
                        "OLD"."TimeHorizon",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID",
                        "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID",
                        "OLD"."_PhysicalAsset.PhysicalAssetID",
                        "OLD"."_Security.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."IsFxRating" AS "OLD_IsFxRating" ,
                "OLD"."RatingAgency" AS "OLD_RatingAgency" ,
                "OLD"."RatingCategory" AS "OLD_RatingCategory" ,
                "OLD"."RatingMethod" AS "OLD_RatingMethod" ,
                "OLD"."RatingStatus" AS "OLD_RatingStatus" ,
                "OLD"."TimeHorizon" AS "OLD_TimeHorizon" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" AS "OLD_ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" AS "OLD_ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" AS "OLD__PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Security.FinancialInstrumentID" AS "OLD__Security.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" AS "OLD_ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" ,
                "OLD"."_EmployeeApprovingTheRating.BusinessPartnerID" AS "OLD__EmployeeApprovingTheRating.BusinessPartnerID" ,
                "OLD"."_OverridingRating.IsFxRating" AS "OLD__OverridingRating.IsFxRating" ,
                "OLD"."_OverridingRating.RatingAgency" AS "OLD__OverridingRating.RatingAgency" ,
                "OLD"."_OverridingRating.RatingCategory" AS "OLD__OverridingRating.RatingCategory" ,
                "OLD"."_OverridingRating.RatingMethod" AS "OLD__OverridingRating.RatingMethod" ,
                "OLD"."_OverridingRating.RatingStatus" AS "OLD__OverridingRating.RatingStatus" ,
                "OLD"."_OverridingRating.TimeHorizon" AS "OLD__OverridingRating.TimeHorizon" ,
                "OLD"."_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD__OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_OverridingRating.ASSOC_FinancialContract.FinancialContractID" AS "OLD__OverridingRating.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_OverridingRating.ASSOC_FinancialContract.IDSystem" AS "OLD__OverridingRating.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" AS "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" AS "OLD__OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_OverridingRating._PhysicalAsset.PhysicalAssetID" AS "OLD__OverridingRating._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_OverridingRating._Security.FinancialInstrumentID" AS "OLD__OverridingRating._Security.FinancialInstrumentID" ,
                "OLD"."AcquisitionDate" AS "OLD_AcquisitionDate" ,
                "OLD"."ApprovalDate" AS "OLD_ApprovalDate" ,
                "OLD"."Commissioned" AS "OLD_Commissioned" ,
                "OLD"."CreationDate" AS "OLD_CreationDate" ,
                "OLD"."Endorsed" AS "OLD_Endorsed" ,
                "OLD"."MarginOfConservatismCategoryA" AS "OLD_MarginOfConservatismCategoryA" ,
                "OLD"."MarginOfConservatismCategoryB" AS "OLD_MarginOfConservatismCategoryB" ,
                "OLD"."MarginOfConservatismCategoryC" AS "OLD_MarginOfConservatismCategoryC" ,
                "OLD"."PublicationDate" AS "OLD_PublicationDate" ,
                "OLD"."Rating" AS "OLD_Rating" ,
                "OLD"."RatingBasedOnMarginsOfConservatism" AS "OLD_RatingBasedOnMarginsOfConservatism" ,
                "OLD"."RatingComment" AS "OLD_RatingComment" ,
                "OLD"."RatingDescription" AS "OLD_RatingDescription" ,
                "OLD"."RatingMethodVersion" AS "OLD_RatingMethodVersion" ,
                "OLD"."RatingOutlook" AS "OLD_RatingOutlook" ,
                "OLD"."RatingRank" AS "OLD_RatingRank" ,
                "OLD"."RatingStatusChangeDate" AS "OLD_RatingStatusChangeDate" ,
                "OLD"."RatingStatusReason" AS "OLD_RatingStatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Rating" as "OLD"
            on
               "IN"."IsFxRating" = "OLD"."IsFxRating" and
               "IN"."RatingAgency" = "OLD"."RatingAgency" and
               "IN"."RatingCategory" = "OLD"."RatingCategory" and
               "IN"."RatingMethod" = "OLD"."RatingMethod" and
               "IN"."RatingStatus" = "OLD"."RatingStatus" and
               "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
               "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
               "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
               "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
               "IN"."ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" and
               "IN"."ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" and
               "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
               "IN"."_Security.FinancialInstrumentID" = "OLD"."_Security.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
