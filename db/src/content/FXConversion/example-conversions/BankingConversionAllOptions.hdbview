-- This example shows the calling syntax of the scalar SQL function convert_currency
-- For details, see help page https://help.sap.com/viewer/4fe29514fd584807ac9f2a04f6754767/latest/en-US/d22d746ed2951014bb7fb0114ffdaf96.html
--
-- In this view, you see all the options that can be an inpu to the CONVERT_CURRENCY function.
-- For a simpler syntax, check MinimalBankingConversion.hdbview
--
-- In the example below, you see an "indirect" conversion of USD to CAD via EUR.
-- The used rates can be looked up via:
-- select * from "sap.fsdm::EndOfDayExchangeRateObservation" where "BusinessValidFrom" = to_date('2019-05-29');
-- select fx_rate, * from "/BA1/F4_FXRATES" where valid_date = '20190529000000';


VIEW "BankingConversionAllOptions" AS
SELECT CONVERT_CURRENCY(amount=>price,
                        method => 'Banking',
                        schema =>  current_object_schema(),
                        client => '100',
                        source_unit =>source_unit,
                        target_unit => target_unit,
                        reference_date =>reference_date,
                        market_data_area => 'ECB',
                        accuracy => 'highest',
                        system_time => system_timestamp, -- default would be "now"
                        conversion_type => 'M', -- default value
                        error_handling=>'fail on error', -- default value
                        steps => 'convert', -- in general, you only want to convert, especially if there are subsequent aggregations. Otherwise, round may also make sense.
                        configuration_table => '/BA1/TF4_FXRTTYP', -- default value
                        precisions_table => 'TCURX', -- default value
                        notations_table => 'TCURN', -- default value
                        rates_table => '/BA1/F4_FXRATES', -- default value
                        prefactors_table => '/BA1/TF4_FXCVFCT') -- default value
                        as converted
                        FROM ( 
                        	select 100 as price, 
                      			   'USD' as source_unit, 
                      			   'CAD' as target_unit,
                      			   to_date('2019-05-29', 'YYYY-MM-DD') as reference_date,
                      			   current_timestamp as system_timestamp from "sap.fsdm.synonym::DUMMY"
                        );
