PROCEDURE "sap.fsdm.procedures::WriteDownAmountReadOnly" (IN ROW "sap.fsdm.tabletypes::WriteDownAmountTT", OUT CURR_DEL "sap.fsdm.tabletypes::WriteDownAmountTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::WriteDownAmountTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'LotID=' || TO_VARCHAR("LotID") || ' ' ||
                'WriteDownSubtype=' || TO_VARCHAR("WriteDownSubtype") || ' ' ||
                'WriteDownType=' || TO_VARCHAR("WriteDownType") || ' ' ||
                'ASSOC_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("ASSOC_AccountingSystem.AccountingSystemID") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                'ASSOC_PositionCurrency.PositionCurrency=' || TO_VARCHAR("ASSOC_PositionCurrency.PositionCurrency") || ' ' ||
                'ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID=' || TO_VARCHAR("ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID") || ' ' ||
                'ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem=' || TO_VARCHAR("ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."LotID",
                        "IN"."WriteDownSubtype",
                        "IN"."WriteDownType",
                        "IN"."ASSOC_AccountingSystem.AccountingSystemID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrency.PositionCurrency",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."LotID",
                        "IN"."WriteDownSubtype",
                        "IN"."WriteDownType",
                        "IN"."ASSOC_AccountingSystem.AccountingSystemID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrency.PositionCurrency",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "LotID",
                        "WriteDownSubtype",
                        "WriteDownType",
                        "ASSOC_AccountingSystem.AccountingSystemID",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "ASSOC_PositionCurrency.PositionCurrency",
                        "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."LotID",
                                    "IN"."WriteDownSubtype",
                                    "IN"."WriteDownType",
                                    "IN"."ASSOC_AccountingSystem.AccountingSystemID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PositionCurrency.PositionCurrency",
                                    "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."LotID",
                                    "IN"."WriteDownSubtype",
                                    "IN"."WriteDownType",
                                    "IN"."ASSOC_AccountingSystem.AccountingSystemID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PositionCurrency.PositionCurrency",
                                    "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                                    "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "LotID",
        "WriteDownSubtype",
        "WriteDownType",
        "ASSOC_AccountingSystem.AccountingSystemID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PositionCurrency.PositionCurrency",
        "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::WriteDownAmount" WHERE
        (            "LotID" ,
            "WriteDownSubtype" ,
            "WriteDownType" ,
            "ASSOC_AccountingSystem.AccountingSystemID" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCurrency.PositionCurrency" ,
            "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."LotID",
            "OLD"."WriteDownSubtype",
            "OLD"."WriteDownType",
            "OLD"."ASSOC_AccountingSystem.AccountingSystemID",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."ASSOC_PositionCurrency.PositionCurrency",
            "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::WriteDownAmount" as "OLD"
            on
               ifnull( "IN"."LotID",'' ) = "OLD"."LotID" and
               ifnull( "IN"."WriteDownSubtype",'' ) = "OLD"."WriteDownSubtype" and
               ifnull( "IN"."WriteDownType",'' ) = "OLD"."WriteDownType" and
               ifnull( "IN"."ASSOC_AccountingSystem.AccountingSystemID",'' ) = "OLD"."ASSOC_AccountingSystem.AccountingSystemID" and
               ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID",'' ) = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
               ifnull( "IN"."ASSOC_FinancialContract.IDSystem",'' ) = "OLD"."ASSOC_FinancialContract.IDSystem" and
               ifnull( "IN"."ASSOC_PositionCurrency.PositionCurrency",'' ) = "OLD"."ASSOC_PositionCurrency.PositionCurrency" and
               ifnull( "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",'' ) = "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" and
               ifnull( "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",'' ) = "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_SecuritiesAccount.FinancialContractID",'' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
               ifnull( "IN"."_SecuritiesAccount.IDSystem",'' ) = "OLD"."_SecuritiesAccount.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "LotID",
        "WriteDownSubtype",
        "WriteDownType",
        "ASSOC_AccountingSystem.AccountingSystemID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PositionCurrency.PositionCurrency",
        "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
        "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PositionCurrency",
        "WriteDownAmount",
        "WriteDownAmountCategory",
        "WriteDownAmountCurrency",
        "WriteDownAmountInPositionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "LotID", '' ) as "LotID",
                    ifnull( "WriteDownSubtype", '' ) as "WriteDownSubtype",
                    ifnull( "WriteDownType", '' ) as "WriteDownType",
                    ifnull( "ASSOC_AccountingSystem.AccountingSystemID", '' ) as "ASSOC_AccountingSystem.AccountingSystemID",
                    ifnull( "ASSOC_FinancialContract.FinancialContractID", '' ) as "ASSOC_FinancialContract.FinancialContractID",
                    ifnull( "ASSOC_FinancialContract.IDSystem", '' ) as "ASSOC_FinancialContract.IDSystem",
                    ifnull( "ASSOC_PositionCurrency.PositionCurrency", '' ) as "ASSOC_PositionCurrency.PositionCurrency",
                    ifnull( "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID", '' ) as "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                    ifnull( "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem", '' ) as "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
                    ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "PositionCurrency"  ,
                    "WriteDownAmount"  ,
                    "WriteDownAmountCategory"  ,
                    "WriteDownAmountCurrency"  ,
                    "WriteDownAmountInPositionCurrency"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_LotID" as "LotID" ,
                    "OLD_WriteDownSubtype" as "WriteDownSubtype" ,
                    "OLD_WriteDownType" as "WriteDownType" ,
                    "OLD_ASSOC_AccountingSystem.AccountingSystemID" as "ASSOC_AccountingSystem.AccountingSystemID" ,
                    "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
                    "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
                    "OLD_ASSOC_PositionCurrency.PositionCurrency" as "ASSOC_PositionCurrency.PositionCurrency" ,
                    "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                    "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
                    "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_PositionCurrency" as "PositionCurrency" ,
                    "OLD_WriteDownAmount" as "WriteDownAmount" ,
                    "OLD_WriteDownAmountCategory" as "WriteDownAmountCategory" ,
                    "OLD_WriteDownAmountCurrency" as "WriteDownAmountCurrency" ,
                    "OLD_WriteDownAmountInPositionCurrency" as "WriteDownAmountInPositionCurrency" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."LotID",
                        "IN"."WriteDownSubtype",
                        "IN"."WriteDownType",
                        "IN"."ASSOC_AccountingSystem.AccountingSystemID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrency.PositionCurrency",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."LotID" as "OLD_LotID",
                                "OLD"."WriteDownSubtype" as "OLD_WriteDownSubtype",
                                "OLD"."WriteDownType" as "OLD_WriteDownType",
                                "OLD"."ASSOC_AccountingSystem.AccountingSystemID" as "OLD_ASSOC_AccountingSystem.AccountingSystemID",
                                "OLD"."ASSOC_FinancialContract.FinancialContractID" as "OLD_ASSOC_FinancialContract.FinancialContractID",
                                "OLD"."ASSOC_FinancialContract.IDSystem" as "OLD_ASSOC_FinancialContract.IDSystem",
                                "OLD"."ASSOC_PositionCurrency.PositionCurrency" as "OLD_ASSOC_PositionCurrency.PositionCurrency",
                                "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" as "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                                "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" as "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."PositionCurrency" as "OLD_PositionCurrency",
                                "OLD"."WriteDownAmount" as "OLD_WriteDownAmount",
                                "OLD"."WriteDownAmountCategory" as "OLD_WriteDownAmountCategory",
                                "OLD"."WriteDownAmountCurrency" as "OLD_WriteDownAmountCurrency",
                                "OLD"."WriteDownAmountInPositionCurrency" as "OLD_WriteDownAmountInPositionCurrency",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::WriteDownAmount" as "OLD"
            on
                ifnull( "IN"."LotID", '') = "OLD"."LotID" and
                ifnull( "IN"."WriteDownSubtype", '') = "OLD"."WriteDownSubtype" and
                ifnull( "IN"."WriteDownType", '') = "OLD"."WriteDownType" and
                ifnull( "IN"."ASSOC_AccountingSystem.AccountingSystemID", '') = "OLD"."ASSOC_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."ASSOC_FinancialContract.FinancialContractID", '') = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                ifnull( "IN"."ASSOC_FinancialContract.IDSystem", '') = "OLD"."ASSOC_FinancialContract.IDSystem" and
                ifnull( "IN"."ASSOC_PositionCurrency.PositionCurrency", '') = "OLD"."ASSOC_PositionCurrency.PositionCurrency" and
                ifnull( "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID", '') = "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" and
                ifnull( "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem", '') = "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_LotID" as "LotID",
            "OLD_WriteDownSubtype" as "WriteDownSubtype",
            "OLD_WriteDownType" as "WriteDownType",
            "OLD_ASSOC_AccountingSystem.AccountingSystemID" as "ASSOC_AccountingSystem.AccountingSystemID",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD_ASSOC_PositionCurrency.PositionCurrency" as "ASSOC_PositionCurrency.PositionCurrency",
            "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" as "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
            "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" as "ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PositionCurrency" as "PositionCurrency",
            "OLD_WriteDownAmount" as "WriteDownAmount",
            "OLD_WriteDownAmountCategory" as "WriteDownAmountCategory",
            "OLD_WriteDownAmountCurrency" as "WriteDownAmountCurrency",
            "OLD_WriteDownAmountInPositionCurrency" as "WriteDownAmountInPositionCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."LotID",
                        "IN"."WriteDownSubtype",
                        "IN"."WriteDownType",
                        "IN"."ASSOC_AccountingSystem.AccountingSystemID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PositionCurrency.PositionCurrency",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."LotID" as "OLD_LotID",
                        "OLD"."WriteDownSubtype" as "OLD_WriteDownSubtype",
                        "OLD"."WriteDownType" as "OLD_WriteDownType",
                        "OLD"."ASSOC_AccountingSystem.AccountingSystemID" as "OLD_ASSOC_AccountingSystem.AccountingSystemID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID" as "OLD_ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem" as "OLD_ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_PositionCurrency.PositionCurrency" as "OLD_ASSOC_PositionCurrency.PositionCurrency",
                        "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" as "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID",
                        "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" as "OLD_ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."PositionCurrency" as "OLD_PositionCurrency",
                        "OLD"."WriteDownAmount" as "OLD_WriteDownAmount",
                        "OLD"."WriteDownAmountCategory" as "OLD_WriteDownAmountCategory",
                        "OLD"."WriteDownAmountCurrency" as "OLD_WriteDownAmountCurrency",
                        "OLD"."WriteDownAmountInPositionCurrency" as "OLD_WriteDownAmountInPositionCurrency",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::WriteDownAmount" as "OLD"
            on
                ifnull("IN"."LotID", '') = "OLD"."LotID" and
                ifnull("IN"."WriteDownSubtype", '') = "OLD"."WriteDownSubtype" and
                ifnull("IN"."WriteDownType", '') = "OLD"."WriteDownType" and
                ifnull("IN"."ASSOC_AccountingSystem.AccountingSystemID", '') = "OLD"."ASSOC_AccountingSystem.AccountingSystemID" and
                ifnull("IN"."ASSOC_FinancialContract.FinancialContractID", '') = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                ifnull("IN"."ASSOC_FinancialContract.IDSystem", '') = "OLD"."ASSOC_FinancialContract.IDSystem" and
                ifnull("IN"."ASSOC_PositionCurrency.PositionCurrency", '') = "OLD"."ASSOC_PositionCurrency.PositionCurrency" and
                ifnull("IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID", '') = "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.FinancialContractID" and
                ifnull("IN"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem", '') = "OLD"."ASSOC_PositionCurrency.ASSOC_MultiCcyAccnt.IDSystem" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull("IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
