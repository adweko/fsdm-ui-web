PROCEDURE "sap.fsdm.procedures::ValuationScheduleEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ValuationScheduleTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ValuationScheduleTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ValuationScheduleTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" is null and
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" is null and
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" is null and
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ValuationSchedule" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" and
                "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ValuationSchedule_Historical" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" and
                "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" 
        );

END
