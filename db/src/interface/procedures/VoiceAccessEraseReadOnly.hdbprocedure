PROCEDURE "sap.fsdm.procedures::VoiceAccessEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::VoiceAccessTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::VoiceAccessTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::VoiceAccessTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountName" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_VoiceAccessSystem.BankingChannelID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "AccountName" ,
                "ASSOC_BusinessPartner.BusinessPartnerID" ,
                "ASSOC_VoiceAccessSystem.BankingChannelID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."AccountName" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::VoiceAccess" "OLD"
            on
                "IN"."AccountName" = "OLD"."AccountName" and
                "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                "IN"."ASSOC_VoiceAccessSystem.BankingChannelID" = "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "AccountName" ,
            "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "ASSOC_VoiceAccessSystem.BankingChannelID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."AccountName" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::VoiceAccess_Historical" "OLD"
            on
                "IN"."AccountName" = "OLD"."AccountName" and
                "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                "IN"."ASSOC_VoiceAccessSystem.BankingChannelID" = "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" 
        );

END
