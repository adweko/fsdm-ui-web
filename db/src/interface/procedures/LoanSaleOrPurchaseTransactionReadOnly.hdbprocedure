PROCEDURE "sap.fsdm.procedures::LoanSaleOrPurchaseTransactionReadOnly" (IN ROW "sap.fsdm.tabletypes::LoanSaleOrPurchaseTransactionTT", OUT CURR_DEL "sap.fsdm.tabletypes::LoanSaleOrPurchaseTransactionTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::LoanSaleOrPurchaseTransactionTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'TransactionID=' || TO_VARCHAR("TransactionID") || ' ' ||
                '_Loan.FinancialContractID=' || TO_VARCHAR("_Loan.FinancialContractID") || ' ' ||
                '_Loan.IDSystem=' || TO_VARCHAR("_Loan.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."TransactionID",
                        "IN"."_Loan.FinancialContractID",
                        "IN"."_Loan.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."TransactionID",
                        "IN"."_Loan.FinancialContractID",
                        "IN"."_Loan.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "TransactionID",
                        "_Loan.FinancialContractID",
                        "_Loan.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."TransactionID",
                                    "IN"."_Loan.FinancialContractID",
                                    "IN"."_Loan.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."TransactionID",
                                    "IN"."_Loan.FinancialContractID",
                                    "IN"."_Loan.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "TransactionID",
        "_Loan.FinancialContractID",
        "_Loan.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::LoanSaleOrPurchaseTransaction" WHERE
        (            "TransactionID" ,
            "_Loan.FinancialContractID" ,
            "_Loan.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."TransactionID",
            "OLD"."_Loan.FinancialContractID",
            "OLD"."_Loan.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::LoanSaleOrPurchaseTransaction" as "OLD"
            on
               ifnull( "IN"."TransactionID",'' ) = "OLD"."TransactionID" and
               ifnull( "IN"."_Loan.FinancialContractID",'' ) = "OLD"."_Loan.FinancialContractID" and
               ifnull( "IN"."_Loan.IDSystem",'' ) = "OLD"."_Loan.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "TransactionID",
        "_Loan.FinancialContractID",
        "_Loan.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_BankingChannel.BankingChannelID",
        "_BusinessEventDataOwner.BusinessPartnerID",
        "_LoanPurchaser.BusinessPartnerID",
        "_LoanSeller.BusinessPartnerID",
        "CarryingAmount",
        "CarryingAmountCurrency",
        "ValueDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "TransactionID", '' ) as "TransactionID",
                    ifnull( "_Loan.FinancialContractID", '' ) as "_Loan.FinancialContractID",
                    ifnull( "_Loan.IDSystem", '' ) as "_Loan.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "_BankingChannel.BankingChannelID"  ,
                    "_BusinessEventDataOwner.BusinessPartnerID"  ,
                    "_LoanPurchaser.BusinessPartnerID"  ,
                    "_LoanSeller.BusinessPartnerID"  ,
                    "CarryingAmount"  ,
                    "CarryingAmountCurrency"  ,
                    "ValueDate"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_TransactionID" as "TransactionID" ,
                    "OLD__Loan.FinancialContractID" as "_Loan.FinancialContractID" ,
                    "OLD__Loan.IDSystem" as "_Loan.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID" ,
                    "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID" ,
                    "OLD__LoanPurchaser.BusinessPartnerID" as "_LoanPurchaser.BusinessPartnerID" ,
                    "OLD__LoanSeller.BusinessPartnerID" as "_LoanSeller.BusinessPartnerID" ,
                    "OLD_CarryingAmount" as "CarryingAmount" ,
                    "OLD_CarryingAmountCurrency" as "CarryingAmountCurrency" ,
                    "OLD_ValueDate" as "ValueDate" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."TransactionID",
                        "IN"."_Loan.FinancialContractID",
                        "IN"."_Loan.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."TransactionID" as "OLD_TransactionID",
                                "OLD"."_Loan.FinancialContractID" as "OLD__Loan.FinancialContractID",
                                "OLD"."_Loan.IDSystem" as "OLD__Loan.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_BankingChannel.BankingChannelID" as "OLD__BankingChannel.BankingChannelID",
                                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" as "OLD__BusinessEventDataOwner.BusinessPartnerID",
                                "OLD"."_LoanPurchaser.BusinessPartnerID" as "OLD__LoanPurchaser.BusinessPartnerID",
                                "OLD"."_LoanSeller.BusinessPartnerID" as "OLD__LoanSeller.BusinessPartnerID",
                                "OLD"."CarryingAmount" as "OLD_CarryingAmount",
                                "OLD"."CarryingAmountCurrency" as "OLD_CarryingAmountCurrency",
                                "OLD"."ValueDate" as "OLD_ValueDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::LoanSaleOrPurchaseTransaction" as "OLD"
            on
                ifnull( "IN"."TransactionID", '') = "OLD"."TransactionID" and
                ifnull( "IN"."_Loan.FinancialContractID", '') = "OLD"."_Loan.FinancialContractID" and
                ifnull( "IN"."_Loan.IDSystem", '') = "OLD"."_Loan.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_TransactionID" as "TransactionID",
            "OLD__Loan.FinancialContractID" as "_Loan.FinancialContractID",
            "OLD__Loan.IDSystem" as "_Loan.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID",
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID",
            "OLD__LoanPurchaser.BusinessPartnerID" as "_LoanPurchaser.BusinessPartnerID",
            "OLD__LoanSeller.BusinessPartnerID" as "_LoanSeller.BusinessPartnerID",
            "OLD_CarryingAmount" as "CarryingAmount",
            "OLD_CarryingAmountCurrency" as "CarryingAmountCurrency",
            "OLD_ValueDate" as "ValueDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."TransactionID",
                        "IN"."_Loan.FinancialContractID",
                        "IN"."_Loan.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."TransactionID" as "OLD_TransactionID",
                        "OLD"."_Loan.FinancialContractID" as "OLD__Loan.FinancialContractID",
                        "OLD"."_Loan.IDSystem" as "OLD__Loan.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."_BankingChannel.BankingChannelID" as "OLD__BankingChannel.BankingChannelID",
                        "OLD"."_BusinessEventDataOwner.BusinessPartnerID" as "OLD__BusinessEventDataOwner.BusinessPartnerID",
                        "OLD"."_LoanPurchaser.BusinessPartnerID" as "OLD__LoanPurchaser.BusinessPartnerID",
                        "OLD"."_LoanSeller.BusinessPartnerID" as "OLD__LoanSeller.BusinessPartnerID",
                        "OLD"."CarryingAmount" as "OLD_CarryingAmount",
                        "OLD"."CarryingAmountCurrency" as "OLD_CarryingAmountCurrency",
                        "OLD"."ValueDate" as "OLD_ValueDate",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::LoanSaleOrPurchaseTransaction" as "OLD"
            on
                ifnull("IN"."TransactionID", '') = "OLD"."TransactionID" and
                ifnull("IN"."_Loan.FinancialContractID", '') = "OLD"."_Loan.FinancialContractID" and
                ifnull("IN"."_Loan.IDSystem", '') = "OLD"."_Loan.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
