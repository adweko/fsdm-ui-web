PROCEDURE "sap.fsdm.procedures::BusinessPartnerClassificationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerClassificationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::BusinessPartnerClassificationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::BusinessPartnerClassificationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ASSOC_BusinessPartnerID.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartnerID.BusinessPartnerID") || ' ' ||
                'ASSOC_ClassificationOfBusinessPartner.PartnerClass=' || TO_VARCHAR("ASSOC_ClassificationOfBusinessPartner.PartnerClass") || ' ' ||
                'ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem=' || TO_VARCHAR("ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                        "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                        "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                        "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                                    "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                                    "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                                    "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                                    "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_BusinessPartnerID.BusinessPartnerID" is null and
            "ASSOC_ClassificationOfBusinessPartner.PartnerClass" is null and
            "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ASSOC_BusinessPartnerID.BusinessPartnerID",
            "ASSOC_ClassificationOfBusinessPartner.PartnerClass",
            "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::BusinessPartnerClassification" WHERE
            (
            "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
            "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
            "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BusinessPartnerClassification" as "OLD"
        on
                              "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                              "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" and
                              "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ASSOC_BusinessPartnerID.BusinessPartnerID",
        "ASSOC_ClassificationOfBusinessPartner.PartnerClass",
        "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" as "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClass" as "ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
            "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" as "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                        "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" AS "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" AS "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerClassification" as "OLD"
            on
                                      "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                                      "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" and
                                      "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" as "ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClass" as "ASSOC_ClassificationOfBusinessPartner.PartnerClass",
            "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" as "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass",
                        "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" AS "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" AS "OLD_ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerClassification" as "OLD"
            on
               "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
               "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" and
               "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
