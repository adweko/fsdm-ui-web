PROCEDURE "sap.fsdm.procedures::AppointmentBasedOnQualificationDelete" (IN ROW "sap.fsdm.tabletypes::AppointmentBasedOnQualificationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Appointment.SequenceNumber=' || TO_VARCHAR("_Appointment.SequenceNumber") || ' ' ||
                '_Appointment._IndividualPerson.BusinessPartnerID=' || TO_VARCHAR("_Appointment._IndividualPerson.BusinessPartnerID") || ' ' ||
                '_ProofOfQualification.IDSystem=' || TO_VARCHAR("_ProofOfQualification.IDSystem") || ' ' ||
                '_ProofOfQualification.IssuingOrganization=' || TO_VARCHAR("_ProofOfQualification.IssuingOrganization") || ' ' ||
                '_ProofOfQualification.QualificationIdentifier=' || TO_VARCHAR("_ProofOfQualification.QualificationIdentifier") || ' ' ||
                '_ProofOfQualification._IndividualPerson.BusinessPartnerID=' || TO_VARCHAR("_ProofOfQualification._IndividualPerson.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Appointment.SequenceNumber",
                        "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "IN"."_ProofOfQualification.IDSystem",
                        "IN"."_ProofOfQualification.IssuingOrganization",
                        "IN"."_ProofOfQualification.QualificationIdentifier",
                        "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Appointment.SequenceNumber",
                        "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "IN"."_ProofOfQualification.IDSystem",
                        "IN"."_ProofOfQualification.IssuingOrganization",
                        "IN"."_ProofOfQualification.QualificationIdentifier",
                        "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Appointment.SequenceNumber",
                        "_Appointment._IndividualPerson.BusinessPartnerID",
                        "_ProofOfQualification.IDSystem",
                        "_ProofOfQualification.IssuingOrganization",
                        "_ProofOfQualification.QualificationIdentifier",
                        "_ProofOfQualification._IndividualPerson.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Appointment.SequenceNumber",
                                    "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                                    "IN"."_ProofOfQualification.IDSystem",
                                    "IN"."_ProofOfQualification.IssuingOrganization",
                                    "IN"."_ProofOfQualification.QualificationIdentifier",
                                    "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Appointment.SequenceNumber",
                                    "IN"."_Appointment._IndividualPerson.BusinessPartnerID",
                                    "IN"."_ProofOfQualification.IDSystem",
                                    "IN"."_ProofOfQualification.IssuingOrganization",
                                    "IN"."_ProofOfQualification.QualificationIdentifier",
                                    "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Appointment.SequenceNumber" is null and
            "_Appointment._IndividualPerson.BusinessPartnerID" is null and
            "_ProofOfQualification.IDSystem" is null and
            "_ProofOfQualification.IssuingOrganization" is null and
            "_ProofOfQualification.QualificationIdentifier" is null and
            "_ProofOfQualification._IndividualPerson.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::AppointmentBasedOnQualification" (
        "_Appointment.SequenceNumber",
        "_Appointment._IndividualPerson.BusinessPartnerID",
        "_ProofOfQualification.IDSystem",
        "_ProofOfQualification.IssuingOrganization",
        "_ProofOfQualification.QualificationIdentifier",
        "_ProofOfQualification._IndividualPerson.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Appointment.SequenceNumber" as "_Appointment.SequenceNumber" ,
            "OLD__Appointment._IndividualPerson.BusinessPartnerID" as "_Appointment._IndividualPerson.BusinessPartnerID" ,
            "OLD__ProofOfQualification.IDSystem" as "_ProofOfQualification.IDSystem" ,
            "OLD__ProofOfQualification.IssuingOrganization" as "_ProofOfQualification.IssuingOrganization" ,
            "OLD__ProofOfQualification.QualificationIdentifier" as "_ProofOfQualification.QualificationIdentifier" ,
            "OLD__ProofOfQualification._IndividualPerson.BusinessPartnerID" as "_ProofOfQualification._IndividualPerson.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Appointment.SequenceNumber",
                        "OLD"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "OLD"."_ProofOfQualification.IDSystem",
                        "OLD"."_ProofOfQualification.IssuingOrganization",
                        "OLD"."_ProofOfQualification.QualificationIdentifier",
                        "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_Appointment.SequenceNumber" AS "OLD__Appointment.SequenceNumber" ,
                "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" AS "OLD__Appointment._IndividualPerson.BusinessPartnerID" ,
                "OLD"."_ProofOfQualification.IDSystem" AS "OLD__ProofOfQualification.IDSystem" ,
                "OLD"."_ProofOfQualification.IssuingOrganization" AS "OLD__ProofOfQualification.IssuingOrganization" ,
                "OLD"."_ProofOfQualification.QualificationIdentifier" AS "OLD__ProofOfQualification.QualificationIdentifier" ,
                "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" AS "OLD__ProofOfQualification._IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AppointmentBasedOnQualification" as "OLD"
            on
                      "IN"."_Appointment.SequenceNumber" = "OLD"."_Appointment.SequenceNumber" and
                      "IN"."_Appointment._IndividualPerson.BusinessPartnerID" = "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" and
                      "IN"."_ProofOfQualification.IDSystem" = "OLD"."_ProofOfQualification.IDSystem" and
                      "IN"."_ProofOfQualification.IssuingOrganization" = "OLD"."_ProofOfQualification.IssuingOrganization" and
                      "IN"."_ProofOfQualification.QualificationIdentifier" = "OLD"."_ProofOfQualification.QualificationIdentifier" and
                      "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" = "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::AppointmentBasedOnQualification" (
        "_Appointment.SequenceNumber",
        "_Appointment._IndividualPerson.BusinessPartnerID",
        "_ProofOfQualification.IDSystem",
        "_ProofOfQualification.IssuingOrganization",
        "_ProofOfQualification.QualificationIdentifier",
        "_ProofOfQualification._IndividualPerson.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Appointment.SequenceNumber" as "_Appointment.SequenceNumber",
            "OLD__Appointment._IndividualPerson.BusinessPartnerID" as "_Appointment._IndividualPerson.BusinessPartnerID",
            "OLD__ProofOfQualification.IDSystem" as "_ProofOfQualification.IDSystem",
            "OLD__ProofOfQualification.IssuingOrganization" as "_ProofOfQualification.IssuingOrganization",
            "OLD__ProofOfQualification.QualificationIdentifier" as "_ProofOfQualification.QualificationIdentifier",
            "OLD__ProofOfQualification._IndividualPerson.BusinessPartnerID" as "_ProofOfQualification._IndividualPerson.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Appointment.SequenceNumber",
                        "OLD"."_Appointment._IndividualPerson.BusinessPartnerID",
                        "OLD"."_ProofOfQualification.IDSystem",
                        "OLD"."_ProofOfQualification.IssuingOrganization",
                        "OLD"."_ProofOfQualification.QualificationIdentifier",
                        "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Appointment.SequenceNumber" AS "OLD__Appointment.SequenceNumber" ,
                "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" AS "OLD__Appointment._IndividualPerson.BusinessPartnerID" ,
                "OLD"."_ProofOfQualification.IDSystem" AS "OLD__ProofOfQualification.IDSystem" ,
                "OLD"."_ProofOfQualification.IssuingOrganization" AS "OLD__ProofOfQualification.IssuingOrganization" ,
                "OLD"."_ProofOfQualification.QualificationIdentifier" AS "OLD__ProofOfQualification.QualificationIdentifier" ,
                "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" AS "OLD__ProofOfQualification._IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AppointmentBasedOnQualification" as "OLD"
            on
                                                "IN"."_Appointment.SequenceNumber" = "OLD"."_Appointment.SequenceNumber" and
                                                "IN"."_Appointment._IndividualPerson.BusinessPartnerID" = "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" and
                                                "IN"."_ProofOfQualification.IDSystem" = "OLD"."_ProofOfQualification.IDSystem" and
                                                "IN"."_ProofOfQualification.IssuingOrganization" = "OLD"."_ProofOfQualification.IssuingOrganization" and
                                                "IN"."_ProofOfQualification.QualificationIdentifier" = "OLD"."_ProofOfQualification.QualificationIdentifier" and
                                                "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" = "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::AppointmentBasedOnQualification"
    where (
        "_Appointment.SequenceNumber",
        "_Appointment._IndividualPerson.BusinessPartnerID",
        "_ProofOfQualification.IDSystem",
        "_ProofOfQualification.IssuingOrganization",
        "_ProofOfQualification.QualificationIdentifier",
        "_ProofOfQualification._IndividualPerson.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_Appointment.SequenceNumber",
            "OLD"."_Appointment._IndividualPerson.BusinessPartnerID",
            "OLD"."_ProofOfQualification.IDSystem",
            "OLD"."_ProofOfQualification.IssuingOrganization",
            "OLD"."_ProofOfQualification.QualificationIdentifier",
            "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::AppointmentBasedOnQualification" as "OLD"
        on
                                       "IN"."_Appointment.SequenceNumber" = "OLD"."_Appointment.SequenceNumber" and
                                       "IN"."_Appointment._IndividualPerson.BusinessPartnerID" = "OLD"."_Appointment._IndividualPerson.BusinessPartnerID" and
                                       "IN"."_ProofOfQualification.IDSystem" = "OLD"."_ProofOfQualification.IDSystem" and
                                       "IN"."_ProofOfQualification.IssuingOrganization" = "OLD"."_ProofOfQualification.IssuingOrganization" and
                                       "IN"."_ProofOfQualification.QualificationIdentifier" = "OLD"."_ProofOfQualification.QualificationIdentifier" and
                                       "IN"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" = "OLD"."_ProofOfQualification._IndividualPerson.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
