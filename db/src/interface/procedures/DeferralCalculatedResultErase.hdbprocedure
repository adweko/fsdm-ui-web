PROCEDURE "sap.fsdm.procedures::DeferralCalculatedResultErase" (IN ROW "sap.fsdm.tabletypes::DeferralCalculatedResultTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountingChangeSequenceNumber" is null and
            "DeferralCalculationMethod" is null and
            "DeferralType" is null and
            "LotID" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null and
            "_SettlementItem.IDSystem" is null and
            "_SettlementItem.ItemNumber" is null and
            "_SettlementItem.SettlementID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::DeferralCalculatedResult"
        WHERE
        (            "AccountingChangeSequenceNumber" ,
            "DeferralCalculationMethod" ,
            "DeferralType" ,
            "LotID" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" ,
            "_SettlementItem.IDSystem" ,
            "_SettlementItem.ItemNumber" ,
            "_SettlementItem.SettlementID" 
        ) in
        (
            select                 "OLD"."AccountingChangeSequenceNumber" ,
                "OLD"."DeferralCalculationMethod" ,
                "OLD"."DeferralType" ,
                "OLD"."LotID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" ,
                "OLD"."_SettlementItem.IDSystem" ,
                "OLD"."_SettlementItem.ItemNumber" ,
                "OLD"."_SettlementItem.SettlementID" 
            from :ROW "IN"
            inner join "sap.fsdm::DeferralCalculatedResult" "OLD"
            on
            "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
            "IN"."DeferralCalculationMethod" = "OLD"."DeferralCalculationMethod" and
            "IN"."DeferralType" = "OLD"."DeferralType" and
            "IN"."LotID" = "OLD"."LotID" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
            "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
            "IN"."_SettlementItem.IDSystem" = "OLD"."_SettlementItem.IDSystem" and
            "IN"."_SettlementItem.ItemNumber" = "OLD"."_SettlementItem.ItemNumber" and
            "IN"."_SettlementItem.SettlementID" = "OLD"."_SettlementItem.SettlementID" 
        );

        --delete data from history table
        delete from "sap.fsdm::DeferralCalculatedResult_Historical"
        WHERE
        (
            "AccountingChangeSequenceNumber" ,
            "DeferralCalculationMethod" ,
            "DeferralType" ,
            "LotID" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" ,
            "_SettlementItem.IDSystem" ,
            "_SettlementItem.ItemNumber" ,
            "_SettlementItem.SettlementID" 
        ) in
        (
            select
                "OLD"."AccountingChangeSequenceNumber" ,
                "OLD"."DeferralCalculationMethod" ,
                "OLD"."DeferralType" ,
                "OLD"."LotID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" ,
                "OLD"."_SettlementItem.IDSystem" ,
                "OLD"."_SettlementItem.ItemNumber" ,
                "OLD"."_SettlementItem.SettlementID" 
            from :ROW "IN"
            inner join "sap.fsdm::DeferralCalculatedResult_Historical" "OLD"
            on
                "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
                "IN"."DeferralCalculationMethod" = "OLD"."DeferralCalculationMethod" and
                "IN"."DeferralType" = "OLD"."DeferralType" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" and
                "IN"."_SettlementItem.IDSystem" = "OLD"."_SettlementItem.IDSystem" and
                "IN"."_SettlementItem.ItemNumber" = "OLD"."_SettlementItem.ItemNumber" and
                "IN"."_SettlementItem.SettlementID" = "OLD"."_SettlementItem.SettlementID" 
        );

END
