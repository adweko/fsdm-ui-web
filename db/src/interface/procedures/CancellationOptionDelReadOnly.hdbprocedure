PROCEDURE "sap.fsdm.procedures::CancellationOptionDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CancellationOptionTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CancellationOptionTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CancellationOptionTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_RentalContractInformation.RentalContractInformationReferenceID=' || TO_VARCHAR("_RentalContractInformation.RentalContractInformationReferenceID") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_RentalContractInformation.RentalContractInformationReferenceID",
                        "_Trade.IDSystem",
                        "_Trade.TradeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_RentalContractInformation.RentalContractInformationReferenceID",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_RentalContractInformation.RentalContractInformationReferenceID",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_RentalContractInformation.RentalContractInformationReferenceID" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceNumber",
            "ASSOC_FinancialContract.FinancialContractID",
            "ASSOC_FinancialContract.IDSystem",
            "_RentalContractInformation.RentalContractInformationReferenceID",
            "_Trade.IDSystem",
            "_Trade.TradeID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CancellationOption" WHERE
            (
            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CancellationOption" as "OLD"
        on
                              "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                              "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                              "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" and
                              "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                              "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_RentalContractInformation.RentalContractInformationReferenceID",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BeginOfExercisePeriod",
        "BusinessCalendar",
        "BusinessDayConvention",
        "CancellationOptionCategory",
        "CancellationOptionHolder",
        "CancellationOptionType",
        "EndOfExercisePeriod",
        "NoticePeriodLength",
        "NoticePeriodTimeUnit",
        "OpportunityCostCompensation",
        "PartialTerminationAllowed",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__RentalContractInformation.RentalContractInformationReferenceID" as "_RentalContractInformation.RentalContractInformationReferenceID" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BeginOfExercisePeriod" as "BeginOfExercisePeriod" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_CancellationOptionCategory" as "CancellationOptionCategory" ,
            "OLD_CancellationOptionHolder" as "CancellationOptionHolder" ,
            "OLD_CancellationOptionType" as "CancellationOptionType" ,
            "OLD_EndOfExercisePeriod" as "EndOfExercisePeriod" ,
            "OLD_NoticePeriodLength" as "NoticePeriodLength" ,
            "OLD_NoticePeriodTimeUnit" as "NoticePeriodTimeUnit" ,
            "OLD_OpportunityCostCompensation" as "OpportunityCostCompensation" ,
            "OLD_PartialTerminationAllowed" as "PartialTerminationAllowed" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BeginOfExercisePeriod" AS "OLD_BeginOfExercisePeriod" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CancellationOptionCategory" AS "OLD_CancellationOptionCategory" ,
                "OLD"."CancellationOptionHolder" AS "OLD_CancellationOptionHolder" ,
                "OLD"."CancellationOptionType" AS "OLD_CancellationOptionType" ,
                "OLD"."EndOfExercisePeriod" AS "OLD_EndOfExercisePeriod" ,
                "OLD"."NoticePeriodLength" AS "OLD_NoticePeriodLength" ,
                "OLD"."NoticePeriodTimeUnit" AS "OLD_NoticePeriodTimeUnit" ,
                "OLD"."OpportunityCostCompensation" AS "OLD_OpportunityCostCompensation" ,
                "OLD"."PartialTerminationAllowed" AS "OLD_PartialTerminationAllowed" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CancellationOption" as "OLD"
            on
                                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                      "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" and
                                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__RentalContractInformation.RentalContractInformationReferenceID" as "_RentalContractInformation.RentalContractInformationReferenceID",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BeginOfExercisePeriod" as "BeginOfExercisePeriod",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_CancellationOptionCategory" as "CancellationOptionCategory",
            "OLD_CancellationOptionHolder" as "CancellationOptionHolder",
            "OLD_CancellationOptionType" as "CancellationOptionType",
            "OLD_EndOfExercisePeriod" as "EndOfExercisePeriod",
            "OLD_NoticePeriodLength" as "NoticePeriodLength",
            "OLD_NoticePeriodTimeUnit" as "NoticePeriodTimeUnit",
            "OLD_OpportunityCostCompensation" as "OpportunityCostCompensation",
            "OLD_PartialTerminationAllowed" as "PartialTerminationAllowed",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_RentalContractInformation.RentalContractInformationReferenceID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BeginOfExercisePeriod" AS "OLD_BeginOfExercisePeriod" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CancellationOptionCategory" AS "OLD_CancellationOptionCategory" ,
                "OLD"."CancellationOptionHolder" AS "OLD_CancellationOptionHolder" ,
                "OLD"."CancellationOptionType" AS "OLD_CancellationOptionType" ,
                "OLD"."EndOfExercisePeriod" AS "OLD_EndOfExercisePeriod" ,
                "OLD"."NoticePeriodLength" AS "OLD_NoticePeriodLength" ,
                "OLD"."NoticePeriodTimeUnit" AS "OLD_NoticePeriodTimeUnit" ,
                "OLD"."OpportunityCostCompensation" AS "OLD_OpportunityCostCompensation" ,
                "OLD"."PartialTerminationAllowed" AS "OLD_PartialTerminationAllowed" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CancellationOption" as "OLD"
            on
               "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
               "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
               "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
               "IN"."_RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."_RentalContractInformation.RentalContractInformationReferenceID" and
               "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
               "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
