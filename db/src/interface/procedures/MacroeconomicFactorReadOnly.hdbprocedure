PROCEDURE "sap.fsdm.procedures::MacroeconomicFactorReadOnly" (IN ROW "sap.fsdm.tabletypes::MacroeconomicFactorTT", OUT CURR_DEL "sap.fsdm.tabletypes::MacroeconomicFactorTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::MacroeconomicFactorTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CalculationPeriodID=' || TO_VARCHAR("CalculationPeriodID") || ' ' ||
                'Provider=' || TO_VARCHAR("Provider") || ' ' ||
                'Type=' || TO_VARCHAR("Type") || ' ' ||
                '_Segment.SegmentID=' || TO_VARCHAR("_Segment.SegmentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CalculationPeriodID",
                        "IN"."Provider",
                        "IN"."Type",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CalculationPeriodID",
                        "IN"."Provider",
                        "IN"."Type",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CalculationPeriodID",
                        "Provider",
                        "Type",
                        "_Segment.SegmentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CalculationPeriodID",
                                    "IN"."Provider",
                                    "IN"."Type",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CalculationPeriodID",
                                    "IN"."Provider",
                                    "IN"."Type",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "CalculationPeriodID",
        "Provider",
        "Type",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::MacroeconomicFactor" WHERE
        (            "CalculationPeriodID" ,
            "Provider" ,
            "Type" ,
            "_Segment.SegmentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."CalculationPeriodID",
            "OLD"."Provider",
            "OLD"."Type",
            "OLD"."_Segment.SegmentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::MacroeconomicFactor" as "OLD"
            on
               ifnull( "IN"."CalculationPeriodID",'' ) = "OLD"."CalculationPeriodID" and
               ifnull( "IN"."Provider",'' ) = "OLD"."Provider" and
               ifnull( "IN"."Type",'' ) = "OLD"."Type" and
               ifnull( "IN"."_Segment.SegmentID",'' ) = "OLD"."_Segment.SegmentID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "CalculationPeriodID",
        "Provider",
        "Type",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationPeriodLength",
        "CalculationPeriodStartDate",
        "CalculationPeriodTimeUnit",
        "Currency",
        "Description",
        "PeriodOverPeriodLength",
        "PeriodOverPeriodTimeUnit",
        "PublicationDate",
        "Rate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "CalculationPeriodID", '' ) as "CalculationPeriodID",
                    ifnull( "Provider", '' ) as "Provider",
                    ifnull( "Type", '' ) as "Type",
                    ifnull( "_Segment.SegmentID", '' ) as "_Segment.SegmentID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "Amount"  ,
                    "CalculationPeriodLength"  ,
                    "CalculationPeriodStartDate"  ,
                    "CalculationPeriodTimeUnit"  ,
                    "Currency"  ,
                    "Description"  ,
                    "PeriodOverPeriodLength"  ,
                    "PeriodOverPeriodTimeUnit"  ,
                    "PublicationDate"  ,
                    "Rate"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_CalculationPeriodID" as "CalculationPeriodID" ,
                    "OLD_Provider" as "Provider" ,
                    "OLD_Type" as "Type" ,
                    "OLD__Segment.SegmentID" as "_Segment.SegmentID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_Amount" as "Amount" ,
                    "OLD_CalculationPeriodLength" as "CalculationPeriodLength" ,
                    "OLD_CalculationPeriodStartDate" as "CalculationPeriodStartDate" ,
                    "OLD_CalculationPeriodTimeUnit" as "CalculationPeriodTimeUnit" ,
                    "OLD_Currency" as "Currency" ,
                    "OLD_Description" as "Description" ,
                    "OLD_PeriodOverPeriodLength" as "PeriodOverPeriodLength" ,
                    "OLD_PeriodOverPeriodTimeUnit" as "PeriodOverPeriodTimeUnit" ,
                    "OLD_PublicationDate" as "PublicationDate" ,
                    "OLD_Rate" as "Rate" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."CalculationPeriodID",
                        "IN"."Provider",
                        "IN"."Type",
                        "IN"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."CalculationPeriodID" as "OLD_CalculationPeriodID",
                                "OLD"."Provider" as "OLD_Provider",
                                "OLD"."Type" as "OLD_Type",
                                "OLD"."_Segment.SegmentID" as "OLD__Segment.SegmentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Amount" as "OLD_Amount",
                                "OLD"."CalculationPeriodLength" as "OLD_CalculationPeriodLength",
                                "OLD"."CalculationPeriodStartDate" as "OLD_CalculationPeriodStartDate",
                                "OLD"."CalculationPeriodTimeUnit" as "OLD_CalculationPeriodTimeUnit",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."PeriodOverPeriodLength" as "OLD_PeriodOverPeriodLength",
                                "OLD"."PeriodOverPeriodTimeUnit" as "OLD_PeriodOverPeriodTimeUnit",
                                "OLD"."PublicationDate" as "OLD_PublicationDate",
                                "OLD"."Rate" as "OLD_Rate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::MacroeconomicFactor" as "OLD"
            on
                ifnull( "IN"."CalculationPeriodID", '') = "OLD"."CalculationPeriodID" and
                ifnull( "IN"."Provider", '') = "OLD"."Provider" and
                ifnull( "IN"."Type", '') = "OLD"."Type" and
                ifnull( "IN"."_Segment.SegmentID", '') = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_CalculationPeriodID" as "CalculationPeriodID",
            "OLD_Provider" as "Provider",
            "OLD_Type" as "Type",
            "OLD__Segment.SegmentID" as "_Segment.SegmentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Amount" as "Amount",
            "OLD_CalculationPeriodLength" as "CalculationPeriodLength",
            "OLD_CalculationPeriodStartDate" as "CalculationPeriodStartDate",
            "OLD_CalculationPeriodTimeUnit" as "CalculationPeriodTimeUnit",
            "OLD_Currency" as "Currency",
            "OLD_Description" as "Description",
            "OLD_PeriodOverPeriodLength" as "PeriodOverPeriodLength",
            "OLD_PeriodOverPeriodTimeUnit" as "PeriodOverPeriodTimeUnit",
            "OLD_PublicationDate" as "PublicationDate",
            "OLD_Rate" as "Rate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."CalculationPeriodID",
                        "IN"."Provider",
                        "IN"."Type",
                        "IN"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."CalculationPeriodID" as "OLD_CalculationPeriodID",
                        "OLD"."Provider" as "OLD_Provider",
                        "OLD"."Type" as "OLD_Type",
                        "OLD"."_Segment.SegmentID" as "OLD__Segment.SegmentID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."Amount" as "OLD_Amount",
                        "OLD"."CalculationPeriodLength" as "OLD_CalculationPeriodLength",
                        "OLD"."CalculationPeriodStartDate" as "OLD_CalculationPeriodStartDate",
                        "OLD"."CalculationPeriodTimeUnit" as "OLD_CalculationPeriodTimeUnit",
                        "OLD"."Currency" as "OLD_Currency",
                        "OLD"."Description" as "OLD_Description",
                        "OLD"."PeriodOverPeriodLength" as "OLD_PeriodOverPeriodLength",
                        "OLD"."PeriodOverPeriodTimeUnit" as "OLD_PeriodOverPeriodTimeUnit",
                        "OLD"."PublicationDate" as "OLD_PublicationDate",
                        "OLD"."Rate" as "OLD_Rate",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::MacroeconomicFactor" as "OLD"
            on
                ifnull("IN"."CalculationPeriodID", '') = "OLD"."CalculationPeriodID" and
                ifnull("IN"."Provider", '') = "OLD"."Provider" and
                ifnull("IN"."Type", '') = "OLD"."Type" and
                ifnull("IN"."_Segment.SegmentID", '') = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
