namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"ResultGroup";

entity "CounterpartyCreditRiskSingleExposureResults" {
    key "AssetSubclass"                                : String(40)                                                   default '';
    key "MarginedNettingSet"                           : Boolean                                                      default false;
    key "_ExchangeTradedNettingSet"                    : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_FinancialContract"                           : association to FinancialContract {
                                                                                            FinancialContractID,
                                                                                            IDSystem
                                                                                          }                           not null;
    key "_FinancialContractNettingSet"                 : association to FinancialContract {
                                                                                            FinancialContractID,
                                                                                            IDSystem
                                                                                          }                           not null;
    key "_FinancialInstrument"                         : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_ResultGroup"                                 : association to ResultGroup {
                                                                                      ResultDataProvider,
                                                                                      ResultGroupID
                                                                                    }                                 not null;
    key "_SecuritiesAccount"                           : association to FinancialContract {
                                                                                            FinancialContractID,
                                                                                            IDSystem
                                                                                          }                           not null;
    key "_SecuritiesAccountOfExchangeTradedNettingSet" : association to FinancialContract {
                                                                                            FinancialContractID,
                                                                                            IDSystem
                                                                                          }                           not null;
    key "BusinessValidFrom"                            : LocalDate;
    key "BusinessValidTo"                              : LocalDate;
        "SystemValidFrom"                              : UTCTimestamp                                                 not null;
        "SystemValidTo"                                : UTCTimestamp                                                 not null;
        "AdjustedNotionalAmount"                       : Decimal(34, 6);
        "AdjustedNotionalAmountCurrency"               : String(3);
        "LongSettlementTransaction"                    : Boolean;
        "LongShort"                                    : String(10);
        "MarketValue"                                  : Decimal(34, 6);
        "MarketValueCurrency"                          : String(3);
        "NotionalAmount"                               : Decimal(34, 6);
        "NotionalAmountCurrency"                       : String(3);
        "ReferenceEntityType"                          : String(40);
        "SourceSystemID"                               : String(128);
        "ChangeTimestampInSourceSystem"                : UTCTimestamp;
        "ChangingUserInSourceSystem"                   : String(128);
        "ChangingProcessType"                          : String(40);
        "ChangingProcessID"                            : String(128);
}
technical configuration {
    column store;
};

entity "CounterpartyCreditRiskSingleExposureResults_Historical" {
    "AssetSubclass"                                : String(40)                                                   default '' not null;
    "MarginedNettingSet"                           : Boolean                                                      default false not null;
    "_ExchangeTradedNettingSet"                    : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_FinancialContract"                           : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    "_FinancialContractNettingSet"                 : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    "_FinancialInstrument"                         : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_ResultGroup"                                 : association to ResultGroup {
                                                                                  ResultDataProvider,
                                                                                  ResultGroupID
                                                                                }                                 not null;
    "_SecuritiesAccount"                           : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    "_SecuritiesAccountOfExchangeTradedNettingSet" : association to FinancialContract {
                                                                                        FinancialContractID,
                                                                                        IDSystem
                                                                                      }                           not null;
    "BusinessValidFrom"                            : LocalDate                                                    not null;
    "BusinessValidTo"                              : LocalDate                                                    not null;
    "SystemValidFrom"                              : UTCTimestamp                                                 not null;
    "SystemValidTo"                                : UTCTimestamp                                                 not null;
    "AdjustedNotionalAmount"                       : Decimal(34, 6);
    "AdjustedNotionalAmountCurrency"               : String(3);
    "LongSettlementTransaction"                    : Boolean;
    "LongShort"                                    : String(10);
    "MarketValue"                                  : Decimal(34, 6);
    "MarketValueCurrency"                          : String(3);
    "NotionalAmount"                               : Decimal(34, 6);
    "NotionalAmountCurrency"                       : String(3);
    "ReferenceEntityType"                          : String(40);
    "SourceSystemID"                               : String(128);
    "ChangeTimestampInSourceSystem"                : UTCTimestamp;
    "ChangingUserInSourceSystem"                   : String(128);
    "ChangingProcessType"                          : String(40);
    "ChangingProcessID"                            : String(128);
}
technical configuration {
    column store;
};