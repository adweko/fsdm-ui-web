PROCEDURE "sap.fsdm.procedures::ListedDerivativeTradingObservationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ListedDerivativeTradingObservationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ListedDerivativeTradingObservationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ListedDerivativeTradingObservationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'MaturityDate=' || TO_VARCHAR("MaturityDate") || ' ' ||
                'PriceDataProvider=' || TO_VARCHAR("PriceDataProvider") || ' ' ||
                'PriceSeriesType=' || TO_VARCHAR("PriceSeriesType") || ' ' ||
                'PutOrCall=' || TO_VARCHAR("PutOrCall") || ' ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'StrikePrice=' || TO_VARCHAR("StrikePrice") || ' ' ||
                'Timestamp=' || TO_VARCHAR("Timestamp") || ' ' ||
                '_Exchange.MarketIdentifierCode=' || TO_VARCHAR("_Exchange.MarketIdentifierCode") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."MaturityDate",
                        "IN"."PriceDataProvider",
                        "IN"."PriceSeriesType",
                        "IN"."PutOrCall",
                        "IN"."SequenceNumber",
                        "IN"."StrikePrice",
                        "IN"."Timestamp",
                        "IN"."_Exchange.MarketIdentifierCode",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."MaturityDate",
                        "IN"."PriceDataProvider",
                        "IN"."PriceSeriesType",
                        "IN"."PutOrCall",
                        "IN"."SequenceNumber",
                        "IN"."StrikePrice",
                        "IN"."Timestamp",
                        "IN"."_Exchange.MarketIdentifierCode",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "MaturityDate",
                        "PriceDataProvider",
                        "PriceSeriesType",
                        "PutOrCall",
                        "SequenceNumber",
                        "StrikePrice",
                        "Timestamp",
                        "_Exchange.MarketIdentifierCode",
                        "_FinancialInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."MaturityDate",
                                    "IN"."PriceDataProvider",
                                    "IN"."PriceSeriesType",
                                    "IN"."PutOrCall",
                                    "IN"."SequenceNumber",
                                    "IN"."StrikePrice",
                                    "IN"."Timestamp",
                                    "IN"."_Exchange.MarketIdentifierCode",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."MaturityDate",
                                    "IN"."PriceDataProvider",
                                    "IN"."PriceSeriesType",
                                    "IN"."PutOrCall",
                                    "IN"."SequenceNumber",
                                    "IN"."StrikePrice",
                                    "IN"."Timestamp",
                                    "IN"."_Exchange.MarketIdentifierCode",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "MaturityDate" is null and
            "PriceDataProvider" is null and
            "PriceSeriesType" is null and
            "PutOrCall" is null and
            "SequenceNumber" is null and
            "StrikePrice" is null and
            "Timestamp" is null and
            "_Exchange.MarketIdentifierCode" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "MaturityDate",
            "PriceDataProvider",
            "PriceSeriesType",
            "PutOrCall",
            "SequenceNumber",
            "StrikePrice",
            "Timestamp",
            "_Exchange.MarketIdentifierCode",
            "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::ListedDerivativeTradingObservation" WHERE
            (
            "MaturityDate" ,
            "PriceDataProvider" ,
            "PriceSeriesType" ,
            "PutOrCall" ,
            "SequenceNumber" ,
            "StrikePrice" ,
            "Timestamp" ,
            "_Exchange.MarketIdentifierCode" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."MaturityDate",
            "OLD"."PriceDataProvider",
            "OLD"."PriceSeriesType",
            "OLD"."PutOrCall",
            "OLD"."SequenceNumber",
            "OLD"."StrikePrice",
            "OLD"."Timestamp",
            "OLD"."_Exchange.MarketIdentifierCode",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ListedDerivativeTradingObservation" as "OLD"
        on
                              "IN"."MaturityDate" = "OLD"."MaturityDate" and
                              "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                              "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                              "IN"."PutOrCall" = "OLD"."PutOrCall" and
                              "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                              "IN"."StrikePrice" = "OLD"."StrikePrice" and
                              "IN"."Timestamp" = "OLD"."Timestamp" and
                              "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "MaturityDate",
        "PriceDataProvider",
        "PriceSeriesType",
        "PutOrCall",
        "SequenceNumber",
        "StrikePrice",
        "Timestamp",
        "_Exchange.MarketIdentifierCode",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CommodityReferencePrice.ReferencePriceID",
        "Currency",
        "ListedDerivativeSymbol",
        "ListedDerivativeTradingObservationCategory",
        "Price",
        "PriceNotationForm",
        "Quantity",
        "StrikePriceCurrency",
        "TickID",
        "Unit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_MaturityDate" as "MaturityDate" ,
            "OLD_PriceDataProvider" as "PriceDataProvider" ,
            "OLD_PriceSeriesType" as "PriceSeriesType" ,
            "OLD_PutOrCall" as "PutOrCall" ,
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_StrikePrice" as "StrikePrice" ,
            "OLD_Timestamp" as "Timestamp" ,
            "OLD__Exchange.MarketIdentifierCode" as "_Exchange.MarketIdentifierCode" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__CommodityReferencePrice.ReferencePriceID" as "_CommodityReferencePrice.ReferencePriceID" ,
            "OLD_Currency" as "Currency" ,
            "OLD_ListedDerivativeSymbol" as "ListedDerivativeSymbol" ,
            "OLD_ListedDerivativeTradingObservationCategory" as "ListedDerivativeTradingObservationCategory" ,
            "OLD_Price" as "Price" ,
            "OLD_PriceNotationForm" as "PriceNotationForm" ,
            "OLD_Quantity" as "Quantity" ,
            "OLD_StrikePriceCurrency" as "StrikePriceCurrency" ,
            "OLD_TickID" as "TickID" ,
            "OLD_Unit" as "Unit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."MaturityDate",
                        "OLD"."PriceDataProvider",
                        "OLD"."PriceSeriesType",
                        "OLD"."PutOrCall",
                        "OLD"."SequenceNumber",
                        "OLD"."StrikePrice",
                        "OLD"."Timestamp",
                        "OLD"."_Exchange.MarketIdentifierCode",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."MaturityDate" AS "OLD_MaturityDate" ,
                "OLD"."PriceDataProvider" AS "OLD_PriceDataProvider" ,
                "OLD"."PriceSeriesType" AS "OLD_PriceSeriesType" ,
                "OLD"."PutOrCall" AS "OLD_PutOrCall" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."StrikePrice" AS "OLD_StrikePrice" ,
                "OLD"."Timestamp" AS "OLD_Timestamp" ,
                "OLD"."_Exchange.MarketIdentifierCode" AS "OLD__Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_CommodityReferencePrice.ReferencePriceID" AS "OLD__CommodityReferencePrice.ReferencePriceID" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."ListedDerivativeSymbol" AS "OLD_ListedDerivativeSymbol" ,
                "OLD"."ListedDerivativeTradingObservationCategory" AS "OLD_ListedDerivativeTradingObservationCategory" ,
                "OLD"."Price" AS "OLD_Price" ,
                "OLD"."PriceNotationForm" AS "OLD_PriceNotationForm" ,
                "OLD"."Quantity" AS "OLD_Quantity" ,
                "OLD"."StrikePriceCurrency" AS "OLD_StrikePriceCurrency" ,
                "OLD"."TickID" AS "OLD_TickID" ,
                "OLD"."Unit" AS "OLD_Unit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ListedDerivativeTradingObservation" as "OLD"
            on
                                      "IN"."MaturityDate" = "OLD"."MaturityDate" and
                                      "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                                      "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
                                      "IN"."PutOrCall" = "OLD"."PutOrCall" and
                                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                      "IN"."StrikePrice" = "OLD"."StrikePrice" and
                                      "IN"."Timestamp" = "OLD"."Timestamp" and
                                      "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_MaturityDate" as "MaturityDate",
            "OLD_PriceDataProvider" as "PriceDataProvider",
            "OLD_PriceSeriesType" as "PriceSeriesType",
            "OLD_PutOrCall" as "PutOrCall",
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_StrikePrice" as "StrikePrice",
            "OLD_Timestamp" as "Timestamp",
            "OLD__Exchange.MarketIdentifierCode" as "_Exchange.MarketIdentifierCode",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__CommodityReferencePrice.ReferencePriceID" as "_CommodityReferencePrice.ReferencePriceID",
            "OLD_Currency" as "Currency",
            "OLD_ListedDerivativeSymbol" as "ListedDerivativeSymbol",
            "OLD_ListedDerivativeTradingObservationCategory" as "ListedDerivativeTradingObservationCategory",
            "OLD_Price" as "Price",
            "OLD_PriceNotationForm" as "PriceNotationForm",
            "OLD_Quantity" as "Quantity",
            "OLD_StrikePriceCurrency" as "StrikePriceCurrency",
            "OLD_TickID" as "TickID",
            "OLD_Unit" as "Unit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."MaturityDate",
                        "OLD"."PriceDataProvider",
                        "OLD"."PriceSeriesType",
                        "OLD"."PutOrCall",
                        "OLD"."SequenceNumber",
                        "OLD"."StrikePrice",
                        "OLD"."Timestamp",
                        "OLD"."_Exchange.MarketIdentifierCode",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."MaturityDate" AS "OLD_MaturityDate" ,
                "OLD"."PriceDataProvider" AS "OLD_PriceDataProvider" ,
                "OLD"."PriceSeriesType" AS "OLD_PriceSeriesType" ,
                "OLD"."PutOrCall" AS "OLD_PutOrCall" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."StrikePrice" AS "OLD_StrikePrice" ,
                "OLD"."Timestamp" AS "OLD_Timestamp" ,
                "OLD"."_Exchange.MarketIdentifierCode" AS "OLD__Exchange.MarketIdentifierCode" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_CommodityReferencePrice.ReferencePriceID" AS "OLD__CommodityReferencePrice.ReferencePriceID" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."ListedDerivativeSymbol" AS "OLD_ListedDerivativeSymbol" ,
                "OLD"."ListedDerivativeTradingObservationCategory" AS "OLD_ListedDerivativeTradingObservationCategory" ,
                "OLD"."Price" AS "OLD_Price" ,
                "OLD"."PriceNotationForm" AS "OLD_PriceNotationForm" ,
                "OLD"."Quantity" AS "OLD_Quantity" ,
                "OLD"."StrikePriceCurrency" AS "OLD_StrikePriceCurrency" ,
                "OLD"."TickID" AS "OLD_TickID" ,
                "OLD"."Unit" AS "OLD_Unit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ListedDerivativeTradingObservation" as "OLD"
            on
               "IN"."MaturityDate" = "OLD"."MaturityDate" and
               "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
               "IN"."PriceSeriesType" = "OLD"."PriceSeriesType" and
               "IN"."PutOrCall" = "OLD"."PutOrCall" and
               "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
               "IN"."StrikePrice" = "OLD"."StrikePrice" and
               "IN"."Timestamp" = "OLD"."Timestamp" and
               "IN"."_Exchange.MarketIdentifierCode" = "OLD"."_Exchange.MarketIdentifierCode" and
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
