PROCEDURE "sap.fsdm.procedures::AddressEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::AddressTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::AddressTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::AddressTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AddressType" is null and
            "SequenceNumber" is null and
            "ASSOC_BankingChannel.BankingChannelID" is null and
            "ASSOC_BusinessPartnerID.BusinessPartnerID" is null and
            "ASSOC_OrganizationalUnit.IDSystem" is null and
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" is null and
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_PhysicalAsset.PhysicalAssetID" is null and
            "_Claim.ID" is null and
            "_Claim.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "AddressType" ,
                "SequenceNumber" ,
                "ASSOC_BankingChannel.BankingChannelID" ,
                "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "ASSOC_OrganizationalUnit.IDSystem" ,
                "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "_Claim.ID" ,
                "_Claim.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."AddressType" ,
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Claim.ID" ,
                "OLD"."_Claim.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Address" "OLD"
            on
                "IN"."AddressType" = "OLD"."AddressType" and
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_BankingChannel.BankingChannelID" and
                "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                "IN"."_Claim.ID" = "OLD"."_Claim.ID" and
                "IN"."_Claim.IDSystem" = "OLD"."_Claim.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "AddressType" ,
            "SequenceNumber" ,
            "ASSOC_BankingChannel.BankingChannelID" ,
            "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "ASSOC_OrganizationalUnit.IDSystem" ,
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "_Claim.ID" ,
            "_Claim.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."AddressType" ,
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Claim.ID" ,
                "OLD"."_Claim.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Address_Historical" "OLD"
            on
                "IN"."AddressType" = "OLD"."AddressType" and
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_BankingChannel.BankingChannelID" and
                "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_PhysicalAsset.PhysicalAssetID" and
                "IN"."_Claim.ID" = "OLD"."_Claim.ID" and
                "IN"."_Claim.IDSystem" = "OLD"."_Claim.IDSystem" 
        );

END
