PROCEDURE "sap.fsdm.procedures::StandardPaymentIntervalListEntryListDel" (IN ROW "sap.fsdm.tabletypes::StandardPaymentIntervalListEntryTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PeriodUntilPaymentIntervalEnd=' || TO_VARCHAR("PeriodUntilPaymentIntervalEnd") || ' ' ||
                'PeriodUntilPaymentIntervalStart=' || TO_VARCHAR("PeriodUntilPaymentIntervalStart") || ' ' ||
                'ASSOC_StandardPaymentIntervalList.SequenceNumber=' || TO_VARCHAR("ASSOC_StandardPaymentIntervalList.SequenceNumber") || ' ' ||
                'ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID=' || TO_VARCHAR("ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID") || ' ' ||
                'ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem=' || TO_VARCHAR("ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem") || ' ' ||
                'ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme=' || TO_VARCHAR("ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme") || ' ' ||
                'ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID=' || TO_VARCHAR("ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PeriodUntilPaymentIntervalEnd",
                        "IN"."PeriodUntilPaymentIntervalStart",
                        "IN"."ASSOC_StandardPaymentIntervalList.SequenceNumber",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PeriodUntilPaymentIntervalEnd",
                        "IN"."PeriodUntilPaymentIntervalStart",
                        "IN"."ASSOC_StandardPaymentIntervalList.SequenceNumber",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                        "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PeriodUntilPaymentIntervalEnd",
                        "PeriodUntilPaymentIntervalStart",
                        "ASSOC_StandardPaymentIntervalList.SequenceNumber",
                        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PeriodUntilPaymentIntervalEnd",
                                    "IN"."PeriodUntilPaymentIntervalStart",
                                    "IN"."ASSOC_StandardPaymentIntervalList.SequenceNumber",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PeriodUntilPaymentIntervalEnd",
                                    "IN"."PeriodUntilPaymentIntervalStart",
                                    "IN"."ASSOC_StandardPaymentIntervalList.SequenceNumber",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                                    "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::StandardPaymentIntervalListEntry" (
        "PeriodUntilPaymentIntervalEnd",
        "PeriodUntilPaymentIntervalStart",
        "ASSOC_StandardPaymentIntervalList.SequenceNumber",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PaymentAmount",
        "PaymentCurrency",
        "PaymentPercentage",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PeriodUntilPaymentIntervalEnd" as "PeriodUntilPaymentIntervalEnd" ,
            "OLD_PeriodUntilPaymentIntervalStart" as "PeriodUntilPaymentIntervalStart" ,
            "OLD_ASSOC_StandardPaymentIntervalList.SequenceNumber" as "ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_PaymentAmount" as "PaymentAmount" ,
            "OLD_PaymentCurrency" as "PaymentCurrency" ,
            "OLD_PaymentPercentage" as "PaymentPercentage" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PeriodUntilPaymentIntervalEnd" AS "OLD_PeriodUntilPaymentIntervalEnd" ,
                "OLD"."PeriodUntilPaymentIntervalStart" AS "OLD_PeriodUntilPaymentIntervalStart" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber" AS "OLD_ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PaymentAmount" AS "OLD_PaymentAmount" ,
                "OLD"."PaymentCurrency" AS "OLD_PaymentCurrency" ,
                "OLD"."PaymentPercentage" AS "OLD_PaymentPercentage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardPaymentIntervalListEntry" as "OLD"
            on
                      "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" and
                      "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" and
                      "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                      "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::StandardPaymentIntervalListEntry" (
        "PeriodUntilPaymentIntervalEnd",
        "PeriodUntilPaymentIntervalStart",
        "ASSOC_StandardPaymentIntervalList.SequenceNumber",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PaymentAmount",
        "PaymentCurrency",
        "PaymentPercentage",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PeriodUntilPaymentIntervalEnd" as "PeriodUntilPaymentIntervalEnd",
            "OLD_PeriodUntilPaymentIntervalStart" as "PeriodUntilPaymentIntervalStart",
            "OLD_ASSOC_StandardPaymentIntervalList.SequenceNumber" as "ASSOC_StandardPaymentIntervalList.SequenceNumber",
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
            "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PaymentAmount" as "PaymentAmount",
            "OLD_PaymentCurrency" as "PaymentCurrency",
            "OLD_PaymentPercentage" as "PaymentPercentage",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PeriodUntilPaymentIntervalEnd" AS "OLD_PeriodUntilPaymentIntervalEnd" ,
                "OLD"."PeriodUntilPaymentIntervalStart" AS "OLD_PeriodUntilPaymentIntervalStart" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber" AS "OLD_ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PaymentAmount" AS "OLD_PaymentAmount" ,
                "OLD"."PaymentCurrency" AS "OLD_PaymentCurrency" ,
                "OLD"."PaymentPercentage" AS "OLD_PaymentPercentage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardPaymentIntervalListEntry" as "OLD"
            on
                                                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" and
                                                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" and
                                                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                                                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::StandardPaymentIntervalListEntry"
    where (
        "PeriodUntilPaymentIntervalEnd",
        "PeriodUntilPaymentIntervalStart",
        "ASSOC_StandardPaymentIntervalList.SequenceNumber",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
        "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PeriodUntilPaymentIntervalEnd",
            "OLD"."PeriodUntilPaymentIntervalStart",
            "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber",
            "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID",
            "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem",
            "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme",
            "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::StandardPaymentIntervalListEntry" as "OLD"
        on
                                       "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" and
                                       "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" and
                                       "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                                       "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
