PROCEDURE "sap.fsdm.procedures::YieldCurveCompositionYieldCurveAssignmentDelete" (IN ROW "sap.fsdm.tabletypes::YieldCurveCompositionYieldCurveAssignmentTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_YieldCurve.ProviderOfYieldCurve=' || TO_VARCHAR("_YieldCurve.ProviderOfYieldCurve") || ' ' ||
                '_YieldCurve.YieldCurveID=' || TO_VARCHAR("_YieldCurve.YieldCurveID") || ' ' ||
                '_YieldCurveComposition.YieldCurveCompositionID=' || TO_VARCHAR("_YieldCurveComposition.YieldCurveCompositionID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_YieldCurve.ProviderOfYieldCurve",
                        "IN"."_YieldCurve.YieldCurveID",
                        "IN"."_YieldCurveComposition.YieldCurveCompositionID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_YieldCurve.ProviderOfYieldCurve",
                        "IN"."_YieldCurve.YieldCurveID",
                        "IN"."_YieldCurveComposition.YieldCurveCompositionID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_YieldCurve.ProviderOfYieldCurve",
                        "_YieldCurve.YieldCurveID",
                        "_YieldCurveComposition.YieldCurveCompositionID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_YieldCurve.ProviderOfYieldCurve",
                                    "IN"."_YieldCurve.YieldCurveID",
                                    "IN"."_YieldCurveComposition.YieldCurveCompositionID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_YieldCurve.ProviderOfYieldCurve",
                                    "IN"."_YieldCurve.YieldCurveID",
                                    "IN"."_YieldCurveComposition.YieldCurveCompositionID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_YieldCurve.ProviderOfYieldCurve" is null and
            "_YieldCurve.YieldCurveID" is null and
            "_YieldCurveComposition.YieldCurveCompositionID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::YieldCurveCompositionYieldCurveAssignment" (
        "_YieldCurve.ProviderOfYieldCurve",
        "_YieldCurve.YieldCurveID",
        "_YieldCurveComposition.YieldCurveCompositionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ComponentCurveIsSubtracted",
        "ComponentCurveWeight",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__YieldCurve.ProviderOfYieldCurve" as "_YieldCurve.ProviderOfYieldCurve" ,
            "OLD__YieldCurve.YieldCurveID" as "_YieldCurve.YieldCurveID" ,
            "OLD__YieldCurveComposition.YieldCurveCompositionID" as "_YieldCurveComposition.YieldCurveCompositionID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ComponentCurveIsSubtracted" as "ComponentCurveIsSubtracted" ,
            "OLD_ComponentCurveWeight" as "ComponentCurveWeight" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_YieldCurve.ProviderOfYieldCurve",
                        "OLD"."_YieldCurve.YieldCurveID",
                        "OLD"."_YieldCurveComposition.YieldCurveCompositionID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_YieldCurve.ProviderOfYieldCurve" AS "OLD__YieldCurve.ProviderOfYieldCurve" ,
                "OLD"."_YieldCurve.YieldCurveID" AS "OLD__YieldCurve.YieldCurveID" ,
                "OLD"."_YieldCurveComposition.YieldCurveCompositionID" AS "OLD__YieldCurveComposition.YieldCurveCompositionID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ComponentCurveIsSubtracted" AS "OLD_ComponentCurveIsSubtracted" ,
                "OLD"."ComponentCurveWeight" AS "OLD_ComponentCurveWeight" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::YieldCurveCompositionYieldCurveAssignment" as "OLD"
            on
                      "IN"."_YieldCurve.ProviderOfYieldCurve" = "OLD"."_YieldCurve.ProviderOfYieldCurve" and
                      "IN"."_YieldCurve.YieldCurveID" = "OLD"."_YieldCurve.YieldCurveID" and
                      "IN"."_YieldCurveComposition.YieldCurveCompositionID" = "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::YieldCurveCompositionYieldCurveAssignment" (
        "_YieldCurve.ProviderOfYieldCurve",
        "_YieldCurve.YieldCurveID",
        "_YieldCurveComposition.YieldCurveCompositionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ComponentCurveIsSubtracted",
        "ComponentCurveWeight",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__YieldCurve.ProviderOfYieldCurve" as "_YieldCurve.ProviderOfYieldCurve",
            "OLD__YieldCurve.YieldCurveID" as "_YieldCurve.YieldCurveID",
            "OLD__YieldCurveComposition.YieldCurveCompositionID" as "_YieldCurveComposition.YieldCurveCompositionID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ComponentCurveIsSubtracted" as "ComponentCurveIsSubtracted",
            "OLD_ComponentCurveWeight" as "ComponentCurveWeight",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_YieldCurve.ProviderOfYieldCurve",
                        "OLD"."_YieldCurve.YieldCurveID",
                        "OLD"."_YieldCurveComposition.YieldCurveCompositionID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_YieldCurve.ProviderOfYieldCurve" AS "OLD__YieldCurve.ProviderOfYieldCurve" ,
                "OLD"."_YieldCurve.YieldCurveID" AS "OLD__YieldCurve.YieldCurveID" ,
                "OLD"."_YieldCurveComposition.YieldCurveCompositionID" AS "OLD__YieldCurveComposition.YieldCurveCompositionID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ComponentCurveIsSubtracted" AS "OLD_ComponentCurveIsSubtracted" ,
                "OLD"."ComponentCurveWeight" AS "OLD_ComponentCurveWeight" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::YieldCurveCompositionYieldCurveAssignment" as "OLD"
            on
                                                "IN"."_YieldCurve.ProviderOfYieldCurve" = "OLD"."_YieldCurve.ProviderOfYieldCurve" and
                                                "IN"."_YieldCurve.YieldCurveID" = "OLD"."_YieldCurve.YieldCurveID" and
                                                "IN"."_YieldCurveComposition.YieldCurveCompositionID" = "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::YieldCurveCompositionYieldCurveAssignment"
    where (
        "_YieldCurve.ProviderOfYieldCurve",
        "_YieldCurve.YieldCurveID",
        "_YieldCurveComposition.YieldCurveCompositionID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_YieldCurve.ProviderOfYieldCurve",
            "OLD"."_YieldCurve.YieldCurveID",
            "OLD"."_YieldCurveComposition.YieldCurveCompositionID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::YieldCurveCompositionYieldCurveAssignment" as "OLD"
        on
                                       "IN"."_YieldCurve.ProviderOfYieldCurve" = "OLD"."_YieldCurve.ProviderOfYieldCurve" and
                                       "IN"."_YieldCurve.YieldCurveID" = "OLD"."_YieldCurve.YieldCurveID" and
                                       "IN"."_YieldCurveComposition.YieldCurveCompositionID" = "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
