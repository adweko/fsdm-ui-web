PROCEDURE "sap.fsdm.procedures::ScaleIntervalErase" (IN ROW "sap.fsdm.tabletypes::ScaleIntervalTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_FeeSpecification.SequenceNumber" is null and
            "ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" is null and
            "ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" is null and
            "ASSOC_FeeSpecification._LeaseService.ID" is null and
            "ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" is null and
            "ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" is null and
            "ASSOC_FeeSpecification._Trade.IDSystem" is null and
            "ASSOC_FeeSpecification._Trade.TradeID" is null and
            "ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" is null and
            "ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" is null and
            "ASSOC_InterestSpecification.SequenceNumber" is null and
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" is null and
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" is null and
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" is null and
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" is null and
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" is null and
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" is null and
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" is null and
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" is null and
            "ASSOC_InterestSpecification._Trade.IDSystem" is null and
            "ASSOC_InterestSpecification._Trade.TradeID" is null and
            "ASSOC_StandardFee.SequenceNumber" is null and
            "ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" is null and
            "ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" is null and
            "ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" is null and
            "ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" is null and
            "ASSOC_StandardInterest.SequenceNumber" is null and
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" is null and
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" is null and
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" is null and
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" is null and
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" is null and
            "ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" is null and
            "ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" is null and
            "ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" is null and
            "ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" is null and
            "_LeaseRate.SequenceNumber" is null and
            "_LeaseRate._LeaseAgreement.FinancialContractID" is null and
            "_LeaseRate._LeaseAgreement.IDSystem" is null and
            "_PnLParticipation.SequenceNumber" is null and
            "_PnLParticipation._ParticipationCert.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ScaleInterval"
        WHERE
        (            "ASSOC_FeeSpecification.SequenceNumber" ,
            "ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" ,
            "ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" ,
            "ASSOC_FeeSpecification._LeaseService.ID" ,
            "ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
            "ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" ,
            "ASSOC_FeeSpecification._Trade.IDSystem" ,
            "ASSOC_FeeSpecification._Trade.TradeID" ,
            "ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" ,
            "ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "ASSOC_InterestSpecification.SequenceNumber" ,
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
            "ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" ,
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" ,
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" ,
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
            "ASSOC_InterestSpecification._Trade.IDSystem" ,
            "ASSOC_InterestSpecification._Trade.TradeID" ,
            "ASSOC_StandardFee.SequenceNumber" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "ASSOC_StandardInterest.SequenceNumber" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "_LeaseRate.SequenceNumber" ,
            "_LeaseRate._LeaseAgreement.FinancialContractID" ,
            "_LeaseRate._LeaseAgreement.IDSystem" ,
            "_PnLParticipation.SequenceNumber" ,
            "_PnLParticipation._ParticipationCert.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."ASSOC_FeeSpecification.SequenceNumber" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."ASSOC_FeeSpecification._LeaseService.ID" ,
                "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification._Trade.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification._Trade.TradeID" ,
                "OLD"."ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification.SequenceNumber" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._Trade.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._Trade.TradeID" ,
                "OLD"."ASSOC_StandardFee.SequenceNumber" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."ASSOC_StandardInterest.SequenceNumber" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."_LeaseRate.SequenceNumber" ,
                "OLD"."_LeaseRate._LeaseAgreement.FinancialContractID" ,
                "OLD"."_LeaseRate._LeaseAgreement.IDSystem" ,
                "OLD"."_PnLParticipation.SequenceNumber" ,
                "OLD"."_PnLParticipation._ParticipationCert.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::ScaleInterval" "OLD"
            on
            "IN"."ASSOC_FeeSpecification.SequenceNumber" = "OLD"."ASSOC_FeeSpecification.SequenceNumber" and
            "IN"."ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" and
            "IN"."ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" and
            "IN"."ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" and
            "IN"."ASSOC_FeeSpecification._LeaseService.ID" = "OLD"."ASSOC_FeeSpecification._LeaseService.ID" and
            "IN"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" = "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" and
            "IN"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" = "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" and
            "IN"."ASSOC_FeeSpecification._Trade.IDSystem" = "OLD"."ASSOC_FeeSpecification._Trade.IDSystem" and
            "IN"."ASSOC_FeeSpecification._Trade.TradeID" = "OLD"."ASSOC_FeeSpecification._Trade.TradeID" and
            "IN"."ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" and
            "IN"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
            "IN"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" and
            "IN"."ASSOC_InterestSpecification.SequenceNumber" = "OLD"."ASSOC_InterestSpecification.SequenceNumber" and
            "IN"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" and
            "IN"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" and
            "IN"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" and
            "IN"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" and
            "IN"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" and
            "IN"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" = "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" and
            "IN"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" and
            "IN"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" and
            "IN"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" = "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" and
            "IN"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" and
            "IN"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" and
            "IN"."ASSOC_InterestSpecification._Trade.IDSystem" = "OLD"."ASSOC_InterestSpecification._Trade.IDSystem" and
            "IN"."ASSOC_InterestSpecification._Trade.TradeID" = "OLD"."ASSOC_InterestSpecification._Trade.TradeID" and
            "IN"."ASSOC_StandardFee.SequenceNumber" = "OLD"."ASSOC_StandardFee.SequenceNumber" and
            "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" and
            "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" and
            "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" and
            "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
            "IN"."ASSOC_StandardInterest.SequenceNumber" = "OLD"."ASSOC_StandardInterest.SequenceNumber" and
            "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" and
            "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" and
            "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" and
            "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" and
            "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" and
            "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" and
            "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" and
            "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" and
            "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
            "IN"."_LeaseRate.SequenceNumber" = "OLD"."_LeaseRate.SequenceNumber" and
            "IN"."_LeaseRate._LeaseAgreement.FinancialContractID" = "OLD"."_LeaseRate._LeaseAgreement.FinancialContractID" and
            "IN"."_LeaseRate._LeaseAgreement.IDSystem" = "OLD"."_LeaseRate._LeaseAgreement.IDSystem" and
            "IN"."_PnLParticipation.SequenceNumber" = "OLD"."_PnLParticipation.SequenceNumber" and
            "IN"."_PnLParticipation._ParticipationCert.FinancialInstrumentID" = "OLD"."_PnLParticipation._ParticipationCert.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ScaleInterval_Historical"
        WHERE
        (
            "ASSOC_FeeSpecification.SequenceNumber" ,
            "ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" ,
            "ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" ,
            "ASSOC_FeeSpecification._LeaseService.ID" ,
            "ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
            "ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" ,
            "ASSOC_FeeSpecification._Trade.IDSystem" ,
            "ASSOC_FeeSpecification._Trade.TradeID" ,
            "ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" ,
            "ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "ASSOC_InterestSpecification.SequenceNumber" ,
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
            "ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" ,
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" ,
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
            "ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" ,
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
            "ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
            "ASSOC_InterestSpecification._Trade.IDSystem" ,
            "ASSOC_InterestSpecification._Trade.TradeID" ,
            "ASSOC_StandardFee.SequenceNumber" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" ,
            "ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "ASSOC_StandardInterest.SequenceNumber" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" ,
            "ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" ,
            "ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "_LeaseRate.SequenceNumber" ,
            "_LeaseRate._LeaseAgreement.FinancialContractID" ,
            "_LeaseRate._LeaseAgreement.IDSystem" ,
            "_PnLParticipation.SequenceNumber" ,
            "_PnLParticipation._ParticipationCert.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."ASSOC_FeeSpecification.SequenceNumber" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."ASSOC_FeeSpecification._LeaseService.ID" ,
                "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification._Trade.IDSystem" ,
                "OLD"."ASSOC_FeeSpecification._Trade.TradeID" ,
                "OLD"."ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification.SequenceNumber" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._Trade.IDSystem" ,
                "OLD"."ASSOC_InterestSpecification._Trade.TradeID" ,
                "OLD"."ASSOC_StandardFee.SequenceNumber" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."ASSOC_StandardInterest.SequenceNumber" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."_LeaseRate.SequenceNumber" ,
                "OLD"."_LeaseRate._LeaseAgreement.FinancialContractID" ,
                "OLD"."_LeaseRate._LeaseAgreement.IDSystem" ,
                "OLD"."_PnLParticipation.SequenceNumber" ,
                "OLD"."_PnLParticipation._ParticipationCert.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::ScaleInterval_Historical" "OLD"
            on
                "IN"."ASSOC_FeeSpecification.SequenceNumber" = "OLD"."ASSOC_FeeSpecification.SequenceNumber" and
                "IN"."ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FeeSpecification.ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.IDSystem" and
                "IN"."ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_FeeSpecification.ASSOC_TransferOrder.TransferOrderID" and
                "IN"."ASSOC_FeeSpecification._LeaseService.ID" = "OLD"."ASSOC_FeeSpecification._LeaseService.ID" and
                "IN"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" = "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.FinancialContractID" and
                "IN"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" = "OLD"."ASSOC_FeeSpecification._LeaseService._LeaseServiceAgreement.IDSystem" and
                "IN"."ASSOC_FeeSpecification._Trade.IDSystem" = "OLD"."ASSOC_FeeSpecification._Trade.IDSystem" and
                "IN"."ASSOC_FeeSpecification._Trade.TradeID" = "OLD"."ASSOC_FeeSpecification._Trade.TradeID" and
                "IN"."ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."ASSOC_FeeSpecification._TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."ASSOC_FeeSpecification._TrancheInSyndication._SyndicationAgreement.IDSystem" and
                "IN"."ASSOC_InterestSpecification.SequenceNumber" = "OLD"."ASSOC_InterestSpecification.SequenceNumber" and
                "IN"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" = "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.PositionCurrency" and
                "IN"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_InterestSpecification.ASSOC_CcyOfMultiCcyAccnt.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_InterestSpecification.ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_InterestSpecification._DebtInstrument.FinancialInstrumentID" and
                "IN"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" = "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification.ComponentNumber" and
                "IN"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.FinancialContractID" and
                "IN"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" = "OLD"."ASSOC_InterestSpecification._OptionOfReferenceRateSpecification._InterestRateOption.IDSystem" and
                "IN"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" = "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification.ComponentNumber" and
                "IN"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.FinancialContractID" and
                "IN"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" = "OLD"."ASSOC_InterestSpecification._OptionOfStrikeSpecification._InterestRateOption.IDSystem" and
                "IN"."ASSOC_InterestSpecification._Trade.IDSystem" = "OLD"."ASSOC_InterestSpecification._Trade.IDSystem" and
                "IN"."ASSOC_InterestSpecification._Trade.TradeID" = "OLD"."ASSOC_InterestSpecification._Trade.TradeID" and
                "IN"."ASSOC_StandardFee.SequenceNumber" = "OLD"."ASSOC_StandardFee.SequenceNumber" and
                "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.FinancialStandardProductID" and
                "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.IDSystem" and
                "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.PricingScheme" and
                "IN"."ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardFee.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
                "IN"."ASSOC_StandardInterest.SequenceNumber" = "OLD"."ASSOC_StandardInterest.SequenceNumber" and
                "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.PositionCurrency" and
                "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.FinancialStandardProductID" and
                "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.IDSystem" and
                "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.PricingScheme" and
                "IN"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardInterest.ASSOC_PosCcyOfStdAcc.ASSOC_StdMultiCcyAcct.ASSOC_Company.BusinessPartnerID" and
                "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.FinancialStandardProductID" and
                "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.IDSystem" and
                "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.PricingScheme" and
                "IN"."ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardInterest.ASSOC_StandardProduct.ASSOC_Company.BusinessPartnerID" and
                "IN"."_LeaseRate.SequenceNumber" = "OLD"."_LeaseRate.SequenceNumber" and
                "IN"."_LeaseRate._LeaseAgreement.FinancialContractID" = "OLD"."_LeaseRate._LeaseAgreement.FinancialContractID" and
                "IN"."_LeaseRate._LeaseAgreement.IDSystem" = "OLD"."_LeaseRate._LeaseAgreement.IDSystem" and
                "IN"."_PnLParticipation.SequenceNumber" = "OLD"."_PnLParticipation.SequenceNumber" and
                "IN"."_PnLParticipation._ParticipationCert.FinancialInstrumentID" = "OLD"."_PnLParticipation._ParticipationCert.FinancialInstrumentID" 
        );

END
