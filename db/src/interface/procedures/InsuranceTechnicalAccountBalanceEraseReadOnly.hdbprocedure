PROCEDURE "sap.fsdm.procedures::InsuranceTechnicalAccountBalanceEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InsuranceTechnicalAccountBalanceTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InsuranceTechnicalAccountBalanceTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InsuranceTechnicalAccountBalanceTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Type" is null and
            "_Coverage.ID" is null and
            "_Coverage._InsuranceContract.FinancialContractID" is null and
            "_Coverage._InsuranceContract.IDSystem" is null and
            "_Coverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_Coverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null and
            "_Option.Type" is null and
            "_Option._InsuranceCoverage.ID" is null and
            "_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_Option._InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Type" ,
                "_Coverage.ID" ,
                "_Coverage._InsuranceContract.FinancialContractID" ,
                "_Coverage._InsuranceContract.IDSystem" ,
                "_Coverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "_Coverage._InsuredObject._FinancialContract.IDSystem" ,
                "_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "_Option.Type" ,
                "_Option._InsuranceCoverage.ID" ,
                "_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "_Option._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Type" ,
                "OLD"."_Coverage.ID" ,
                "OLD"."_Coverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Coverage._InsuranceContract.IDSystem" ,
                "OLD"."_Coverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Coverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Option.Type" ,
                "OLD"."_Option._InsuranceCoverage.ID" ,
                "OLD"."_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Option._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InsuranceTechnicalAccountBalance" "OLD"
            on
                "IN"."Type" = "OLD"."Type" and
                "IN"."_Coverage.ID" = "OLD"."_Coverage.ID" and
                "IN"."_Coverage._InsuranceContract.FinancialContractID" = "OLD"."_Coverage._InsuranceContract.FinancialContractID" and
                "IN"."_Coverage._InsuranceContract.IDSystem" = "OLD"."_Coverage._InsuranceContract.IDSystem" and
                "IN"."_Coverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Coverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_Coverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Coverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" and
                "IN"."_Option.Type" = "OLD"."_Option.Type" and
                "IN"."_Option._InsuranceCoverage.ID" = "OLD"."_Option._InsuranceCoverage.ID" and
                "IN"."_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_Option._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Option._InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Type" ,
            "_Coverage.ID" ,
            "_Coverage._InsuranceContract.FinancialContractID" ,
            "_Coverage._InsuranceContract.IDSystem" ,
            "_Coverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_Coverage._InsuredObject._FinancialContract.IDSystem" ,
            "_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "_Option.Type" ,
            "_Option._InsuranceCoverage.ID" ,
            "_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_Option._InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Type" ,
                "OLD"."_Coverage.ID" ,
                "OLD"."_Coverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Coverage._InsuranceContract.IDSystem" ,
                "OLD"."_Coverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Coverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Option.Type" ,
                "OLD"."_Option._InsuranceCoverage.ID" ,
                "OLD"."_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_Option._InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InsuranceTechnicalAccountBalance_Historical" "OLD"
            on
                "IN"."Type" = "OLD"."Type" and
                "IN"."_Coverage.ID" = "OLD"."_Coverage.ID" and
                "IN"."_Coverage._InsuranceContract.FinancialContractID" = "OLD"."_Coverage._InsuranceContract.FinancialContractID" and
                "IN"."_Coverage._InsuranceContract.IDSystem" = "OLD"."_Coverage._InsuranceContract.IDSystem" and
                "IN"."_Coverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Coverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_Coverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Coverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Coverage._InsuredObject._PhysicalAsset.PhysicalAssetID" and
                "IN"."_Option.Type" = "OLD"."_Option.Type" and
                "IN"."_Option._InsuranceCoverage.ID" = "OLD"."_Option._InsuranceCoverage.ID" and
                "IN"."_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_Option._InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_Option._InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_Option._InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_Option._InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_Option._InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
