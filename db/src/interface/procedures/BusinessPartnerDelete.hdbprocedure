PROCEDURE "sap.fsdm.procedures::BusinessPartnerDelete" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BusinessPartnerID=' || TO_VARCHAR("BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::BusinessPartner" (
        "BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_RepresentedGeographicalUnit.GeographicalStructureID",
        "_RepresentedGeographicalUnit.GeographicalUnitID",
        "AristocraticTitle",
        "BusinessPartnerCategory",
        "ChildrenAtHomeIndicator",
        "ConcentrationFactor",
        "Consultation",
        "ContributionsFromAllClearingMembers",
        "CountryOfBirth",
        "CountryOfFoundation",
        "CountryOfIncorporation",
        "CountryOfMainEconomicActivities",
        "DateOfBirth",
        "DateOfDeath",
        "DateOfFoundation",
        "DateOfIncorporation",
        "DeceasedFlag",
        "DefaultFundCurrency",
        "EducationLevel",
        "EmployeeID",
        "EmploymentDate",
        "FundType",
        "Gender",
        "GivenName",
        "GlobalSystemRelevantFinancialInstitution",
        "GroupCategory",
        "GroupType",
        "HouseHoldGrossIncome",
        "HouseHoldGrossIncomeCurrency",
        "HouseHoldGrossIncomeTimeUnit",
        "HouseHoldNetIncome",
        "HouseHoldNetIncomeCurrency",
        "HouseHoldNetIncomeTimeUnit",
        "HypotheticalCapitalRequirementsOfCCP",
        "IndividualCategory",
        "InstitutionalProtectionScheme",
        "IsCompany",
        "IsHomeOwner",
        "IsPoliticallyExposedPerson",
        "JobTitle",
        "JointLiabilityType",
        "LastName",
        "LeavingDate",
        "LegalEntityUnderPublicLawType",
        "LegalForm",
        "LifecycleStatus",
        "LifecycleStatusChangeDate",
        "LifecycleStatusReason",
        "LiquidationDate",
        "MaritalStatus",
        "MiddleName",
        "MinorToMajorDate",
        "Name",
        "NameAtBirth",
        "NameSupplement",
        "Nationality",
        "NumberHouseholdMembers",
        "NumberOfClearingMembers",
        "OrganizationCategory",
        "PlaceOfBirth",
        "PlaceOfRegister",
        "PreferredCorrespondenceLanguage",
        "PrefundedOwnResourcesOfCCP",
        "QualifiedCCP",
        "Register",
        "RegulatedFinancialInstitution",
        "RelationshipStartDate",
        "ReligiousHonorific",
        "ResidingCountry",
        "ResidingSince",
        "ResidingStatus",
        "RiskCountry",
        "RoleInClearing",
        "SecondNationality",
        "ShadowBank",
        "ShortName",
        "SmokerIndicator",
        "Title",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_BusinessPartnerID" as "BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__RepresentedGeographicalUnit.GeographicalStructureID" as "_RepresentedGeographicalUnit.GeographicalStructureID" ,
            "OLD__RepresentedGeographicalUnit.GeographicalUnitID" as "_RepresentedGeographicalUnit.GeographicalUnitID" ,
            "OLD_AristocraticTitle" as "AristocraticTitle" ,
            "OLD_BusinessPartnerCategory" as "BusinessPartnerCategory" ,
            "OLD_ChildrenAtHomeIndicator" as "ChildrenAtHomeIndicator" ,
            "OLD_ConcentrationFactor" as "ConcentrationFactor" ,
            "OLD_Consultation" as "Consultation" ,
            "OLD_ContributionsFromAllClearingMembers" as "ContributionsFromAllClearingMembers" ,
            "OLD_CountryOfBirth" as "CountryOfBirth" ,
            "OLD_CountryOfFoundation" as "CountryOfFoundation" ,
            "OLD_CountryOfIncorporation" as "CountryOfIncorporation" ,
            "OLD_CountryOfMainEconomicActivities" as "CountryOfMainEconomicActivities" ,
            "OLD_DateOfBirth" as "DateOfBirth" ,
            "OLD_DateOfDeath" as "DateOfDeath" ,
            "OLD_DateOfFoundation" as "DateOfFoundation" ,
            "OLD_DateOfIncorporation" as "DateOfIncorporation" ,
            "OLD_DeceasedFlag" as "DeceasedFlag" ,
            "OLD_DefaultFundCurrency" as "DefaultFundCurrency" ,
            "OLD_EducationLevel" as "EducationLevel" ,
            "OLD_EmployeeID" as "EmployeeID" ,
            "OLD_EmploymentDate" as "EmploymentDate" ,
            "OLD_FundType" as "FundType" ,
            "OLD_Gender" as "Gender" ,
            "OLD_GivenName" as "GivenName" ,
            "OLD_GlobalSystemRelevantFinancialInstitution" as "GlobalSystemRelevantFinancialInstitution" ,
            "OLD_GroupCategory" as "GroupCategory" ,
            "OLD_GroupType" as "GroupType" ,
            "OLD_HouseHoldGrossIncome" as "HouseHoldGrossIncome" ,
            "OLD_HouseHoldGrossIncomeCurrency" as "HouseHoldGrossIncomeCurrency" ,
            "OLD_HouseHoldGrossIncomeTimeUnit" as "HouseHoldGrossIncomeTimeUnit" ,
            "OLD_HouseHoldNetIncome" as "HouseHoldNetIncome" ,
            "OLD_HouseHoldNetIncomeCurrency" as "HouseHoldNetIncomeCurrency" ,
            "OLD_HouseHoldNetIncomeTimeUnit" as "HouseHoldNetIncomeTimeUnit" ,
            "OLD_HypotheticalCapitalRequirementsOfCCP" as "HypotheticalCapitalRequirementsOfCCP" ,
            "OLD_IndividualCategory" as "IndividualCategory" ,
            "OLD_InstitutionalProtectionScheme" as "InstitutionalProtectionScheme" ,
            "OLD_IsCompany" as "IsCompany" ,
            "OLD_IsHomeOwner" as "IsHomeOwner" ,
            "OLD_IsPoliticallyExposedPerson" as "IsPoliticallyExposedPerson" ,
            "OLD_JobTitle" as "JobTitle" ,
            "OLD_JointLiabilityType" as "JointLiabilityType" ,
            "OLD_LastName" as "LastName" ,
            "OLD_LeavingDate" as "LeavingDate" ,
            "OLD_LegalEntityUnderPublicLawType" as "LegalEntityUnderPublicLawType" ,
            "OLD_LegalForm" as "LegalForm" ,
            "OLD_LifecycleStatus" as "LifecycleStatus" ,
            "OLD_LifecycleStatusChangeDate" as "LifecycleStatusChangeDate" ,
            "OLD_LifecycleStatusReason" as "LifecycleStatusReason" ,
            "OLD_LiquidationDate" as "LiquidationDate" ,
            "OLD_MaritalStatus" as "MaritalStatus" ,
            "OLD_MiddleName" as "MiddleName" ,
            "OLD_MinorToMajorDate" as "MinorToMajorDate" ,
            "OLD_Name" as "Name" ,
            "OLD_NameAtBirth" as "NameAtBirth" ,
            "OLD_NameSupplement" as "NameSupplement" ,
            "OLD_Nationality" as "Nationality" ,
            "OLD_NumberHouseholdMembers" as "NumberHouseholdMembers" ,
            "OLD_NumberOfClearingMembers" as "NumberOfClearingMembers" ,
            "OLD_OrganizationCategory" as "OrganizationCategory" ,
            "OLD_PlaceOfBirth" as "PlaceOfBirth" ,
            "OLD_PlaceOfRegister" as "PlaceOfRegister" ,
            "OLD_PreferredCorrespondenceLanguage" as "PreferredCorrespondenceLanguage" ,
            "OLD_PrefundedOwnResourcesOfCCP" as "PrefundedOwnResourcesOfCCP" ,
            "OLD_QualifiedCCP" as "QualifiedCCP" ,
            "OLD_Register" as "Register" ,
            "OLD_RegulatedFinancialInstitution" as "RegulatedFinancialInstitution" ,
            "OLD_RelationshipStartDate" as "RelationshipStartDate" ,
            "OLD_ReligiousHonorific" as "ReligiousHonorific" ,
            "OLD_ResidingCountry" as "ResidingCountry" ,
            "OLD_ResidingSince" as "ResidingSince" ,
            "OLD_ResidingStatus" as "ResidingStatus" ,
            "OLD_RiskCountry" as "RiskCountry" ,
            "OLD_RoleInClearing" as "RoleInClearing" ,
            "OLD_SecondNationality" as "SecondNationality" ,
            "OLD_ShadowBank" as "ShadowBank" ,
            "OLD_ShortName" as "ShortName" ,
            "OLD_SmokerIndicator" as "SmokerIndicator" ,
            "OLD_Title" as "Title" ,
            "OLD_Type" as "Type" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."BusinessPartnerID" AS "OLD_BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_RepresentedGeographicalUnit.GeographicalStructureID" AS "OLD__RepresentedGeographicalUnit.GeographicalStructureID" ,
                "OLD"."_RepresentedGeographicalUnit.GeographicalUnitID" AS "OLD__RepresentedGeographicalUnit.GeographicalUnitID" ,
                "OLD"."AristocraticTitle" AS "OLD_AristocraticTitle" ,
                "OLD"."BusinessPartnerCategory" AS "OLD_BusinessPartnerCategory" ,
                "OLD"."ChildrenAtHomeIndicator" AS "OLD_ChildrenAtHomeIndicator" ,
                "OLD"."ConcentrationFactor" AS "OLD_ConcentrationFactor" ,
                "OLD"."Consultation" AS "OLD_Consultation" ,
                "OLD"."ContributionsFromAllClearingMembers" AS "OLD_ContributionsFromAllClearingMembers" ,
                "OLD"."CountryOfBirth" AS "OLD_CountryOfBirth" ,
                "OLD"."CountryOfFoundation" AS "OLD_CountryOfFoundation" ,
                "OLD"."CountryOfIncorporation" AS "OLD_CountryOfIncorporation" ,
                "OLD"."CountryOfMainEconomicActivities" AS "OLD_CountryOfMainEconomicActivities" ,
                "OLD"."DateOfBirth" AS "OLD_DateOfBirth" ,
                "OLD"."DateOfDeath" AS "OLD_DateOfDeath" ,
                "OLD"."DateOfFoundation" AS "OLD_DateOfFoundation" ,
                "OLD"."DateOfIncorporation" AS "OLD_DateOfIncorporation" ,
                "OLD"."DeceasedFlag" AS "OLD_DeceasedFlag" ,
                "OLD"."DefaultFundCurrency" AS "OLD_DefaultFundCurrency" ,
                "OLD"."EducationLevel" AS "OLD_EducationLevel" ,
                "OLD"."EmployeeID" AS "OLD_EmployeeID" ,
                "OLD"."EmploymentDate" AS "OLD_EmploymentDate" ,
                "OLD"."FundType" AS "OLD_FundType" ,
                "OLD"."Gender" AS "OLD_Gender" ,
                "OLD"."GivenName" AS "OLD_GivenName" ,
                "OLD"."GlobalSystemRelevantFinancialInstitution" AS "OLD_GlobalSystemRelevantFinancialInstitution" ,
                "OLD"."GroupCategory" AS "OLD_GroupCategory" ,
                "OLD"."GroupType" AS "OLD_GroupType" ,
                "OLD"."HouseHoldGrossIncome" AS "OLD_HouseHoldGrossIncome" ,
                "OLD"."HouseHoldGrossIncomeCurrency" AS "OLD_HouseHoldGrossIncomeCurrency" ,
                "OLD"."HouseHoldGrossIncomeTimeUnit" AS "OLD_HouseHoldGrossIncomeTimeUnit" ,
                "OLD"."HouseHoldNetIncome" AS "OLD_HouseHoldNetIncome" ,
                "OLD"."HouseHoldNetIncomeCurrency" AS "OLD_HouseHoldNetIncomeCurrency" ,
                "OLD"."HouseHoldNetIncomeTimeUnit" AS "OLD_HouseHoldNetIncomeTimeUnit" ,
                "OLD"."HypotheticalCapitalRequirementsOfCCP" AS "OLD_HypotheticalCapitalRequirementsOfCCP" ,
                "OLD"."IndividualCategory" AS "OLD_IndividualCategory" ,
                "OLD"."InstitutionalProtectionScheme" AS "OLD_InstitutionalProtectionScheme" ,
                "OLD"."IsCompany" AS "OLD_IsCompany" ,
                "OLD"."IsHomeOwner" AS "OLD_IsHomeOwner" ,
                "OLD"."IsPoliticallyExposedPerson" AS "OLD_IsPoliticallyExposedPerson" ,
                "OLD"."JobTitle" AS "OLD_JobTitle" ,
                "OLD"."JointLiabilityType" AS "OLD_JointLiabilityType" ,
                "OLD"."LastName" AS "OLD_LastName" ,
                "OLD"."LeavingDate" AS "OLD_LeavingDate" ,
                "OLD"."LegalEntityUnderPublicLawType" AS "OLD_LegalEntityUnderPublicLawType" ,
                "OLD"."LegalForm" AS "OLD_LegalForm" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."LifecycleStatusChangeDate" AS "OLD_LifecycleStatusChangeDate" ,
                "OLD"."LifecycleStatusReason" AS "OLD_LifecycleStatusReason" ,
                "OLD"."LiquidationDate" AS "OLD_LiquidationDate" ,
                "OLD"."MaritalStatus" AS "OLD_MaritalStatus" ,
                "OLD"."MiddleName" AS "OLD_MiddleName" ,
                "OLD"."MinorToMajorDate" AS "OLD_MinorToMajorDate" ,
                "OLD"."Name" AS "OLD_Name" ,
                "OLD"."NameAtBirth" AS "OLD_NameAtBirth" ,
                "OLD"."NameSupplement" AS "OLD_NameSupplement" ,
                "OLD"."Nationality" AS "OLD_Nationality" ,
                "OLD"."NumberHouseholdMembers" AS "OLD_NumberHouseholdMembers" ,
                "OLD"."NumberOfClearingMembers" AS "OLD_NumberOfClearingMembers" ,
                "OLD"."OrganizationCategory" AS "OLD_OrganizationCategory" ,
                "OLD"."PlaceOfBirth" AS "OLD_PlaceOfBirth" ,
                "OLD"."PlaceOfRegister" AS "OLD_PlaceOfRegister" ,
                "OLD"."PreferredCorrespondenceLanguage" AS "OLD_PreferredCorrespondenceLanguage" ,
                "OLD"."PrefundedOwnResourcesOfCCP" AS "OLD_PrefundedOwnResourcesOfCCP" ,
                "OLD"."QualifiedCCP" AS "OLD_QualifiedCCP" ,
                "OLD"."Register" AS "OLD_Register" ,
                "OLD"."RegulatedFinancialInstitution" AS "OLD_RegulatedFinancialInstitution" ,
                "OLD"."RelationshipStartDate" AS "OLD_RelationshipStartDate" ,
                "OLD"."ReligiousHonorific" AS "OLD_ReligiousHonorific" ,
                "OLD"."ResidingCountry" AS "OLD_ResidingCountry" ,
                "OLD"."ResidingSince" AS "OLD_ResidingSince" ,
                "OLD"."ResidingStatus" AS "OLD_ResidingStatus" ,
                "OLD"."RiskCountry" AS "OLD_RiskCountry" ,
                "OLD"."RoleInClearing" AS "OLD_RoleInClearing" ,
                "OLD"."SecondNationality" AS "OLD_SecondNationality" ,
                "OLD"."ShadowBank" AS "OLD_ShadowBank" ,
                "OLD"."ShortName" AS "OLD_ShortName" ,
                "OLD"."SmokerIndicator" AS "OLD_SmokerIndicator" ,
                "OLD"."Title" AS "OLD_Title" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartner" as "OLD"
            on
                      "IN"."BusinessPartnerID" = "OLD"."BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::BusinessPartner" (
        "BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_RepresentedGeographicalUnit.GeographicalStructureID",
        "_RepresentedGeographicalUnit.GeographicalUnitID",
        "AristocraticTitle",
        "BusinessPartnerCategory",
        "ChildrenAtHomeIndicator",
        "ConcentrationFactor",
        "Consultation",
        "ContributionsFromAllClearingMembers",
        "CountryOfBirth",
        "CountryOfFoundation",
        "CountryOfIncorporation",
        "CountryOfMainEconomicActivities",
        "DateOfBirth",
        "DateOfDeath",
        "DateOfFoundation",
        "DateOfIncorporation",
        "DeceasedFlag",
        "DefaultFundCurrency",
        "EducationLevel",
        "EmployeeID",
        "EmploymentDate",
        "FundType",
        "Gender",
        "GivenName",
        "GlobalSystemRelevantFinancialInstitution",
        "GroupCategory",
        "GroupType",
        "HouseHoldGrossIncome",
        "HouseHoldGrossIncomeCurrency",
        "HouseHoldGrossIncomeTimeUnit",
        "HouseHoldNetIncome",
        "HouseHoldNetIncomeCurrency",
        "HouseHoldNetIncomeTimeUnit",
        "HypotheticalCapitalRequirementsOfCCP",
        "IndividualCategory",
        "InstitutionalProtectionScheme",
        "IsCompany",
        "IsHomeOwner",
        "IsPoliticallyExposedPerson",
        "JobTitle",
        "JointLiabilityType",
        "LastName",
        "LeavingDate",
        "LegalEntityUnderPublicLawType",
        "LegalForm",
        "LifecycleStatus",
        "LifecycleStatusChangeDate",
        "LifecycleStatusReason",
        "LiquidationDate",
        "MaritalStatus",
        "MiddleName",
        "MinorToMajorDate",
        "Name",
        "NameAtBirth",
        "NameSupplement",
        "Nationality",
        "NumberHouseholdMembers",
        "NumberOfClearingMembers",
        "OrganizationCategory",
        "PlaceOfBirth",
        "PlaceOfRegister",
        "PreferredCorrespondenceLanguage",
        "PrefundedOwnResourcesOfCCP",
        "QualifiedCCP",
        "Register",
        "RegulatedFinancialInstitution",
        "RelationshipStartDate",
        "ReligiousHonorific",
        "ResidingCountry",
        "ResidingSince",
        "ResidingStatus",
        "RiskCountry",
        "RoleInClearing",
        "SecondNationality",
        "ShadowBank",
        "ShortName",
        "SmokerIndicator",
        "Title",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_BusinessPartnerID" as "BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__RepresentedGeographicalUnit.GeographicalStructureID" as "_RepresentedGeographicalUnit.GeographicalStructureID",
            "OLD__RepresentedGeographicalUnit.GeographicalUnitID" as "_RepresentedGeographicalUnit.GeographicalUnitID",
            "OLD_AristocraticTitle" as "AristocraticTitle",
            "OLD_BusinessPartnerCategory" as "BusinessPartnerCategory",
            "OLD_ChildrenAtHomeIndicator" as "ChildrenAtHomeIndicator",
            "OLD_ConcentrationFactor" as "ConcentrationFactor",
            "OLD_Consultation" as "Consultation",
            "OLD_ContributionsFromAllClearingMembers" as "ContributionsFromAllClearingMembers",
            "OLD_CountryOfBirth" as "CountryOfBirth",
            "OLD_CountryOfFoundation" as "CountryOfFoundation",
            "OLD_CountryOfIncorporation" as "CountryOfIncorporation",
            "OLD_CountryOfMainEconomicActivities" as "CountryOfMainEconomicActivities",
            "OLD_DateOfBirth" as "DateOfBirth",
            "OLD_DateOfDeath" as "DateOfDeath",
            "OLD_DateOfFoundation" as "DateOfFoundation",
            "OLD_DateOfIncorporation" as "DateOfIncorporation",
            "OLD_DeceasedFlag" as "DeceasedFlag",
            "OLD_DefaultFundCurrency" as "DefaultFundCurrency",
            "OLD_EducationLevel" as "EducationLevel",
            "OLD_EmployeeID" as "EmployeeID",
            "OLD_EmploymentDate" as "EmploymentDate",
            "OLD_FundType" as "FundType",
            "OLD_Gender" as "Gender",
            "OLD_GivenName" as "GivenName",
            "OLD_GlobalSystemRelevantFinancialInstitution" as "GlobalSystemRelevantFinancialInstitution",
            "OLD_GroupCategory" as "GroupCategory",
            "OLD_GroupType" as "GroupType",
            "OLD_HouseHoldGrossIncome" as "HouseHoldGrossIncome",
            "OLD_HouseHoldGrossIncomeCurrency" as "HouseHoldGrossIncomeCurrency",
            "OLD_HouseHoldGrossIncomeTimeUnit" as "HouseHoldGrossIncomeTimeUnit",
            "OLD_HouseHoldNetIncome" as "HouseHoldNetIncome",
            "OLD_HouseHoldNetIncomeCurrency" as "HouseHoldNetIncomeCurrency",
            "OLD_HouseHoldNetIncomeTimeUnit" as "HouseHoldNetIncomeTimeUnit",
            "OLD_HypotheticalCapitalRequirementsOfCCP" as "HypotheticalCapitalRequirementsOfCCP",
            "OLD_IndividualCategory" as "IndividualCategory",
            "OLD_InstitutionalProtectionScheme" as "InstitutionalProtectionScheme",
            "OLD_IsCompany" as "IsCompany",
            "OLD_IsHomeOwner" as "IsHomeOwner",
            "OLD_IsPoliticallyExposedPerson" as "IsPoliticallyExposedPerson",
            "OLD_JobTitle" as "JobTitle",
            "OLD_JointLiabilityType" as "JointLiabilityType",
            "OLD_LastName" as "LastName",
            "OLD_LeavingDate" as "LeavingDate",
            "OLD_LegalEntityUnderPublicLawType" as "LegalEntityUnderPublicLawType",
            "OLD_LegalForm" as "LegalForm",
            "OLD_LifecycleStatus" as "LifecycleStatus",
            "OLD_LifecycleStatusChangeDate" as "LifecycleStatusChangeDate",
            "OLD_LifecycleStatusReason" as "LifecycleStatusReason",
            "OLD_LiquidationDate" as "LiquidationDate",
            "OLD_MaritalStatus" as "MaritalStatus",
            "OLD_MiddleName" as "MiddleName",
            "OLD_MinorToMajorDate" as "MinorToMajorDate",
            "OLD_Name" as "Name",
            "OLD_NameAtBirth" as "NameAtBirth",
            "OLD_NameSupplement" as "NameSupplement",
            "OLD_Nationality" as "Nationality",
            "OLD_NumberHouseholdMembers" as "NumberHouseholdMembers",
            "OLD_NumberOfClearingMembers" as "NumberOfClearingMembers",
            "OLD_OrganizationCategory" as "OrganizationCategory",
            "OLD_PlaceOfBirth" as "PlaceOfBirth",
            "OLD_PlaceOfRegister" as "PlaceOfRegister",
            "OLD_PreferredCorrespondenceLanguage" as "PreferredCorrespondenceLanguage",
            "OLD_PrefundedOwnResourcesOfCCP" as "PrefundedOwnResourcesOfCCP",
            "OLD_QualifiedCCP" as "QualifiedCCP",
            "OLD_Register" as "Register",
            "OLD_RegulatedFinancialInstitution" as "RegulatedFinancialInstitution",
            "OLD_RelationshipStartDate" as "RelationshipStartDate",
            "OLD_ReligiousHonorific" as "ReligiousHonorific",
            "OLD_ResidingCountry" as "ResidingCountry",
            "OLD_ResidingSince" as "ResidingSince",
            "OLD_ResidingStatus" as "ResidingStatus",
            "OLD_RiskCountry" as "RiskCountry",
            "OLD_RoleInClearing" as "RoleInClearing",
            "OLD_SecondNationality" as "SecondNationality",
            "OLD_ShadowBank" as "ShadowBank",
            "OLD_ShortName" as "ShortName",
            "OLD_SmokerIndicator" as "SmokerIndicator",
            "OLD_Title" as "Title",
            "OLD_Type" as "Type",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."BusinessPartnerID" AS "OLD_BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_RepresentedGeographicalUnit.GeographicalStructureID" AS "OLD__RepresentedGeographicalUnit.GeographicalStructureID" ,
                "OLD"."_RepresentedGeographicalUnit.GeographicalUnitID" AS "OLD__RepresentedGeographicalUnit.GeographicalUnitID" ,
                "OLD"."AristocraticTitle" AS "OLD_AristocraticTitle" ,
                "OLD"."BusinessPartnerCategory" AS "OLD_BusinessPartnerCategory" ,
                "OLD"."ChildrenAtHomeIndicator" AS "OLD_ChildrenAtHomeIndicator" ,
                "OLD"."ConcentrationFactor" AS "OLD_ConcentrationFactor" ,
                "OLD"."Consultation" AS "OLD_Consultation" ,
                "OLD"."ContributionsFromAllClearingMembers" AS "OLD_ContributionsFromAllClearingMembers" ,
                "OLD"."CountryOfBirth" AS "OLD_CountryOfBirth" ,
                "OLD"."CountryOfFoundation" AS "OLD_CountryOfFoundation" ,
                "OLD"."CountryOfIncorporation" AS "OLD_CountryOfIncorporation" ,
                "OLD"."CountryOfMainEconomicActivities" AS "OLD_CountryOfMainEconomicActivities" ,
                "OLD"."DateOfBirth" AS "OLD_DateOfBirth" ,
                "OLD"."DateOfDeath" AS "OLD_DateOfDeath" ,
                "OLD"."DateOfFoundation" AS "OLD_DateOfFoundation" ,
                "OLD"."DateOfIncorporation" AS "OLD_DateOfIncorporation" ,
                "OLD"."DeceasedFlag" AS "OLD_DeceasedFlag" ,
                "OLD"."DefaultFundCurrency" AS "OLD_DefaultFundCurrency" ,
                "OLD"."EducationLevel" AS "OLD_EducationLevel" ,
                "OLD"."EmployeeID" AS "OLD_EmployeeID" ,
                "OLD"."EmploymentDate" AS "OLD_EmploymentDate" ,
                "OLD"."FundType" AS "OLD_FundType" ,
                "OLD"."Gender" AS "OLD_Gender" ,
                "OLD"."GivenName" AS "OLD_GivenName" ,
                "OLD"."GlobalSystemRelevantFinancialInstitution" AS "OLD_GlobalSystemRelevantFinancialInstitution" ,
                "OLD"."GroupCategory" AS "OLD_GroupCategory" ,
                "OLD"."GroupType" AS "OLD_GroupType" ,
                "OLD"."HouseHoldGrossIncome" AS "OLD_HouseHoldGrossIncome" ,
                "OLD"."HouseHoldGrossIncomeCurrency" AS "OLD_HouseHoldGrossIncomeCurrency" ,
                "OLD"."HouseHoldGrossIncomeTimeUnit" AS "OLD_HouseHoldGrossIncomeTimeUnit" ,
                "OLD"."HouseHoldNetIncome" AS "OLD_HouseHoldNetIncome" ,
                "OLD"."HouseHoldNetIncomeCurrency" AS "OLD_HouseHoldNetIncomeCurrency" ,
                "OLD"."HouseHoldNetIncomeTimeUnit" AS "OLD_HouseHoldNetIncomeTimeUnit" ,
                "OLD"."HypotheticalCapitalRequirementsOfCCP" AS "OLD_HypotheticalCapitalRequirementsOfCCP" ,
                "OLD"."IndividualCategory" AS "OLD_IndividualCategory" ,
                "OLD"."InstitutionalProtectionScheme" AS "OLD_InstitutionalProtectionScheme" ,
                "OLD"."IsCompany" AS "OLD_IsCompany" ,
                "OLD"."IsHomeOwner" AS "OLD_IsHomeOwner" ,
                "OLD"."IsPoliticallyExposedPerson" AS "OLD_IsPoliticallyExposedPerson" ,
                "OLD"."JobTitle" AS "OLD_JobTitle" ,
                "OLD"."JointLiabilityType" AS "OLD_JointLiabilityType" ,
                "OLD"."LastName" AS "OLD_LastName" ,
                "OLD"."LeavingDate" AS "OLD_LeavingDate" ,
                "OLD"."LegalEntityUnderPublicLawType" AS "OLD_LegalEntityUnderPublicLawType" ,
                "OLD"."LegalForm" AS "OLD_LegalForm" ,
                "OLD"."LifecycleStatus" AS "OLD_LifecycleStatus" ,
                "OLD"."LifecycleStatusChangeDate" AS "OLD_LifecycleStatusChangeDate" ,
                "OLD"."LifecycleStatusReason" AS "OLD_LifecycleStatusReason" ,
                "OLD"."LiquidationDate" AS "OLD_LiquidationDate" ,
                "OLD"."MaritalStatus" AS "OLD_MaritalStatus" ,
                "OLD"."MiddleName" AS "OLD_MiddleName" ,
                "OLD"."MinorToMajorDate" AS "OLD_MinorToMajorDate" ,
                "OLD"."Name" AS "OLD_Name" ,
                "OLD"."NameAtBirth" AS "OLD_NameAtBirth" ,
                "OLD"."NameSupplement" AS "OLD_NameSupplement" ,
                "OLD"."Nationality" AS "OLD_Nationality" ,
                "OLD"."NumberHouseholdMembers" AS "OLD_NumberHouseholdMembers" ,
                "OLD"."NumberOfClearingMembers" AS "OLD_NumberOfClearingMembers" ,
                "OLD"."OrganizationCategory" AS "OLD_OrganizationCategory" ,
                "OLD"."PlaceOfBirth" AS "OLD_PlaceOfBirth" ,
                "OLD"."PlaceOfRegister" AS "OLD_PlaceOfRegister" ,
                "OLD"."PreferredCorrespondenceLanguage" AS "OLD_PreferredCorrespondenceLanguage" ,
                "OLD"."PrefundedOwnResourcesOfCCP" AS "OLD_PrefundedOwnResourcesOfCCP" ,
                "OLD"."QualifiedCCP" AS "OLD_QualifiedCCP" ,
                "OLD"."Register" AS "OLD_Register" ,
                "OLD"."RegulatedFinancialInstitution" AS "OLD_RegulatedFinancialInstitution" ,
                "OLD"."RelationshipStartDate" AS "OLD_RelationshipStartDate" ,
                "OLD"."ReligiousHonorific" AS "OLD_ReligiousHonorific" ,
                "OLD"."ResidingCountry" AS "OLD_ResidingCountry" ,
                "OLD"."ResidingSince" AS "OLD_ResidingSince" ,
                "OLD"."ResidingStatus" AS "OLD_ResidingStatus" ,
                "OLD"."RiskCountry" AS "OLD_RiskCountry" ,
                "OLD"."RoleInClearing" AS "OLD_RoleInClearing" ,
                "OLD"."SecondNationality" AS "OLD_SecondNationality" ,
                "OLD"."ShadowBank" AS "OLD_ShadowBank" ,
                "OLD"."ShortName" AS "OLD_ShortName" ,
                "OLD"."SmokerIndicator" AS "OLD_SmokerIndicator" ,
                "OLD"."Title" AS "OLD_Title" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartner" as "OLD"
            on
                                                "IN"."BusinessPartnerID" = "OLD"."BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::BusinessPartner"
    where (
        "BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BusinessPartner" as "OLD"
        on
                                       "IN"."BusinessPartnerID" = "OLD"."BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
