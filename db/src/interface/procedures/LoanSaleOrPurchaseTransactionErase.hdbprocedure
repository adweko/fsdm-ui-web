PROCEDURE "sap.fsdm.procedures::LoanSaleOrPurchaseTransactionErase" (IN ROW "sap.fsdm.tabletypes::LoanSaleOrPurchaseTransactionTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "TransactionID" is null and
            "_Loan.FinancialContractID" is null and
            "_Loan.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::LoanSaleOrPurchaseTransaction"
        WHERE
        (            "TransactionID" ,
            "_Loan.FinancialContractID" ,
            "_Loan.IDSystem" 
        ) in
        (
            select                 "OLD"."TransactionID" ,
                "OLD"."_Loan.FinancialContractID" ,
                "OLD"."_Loan.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::LoanSaleOrPurchaseTransaction" "OLD"
            on
            "IN"."TransactionID" = "OLD"."TransactionID" and
            "IN"."_Loan.FinancialContractID" = "OLD"."_Loan.FinancialContractID" and
            "IN"."_Loan.IDSystem" = "OLD"."_Loan.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::LoanSaleOrPurchaseTransaction_Historical"
        WHERE
        (
            "TransactionID" ,
            "_Loan.FinancialContractID" ,
            "_Loan.IDSystem" 
        ) in
        (
            select
                "OLD"."TransactionID" ,
                "OLD"."_Loan.FinancialContractID" ,
                "OLD"."_Loan.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::LoanSaleOrPurchaseTransaction_Historical" "OLD"
            on
                "IN"."TransactionID" = "OLD"."TransactionID" and
                "IN"."_Loan.FinancialContractID" = "OLD"."_Loan.FinancialContractID" and
                "IN"."_Loan.IDSystem" = "OLD"."_Loan.IDSystem" 
        );

END
