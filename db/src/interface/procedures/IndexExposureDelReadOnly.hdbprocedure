PROCEDURE "sap.fsdm.procedures::IndexExposureDelReadOnly" (IN ROW "sap.fsdm.tabletypes::IndexExposureTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::IndexExposureTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::IndexExposureTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'MarketRiskAnalysisType=' || TO_VARCHAR("MarketRiskAnalysisType") || ' ' ||
                'MarketRiskSplitPartType=' || TO_VARCHAR("MarketRiskSplitPartType") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'RoleOfCurrency=' || TO_VARCHAR("RoleOfCurrency") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_Index.IndexID=' || TO_VARCHAR("_Index.IndexID") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_RiskReportingNode.RiskReportingNodeID=' || TO_VARCHAR("_RiskReportingNode.RiskReportingNodeID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."MarketRiskAnalysisType",
                        "IN"."MarketRiskSplitPartType",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."MarketRiskAnalysisType",
                        "IN"."MarketRiskSplitPartType",
                        "IN"."RiskProvisionScenario",
                        "IN"."RoleOfCurrency",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_RiskReportingNode.RiskReportingNodeID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "MarketRiskAnalysisType",
                        "MarketRiskSplitPartType",
                        "RiskProvisionScenario",
                        "RoleOfCurrency",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_Index.IndexID",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_RiskReportingNode.RiskReportingNodeID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."MarketRiskAnalysisType",
                                    "IN"."MarketRiskSplitPartType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_Index.IndexID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."MarketRiskAnalysisType",
                                    "IN"."MarketRiskSplitPartType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."RoleOfCurrency",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_Index.IndexID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_RiskReportingNode.RiskReportingNodeID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "MarketRiskAnalysisType" is null and
            "MarketRiskSplitPartType" is null and
            "RiskProvisionScenario" is null and
            "RoleOfCurrency" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_Index.IndexID" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "MarketRiskAnalysisType",
            "MarketRiskSplitPartType",
            "RiskProvisionScenario",
            "RoleOfCurrency",
            "_FinancialContract.FinancialContractID",
            "_FinancialContract.IDSystem",
            "_FinancialInstrument.FinancialInstrumentID",
            "_Index.IndexID",
            "_ResultGroup.ResultDataProvider",
            "_ResultGroup.ResultGroupID",
            "_RiskReportingNode.RiskReportingNodeID",
            "_SecuritiesAccount.FinancialContractID",
            "_SecuritiesAccount.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::IndexExposure" WHERE
            (
            "MarketRiskAnalysisType" ,
            "MarketRiskSplitPartType" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_Index.IndexID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."MarketRiskAnalysisType",
            "OLD"."MarketRiskSplitPartType",
            "OLD"."RiskProvisionScenario",
            "OLD"."RoleOfCurrency",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_Index.IndexID",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_RiskReportingNode.RiskReportingNodeID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::IndexExposure" as "OLD"
        on
                              "IN"."MarketRiskAnalysisType" = "OLD"."MarketRiskAnalysisType" and
                              "IN"."MarketRiskSplitPartType" = "OLD"."MarketRiskSplitPartType" and
                              "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                              "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                              "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                              "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                              "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                              "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                              "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                              "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                              "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                              "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "MarketRiskAnalysisType",
        "MarketRiskSplitPartType",
        "RiskProvisionScenario",
        "RoleOfCurrency",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_Index.IndexID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_RiskReportingNode.RiskReportingNodeID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Currency",
        "DollarBeta",
        "NetPresentValue",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_MarketRiskAnalysisType" as "MarketRiskAnalysisType" ,
            "OLD_MarketRiskSplitPartType" as "MarketRiskSplitPartType" ,
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
            "OLD_RoleOfCurrency" as "RoleOfCurrency" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__Index.IndexID" as "_Index.IndexID" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID" ,
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Currency" as "Currency" ,
            "OLD_DollarBeta" as "DollarBeta" ,
            "OLD_NetPresentValue" as "NetPresentValue" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."MarketRiskAnalysisType",
                        "OLD"."MarketRiskSplitPartType",
                        "OLD"."RiskProvisionScenario",
                        "OLD"."RoleOfCurrency",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_Index.IndexID",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_RiskReportingNode.RiskReportingNodeID",
                        "OLD"."_SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."MarketRiskAnalysisType" AS "OLD_MarketRiskAnalysisType" ,
                "OLD"."MarketRiskSplitPartType" AS "OLD_MarketRiskSplitPartType" ,
                "OLD"."RiskProvisionScenario" AS "OLD_RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" AS "OLD_RoleOfCurrency" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_Index.IndexID" AS "OLD__Index.IndexID" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" AS "OLD__RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" AS "OLD__SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" AS "OLD__SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."DollarBeta" AS "OLD_DollarBeta" ,
                "OLD"."NetPresentValue" AS "OLD_NetPresentValue" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::IndexExposure" as "OLD"
            on
                                      "IN"."MarketRiskAnalysisType" = "OLD"."MarketRiskAnalysisType" and
                                      "IN"."MarketRiskSplitPartType" = "OLD"."MarketRiskSplitPartType" and
                                      "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                                      "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                      "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                                      "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                      "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                                      "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                                      "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                                      "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_MarketRiskAnalysisType" as "MarketRiskAnalysisType",
            "OLD_MarketRiskSplitPartType" as "MarketRiskSplitPartType",
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario",
            "OLD_RoleOfCurrency" as "RoleOfCurrency",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__Index.IndexID" as "_Index.IndexID",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__RiskReportingNode.RiskReportingNodeID" as "_RiskReportingNode.RiskReportingNodeID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Currency" as "Currency",
            "OLD_DollarBeta" as "DollarBeta",
            "OLD_NetPresentValue" as "NetPresentValue",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."MarketRiskAnalysisType",
                        "OLD"."MarketRiskSplitPartType",
                        "OLD"."RiskProvisionScenario",
                        "OLD"."RoleOfCurrency",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_Index.IndexID",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."_RiskReportingNode.RiskReportingNodeID",
                        "OLD"."_SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."MarketRiskAnalysisType" AS "OLD_MarketRiskAnalysisType" ,
                "OLD"."MarketRiskSplitPartType" AS "OLD_MarketRiskSplitPartType" ,
                "OLD"."RiskProvisionScenario" AS "OLD_RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" AS "OLD_RoleOfCurrency" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_Index.IndexID" AS "OLD__Index.IndexID" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" AS "OLD__RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" AS "OLD__SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" AS "OLD__SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Currency" AS "OLD_Currency" ,
                "OLD"."DollarBeta" AS "OLD_DollarBeta" ,
                "OLD"."NetPresentValue" AS "OLD_NetPresentValue" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::IndexExposure" as "OLD"
            on
               "IN"."MarketRiskAnalysisType" = "OLD"."MarketRiskAnalysisType" and
               "IN"."MarketRiskSplitPartType" = "OLD"."MarketRiskSplitPartType" and
               "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
               "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
               "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
               "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
               "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
               "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
               "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
               "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
               "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
