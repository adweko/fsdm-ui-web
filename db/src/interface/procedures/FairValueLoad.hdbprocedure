PROCEDURE "sap.fsdm.procedures::FairValueLoad" (IN ROW "sap.fsdm.tabletypes::FairValueTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AccountingChangeSequenceNumber=' || TO_VARCHAR("AccountingChangeSequenceNumber") || ' ' ||
                'ComponentNumber=' || TO_VARCHAR("ComponentNumber") || ' ' ||
                'Currency=' || TO_VARCHAR("Currency") || ' ' ||
                'FVCalculationMethod=' || TO_VARCHAR("FVCalculationMethod") || ' ' ||
                'IndicatorResultBeforeChange=' || TO_VARCHAR("IndicatorResultBeforeChange") || ' ' ||
                'LotID=' || TO_VARCHAR("LotID") || ' ' ||
                'RoleOfPayer=' || TO_VARCHAR("RoleOfPayer") || ' ' ||
                '_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("_AccountingSystem.AccountingSystemID") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_InvestmentAccount.FinancialContractID=' || TO_VARCHAR("_InvestmentAccount.FinancialContractID") || ' ' ||
                '_InvestmentAccount.IDSystem=' || TO_VARCHAR("_InvestmentAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AccountingChangeSequenceNumber",
                        "IN"."ComponentNumber",
                        "IN"."Currency",
                        "IN"."FVCalculationMethod",
                        "IN"."IndicatorResultBeforeChange",
                        "IN"."LotID",
                        "IN"."RoleOfPayer",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AccountingChangeSequenceNumber",
                        "IN"."ComponentNumber",
                        "IN"."Currency",
                        "IN"."FVCalculationMethod",
                        "IN"."IndicatorResultBeforeChange",
                        "IN"."LotID",
                        "IN"."RoleOfPayer",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AccountingChangeSequenceNumber",
                        "ComponentNumber",
                        "Currency",
                        "FVCalculationMethod",
                        "IndicatorResultBeforeChange",
                        "LotID",
                        "RoleOfPayer",
                        "_AccountingSystem.AccountingSystemID",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_InvestmentAccount.FinancialContractID",
                        "_InvestmentAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AccountingChangeSequenceNumber",
                                    "IN"."ComponentNumber",
                                    "IN"."Currency",
                                    "IN"."FVCalculationMethod",
                                    "IN"."IndicatorResultBeforeChange",
                                    "IN"."LotID",
                                    "IN"."RoleOfPayer",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InvestmentAccount.FinancialContractID",
                                    "IN"."_InvestmentAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AccountingChangeSequenceNumber",
                                    "IN"."ComponentNumber",
                                    "IN"."Currency",
                                    "IN"."FVCalculationMethod",
                                    "IN"."IndicatorResultBeforeChange",
                                    "IN"."LotID",
                                    "IN"."RoleOfPayer",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InvestmentAccount.FinancialContractID",
                                    "IN"."_InvestmentAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::FairValue" (
        "AccountingChangeSequenceNumber",
        "ComponentNumber",
        "Currency",
        "FVCalculationMethod",
        "IndicatorResultBeforeChange",
        "LotID",
        "RoleOfPayer",
        "_AccountingSystem.AccountingSystemID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingChangeDate",
        "AccountingChangeReason",
        "Category",
        "FairValueInFunctionalCurrency",
        "FairValueInGroupCurrency",
        "FairValueInHardCurrency",
        "FairValueInIndexlCurrency",
        "FairValueInLocalCurrency",
        "FairValueInPaymentCurrency",
        "FairValueInPositionCurrency",
        "FunctionalCurrency",
        "GroupCurrency",
        "HardCurrency",
        "IndexCurrency",
        "LocalCurrency",
        "PaymentCurrency",
        "PositionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AccountingChangeSequenceNumber" as "AccountingChangeSequenceNumber" ,
            "OLD_ComponentNumber" as "ComponentNumber" ,
            "OLD_Currency" as "Currency" ,
            "OLD_FVCalculationMethod" as "FVCalculationMethod" ,
            "OLD_IndicatorResultBeforeChange" as "IndicatorResultBeforeChange" ,
            "OLD_LotID" as "LotID" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__InvestmentAccount.FinancialContractID" as "_InvestmentAccount.FinancialContractID" ,
            "OLD__InvestmentAccount.IDSystem" as "_InvestmentAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AccountingChangeDate" as "AccountingChangeDate" ,
            "OLD_AccountingChangeReason" as "AccountingChangeReason" ,
            "OLD_Category" as "Category" ,
            "OLD_FairValueInFunctionalCurrency" as "FairValueInFunctionalCurrency" ,
            "OLD_FairValueInGroupCurrency" as "FairValueInGroupCurrency" ,
            "OLD_FairValueInHardCurrency" as "FairValueInHardCurrency" ,
            "OLD_FairValueInIndexlCurrency" as "FairValueInIndexlCurrency" ,
            "OLD_FairValueInLocalCurrency" as "FairValueInLocalCurrency" ,
            "OLD_FairValueInPaymentCurrency" as "FairValueInPaymentCurrency" ,
            "OLD_FairValueInPositionCurrency" as "FairValueInPositionCurrency" ,
            "OLD_FunctionalCurrency" as "FunctionalCurrency" ,
            "OLD_GroupCurrency" as "GroupCurrency" ,
            "OLD_HardCurrency" as "HardCurrency" ,
            "OLD_IndexCurrency" as "IndexCurrency" ,
            "OLD_LocalCurrency" as "LocalCurrency" ,
            "OLD_PaymentCurrency" as "PaymentCurrency" ,
            "OLD_PositionCurrency" as "PositionCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."AccountingChangeSequenceNumber",
                        "IN"."ComponentNumber",
                        "IN"."Currency",
                        "IN"."FVCalculationMethod",
                        "IN"."IndicatorResultBeforeChange",
                        "IN"."LotID",
                        "IN"."RoleOfPayer",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."AccountingChangeSequenceNumber" as "OLD_AccountingChangeSequenceNumber",
                                "OLD"."ComponentNumber" as "OLD_ComponentNumber",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."FVCalculationMethod" as "OLD_FVCalculationMethod",
                                "OLD"."IndicatorResultBeforeChange" as "OLD_IndicatorResultBeforeChange",
                                "OLD"."LotID" as "OLD_LotID",
                                "OLD"."RoleOfPayer" as "OLD_RoleOfPayer",
                                "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                                "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                                "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                                "OLD"."_Collection._Client.BusinessPartnerID" as "OLD__Collection._Client.BusinessPartnerID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_InvestmentAccount.FinancialContractID" as "OLD__InvestmentAccount.FinancialContractID",
                                "OLD"."_InvestmentAccount.IDSystem" as "OLD__InvestmentAccount.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AccountingChangeDate" as "OLD_AccountingChangeDate",
                                "OLD"."AccountingChangeReason" as "OLD_AccountingChangeReason",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."FairValueInFunctionalCurrency" as "OLD_FairValueInFunctionalCurrency",
                                "OLD"."FairValueInGroupCurrency" as "OLD_FairValueInGroupCurrency",
                                "OLD"."FairValueInHardCurrency" as "OLD_FairValueInHardCurrency",
                                "OLD"."FairValueInIndexlCurrency" as "OLD_FairValueInIndexlCurrency",
                                "OLD"."FairValueInLocalCurrency" as "OLD_FairValueInLocalCurrency",
                                "OLD"."FairValueInPaymentCurrency" as "OLD_FairValueInPaymentCurrency",
                                "OLD"."FairValueInPositionCurrency" as "OLD_FairValueInPositionCurrency",
                                "OLD"."FunctionalCurrency" as "OLD_FunctionalCurrency",
                                "OLD"."GroupCurrency" as "OLD_GroupCurrency",
                                "OLD"."HardCurrency" as "OLD_HardCurrency",
                                "OLD"."IndexCurrency" as "OLD_IndexCurrency",
                                "OLD"."LocalCurrency" as "OLD_LocalCurrency",
                                "OLD"."PaymentCurrency" as "OLD_PaymentCurrency",
                                "OLD"."PositionCurrency" as "OLD_PositionCurrency",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::FairValue" as "OLD"
            on
                ifnull( "IN"."AccountingChangeSequenceNumber", -1) = "OLD"."AccountingChangeSequenceNumber" and
                ifnull( "IN"."ComponentNumber", -1) = "OLD"."ComponentNumber" and
                ifnull( "IN"."Currency", '') = "OLD"."Currency" and
                ifnull( "IN"."FVCalculationMethod", '') = "OLD"."FVCalculationMethod" and
                ifnull( "IN"."IndicatorResultBeforeChange", false) = "OLD"."IndicatorResultBeforeChange" and
                ifnull( "IN"."LotID", '') = "OLD"."LotID" and
                ifnull( "IN"."RoleOfPayer", '') = "OLD"."RoleOfPayer" and
                ifnull( "IN"."_AccountingSystem.AccountingSystemID", '') = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" and
                ifnull( "IN"."_Collection._Client.BusinessPartnerID", '') = "OLD"."_Collection._Client.BusinessPartnerID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_InvestmentAccount.FinancialContractID", '') = "OLD"."_InvestmentAccount.FinancialContractID" and
                ifnull( "IN"."_InvestmentAccount.IDSystem", '') = "OLD"."_InvestmentAccount.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::FairValue" (
        "AccountingChangeSequenceNumber",
        "ComponentNumber",
        "Currency",
        "FVCalculationMethod",
        "IndicatorResultBeforeChange",
        "LotID",
        "RoleOfPayer",
        "_AccountingSystem.AccountingSystemID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingChangeDate",
        "AccountingChangeReason",
        "Category",
        "FairValueInFunctionalCurrency",
        "FairValueInGroupCurrency",
        "FairValueInHardCurrency",
        "FairValueInIndexlCurrency",
        "FairValueInLocalCurrency",
        "FairValueInPaymentCurrency",
        "FairValueInPositionCurrency",
        "FunctionalCurrency",
        "GroupCurrency",
        "HardCurrency",
        "IndexCurrency",
        "LocalCurrency",
        "PaymentCurrency",
        "PositionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AccountingChangeSequenceNumber" as "AccountingChangeSequenceNumber",
            "OLD_ComponentNumber" as "ComponentNumber",
            "OLD_Currency" as "Currency",
            "OLD_FVCalculationMethod" as "FVCalculationMethod",
            "OLD_IndicatorResultBeforeChange" as "IndicatorResultBeforeChange",
            "OLD_LotID" as "LotID",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__InvestmentAccount.FinancialContractID" as "_InvestmentAccount.FinancialContractID",
            "OLD__InvestmentAccount.IDSystem" as "_InvestmentAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AccountingChangeDate" as "AccountingChangeDate",
            "OLD_AccountingChangeReason" as "AccountingChangeReason",
            "OLD_Category" as "Category",
            "OLD_FairValueInFunctionalCurrency" as "FairValueInFunctionalCurrency",
            "OLD_FairValueInGroupCurrency" as "FairValueInGroupCurrency",
            "OLD_FairValueInHardCurrency" as "FairValueInHardCurrency",
            "OLD_FairValueInIndexlCurrency" as "FairValueInIndexlCurrency",
            "OLD_FairValueInLocalCurrency" as "FairValueInLocalCurrency",
            "OLD_FairValueInPaymentCurrency" as "FairValueInPaymentCurrency",
            "OLD_FairValueInPositionCurrency" as "FairValueInPositionCurrency",
            "OLD_FunctionalCurrency" as "FunctionalCurrency",
            "OLD_GroupCurrency" as "GroupCurrency",
            "OLD_HardCurrency" as "HardCurrency",
            "OLD_IndexCurrency" as "IndexCurrency",
            "OLD_LocalCurrency" as "LocalCurrency",
            "OLD_PaymentCurrency" as "PaymentCurrency",
            "OLD_PositionCurrency" as "PositionCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."AccountingChangeSequenceNumber",
                        "IN"."ComponentNumber",
                        "IN"."Currency",
                        "IN"."FVCalculationMethod",
                        "IN"."IndicatorResultBeforeChange",
                        "IN"."LotID",
                        "IN"."RoleOfPayer",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InvestmentAccount.FinancialContractID",
                        "IN"."_InvestmentAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."AccountingChangeSequenceNumber" as "OLD_AccountingChangeSequenceNumber",
                        "OLD"."ComponentNumber" as "OLD_ComponentNumber",
                        "OLD"."Currency" as "OLD_Currency",
                        "OLD"."FVCalculationMethod" as "OLD_FVCalculationMethod",
                        "OLD"."IndicatorResultBeforeChange" as "OLD_IndicatorResultBeforeChange",
                        "OLD"."LotID" as "OLD_LotID",
                        "OLD"."RoleOfPayer" as "OLD_RoleOfPayer",
                        "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                        "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                        "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID" as "OLD__Collection._Client.BusinessPartnerID",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InvestmentAccount.FinancialContractID" as "OLD__InvestmentAccount.FinancialContractID",
                        "OLD"."_InvestmentAccount.IDSystem" as "OLD__InvestmentAccount.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."AccountingChangeDate" as "OLD_AccountingChangeDate",
                        "OLD"."AccountingChangeReason" as "OLD_AccountingChangeReason",
                        "OLD"."Category" as "OLD_Category",
                        "OLD"."FairValueInFunctionalCurrency" as "OLD_FairValueInFunctionalCurrency",
                        "OLD"."FairValueInGroupCurrency" as "OLD_FairValueInGroupCurrency",
                        "OLD"."FairValueInHardCurrency" as "OLD_FairValueInHardCurrency",
                        "OLD"."FairValueInIndexlCurrency" as "OLD_FairValueInIndexlCurrency",
                        "OLD"."FairValueInLocalCurrency" as "OLD_FairValueInLocalCurrency",
                        "OLD"."FairValueInPaymentCurrency" as "OLD_FairValueInPaymentCurrency",
                        "OLD"."FairValueInPositionCurrency" as "OLD_FairValueInPositionCurrency",
                        "OLD"."FunctionalCurrency" as "OLD_FunctionalCurrency",
                        "OLD"."GroupCurrency" as "OLD_GroupCurrency",
                        "OLD"."HardCurrency" as "OLD_HardCurrency",
                        "OLD"."IndexCurrency" as "OLD_IndexCurrency",
                        "OLD"."LocalCurrency" as "OLD_LocalCurrency",
                        "OLD"."PaymentCurrency" as "OLD_PaymentCurrency",
                        "OLD"."PositionCurrency" as "OLD_PositionCurrency",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::FairValue" as "OLD"
            on
                ifnull( "IN"."AccountingChangeSequenceNumber", -1 ) = "OLD"."AccountingChangeSequenceNumber" and
                ifnull( "IN"."ComponentNumber", -1 ) = "OLD"."ComponentNumber" and
                ifnull( "IN"."Currency", '' ) = "OLD"."Currency" and
                ifnull( "IN"."FVCalculationMethod", '' ) = "OLD"."FVCalculationMethod" and
                ifnull( "IN"."IndicatorResultBeforeChange", false ) = "OLD"."IndicatorResultBeforeChange" and
                ifnull( "IN"."LotID", '' ) = "OLD"."LotID" and
                ifnull( "IN"."RoleOfPayer", '' ) = "OLD"."RoleOfPayer" and
                ifnull( "IN"."_AccountingSystem.AccountingSystemID", '' ) = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."_Collection.CollectionID", '' ) = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '' ) = "OLD"."_Collection.IDSystem" and
                ifnull( "IN"."_Collection._Client.BusinessPartnerID", '' ) = "OLD"."_Collection._Client.BusinessPartnerID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_InvestmentAccount.FinancialContractID", '' ) = "OLD"."_InvestmentAccount.FinancialContractID" and
                ifnull( "IN"."_InvestmentAccount.IDSystem", '' ) = "OLD"."_InvestmentAccount.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::FairValue"
    where (
        "AccountingChangeSequenceNumber",
        "ComponentNumber",
        "Currency",
        "FVCalculationMethod",
        "IndicatorResultBeforeChange",
        "LotID",
        "RoleOfPayer",
        "_AccountingSystem.AccountingSystemID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."AccountingChangeSequenceNumber",
            "OLD"."ComponentNumber",
            "OLD"."Currency",
            "OLD"."FVCalculationMethod",
            "OLD"."IndicatorResultBeforeChange",
            "OLD"."LotID",
            "OLD"."RoleOfPayer",
            "OLD"."_AccountingSystem.AccountingSystemID",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_InvestmentAccount.FinancialContractID",
            "OLD"."_InvestmentAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::FairValue" as "OLD"
        on
           ifnull( "IN"."AccountingChangeSequenceNumber", -1 ) = "OLD"."AccountingChangeSequenceNumber" and
           ifnull( "IN"."ComponentNumber", -1 ) = "OLD"."ComponentNumber" and
           ifnull( "IN"."Currency", '' ) = "OLD"."Currency" and
           ifnull( "IN"."FVCalculationMethod", '' ) = "OLD"."FVCalculationMethod" and
           ifnull( "IN"."IndicatorResultBeforeChange", false ) = "OLD"."IndicatorResultBeforeChange" and
           ifnull( "IN"."LotID", '' ) = "OLD"."LotID" and
           ifnull( "IN"."RoleOfPayer", '' ) = "OLD"."RoleOfPayer" and
           ifnull( "IN"."_AccountingSystem.AccountingSystemID", '' ) = "OLD"."_AccountingSystem.AccountingSystemID" and
           ifnull( "IN"."_Collection.CollectionID", '' ) = "OLD"."_Collection.CollectionID" and
           ifnull( "IN"."_Collection.IDSystem", '' ) = "OLD"."_Collection.IDSystem" and
           ifnull( "IN"."_Collection._Client.BusinessPartnerID", '' ) = "OLD"."_Collection._Client.BusinessPartnerID" and
           ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
           ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
           ifnull( "IN"."_InvestmentAccount.FinancialContractID", '' ) = "OLD"."_InvestmentAccount.FinancialContractID" and
           ifnull( "IN"."_InvestmentAccount.IDSystem", '' ) = "OLD"."_InvestmentAccount.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::FairValue" (
        "AccountingChangeSequenceNumber",
        "ComponentNumber",
        "Currency",
        "FVCalculationMethod",
        "IndicatorResultBeforeChange",
        "LotID",
        "RoleOfPayer",
        "_AccountingSystem.AccountingSystemID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InvestmentAccount.FinancialContractID",
        "_InvestmentAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AccountingChangeDate",
        "AccountingChangeReason",
        "Category",
        "FairValueInFunctionalCurrency",
        "FairValueInGroupCurrency",
        "FairValueInHardCurrency",
        "FairValueInIndexlCurrency",
        "FairValueInLocalCurrency",
        "FairValueInPaymentCurrency",
        "FairValueInPositionCurrency",
        "FunctionalCurrency",
        "GroupCurrency",
        "HardCurrency",
        "IndexCurrency",
        "LocalCurrency",
        "PaymentCurrency",
        "PositionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "AccountingChangeSequenceNumber", -1 ) as "AccountingChangeSequenceNumber",
            ifnull( "ComponentNumber", -1 ) as "ComponentNumber",
            ifnull( "Currency", '' ) as "Currency",
            ifnull( "FVCalculationMethod", '' ) as "FVCalculationMethod",
            ifnull( "IndicatorResultBeforeChange", false ) as "IndicatorResultBeforeChange",
            ifnull( "LotID", '' ) as "LotID",
            ifnull( "RoleOfPayer", '' ) as "RoleOfPayer",
            ifnull( "_AccountingSystem.AccountingSystemID", '' ) as "_AccountingSystem.AccountingSystemID",
            ifnull( "_Collection.CollectionID", '' ) as "_Collection.CollectionID",
            ifnull( "_Collection.IDSystem", '' ) as "_Collection.IDSystem",
            ifnull( "_Collection._Client.BusinessPartnerID", '' ) as "_Collection._Client.BusinessPartnerID",
            ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
            ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
            ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
            ifnull( "_InvestmentAccount.FinancialContractID", '' ) as "_InvestmentAccount.FinancialContractID",
            ifnull( "_InvestmentAccount.IDSystem", '' ) as "_InvestmentAccount.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "AccountingChangeDate"  ,
            "AccountingChangeReason"  ,
            "Category"  ,
            "FairValueInFunctionalCurrency"  ,
            "FairValueInGroupCurrency"  ,
            "FairValueInHardCurrency"  ,
            "FairValueInIndexlCurrency"  ,
            "FairValueInLocalCurrency"  ,
            "FairValueInPaymentCurrency"  ,
            "FairValueInPositionCurrency"  ,
            "FunctionalCurrency"  ,
            "GroupCurrency"  ,
            "HardCurrency"  ,
            "IndexCurrency"  ,
            "LocalCurrency"  ,
            "PaymentCurrency"  ,
            "PositionCurrency"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END