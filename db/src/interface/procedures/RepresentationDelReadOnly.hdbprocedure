PROCEDURE "sap.fsdm.procedures::RepresentationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::RepresentationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::RepresentationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::RepresentationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'TypeOfContact=' || TO_VARCHAR("TypeOfContact") || ' ' ||
                'ASSOC_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartner.BusinessPartnerID") || ' ' ||
                'ASSOC_ContactPerson.BusinessPartnerID=' || TO_VARCHAR("ASSOC_ContactPerson.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."TypeOfContact",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_ContactPerson.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."TypeOfContact",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_ContactPerson.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "TypeOfContact",
                        "ASSOC_BusinessPartner.BusinessPartnerID",
                        "ASSOC_ContactPerson.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."TypeOfContact",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_ContactPerson.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."TypeOfContact",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_ContactPerson.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "TypeOfContact" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_ContactPerson.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "TypeOfContact",
            "ASSOC_BusinessPartner.BusinessPartnerID",
            "ASSOC_ContactPerson.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Representation" WHERE
            (
            "TypeOfContact" ,
            "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "ASSOC_ContactPerson.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."TypeOfContact",
            "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD"."ASSOC_ContactPerson.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Representation" as "OLD"
        on
                              "IN"."TypeOfContact" = "OLD"."TypeOfContact" and
                              "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                              "IN"."ASSOC_ContactPerson.BusinessPartnerID" = "OLD"."ASSOC_ContactPerson.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "TypeOfContact",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_ContactPerson.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PowerOfAttorney",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_TypeOfContact" as "TypeOfContact" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_ContactPerson.BusinessPartnerID" as "ASSOC_ContactPerson.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_PowerOfAttorney" as "PowerOfAttorney" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."TypeOfContact",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_ContactPerson.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."TypeOfContact" AS "OLD_TypeOfContact" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_ContactPerson.BusinessPartnerID" AS "OLD_ASSOC_ContactPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PowerOfAttorney" AS "OLD_PowerOfAttorney" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Representation" as "OLD"
            on
                                      "IN"."TypeOfContact" = "OLD"."TypeOfContact" and
                                      "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                                      "IN"."ASSOC_ContactPerson.BusinessPartnerID" = "OLD"."ASSOC_ContactPerson.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_TypeOfContact" as "TypeOfContact",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_ContactPerson.BusinessPartnerID" as "ASSOC_ContactPerson.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PowerOfAttorney" as "PowerOfAttorney",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."TypeOfContact",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_ContactPerson.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."TypeOfContact" AS "OLD_TypeOfContact" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_ContactPerson.BusinessPartnerID" AS "OLD_ASSOC_ContactPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PowerOfAttorney" AS "OLD_PowerOfAttorney" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Representation" as "OLD"
            on
               "IN"."TypeOfContact" = "OLD"."TypeOfContact" and
               "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
               "IN"."ASSOC_ContactPerson.BusinessPartnerID" = "OLD"."ASSOC_ContactPerson.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
