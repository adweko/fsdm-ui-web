PROCEDURE "sap.fsdm.procedures::ForbearanceMeasureErase" (IN ROW "sap.fsdm.tabletypes::ForbearanceMeasureTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ForbearanceCategory" is null and
            "MeasureInForceStartDate" is null and
            "_ContractStatus.ContractStatusCategory" is null and
            "_ContractStatus.ContractStatusType" is null and
            "_ContractStatus.ASSOC_FinancialContract.FinancialContractID" is null and
            "_ContractStatus.ASSOC_FinancialContract.IDSystem" is null and
            "_InstrumentStatus.InstrumentStatusCategory" is null and
            "_InstrumentStatus.InstrumentStatusType" is null and
            "_InstrumentStatus._Instrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ForbearanceMeasure"
        WHERE
        (            "ForbearanceCategory" ,
            "MeasureInForceStartDate" ,
            "_ContractStatus.ContractStatusCategory" ,
            "_ContractStatus.ContractStatusType" ,
            "_ContractStatus.ASSOC_FinancialContract.FinancialContractID" ,
            "_ContractStatus.ASSOC_FinancialContract.IDSystem" ,
            "_InstrumentStatus.InstrumentStatusCategory" ,
            "_InstrumentStatus.InstrumentStatusType" ,
            "_InstrumentStatus._Instrument.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."ForbearanceCategory" ,
                "OLD"."MeasureInForceStartDate" ,
                "OLD"."_ContractStatus.ContractStatusCategory" ,
                "OLD"."_ContractStatus.ContractStatusType" ,
                "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_InstrumentStatus.InstrumentStatusCategory" ,
                "OLD"."_InstrumentStatus.InstrumentStatusType" ,
                "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::ForbearanceMeasure" "OLD"
            on
            "IN"."ForbearanceCategory" = "OLD"."ForbearanceCategory" and
            "IN"."MeasureInForceStartDate" = "OLD"."MeasureInForceStartDate" and
            "IN"."_ContractStatus.ContractStatusCategory" = "OLD"."_ContractStatus.ContractStatusCategory" and
            "IN"."_ContractStatus.ContractStatusType" = "OLD"."_ContractStatus.ContractStatusType" and
            "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" and
            "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem" = "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" and
            "IN"."_InstrumentStatus.InstrumentStatusCategory" = "OLD"."_InstrumentStatus.InstrumentStatusCategory" and
            "IN"."_InstrumentStatus.InstrumentStatusType" = "OLD"."_InstrumentStatus.InstrumentStatusType" and
            "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID" = "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ForbearanceMeasure_Historical"
        WHERE
        (
            "ForbearanceCategory" ,
            "MeasureInForceStartDate" ,
            "_ContractStatus.ContractStatusCategory" ,
            "_ContractStatus.ContractStatusType" ,
            "_ContractStatus.ASSOC_FinancialContract.FinancialContractID" ,
            "_ContractStatus.ASSOC_FinancialContract.IDSystem" ,
            "_InstrumentStatus.InstrumentStatusCategory" ,
            "_InstrumentStatus.InstrumentStatusType" ,
            "_InstrumentStatus._Instrument.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."ForbearanceCategory" ,
                "OLD"."MeasureInForceStartDate" ,
                "OLD"."_ContractStatus.ContractStatusCategory" ,
                "OLD"."_ContractStatus.ContractStatusType" ,
                "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_InstrumentStatus.InstrumentStatusCategory" ,
                "OLD"."_InstrumentStatus.InstrumentStatusType" ,
                "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::ForbearanceMeasure_Historical" "OLD"
            on
                "IN"."ForbearanceCategory" = "OLD"."ForbearanceCategory" and
                "IN"."MeasureInForceStartDate" = "OLD"."MeasureInForceStartDate" and
                "IN"."_ContractStatus.ContractStatusCategory" = "OLD"."_ContractStatus.ContractStatusCategory" and
                "IN"."_ContractStatus.ContractStatusType" = "OLD"."_ContractStatus.ContractStatusType" and
                "IN"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_ContractStatus.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."_ContractStatus.ASSOC_FinancialContract.IDSystem" = "OLD"."_ContractStatus.ASSOC_FinancialContract.IDSystem" and
                "IN"."_InstrumentStatus.InstrumentStatusCategory" = "OLD"."_InstrumentStatus.InstrumentStatusCategory" and
                "IN"."_InstrumentStatus.InstrumentStatusType" = "OLD"."_InstrumentStatus.InstrumentStatusType" and
                "IN"."_InstrumentStatus._Instrument.FinancialInstrumentID" = "OLD"."_InstrumentStatus._Instrument.FinancialInstrumentID" 
        );

END
