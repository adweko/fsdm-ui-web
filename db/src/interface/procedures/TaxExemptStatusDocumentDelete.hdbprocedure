PROCEDURE "sap.fsdm.procedures::TaxExemptStatusDocumentDelete" (IN ROW "sap.fsdm.tabletypes::TaxExemptStatusDocumentTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Category=' || TO_VARCHAR("Category") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Category",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Category",
                        "_BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Category",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Category" is null and
            "_BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::TaxExemptStatusDocument" (
        "Category",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Organization.BusinessPartnerID",
        "CapitalClaimCompanyIncomeStartDate",
        "ExemptionThreshold",
        "ExemptionThresholdCurrency",
        "FuturesandOptionsIncomefromLettingandLeasingStartDate",
        "FuturesandOptionsInvestmentCompanyIncomeStartDate",
        "IssueDate",
        "SerialNumber",
        "Subtype",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Category" as "Category" ,
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID" ,
            "OLD_CapitalClaimCompanyIncomeStartDate" as "CapitalClaimCompanyIncomeStartDate" ,
            "OLD_ExemptionThreshold" as "ExemptionThreshold" ,
            "OLD_ExemptionThresholdCurrency" as "ExemptionThresholdCurrency" ,
            "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate" as "FuturesandOptionsIncomefromLettingandLeasingStartDate" ,
            "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate" as "FuturesandOptionsInvestmentCompanyIncomeStartDate" ,
            "OLD_IssueDate" as "IssueDate" ,
            "OLD_SerialNumber" as "SerialNumber" ,
            "OLD_Subtype" as "Subtype" ,
            "OLD_Type" as "Type" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Category",
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Category" AS "OLD_Category" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Organization.BusinessPartnerID" AS "OLD__Organization.BusinessPartnerID" ,
                "OLD"."CapitalClaimCompanyIncomeStartDate" AS "OLD_CapitalClaimCompanyIncomeStartDate" ,
                "OLD"."ExemptionThreshold" AS "OLD_ExemptionThreshold" ,
                "OLD"."ExemptionThresholdCurrency" AS "OLD_ExemptionThresholdCurrency" ,
                "OLD"."FuturesandOptionsIncomefromLettingandLeasingStartDate" AS "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate" ,
                "OLD"."FuturesandOptionsInvestmentCompanyIncomeStartDate" AS "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate" ,
                "OLD"."IssueDate" AS "OLD_IssueDate" ,
                "OLD"."SerialNumber" AS "OLD_SerialNumber" ,
                "OLD"."Subtype" AS "OLD_Subtype" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptStatusDocument" as "OLD"
            on
                      "IN"."Category" = "OLD"."Category" and
                      "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::TaxExemptStatusDocument" (
        "Category",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Organization.BusinessPartnerID",
        "CapitalClaimCompanyIncomeStartDate",
        "ExemptionThreshold",
        "ExemptionThresholdCurrency",
        "FuturesandOptionsIncomefromLettingandLeasingStartDate",
        "FuturesandOptionsInvestmentCompanyIncomeStartDate",
        "IssueDate",
        "SerialNumber",
        "Subtype",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Category" as "Category",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID",
            "OLD_CapitalClaimCompanyIncomeStartDate" as "CapitalClaimCompanyIncomeStartDate",
            "OLD_ExemptionThreshold" as "ExemptionThreshold",
            "OLD_ExemptionThresholdCurrency" as "ExemptionThresholdCurrency",
            "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate" as "FuturesandOptionsIncomefromLettingandLeasingStartDate",
            "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate" as "FuturesandOptionsInvestmentCompanyIncomeStartDate",
            "OLD_IssueDate" as "IssueDate",
            "OLD_SerialNumber" as "SerialNumber",
            "OLD_Subtype" as "Subtype",
            "OLD_Type" as "Type",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Category",
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Category" AS "OLD_Category" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Organization.BusinessPartnerID" AS "OLD__Organization.BusinessPartnerID" ,
                "OLD"."CapitalClaimCompanyIncomeStartDate" AS "OLD_CapitalClaimCompanyIncomeStartDate" ,
                "OLD"."ExemptionThreshold" AS "OLD_ExemptionThreshold" ,
                "OLD"."ExemptionThresholdCurrency" AS "OLD_ExemptionThresholdCurrency" ,
                "OLD"."FuturesandOptionsIncomefromLettingandLeasingStartDate" AS "OLD_FuturesandOptionsIncomefromLettingandLeasingStartDate" ,
                "OLD"."FuturesandOptionsInvestmentCompanyIncomeStartDate" AS "OLD_FuturesandOptionsInvestmentCompanyIncomeStartDate" ,
                "OLD"."IssueDate" AS "OLD_IssueDate" ,
                "OLD"."SerialNumber" AS "OLD_SerialNumber" ,
                "OLD"."Subtype" AS "OLD_Subtype" ,
                "OLD"."Type" AS "OLD_Type" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TaxExemptStatusDocument" as "OLD"
            on
                                                "IN"."Category" = "OLD"."Category" and
                                                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::TaxExemptStatusDocument"
    where (
        "Category",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Category",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TaxExemptStatusDocument" as "OLD"
        on
                                       "IN"."Category" = "OLD"."Category" and
                                       "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
