PROCEDURE "sap.fsdm.procedures::TaxComplianceDataDelete" (IN ROW "sap.fsdm.tabletypes::TaxComplianceDataTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'TaxComplianceCountry=' || TO_VARCHAR("TaxComplianceCountry") || ' ' ||
                'TaxComplianceType=' || TO_VARCHAR("TaxComplianceType") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."TaxComplianceCountry",
                        "IN"."TaxComplianceType",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."TaxComplianceCountry",
                        "IN"."TaxComplianceType",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "TaxComplianceCountry",
                        "TaxComplianceType",
                        "_BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."TaxComplianceCountry",
                                    "IN"."TaxComplianceType",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."TaxComplianceCountry",
                                    "IN"."TaxComplianceType",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "TaxComplianceCountry" is null and
            "TaxComplianceType" is null and
            "_BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::TaxComplianceData" (
        "TaxComplianceCountry",
        "TaxComplianceType",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "TaxCompliancePartnerAgreementStatus",
        "TaxCompliancePartnerAgreementStatusDate",
        "TaxComplianceStatus",
        "TaxComplianceStatusRatingDate",
        "TaxComplianceStatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_TaxComplianceCountry" as "TaxComplianceCountry" ,
            "OLD_TaxComplianceType" as "TaxComplianceType" ,
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_TaxCompliancePartnerAgreementStatus" as "TaxCompliancePartnerAgreementStatus" ,
            "OLD_TaxCompliancePartnerAgreementStatusDate" as "TaxCompliancePartnerAgreementStatusDate" ,
            "OLD_TaxComplianceStatus" as "TaxComplianceStatus" ,
            "OLD_TaxComplianceStatusRatingDate" as "TaxComplianceStatusRatingDate" ,
            "OLD_TaxComplianceStatusReason" as "TaxComplianceStatusReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."TaxComplianceCountry",
                        "OLD"."TaxComplianceType",
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."TaxComplianceCountry" AS "OLD_TaxComplianceCountry" ,
                "OLD"."TaxComplianceType" AS "OLD_TaxComplianceType" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."TaxCompliancePartnerAgreementStatus" AS "OLD_TaxCompliancePartnerAgreementStatus" ,
                "OLD"."TaxCompliancePartnerAgreementStatusDate" AS "OLD_TaxCompliancePartnerAgreementStatusDate" ,
                "OLD"."TaxComplianceStatus" AS "OLD_TaxComplianceStatus" ,
                "OLD"."TaxComplianceStatusRatingDate" AS "OLD_TaxComplianceStatusRatingDate" ,
                "OLD"."TaxComplianceStatusReason" AS "OLD_TaxComplianceStatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TaxComplianceData" as "OLD"
            on
                      "IN"."TaxComplianceCountry" = "OLD"."TaxComplianceCountry" and
                      "IN"."TaxComplianceType" = "OLD"."TaxComplianceType" and
                      "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::TaxComplianceData" (
        "TaxComplianceCountry",
        "TaxComplianceType",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "TaxCompliancePartnerAgreementStatus",
        "TaxCompliancePartnerAgreementStatusDate",
        "TaxComplianceStatus",
        "TaxComplianceStatusRatingDate",
        "TaxComplianceStatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_TaxComplianceCountry" as "TaxComplianceCountry",
            "OLD_TaxComplianceType" as "TaxComplianceType",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_TaxCompliancePartnerAgreementStatus" as "TaxCompliancePartnerAgreementStatus",
            "OLD_TaxCompliancePartnerAgreementStatusDate" as "TaxCompliancePartnerAgreementStatusDate",
            "OLD_TaxComplianceStatus" as "TaxComplianceStatus",
            "OLD_TaxComplianceStatusRatingDate" as "TaxComplianceStatusRatingDate",
            "OLD_TaxComplianceStatusReason" as "TaxComplianceStatusReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."TaxComplianceCountry",
                        "OLD"."TaxComplianceType",
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."TaxComplianceCountry" AS "OLD_TaxComplianceCountry" ,
                "OLD"."TaxComplianceType" AS "OLD_TaxComplianceType" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."TaxCompliancePartnerAgreementStatus" AS "OLD_TaxCompliancePartnerAgreementStatus" ,
                "OLD"."TaxCompliancePartnerAgreementStatusDate" AS "OLD_TaxCompliancePartnerAgreementStatusDate" ,
                "OLD"."TaxComplianceStatus" AS "OLD_TaxComplianceStatus" ,
                "OLD"."TaxComplianceStatusRatingDate" AS "OLD_TaxComplianceStatusRatingDate" ,
                "OLD"."TaxComplianceStatusReason" AS "OLD_TaxComplianceStatusReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TaxComplianceData" as "OLD"
            on
                                                "IN"."TaxComplianceCountry" = "OLD"."TaxComplianceCountry" and
                                                "IN"."TaxComplianceType" = "OLD"."TaxComplianceType" and
                                                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::TaxComplianceData"
    where (
        "TaxComplianceCountry",
        "TaxComplianceType",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."TaxComplianceCountry",
            "OLD"."TaxComplianceType",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TaxComplianceData" as "OLD"
        on
                                       "IN"."TaxComplianceCountry" = "OLD"."TaxComplianceCountry" and
                                       "IN"."TaxComplianceType" = "OLD"."TaxComplianceType" and
                                       "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
