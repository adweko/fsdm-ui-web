PROCEDURE "sap.fsdm.procedures::CreditRiskAdjustmentErase" (IN ROW "sap.fsdm.tabletypes::CreditRiskAdjustmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountingChangeSequenceNumber" is null and
            "CalculationMethod" is null and
            "IndicatorResultBeforeChange" is null and
            "LotID" is null and
            "RiskProvisionScenario" is null and
            "RiskProvisionSubType" is null and
            "RiskProvisionType" is null and
            "ASSOC_AccountingSystem.AccountingSystemID" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" is null and
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CreditRiskAdjustment"
        WHERE
        (            "AccountingChangeSequenceNumber" ,
            "CalculationMethod" ,
            "IndicatorResultBeforeChange" ,
            "LotID" ,
            "RiskProvisionScenario" ,
            "RiskProvisionSubType" ,
            "RiskProvisionType" ,
            "ASSOC_AccountingSystem.AccountingSystemID" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" 
        ) in
        (
            select                 "OLD"."AccountingChangeSequenceNumber" ,
                "OLD"."CalculationMethod" ,
                "OLD"."IndicatorResultBeforeChange" ,
                "OLD"."LotID" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RiskProvisionSubType" ,
                "OLD"."RiskProvisionType" ,
                "OLD"."ASSOC_AccountingSystem.AccountingSystemID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskAdjustment" "OLD"
            on
            "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
            "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
            "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
            "IN"."LotID" = "OLD"."LotID" and
            "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
            "IN"."RiskProvisionSubType" = "OLD"."RiskProvisionSubType" and
            "IN"."RiskProvisionType" = "OLD"."RiskProvisionType" and
            "IN"."ASSOC_AccountingSystem.AccountingSystemID" = "OLD"."ASSOC_AccountingSystem.AccountingSystemID" and
            "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
            "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
            "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
            "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
            "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::CreditRiskAdjustment_Historical"
        WHERE
        (
            "AccountingChangeSequenceNumber" ,
            "CalculationMethod" ,
            "IndicatorResultBeforeChange" ,
            "LotID" ,
            "RiskProvisionScenario" ,
            "RiskProvisionSubType" ,
            "RiskProvisionType" ,
            "ASSOC_AccountingSystem.AccountingSystemID" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" 
        ) in
        (
            select
                "OLD"."AccountingChangeSequenceNumber" ,
                "OLD"."CalculationMethod" ,
                "OLD"."IndicatorResultBeforeChange" ,
                "OLD"."LotID" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RiskProvisionSubType" ,
                "OLD"."RiskProvisionType" ,
                "OLD"."ASSOC_AccountingSystem.AccountingSystemID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskAdjustment_Historical" "OLD"
            on
                "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
                "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
                "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."RiskProvisionSubType" = "OLD"."RiskProvisionSubType" and
                "IN"."RiskProvisionType" = "OLD"."RiskProvisionType" and
                "IN"."ASSOC_AccountingSystem.AccountingSystemID" = "OLD"."ASSOC_AccountingSystem.AccountingSystemID" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        );

END
