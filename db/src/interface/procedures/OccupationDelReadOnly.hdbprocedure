PROCEDURE "sap.fsdm.procedures::OccupationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::OccupationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::OccupationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::OccupationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_IndividualPerson.BusinessPartnerID=' || TO_VARCHAR("ASSOC_IndividualPerson.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_IndividualPerson.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_IndividualPerson.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_IndividualPerson.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_IndividualPerson.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_IndividualPerson.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_IndividualPerson.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceNumber",
            "ASSOC_IndividualPerson.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Occupation" WHERE
            (
            "SequenceNumber" ,
            "ASSOC_IndividualPerson.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_IndividualPerson.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Occupation" as "OLD"
        on
                              "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                              "IN"."ASSOC_IndividualPerson.BusinessPartnerID" = "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceNumber",
        "ASSOC_IndividualPerson.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Designation",
        "Employer",
        "EmployerClass",
        "EmployerCode",
        "EmployerSector",
        "GrossIncomeCurrency",
        "GrossIncomeTimeUnit",
        "NetIncomeCurrency",
        "NetIncomeEvidenceProvided",
        "NetIncomeTimeUnit",
        "Occupation",
        "Profession",
        "RegularGrossIncome",
        "RegularNetIncome",
        "RetiredSince",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_IndividualPerson.BusinessPartnerID" as "ASSOC_IndividualPerson.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Designation" as "Designation" ,
            "OLD_Employer" as "Employer" ,
            "OLD_EmployerClass" as "EmployerClass" ,
            "OLD_EmployerCode" as "EmployerCode" ,
            "OLD_EmployerSector" as "EmployerSector" ,
            "OLD_GrossIncomeCurrency" as "GrossIncomeCurrency" ,
            "OLD_GrossIncomeTimeUnit" as "GrossIncomeTimeUnit" ,
            "OLD_NetIncomeCurrency" as "NetIncomeCurrency" ,
            "OLD_NetIncomeEvidenceProvided" as "NetIncomeEvidenceProvided" ,
            "OLD_NetIncomeTimeUnit" as "NetIncomeTimeUnit" ,
            "OLD_Occupation" as "Occupation" ,
            "OLD_Profession" as "Profession" ,
            "OLD_RegularGrossIncome" as "RegularGrossIncome" ,
            "OLD_RegularNetIncome" as "RegularNetIncome" ,
            "OLD_RetiredSince" as "RetiredSince" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_IndividualPerson.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" AS "OLD_ASSOC_IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Designation" AS "OLD_Designation" ,
                "OLD"."Employer" AS "OLD_Employer" ,
                "OLD"."EmployerClass" AS "OLD_EmployerClass" ,
                "OLD"."EmployerCode" AS "OLD_EmployerCode" ,
                "OLD"."EmployerSector" AS "OLD_EmployerSector" ,
                "OLD"."GrossIncomeCurrency" AS "OLD_GrossIncomeCurrency" ,
                "OLD"."GrossIncomeTimeUnit" AS "OLD_GrossIncomeTimeUnit" ,
                "OLD"."NetIncomeCurrency" AS "OLD_NetIncomeCurrency" ,
                "OLD"."NetIncomeEvidenceProvided" AS "OLD_NetIncomeEvidenceProvided" ,
                "OLD"."NetIncomeTimeUnit" AS "OLD_NetIncomeTimeUnit" ,
                "OLD"."Occupation" AS "OLD_Occupation" ,
                "OLD"."Profession" AS "OLD_Profession" ,
                "OLD"."RegularGrossIncome" AS "OLD_RegularGrossIncome" ,
                "OLD"."RegularNetIncome" AS "OLD_RegularNetIncome" ,
                "OLD"."RetiredSince" AS "OLD_RetiredSince" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Occupation" as "OLD"
            on
                                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                      "IN"."ASSOC_IndividualPerson.BusinessPartnerID" = "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_IndividualPerson.BusinessPartnerID" as "ASSOC_IndividualPerson.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Designation" as "Designation",
            "OLD_Employer" as "Employer",
            "OLD_EmployerClass" as "EmployerClass",
            "OLD_EmployerCode" as "EmployerCode",
            "OLD_EmployerSector" as "EmployerSector",
            "OLD_GrossIncomeCurrency" as "GrossIncomeCurrency",
            "OLD_GrossIncomeTimeUnit" as "GrossIncomeTimeUnit",
            "OLD_NetIncomeCurrency" as "NetIncomeCurrency",
            "OLD_NetIncomeEvidenceProvided" as "NetIncomeEvidenceProvided",
            "OLD_NetIncomeTimeUnit" as "NetIncomeTimeUnit",
            "OLD_Occupation" as "Occupation",
            "OLD_Profession" as "Profession",
            "OLD_RegularGrossIncome" as "RegularGrossIncome",
            "OLD_RegularNetIncome" as "RegularNetIncome",
            "OLD_RetiredSince" as "RetiredSince",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_IndividualPerson.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" AS "OLD_ASSOC_IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."Designation" AS "OLD_Designation" ,
                "OLD"."Employer" AS "OLD_Employer" ,
                "OLD"."EmployerClass" AS "OLD_EmployerClass" ,
                "OLD"."EmployerCode" AS "OLD_EmployerCode" ,
                "OLD"."EmployerSector" AS "OLD_EmployerSector" ,
                "OLD"."GrossIncomeCurrency" AS "OLD_GrossIncomeCurrency" ,
                "OLD"."GrossIncomeTimeUnit" AS "OLD_GrossIncomeTimeUnit" ,
                "OLD"."NetIncomeCurrency" AS "OLD_NetIncomeCurrency" ,
                "OLD"."NetIncomeEvidenceProvided" AS "OLD_NetIncomeEvidenceProvided" ,
                "OLD"."NetIncomeTimeUnit" AS "OLD_NetIncomeTimeUnit" ,
                "OLD"."Occupation" AS "OLD_Occupation" ,
                "OLD"."Profession" AS "OLD_Profession" ,
                "OLD"."RegularGrossIncome" AS "OLD_RegularGrossIncome" ,
                "OLD"."RegularNetIncome" AS "OLD_RegularNetIncome" ,
                "OLD"."RetiredSince" AS "OLD_RetiredSince" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Occupation" as "OLD"
            on
               "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
               "IN"."ASSOC_IndividualPerson.BusinessPartnerID" = "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
