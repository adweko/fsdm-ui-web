PROCEDURE "sap.fsdm.procedures::GLAccountFinancialContractAssignmentLoad" (IN ROW "sap.fsdm.tabletypes::GLAccountFinancialContractAssignmentTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_GLAccount.GLAccount=' || TO_VARCHAR("_GLAccount.GLAccount") || ' ' ||
                '_GLAccount._ChartOfAccounts.ChartOfAccountId=' || TO_VARCHAR("_GLAccount._ChartOfAccounts.ChartOfAccountId") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_GLAccount.GLAccount",
                        "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_GLAccount.GLAccount",
                        "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_GLAccount.GLAccount",
                        "_GLAccount._ChartOfAccounts.ChartOfAccountId"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_GLAccount.GLAccount",
                                    "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_GLAccount.GLAccount",
                                    "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::GLAccountFinancialContractAssignment" (
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_GLAccount.GLAccount",
        "_GLAccount._ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__GLAccount.GLAccount" as "_GLAccount.GLAccount" ,
            "OLD__GLAccount._ChartOfAccounts.ChartOfAccountId" as "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_GLAccount.GLAccount",
                        "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_GLAccount.GLAccount" as "OLD__GLAccount.GLAccount",
                                "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" as "OLD__GLAccount._ChartOfAccounts.ChartOfAccountId",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::GLAccountFinancialContractAssignment" as "OLD"
            on
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_GLAccount.GLAccount", '') = "OLD"."_GLAccount.GLAccount" and
                ifnull( "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId", '') = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::GLAccountFinancialContractAssignment" (
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_GLAccount.GLAccount",
        "_GLAccount._ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__GLAccount.GLAccount" as "_GLAccount.GLAccount",
            "OLD__GLAccount._ChartOfAccounts.ChartOfAccountId" as "_GLAccount._ChartOfAccounts.ChartOfAccountId",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_GLAccount.GLAccount",
                        "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_GLAccount.GLAccount" as "OLD__GLAccount.GLAccount",
                        "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" as "OLD__GLAccount._ChartOfAccounts.ChartOfAccountId",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::GLAccountFinancialContractAssignment" as "OLD"
            on
                ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_GLAccount.GLAccount", '' ) = "OLD"."_GLAccount.GLAccount" and
                ifnull( "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId", '' ) = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::GLAccountFinancialContractAssignment"
    where (
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_GLAccount.GLAccount",
        "_GLAccount._ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_GLAccount.GLAccount",
            "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::GLAccountFinancialContractAssignment" as "OLD"
        on
           ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
           ifnull( "IN"."_GLAccount.GLAccount", '' ) = "OLD"."_GLAccount.GLAccount" and
           ifnull( "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId", '' ) = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::GLAccountFinancialContractAssignment" (
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_GLAccount.GLAccount",
        "_GLAccount._ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
            ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
            ifnull( "_GLAccount.GLAccount", '' ) as "_GLAccount.GLAccount",
            ifnull( "_GLAccount._ChartOfAccounts.ChartOfAccountId", '' ) as "_GLAccount._ChartOfAccounts.ChartOfAccountId",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END