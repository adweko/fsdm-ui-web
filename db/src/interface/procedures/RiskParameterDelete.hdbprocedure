PROCEDURE "sap.fsdm.procedures::RiskParameterDelete" (IN ROW "sap.fsdm.tabletypes::RiskParameterTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'EstimationMethod=' || TO_VARCHAR("EstimationMethod") || ' ' ||
                'RiskParameterType=' || TO_VARCHAR("RiskParameterType") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'TimeHorizon=' || TO_VARCHAR("TimeHorizon") || ' ' ||
                'TimeHorizonUnit=' || TO_VARCHAR("TimeHorizonUnit") || ' ' ||
                '_Segment.SegmentID=' || TO_VARCHAR("_Segment.SegmentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."EstimationMethod",
                        "IN"."RiskParameterType",
                        "IN"."RiskProvisionScenario",
                        "IN"."TimeHorizon",
                        "IN"."TimeHorizonUnit",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."EstimationMethod",
                        "IN"."RiskParameterType",
                        "IN"."RiskProvisionScenario",
                        "IN"."TimeHorizon",
                        "IN"."TimeHorizonUnit",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "EstimationMethod",
                        "RiskParameterType",
                        "RiskProvisionScenario",
                        "TimeHorizon",
                        "TimeHorizonUnit",
                        "_Segment.SegmentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."EstimationMethod",
                                    "IN"."RiskParameterType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeHorizonUnit",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."EstimationMethod",
                                    "IN"."RiskParameterType",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeHorizonUnit",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "EstimationMethod" is null and
            "RiskParameterType" is null and
            "RiskProvisionScenario" is null and
            "TimeHorizon" is null and
            "TimeHorizonUnit" is null and
            "_Segment.SegmentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::RiskParameter" (
        "EstimationMethod",
        "RiskParameterType",
        "RiskProvisionScenario",
        "TimeHorizon",
        "TimeHorizonUnit",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "Rate",
        "RateBasedOnMarginsOfConservatism",
        "RiskParameterCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_EstimationMethod" as "EstimationMethod" ,
            "OLD_RiskParameterType" as "RiskParameterType" ,
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
            "OLD_TimeHorizon" as "TimeHorizon" ,
            "OLD_TimeHorizonUnit" as "TimeHorizonUnit" ,
            "OLD__Segment.SegmentID" as "_Segment.SegmentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA" ,
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB" ,
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC" ,
            "OLD_Rate" as "Rate" ,
            "OLD_RateBasedOnMarginsOfConservatism" as "RateBasedOnMarginsOfConservatism" ,
            "OLD_RiskParameterCategory" as "RiskParameterCategory" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."EstimationMethod",
                        "OLD"."RiskParameterType",
                        "OLD"."RiskProvisionScenario",
                        "OLD"."TimeHorizon",
                        "OLD"."TimeHorizonUnit",
                        "OLD"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."EstimationMethod" AS "OLD_EstimationMethod" ,
                "OLD"."RiskParameterType" AS "OLD_RiskParameterType" ,
                "OLD"."RiskProvisionScenario" AS "OLD_RiskProvisionScenario" ,
                "OLD"."TimeHorizon" AS "OLD_TimeHorizon" ,
                "OLD"."TimeHorizonUnit" AS "OLD_TimeHorizonUnit" ,
                "OLD"."_Segment.SegmentID" AS "OLD__Segment.SegmentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."MarginOfConservatismCategoryA" AS "OLD_MarginOfConservatismCategoryA" ,
                "OLD"."MarginOfConservatismCategoryB" AS "OLD_MarginOfConservatismCategoryB" ,
                "OLD"."MarginOfConservatismCategoryC" AS "OLD_MarginOfConservatismCategoryC" ,
                "OLD"."Rate" AS "OLD_Rate" ,
                "OLD"."RateBasedOnMarginsOfConservatism" AS "OLD_RateBasedOnMarginsOfConservatism" ,
                "OLD"."RiskParameterCategory" AS "OLD_RiskParameterCategory" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RiskParameter" as "OLD"
            on
                      "IN"."EstimationMethod" = "OLD"."EstimationMethod" and
                      "IN"."RiskParameterType" = "OLD"."RiskParameterType" and
                      "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                      "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                      "IN"."TimeHorizonUnit" = "OLD"."TimeHorizonUnit" and
                      "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RiskParameter" (
        "EstimationMethod",
        "RiskParameterType",
        "RiskProvisionScenario",
        "TimeHorizon",
        "TimeHorizonUnit",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "Rate",
        "RateBasedOnMarginsOfConservatism",
        "RiskParameterCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_EstimationMethod" as "EstimationMethod",
            "OLD_RiskParameterType" as "RiskParameterType",
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario",
            "OLD_TimeHorizon" as "TimeHorizon",
            "OLD_TimeHorizonUnit" as "TimeHorizonUnit",
            "OLD__Segment.SegmentID" as "_Segment.SegmentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA",
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB",
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC",
            "OLD_Rate" as "Rate",
            "OLD_RateBasedOnMarginsOfConservatism" as "RateBasedOnMarginsOfConservatism",
            "OLD_RiskParameterCategory" as "RiskParameterCategory",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."EstimationMethod",
                        "OLD"."RiskParameterType",
                        "OLD"."RiskProvisionScenario",
                        "OLD"."TimeHorizon",
                        "OLD"."TimeHorizonUnit",
                        "OLD"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."EstimationMethod" AS "OLD_EstimationMethod" ,
                "OLD"."RiskParameterType" AS "OLD_RiskParameterType" ,
                "OLD"."RiskProvisionScenario" AS "OLD_RiskProvisionScenario" ,
                "OLD"."TimeHorizon" AS "OLD_TimeHorizon" ,
                "OLD"."TimeHorizonUnit" AS "OLD_TimeHorizonUnit" ,
                "OLD"."_Segment.SegmentID" AS "OLD__Segment.SegmentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."MarginOfConservatismCategoryA" AS "OLD_MarginOfConservatismCategoryA" ,
                "OLD"."MarginOfConservatismCategoryB" AS "OLD_MarginOfConservatismCategoryB" ,
                "OLD"."MarginOfConservatismCategoryC" AS "OLD_MarginOfConservatismCategoryC" ,
                "OLD"."Rate" AS "OLD_Rate" ,
                "OLD"."RateBasedOnMarginsOfConservatism" AS "OLD_RateBasedOnMarginsOfConservatism" ,
                "OLD"."RiskParameterCategory" AS "OLD_RiskParameterCategory" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::RiskParameter" as "OLD"
            on
                                                "IN"."EstimationMethod" = "OLD"."EstimationMethod" and
                                                "IN"."RiskParameterType" = "OLD"."RiskParameterType" and
                                                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                                                "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                                                "IN"."TimeHorizonUnit" = "OLD"."TimeHorizonUnit" and
                                                "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::RiskParameter"
    where (
        "EstimationMethod",
        "RiskParameterType",
        "RiskProvisionScenario",
        "TimeHorizon",
        "TimeHorizonUnit",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."EstimationMethod",
            "OLD"."RiskParameterType",
            "OLD"."RiskProvisionScenario",
            "OLD"."TimeHorizon",
            "OLD"."TimeHorizonUnit",
            "OLD"."_Segment.SegmentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RiskParameter" as "OLD"
        on
                                       "IN"."EstimationMethod" = "OLD"."EstimationMethod" and
                                       "IN"."RiskParameterType" = "OLD"."RiskParameterType" and
                                       "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                                       "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                                       "IN"."TimeHorizonUnit" = "OLD"."TimeHorizonUnit" and
                                       "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
