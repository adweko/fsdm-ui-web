PROCEDURE "sap.fsdm.procedures::CounterpartyCreditRiskHedingSetResultsEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::CounterpartyCreditRiskHedingSetResultsTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::CounterpartyCreditRiskHedingSetResultsTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::CounterpartyCreditRiskHedingSetResultsTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "HedgingSetID" is null and
            "MarginedNettingSet" is null and
            "_ExchangeTradedNettingSet.FinancialInstrumentID" is null and
            "_FinancialContractNettingSet.FinancialContractID" is null and
            "_FinancialContractNettingSet.IDSystem" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" is null and
            "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "HedgingSetID" ,
                "MarginedNettingSet" ,
                "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
                "_FinancialContractNettingSet.FinancialContractID" ,
                "_FinancialContractNettingSet.IDSystem" ,
                "_ResultGroup.ResultDataProvider" ,
                "_ResultGroup.ResultGroupID" ,
                "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
                "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."HedgingSetID" ,
                "OLD"."MarginedNettingSet" ,
                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" ,
                "OLD"."_FinancialContractNettingSet.FinancialContractID" ,
                "OLD"."_FinancialContractNettingSet.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskHedingSetResults" "OLD"
            on
                "IN"."HedgingSetID" = "OLD"."HedgingSetID" and
                "IN"."MarginedNettingSet" = "OLD"."MarginedNettingSet" and
                "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID" = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                "IN"."_FinancialContractNettingSet.FinancialContractID" = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                "IN"."_FinancialContractNettingSet.IDSystem" = "OLD"."_FinancialContractNettingSet.IDSystem" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "HedgingSetID" ,
            "MarginedNettingSet" ,
            "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
            "_FinancialContractNettingSet.FinancialContractID" ,
            "_FinancialContractNettingSet.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
            "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."HedgingSetID" ,
                "OLD"."MarginedNettingSet" ,
                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" ,
                "OLD"."_FinancialContractNettingSet.FinancialContractID" ,
                "OLD"."_FinancialContractNettingSet.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskHedingSetResults_Historical" "OLD"
            on
                "IN"."HedgingSetID" = "OLD"."HedgingSetID" and
                "IN"."MarginedNettingSet" = "OLD"."MarginedNettingSet" and
                "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID" = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                "IN"."_FinancialContractNettingSet.FinancialContractID" = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                "IN"."_FinancialContractNettingSet.IDSystem" = "OLD"."_FinancialContractNettingSet.IDSystem" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
        );

END
