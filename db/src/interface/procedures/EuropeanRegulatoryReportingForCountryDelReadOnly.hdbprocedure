PROCEDURE "sap.fsdm.procedures::EuropeanRegulatoryReportingForCountryDelReadOnly" (IN ROW "sap.fsdm.tabletypes::EuropeanRegulatoryReportingForCountryTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::EuropeanRegulatoryReportingForCountryTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EuropeanRegulatoryReportingForCountryTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Country.GeographicalStructureID=' || TO_VARCHAR("_Country.GeographicalStructureID") || ' ' ||
                '_Country.GeographicalUnitID=' || TO_VARCHAR("_Country.GeographicalUnitID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Country.GeographicalStructureID",
                        "IN"."_Country.GeographicalUnitID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Country.GeographicalStructureID",
                        "IN"."_Country.GeographicalUnitID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Country.GeographicalStructureID",
                        "_Country.GeographicalUnitID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Country.GeographicalStructureID",
                                    "IN"."_Country.GeographicalUnitID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Country.GeographicalStructureID",
                                    "IN"."_Country.GeographicalUnitID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Country.GeographicalStructureID" is null and
            "_Country.GeographicalUnitID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_Country.GeographicalStructureID",
            "_Country.GeographicalUnitID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::EuropeanRegulatoryReportingForCountry" WHERE
            (
            "_Country.GeographicalStructureID" ,
            "_Country.GeographicalUnitID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_Country.GeographicalStructureID",
            "OLD"."_Country.GeographicalUnitID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::EuropeanRegulatoryReportingForCountry" as "OLD"
        on
                              "IN"."_Country.GeographicalStructureID" = "OLD"."_Country.GeographicalStructureID" and
                              "IN"."_Country.GeographicalUnitID" = "OLD"."_Country.GeographicalUnitID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_Country.GeographicalStructureID",
        "_Country.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CREEligibleForReducedRiskWeight",
        "RREEligibleForReducedRiskWeight",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__Country.GeographicalStructureID" as "_Country.GeographicalStructureID" ,
            "OLD__Country.GeographicalUnitID" as "_Country.GeographicalUnitID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CREEligibleForReducedRiskWeight" as "CREEligibleForReducedRiskWeight" ,
            "OLD_RREEligibleForReducedRiskWeight" as "RREEligibleForReducedRiskWeight" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Country.GeographicalStructureID",
                        "OLD"."_Country.GeographicalUnitID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_Country.GeographicalStructureID" AS "OLD__Country.GeographicalStructureID" ,
                "OLD"."_Country.GeographicalUnitID" AS "OLD__Country.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CREEligibleForReducedRiskWeight" AS "OLD_CREEligibleForReducedRiskWeight" ,
                "OLD"."RREEligibleForReducedRiskWeight" AS "OLD_RREEligibleForReducedRiskWeight" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EuropeanRegulatoryReportingForCountry" as "OLD"
            on
                                      "IN"."_Country.GeographicalStructureID" = "OLD"."_Country.GeographicalStructureID" and
                                      "IN"."_Country.GeographicalUnitID" = "OLD"."_Country.GeographicalUnitID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__Country.GeographicalStructureID" as "_Country.GeographicalStructureID",
            "OLD__Country.GeographicalUnitID" as "_Country.GeographicalUnitID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CREEligibleForReducedRiskWeight" as "CREEligibleForReducedRiskWeight",
            "OLD_RREEligibleForReducedRiskWeight" as "RREEligibleForReducedRiskWeight",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Country.GeographicalStructureID",
                        "OLD"."_Country.GeographicalUnitID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Country.GeographicalStructureID" AS "OLD__Country.GeographicalStructureID" ,
                "OLD"."_Country.GeographicalUnitID" AS "OLD__Country.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CREEligibleForReducedRiskWeight" AS "OLD_CREEligibleForReducedRiskWeight" ,
                "OLD"."RREEligibleForReducedRiskWeight" AS "OLD_RREEligibleForReducedRiskWeight" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EuropeanRegulatoryReportingForCountry" as "OLD"
            on
               "IN"."_Country.GeographicalStructureID" = "OLD"."_Country.GeographicalStructureID" and
               "IN"."_Country.GeographicalUnitID" = "OLD"."_Country.GeographicalUnitID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
