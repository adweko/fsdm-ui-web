PROCEDURE "sap.fsdm.procedures::TradeStatusDelReadOnly" (IN ROW "sap.fsdm.tabletypes::TradeStatusTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::TradeStatusTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::TradeStatusTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'TradeStatusCategory=' || TO_VARCHAR("TradeStatusCategory") || ' ' ||
                'TradeStatusType=' || TO_VARCHAR("TradeStatusType") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."TradeStatusCategory",
                        "IN"."TradeStatusType",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."TradeStatusCategory",
                        "IN"."TradeStatusType",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "TradeStatusCategory",
                        "TradeStatusType",
                        "_Trade.IDSystem",
                        "_Trade.TradeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."TradeStatusCategory",
                                    "IN"."TradeStatusType",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."TradeStatusCategory",
                                    "IN"."TradeStatusType",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "TradeStatusCategory" is null and
            "TradeStatusType" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "TradeStatusCategory",
            "TradeStatusType",
            "_Trade.IDSystem",
            "_Trade.TradeID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::TradeStatus" WHERE
            (
            "TradeStatusCategory" ,
            "TradeStatusType" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."TradeStatusCategory",
            "OLD"."TradeStatusType",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TradeStatus" as "OLD"
        on
                              "IN"."TradeStatusCategory" = "OLD"."TradeStatusCategory" and
                              "IN"."TradeStatusType" = "OLD"."TradeStatusType" and
                              "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                              "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "TradeStatusCategory",
        "TradeStatusType",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_EmployeeResponsibleForTradeStatusChange.BusinessPartnerID",
        "Status",
        "StatusChangeDate",
        "StatusChangeReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_TradeStatusCategory" as "TradeStatusCategory" ,
            "OLD_TradeStatusType" as "TradeStatusType" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" as "_EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeDate" as "StatusChangeDate" ,
            "OLD_StatusChangeReason" as "StatusChangeReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."TradeStatusCategory",
                        "OLD"."TradeStatusType",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."TradeStatusCategory" AS "OLD_TradeStatusCategory" ,
                "OLD"."TradeStatusType" AS "OLD_TradeStatusType" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" AS "OLD__EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TradeStatus" as "OLD"
            on
                                      "IN"."TradeStatusCategory" = "OLD"."TradeStatusCategory" and
                                      "IN"."TradeStatusType" = "OLD"."TradeStatusType" and
                                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_TradeStatusCategory" as "TradeStatusCategory",
            "OLD_TradeStatusType" as "TradeStatusType",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" as "_EmployeeResponsibleForTradeStatusChange.BusinessPartnerID",
            "OLD_Status" as "Status",
            "OLD_StatusChangeDate" as "StatusChangeDate",
            "OLD_StatusChangeReason" as "StatusChangeReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."TradeStatusCategory",
                        "OLD"."TradeStatusType",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."TradeStatusCategory" AS "OLD_TradeStatusCategory" ,
                "OLD"."TradeStatusType" AS "OLD_TradeStatusType" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" AS "OLD__EmployeeResponsibleForTradeStatusChange.BusinessPartnerID" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeDate" AS "OLD_StatusChangeDate" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TradeStatus" as "OLD"
            on
               "IN"."TradeStatusCategory" = "OLD"."TradeStatusCategory" and
               "IN"."TradeStatusType" = "OLD"."TradeStatusType" and
               "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
               "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
