PROCEDURE "sap.fsdm.procedures::TrancheInSyndicationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::TrancheInSyndicationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::TrancheInSyndicationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::TrancheInSyndicationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'TrancheSequenceNumber=' || TO_VARCHAR("TrancheSequenceNumber") || ' ' ||
                '_SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_SyndicationAgreement.FinancialContractID") || ' ' ||
                '_SyndicationAgreement.IDSystem=' || TO_VARCHAR("_SyndicationAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."TrancheSequenceNumber",
                        "IN"."_SyndicationAgreement.FinancialContractID",
                        "IN"."_SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."TrancheSequenceNumber",
                        "IN"."_SyndicationAgreement.FinancialContractID",
                        "IN"."_SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "TrancheSequenceNumber",
                        "_SyndicationAgreement.FinancialContractID",
                        "_SyndicationAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."TrancheSequenceNumber",
                                    "IN"."_SyndicationAgreement.FinancialContractID",
                                    "IN"."_SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."TrancheSequenceNumber",
                                    "IN"."_SyndicationAgreement.FinancialContractID",
                                    "IN"."_SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "TrancheSequenceNumber" is null and
            "_SyndicationAgreement.FinancialContractID" is null and
            "_SyndicationAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "TrancheSequenceNumber",
            "_SyndicationAgreement.FinancialContractID",
            "_SyndicationAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::TrancheInSyndication" WHERE
            (
            "TrancheSequenceNumber" ,
            "_SyndicationAgreement.FinancialContractID" ,
            "_SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."TrancheSequenceNumber",
            "OLD"."_SyndicationAgreement.FinancialContractID",
            "OLD"."_SyndicationAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TrancheInSyndication" as "OLD"
        on
                              "IN"."TrancheSequenceNumber" = "OLD"."TrancheSequenceNumber" and
                              "IN"."_SyndicationAgreement.FinancialContractID" = "OLD"."_SyndicationAgreement.FinancialContractID" and
                              "IN"."_SyndicationAgreement.IDSystem" = "OLD"."_SyndicationAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "TrancheSequenceNumber",
        "_SyndicationAgreement.FinancialContractID",
        "_SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "IsTrancheRevolving",
        "Status",
        "TrancheAmount",
        "TrancheCurrency",
        "TrancheEndDate",
        "TrancheStartDate",
        "TrancheType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_TrancheSequenceNumber" as "TrancheSequenceNumber" ,
            "OLD__SyndicationAgreement.FinancialContractID" as "_SyndicationAgreement.FinancialContractID" ,
            "OLD__SyndicationAgreement.IDSystem" as "_SyndicationAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_IsTrancheRevolving" as "IsTrancheRevolving" ,
            "OLD_Status" as "Status" ,
            "OLD_TrancheAmount" as "TrancheAmount" ,
            "OLD_TrancheCurrency" as "TrancheCurrency" ,
            "OLD_TrancheEndDate" as "TrancheEndDate" ,
            "OLD_TrancheStartDate" as "TrancheStartDate" ,
            "OLD_TrancheType" as "TrancheType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."TrancheSequenceNumber",
                        "OLD"."_SyndicationAgreement.FinancialContractID",
                        "OLD"."_SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."TrancheSequenceNumber" AS "OLD_TrancheSequenceNumber" ,
                "OLD"."_SyndicationAgreement.FinancialContractID" AS "OLD__SyndicationAgreement.FinancialContractID" ,
                "OLD"."_SyndicationAgreement.IDSystem" AS "OLD__SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."IsTrancheRevolving" AS "OLD_IsTrancheRevolving" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."TrancheAmount" AS "OLD_TrancheAmount" ,
                "OLD"."TrancheCurrency" AS "OLD_TrancheCurrency" ,
                "OLD"."TrancheEndDate" AS "OLD_TrancheEndDate" ,
                "OLD"."TrancheStartDate" AS "OLD_TrancheStartDate" ,
                "OLD"."TrancheType" AS "OLD_TrancheType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TrancheInSyndication" as "OLD"
            on
                                      "IN"."TrancheSequenceNumber" = "OLD"."TrancheSequenceNumber" and
                                      "IN"."_SyndicationAgreement.FinancialContractID" = "OLD"."_SyndicationAgreement.FinancialContractID" and
                                      "IN"."_SyndicationAgreement.IDSystem" = "OLD"."_SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_TrancheSequenceNumber" as "TrancheSequenceNumber",
            "OLD__SyndicationAgreement.FinancialContractID" as "_SyndicationAgreement.FinancialContractID",
            "OLD__SyndicationAgreement.IDSystem" as "_SyndicationAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_IsTrancheRevolving" as "IsTrancheRevolving",
            "OLD_Status" as "Status",
            "OLD_TrancheAmount" as "TrancheAmount",
            "OLD_TrancheCurrency" as "TrancheCurrency",
            "OLD_TrancheEndDate" as "TrancheEndDate",
            "OLD_TrancheStartDate" as "TrancheStartDate",
            "OLD_TrancheType" as "TrancheType",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."TrancheSequenceNumber",
                        "OLD"."_SyndicationAgreement.FinancialContractID",
                        "OLD"."_SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."TrancheSequenceNumber" AS "OLD_TrancheSequenceNumber" ,
                "OLD"."_SyndicationAgreement.FinancialContractID" AS "OLD__SyndicationAgreement.FinancialContractID" ,
                "OLD"."_SyndicationAgreement.IDSystem" AS "OLD__SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."IsTrancheRevolving" AS "OLD_IsTrancheRevolving" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."TrancheAmount" AS "OLD_TrancheAmount" ,
                "OLD"."TrancheCurrency" AS "OLD_TrancheCurrency" ,
                "OLD"."TrancheEndDate" AS "OLD_TrancheEndDate" ,
                "OLD"."TrancheStartDate" AS "OLD_TrancheStartDate" ,
                "OLD"."TrancheType" AS "OLD_TrancheType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TrancheInSyndication" as "OLD"
            on
               "IN"."TrancheSequenceNumber" = "OLD"."TrancheSequenceNumber" and
               "IN"."_SyndicationAgreement.FinancialContractID" = "OLD"."_SyndicationAgreement.FinancialContractID" and
               "IN"."_SyndicationAgreement.IDSystem" = "OLD"."_SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
