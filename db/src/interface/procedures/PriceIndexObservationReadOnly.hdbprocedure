PROCEDURE "sap.fsdm.procedures::PriceIndexObservationReadOnly" (IN ROW "sap.fsdm.tabletypes::PriceIndexObservationTT", OUT CURR_DEL "sap.fsdm.tabletypes::PriceIndexObservationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::PriceIndexObservationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BasePeriod=' || TO_VARCHAR("BasePeriod") || ' ' ||
                'PriceIndexPeriod=' || TO_VARCHAR("PriceIndexPeriod") || ' ' ||
                '_PriceIndex.IndexID=' || TO_VARCHAR("_PriceIndex.IndexID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BasePeriod",
                        "IN"."PriceIndexPeriod",
                        "IN"."_PriceIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BasePeriod",
                        "IN"."PriceIndexPeriod",
                        "IN"."_PriceIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BasePeriod",
                        "PriceIndexPeriod",
                        "_PriceIndex.IndexID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BasePeriod",
                                    "IN"."PriceIndexPeriod",
                                    "IN"."_PriceIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BasePeriod",
                                    "IN"."PriceIndexPeriod",
                                    "IN"."_PriceIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "BasePeriod",
        "PriceIndexPeriod",
        "_PriceIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::PriceIndexObservation" WHERE
        (            "BasePeriod" ,
            "PriceIndexPeriod" ,
            "_PriceIndex.IndexID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."BasePeriod",
            "OLD"."PriceIndexPeriod",
            "OLD"."_PriceIndex.IndexID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::PriceIndexObservation" as "OLD"
            on
               ifnull( "IN"."BasePeriod",'' ) = "OLD"."BasePeriod" and
               ifnull( "IN"."PriceIndexPeriod",'' ) = "OLD"."PriceIndexPeriod" and
               ifnull( "IN"."_PriceIndex.IndexID",'' ) = "OLD"."_PriceIndex.IndexID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "BasePeriod",
        "PriceIndexPeriod",
        "_PriceIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PriceIndexValue",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "BasePeriod", '' ) as "BasePeriod",
                    ifnull( "PriceIndexPeriod", '' ) as "PriceIndexPeriod",
                    ifnull( "_PriceIndex.IndexID", '' ) as "_PriceIndex.IndexID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "PriceIndexValue"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_BasePeriod" as "BasePeriod" ,
                    "OLD_PriceIndexPeriod" as "PriceIndexPeriod" ,
                    "OLD__PriceIndex.IndexID" as "_PriceIndex.IndexID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_PriceIndexValue" as "PriceIndexValue" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."BasePeriod",
                        "IN"."PriceIndexPeriod",
                        "IN"."_PriceIndex.IndexID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."BasePeriod" as "OLD_BasePeriod",
                                "OLD"."PriceIndexPeriod" as "OLD_PriceIndexPeriod",
                                "OLD"."_PriceIndex.IndexID" as "OLD__PriceIndex.IndexID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."PriceIndexValue" as "OLD_PriceIndexValue",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PriceIndexObservation" as "OLD"
            on
                ifnull( "IN"."BasePeriod", '') = "OLD"."BasePeriod" and
                ifnull( "IN"."PriceIndexPeriod", '') = "OLD"."PriceIndexPeriod" and
                ifnull( "IN"."_PriceIndex.IndexID", '') = "OLD"."_PriceIndex.IndexID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_BasePeriod" as "BasePeriod",
            "OLD_PriceIndexPeriod" as "PriceIndexPeriod",
            "OLD__PriceIndex.IndexID" as "_PriceIndex.IndexID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PriceIndexValue" as "PriceIndexValue",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."BasePeriod",
                        "IN"."PriceIndexPeriod",
                        "IN"."_PriceIndex.IndexID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."BasePeriod" as "OLD_BasePeriod",
                        "OLD"."PriceIndexPeriod" as "OLD_PriceIndexPeriod",
                        "OLD"."_PriceIndex.IndexID" as "OLD__PriceIndex.IndexID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."PriceIndexValue" as "OLD_PriceIndexValue",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PriceIndexObservation" as "OLD"
            on
                ifnull("IN"."BasePeriod", '') = "OLD"."BasePeriod" and
                ifnull("IN"."PriceIndexPeriod", '') = "OLD"."PriceIndexPeriod" and
                ifnull("IN"."_PriceIndex.IndexID", '') = "OLD"."_PriceIndex.IndexID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
