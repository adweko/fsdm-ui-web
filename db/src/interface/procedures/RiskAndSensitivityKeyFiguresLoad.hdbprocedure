PROCEDURE "sap.fsdm.procedures::RiskAndSensitivityKeyFiguresLoad" (IN ROW "sap.fsdm.tabletypes::RiskAndSensitivityKeyFiguresTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AssetOrLiability=' || TO_VARCHAR("AssetOrLiability") || ' ' ||
                'Category=' || TO_VARCHAR("Category") || ' ' ||
                'KeyDate=' || TO_VARCHAR("KeyDate") || ' ' ||
                'Provider=' || TO_VARCHAR("Provider") || ' ' ||
                'Scenario=' || TO_VARCHAR("Scenario") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AssetOrLiability",
                        "Category",
                        "KeyDate",
                        "Provider",
                        "Scenario",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AssetOrLiability",
                                    "IN"."Category",
                                    "IN"."KeyDate",
                                    "IN"."Provider",
                                    "IN"."Scenario",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AssetOrLiability",
                                    "IN"."Category",
                                    "IN"."KeyDate",
                                    "IN"."Provider",
                                    "IN"."Scenario",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::RiskAndSensitivityKeyFigures" (
        "AssetOrLiability",
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CalculationMethod",
        "Description",
        "MacaulayDuration",
        "MacaulayDurationTimeUnit",
        "ModifiedDuration",
        "ModifiedDurationTimeUnit",
        "Name",
        "WeightedAverageLife",
        "WeightedAverageLifeTimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AssetOrLiability" as "AssetOrLiability" ,
            "OLD_Category" as "Category" ,
            "OLD_KeyDate" as "KeyDate" ,
            "OLD_Provider" as "Provider" ,
            "OLD_Scenario" as "Scenario" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CalculationMethod" as "CalculationMethod" ,
            "OLD_Description" as "Description" ,
            "OLD_MacaulayDuration" as "MacaulayDuration" ,
            "OLD_MacaulayDurationTimeUnit" as "MacaulayDurationTimeUnit" ,
            "OLD_ModifiedDuration" as "ModifiedDuration" ,
            "OLD_ModifiedDurationTimeUnit" as "ModifiedDurationTimeUnit" ,
            "OLD_Name" as "Name" ,
            "OLD_WeightedAverageLife" as "WeightedAverageLife" ,
            "OLD_WeightedAverageLifeTimeUnit" as "WeightedAverageLifeTimeUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."AssetOrLiability" as "OLD_AssetOrLiability",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."KeyDate" as "OLD_KeyDate",
                                "OLD"."Provider" as "OLD_Provider",
                                "OLD"."Scenario" as "OLD_Scenario",
                                "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                                "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."MacaulayDuration" as "OLD_MacaulayDuration",
                                "OLD"."MacaulayDurationTimeUnit" as "OLD_MacaulayDurationTimeUnit",
                                "OLD"."ModifiedDuration" as "OLD_ModifiedDuration",
                                "OLD"."ModifiedDurationTimeUnit" as "OLD_ModifiedDurationTimeUnit",
                                "OLD"."Name" as "OLD_Name",
                                "OLD"."WeightedAverageLife" as "OLD_WeightedAverageLife",
                                "OLD"."WeightedAverageLifeTimeUnit" as "OLD_WeightedAverageLifeTimeUnit",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RiskAndSensitivityKeyFigures" as "OLD"
            on
                ifnull( "IN"."AssetOrLiability", '') = "OLD"."AssetOrLiability" and
                ifnull( "IN"."Category", '') = "OLD"."Category" and
                ifnull( "IN"."KeyDate", '0001-01-01') = "OLD"."KeyDate" and
                ifnull( "IN"."Provider", '') = "OLD"."Provider" and
                ifnull( "IN"."Scenario", '') = "OLD"."Scenario" and
                ifnull( "IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RiskAndSensitivityKeyFigures" (
        "AssetOrLiability",
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CalculationMethod",
        "Description",
        "MacaulayDuration",
        "MacaulayDurationTimeUnit",
        "ModifiedDuration",
        "ModifiedDurationTimeUnit",
        "Name",
        "WeightedAverageLife",
        "WeightedAverageLifeTimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AssetOrLiability" as "AssetOrLiability",
            "OLD_Category" as "Category",
            "OLD_KeyDate" as "KeyDate",
            "OLD_Provider" as "Provider",
            "OLD_Scenario" as "Scenario",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_Description" as "Description",
            "OLD_MacaulayDuration" as "MacaulayDuration",
            "OLD_MacaulayDurationTimeUnit" as "MacaulayDurationTimeUnit",
            "OLD_ModifiedDuration" as "ModifiedDuration",
            "OLD_ModifiedDurationTimeUnit" as "ModifiedDurationTimeUnit",
            "OLD_Name" as "Name",
            "OLD_WeightedAverageLife" as "WeightedAverageLife",
            "OLD_WeightedAverageLifeTimeUnit" as "WeightedAverageLifeTimeUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."Category",
                        "IN"."KeyDate",
                        "IN"."Provider",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."AssetOrLiability" as "OLD_AssetOrLiability",
                        "OLD"."Category" as "OLD_Category",
                        "OLD"."KeyDate" as "OLD_KeyDate",
                        "OLD"."Provider" as "OLD_Provider",
                        "OLD"."Scenario" as "OLD_Scenario",
                        "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                        "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                        "OLD"."Description" as "OLD_Description",
                        "OLD"."MacaulayDuration" as "OLD_MacaulayDuration",
                        "OLD"."MacaulayDurationTimeUnit" as "OLD_MacaulayDurationTimeUnit",
                        "OLD"."ModifiedDuration" as "OLD_ModifiedDuration",
                        "OLD"."ModifiedDurationTimeUnit" as "OLD_ModifiedDurationTimeUnit",
                        "OLD"."Name" as "OLD_Name",
                        "OLD"."WeightedAverageLife" as "OLD_WeightedAverageLife",
                        "OLD"."WeightedAverageLifeTimeUnit" as "OLD_WeightedAverageLifeTimeUnit",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RiskAndSensitivityKeyFigures" as "OLD"
            on
                ifnull( "IN"."AssetOrLiability", '' ) = "OLD"."AssetOrLiability" and
                ifnull( "IN"."Category", '' ) = "OLD"."Category" and
                ifnull( "IN"."KeyDate", '0001-01-01' ) = "OLD"."KeyDate" and
                ifnull( "IN"."Provider", '' ) = "OLD"."Provider" and
                ifnull( "IN"."Scenario", '' ) = "OLD"."Scenario" and
                ifnull( "IN"."_Collection.CollectionID", '' ) = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '' ) = "OLD"."_Collection.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::RiskAndSensitivityKeyFigures"
    where (
        "AssetOrLiability",
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."AssetOrLiability",
            "OLD"."Category",
            "OLD"."KeyDate",
            "OLD"."Provider",
            "OLD"."Scenario",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RiskAndSensitivityKeyFigures" as "OLD"
        on
           ifnull( "IN"."AssetOrLiability", '' ) = "OLD"."AssetOrLiability" and
           ifnull( "IN"."Category", '' ) = "OLD"."Category" and
           ifnull( "IN"."KeyDate", '0001-01-01' ) = "OLD"."KeyDate" and
           ifnull( "IN"."Provider", '' ) = "OLD"."Provider" and
           ifnull( "IN"."Scenario", '' ) = "OLD"."Scenario" and
           ifnull( "IN"."_Collection.CollectionID", '' ) = "OLD"."_Collection.CollectionID" and
           ifnull( "IN"."_Collection.IDSystem", '' ) = "OLD"."_Collection.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::RiskAndSensitivityKeyFigures" (
        "AssetOrLiability",
        "Category",
        "KeyDate",
        "Provider",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CalculationMethod",
        "Description",
        "MacaulayDuration",
        "MacaulayDurationTimeUnit",
        "ModifiedDuration",
        "ModifiedDurationTimeUnit",
        "Name",
        "WeightedAverageLife",
        "WeightedAverageLifeTimeUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "AssetOrLiability", '' ) as "AssetOrLiability",
            ifnull( "Category", '' ) as "Category",
            ifnull( "KeyDate", '0001-01-01' ) as "KeyDate",
            ifnull( "Provider", '' ) as "Provider",
            ifnull( "Scenario", '' ) as "Scenario",
            ifnull( "_Collection.CollectionID", '' ) as "_Collection.CollectionID",
            ifnull( "_Collection.IDSystem", '' ) as "_Collection.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "CalculationMethod"  ,
            "Description"  ,
            "MacaulayDuration"  ,
            "MacaulayDurationTimeUnit"  ,
            "ModifiedDuration"  ,
            "ModifiedDurationTimeUnit"  ,
            "Name"  ,
            "WeightedAverageLife"  ,
            "WeightedAverageLifeTimeUnit"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END