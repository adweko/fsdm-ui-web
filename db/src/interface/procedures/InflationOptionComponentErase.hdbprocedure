PROCEDURE "sap.fsdm.procedures::InflationOptionComponentErase" (IN ROW "sap.fsdm.tabletypes::InflationOptionComponentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ComponentNumber" is null and
            "_InflationOption.FinancialContractID" is null and
            "_InflationOption.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::InflationOptionComponent"
        WHERE
        (            "ComponentNumber" ,
            "_InflationOption.FinancialContractID" ,
            "_InflationOption.IDSystem" 
        ) in
        (
            select                 "OLD"."ComponentNumber" ,
                "OLD"."_InflationOption.FinancialContractID" ,
                "OLD"."_InflationOption.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::InflationOptionComponent" "OLD"
            on
            "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
            "IN"."_InflationOption.FinancialContractID" = "OLD"."_InflationOption.FinancialContractID" and
            "IN"."_InflationOption.IDSystem" = "OLD"."_InflationOption.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::InflationOptionComponent_Historical"
        WHERE
        (
            "ComponentNumber" ,
            "_InflationOption.FinancialContractID" ,
            "_InflationOption.IDSystem" 
        ) in
        (
            select
                "OLD"."ComponentNumber" ,
                "OLD"."_InflationOption.FinancialContractID" ,
                "OLD"."_InflationOption.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::InflationOptionComponent_Historical" "OLD"
            on
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."_InflationOption.FinancialContractID" = "OLD"."_InflationOption.FinancialContractID" and
                "IN"."_InflationOption.IDSystem" = "OLD"."_InflationOption.IDSystem" 
        );

END
