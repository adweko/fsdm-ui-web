PROCEDURE "sap.fsdm.procedures::RegionDelReadOnly" (IN ROW "sap.fsdm.tabletypes::RegionTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::RegionTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::RegionTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Region=' || TO_VARCHAR("Region") || ' ' ||
                '_ReinsuranceCoverage.CoverageID=' || TO_VARCHAR("_ReinsuranceCoverage.CoverageID") || ' ' ||
                '_ReinsuranceCoverage.SectionID=' || TO_VARCHAR("_ReinsuranceCoverage.SectionID") || ' ' ||
                '_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID=' || TO_VARCHAR("_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID") || ' ' ||
                '_ReinsuranceCoverage._ReinsuranceContract.IDSystem=' || TO_VARCHAR("_ReinsuranceCoverage._ReinsuranceContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Region",
                        "IN"."_ReinsuranceCoverage.CoverageID",
                        "IN"."_ReinsuranceCoverage.SectionID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Region",
                        "IN"."_ReinsuranceCoverage.CoverageID",
                        "IN"."_ReinsuranceCoverage.SectionID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Region",
                        "_ReinsuranceCoverage.CoverageID",
                        "_ReinsuranceCoverage.SectionID",
                        "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Region",
                                    "IN"."_ReinsuranceCoverage.CoverageID",
                                    "IN"."_ReinsuranceCoverage.SectionID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Region",
                                    "IN"."_ReinsuranceCoverage.CoverageID",
                                    "IN"."_ReinsuranceCoverage.SectionID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                                    "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Region" is null and
            "_ReinsuranceCoverage.CoverageID" is null and
            "_ReinsuranceCoverage.SectionID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "Region",
            "_ReinsuranceCoverage.CoverageID",
            "_ReinsuranceCoverage.SectionID",
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Region" WHERE
            (
            "Region" ,
            "_ReinsuranceCoverage.CoverageID" ,
            "_ReinsuranceCoverage.SectionID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."Region",
            "OLD"."_ReinsuranceCoverage.CoverageID",
            "OLD"."_ReinsuranceCoverage.SectionID",
            "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
            "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Region" as "OLD"
        on
                              "IN"."Region" = "OLD"."Region" and
                              "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                              "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                              "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                              "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "Region",
        "_ReinsuranceCoverage.CoverageID",
        "_ReinsuranceCoverage.SectionID",
        "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
        "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DefaultIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_Region" as "Region" ,
            "OLD__ReinsuranceCoverage.CoverageID" as "_ReinsuranceCoverage.CoverageID" ,
            "OLD__ReinsuranceCoverage.SectionID" as "_ReinsuranceCoverage.SectionID" ,
            "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" as "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
            "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" as "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_DefaultIndicator" as "DefaultIndicator" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Region",
                        "OLD"."_ReinsuranceCoverage.CoverageID",
                        "OLD"."_ReinsuranceCoverage.SectionID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."Region" AS "OLD_Region" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" AS "OLD__ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" AS "OLD__ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DefaultIndicator" AS "OLD_DefaultIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Region" as "OLD"
            on
                                      "IN"."Region" = "OLD"."Region" and
                                      "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                                      "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                                      "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                                      "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_Region" as "Region",
            "OLD__ReinsuranceCoverage.CoverageID" as "_ReinsuranceCoverage.CoverageID",
            "OLD__ReinsuranceCoverage.SectionID" as "_ReinsuranceCoverage.SectionID",
            "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" as "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
            "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" as "_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DefaultIndicator" as "DefaultIndicator",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Region",
                        "OLD"."_ReinsuranceCoverage.CoverageID",
                        "OLD"."_ReinsuranceCoverage.SectionID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID",
                        "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Region" AS "OLD_Region" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" AS "OLD__ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" AS "OLD__ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" AS "OLD__ReinsuranceCoverage._ReinsuranceContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DefaultIndicator" AS "OLD_DefaultIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Region" as "OLD"
            on
               "IN"."Region" = "OLD"."Region" and
               "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
               "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
               "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
               "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
