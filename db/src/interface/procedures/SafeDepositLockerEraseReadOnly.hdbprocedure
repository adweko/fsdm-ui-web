PROCEDURE "sap.fsdm.procedures::SafeDepositLockerEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::SafeDepositLockerTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::SafeDepositLockerTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::SafeDepositLockerTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "LockerID" is null and
            "_SafeDepositLockerBranch.BankingChannelID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "LockerID" ,
                "_SafeDepositLockerBranch.BankingChannelID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."LockerID" ,
                "OLD"."_SafeDepositLockerBranch.BankingChannelID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::SafeDepositLocker" "OLD"
            on
                "IN"."LockerID" = "OLD"."LockerID" and
                "IN"."_SafeDepositLockerBranch.BankingChannelID" = "OLD"."_SafeDepositLockerBranch.BankingChannelID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "LockerID" ,
            "_SafeDepositLockerBranch.BankingChannelID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."LockerID" ,
                "OLD"."_SafeDepositLockerBranch.BankingChannelID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::SafeDepositLocker_Historical" "OLD"
            on
                "IN"."LockerID" = "OLD"."LockerID" and
                "IN"."_SafeDepositLockerBranch.BankingChannelID" = "OLD"."_SafeDepositLockerBranch.BankingChannelID" 
        );

END
