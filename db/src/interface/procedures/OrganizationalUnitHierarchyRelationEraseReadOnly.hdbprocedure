PROCEDURE "sap.fsdm.procedures::OrganizationalUnitHierarchyRelationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::OrganizationalUnitHierarchyRelationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::OrganizationalUnitHierarchyRelationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::OrganizationalUnitHierarchyRelationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "OrganizationalUnitHierarchyID" is null and
            "ASSOC_Child.IDSystem" is null and
            "ASSOC_Child.OrganizationalUnitID" is null and
            "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_Parent.IDSystem" is null and
            "ASSOC_Parent.OrganizationalUnitID" is null and
            "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "OrganizationalUnitHierarchyID" ,
                "ASSOC_Child.IDSystem" ,
                "ASSOC_Child.OrganizationalUnitID" ,
                "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "ASSOC_Parent.IDSystem" ,
                "ASSOC_Parent.OrganizationalUnitID" ,
                "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."OrganizationalUnitHierarchyID" ,
                "OLD"."ASSOC_Child.IDSystem" ,
                "OLD"."ASSOC_Child.OrganizationalUnitID" ,
                "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Parent.IDSystem" ,
                "OLD"."ASSOC_Parent.OrganizationalUnitID" ,
                "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::OrganizationalUnitHierarchyRelation" "OLD"
            on
                "IN"."OrganizationalUnitHierarchyID" = "OLD"."OrganizationalUnitHierarchyID" and
                "IN"."ASSOC_Child.IDSystem" = "OLD"."ASSOC_Child.IDSystem" and
                "IN"."ASSOC_Child.OrganizationalUnitID" = "OLD"."ASSOC_Child.OrganizationalUnitID" and
                "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_Parent.IDSystem" = "OLD"."ASSOC_Parent.IDSystem" and
                "IN"."ASSOC_Parent.OrganizationalUnitID" = "OLD"."ASSOC_Parent.OrganizationalUnitID" and
                "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "OrganizationalUnitHierarchyID" ,
            "ASSOC_Child.IDSystem" ,
            "ASSOC_Child.OrganizationalUnitID" ,
            "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "ASSOC_Parent.IDSystem" ,
            "ASSOC_Parent.OrganizationalUnitID" ,
            "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."OrganizationalUnitHierarchyID" ,
                "OLD"."ASSOC_Child.IDSystem" ,
                "OLD"."ASSOC_Child.OrganizationalUnitID" ,
                "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Parent.IDSystem" ,
                "OLD"."ASSOC_Parent.OrganizationalUnitID" ,
                "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::OrganizationalUnitHierarchyRelation_Historical" "OLD"
            on
                "IN"."OrganizationalUnitHierarchyID" = "OLD"."OrganizationalUnitHierarchyID" and
                "IN"."ASSOC_Child.IDSystem" = "OLD"."ASSOC_Child.IDSystem" and
                "IN"."ASSOC_Child.OrganizationalUnitID" = "OLD"."ASSOC_Child.OrganizationalUnitID" and
                "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."ASSOC_Parent.IDSystem" = "OLD"."ASSOC_Parent.IDSystem" and
                "IN"."ASSOC_Parent.OrganizationalUnitID" = "OLD"."ASSOC_Parent.OrganizationalUnitID" and
                "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        );

END
