PROCEDURE "sap.fsdm.procedures::CoveredFinancialContractAssignmentErase" (IN ROW "sap.fsdm.tabletypes::CoveredFinancialContractAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_CollateralPool.CollectionID" is null and
            "_CollateralPool.IDSystem" is null and
            "_CollateralPool._Client.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CoveredFinancialContractAssignment"
        WHERE
        (            "_CollateralPool.CollectionID" ,
            "_CollateralPool.IDSystem" ,
            "_CollateralPool._Client.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select                 "OLD"."_CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" ,
                "OLD"."_CollateralPool._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CoveredFinancialContractAssignment" "OLD"
            on
            "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
            "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
            "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::CoveredFinancialContractAssignment_Historical"
        WHERE
        (
            "_CollateralPool.CollectionID" ,
            "_CollateralPool.IDSystem" ,
            "_CollateralPool._Client.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select
                "OLD"."_CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" ,
                "OLD"."_CollateralPool._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CoveredFinancialContractAssignment_Historical" "OLD"
            on
                "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

END
