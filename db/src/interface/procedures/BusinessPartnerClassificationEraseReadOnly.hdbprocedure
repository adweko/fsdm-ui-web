PROCEDURE "sap.fsdm.procedures::BusinessPartnerClassificationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerClassificationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::BusinessPartnerClassificationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::BusinessPartnerClassificationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_BusinessPartnerID.BusinessPartnerID" is null and
            "ASSOC_ClassificationOfBusinessPartner.PartnerClass" is null and
            "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
                "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::BusinessPartnerClassification" "OLD"
            on
                "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" and
                "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
            "ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" ,
                "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::BusinessPartnerClassification_Historical" "OLD"
            on
                "IN"."ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerID.BusinessPartnerID" and
                "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClass" and
                "IN"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" = "OLD"."ASSOC_ClassificationOfBusinessPartner.PartnerClassificationSystem" 
        );

END
