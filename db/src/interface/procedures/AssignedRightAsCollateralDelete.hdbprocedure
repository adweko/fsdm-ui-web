PROCEDURE "sap.fsdm.procedures::AssignedRightAsCollateralDelete" (IN ROW "sap.fsdm.tabletypes::AssignedRightAsCollateralTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_SimpleCollateralAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_SimpleCollateralAgreement.FinancialContractID") || ' ' ||
                'ASSOC_SimpleCollateralAgreement.IDSystem=' || TO_VARCHAR("ASSOC_SimpleCollateralAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "ASSOC_SimpleCollateralAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                                    "IN"."ASSOC_SimpleCollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_SimpleCollateralAgreement.FinancialContractID" is null and
            "ASSOC_SimpleCollateralAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::AssignedRightAsCollateral" (
        "SequenceNumber",
        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
        "ASSOC_SimpleCollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
        "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_AssignedFinancialContract.FinancialContractID",
        "ASSOC_AssignedFinancialContract.IDSystem",
        "ASSOC_AssignedReceivable.ReceivableID",
        "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
        "AssignedAmount",
        "AssignedAmountCurrency",
        "AssignedPercentage",
        "AssignmentOfRightCategory",
        "InitialCollateralValue",
        "InitialCollateralValueCurrency",
        "RankAmongAssetsAssignedToCollateralAgreement",
        "RankAmongCollateralAgreementsCoveredByAsset",
        "RoleOfObligorInAssignedContract",
        "ThirdPartyAccountIdentificationSystem",
        "ThirdPartyBankAccountCurrency",
        "ThirdPartyBankAccountHolderName",
        "ThirdPartyBankAccountID",
        "ThirdPartyBankID",
        "ThirdPartyBankIdentificationSystem",
        "ThirdPartyBankName",
        "ThirdPartyIBAN",
        "ThirdPartySWIFT",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID" as "ASSOC_SimpleCollateralAgreement.FinancialContractID" ,
            "OLD_ASSOC_SimpleCollateralAgreement.IDSystem" as "ASSOC_SimpleCollateralAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" as "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" ,
            "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
            "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
            "OLD_ASSOC_AssignedFinancialContract.FinancialContractID" as "ASSOC_AssignedFinancialContract.FinancialContractID" ,
            "OLD_ASSOC_AssignedFinancialContract.IDSystem" as "ASSOC_AssignedFinancialContract.IDSystem" ,
            "OLD_ASSOC_AssignedReceivable.ReceivableID" as "ASSOC_AssignedReceivable.ReceivableID" ,
            "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" as "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" ,
            "OLD_AssignedAmount" as "AssignedAmount" ,
            "OLD_AssignedAmountCurrency" as "AssignedAmountCurrency" ,
            "OLD_AssignedPercentage" as "AssignedPercentage" ,
            "OLD_AssignmentOfRightCategory" as "AssignmentOfRightCategory" ,
            "OLD_InitialCollateralValue" as "InitialCollateralValue" ,
            "OLD_InitialCollateralValueCurrency" as "InitialCollateralValueCurrency" ,
            "OLD_RankAmongAssetsAssignedToCollateralAgreement" as "RankAmongAssetsAssignedToCollateralAgreement" ,
            "OLD_RankAmongCollateralAgreementsCoveredByAsset" as "RankAmongCollateralAgreementsCoveredByAsset" ,
            "OLD_RoleOfObligorInAssignedContract" as "RoleOfObligorInAssignedContract" ,
            "OLD_ThirdPartyAccountIdentificationSystem" as "ThirdPartyAccountIdentificationSystem" ,
            "OLD_ThirdPartyBankAccountCurrency" as "ThirdPartyBankAccountCurrency" ,
            "OLD_ThirdPartyBankAccountHolderName" as "ThirdPartyBankAccountHolderName" ,
            "OLD_ThirdPartyBankAccountID" as "ThirdPartyBankAccountID" ,
            "OLD_ThirdPartyBankID" as "ThirdPartyBankID" ,
            "OLD_ThirdPartyBankIdentificationSystem" as "ThirdPartyBankIdentificationSystem" ,
            "OLD_ThirdPartyBankName" as "ThirdPartyBankName" ,
            "OLD_ThirdPartyIBAN" as "ThirdPartyIBAN" ,
            "OLD_ThirdPartySWIFT" as "ThirdPartySWIFT" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" AS "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" AS "OLD_ASSOC_SimpleCollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" AS "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" ,
                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_AssignedFinancialContract.FinancialContractID" AS "OLD_ASSOC_AssignedFinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_AssignedFinancialContract.IDSystem" AS "OLD_ASSOC_AssignedFinancialContract.IDSystem" ,
                "OLD"."ASSOC_AssignedReceivable.ReceivableID" AS "OLD_ASSOC_AssignedReceivable.ReceivableID" ,
                "OLD"."ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" AS "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" ,
                "OLD"."AssignedAmount" AS "OLD_AssignedAmount" ,
                "OLD"."AssignedAmountCurrency" AS "OLD_AssignedAmountCurrency" ,
                "OLD"."AssignedPercentage" AS "OLD_AssignedPercentage" ,
                "OLD"."AssignmentOfRightCategory" AS "OLD_AssignmentOfRightCategory" ,
                "OLD"."InitialCollateralValue" AS "OLD_InitialCollateralValue" ,
                "OLD"."InitialCollateralValueCurrency" AS "OLD_InitialCollateralValueCurrency" ,
                "OLD"."RankAmongAssetsAssignedToCollateralAgreement" AS "OLD_RankAmongAssetsAssignedToCollateralAgreement" ,
                "OLD"."RankAmongCollateralAgreementsCoveredByAsset" AS "OLD_RankAmongCollateralAgreementsCoveredByAsset" ,
                "OLD"."RoleOfObligorInAssignedContract" AS "OLD_RoleOfObligorInAssignedContract" ,
                "OLD"."ThirdPartyAccountIdentificationSystem" AS "OLD_ThirdPartyAccountIdentificationSystem" ,
                "OLD"."ThirdPartyBankAccountCurrency" AS "OLD_ThirdPartyBankAccountCurrency" ,
                "OLD"."ThirdPartyBankAccountHolderName" AS "OLD_ThirdPartyBankAccountHolderName" ,
                "OLD"."ThirdPartyBankAccountID" AS "OLD_ThirdPartyBankAccountID" ,
                "OLD"."ThirdPartyBankID" AS "OLD_ThirdPartyBankID" ,
                "OLD"."ThirdPartyBankIdentificationSystem" AS "OLD_ThirdPartyBankIdentificationSystem" ,
                "OLD"."ThirdPartyBankName" AS "OLD_ThirdPartyBankName" ,
                "OLD"."ThirdPartyIBAN" AS "OLD_ThirdPartyIBAN" ,
                "OLD"."ThirdPartySWIFT" AS "OLD_ThirdPartySWIFT" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AssignedRightAsCollateral" as "OLD"
            on
                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                      "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID" = "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" and
                      "IN"."ASSOC_SimpleCollateralAgreement.IDSystem" = "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::AssignedRightAsCollateral" (
        "SequenceNumber",
        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
        "ASSOC_SimpleCollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
        "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_AssignedFinancialContract.FinancialContractID",
        "ASSOC_AssignedFinancialContract.IDSystem",
        "ASSOC_AssignedReceivable.ReceivableID",
        "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
        "AssignedAmount",
        "AssignedAmountCurrency",
        "AssignedPercentage",
        "AssignmentOfRightCategory",
        "InitialCollateralValue",
        "InitialCollateralValueCurrency",
        "RankAmongAssetsAssignedToCollateralAgreement",
        "RankAmongCollateralAgreementsCoveredByAsset",
        "RoleOfObligorInAssignedContract",
        "ThirdPartyAccountIdentificationSystem",
        "ThirdPartyBankAccountCurrency",
        "ThirdPartyBankAccountHolderName",
        "ThirdPartyBankAccountID",
        "ThirdPartyBankID",
        "ThirdPartyBankIdentificationSystem",
        "ThirdPartyBankName",
        "ThirdPartyIBAN",
        "ThirdPartySWIFT",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID" as "ASSOC_SimpleCollateralAgreement.FinancialContractID",
            "OLD_ASSOC_SimpleCollateralAgreement.IDSystem" as "ASSOC_SimpleCollateralAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" as "ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType",
            "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
            "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
            "OLD_ASSOC_AssignedFinancialContract.FinancialContractID" as "ASSOC_AssignedFinancialContract.FinancialContractID",
            "OLD_ASSOC_AssignedFinancialContract.IDSystem" as "ASSOC_AssignedFinancialContract.IDSystem",
            "OLD_ASSOC_AssignedReceivable.ReceivableID" as "ASSOC_AssignedReceivable.ReceivableID",
            "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" as "ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID",
            "OLD_AssignedAmount" as "AssignedAmount",
            "OLD_AssignedAmountCurrency" as "AssignedAmountCurrency",
            "OLD_AssignedPercentage" as "AssignedPercentage",
            "OLD_AssignmentOfRightCategory" as "AssignmentOfRightCategory",
            "OLD_InitialCollateralValue" as "InitialCollateralValue",
            "OLD_InitialCollateralValueCurrency" as "InitialCollateralValueCurrency",
            "OLD_RankAmongAssetsAssignedToCollateralAgreement" as "RankAmongAssetsAssignedToCollateralAgreement",
            "OLD_RankAmongCollateralAgreementsCoveredByAsset" as "RankAmongCollateralAgreementsCoveredByAsset",
            "OLD_RoleOfObligorInAssignedContract" as "RoleOfObligorInAssignedContract",
            "OLD_ThirdPartyAccountIdentificationSystem" as "ThirdPartyAccountIdentificationSystem",
            "OLD_ThirdPartyBankAccountCurrency" as "ThirdPartyBankAccountCurrency",
            "OLD_ThirdPartyBankAccountHolderName" as "ThirdPartyBankAccountHolderName",
            "OLD_ThirdPartyBankAccountID" as "ThirdPartyBankAccountID",
            "OLD_ThirdPartyBankID" as "ThirdPartyBankID",
            "OLD_ThirdPartyBankIdentificationSystem" as "ThirdPartyBankIdentificationSystem",
            "OLD_ThirdPartyBankName" as "ThirdPartyBankName",
            "OLD_ThirdPartyIBAN" as "ThirdPartyIBAN",
            "OLD_ThirdPartySWIFT" as "ThirdPartySWIFT",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
                        "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" AS "OLD_ASSOC_SimpleCollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" AS "OLD_ASSOC_SimpleCollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" AS "OLD_ASSOC_AssignedBusinessInterest.BusinessPartnerRelationType" ,
                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_AssignedBusinessInterest.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_AssignedBusinessInterest.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_AssignedFinancialContract.FinancialContractID" AS "OLD_ASSOC_AssignedFinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_AssignedFinancialContract.IDSystem" AS "OLD_ASSOC_AssignedFinancialContract.IDSystem" ,
                "OLD"."ASSOC_AssignedReceivable.ReceivableID" AS "OLD_ASSOC_AssignedReceivable.ReceivableID" ,
                "OLD"."ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" AS "OLD_ASSOC_ReceivableRepresentingAssignedBankAccount.ReceivableID" ,
                "OLD"."AssignedAmount" AS "OLD_AssignedAmount" ,
                "OLD"."AssignedAmountCurrency" AS "OLD_AssignedAmountCurrency" ,
                "OLD"."AssignedPercentage" AS "OLD_AssignedPercentage" ,
                "OLD"."AssignmentOfRightCategory" AS "OLD_AssignmentOfRightCategory" ,
                "OLD"."InitialCollateralValue" AS "OLD_InitialCollateralValue" ,
                "OLD"."InitialCollateralValueCurrency" AS "OLD_InitialCollateralValueCurrency" ,
                "OLD"."RankAmongAssetsAssignedToCollateralAgreement" AS "OLD_RankAmongAssetsAssignedToCollateralAgreement" ,
                "OLD"."RankAmongCollateralAgreementsCoveredByAsset" AS "OLD_RankAmongCollateralAgreementsCoveredByAsset" ,
                "OLD"."RoleOfObligorInAssignedContract" AS "OLD_RoleOfObligorInAssignedContract" ,
                "OLD"."ThirdPartyAccountIdentificationSystem" AS "OLD_ThirdPartyAccountIdentificationSystem" ,
                "OLD"."ThirdPartyBankAccountCurrency" AS "OLD_ThirdPartyBankAccountCurrency" ,
                "OLD"."ThirdPartyBankAccountHolderName" AS "OLD_ThirdPartyBankAccountHolderName" ,
                "OLD"."ThirdPartyBankAccountID" AS "OLD_ThirdPartyBankAccountID" ,
                "OLD"."ThirdPartyBankID" AS "OLD_ThirdPartyBankID" ,
                "OLD"."ThirdPartyBankIdentificationSystem" AS "OLD_ThirdPartyBankIdentificationSystem" ,
                "OLD"."ThirdPartyBankName" AS "OLD_ThirdPartyBankName" ,
                "OLD"."ThirdPartyIBAN" AS "OLD_ThirdPartyIBAN" ,
                "OLD"."ThirdPartySWIFT" AS "OLD_ThirdPartySWIFT" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AssignedRightAsCollateral" as "OLD"
            on
                                                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                                "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID" = "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" and
                                                "IN"."ASSOC_SimpleCollateralAgreement.IDSystem" = "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::AssignedRightAsCollateral"
    where (
        "SequenceNumber",
        "ASSOC_SimpleCollateralAgreement.FinancialContractID",
        "ASSOC_SimpleCollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID",
            "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::AssignedRightAsCollateral" as "OLD"
        on
                                       "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                       "IN"."ASSOC_SimpleCollateralAgreement.FinancialContractID" = "OLD"."ASSOC_SimpleCollateralAgreement.FinancialContractID" and
                                       "IN"."ASSOC_SimpleCollateralAgreement.IDSystem" = "OLD"."ASSOC_SimpleCollateralAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
