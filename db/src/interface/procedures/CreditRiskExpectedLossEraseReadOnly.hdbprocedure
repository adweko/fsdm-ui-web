PROCEDURE "sap.fsdm.procedures::CreditRiskExpectedLossEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::CreditRiskExpectedLossTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::CreditRiskExpectedLossTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::CreditRiskExpectedLossTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BestEstimateELMethod" is null and
            "BestEstimateELTimeHorizon" is null and
            "BestEstimateELTimeUnit" is null and
            "ASSOC_CollateralPortion.PortionNumber" is null and
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "BestEstimateELMethod" ,
                "BestEstimateELTimeHorizon" ,
                "BestEstimateELTimeUnit" ,
                "ASSOC_CollateralPortion.PortionNumber" ,
                "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "_Collection._Client.BusinessPartnerID" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."BestEstimateELMethod" ,
                "OLD"."BestEstimateELTimeHorizon" ,
                "OLD"."BestEstimateELTimeUnit" ,
                "OLD"."ASSOC_CollateralPortion.PortionNumber" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskExpectedLoss" "OLD"
            on
                "IN"."BestEstimateELMethod" = "OLD"."BestEstimateELMethod" and
                "IN"."BestEstimateELTimeHorizon" = "OLD"."BestEstimateELTimeHorizon" and
                "IN"."BestEstimateELTimeUnit" = "OLD"."BestEstimateELTimeUnit" and
                "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "BestEstimateELMethod" ,
            "BestEstimateELTimeHorizon" ,
            "BestEstimateELTimeUnit" ,
            "ASSOC_CollateralPortion.PortionNumber" ,
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."BestEstimateELMethod" ,
                "OLD"."BestEstimateELTimeHorizon" ,
                "OLD"."BestEstimateELTimeUnit" ,
                "OLD"."ASSOC_CollateralPortion.PortionNumber" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditRiskExpectedLoss_Historical" "OLD"
            on
                "IN"."BestEstimateELMethod" = "OLD"."BestEstimateELMethod" and
                "IN"."BestEstimateELTimeHorizon" = "OLD"."BestEstimateELTimeHorizon" and
                "IN"."BestEstimateELTimeUnit" = "OLD"."BestEstimateELTimeUnit" and
                "IN"."ASSOC_CollateralPortion.PortionNumber" = "OLD"."ASSOC_CollateralPortion.PortionNumber" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                "IN"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
