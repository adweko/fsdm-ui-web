PROCEDURE "sap.fsdm.procedures::MaturityBandEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::MaturityBandTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::MaturityBandTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::MaturityBandTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "MaturityBandID" is null and
            "TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "MaturityBandID" ,
                "TimeBucketID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."MaturityBandID" ,
                "OLD"."TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::MaturityBand" "OLD"
            on
                "IN"."MaturityBandID" = "OLD"."MaturityBandID" and
                "IN"."TimeBucketID" = "OLD"."TimeBucketID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "MaturityBandID" ,
            "TimeBucketID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."MaturityBandID" ,
                "OLD"."TimeBucketID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::MaturityBand_Historical" "OLD"
            on
                "IN"."MaturityBandID" = "OLD"."MaturityBandID" and
                "IN"."TimeBucketID" = "OLD"."TimeBucketID" 
        );

END
