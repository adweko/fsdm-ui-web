PROCEDURE "sap.fsdm.procedures::StandardReferenceObligationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::StandardReferenceObligationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::StandardReferenceObligationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::StandardReferenceObligationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_BusinessPartner.BusinessPartnerID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_BusinessPartner.BusinessPartnerID" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::StandardReferenceObligation" "OLD"
            on
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_BusinessPartner.BusinessPartnerID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::StandardReferenceObligation_Historical" "OLD"
            on
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
