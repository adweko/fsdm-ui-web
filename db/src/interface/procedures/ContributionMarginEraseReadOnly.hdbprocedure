PROCEDURE "sap.fsdm.procedures::ContributionMarginEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ContributionMarginTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ContributionMarginTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ContributionMarginTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Category" is null and
            "PeriodFrequency" is null and
            "PeriodStartDate" is null and
            "RoleOfCurrency" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_OrganizationalUnit.IDSystem" is null and
            "_OrganizationalUnit.OrganizationalUnitID" is null and
            "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null and
            "_SettlementItem.IDSystem" is null and
            "_SettlementItem.ItemNumber" is null and
            "_SettlementItem.SettlementID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Category" ,
                "PeriodFrequency" ,
                "PeriodStartDate" ,
                "RoleOfCurrency" ,
                "_BusinessPartner.BusinessPartnerID" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_OrganizationalUnit.IDSystem" ,
                "_OrganizationalUnit.OrganizationalUnitID" ,
                "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "_SecuritiesAccount.FinancialContractID" ,
                "_SecuritiesAccount.IDSystem" ,
                "_SettlementItem.IDSystem" ,
                "_SettlementItem.ItemNumber" ,
                "_SettlementItem.SettlementID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Category" ,
                "OLD"."PeriodFrequency" ,
                "OLD"."PeriodStartDate" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_OrganizationalUnit.IDSystem" ,
                "OLD"."_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_SettlementItem.IDSystem" ,
                "OLD"."_SettlementItem.ItemNumber" ,
                "OLD"."_SettlementItem.SettlementID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ContributionMargin" "OLD"
            on
                "IN"."Category" = "OLD"."Category" and
                "IN"."PeriodFrequency" = "OLD"."PeriodFrequency" and
                "IN"."PeriodStartDate" = "OLD"."PeriodStartDate" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_OrganizationalUnit.IDSystem" = "OLD"."_OrganizationalUnit.IDSystem" and
                "IN"."_OrganizationalUnit.OrganizationalUnitID" = "OLD"."_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                "IN"."_SettlementItem.IDSystem" = "OLD"."_SettlementItem.IDSystem" and
                "IN"."_SettlementItem.ItemNumber" = "OLD"."_SettlementItem.ItemNumber" and
                "IN"."_SettlementItem.SettlementID" = "OLD"."_SettlementItem.SettlementID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Category" ,
            "PeriodFrequency" ,
            "PeriodStartDate" ,
            "RoleOfCurrency" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_OrganizationalUnit.IDSystem" ,
            "_OrganizationalUnit.OrganizationalUnitID" ,
            "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "_SettlementItem.IDSystem" ,
            "_SettlementItem.ItemNumber" ,
            "_SettlementItem.SettlementID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Category" ,
                "OLD"."PeriodFrequency" ,
                "OLD"."PeriodStartDate" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_OrganizationalUnit.IDSystem" ,
                "OLD"."_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."_SettlementItem.IDSystem" ,
                "OLD"."_SettlementItem.ItemNumber" ,
                "OLD"."_SettlementItem.SettlementID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ContributionMargin_Historical" "OLD"
            on
                "IN"."Category" = "OLD"."Category" and
                "IN"."PeriodFrequency" = "OLD"."PeriodFrequency" and
                "IN"."PeriodStartDate" = "OLD"."PeriodStartDate" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_OrganizationalUnit.IDSystem" = "OLD"."_OrganizationalUnit.IDSystem" and
                "IN"."_OrganizationalUnit.OrganizationalUnitID" = "OLD"."_OrganizationalUnit.OrganizationalUnitID" and
                "IN"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                "IN"."_SettlementItem.IDSystem" = "OLD"."_SettlementItem.IDSystem" and
                "IN"."_SettlementItem.ItemNumber" = "OLD"."_SettlementItem.ItemNumber" and
                "IN"."_SettlementItem.SettlementID" = "OLD"."_SettlementItem.SettlementID" 
        );

END
