PROCEDURE "sap.fsdm.procedures::CreditDefaultSwapReferenceEntityErase" (IN ROW "sap.fsdm.tabletypes::CreditDefaultSwapReferenceEntityTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Basket.FinancialInstrumentID" is null and
            "_ProtectionLeg._CreditDefaultSwap.FinancialContractID" is null and
            "_ProtectionLeg._CreditDefaultSwap.IDSystem" is null and
            "_ReferenceEntity.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CreditDefaultSwapReferenceEntity"
        WHERE
        (            "_Basket.FinancialInstrumentID" ,
            "_ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
            "_ProtectionLeg._CreditDefaultSwap.IDSystem" ,
            "_ReferenceEntity.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."_Basket.FinancialInstrumentID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" ,
                "OLD"."_ReferenceEntity.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditDefaultSwapReferenceEntity" "OLD"
            on
            "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
            "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" = "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" and
            "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem" = "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" and
            "IN"."_ReferenceEntity.BusinessPartnerID" = "OLD"."_ReferenceEntity.BusinessPartnerID" 
        );

        --delete data from history table
        delete from "sap.fsdm::CreditDefaultSwapReferenceEntity_Historical"
        WHERE
        (
            "_Basket.FinancialInstrumentID" ,
            "_ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
            "_ProtectionLeg._CreditDefaultSwap.IDSystem" ,
            "_ReferenceEntity.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."_Basket.FinancialInstrumentID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" ,
                "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" ,
                "OLD"."_ReferenceEntity.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditDefaultSwapReferenceEntity_Historical" "OLD"
            on
                "IN"."_Basket.FinancialInstrumentID" = "OLD"."_Basket.FinancialInstrumentID" and
                "IN"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" = "OLD"."_ProtectionLeg._CreditDefaultSwap.FinancialContractID" and
                "IN"."_ProtectionLeg._CreditDefaultSwap.IDSystem" = "OLD"."_ProtectionLeg._CreditDefaultSwap.IDSystem" and
                "IN"."_ReferenceEntity.BusinessPartnerID" = "OLD"."_ReferenceEntity.BusinessPartnerID" 
        );

END
