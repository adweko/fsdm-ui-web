PROCEDURE "sap.fsdm.procedures::CardProgramParticipationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::CardProgramParticipationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::CardProgramParticipationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::CardProgramParticipationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CardProgramID" is null and
            "ASSOC_CardIssue.CardIssueID" is null and
            "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" is null and
            "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" is null and
            "ASSOC_CardIssue._BankAccount.FinancialContractID" is null and
            "ASSOC_CardIssue._BankAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "CardProgramID" ,
                "ASSOC_CardIssue.CardIssueID" ,
                "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "ASSOC_CardIssue._BankAccount.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."CardProgramID" ,
                "OLD"."ASSOC_CardIssue.CardIssueID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CardProgramParticipation" "OLD"
            on
                "IN"."CardProgramID" = "OLD"."CardProgramID" and
                "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "CardProgramID" ,
            "ASSOC_CardIssue.CardIssueID" ,
            "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
            "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
            "ASSOC_CardIssue._BankAccount.FinancialContractID" ,
            "ASSOC_CardIssue._BankAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."CardProgramID" ,
                "OLD"."ASSOC_CardIssue.CardIssueID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CardProgramParticipation_Historical" "OLD"
            on
                "IN"."CardProgramID" = "OLD"."CardProgramID" and
                "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
        );

END
