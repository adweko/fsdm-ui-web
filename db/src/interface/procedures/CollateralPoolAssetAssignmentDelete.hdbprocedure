PROCEDURE "sap.fsdm.procedures::CollateralPoolAssetAssignmentDelete" (IN ROW "sap.fsdm.tabletypes::CollateralPoolAssetAssignmentTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CollectionAssignmentType=' || TO_VARCHAR("CollectionAssignmentType") || ' ' ||
                'LotID=' || TO_VARCHAR("LotID") || ' ' ||
                '_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("_AccountingSystem.AccountingSystemID") || ' ' ||
                '_CollateralPool.CollectionID=' || TO_VARCHAR("_CollateralPool.CollectionID") || ' ' ||
                '_CollateralPool.IDSystem=' || TO_VARCHAR("_CollateralPool.IDSystem") || ' ' ||
                '_CollateralPool._Client.BusinessPartnerID=' || TO_VARCHAR("_CollateralPool._Client.BusinessPartnerID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CollectionAssignmentType",
                        "IN"."LotID",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CollectionAssignmentType",
                        "IN"."LotID",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CollectionAssignmentType",
                        "LotID",
                        "_AccountingSystem.AccountingSystemID",
                        "_CollateralPool.CollectionID",
                        "_CollateralPool.IDSystem",
                        "_CollateralPool._Client.BusinessPartnerID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem",
                        "_Trade.IDSystem",
                        "_Trade.TradeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CollectionAssignmentType",
                                    "IN"."LotID",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_CollateralPool._Client.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CollectionAssignmentType",
                                    "IN"."LotID",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_CollateralPool._Client.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CollectionAssignmentType" is null and
            "LotID" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_CollateralPool.CollectionID" is null and
            "_CollateralPool.IDSystem" is null and
            "_CollateralPool._Client.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CollateralPoolAssetAssignment" (
        "CollectionAssignmentType",
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmountAssignedToPool",
        "AssetAssignmentCategory",
        "CollectionAssetAssignmentCategory",
        "CurrencyOfAmountAssignedToPool",
        "HedgeRatio",
        "LongShort",
        "PoolCoverageType",
        "ShareAssignedToPool",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CollectionAssignmentType" as "CollectionAssignmentType" ,
            "OLD_LotID" as "LotID" ,
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID" ,
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem" ,
            "OLD__CollateralPool._Client.BusinessPartnerID" as "_CollateralPool._Client.BusinessPartnerID" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AmountAssignedToPool" as "AmountAssignedToPool" ,
            "OLD_AssetAssignmentCategory" as "AssetAssignmentCategory" ,
            "OLD_CollectionAssetAssignmentCategory" as "CollectionAssetAssignmentCategory" ,
            "OLD_CurrencyOfAmountAssignedToPool" as "CurrencyOfAmountAssignedToPool" ,
            "OLD_HedgeRatio" as "HedgeRatio" ,
            "OLD_LongShort" as "LongShort" ,
            "OLD_PoolCoverageType" as "PoolCoverageType" ,
            "OLD_ShareAssignedToPool" as "ShareAssignedToPool" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CollectionAssignmentType",
                        "OLD"."LotID",
                        "OLD"."_AccountingSystem.AccountingSystemID",
                        "OLD"."_CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem",
                        "OLD"."_CollateralPool._Client.BusinessPartnerID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."CollectionAssignmentType" AS "OLD_CollectionAssignmentType" ,
                "OLD"."LotID" AS "OLD_LotID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" AS "OLD__AccountingSystem.AccountingSystemID" ,
                "OLD"."_CollateralPool.CollectionID" AS "OLD__CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" AS "OLD__CollateralPool.IDSystem" ,
                "OLD"."_CollateralPool._Client.BusinessPartnerID" AS "OLD__CollateralPool._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" AS "OLD__SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" AS "OLD__SecuritiesAccount.IDSystem" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AmountAssignedToPool" AS "OLD_AmountAssignedToPool" ,
                "OLD"."AssetAssignmentCategory" AS "OLD_AssetAssignmentCategory" ,
                "OLD"."CollectionAssetAssignmentCategory" AS "OLD_CollectionAssetAssignmentCategory" ,
                "OLD"."CurrencyOfAmountAssignedToPool" AS "OLD_CurrencyOfAmountAssignedToPool" ,
                "OLD"."HedgeRatio" AS "OLD_HedgeRatio" ,
                "OLD"."LongShort" AS "OLD_LongShort" ,
                "OLD"."PoolCoverageType" AS "OLD_PoolCoverageType" ,
                "OLD"."ShareAssignedToPool" AS "OLD_ShareAssignedToPool" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolAssetAssignment" as "OLD"
            on
                      "IN"."CollectionAssignmentType" = "OLD"."CollectionAssignmentType" and
                      "IN"."LotID" = "OLD"."LotID" and
                      "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                      "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                      "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                      "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                      "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                      "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CollateralPoolAssetAssignment" (
        "CollectionAssignmentType",
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmountAssignedToPool",
        "AssetAssignmentCategory",
        "CollectionAssetAssignmentCategory",
        "CurrencyOfAmountAssignedToPool",
        "HedgeRatio",
        "LongShort",
        "PoolCoverageType",
        "ShareAssignedToPool",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CollectionAssignmentType" as "CollectionAssignmentType",
            "OLD_LotID" as "LotID",
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID",
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID",
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem",
            "OLD__CollateralPool._Client.BusinessPartnerID" as "_CollateralPool._Client.BusinessPartnerID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AmountAssignedToPool" as "AmountAssignedToPool",
            "OLD_AssetAssignmentCategory" as "AssetAssignmentCategory",
            "OLD_CollectionAssetAssignmentCategory" as "CollectionAssetAssignmentCategory",
            "OLD_CurrencyOfAmountAssignedToPool" as "CurrencyOfAmountAssignedToPool",
            "OLD_HedgeRatio" as "HedgeRatio",
            "OLD_LongShort" as "LongShort",
            "OLD_PoolCoverageType" as "PoolCoverageType",
            "OLD_ShareAssignedToPool" as "ShareAssignedToPool",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CollectionAssignmentType",
                        "OLD"."LotID",
                        "OLD"."_AccountingSystem.AccountingSystemID",
                        "OLD"."_CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem",
                        "OLD"."_CollateralPool._Client.BusinessPartnerID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CollectionAssignmentType" AS "OLD_CollectionAssignmentType" ,
                "OLD"."LotID" AS "OLD_LotID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" AS "OLD__AccountingSystem.AccountingSystemID" ,
                "OLD"."_CollateralPool.CollectionID" AS "OLD__CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" AS "OLD__CollateralPool.IDSystem" ,
                "OLD"."_CollateralPool._Client.BusinessPartnerID" AS "OLD__CollateralPool._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" AS "OLD__SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" AS "OLD__SecuritiesAccount.IDSystem" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AmountAssignedToPool" AS "OLD_AmountAssignedToPool" ,
                "OLD"."AssetAssignmentCategory" AS "OLD_AssetAssignmentCategory" ,
                "OLD"."CollectionAssetAssignmentCategory" AS "OLD_CollectionAssetAssignmentCategory" ,
                "OLD"."CurrencyOfAmountAssignedToPool" AS "OLD_CurrencyOfAmountAssignedToPool" ,
                "OLD"."HedgeRatio" AS "OLD_HedgeRatio" ,
                "OLD"."LongShort" AS "OLD_LongShort" ,
                "OLD"."PoolCoverageType" AS "OLD_PoolCoverageType" ,
                "OLD"."ShareAssignedToPool" AS "OLD_ShareAssignedToPool" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPoolAssetAssignment" as "OLD"
            on
                                                "IN"."CollectionAssignmentType" = "OLD"."CollectionAssignmentType" and
                                                "IN"."LotID" = "OLD"."LotID" and
                                                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                                                "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                                                "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                                                "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                                                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                                                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                                                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CollateralPoolAssetAssignment"
    where (
        "CollectionAssignmentType",
        "LotID",
        "_AccountingSystem.AccountingSystemID",
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."CollectionAssignmentType",
            "OLD"."LotID",
            "OLD"."_AccountingSystem.AccountingSystemID",
            "OLD"."_CollateralPool.CollectionID",
            "OLD"."_CollateralPool.IDSystem",
            "OLD"."_CollateralPool._Client.BusinessPartnerID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CollateralPoolAssetAssignment" as "OLD"
        on
                                       "IN"."CollectionAssignmentType" = "OLD"."CollectionAssignmentType" and
                                       "IN"."LotID" = "OLD"."LotID" and
                                       "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                                       "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                                       "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                                       "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                                       "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                       "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                       "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                       "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                                       "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" and
                                       "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                       "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
