PROCEDURE "sap.fsdm.procedures::BusinessPartnerContractAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerContractAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::BusinessPartnerContractAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::BusinessPartnerContractAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Role=' || TO_VARCHAR("Role") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                'ASSOC_PartnerInParticipation.BusinessPartnerID=' || TO_VARCHAR("ASSOC_PartnerInParticipation.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Role",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Role",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Role",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "ASSOC_PartnerInParticipation.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Role",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Role",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Role" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PartnerInParticipation.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "Role",
            "ASSOC_FinancialContract.FinancialContractID",
            "ASSOC_FinancialContract.IDSystem",
            "ASSOC_PartnerInParticipation.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::BusinessPartnerContractAssignment" WHERE
            (
            "Role" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PartnerInParticipation.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."Role",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BusinessPartnerContractAssignment" as "OLD"
        on
                              "IN"."Role" = "OLD"."Role" and
                              "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                              "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID" = "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "Role",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_PartnerInParticipation.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Address.AddressType",
        "_Address.SequenceNumber",
        "_Address.ASSOC_BankingChannel.BankingChannelID",
        "_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
        "_Address.ASSOC_OrganizationalUnit.IDSystem",
        "_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
        "_Address._Claim.ID",
        "_Address._Claim.IDSystem",
        "AutomobileAssociationMember",
        "BusinessPartnerContractAssignmentCategory",
        "BusinessPartnerMakingTheOffer",
        "ContractDataOwner",
        "MainCounterparty",
        "MainIndicator",
        "MarginPeriod",
        "MarginPeriodTimeUnit",
        "MinimumTransferAmount",
        "MinimumTransferAmountCurrency",
        "OwnerOccupiedPropertyType",
        "PowerOfAttorneyIndicator",
        "ReasonForPartnerAssignment",
        "RoundingAmount",
        "RoundingAmountCurrency",
        "TariffGroup",
        "ThresholdAmount",
        "ThresholdAmountCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_Role" as "Role" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD_ASSOC_PartnerInParticipation.BusinessPartnerID" as "ASSOC_PartnerInParticipation.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__Address.AddressType" as "_Address.AddressType" ,
            "OLD__Address.SequenceNumber" as "_Address.SequenceNumber" ,
            "OLD__Address.ASSOC_BankingChannel.BankingChannelID" as "_Address.ASSOC_BankingChannel.BankingChannelID" ,
            "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" as "_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" as "_Address.ASSOC_OrganizationalUnit.IDSystem" ,
            "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" as "_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" as "_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "OLD__Address._Claim.ID" as "_Address._Claim.ID" ,
            "OLD__Address._Claim.IDSystem" as "_Address._Claim.IDSystem" ,
            "OLD_AutomobileAssociationMember" as "AutomobileAssociationMember" ,
            "OLD_BusinessPartnerContractAssignmentCategory" as "BusinessPartnerContractAssignmentCategory" ,
            "OLD_BusinessPartnerMakingTheOffer" as "BusinessPartnerMakingTheOffer" ,
            "OLD_ContractDataOwner" as "ContractDataOwner" ,
            "OLD_MainCounterparty" as "MainCounterparty" ,
            "OLD_MainIndicator" as "MainIndicator" ,
            "OLD_MarginPeriod" as "MarginPeriod" ,
            "OLD_MarginPeriodTimeUnit" as "MarginPeriodTimeUnit" ,
            "OLD_MinimumTransferAmount" as "MinimumTransferAmount" ,
            "OLD_MinimumTransferAmountCurrency" as "MinimumTransferAmountCurrency" ,
            "OLD_OwnerOccupiedPropertyType" as "OwnerOccupiedPropertyType" ,
            "OLD_PowerOfAttorneyIndicator" as "PowerOfAttorneyIndicator" ,
            "OLD_ReasonForPartnerAssignment" as "ReasonForPartnerAssignment" ,
            "OLD_RoundingAmount" as "RoundingAmount" ,
            "OLD_RoundingAmountCurrency" as "RoundingAmountCurrency" ,
            "OLD_TariffGroup" as "TariffGroup" ,
            "OLD_ThresholdAmount" as "ThresholdAmount" ,
            "OLD_ThresholdAmountCurrency" as "ThresholdAmountCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Role",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."Role" AS "OLD_Role" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID" AS "OLD_ASSOC_PartnerInParticipation.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Address.AddressType" AS "OLD__Address.AddressType" ,
                "OLD"."_Address.SequenceNumber" AS "OLD__Address.SequenceNumber" ,
                "OLD"."_Address.ASSOC_BankingChannel.BankingChannelID" AS "OLD__Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.IDSystem" AS "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Address._Claim.ID" AS "OLD__Address._Claim.ID" ,
                "OLD"."_Address._Claim.IDSystem" AS "OLD__Address._Claim.IDSystem" ,
                "OLD"."AutomobileAssociationMember" AS "OLD_AutomobileAssociationMember" ,
                "OLD"."BusinessPartnerContractAssignmentCategory" AS "OLD_BusinessPartnerContractAssignmentCategory" ,
                "OLD"."BusinessPartnerMakingTheOffer" AS "OLD_BusinessPartnerMakingTheOffer" ,
                "OLD"."ContractDataOwner" AS "OLD_ContractDataOwner" ,
                "OLD"."MainCounterparty" AS "OLD_MainCounterparty" ,
                "OLD"."MainIndicator" AS "OLD_MainIndicator" ,
                "OLD"."MarginPeriod" AS "OLD_MarginPeriod" ,
                "OLD"."MarginPeriodTimeUnit" AS "OLD_MarginPeriodTimeUnit" ,
                "OLD"."MinimumTransferAmount" AS "OLD_MinimumTransferAmount" ,
                "OLD"."MinimumTransferAmountCurrency" AS "OLD_MinimumTransferAmountCurrency" ,
                "OLD"."OwnerOccupiedPropertyType" AS "OLD_OwnerOccupiedPropertyType" ,
                "OLD"."PowerOfAttorneyIndicator" AS "OLD_PowerOfAttorneyIndicator" ,
                "OLD"."ReasonForPartnerAssignment" AS "OLD_ReasonForPartnerAssignment" ,
                "OLD"."RoundingAmount" AS "OLD_RoundingAmount" ,
                "OLD"."RoundingAmountCurrency" AS "OLD_RoundingAmountCurrency" ,
                "OLD"."TariffGroup" AS "OLD_TariffGroup" ,
                "OLD"."ThresholdAmount" AS "OLD_ThresholdAmount" ,
                "OLD"."ThresholdAmountCurrency" AS "OLD_ThresholdAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerContractAssignment" as "OLD"
            on
                                      "IN"."Role" = "OLD"."Role" and
                                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                      "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID" = "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_Role" as "Role",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD_ASSOC_PartnerInParticipation.BusinessPartnerID" as "ASSOC_PartnerInParticipation.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Address.AddressType" as "_Address.AddressType",
            "OLD__Address.SequenceNumber" as "_Address.SequenceNumber",
            "OLD__Address.ASSOC_BankingChannel.BankingChannelID" as "_Address.ASSOC_BankingChannel.BankingChannelID",
            "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" as "_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" as "_Address.ASSOC_OrganizationalUnit.IDSystem",
            "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" as "_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" as "_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD__Address._Claim.ID" as "_Address._Claim.ID",
            "OLD__Address._Claim.IDSystem" as "_Address._Claim.IDSystem",
            "OLD_AutomobileAssociationMember" as "AutomobileAssociationMember",
            "OLD_BusinessPartnerContractAssignmentCategory" as "BusinessPartnerContractAssignmentCategory",
            "OLD_BusinessPartnerMakingTheOffer" as "BusinessPartnerMakingTheOffer",
            "OLD_ContractDataOwner" as "ContractDataOwner",
            "OLD_MainCounterparty" as "MainCounterparty",
            "OLD_MainIndicator" as "MainIndicator",
            "OLD_MarginPeriod" as "MarginPeriod",
            "OLD_MarginPeriodTimeUnit" as "MarginPeriodTimeUnit",
            "OLD_MinimumTransferAmount" as "MinimumTransferAmount",
            "OLD_MinimumTransferAmountCurrency" as "MinimumTransferAmountCurrency",
            "OLD_OwnerOccupiedPropertyType" as "OwnerOccupiedPropertyType",
            "OLD_PowerOfAttorneyIndicator" as "PowerOfAttorneyIndicator",
            "OLD_ReasonForPartnerAssignment" as "ReasonForPartnerAssignment",
            "OLD_RoundingAmount" as "RoundingAmount",
            "OLD_RoundingAmountCurrency" as "RoundingAmountCurrency",
            "OLD_TariffGroup" as "TariffGroup",
            "OLD_ThresholdAmount" as "ThresholdAmount",
            "OLD_ThresholdAmountCurrency" as "ThresholdAmountCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Role",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Role" AS "OLD_Role" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID" AS "OLD_ASSOC_PartnerInParticipation.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Address.AddressType" AS "OLD__Address.AddressType" ,
                "OLD"."_Address.SequenceNumber" AS "OLD__Address.SequenceNumber" ,
                "OLD"."_Address.ASSOC_BankingChannel.BankingChannelID" AS "OLD__Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.IDSystem" AS "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Address._Claim.ID" AS "OLD__Address._Claim.ID" ,
                "OLD"."_Address._Claim.IDSystem" AS "OLD__Address._Claim.IDSystem" ,
                "OLD"."AutomobileAssociationMember" AS "OLD_AutomobileAssociationMember" ,
                "OLD"."BusinessPartnerContractAssignmentCategory" AS "OLD_BusinessPartnerContractAssignmentCategory" ,
                "OLD"."BusinessPartnerMakingTheOffer" AS "OLD_BusinessPartnerMakingTheOffer" ,
                "OLD"."ContractDataOwner" AS "OLD_ContractDataOwner" ,
                "OLD"."MainCounterparty" AS "OLD_MainCounterparty" ,
                "OLD"."MainIndicator" AS "OLD_MainIndicator" ,
                "OLD"."MarginPeriod" AS "OLD_MarginPeriod" ,
                "OLD"."MarginPeriodTimeUnit" AS "OLD_MarginPeriodTimeUnit" ,
                "OLD"."MinimumTransferAmount" AS "OLD_MinimumTransferAmount" ,
                "OLD"."MinimumTransferAmountCurrency" AS "OLD_MinimumTransferAmountCurrency" ,
                "OLD"."OwnerOccupiedPropertyType" AS "OLD_OwnerOccupiedPropertyType" ,
                "OLD"."PowerOfAttorneyIndicator" AS "OLD_PowerOfAttorneyIndicator" ,
                "OLD"."ReasonForPartnerAssignment" AS "OLD_ReasonForPartnerAssignment" ,
                "OLD"."RoundingAmount" AS "OLD_RoundingAmount" ,
                "OLD"."RoundingAmountCurrency" AS "OLD_RoundingAmountCurrency" ,
                "OLD"."TariffGroup" AS "OLD_TariffGroup" ,
                "OLD"."ThresholdAmount" AS "OLD_ThresholdAmount" ,
                "OLD"."ThresholdAmountCurrency" AS "OLD_ThresholdAmountCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerContractAssignment" as "OLD"
            on
               "IN"."Role" = "OLD"."Role" and
               "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
               "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
               "IN"."ASSOC_PartnerInParticipation.BusinessPartnerID" = "OLD"."ASSOC_PartnerInParticipation.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
