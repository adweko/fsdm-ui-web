PROCEDURE "sap.fsdm.procedures::InjuredPersonEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InjuredPersonTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InjuredPersonTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InjuredPersonTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_IndividualPerson.BusinessPartnerID" is null and
            "_PersonalInjury.LossCategory" is null and
            "_PersonalInjury._Claim.ID" is null and
            "_PersonalInjury._Claim.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_IndividualPerson.BusinessPartnerID" ,
                "_PersonalInjury.LossCategory" ,
                "_PersonalInjury._Claim.ID" ,
                "_PersonalInjury._Claim.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_IndividualPerson.BusinessPartnerID" ,
                "OLD"."_PersonalInjury.LossCategory" ,
                "OLD"."_PersonalInjury._Claim.ID" ,
                "OLD"."_PersonalInjury._Claim.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InjuredPerson" "OLD"
            on
                "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                "IN"."_PersonalInjury.LossCategory" = "OLD"."_PersonalInjury.LossCategory" and
                "IN"."_PersonalInjury._Claim.ID" = "OLD"."_PersonalInjury._Claim.ID" and
                "IN"."_PersonalInjury._Claim.IDSystem" = "OLD"."_PersonalInjury._Claim.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_IndividualPerson.BusinessPartnerID" ,
            "_PersonalInjury.LossCategory" ,
            "_PersonalInjury._Claim.ID" ,
            "_PersonalInjury._Claim.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_IndividualPerson.BusinessPartnerID" ,
                "OLD"."_PersonalInjury.LossCategory" ,
                "OLD"."_PersonalInjury._Claim.ID" ,
                "OLD"."_PersonalInjury._Claim.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InjuredPerson_Historical" "OLD"
            on
                "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                "IN"."_PersonalInjury.LossCategory" = "OLD"."_PersonalInjury.LossCategory" and
                "IN"."_PersonalInjury._Claim.ID" = "OLD"."_PersonalInjury._Claim.ID" and
                "IN"."_PersonalInjury._Claim.IDSystem" = "OLD"."_PersonalInjury._Claim.IDSystem" 
        );

END
