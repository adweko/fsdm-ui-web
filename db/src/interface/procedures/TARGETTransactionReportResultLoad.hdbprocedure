PROCEDURE "sap.fsdm.procedures::TARGETTransactionReportResultLoad" (IN ROW "sap.fsdm.tabletypes::TARGETTransactionReportResultTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SettlementOrder=' || TO_VARCHAR("SettlementOrder") || ' ' ||
                'SettlementReference=' || TO_VARCHAR("SettlementReference") || ' ' ||
                '_BankAccount.FinancialContractID=' || TO_VARCHAR("_BankAccount.FinancialContractID") || ' ' ||
                '_BankAccount.IDSystem=' || TO_VARCHAR("_BankAccount.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SettlementOrder",
                        "IN"."SettlementReference",
                        "IN"."_BankAccount.FinancialContractID",
                        "IN"."_BankAccount.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SettlementOrder",
                        "IN"."SettlementReference",
                        "IN"."_BankAccount.FinancialContractID",
                        "IN"."_BankAccount.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SettlementOrder",
                        "SettlementReference",
                        "_BankAccount.FinancialContractID",
                        "_BankAccount.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SettlementOrder",
                                    "IN"."SettlementReference",
                                    "IN"."_BankAccount.FinancialContractID",
                                    "IN"."_BankAccount.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SettlementOrder",
                                    "IN"."SettlementReference",
                                    "IN"."_BankAccount.FinancialContractID",
                                    "IN"."_BankAccount.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::TARGETTransactionReportResult" (
        "SettlementOrder",
        "SettlementReference",
        "_BankAccount.FinancialContractID",
        "_BankAccount.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AncillarySystemBIC",
        "BIC52",
        "BIC57",
        "BIC58",
        "CollateralCLPostedAmount",
        "CollateralCLPostedAmountCurrency",
        "DebitOrCredit",
        "Field21",
        "ParticipantBIC",
        "PaymentSubtype",
        "RunningBalanceAmount",
        "RunningBalanceAmountCurrency",
        "SettlementPriority",
        "SettlementTimestamp",
        "SwiftMessageType",
        "TransactionAmount",
        "TransactionAmountCurrency",
        "TransactionReference",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SettlementOrder" as "SettlementOrder" ,
            "OLD_SettlementReference" as "SettlementReference" ,
            "OLD__BankAccount.FinancialContractID" as "_BankAccount.FinancialContractID" ,
            "OLD__BankAccount.IDSystem" as "_BankAccount.IDSystem" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AncillarySystemBIC" as "AncillarySystemBIC" ,
            "OLD_BIC52" as "BIC52" ,
            "OLD_BIC57" as "BIC57" ,
            "OLD_BIC58" as "BIC58" ,
            "OLD_CollateralCLPostedAmount" as "CollateralCLPostedAmount" ,
            "OLD_CollateralCLPostedAmountCurrency" as "CollateralCLPostedAmountCurrency" ,
            "OLD_DebitOrCredit" as "DebitOrCredit" ,
            "OLD_Field21" as "Field21" ,
            "OLD_ParticipantBIC" as "ParticipantBIC" ,
            "OLD_PaymentSubtype" as "PaymentSubtype" ,
            "OLD_RunningBalanceAmount" as "RunningBalanceAmount" ,
            "OLD_RunningBalanceAmountCurrency" as "RunningBalanceAmountCurrency" ,
            "OLD_SettlementPriority" as "SettlementPriority" ,
            "OLD_SettlementTimestamp" as "SettlementTimestamp" ,
            "OLD_SwiftMessageType" as "SwiftMessageType" ,
            "OLD_TransactionAmount" as "TransactionAmount" ,
            "OLD_TransactionAmountCurrency" as "TransactionAmountCurrency" ,
            "OLD_TransactionReference" as "TransactionReference" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."SettlementOrder",
                        "IN"."SettlementReference",
                        "IN"."_BankAccount.FinancialContractID",
                        "IN"."_BankAccount.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."SettlementOrder" as "OLD_SettlementOrder",
                                "OLD"."SettlementReference" as "OLD_SettlementReference",
                                "OLD"."_BankAccount.FinancialContractID" as "OLD__BankAccount.FinancialContractID",
                                "OLD"."_BankAccount.IDSystem" as "OLD__BankAccount.IDSystem",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AncillarySystemBIC" as "OLD_AncillarySystemBIC",
                                "OLD"."BIC52" as "OLD_BIC52",
                                "OLD"."BIC57" as "OLD_BIC57",
                                "OLD"."BIC58" as "OLD_BIC58",
                                "OLD"."CollateralCLPostedAmount" as "OLD_CollateralCLPostedAmount",
                                "OLD"."CollateralCLPostedAmountCurrency" as "OLD_CollateralCLPostedAmountCurrency",
                                "OLD"."DebitOrCredit" as "OLD_DebitOrCredit",
                                "OLD"."Field21" as "OLD_Field21",
                                "OLD"."ParticipantBIC" as "OLD_ParticipantBIC",
                                "OLD"."PaymentSubtype" as "OLD_PaymentSubtype",
                                "OLD"."RunningBalanceAmount" as "OLD_RunningBalanceAmount",
                                "OLD"."RunningBalanceAmountCurrency" as "OLD_RunningBalanceAmountCurrency",
                                "OLD"."SettlementPriority" as "OLD_SettlementPriority",
                                "OLD"."SettlementTimestamp" as "OLD_SettlementTimestamp",
                                "OLD"."SwiftMessageType" as "OLD_SwiftMessageType",
                                "OLD"."TransactionAmount" as "OLD_TransactionAmount",
                                "OLD"."TransactionAmountCurrency" as "OLD_TransactionAmountCurrency",
                                "OLD"."TransactionReference" as "OLD_TransactionReference",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::TARGETTransactionReportResult" as "OLD"
            on
                ifnull( "IN"."SettlementOrder", -1) = "OLD"."SettlementOrder" and
                ifnull( "IN"."SettlementReference", '') = "OLD"."SettlementReference" and
                ifnull( "IN"."_BankAccount.FinancialContractID", '') = "OLD"."_BankAccount.FinancialContractID" and
                ifnull( "IN"."_BankAccount.IDSystem", '') = "OLD"."_BankAccount.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::TARGETTransactionReportResult" (
        "SettlementOrder",
        "SettlementReference",
        "_BankAccount.FinancialContractID",
        "_BankAccount.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AncillarySystemBIC",
        "BIC52",
        "BIC57",
        "BIC58",
        "CollateralCLPostedAmount",
        "CollateralCLPostedAmountCurrency",
        "DebitOrCredit",
        "Field21",
        "ParticipantBIC",
        "PaymentSubtype",
        "RunningBalanceAmount",
        "RunningBalanceAmountCurrency",
        "SettlementPriority",
        "SettlementTimestamp",
        "SwiftMessageType",
        "TransactionAmount",
        "TransactionAmountCurrency",
        "TransactionReference",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SettlementOrder" as "SettlementOrder",
            "OLD_SettlementReference" as "SettlementReference",
            "OLD__BankAccount.FinancialContractID" as "_BankAccount.FinancialContractID",
            "OLD__BankAccount.IDSystem" as "_BankAccount.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AncillarySystemBIC" as "AncillarySystemBIC",
            "OLD_BIC52" as "BIC52",
            "OLD_BIC57" as "BIC57",
            "OLD_BIC58" as "BIC58",
            "OLD_CollateralCLPostedAmount" as "CollateralCLPostedAmount",
            "OLD_CollateralCLPostedAmountCurrency" as "CollateralCLPostedAmountCurrency",
            "OLD_DebitOrCredit" as "DebitOrCredit",
            "OLD_Field21" as "Field21",
            "OLD_ParticipantBIC" as "ParticipantBIC",
            "OLD_PaymentSubtype" as "PaymentSubtype",
            "OLD_RunningBalanceAmount" as "RunningBalanceAmount",
            "OLD_RunningBalanceAmountCurrency" as "RunningBalanceAmountCurrency",
            "OLD_SettlementPriority" as "SettlementPriority",
            "OLD_SettlementTimestamp" as "SettlementTimestamp",
            "OLD_SwiftMessageType" as "SwiftMessageType",
            "OLD_TransactionAmount" as "TransactionAmount",
            "OLD_TransactionAmountCurrency" as "TransactionAmountCurrency",
            "OLD_TransactionReference" as "TransactionReference",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."SettlementOrder",
                        "IN"."SettlementReference",
                        "IN"."_BankAccount.FinancialContractID",
                        "IN"."_BankAccount.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."SettlementOrder" as "OLD_SettlementOrder",
                        "OLD"."SettlementReference" as "OLD_SettlementReference",
                        "OLD"."_BankAccount.FinancialContractID" as "OLD__BankAccount.FinancialContractID",
                        "OLD"."_BankAccount.IDSystem" as "OLD__BankAccount.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."AncillarySystemBIC" as "OLD_AncillarySystemBIC",
                        "OLD"."BIC52" as "OLD_BIC52",
                        "OLD"."BIC57" as "OLD_BIC57",
                        "OLD"."BIC58" as "OLD_BIC58",
                        "OLD"."CollateralCLPostedAmount" as "OLD_CollateralCLPostedAmount",
                        "OLD"."CollateralCLPostedAmountCurrency" as "OLD_CollateralCLPostedAmountCurrency",
                        "OLD"."DebitOrCredit" as "OLD_DebitOrCredit",
                        "OLD"."Field21" as "OLD_Field21",
                        "OLD"."ParticipantBIC" as "OLD_ParticipantBIC",
                        "OLD"."PaymentSubtype" as "OLD_PaymentSubtype",
                        "OLD"."RunningBalanceAmount" as "OLD_RunningBalanceAmount",
                        "OLD"."RunningBalanceAmountCurrency" as "OLD_RunningBalanceAmountCurrency",
                        "OLD"."SettlementPriority" as "OLD_SettlementPriority",
                        "OLD"."SettlementTimestamp" as "OLD_SettlementTimestamp",
                        "OLD"."SwiftMessageType" as "OLD_SwiftMessageType",
                        "OLD"."TransactionAmount" as "OLD_TransactionAmount",
                        "OLD"."TransactionAmountCurrency" as "OLD_TransactionAmountCurrency",
                        "OLD"."TransactionReference" as "OLD_TransactionReference",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::TARGETTransactionReportResult" as "OLD"
            on
                ifnull( "IN"."SettlementOrder", -1 ) = "OLD"."SettlementOrder" and
                ifnull( "IN"."SettlementReference", '' ) = "OLD"."SettlementReference" and
                ifnull( "IN"."_BankAccount.FinancialContractID", '' ) = "OLD"."_BankAccount.FinancialContractID" and
                ifnull( "IN"."_BankAccount.IDSystem", '' ) = "OLD"."_BankAccount.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::TARGETTransactionReportResult"
    where (
        "SettlementOrder",
        "SettlementReference",
        "_BankAccount.FinancialContractID",
        "_BankAccount.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SettlementOrder",
            "OLD"."SettlementReference",
            "OLD"."_BankAccount.FinancialContractID",
            "OLD"."_BankAccount.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TARGETTransactionReportResult" as "OLD"
        on
           ifnull( "IN"."SettlementOrder", -1 ) = "OLD"."SettlementOrder" and
           ifnull( "IN"."SettlementReference", '' ) = "OLD"."SettlementReference" and
           ifnull( "IN"."_BankAccount.FinancialContractID", '' ) = "OLD"."_BankAccount.FinancialContractID" and
           ifnull( "IN"."_BankAccount.IDSystem", '' ) = "OLD"."_BankAccount.IDSystem" and
           ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::TARGETTransactionReportResult" (
        "SettlementOrder",
        "SettlementReference",
        "_BankAccount.FinancialContractID",
        "_BankAccount.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AncillarySystemBIC",
        "BIC52",
        "BIC57",
        "BIC58",
        "CollateralCLPostedAmount",
        "CollateralCLPostedAmountCurrency",
        "DebitOrCredit",
        "Field21",
        "ParticipantBIC",
        "PaymentSubtype",
        "RunningBalanceAmount",
        "RunningBalanceAmountCurrency",
        "SettlementPriority",
        "SettlementTimestamp",
        "SwiftMessageType",
        "TransactionAmount",
        "TransactionAmountCurrency",
        "TransactionReference",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "SettlementOrder", -1 ) as "SettlementOrder",
            ifnull( "SettlementReference", '' ) as "SettlementReference",
            ifnull( "_BankAccount.FinancialContractID", '' ) as "_BankAccount.FinancialContractID",
            ifnull( "_BankAccount.IDSystem", '' ) as "_BankAccount.IDSystem",
            ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
            ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "AncillarySystemBIC"  ,
            "BIC52"  ,
            "BIC57"  ,
            "BIC58"  ,
            "CollateralCLPostedAmount"  ,
            "CollateralCLPostedAmountCurrency"  ,
            "DebitOrCredit"  ,
            "Field21"  ,
            "ParticipantBIC"  ,
            "PaymentSubtype"  ,
            "RunningBalanceAmount"  ,
            "RunningBalanceAmountCurrency"  ,
            "SettlementPriority"  ,
            "SettlementTimestamp"  ,
            "SwiftMessageType"  ,
            "TransactionAmount"  ,
            "TransactionAmountCurrency"  ,
            "TransactionReference"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END