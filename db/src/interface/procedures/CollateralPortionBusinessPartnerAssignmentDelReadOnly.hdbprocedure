PROCEDURE "sap.fsdm.procedures::CollateralPortionBusinessPartnerAssignmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CollateralPortionBusinessPartnerAssignmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CollateralPortionBusinessPartnerAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CollateralPortionBusinessPartnerAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_CollateralPortion.PortionNumber=' || TO_VARCHAR("_CollateralPortion.PortionNumber") || ' ' ||
                '_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID=' || TO_VARCHAR("_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID") || ' ' ||
                '_CollateralPortion.ASSOC_CollateralAgreement.IDSystem=' || TO_VARCHAR("_CollateralPortion.ASSOC_CollateralAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_CollateralPortion.PortionNumber",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_BusinessPartner.BusinessPartnerID",
                        "_CollateralPortion.PortionNumber",
                        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_CollateralPortion.PortionNumber",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_CollateralPortion.PortionNumber",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                                    "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_BusinessPartner.BusinessPartnerID" is null and
            "_CollateralPortion.PortionNumber" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" is null and
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_BusinessPartner.BusinessPartnerID",
            "_CollateralPortion.PortionNumber",
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CollateralPortionBusinessPartnerAssignment" WHERE
            (
            "_BusinessPartner.BusinessPartnerID" ,
            "_CollateralPortion.PortionNumber" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_CollateralPortion.PortionNumber",
            "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CollateralPortionBusinessPartnerAssignment" as "OLD"
        on
                              "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                              "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                              "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                              "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_BusinessPartner.BusinessPartnerID",
        "_CollateralPortion.PortionNumber",
        "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
        "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CollateralPortionConsideredInBusinessPartnerRating",
        "RankAmongBusinessPartnersCoveredByTheCollateral",
        "RankAmongCollateralsCoveringTheBusinessPartner",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "OLD__CollateralPortion.PortionNumber" as "_CollateralPortion.PortionNumber" ,
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CollateralPortionConsideredInBusinessPartnerRating" as "CollateralPortionConsideredInBusinessPartnerRating" ,
            "OLD_RankAmongBusinessPartnersCoveredByTheCollateral" as "RankAmongBusinessPartnersCoveredByTheCollateral" ,
            "OLD_RankAmongCollateralsCoveringTheBusinessPartner" as "RankAmongCollateralsCoveringTheBusinessPartner" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."_CollateralPortion.PortionNumber",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."_CollateralPortion.PortionNumber" AS "OLD__CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CollateralPortionConsideredInBusinessPartnerRating" AS "OLD_CollateralPortionConsideredInBusinessPartnerRating" ,
                "OLD"."RankAmongBusinessPartnersCoveredByTheCollateral" AS "OLD_RankAmongBusinessPartnersCoveredByTheCollateral" ,
                "OLD"."RankAmongCollateralsCoveringTheBusinessPartner" AS "OLD_RankAmongCollateralsCoveringTheBusinessPartner" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPortionBusinessPartnerAssignment" as "OLD"
            on
                                      "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                                      "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
                                      "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
                                      "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "OLD__CollateralPortion.PortionNumber" as "_CollateralPortion.PortionNumber",
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" as "_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
            "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" as "_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CollateralPortionConsideredInBusinessPartnerRating" as "CollateralPortionConsideredInBusinessPartnerRating",
            "OLD_RankAmongBusinessPartnersCoveredByTheCollateral" as "RankAmongBusinessPartnersCoveredByTheCollateral",
            "OLD_RankAmongCollateralsCoveringTheBusinessPartner" as "RankAmongCollateralsCoveringTheBusinessPartner",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."_CollateralPortion.PortionNumber",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID",
                        "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."_CollateralPortion.PortionNumber" AS "OLD__CollateralPortion.PortionNumber" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" ,
                "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AS "OLD__CollateralPortion.ASSOC_CollateralAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CollateralPortionConsideredInBusinessPartnerRating" AS "OLD_CollateralPortionConsideredInBusinessPartnerRating" ,
                "OLD"."RankAmongBusinessPartnersCoveredByTheCollateral" AS "OLD_RankAmongBusinessPartnersCoveredByTheCollateral" ,
                "OLD"."RankAmongCollateralsCoveringTheBusinessPartner" AS "OLD_RankAmongCollateralsCoveringTheBusinessPartner" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CollateralPortionBusinessPartnerAssignment" as "OLD"
            on
               "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
               "IN"."_CollateralPortion.PortionNumber" = "OLD"."_CollateralPortion.PortionNumber" and
               "IN"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" and
               "IN"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" = "OLD"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
