PROCEDURE "sap.fsdm.procedures::BusinessPartnerCreditRiskAssessmentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::BusinessPartnerCreditRiskAssessmentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::BusinessPartnerCreditRiskAssessmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::BusinessPartnerCreditRiskAssessmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BusinessPartnerCreditRiskAssessmentReason=' || TO_VARCHAR("BusinessPartnerCreditRiskAssessmentReason") || ' ' ||
                'CreditRiskAssessmentDate=' || TO_VARCHAR("CreditRiskAssessmentDate") || ' ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."CreditRiskAssessmentDate",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."CreditRiskAssessmentDate",
                        "IN"."_BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BusinessPartnerCreditRiskAssessmentReason",
                        "CreditRiskAssessmentDate",
                        "_BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerCreditRiskAssessmentReason",
                                    "IN"."CreditRiskAssessmentDate",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BusinessPartnerCreditRiskAssessmentReason",
                                    "IN"."CreditRiskAssessmentDate",
                                    "IN"."_BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "BusinessPartnerCreditRiskAssessmentReason" is null and
            "CreditRiskAssessmentDate" is null and
            "_BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "BusinessPartnerCreditRiskAssessmentReason",
            "CreditRiskAssessmentDate",
            "_BusinessPartner.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::BusinessPartnerCreditRiskAssessment" WHERE
            (
            "BusinessPartnerCreditRiskAssessmentReason" ,
            "CreditRiskAssessmentDate" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."BusinessPartnerCreditRiskAssessmentReason",
            "OLD"."CreditRiskAssessmentDate",
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" as "OLD"
        on
                              "IN"."BusinessPartnerCreditRiskAssessmentReason" = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
                              "IN"."CreditRiskAssessmentDate" = "OLD"."CreditRiskAssessmentDate" and
                              "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "BusinessPartnerCreditRiskAssessmentReason",
        "CreditRiskAssessmentDate",
        "_BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ComplianceStatus",
        "ComplianceStatusValidityEndDate",
        "ComplianceStatusValidityStartDate",
        "DataRequestDate",
        "DisclosureRequirement",
        "FollowUpDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_BusinessPartnerCreditRiskAssessmentReason" as "BusinessPartnerCreditRiskAssessmentReason" ,
            "OLD_CreditRiskAssessmentDate" as "CreditRiskAssessmentDate" ,
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ComplianceStatus" as "ComplianceStatus" ,
            "OLD_ComplianceStatusValidityEndDate" as "ComplianceStatusValidityEndDate" ,
            "OLD_ComplianceStatusValidityStartDate" as "ComplianceStatusValidityStartDate" ,
            "OLD_DataRequestDate" as "DataRequestDate" ,
            "OLD_DisclosureRequirement" as "DisclosureRequirement" ,
            "OLD_FollowUpDate" as "FollowUpDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."BusinessPartnerCreditRiskAssessmentReason",
                        "OLD"."CreditRiskAssessmentDate",
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."BusinessPartnerCreditRiskAssessmentReason" AS "OLD_BusinessPartnerCreditRiskAssessmentReason" ,
                "OLD"."CreditRiskAssessmentDate" AS "OLD_CreditRiskAssessmentDate" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ComplianceStatus" AS "OLD_ComplianceStatus" ,
                "OLD"."ComplianceStatusValidityEndDate" AS "OLD_ComplianceStatusValidityEndDate" ,
                "OLD"."ComplianceStatusValidityStartDate" AS "OLD_ComplianceStatusValidityStartDate" ,
                "OLD"."DataRequestDate" AS "OLD_DataRequestDate" ,
                "OLD"."DisclosureRequirement" AS "OLD_DisclosureRequirement" ,
                "OLD"."FollowUpDate" AS "OLD_FollowUpDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" as "OLD"
            on
                                      "IN"."BusinessPartnerCreditRiskAssessmentReason" = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
                                      "IN"."CreditRiskAssessmentDate" = "OLD"."CreditRiskAssessmentDate" and
                                      "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_BusinessPartnerCreditRiskAssessmentReason" as "BusinessPartnerCreditRiskAssessmentReason",
            "OLD_CreditRiskAssessmentDate" as "CreditRiskAssessmentDate",
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ComplianceStatus" as "ComplianceStatus",
            "OLD_ComplianceStatusValidityEndDate" as "ComplianceStatusValidityEndDate",
            "OLD_ComplianceStatusValidityStartDate" as "ComplianceStatusValidityStartDate",
            "OLD_DataRequestDate" as "DataRequestDate",
            "OLD_DisclosureRequirement" as "DisclosureRequirement",
            "OLD_FollowUpDate" as "FollowUpDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."BusinessPartnerCreditRiskAssessmentReason",
                        "OLD"."CreditRiskAssessmentDate",
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."BusinessPartnerCreditRiskAssessmentReason" AS "OLD_BusinessPartnerCreditRiskAssessmentReason" ,
                "OLD"."CreditRiskAssessmentDate" AS "OLD_CreditRiskAssessmentDate" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ComplianceStatus" AS "OLD_ComplianceStatus" ,
                "OLD"."ComplianceStatusValidityEndDate" AS "OLD_ComplianceStatusValidityEndDate" ,
                "OLD"."ComplianceStatusValidityStartDate" AS "OLD_ComplianceStatusValidityStartDate" ,
                "OLD"."DataRequestDate" AS "OLD_DataRequestDate" ,
                "OLD"."DisclosureRequirement" AS "OLD_DisclosureRequirement" ,
                "OLD"."FollowUpDate" AS "OLD_FollowUpDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::BusinessPartnerCreditRiskAssessment" as "OLD"
            on
               "IN"."BusinessPartnerCreditRiskAssessmentReason" = "OLD"."BusinessPartnerCreditRiskAssessmentReason" and
               "IN"."CreditRiskAssessmentDate" = "OLD"."CreditRiskAssessmentDate" and
               "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
