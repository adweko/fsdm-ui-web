PROCEDURE "sap.fsdm.procedures::ClassificationOfFundEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ClassificationOfFundTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ClassificationOfFundTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ClassificationOfFundTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Fund.FundID" is null and
            "_Fund._InvestmentCorporation.BusinessPartnerID" is null and
            "_FundClass.FundClass" is null and
            "_FundClass.FundClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_Fund.FundID" ,
                "_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "_FundClass.FundClass" ,
                "_FundClass.FundClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_FundClass.FundClass" ,
                "OLD"."_FundClass.FundClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ClassificationOfFund" "OLD"
            on
                "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                "IN"."_FundClass.FundClass" = "OLD"."_FundClass.FundClass" and
                "IN"."_FundClass.FundClassificationSystem" = "OLD"."_FundClass.FundClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_Fund.FundID" ,
            "_Fund._InvestmentCorporation.BusinessPartnerID" ,
            "_FundClass.FundClass" ,
            "_FundClass.FundClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_FundClass.FundClass" ,
                "OLD"."_FundClass.FundClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ClassificationOfFund_Historical" "OLD"
            on
                "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                "IN"."_FundClass.FundClass" = "OLD"."_FundClass.FundClass" and
                "IN"."_FundClass.FundClassificationSystem" = "OLD"."_FundClass.FundClassificationSystem" 
        );

END
