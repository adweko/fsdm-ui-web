PROCEDURE "sap.fsdm.procedures::FixedHolidayEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FixedHolidayTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FixedHolidayTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FixedHolidayTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "DayOfMonth" is null and
            "MonthOfYear" is null and
            "_BusinessCalendar.BusinessCalendar" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "DayOfMonth" ,
                "MonthOfYear" ,
                "_BusinessCalendar.BusinessCalendar" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."DayOfMonth" ,
                "OLD"."MonthOfYear" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FixedHoliday" "OLD"
            on
                "IN"."DayOfMonth" = "OLD"."DayOfMonth" and
                "IN"."MonthOfYear" = "OLD"."MonthOfYear" and
                "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "DayOfMonth" ,
            "MonthOfYear" ,
            "_BusinessCalendar.BusinessCalendar" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."DayOfMonth" ,
                "OLD"."MonthOfYear" ,
                "OLD"."_BusinessCalendar.BusinessCalendar" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FixedHoliday_Historical" "OLD"
            on
                "IN"."DayOfMonth" = "OLD"."DayOfMonth" and
                "IN"."MonthOfYear" = "OLD"."MonthOfYear" and
                "IN"."_BusinessCalendar.BusinessCalendar" = "OLD"."_BusinessCalendar.BusinessCalendar" 
        );

END
