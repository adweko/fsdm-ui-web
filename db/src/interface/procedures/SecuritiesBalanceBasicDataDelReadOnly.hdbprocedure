PROCEDURE "sap.fsdm.procedures::SecuritiesBalanceBasicDataDelReadOnly" (IN ROW "sap.fsdm.tabletypes::SecuritiesBalanceBasicDataTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::SecuritiesBalanceBasicDataTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SecuritiesBalanceBasicDataTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_FinancialInstrument.FinancialInstrumentID",
            "_SecuritiesAccount.FinancialContractID",
            "_SecuritiesAccount.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::SecuritiesBalanceBasicData" WHERE
            (
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SecuritiesBalanceBasicData" as "OLD"
        on
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                              "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                              "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FirstAcquisitionDate",
        "POCIAcquisitionDate",
        "POCIAcquisitionStatus",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_FirstAcquisitionDate" as "FirstAcquisitionDate" ,
            "OLD_POCIAcquisitionDate" as "POCIAcquisitionDate" ,
            "OLD_POCIAcquisitionStatus" as "POCIAcquisitionStatus" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" AS "OLD__SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" AS "OLD__SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."FirstAcquisitionDate" AS "OLD_FirstAcquisitionDate" ,
                "OLD"."POCIAcquisitionDate" AS "OLD_POCIAcquisitionDate" ,
                "OLD"."POCIAcquisitionStatus" AS "OLD_POCIAcquisitionStatus" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SecuritiesBalanceBasicData" as "OLD"
            on
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                      "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                                      "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_FirstAcquisitionDate" as "FirstAcquisitionDate",
            "OLD_POCIAcquisitionDate" as "POCIAcquisitionDate",
            "OLD_POCIAcquisitionStatus" as "POCIAcquisitionStatus",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" AS "OLD__SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" AS "OLD__SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."FirstAcquisitionDate" AS "OLD_FirstAcquisitionDate" ,
                "OLD"."POCIAcquisitionDate" AS "OLD_POCIAcquisitionDate" ,
                "OLD"."POCIAcquisitionStatus" AS "OLD_POCIAcquisitionStatus" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SecuritiesBalanceBasicData" as "OLD"
            on
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
               "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
