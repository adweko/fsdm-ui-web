namespace "sap"."fsdm";

using "sap"."fsdm"::"Fee";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"Interest";
using "sap"."fsdm"::"PaymentSchedule";
using "sap"."fsdm"::"PaymentWaterfall";

entity "WaterfallAssignment" {
    key "_Fee"                          : association to Fee {
                                                               SequenceNumber,
                                                               ASSOC_FinancialContract,
                                                               ASSOC_TransferOrder,
                                                               _LeaseService,
                                                               _Trade,
                                                               _TrancheInSyndication
                                                             }                                         not null;
    key "_FinancialContract"            : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    key "_FinancialInstrument"          : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_Interest"                     : association to Interest {
                                                                    SequenceNumber,
                                                                    ASSOC_CcyOfMultiCcyAccnt,
                                                                    ASSOC_FinancialContract,
                                                                    _DebtInstrument,
                                                                    _OptionOfReferenceRateSpecification,
                                                                    _OptionOfStrikeSpecification,
                                                                    _Trade
                                                                  }                                    not null;
    key "_PaymentSchedule"              : association to PaymentSchedule {
                                                                           SequenceNumber,
                                                                           ASSOC_FinancialContract,
                                                                           ASSOC_Receivable,
                                                                           _CreditCardLoan,
                                                                           _DebtInstrument,
                                                                           _InflationOptionComponent,
                                                                           _InterestRateOptionComponent
                                                                         }                             not null;
    key "_PaymentWaterfall"             : association to PaymentWaterfall {
                                                                            WaterfallCategory,
                                                                            _FinancialContract
                                                                          }                            not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                                                 not null;
        "SystemValidTo"                 : UTCTimestamp                                                 not null;
        "PositionInWaterfall"           : Integer;
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "WaterfallAssignment_Historical" {
    "_Fee"                          : association to Fee {
                                                           SequenceNumber,
                                                           ASSOC_FinancialContract,
                                                           ASSOC_TransferOrder,
                                                           _LeaseService,
                                                           _Trade,
                                                           _TrancheInSyndication
                                                         }                                         not null;
    "_FinancialContract"            : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }                           not null;
    "_FinancialInstrument"          : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_Interest"                     : association to Interest {
                                                                SequenceNumber,
                                                                ASSOC_CcyOfMultiCcyAccnt,
                                                                ASSOC_FinancialContract,
                                                                _DebtInstrument,
                                                                _OptionOfReferenceRateSpecification,
                                                                _OptionOfStrikeSpecification,
                                                                _Trade
                                                              }                                    not null;
    "_PaymentSchedule"              : association to PaymentSchedule {
                                                                       SequenceNumber,
                                                                       ASSOC_FinancialContract,
                                                                       ASSOC_Receivable,
                                                                       _CreditCardLoan,
                                                                       _DebtInstrument,
                                                                       _InflationOptionComponent,
                                                                       _InterestRateOptionComponent
                                                                     }                             not null;
    "_PaymentWaterfall"             : association to PaymentWaterfall {
                                                                        WaterfallCategory,
                                                                        _FinancialContract
                                                                      }                            not null;
    "BusinessValidFrom"             : LocalDate                                                    not null;
    "BusinessValidTo"               : LocalDate                                                    not null;
    "SystemValidFrom"               : UTCTimestamp                                                 not null;
    "SystemValidTo"                 : UTCTimestamp                                                 not null;
    "PositionInWaterfall"           : Integer;
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};