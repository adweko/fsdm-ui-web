PROCEDURE "sap.fsdm.procedures::CoveredFinancialContractAssignmentDelete" (IN ROW "sap.fsdm.tabletypes::CoveredFinancialContractAssignmentTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_CollateralPool.CollectionID=' || TO_VARCHAR("_CollateralPool.CollectionID") || ' ' ||
                '_CollateralPool.IDSystem=' || TO_VARCHAR("_CollateralPool.IDSystem") || ' ' ||
                '_CollateralPool._Client.BusinessPartnerID=' || TO_VARCHAR("_CollateralPool._Client.BusinessPartnerID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_CollateralPool.CollectionID",
                        "IN"."_CollateralPool.IDSystem",
                        "IN"."_CollateralPool._Client.BusinessPartnerID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_CollateralPool.CollectionID",
                        "_CollateralPool.IDSystem",
                        "_CollateralPool._Client.BusinessPartnerID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_CollateralPool._Client.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_CollateralPool.CollectionID",
                                    "IN"."_CollateralPool.IDSystem",
                                    "IN"."_CollateralPool._Client.BusinessPartnerID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_CollateralPool.CollectionID" is null and
            "_CollateralPool.IDSystem" is null and
            "_CollateralPool._Client.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CoveredFinancialContractAssignment" (
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CoverageAmount",
        "CoverageAmountCurrency",
        "CoverageShare",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID" ,
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem" ,
            "OLD__CollateralPool._Client.BusinessPartnerID" as "_CollateralPool._Client.BusinessPartnerID" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CoverageAmount" as "CoverageAmount" ,
            "OLD_CoverageAmountCurrency" as "CoverageAmountCurrency" ,
            "OLD_CoverageShare" as "CoverageShare" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem",
                        "OLD"."_CollateralPool._Client.BusinessPartnerID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_CollateralPool.CollectionID" AS "OLD__CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" AS "OLD__CollateralPool.IDSystem" ,
                "OLD"."_CollateralPool._Client.BusinessPartnerID" AS "OLD__CollateralPool._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CoverageAmount" AS "OLD_CoverageAmount" ,
                "OLD"."CoverageAmountCurrency" AS "OLD_CoverageAmountCurrency" ,
                "OLD"."CoverageShare" AS "OLD_CoverageShare" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CoveredFinancialContractAssignment" as "OLD"
            on
                      "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                      "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                      "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CoveredFinancialContractAssignment" (
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CoverageAmount",
        "CoverageAmountCurrency",
        "CoverageShare",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__CollateralPool.CollectionID" as "_CollateralPool.CollectionID",
            "OLD__CollateralPool.IDSystem" as "_CollateralPool.IDSystem",
            "OLD__CollateralPool._Client.BusinessPartnerID" as "_CollateralPool._Client.BusinessPartnerID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CoverageAmount" as "CoverageAmount",
            "OLD_CoverageAmountCurrency" as "CoverageAmountCurrency",
            "OLD_CoverageShare" as "CoverageShare",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_CollateralPool.CollectionID",
                        "OLD"."_CollateralPool.IDSystem",
                        "OLD"."_CollateralPool._Client.BusinessPartnerID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_CollateralPool.CollectionID" AS "OLD__CollateralPool.CollectionID" ,
                "OLD"."_CollateralPool.IDSystem" AS "OLD__CollateralPool.IDSystem" ,
                "OLD"."_CollateralPool._Client.BusinessPartnerID" AS "OLD__CollateralPool._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CoverageAmount" AS "OLD_CoverageAmount" ,
                "OLD"."CoverageAmountCurrency" AS "OLD_CoverageAmountCurrency" ,
                "OLD"."CoverageShare" AS "OLD_CoverageShare" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CoveredFinancialContractAssignment" as "OLD"
            on
                                                "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                                                "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                                                "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                                                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CoveredFinancialContractAssignment"
    where (
        "_CollateralPool.CollectionID",
        "_CollateralPool.IDSystem",
        "_CollateralPool._Client.BusinessPartnerID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_CollateralPool.CollectionID",
            "OLD"."_CollateralPool.IDSystem",
            "OLD"."_CollateralPool._Client.BusinessPartnerID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CoveredFinancialContractAssignment" as "OLD"
        on
                                       "IN"."_CollateralPool.CollectionID" = "OLD"."_CollateralPool.CollectionID" and
                                       "IN"."_CollateralPool.IDSystem" = "OLD"."_CollateralPool.IDSystem" and
                                       "IN"."_CollateralPool._Client.BusinessPartnerID" = "OLD"."_CollateralPool._Client.BusinessPartnerID" and
                                       "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                       "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
