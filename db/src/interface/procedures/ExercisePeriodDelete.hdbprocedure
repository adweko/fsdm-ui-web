PROCEDURE "sap.fsdm.procedures::ExercisePeriodDelete" (IN ROW "sap.fsdm.tabletypes::ExercisePeriodTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'StartDate=' || TO_VARCHAR("StartDate") || ' ' ||
                '_Option.FinancialContractID=' || TO_VARCHAR("_Option.FinancialContractID") || ' ' ||
                '_Option.IDSystem=' || TO_VARCHAR("_Option.IDSystem") || ' ' ||
                '_Swaption.FinancialContractID=' || TO_VARCHAR("_Swaption.FinancialContractID") || ' ' ||
                '_Swaption.IDSystem=' || TO_VARCHAR("_Swaption.IDSystem") || ' ' ||
                '_Warrant.FinancialInstrumentID=' || TO_VARCHAR("_Warrant.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."StartDate",
                        "IN"."_Option.FinancialContractID",
                        "IN"."_Option.IDSystem",
                        "IN"."_Swaption.FinancialContractID",
                        "IN"."_Swaption.IDSystem",
                        "IN"."_Warrant.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."StartDate",
                        "IN"."_Option.FinancialContractID",
                        "IN"."_Option.IDSystem",
                        "IN"."_Swaption.FinancialContractID",
                        "IN"."_Swaption.IDSystem",
                        "IN"."_Warrant.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "StartDate",
                        "_Option.FinancialContractID",
                        "_Option.IDSystem",
                        "_Swaption.FinancialContractID",
                        "_Swaption.IDSystem",
                        "_Warrant.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."StartDate",
                                    "IN"."_Option.FinancialContractID",
                                    "IN"."_Option.IDSystem",
                                    "IN"."_Swaption.FinancialContractID",
                                    "IN"."_Swaption.IDSystem",
                                    "IN"."_Warrant.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."StartDate",
                                    "IN"."_Option.FinancialContractID",
                                    "IN"."_Option.IDSystem",
                                    "IN"."_Swaption.FinancialContractID",
                                    "IN"."_Swaption.IDSystem",
                                    "IN"."_Warrant.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "StartDate" is null and
            "_Option.FinancialContractID" is null and
            "_Option.IDSystem" is null and
            "_Swaption.FinancialContractID" is null and
            "_Swaption.IDSystem" is null and
            "_Warrant.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ExercisePeriod" (
        "StartDate",
        "_Option.FinancialContractID",
        "_Option.IDSystem",
        "_Swaption.FinancialContractID",
        "_Swaption.IDSystem",
        "_Warrant.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "EndDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_StartDate" as "StartDate" ,
            "OLD__Option.FinancialContractID" as "_Option.FinancialContractID" ,
            "OLD__Option.IDSystem" as "_Option.IDSystem" ,
            "OLD__Swaption.FinancialContractID" as "_Swaption.FinancialContractID" ,
            "OLD__Swaption.IDSystem" as "_Swaption.IDSystem" ,
            "OLD__Warrant.FinancialInstrumentID" as "_Warrant.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."StartDate",
                        "OLD"."_Option.FinancialContractID",
                        "OLD"."_Option.IDSystem",
                        "OLD"."_Swaption.FinancialContractID",
                        "OLD"."_Swaption.IDSystem",
                        "OLD"."_Warrant.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."_Option.FinancialContractID" AS "OLD__Option.FinancialContractID" ,
                "OLD"."_Option.IDSystem" AS "OLD__Option.IDSystem" ,
                "OLD"."_Swaption.FinancialContractID" AS "OLD__Swaption.FinancialContractID" ,
                "OLD"."_Swaption.IDSystem" AS "OLD__Swaption.IDSystem" ,
                "OLD"."_Warrant.FinancialInstrumentID" AS "OLD__Warrant.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ExercisePeriod" as "OLD"
            on
                      "IN"."StartDate" = "OLD"."StartDate" and
                      "IN"."_Option.FinancialContractID" = "OLD"."_Option.FinancialContractID" and
                      "IN"."_Option.IDSystem" = "OLD"."_Option.IDSystem" and
                      "IN"."_Swaption.FinancialContractID" = "OLD"."_Swaption.FinancialContractID" and
                      "IN"."_Swaption.IDSystem" = "OLD"."_Swaption.IDSystem" and
                      "IN"."_Warrant.FinancialInstrumentID" = "OLD"."_Warrant.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ExercisePeriod" (
        "StartDate",
        "_Option.FinancialContractID",
        "_Option.IDSystem",
        "_Swaption.FinancialContractID",
        "_Swaption.IDSystem",
        "_Warrant.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "EndDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_StartDate" as "StartDate",
            "OLD__Option.FinancialContractID" as "_Option.FinancialContractID",
            "OLD__Option.IDSystem" as "_Option.IDSystem",
            "OLD__Swaption.FinancialContractID" as "_Swaption.FinancialContractID",
            "OLD__Swaption.IDSystem" as "_Swaption.IDSystem",
            "OLD__Warrant.FinancialInstrumentID" as "_Warrant.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_EndDate" as "EndDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."StartDate",
                        "OLD"."_Option.FinancialContractID",
                        "OLD"."_Option.IDSystem",
                        "OLD"."_Swaption.FinancialContractID",
                        "OLD"."_Swaption.IDSystem",
                        "OLD"."_Warrant.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."_Option.FinancialContractID" AS "OLD__Option.FinancialContractID" ,
                "OLD"."_Option.IDSystem" AS "OLD__Option.IDSystem" ,
                "OLD"."_Swaption.FinancialContractID" AS "OLD__Swaption.FinancialContractID" ,
                "OLD"."_Swaption.IDSystem" AS "OLD__Swaption.IDSystem" ,
                "OLD"."_Warrant.FinancialInstrumentID" AS "OLD__Warrant.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ExercisePeriod" as "OLD"
            on
                                                "IN"."StartDate" = "OLD"."StartDate" and
                                                "IN"."_Option.FinancialContractID" = "OLD"."_Option.FinancialContractID" and
                                                "IN"."_Option.IDSystem" = "OLD"."_Option.IDSystem" and
                                                "IN"."_Swaption.FinancialContractID" = "OLD"."_Swaption.FinancialContractID" and
                                                "IN"."_Swaption.IDSystem" = "OLD"."_Swaption.IDSystem" and
                                                "IN"."_Warrant.FinancialInstrumentID" = "OLD"."_Warrant.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ExercisePeriod"
    where (
        "StartDate",
        "_Option.FinancialContractID",
        "_Option.IDSystem",
        "_Swaption.FinancialContractID",
        "_Swaption.IDSystem",
        "_Warrant.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."StartDate",
            "OLD"."_Option.FinancialContractID",
            "OLD"."_Option.IDSystem",
            "OLD"."_Swaption.FinancialContractID",
            "OLD"."_Swaption.IDSystem",
            "OLD"."_Warrant.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ExercisePeriod" as "OLD"
        on
                                       "IN"."StartDate" = "OLD"."StartDate" and
                                       "IN"."_Option.FinancialContractID" = "OLD"."_Option.FinancialContractID" and
                                       "IN"."_Option.IDSystem" = "OLD"."_Option.IDSystem" and
                                       "IN"."_Swaption.FinancialContractID" = "OLD"."_Swaption.FinancialContractID" and
                                       "IN"."_Swaption.IDSystem" = "OLD"."_Swaption.IDSystem" and
                                       "IN"."_Warrant.FinancialInstrumentID" = "OLD"."_Warrant.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
