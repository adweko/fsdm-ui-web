PROCEDURE "sap.fsdm.procedures::PermittedEffectiveTerminationDatesEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::PermittedEffectiveTerminationDatesTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::PermittedEffectiveTerminationDatesTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::PermittedEffectiveTerminationDatesTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_TerminationOption.SequenceNumber" is null and
            "ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" is null and
            "ASSOC_TerminationOption._Trade.IDSystem" is null and
            "ASSOC_TerminationOption._Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "ASSOC_TerminationOption.SequenceNumber" ,
                "ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
                "ASSOC_TerminationOption._Trade.IDSystem" ,
                "ASSOC_TerminationOption._Trade.TradeID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_TerminationOption.SequenceNumber" ,
                "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."ASSOC_TerminationOption._Trade.IDSystem" ,
                "OLD"."ASSOC_TerminationOption._Trade.TradeID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PermittedEffectiveTerminationDates" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_TerminationOption.SequenceNumber" = "OLD"."ASSOC_TerminationOption.SequenceNumber" and
                "IN"."ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" and
                "IN"."ASSOC_TerminationOption._Trade.IDSystem" = "OLD"."ASSOC_TerminationOption._Trade.IDSystem" and
                "IN"."ASSOC_TerminationOption._Trade.TradeID" = "OLD"."ASSOC_TerminationOption._Trade.TradeID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "ASSOC_TerminationOption.SequenceNumber" ,
            "ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
            "ASSOC_TerminationOption._Trade.IDSystem" ,
            "ASSOC_TerminationOption._Trade.TradeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_TerminationOption.SequenceNumber" ,
                "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."ASSOC_TerminationOption._Trade.IDSystem" ,
                "OLD"."ASSOC_TerminationOption._Trade.TradeID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::PermittedEffectiveTerminationDates_Historical" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_TerminationOption.SequenceNumber" = "OLD"."ASSOC_TerminationOption.SequenceNumber" and
                "IN"."ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_TerminationOption.ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" = "OLD"."ASSOC_TerminationOption._RentalContractInformation.RentalContractInformationReferenceID" and
                "IN"."ASSOC_TerminationOption._Trade.IDSystem" = "OLD"."ASSOC_TerminationOption._Trade.IDSystem" and
                "IN"."ASSOC_TerminationOption._Trade.TradeID" = "OLD"."ASSOC_TerminationOption._Trade.TradeID" 
        );

END
