PROCEDURE "sap.fsdm.procedures::EligibleRatingMethodEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::EligibleRatingMethodTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::EligibleRatingMethodTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::EligibleRatingMethodTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Regulation" is null and
            "_RatingMethod.RatingMethod" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "Regulation" ,
                "_RatingMethod.RatingMethod" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."Regulation" ,
                "OLD"."_RatingMethod.RatingMethod" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EligibleRatingMethod" "OLD"
            on
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_RatingMethod.RatingMethod" = "OLD"."_RatingMethod.RatingMethod" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "Regulation" ,
            "_RatingMethod.RatingMethod" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."Regulation" ,
                "OLD"."_RatingMethod.RatingMethod" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EligibleRatingMethod_Historical" "OLD"
            on
                "IN"."Regulation" = "OLD"."Regulation" and
                "IN"."_RatingMethod.RatingMethod" = "OLD"."_RatingMethod.RatingMethod" 
        );

END
