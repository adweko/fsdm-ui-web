PROCEDURE "sap.fsdm.procedures::CurveDataPointEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::CurveDataPointTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::CurveDataPointTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::CurveDataPointTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "TimeToMaturity" is null and
            "TimeToMaturityUnit" is null and
            "ASSOC_YieldCurve.ProviderOfYieldCurve" is null and
            "ASSOC_YieldCurve.YieldCurveID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "TimeToMaturity" ,
                "TimeToMaturityUnit" ,
                "ASSOC_YieldCurve.ProviderOfYieldCurve" ,
                "ASSOC_YieldCurve.YieldCurveID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."TimeToMaturity" ,
                "OLD"."TimeToMaturityUnit" ,
                "OLD"."ASSOC_YieldCurve.ProviderOfYieldCurve" ,
                "OLD"."ASSOC_YieldCurve.YieldCurveID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CurveDataPoint" "OLD"
            on
                "IN"."TimeToMaturity" = "OLD"."TimeToMaturity" and
                "IN"."TimeToMaturityUnit" = "OLD"."TimeToMaturityUnit" and
                "IN"."ASSOC_YieldCurve.ProviderOfYieldCurve" = "OLD"."ASSOC_YieldCurve.ProviderOfYieldCurve" and
                "IN"."ASSOC_YieldCurve.YieldCurveID" = "OLD"."ASSOC_YieldCurve.YieldCurveID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "TimeToMaturity" ,
            "TimeToMaturityUnit" ,
            "ASSOC_YieldCurve.ProviderOfYieldCurve" ,
            "ASSOC_YieldCurve.YieldCurveID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."TimeToMaturity" ,
                "OLD"."TimeToMaturityUnit" ,
                "OLD"."ASSOC_YieldCurve.ProviderOfYieldCurve" ,
                "OLD"."ASSOC_YieldCurve.YieldCurveID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::CurveDataPoint_Historical" "OLD"
            on
                "IN"."TimeToMaturity" = "OLD"."TimeToMaturity" and
                "IN"."TimeToMaturityUnit" = "OLD"."TimeToMaturityUnit" and
                "IN"."ASSOC_YieldCurve.ProviderOfYieldCurve" = "OLD"."ASSOC_YieldCurve.ProviderOfYieldCurve" and
                "IN"."ASSOC_YieldCurve.YieldCurveID" = "OLD"."ASSOC_YieldCurve.YieldCurveID" 
        );

END
