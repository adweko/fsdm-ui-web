view "sap.fsdm.SQLViews::CollateralPortion_View" 
as select
      "PortionNumber" ,
      "ASSOC_CollateralAgreement.FinancialContractID" ,
      "ASSOC_CollateralAgreement.IDSystem" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID" ,
      "_Creditor.BusinessPartnerID" ,
      "AgreedCoverageEndDate" ,
      "AgreedCoverageStartDate" ,
      "CollateralPortionConsideredInBusinessPartnerRating" ,
      "CollateralPortionCoverageCategory" ,
      "CoveredPercentageOfObligations" ,
      "MaximumCollateralPortionAmount" ,
      "MaximumCollateralPortionAmountCurrency" ,
      "PercentageOfTotalCollateralForPortion" ,
      "PortionName" ,
      "RankAgainstOtherPortions" ,
      "RoleOfCreditor" ,
      "ValueDatedCollateralPortionAmount" ,
      "ValueDatedCollateralPortionAmountCurrency" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::CollateralPortion"
with associations
(
	join "sap.fsdm.SQLViews::CollateralDistributionResult_View" as "_CollateralDistributionResult_ASSOC_CollateralPortion"
	  on "PortionNumber" = "_CollateralDistributionResult_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.FinancialContractID" = "_CollateralDistributionResult_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CollateralDistributionResult_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "ASSOC_CollateralAgreement"
	  on "ASSOC_CollateralAgreement"."FinancialContractID" = "ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement"."IDSystem" = "ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "ASSOC_CoveredObligorInCaseOfFullCoverage"
	  on "ASSOC_CoveredObligorInCaseOfFullCoverage"."BusinessPartnerID" = "ASSOC_CoveredObligorInCaseOfFullCoverage.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "_Creditor"
	  on "_Creditor"."BusinessPartnerID" = "_Creditor.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::CollateralPortionBusinessPartnerAssignment_View" as "_CollateralPortionBusinessPartnerAssignment__CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_CollateralPortionBusinessPartnerAssignment__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CollateralPortionBusinessPartnerAssignment__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AND 
	     "PortionNumber" = "_CollateralPortionBusinessPartnerAssignment__CollateralPortion"."_CollateralPortion.PortionNumber"
	     ,
	join "sap.fsdm.SQLViews::CollateralPortionContractAssignment_View" as "_CollateralPortionContractAssignment_ASSOC_CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_CollateralPortionContractAssignment_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "PortionNumber" = "_CollateralPortionContractAssignment_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CollateralPortionContractAssignment_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CounterpartyCreditRiskCollateralResults_View" as "_CounterpartyCreditRiskCollateralResults__CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_CounterpartyCreditRiskCollateralResults__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CounterpartyCreditRiskCollateralResults__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem" AND 
	     "PortionNumber" = "_CounterpartyCreditRiskCollateralResults__CollateralPortion"."_CollateralPortion.PortionNumber"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExpectedLoss_View" as "_CreditRiskExpectedLoss_ASSOC_CollateralPortion"
	  on "PortionNumber" = "_CreditRiskExpectedLoss_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.FinancialContractID" = "_CreditRiskExpectedLoss_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CreditRiskExpectedLoss_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExposure_View" as "_CreditRiskExposure_ASSOC_CollateralPortion"
	  on "PortionNumber" = "_CreditRiskExposure_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.FinancialContractID" = "_CreditRiskExposure_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CreditRiskExposure_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExposure_View" as "_CreditRiskExposure__SecondCollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_CreditRiskExposure__SecondCollateralPortion"."_SecondCollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CreditRiskExposure__SecondCollateralPortion"."_SecondCollateralPortion.ASSOC_CollateralAgreement.IDSystem" AND 
	     "PortionNumber" = "_CreditRiskExposure__SecondCollateralPortion"."_SecondCollateralPortion.PortionNumber"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskLossGivenDefault_View" as "_CreditRiskLossGivenDefault_ASSOC_CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_CreditRiskLossGivenDefault_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "PortionNumber" = "_CreditRiskLossGivenDefault_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CreditRiskLossGivenDefault_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskRecoveryRate_View" as "_CreditRiskRecoveryRate__CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_CreditRiskRecoveryRate__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "PortionNumber" = "_CreditRiskRecoveryRate__CollateralPortion"."_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_CreditRiskRecoveryRate__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_FinancialInstrument__CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_FinancialInstrument__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "PortionNumber" = "_FinancialInstrument__CollateralPortion"."_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_FinancialInstrument__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::PortionStatus_View" as "_PortionStatus__CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_PortionStatus__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "PortionNumber" = "_PortionStatus__CollateralPortion"."_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_PortionStatus__CollateralPortion"."_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Valuation_View" as "_Valuation_ASSOC_CollateralPortion"
	  on "ASSOC_CollateralAgreement.FinancialContractID" = "_Valuation_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.FinancialContractID" AND 
	     "PortionNumber" = "_Valuation_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.PortionNumber" AND 
	     "ASSOC_CollateralAgreement.IDSystem" = "_Valuation_ASSOC_CollateralPortion"."ASSOC_CollateralPortion.ASSOC_CollateralAgreement.IDSystem"
	     
);