PROCEDURE "sap.fsdm.procedures::ClaimDelete" (IN ROW "sap.fsdm.tabletypes::ClaimTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ID=' || TO_VARCHAR("ID") || ' ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ID",
                        "IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::Claim" (
        "ID",
        "IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "AgentReportedDate",
        "EstimatedLossDate",
        "EstimatedLossTime",
        "IncidentType",
        "InsurerReportedDate",
        "LifeCycleStatus",
        "LossDate",
        "LossTime",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID" ,
            "OLD_IDSystem" as "IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD_AgentReportedDate" as "AgentReportedDate" ,
            "OLD_EstimatedLossDate" as "EstimatedLossDate" ,
            "OLD_EstimatedLossTime" as "EstimatedLossTime" ,
            "OLD_IncidentType" as "IncidentType" ,
            "OLD_InsurerReportedDate" as "InsurerReportedDate" ,
            "OLD_LifeCycleStatus" as "LifeCycleStatus" ,
            "OLD_LossDate" as "LossDate" ,
            "OLD_LossTime" as "LossTime" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."AgentReportedDate" AS "OLD_AgentReportedDate" ,
                "OLD"."EstimatedLossDate" AS "OLD_EstimatedLossDate" ,
                "OLD"."EstimatedLossTime" AS "OLD_EstimatedLossTime" ,
                "OLD"."IncidentType" AS "OLD_IncidentType" ,
                "OLD"."InsurerReportedDate" AS "OLD_InsurerReportedDate" ,
                "OLD"."LifeCycleStatus" AS "OLD_LifeCycleStatus" ,
                "OLD"."LossDate" AS "OLD_LossDate" ,
                "OLD"."LossTime" AS "OLD_LossTime" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Claim" as "OLD"
            on
                      "IN"."ID" = "OLD"."ID" and
                      "IN"."IDSystem" = "OLD"."IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::Claim" (
        "ID",
        "IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "AgentReportedDate",
        "EstimatedLossDate",
        "EstimatedLossTime",
        "IncidentType",
        "InsurerReportedDate",
        "LifeCycleStatus",
        "LossDate",
        "LossTime",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID",
            "OLD_IDSystem" as "IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD_AgentReportedDate" as "AgentReportedDate",
            "OLD_EstimatedLossDate" as "EstimatedLossDate",
            "OLD_EstimatedLossTime" as "EstimatedLossTime",
            "OLD_IncidentType" as "IncidentType",
            "OLD_InsurerReportedDate" as "InsurerReportedDate",
            "OLD_LifeCycleStatus" as "LifeCycleStatus",
            "OLD_LossDate" as "LossDate",
            "OLD_LossTime" as "LossTime",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."AgentReportedDate" AS "OLD_AgentReportedDate" ,
                "OLD"."EstimatedLossDate" AS "OLD_EstimatedLossDate" ,
                "OLD"."EstimatedLossTime" AS "OLD_EstimatedLossTime" ,
                "OLD"."IncidentType" AS "OLD_IncidentType" ,
                "OLD"."InsurerReportedDate" AS "OLD_InsurerReportedDate" ,
                "OLD"."LifeCycleStatus" AS "OLD_LifeCycleStatus" ,
                "OLD"."LossDate" AS "OLD_LossDate" ,
                "OLD"."LossTime" AS "OLD_LossTime" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Claim" as "OLD"
            on
                                                "IN"."ID" = "OLD"."ID" and
                                                "IN"."IDSystem" = "OLD"."IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::Claim"
    where (
        "ID",
        "IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ID",
            "OLD"."IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Claim" as "OLD"
        on
                                       "IN"."ID" = "OLD"."ID" and
                                       "IN"."IDSystem" = "OLD"."IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
