PROCEDURE "sap.fsdm.procedures::FairValueEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FairValueTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FairValueTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FairValueTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountingChangeSequenceNumber" is null and
            "ComponentNumber" is null and
            "Currency" is null and
            "FVCalculationMethod" is null and
            "IndicatorResultBeforeChange" is null and
            "LotID" is null and
            "RoleOfPayer" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InvestmentAccount.FinancialContractID" is null and
            "_InvestmentAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "AccountingChangeSequenceNumber" ,
                "ComponentNumber" ,
                "Currency" ,
                "FVCalculationMethod" ,
                "IndicatorResultBeforeChange" ,
                "LotID" ,
                "RoleOfPayer" ,
                "_AccountingSystem.AccountingSystemID" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "_Collection._Client.BusinessPartnerID" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_InvestmentAccount.FinancialContractID" ,
                "_InvestmentAccount.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."AccountingChangeSequenceNumber" ,
                "OLD"."ComponentNumber" ,
                "OLD"."Currency" ,
                "OLD"."FVCalculationMethod" ,
                "OLD"."IndicatorResultBeforeChange" ,
                "OLD"."LotID" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FairValue" "OLD"
            on
                "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."Currency" = "OLD"."Currency" and
                "IN"."FVCalculationMethod" = "OLD"."FVCalculationMethod" and
                "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "AccountingChangeSequenceNumber" ,
            "ComponentNumber" ,
            "Currency" ,
            "FVCalculationMethod" ,
            "IndicatorResultBeforeChange" ,
            "LotID" ,
            "RoleOfPayer" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InvestmentAccount.FinancialContractID" ,
            "_InvestmentAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."AccountingChangeSequenceNumber" ,
                "OLD"."ComponentNumber" ,
                "OLD"."Currency" ,
                "OLD"."FVCalculationMethod" ,
                "OLD"."IndicatorResultBeforeChange" ,
                "OLD"."LotID" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InvestmentAccount.FinancialContractID" ,
                "OLD"."_InvestmentAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FairValue_Historical" "OLD"
            on
                "IN"."AccountingChangeSequenceNumber" = "OLD"."AccountingChangeSequenceNumber" and
                "IN"."ComponentNumber" = "OLD"."ComponentNumber" and
                "IN"."Currency" = "OLD"."Currency" and
                "IN"."FVCalculationMethod" = "OLD"."FVCalculationMethod" and
                "IN"."IndicatorResultBeforeChange" = "OLD"."IndicatorResultBeforeChange" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_InvestmentAccount.FinancialContractID" = "OLD"."_InvestmentAccount.FinancialContractID" and
                "IN"."_InvestmentAccount.IDSystem" = "OLD"."_InvestmentAccount.IDSystem" 
        );

END
