PROCEDURE "sap.fsdm.procedures::IndexSpecificationDelete" (IN ROW "sap.fsdm.tabletypes::IndexSpecificationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Index.IndexID=' || TO_VARCHAR("_Index.IndexID") || ' ' ||
                '_LifeAndAnnuityInsuranceCoverage.ID=' || TO_VARCHAR("_LifeAndAnnuityInsuranceCoverage.ID") || ' ' ||
                '_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID=' || TO_VARCHAR("_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID") || ' ' ||
                '_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem=' || TO_VARCHAR("_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem") || ' ' ||
                '_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID=' || TO_VARCHAR("_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID") || ' ' ||
                '_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem=' || TO_VARCHAR("_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem") || ' ' ||
                '_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Index.IndexID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage.ID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Index.IndexID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage.ID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Index.IndexID",
                        "_LifeAndAnnuityInsuranceCoverage.ID",
                        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Index.IndexID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage.ID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Index.IndexID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage.ID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                                    "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Index.IndexID" is null and
            "_LifeAndAnnuityInsuranceCoverage.ID" is null and
            "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::IndexSpecification" (
        "_Index.IndexID",
        "_LifeAndAnnuityInsuranceCoverage.ID",
        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AllocationAmount",
        "AllocationPercent",
        "CapRatePercent",
        "EndDate",
        "FloorRatePercent",
        "InceptionIndexLevel",
        "ParticipationPercent",
        "ShieldRatePercent",
        "StartDate",
        "StepRatePercent",
        "StepRateSelectedIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Index.IndexID" as "_Index.IndexID" ,
            "OLD__LifeAndAnnuityInsuranceCoverage.ID" as "_LifeAndAnnuityInsuranceCoverage.ID" ,
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" as "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" as "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AllocationAmount" as "AllocationAmount" ,
            "OLD_AllocationPercent" as "AllocationPercent" ,
            "OLD_CapRatePercent" as "CapRatePercent" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_FloorRatePercent" as "FloorRatePercent" ,
            "OLD_InceptionIndexLevel" as "InceptionIndexLevel" ,
            "OLD_ParticipationPercent" as "ParticipationPercent" ,
            "OLD_ShieldRatePercent" as "ShieldRatePercent" ,
            "OLD_StartDate" as "StartDate" ,
            "OLD_StepRatePercent" as "StepRatePercent" ,
            "OLD_StepRateSelectedIndicator" as "StepRateSelectedIndicator" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Index.IndexID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage.ID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_Index.IndexID" AS "OLD__Index.IndexID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage.ID" AS "OLD__LifeAndAnnuityInsuranceCoverage.ID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AllocationAmount" AS "OLD_AllocationAmount" ,
                "OLD"."AllocationPercent" AS "OLD_AllocationPercent" ,
                "OLD"."CapRatePercent" AS "OLD_CapRatePercent" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."FloorRatePercent" AS "OLD_FloorRatePercent" ,
                "OLD"."InceptionIndexLevel" AS "OLD_InceptionIndexLevel" ,
                "OLD"."ParticipationPercent" AS "OLD_ParticipationPercent" ,
                "OLD"."ShieldRatePercent" AS "OLD_ShieldRatePercent" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."StepRatePercent" AS "OLD_StepRatePercent" ,
                "OLD"."StepRateSelectedIndicator" AS "OLD_StepRateSelectedIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::IndexSpecification" as "OLD"
            on
                      "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                      "IN"."_LifeAndAnnuityInsuranceCoverage.ID" = "OLD"."_LifeAndAnnuityInsuranceCoverage.ID" and
                      "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" and
                      "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" and
                      "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                      "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                      "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::IndexSpecification" (
        "_Index.IndexID",
        "_LifeAndAnnuityInsuranceCoverage.ID",
        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AllocationAmount",
        "AllocationPercent",
        "CapRatePercent",
        "EndDate",
        "FloorRatePercent",
        "InceptionIndexLevel",
        "ParticipationPercent",
        "ShieldRatePercent",
        "StartDate",
        "StepRatePercent",
        "StepRateSelectedIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Index.IndexID" as "_Index.IndexID",
            "OLD__LifeAndAnnuityInsuranceCoverage.ID" as "_LifeAndAnnuityInsuranceCoverage.ID",
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" as "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" as "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AllocationAmount" as "AllocationAmount",
            "OLD_AllocationPercent" as "AllocationPercent",
            "OLD_CapRatePercent" as "CapRatePercent",
            "OLD_EndDate" as "EndDate",
            "OLD_FloorRatePercent" as "FloorRatePercent",
            "OLD_InceptionIndexLevel" as "InceptionIndexLevel",
            "OLD_ParticipationPercent" as "ParticipationPercent",
            "OLD_ShieldRatePercent" as "ShieldRatePercent",
            "OLD_StartDate" as "StartDate",
            "OLD_StepRatePercent" as "StepRatePercent",
            "OLD_StepRateSelectedIndicator" as "StepRateSelectedIndicator",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Index.IndexID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage.ID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Index.IndexID" AS "OLD__Index.IndexID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage.ID" AS "OLD__LifeAndAnnuityInsuranceCoverage.ID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AllocationAmount" AS "OLD_AllocationAmount" ,
                "OLD"."AllocationPercent" AS "OLD_AllocationPercent" ,
                "OLD"."CapRatePercent" AS "OLD_CapRatePercent" ,
                "OLD"."EndDate" AS "OLD_EndDate" ,
                "OLD"."FloorRatePercent" AS "OLD_FloorRatePercent" ,
                "OLD"."InceptionIndexLevel" AS "OLD_InceptionIndexLevel" ,
                "OLD"."ParticipationPercent" AS "OLD_ParticipationPercent" ,
                "OLD"."ShieldRatePercent" AS "OLD_ShieldRatePercent" ,
                "OLD"."StartDate" AS "OLD_StartDate" ,
                "OLD"."StepRatePercent" AS "OLD_StepRatePercent" ,
                "OLD"."StepRateSelectedIndicator" AS "OLD_StepRateSelectedIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::IndexSpecification" as "OLD"
            on
                                                "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                                                "IN"."_LifeAndAnnuityInsuranceCoverage.ID" = "OLD"."_LifeAndAnnuityInsuranceCoverage.ID" and
                                                "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" and
                                                "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" and
                                                "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                                                "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                                                "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::IndexSpecification"
    where (
        "_Index.IndexID",
        "_LifeAndAnnuityInsuranceCoverage.ID",
        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
        "_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_Index.IndexID",
            "OLD"."_LifeAndAnnuityInsuranceCoverage.ID",
            "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem",
            "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::IndexSpecification" as "OLD"
        on
                                       "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                                       "IN"."_LifeAndAnnuityInsuranceCoverage.ID" = "OLD"."_LifeAndAnnuityInsuranceCoverage.ID" and
                                       "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" and
                                       "IN"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" and
                                       "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                                       "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                                       "IN"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
