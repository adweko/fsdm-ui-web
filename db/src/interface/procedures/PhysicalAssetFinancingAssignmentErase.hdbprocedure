PROCEDURE "sap.fsdm.procedures::PhysicalAssetFinancingAssignmentErase" (IN ROW "sap.fsdm.tabletypes::PhysicalAssetFinancingAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::PhysicalAssetFinancingAssignment"
        WHERE
        (            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_PhysicalAsset.PhysicalAssetID" 
        ) in
        (
            select                 "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalAssetFinancingAssignment" "OLD"
            on
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        delete from "sap.fsdm::PhysicalAssetFinancingAssignment_Historical"
        WHERE
        (
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_PhysicalAsset.PhysicalAssetID" 
        ) in
        (
            select
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" 
            from :ROW "IN"
            inner join "sap.fsdm::PhysicalAssetFinancingAssignment_Historical" "OLD"
            on
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" 
        );

END
