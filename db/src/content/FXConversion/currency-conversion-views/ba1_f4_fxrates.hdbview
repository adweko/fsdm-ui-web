-- This view converts the FX rates from the SAP Financial Services Data Model format
-- to the field names and format used for the HANA currency translation
-- while using the full precision of the FSDM data model rates.
--
-- To translate currencies through a base currency, we assume that 
-- you have this currency in the BaseCurrency column of table
-- EndOfDayExchangeRateObservation (for example, EUR in the EU area
-- or USD in the USA)
-- In this case, the BaseCurrency would be mapped into TO_CCY and
-- direct courses in FSDM would be indirect/negative after the mapping
-- in the /BA1/F4_FXRATES format.
--
-- The following points are typically adapted in this view:
-- 1. Instead of the Close rate, you can map any of the other rates
-- 2. You can adapt the map_pricedataprovider_to_mdcode view to
--    map the PriceDataProvider to a master data code.
--    This may be necessary if you want to use the CalculationView conversion.
-- 3. You can change the rate type or use a cross join to use all rate types
--    on the same data from the EndOfDayExchangeRateObservation table
--    (for the cross join, look at the comment at the bottom)
view "/BA1/F4_FXRATES" as
  select 
    '100'                                  as MANDT,      -- On HANA, always keep constant, not used in FSDM
    map_pricedataprovider_to_mdcode.mdcode as MDCODE,     -- Distinguish different PriceDataProviders as master data codes.
    'M'                                    as RATETYPE,   -- Alternatively, you can derive the rate type from the PriceDataProvider 
                                                          -- or use a cross join to use the same price data provider in all configurations
    "QuoteCurrency"                        as FROM_CCY,
    "BaseCurrency"                         as TO_CCY,     -- To convert through a third currency you must use the TO_CCY column.
    '0'                                    as conv_type,  -- 0: Middle Rate (not supported in HANA FX conversion: 1: Bid Rate, 2: Ask Rate, 3: Spread)
	"BusinessValidFrom"                    as VALID_DATE,
    TO_VARCHAR ("SystemValidFrom", 'YYYYMMDDHH24MISSFF7') as sys_time,
    ''                                     as STATUS,     -- Market Data is Active
    ''                                     as REL_STATUS, -- Market Data is Released
    CASE "QuotationType"                                  -- In ABAP, indirect quotes are modelled as negative rates. Above, we have used the BaseCurrency as TO_CCY, so here it is reversed
       WHEN 'Direct' then -1 * "Close"                    -- Other rates than "Close" can be used here
       ELSE  "Close"
    END                                    as FX_RATE,
    "ChangingUserInSourceSystem"           as UNAME
    from "sap.fsdm::EndOfDayExchangeRateObservation" inner join map_pricedataprovider_to_mdcode 
      on "sap.fsdm::EndOfDayExchangeRateObservation"."PriceDataProvider" = map_pricedataprovider_to_mdcode."PriceDataProvider"
-- 	cross join (select distinct ratetype from "/BA1/TF4_FXRTTYP") as c4; -- if you want to use the same PriceDataProvider for all rate types      
