PROCEDURE "sap.fsdm.procedures::RiskParameterEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RiskParameterTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RiskParameterTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RiskParameterTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "EstimationMethod" is null and
            "RiskParameterType" is null and
            "RiskProvisionScenario" is null and
            "TimeHorizon" is null and
            "TimeHorizonUnit" is null and
            "_Segment.SegmentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "EstimationMethod" ,
                "RiskParameterType" ,
                "RiskProvisionScenario" ,
                "TimeHorizon" ,
                "TimeHorizonUnit" ,
                "_Segment.SegmentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."EstimationMethod" ,
                "OLD"."RiskParameterType" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."TimeHorizon" ,
                "OLD"."TimeHorizonUnit" ,
                "OLD"."_Segment.SegmentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RiskParameter" "OLD"
            on
                "IN"."EstimationMethod" = "OLD"."EstimationMethod" and
                "IN"."RiskParameterType" = "OLD"."RiskParameterType" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                "IN"."TimeHorizonUnit" = "OLD"."TimeHorizonUnit" and
                "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "EstimationMethod" ,
            "RiskParameterType" ,
            "RiskProvisionScenario" ,
            "TimeHorizon" ,
            "TimeHorizonUnit" ,
            "_Segment.SegmentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."EstimationMethod" ,
                "OLD"."RiskParameterType" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."TimeHorizon" ,
                "OLD"."TimeHorizonUnit" ,
                "OLD"."_Segment.SegmentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RiskParameter_Historical" "OLD"
            on
                "IN"."EstimationMethod" = "OLD"."EstimationMethod" and
                "IN"."RiskParameterType" = "OLD"."RiskParameterType" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                "IN"."TimeHorizonUnit" = "OLD"."TimeHorizonUnit" and
                "IN"."_Segment.SegmentID" = "OLD"."_Segment.SegmentID" 
        );

END
