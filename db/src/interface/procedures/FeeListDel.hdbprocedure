PROCEDURE "sap.fsdm.procedures::FeeListDel" (IN ROW "sap.fsdm.tabletypes::FeeTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                'ASSOC_TransferOrder.IDSystem=' || TO_VARCHAR("ASSOC_TransferOrder.IDSystem") || ' ' ||
                'ASSOC_TransferOrder.TransferOrderID=' || TO_VARCHAR("ASSOC_TransferOrder.TransferOrderID") || ' ' ||
                '_LeaseService.ID=' || TO_VARCHAR("_LeaseService.ID") || ' ' ||
                '_LeaseService._LeaseServiceAgreement.FinancialContractID=' || TO_VARCHAR("_LeaseService._LeaseServiceAgreement.FinancialContractID") || ' ' ||
                '_LeaseService._LeaseServiceAgreement.IDSystem=' || TO_VARCHAR("_LeaseService._LeaseServiceAgreement.IDSystem") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                '_TrancheInSyndication.TrancheSequenceNumber=' || TO_VARCHAR("_TrancheInSyndication.TrancheSequenceNumber") || ' ' ||
                '_TrancheInSyndication._SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_TrancheInSyndication._SyndicationAgreement.FinancialContractID") || ' ' ||
                '_TrancheInSyndication._SyndicationAgreement.IDSystem=' || TO_VARCHAR("_TrancheInSyndication._SyndicationAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_TransferOrder.IDSystem",
                        "IN"."ASSOC_TransferOrder.TransferOrderID",
                        "IN"."_LeaseService.ID",
                        "IN"."_LeaseService._LeaseServiceAgreement.FinancialContractID",
                        "IN"."_LeaseService._LeaseServiceAgreement.IDSystem",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID",
                        "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_TransferOrder.IDSystem",
                        "IN"."ASSOC_TransferOrder.TransferOrderID",
                        "IN"."_LeaseService.ID",
                        "IN"."_LeaseService._LeaseServiceAgreement.FinancialContractID",
                        "IN"."_LeaseService._LeaseServiceAgreement.IDSystem",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID",
                        "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "ASSOC_TransferOrder.IDSystem",
                        "ASSOC_TransferOrder.TransferOrderID",
                        "_LeaseService.ID",
                        "_LeaseService._LeaseServiceAgreement.FinancialContractID",
                        "_LeaseService._LeaseServiceAgreement.IDSystem",
                        "_Trade.IDSystem",
                        "_Trade.TradeID",
                        "_TrancheInSyndication.TrancheSequenceNumber",
                        "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "_TrancheInSyndication._SyndicationAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_TransferOrder.IDSystem",
                                    "IN"."ASSOC_TransferOrder.TransferOrderID",
                                    "IN"."_LeaseService.ID",
                                    "IN"."_LeaseService._LeaseServiceAgreement.FinancialContractID",
                                    "IN"."_LeaseService._LeaseServiceAgreement.IDSystem",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID",
                                    "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_TransferOrder.IDSystem",
                                    "IN"."ASSOC_TransferOrder.TransferOrderID",
                                    "IN"."_LeaseService.ID",
                                    "IN"."_LeaseService._LeaseServiceAgreement.FinancialContractID",
                                    "IN"."_LeaseService._LeaseServiceAgreement.IDSystem",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID",
                                    "IN"."_TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_TransferOrder.IDSystem" is null and
            "ASSOC_TransferOrder.TransferOrderID" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" is null and
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_Trade.TradeID" is null and
            "_Trade.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::Fee" (
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "_LeaseService.ID",
        "_LeaseService._LeaseServiceAgreement.FinancialContractID",
        "_LeaseService._LeaseServiceAgreement.IDSystem",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "_TrancheInSyndication.TrancheSequenceNumber",
        "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "ConditionAcceptanceDate",
        "ConditionFixedPeriodEndDate",
        "ConditionFixedPeriodStartDate",
        "DayOfMonthOfFeeBillingDate",
        "EventDescription",
        "EventTriggerType",
        "FeeAmount",
        "FeeAssessmentBase",
        "FeeAssessmentBaseAggregationMethod",
        "FeeCalculationMethod",
        "FeeCategory",
        "FeeChargingEndDate",
        "FeeChargingStartDate",
        "FeeCurrency",
        "FeeLowerLimitAmount",
        "FeePercentage",
        "FeeSubtype",
        "FeeTruncatedForStubPeriods",
        "FeeType",
        "FeeUpperLimitAmount",
        "FirstFeeBillingDate",
        "FirstRegularFeeBillingDate",
        "LastFeeBillingDate",
        "PreconditionExists",
        "ReccurringFeePeriodTimeUnit",
        "RecurringFeePeriodLength",
        "RoleOfPayer",
        "ScaleApplies",
        "WaiverCode",
        "WaiverNote",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD_ASSOC_TransferOrder.IDSystem" as "ASSOC_TransferOrder.IDSystem" ,
            "OLD_ASSOC_TransferOrder.TransferOrderID" as "ASSOC_TransferOrder.TransferOrderID" ,
            "OLD__LeaseService.ID" as "_LeaseService.ID" ,
            "OLD__LeaseService._LeaseServiceAgreement.FinancialContractID" as "_LeaseService._LeaseServiceAgreement.FinancialContractID" ,
            "OLD__LeaseService._LeaseServiceAgreement.IDSystem" as "_LeaseService._LeaseServiceAgreement.IDSystem" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "OLD__TrancheInSyndication.TrancheSequenceNumber" as "_TrancheInSyndication.TrancheSequenceNumber" ,
            "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" as "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_ConditionAcceptanceDate" as "ConditionAcceptanceDate" ,
            "OLD_ConditionFixedPeriodEndDate" as "ConditionFixedPeriodEndDate" ,
            "OLD_ConditionFixedPeriodStartDate" as "ConditionFixedPeriodStartDate" ,
            "OLD_DayOfMonthOfFeeBillingDate" as "DayOfMonthOfFeeBillingDate" ,
            "OLD_EventDescription" as "EventDescription" ,
            "OLD_EventTriggerType" as "EventTriggerType" ,
            "OLD_FeeAmount" as "FeeAmount" ,
            "OLD_FeeAssessmentBase" as "FeeAssessmentBase" ,
            "OLD_FeeAssessmentBaseAggregationMethod" as "FeeAssessmentBaseAggregationMethod" ,
            "OLD_FeeCalculationMethod" as "FeeCalculationMethod" ,
            "OLD_FeeCategory" as "FeeCategory" ,
            "OLD_FeeChargingEndDate" as "FeeChargingEndDate" ,
            "OLD_FeeChargingStartDate" as "FeeChargingStartDate" ,
            "OLD_FeeCurrency" as "FeeCurrency" ,
            "OLD_FeeLowerLimitAmount" as "FeeLowerLimitAmount" ,
            "OLD_FeePercentage" as "FeePercentage" ,
            "OLD_FeeSubtype" as "FeeSubtype" ,
            "OLD_FeeTruncatedForStubPeriods" as "FeeTruncatedForStubPeriods" ,
            "OLD_FeeType" as "FeeType" ,
            "OLD_FeeUpperLimitAmount" as "FeeUpperLimitAmount" ,
            "OLD_FirstFeeBillingDate" as "FirstFeeBillingDate" ,
            "OLD_FirstRegularFeeBillingDate" as "FirstRegularFeeBillingDate" ,
            "OLD_LastFeeBillingDate" as "LastFeeBillingDate" ,
            "OLD_PreconditionExists" as "PreconditionExists" ,
            "OLD_ReccurringFeePeriodTimeUnit" as "ReccurringFeePeriodTimeUnit" ,
            "OLD_RecurringFeePeriodLength" as "RecurringFeePeriodLength" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD_ScaleApplies" as "ScaleApplies" ,
            "OLD_WaiverCode" as "WaiverCode" ,
            "OLD_WaiverNote" as "WaiverNote" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_TransferOrder.IDSystem",
                        "OLD"."ASSOC_TransferOrder.TransferOrderID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_Trade.TradeID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.IDSystem" AS "OLD_ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.TransferOrderID" AS "OLD_ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."_LeaseService.ID" AS "OLD__LeaseService.ID" ,
                "OLD"."_LeaseService._LeaseServiceAgreement.FinancialContractID" AS "OLD__LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LeaseService._LeaseServiceAgreement.IDSystem" AS "OLD__LeaseService._LeaseServiceAgreement.IDSystem" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" AS "OLD__TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."ConditionAcceptanceDate" AS "OLD_ConditionAcceptanceDate" ,
                "OLD"."ConditionFixedPeriodEndDate" AS "OLD_ConditionFixedPeriodEndDate" ,
                "OLD"."ConditionFixedPeriodStartDate" AS "OLD_ConditionFixedPeriodStartDate" ,
                "OLD"."DayOfMonthOfFeeBillingDate" AS "OLD_DayOfMonthOfFeeBillingDate" ,
                "OLD"."EventDescription" AS "OLD_EventDescription" ,
                "OLD"."EventTriggerType" AS "OLD_EventTriggerType" ,
                "OLD"."FeeAmount" AS "OLD_FeeAmount" ,
                "OLD"."FeeAssessmentBase" AS "OLD_FeeAssessmentBase" ,
                "OLD"."FeeAssessmentBaseAggregationMethod" AS "OLD_FeeAssessmentBaseAggregationMethod" ,
                "OLD"."FeeCalculationMethod" AS "OLD_FeeCalculationMethod" ,
                "OLD"."FeeCategory" AS "OLD_FeeCategory" ,
                "OLD"."FeeChargingEndDate" AS "OLD_FeeChargingEndDate" ,
                "OLD"."FeeChargingStartDate" AS "OLD_FeeChargingStartDate" ,
                "OLD"."FeeCurrency" AS "OLD_FeeCurrency" ,
                "OLD"."FeeLowerLimitAmount" AS "OLD_FeeLowerLimitAmount" ,
                "OLD"."FeePercentage" AS "OLD_FeePercentage" ,
                "OLD"."FeeSubtype" AS "OLD_FeeSubtype" ,
                "OLD"."FeeTruncatedForStubPeriods" AS "OLD_FeeTruncatedForStubPeriods" ,
                "OLD"."FeeType" AS "OLD_FeeType" ,
                "OLD"."FeeUpperLimitAmount" AS "OLD_FeeUpperLimitAmount" ,
                "OLD"."FirstFeeBillingDate" AS "OLD_FirstFeeBillingDate" ,
                "OLD"."FirstRegularFeeBillingDate" AS "OLD_FirstRegularFeeBillingDate" ,
                "OLD"."LastFeeBillingDate" AS "OLD_LastFeeBillingDate" ,
                "OLD"."PreconditionExists" AS "OLD_PreconditionExists" ,
                "OLD"."ReccurringFeePeriodTimeUnit" AS "OLD_ReccurringFeePeriodTimeUnit" ,
                "OLD"."RecurringFeePeriodLength" AS "OLD_RecurringFeePeriodLength" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."ScaleApplies" AS "OLD_ScaleApplies" ,
                "OLD"."WaiverCode" AS "OLD_WaiverCode" ,
                "OLD"."WaiverNote" AS "OLD_WaiverNote" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Fee" as "OLD"
            on
                      "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                      "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" and
                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                      "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" and
                      "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::Fee" (
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "_LeaseService.ID",
        "_LeaseService._LeaseServiceAgreement.FinancialContractID",
        "_LeaseService._LeaseServiceAgreement.IDSystem",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "_TrancheInSyndication.TrancheSequenceNumber",
        "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "ConditionAcceptanceDate",
        "ConditionFixedPeriodEndDate",
        "ConditionFixedPeriodStartDate",
        "DayOfMonthOfFeeBillingDate",
        "EventDescription",
        "EventTriggerType",
        "FeeAmount",
        "FeeAssessmentBase",
        "FeeAssessmentBaseAggregationMethod",
        "FeeCalculationMethod",
        "FeeCategory",
        "FeeChargingEndDate",
        "FeeChargingStartDate",
        "FeeCurrency",
        "FeeLowerLimitAmount",
        "FeePercentage",
        "FeeSubtype",
        "FeeTruncatedForStubPeriods",
        "FeeType",
        "FeeUpperLimitAmount",
        "FirstFeeBillingDate",
        "FirstRegularFeeBillingDate",
        "LastFeeBillingDate",
        "PreconditionExists",
        "ReccurringFeePeriodTimeUnit",
        "RecurringFeePeriodLength",
        "RoleOfPayer",
        "ScaleApplies",
        "WaiverCode",
        "WaiverNote",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD_ASSOC_TransferOrder.IDSystem" as "ASSOC_TransferOrder.IDSystem",
            "OLD_ASSOC_TransferOrder.TransferOrderID" as "ASSOC_TransferOrder.TransferOrderID",
            "OLD__LeaseService.ID" as "_LeaseService.ID",
            "OLD__LeaseService._LeaseServiceAgreement.FinancialContractID" as "_LeaseService._LeaseServiceAgreement.FinancialContractID",
            "OLD__LeaseService._LeaseServiceAgreement.IDSystem" as "_LeaseService._LeaseServiceAgreement.IDSystem",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "OLD__TrancheInSyndication.TrancheSequenceNumber" as "_TrancheInSyndication.TrancheSequenceNumber",
            "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" as "_TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_ConditionAcceptanceDate" as "ConditionAcceptanceDate",
            "OLD_ConditionFixedPeriodEndDate" as "ConditionFixedPeriodEndDate",
            "OLD_ConditionFixedPeriodStartDate" as "ConditionFixedPeriodStartDate",
            "OLD_DayOfMonthOfFeeBillingDate" as "DayOfMonthOfFeeBillingDate",
            "OLD_EventDescription" as "EventDescription",
            "OLD_EventTriggerType" as "EventTriggerType",
            "OLD_FeeAmount" as "FeeAmount",
            "OLD_FeeAssessmentBase" as "FeeAssessmentBase",
            "OLD_FeeAssessmentBaseAggregationMethod" as "FeeAssessmentBaseAggregationMethod",
            "OLD_FeeCalculationMethod" as "FeeCalculationMethod",
            "OLD_FeeCategory" as "FeeCategory",
            "OLD_FeeChargingEndDate" as "FeeChargingEndDate",
            "OLD_FeeChargingStartDate" as "FeeChargingStartDate",
            "OLD_FeeCurrency" as "FeeCurrency",
            "OLD_FeeLowerLimitAmount" as "FeeLowerLimitAmount",
            "OLD_FeePercentage" as "FeePercentage",
            "OLD_FeeSubtype" as "FeeSubtype",
            "OLD_FeeTruncatedForStubPeriods" as "FeeTruncatedForStubPeriods",
            "OLD_FeeType" as "FeeType",
            "OLD_FeeUpperLimitAmount" as "FeeUpperLimitAmount",
            "OLD_FirstFeeBillingDate" as "FirstFeeBillingDate",
            "OLD_FirstRegularFeeBillingDate" as "FirstRegularFeeBillingDate",
            "OLD_LastFeeBillingDate" as "LastFeeBillingDate",
            "OLD_PreconditionExists" as "PreconditionExists",
            "OLD_ReccurringFeePeriodTimeUnit" as "ReccurringFeePeriodTimeUnit",
            "OLD_RecurringFeePeriodLength" as "RecurringFeePeriodLength",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD_ScaleApplies" as "ScaleApplies",
            "OLD_WaiverCode" as "WaiverCode",
            "OLD_WaiverNote" as "WaiverNote",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_TransferOrder.IDSystem",
                        "OLD"."ASSOC_TransferOrder.TransferOrderID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_Trade.TradeID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.IDSystem" AS "OLD_ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.TransferOrderID" AS "OLD_ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."_LeaseService.ID" AS "OLD__LeaseService.ID" ,
                "OLD"."_LeaseService._LeaseServiceAgreement.FinancialContractID" AS "OLD__LeaseService._LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LeaseService._LeaseServiceAgreement.IDSystem" AS "OLD__LeaseService._LeaseServiceAgreement.IDSystem" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" AS "OLD__TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."ConditionAcceptanceDate" AS "OLD_ConditionAcceptanceDate" ,
                "OLD"."ConditionFixedPeriodEndDate" AS "OLD_ConditionFixedPeriodEndDate" ,
                "OLD"."ConditionFixedPeriodStartDate" AS "OLD_ConditionFixedPeriodStartDate" ,
                "OLD"."DayOfMonthOfFeeBillingDate" AS "OLD_DayOfMonthOfFeeBillingDate" ,
                "OLD"."EventDescription" AS "OLD_EventDescription" ,
                "OLD"."EventTriggerType" AS "OLD_EventTriggerType" ,
                "OLD"."FeeAmount" AS "OLD_FeeAmount" ,
                "OLD"."FeeAssessmentBase" AS "OLD_FeeAssessmentBase" ,
                "OLD"."FeeAssessmentBaseAggregationMethod" AS "OLD_FeeAssessmentBaseAggregationMethod" ,
                "OLD"."FeeCalculationMethod" AS "OLD_FeeCalculationMethod" ,
                "OLD"."FeeCategory" AS "OLD_FeeCategory" ,
                "OLD"."FeeChargingEndDate" AS "OLD_FeeChargingEndDate" ,
                "OLD"."FeeChargingStartDate" AS "OLD_FeeChargingStartDate" ,
                "OLD"."FeeCurrency" AS "OLD_FeeCurrency" ,
                "OLD"."FeeLowerLimitAmount" AS "OLD_FeeLowerLimitAmount" ,
                "OLD"."FeePercentage" AS "OLD_FeePercentage" ,
                "OLD"."FeeSubtype" AS "OLD_FeeSubtype" ,
                "OLD"."FeeTruncatedForStubPeriods" AS "OLD_FeeTruncatedForStubPeriods" ,
                "OLD"."FeeType" AS "OLD_FeeType" ,
                "OLD"."FeeUpperLimitAmount" AS "OLD_FeeUpperLimitAmount" ,
                "OLD"."FirstFeeBillingDate" AS "OLD_FirstFeeBillingDate" ,
                "OLD"."FirstRegularFeeBillingDate" AS "OLD_FirstRegularFeeBillingDate" ,
                "OLD"."LastFeeBillingDate" AS "OLD_LastFeeBillingDate" ,
                "OLD"."PreconditionExists" AS "OLD_PreconditionExists" ,
                "OLD"."ReccurringFeePeriodTimeUnit" AS "OLD_ReccurringFeePeriodTimeUnit" ,
                "OLD"."RecurringFeePeriodLength" AS "OLD_RecurringFeePeriodLength" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."ScaleApplies" AS "OLD_ScaleApplies" ,
                "OLD"."WaiverCode" AS "OLD_WaiverCode" ,
                "OLD"."WaiverNote" AS "OLD_WaiverNote" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Fee" as "OLD"
            on
                                                "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                                                "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" and
                                                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                                "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" and
                                                "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                                                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                                                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::Fee"
    where (
        "SequenceNumber",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "_LeaseService.ID",
        "_LeaseService._LeaseServiceAgreement.FinancialContractID",
        "_LeaseService._LeaseServiceAgreement.IDSystem",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "_TrancheInSyndication.TrancheSequenceNumber",
        "_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."ASSOC_TransferOrder.IDSystem",
            "OLD"."ASSOC_TransferOrder.TransferOrderID",
            "OLD"."_LeaseService.ID",
            "OLD"."_LeaseService._LeaseServiceAgreement.FinancialContractID",
            "OLD"."_LeaseService._LeaseServiceAgreement.IDSystem",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."_TrancheInSyndication.TrancheSequenceNumber",
            "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Fee" as "OLD"
        on
                                       "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                                       "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" and
                                       "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                       "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                       "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" and
                                       "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                                       "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" and
                                       "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
