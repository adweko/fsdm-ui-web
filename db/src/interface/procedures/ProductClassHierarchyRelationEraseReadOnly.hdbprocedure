PROCEDURE "sap.fsdm.procedures::ProductClassHierarchyRelationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ProductClassHierarchyRelationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ProductClassHierarchyRelationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ProductClassHierarchyRelationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ProductHierarchy" is null and
            "ASSOC_Child.ProductClass" is null and
            "ASSOC_Child.ProductClassificationSystem" is null and
            "ASSOC_Parent.ProductClass" is null and
            "ASSOC_Parent.ProductClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ProductHierarchy" ,
                "ASSOC_Child.ProductClass" ,
                "ASSOC_Child.ProductClassificationSystem" ,
                "ASSOC_Parent.ProductClass" ,
                "ASSOC_Parent.ProductClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ProductHierarchy" ,
                "OLD"."ASSOC_Child.ProductClass" ,
                "OLD"."ASSOC_Child.ProductClassificationSystem" ,
                "OLD"."ASSOC_Parent.ProductClass" ,
                "OLD"."ASSOC_Parent.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ProductClassHierarchyRelation" "OLD"
            on
                "IN"."ProductHierarchy" = "OLD"."ProductHierarchy" and
                "IN"."ASSOC_Child.ProductClass" = "OLD"."ASSOC_Child.ProductClass" and
                "IN"."ASSOC_Child.ProductClassificationSystem" = "OLD"."ASSOC_Child.ProductClassificationSystem" and
                "IN"."ASSOC_Parent.ProductClass" = "OLD"."ASSOC_Parent.ProductClass" and
                "IN"."ASSOC_Parent.ProductClassificationSystem" = "OLD"."ASSOC_Parent.ProductClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ProductHierarchy" ,
            "ASSOC_Child.ProductClass" ,
            "ASSOC_Child.ProductClassificationSystem" ,
            "ASSOC_Parent.ProductClass" ,
            "ASSOC_Parent.ProductClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ProductHierarchy" ,
                "OLD"."ASSOC_Child.ProductClass" ,
                "OLD"."ASSOC_Child.ProductClassificationSystem" ,
                "OLD"."ASSOC_Parent.ProductClass" ,
                "OLD"."ASSOC_Parent.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ProductClassHierarchyRelation_Historical" "OLD"
            on
                "IN"."ProductHierarchy" = "OLD"."ProductHierarchy" and
                "IN"."ASSOC_Child.ProductClass" = "OLD"."ASSOC_Child.ProductClass" and
                "IN"."ASSOC_Child.ProductClassificationSystem" = "OLD"."ASSOC_Child.ProductClassificationSystem" and
                "IN"."ASSOC_Parent.ProductClass" = "OLD"."ASSOC_Parent.ProductClass" and
                "IN"."ASSOC_Parent.ProductClassificationSystem" = "OLD"."ASSOC_Parent.ProductClassificationSystem" 
        );

END
