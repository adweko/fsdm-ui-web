sap.ui.define([
    "sap/ui/core/ComponentContainer"
], function (ComponentContainer) {
    "use strict";

    new ComponentContainer({
        name: "sap.ui.hana",
        settings : {
            id : "hana"
            
        },
        async: true
    }).placeAt("content");
});