PROCEDURE "sap.fsdm.procedures::OptionRightCurrencyAssignmentErase" (IN ROW "sap.fsdm.tabletypes::OptionRightCurrencyAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Currency.CurrencyCode" is null and
            "_OptionRight.SequenceNumber" is null and
            "_OptionRight._FinancialContract.FinancialContractID" is null and
            "_OptionRight._FinancialContract.IDSystem" is null and
            "_OptionRight._FinancialInstrument.FinancialInstrumentID" is null and
            "_OptionRight._TrancheInSyndication.TrancheSequenceNumber" is null and
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::OptionRightCurrencyAssignment"
        WHERE
        (            "_Currency.CurrencyCode" ,
            "_OptionRight.SequenceNumber" ,
            "_OptionRight._FinancialContract.FinancialContractID" ,
            "_OptionRight._FinancialContract.IDSystem" ,
            "_OptionRight._FinancialInstrument.FinancialInstrumentID" ,
            "_OptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
        ) in
        (
            select                 "OLD"."_Currency.CurrencyCode" ,
                "OLD"."_OptionRight.SequenceNumber" ,
                "OLD"."_OptionRight._FinancialContract.FinancialContractID" ,
                "OLD"."_OptionRight._FinancialContract.IDSystem" ,
                "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::OptionRightCurrencyAssignment" "OLD"
            on
            "IN"."_Currency.CurrencyCode" = "OLD"."_Currency.CurrencyCode" and
            "IN"."_OptionRight.SequenceNumber" = "OLD"."_OptionRight.SequenceNumber" and
            "IN"."_OptionRight._FinancialContract.FinancialContractID" = "OLD"."_OptionRight._FinancialContract.FinancialContractID" and
            "IN"."_OptionRight._FinancialContract.IDSystem" = "OLD"."_OptionRight._FinancialContract.IDSystem" and
            "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID" = "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" and
            "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" and
            "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
            "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::OptionRightCurrencyAssignment_Historical"
        WHERE
        (
            "_Currency.CurrencyCode" ,
            "_OptionRight.SequenceNumber" ,
            "_OptionRight._FinancialContract.FinancialContractID" ,
            "_OptionRight._FinancialContract.IDSystem" ,
            "_OptionRight._FinancialInstrument.FinancialInstrumentID" ,
            "_OptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
        ) in
        (
            select
                "OLD"."_Currency.CurrencyCode" ,
                "OLD"."_OptionRight.SequenceNumber" ,
                "OLD"."_OptionRight._FinancialContract.FinancialContractID" ,
                "OLD"."_OptionRight._FinancialContract.IDSystem" ,
                "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::OptionRightCurrencyAssignment_Historical" "OLD"
            on
                "IN"."_Currency.CurrencyCode" = "OLD"."_Currency.CurrencyCode" and
                "IN"."_OptionRight.SequenceNumber" = "OLD"."_OptionRight.SequenceNumber" and
                "IN"."_OptionRight._FinancialContract.FinancialContractID" = "OLD"."_OptionRight._FinancialContract.FinancialContractID" and
                "IN"."_OptionRight._FinancialContract.IDSystem" = "OLD"."_OptionRight._FinancialContract.IDSystem" and
                "IN"."_OptionRight._FinancialInstrument.FinancialInstrumentID" = "OLD"."_OptionRight._FinancialInstrument.FinancialInstrumentID" and
                "IN"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_OptionRight._TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_OptionRight._TrancheInSyndication._SyndicationAgreement.IDSystem" 
        );

END
