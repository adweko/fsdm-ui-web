PROCEDURE "sap.fsdm.procedures::FinancialContractContractBundleAssignmentReadOnly" (IN ROW "sap.fsdm.tabletypes::FinancialContractContractBundleAssignmentTT", OUT CURR_DEL "sap.fsdm.tabletypes::FinancialContractContractBundleAssignmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::FinancialContractContractBundleAssignmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_ContractBundle.ContractBundleID=' || TO_VARCHAR("_ContractBundle.ContractBundleID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_ContractBundle.ContractBundleID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_ContractBundle.ContractBundleID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_ContractBundle.ContractBundleID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_ContractBundle.ContractBundleID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_ContractBundle.ContractBundleID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "_ContractBundle.ContractBundleID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::FinancialContractContractBundleAssignment" WHERE
        (            "_ContractBundle.ContractBundleID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."_ContractBundle.ContractBundleID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::FinancialContractContractBundleAssignment" as "OLD"
            on
               ifnull( "IN"."_ContractBundle.ContractBundleID",'' ) = "OLD"."_ContractBundle.ContractBundleID" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "_ContractBundle.ContractBundleID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "_ContractBundle.ContractBundleID", '' ) as "_ContractBundle.ContractBundleID",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD__ContractBundle.ContractBundleID" as "_ContractBundle.ContractBundleID" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."_ContractBundle.ContractBundleID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."_ContractBundle.ContractBundleID" as "OLD__ContractBundle.ContractBundleID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::FinancialContractContractBundleAssignment" as "OLD"
            on
                ifnull( "IN"."_ContractBundle.ContractBundleID", '') = "OLD"."_ContractBundle.ContractBundleID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD__ContractBundle.ContractBundleID" as "_ContractBundle.ContractBundleID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."_ContractBundle.ContractBundleID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."_ContractBundle.ContractBundleID" as "OLD__ContractBundle.ContractBundleID",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::FinancialContractContractBundleAssignment" as "OLD"
            on
                ifnull("IN"."_ContractBundle.ContractBundleID", '') = "OLD"."_ContractBundle.ContractBundleID" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
