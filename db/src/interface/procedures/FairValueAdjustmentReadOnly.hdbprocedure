PROCEDURE "sap.fsdm.procedures::FairValueAdjustmentReadOnly" (IN ROW "sap.fsdm.tabletypes::FairValueAdjustmentTT", OUT CURR_DEL "sap.fsdm.tabletypes::FairValueAdjustmentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::FairValueAdjustmentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Type=' || TO_VARCHAR("Type") || ' ' ||
                '_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("_AccountingSystem.AccountingSystemID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Type",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Type",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Type",
                        "_AccountingSystem.AccountingSystemID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Type",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Type",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "Type",
        "_AccountingSystem.AccountingSystemID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::FairValueAdjustment" WHERE
        (            "Type" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."Type",
            "OLD"."_AccountingSystem.AccountingSystemID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::FairValueAdjustment" as "OLD"
            on
               ifnull( "IN"."Type",'' ) = "OLD"."Type" and
               ifnull( "IN"."_AccountingSystem.AccountingSystemID",'' ) = "OLD"."_AccountingSystem.AccountingSystemID" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_SecuritiesAccount.FinancialContractID",'' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
               ifnull( "IN"."_SecuritiesAccount.IDSystem",'' ) = "OLD"."_SecuritiesAccount.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "Type",
        "_AccountingSystem.AccountingSystemID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "FairValueAdjustmentInFunctionalCurrency",
        "FairValueAdjustmentInGroupCurrency",
        "FairValueAdjustmentInHardCurrency",
        "FairValueAdjustmentInIndexlCurrency",
        "FairValueAdjustmentInLocalCurrency",
        "FairValueAdjustmentInPositionCurrency",
        "FairValueIAdjustmentnPaymentCurrency",
        "FunctionalCurrency",
        "GroupCurrency",
        "HardCurrency",
        "IndexCurrency",
        "LocalCurrency",
        "PaymentCurrency",
        "PostionCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "Type", '' ) as "Type",
                    ifnull( "_AccountingSystem.AccountingSystemID", '' ) as "_AccountingSystem.AccountingSystemID",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
                    ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "FairValueAdjustmentInFunctionalCurrency"  ,
                    "FairValueAdjustmentInGroupCurrency"  ,
                    "FairValueAdjustmentInHardCurrency"  ,
                    "FairValueAdjustmentInIndexlCurrency"  ,
                    "FairValueAdjustmentInLocalCurrency"  ,
                    "FairValueAdjustmentInPositionCurrency"  ,
                    "FairValueIAdjustmentnPaymentCurrency"  ,
                    "FunctionalCurrency"  ,
                    "GroupCurrency"  ,
                    "HardCurrency"  ,
                    "IndexCurrency"  ,
                    "LocalCurrency"  ,
                    "PaymentCurrency"  ,
                    "PostionCurrency"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_Type" as "Type" ,
                    "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
                    "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_FairValueAdjustmentInFunctionalCurrency" as "FairValueAdjustmentInFunctionalCurrency" ,
                    "OLD_FairValueAdjustmentInGroupCurrency" as "FairValueAdjustmentInGroupCurrency" ,
                    "OLD_FairValueAdjustmentInHardCurrency" as "FairValueAdjustmentInHardCurrency" ,
                    "OLD_FairValueAdjustmentInIndexlCurrency" as "FairValueAdjustmentInIndexlCurrency" ,
                    "OLD_FairValueAdjustmentInLocalCurrency" as "FairValueAdjustmentInLocalCurrency" ,
                    "OLD_FairValueAdjustmentInPositionCurrency" as "FairValueAdjustmentInPositionCurrency" ,
                    "OLD_FairValueIAdjustmentnPaymentCurrency" as "FairValueIAdjustmentnPaymentCurrency" ,
                    "OLD_FunctionalCurrency" as "FunctionalCurrency" ,
                    "OLD_GroupCurrency" as "GroupCurrency" ,
                    "OLD_HardCurrency" as "HardCurrency" ,
                    "OLD_IndexCurrency" as "IndexCurrency" ,
                    "OLD_LocalCurrency" as "LocalCurrency" ,
                    "OLD_PaymentCurrency" as "PaymentCurrency" ,
                    "OLD_PostionCurrency" as "PostionCurrency" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."Type",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."Type" as "OLD_Type",
                                "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."FairValueAdjustmentInFunctionalCurrency" as "OLD_FairValueAdjustmentInFunctionalCurrency",
                                "OLD"."FairValueAdjustmentInGroupCurrency" as "OLD_FairValueAdjustmentInGroupCurrency",
                                "OLD"."FairValueAdjustmentInHardCurrency" as "OLD_FairValueAdjustmentInHardCurrency",
                                "OLD"."FairValueAdjustmentInIndexlCurrency" as "OLD_FairValueAdjustmentInIndexlCurrency",
                                "OLD"."FairValueAdjustmentInLocalCurrency" as "OLD_FairValueAdjustmentInLocalCurrency",
                                "OLD"."FairValueAdjustmentInPositionCurrency" as "OLD_FairValueAdjustmentInPositionCurrency",
                                "OLD"."FairValueIAdjustmentnPaymentCurrency" as "OLD_FairValueIAdjustmentnPaymentCurrency",
                                "OLD"."FunctionalCurrency" as "OLD_FunctionalCurrency",
                                "OLD"."GroupCurrency" as "OLD_GroupCurrency",
                                "OLD"."HardCurrency" as "OLD_HardCurrency",
                                "OLD"."IndexCurrency" as "OLD_IndexCurrency",
                                "OLD"."LocalCurrency" as "OLD_LocalCurrency",
                                "OLD"."PaymentCurrency" as "OLD_PaymentCurrency",
                                "OLD"."PostionCurrency" as "OLD_PostionCurrency",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::FairValueAdjustment" as "OLD"
            on
                ifnull( "IN"."Type", '') = "OLD"."Type" and
                ifnull( "IN"."_AccountingSystem.AccountingSystemID", '') = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_Type" as "Type",
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_FairValueAdjustmentInFunctionalCurrency" as "FairValueAdjustmentInFunctionalCurrency",
            "OLD_FairValueAdjustmentInGroupCurrency" as "FairValueAdjustmentInGroupCurrency",
            "OLD_FairValueAdjustmentInHardCurrency" as "FairValueAdjustmentInHardCurrency",
            "OLD_FairValueAdjustmentInIndexlCurrency" as "FairValueAdjustmentInIndexlCurrency",
            "OLD_FairValueAdjustmentInLocalCurrency" as "FairValueAdjustmentInLocalCurrency",
            "OLD_FairValueAdjustmentInPositionCurrency" as "FairValueAdjustmentInPositionCurrency",
            "OLD_FairValueIAdjustmentnPaymentCurrency" as "FairValueIAdjustmentnPaymentCurrency",
            "OLD_FunctionalCurrency" as "FunctionalCurrency",
            "OLD_GroupCurrency" as "GroupCurrency",
            "OLD_HardCurrency" as "HardCurrency",
            "OLD_IndexCurrency" as "IndexCurrency",
            "OLD_LocalCurrency" as "LocalCurrency",
            "OLD_PaymentCurrency" as "PaymentCurrency",
            "OLD_PostionCurrency" as "PostionCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."Type",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."Type" as "OLD_Type",
                        "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."FairValueAdjustmentInFunctionalCurrency" as "OLD_FairValueAdjustmentInFunctionalCurrency",
                        "OLD"."FairValueAdjustmentInGroupCurrency" as "OLD_FairValueAdjustmentInGroupCurrency",
                        "OLD"."FairValueAdjustmentInHardCurrency" as "OLD_FairValueAdjustmentInHardCurrency",
                        "OLD"."FairValueAdjustmentInIndexlCurrency" as "OLD_FairValueAdjustmentInIndexlCurrency",
                        "OLD"."FairValueAdjustmentInLocalCurrency" as "OLD_FairValueAdjustmentInLocalCurrency",
                        "OLD"."FairValueAdjustmentInPositionCurrency" as "OLD_FairValueAdjustmentInPositionCurrency",
                        "OLD"."FairValueIAdjustmentnPaymentCurrency" as "OLD_FairValueIAdjustmentnPaymentCurrency",
                        "OLD"."FunctionalCurrency" as "OLD_FunctionalCurrency",
                        "OLD"."GroupCurrency" as "OLD_GroupCurrency",
                        "OLD"."HardCurrency" as "OLD_HardCurrency",
                        "OLD"."IndexCurrency" as "OLD_IndexCurrency",
                        "OLD"."LocalCurrency" as "OLD_LocalCurrency",
                        "OLD"."PaymentCurrency" as "OLD_PaymentCurrency",
                        "OLD"."PostionCurrency" as "OLD_PostionCurrency",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::FairValueAdjustment" as "OLD"
            on
                ifnull("IN"."Type", '') = "OLD"."Type" and
                ifnull("IN"."_AccountingSystem.AccountingSystemID", '') = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull("IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
