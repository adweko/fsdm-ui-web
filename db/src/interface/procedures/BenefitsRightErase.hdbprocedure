PROCEDURE "sap.fsdm.procedures::BenefitsRightErase" (IN ROW "sap.fsdm.tabletypes::BenefitsRightTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceOrdinalNumber" is null and
            "Type" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_InsuranceCoverage.ID" is null and
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::BenefitsRight"
        WHERE
        (            "SequenceOrdinalNumber" ,
            "Type" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_InsuranceCoverage.ID" ,
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        ) in
        (
            select                 "OLD"."SequenceOrdinalNumber" ,
                "OLD"."Type" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_InsuranceCoverage.ID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            from :ROW "IN"
            inner join "sap.fsdm::BenefitsRight" "OLD"
            on
            "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
            "IN"."Type" = "OLD"."Type" and
            "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
            "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
            "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
            "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
            "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
            "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
            "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        delete from "sap.fsdm::BenefitsRight_Historical"
        WHERE
        (
            "SequenceOrdinalNumber" ,
            "Type" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_InsuranceCoverage.ID" ,
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        ) in
        (
            select
                "OLD"."SequenceOrdinalNumber" ,
                "OLD"."Type" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_InsuranceCoverage.ID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            from :ROW "IN"
            inner join "sap.fsdm::BenefitsRight_Historical" "OLD"
            on
                "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
                "IN"."Type" = "OLD"."Type" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
                "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
