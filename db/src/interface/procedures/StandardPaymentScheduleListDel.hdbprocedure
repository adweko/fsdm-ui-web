PROCEDURE "sap.fsdm.procedures::StandardPaymentScheduleListDel" (IN ROW "sap.fsdm.tabletypes::StandardPaymentScheduleTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_FinancialStandardProduct.FinancialStandardProductID=' || TO_VARCHAR("ASSOC_FinancialStandardProduct.FinancialStandardProductID") || ' ' ||
                'ASSOC_FinancialStandardProduct.IDSystem=' || TO_VARCHAR("ASSOC_FinancialStandardProduct.IDSystem") || ' ' ||
                'ASSOC_FinancialStandardProduct.PricingScheme=' || TO_VARCHAR("ASSOC_FinancialStandardProduct.PricingScheme") || ' ' ||
                'ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID=' || TO_VARCHAR("ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_FinancialStandardProduct.IDSystem",
                        "IN"."ASSOC_FinancialStandardProduct.PricingScheme",
                        "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "IN"."ASSOC_FinancialStandardProduct.IDSystem",
                        "IN"."ASSOC_FinancialStandardProduct.PricingScheme",
                        "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "ASSOC_FinancialStandardProduct.IDSystem",
                        "ASSOC_FinancialStandardProduct.PricingScheme",
                        "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_FinancialStandardProduct.IDSystem",
                                    "IN"."ASSOC_FinancialStandardProduct.PricingScheme",
                                    "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                                    "IN"."ASSOC_FinancialStandardProduct.IDSystem",
                                    "IN"."ASSOC_FinancialStandardProduct.PricingScheme",
                                    "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" is null and
            "ASSOC_FinancialStandardProduct.IDSystem" is null and
            "ASSOC_FinancialStandardProduct.FinancialStandardProductID" is null and
            "ASSOC_FinancialStandardProduct.PricingScheme" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::StandardPaymentSchedule" (
        "SequenceNumber",
        "ASSOC_FinancialStandardProduct.FinancialStandardProductID",
        "ASSOC_FinancialStandardProduct.IDSystem",
        "ASSOC_FinancialStandardProduct.PricingScheme",
        "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "DayOfMonthOfPayment",
        "InstallmentAmount",
        "InstallmentCurrency",
        "InstallmentLagPeriodLength",
        "InstallmentLagTimeUnit",
        "InstallmentPeriodLength",
        "InstallmentPeriodTimeUnit",
        "InstallmentsRelativeToInterestPaymentDates",
        "PartialPaymentRequestsPermitted",
        "PaymentToBeRequested",
        "PeriodUntilBulletPayment",
        "PeriodUntilBulletPaymentTimeUnit",
        "PeriodUntilFirstPayment",
        "PeriodUntilFirstRegularInstallment",
        "PeriodUntilLastPayment",
        "RoleOfPayer",
        "StandardPaymentScheduleCategory",
        "StandardPaymentScheduleType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_FinancialStandardProduct.FinancialStandardProductID" as "ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
            "OLD_ASSOC_FinancialStandardProduct.IDSystem" as "ASSOC_FinancialStandardProduct.IDSystem" ,
            "OLD_ASSOC_FinancialStandardProduct.PricingScheme" as "ASSOC_FinancialStandardProduct.PricingScheme" ,
            "OLD_ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_DayOfMonthOfPayment" as "DayOfMonthOfPayment" ,
            "OLD_InstallmentAmount" as "InstallmentAmount" ,
            "OLD_InstallmentCurrency" as "InstallmentCurrency" ,
            "OLD_InstallmentLagPeriodLength" as "InstallmentLagPeriodLength" ,
            "OLD_InstallmentLagTimeUnit" as "InstallmentLagTimeUnit" ,
            "OLD_InstallmentPeriodLength" as "InstallmentPeriodLength" ,
            "OLD_InstallmentPeriodTimeUnit" as "InstallmentPeriodTimeUnit" ,
            "OLD_InstallmentsRelativeToInterestPaymentDates" as "InstallmentsRelativeToInterestPaymentDates" ,
            "OLD_PartialPaymentRequestsPermitted" as "PartialPaymentRequestsPermitted" ,
            "OLD_PaymentToBeRequested" as "PaymentToBeRequested" ,
            "OLD_PeriodUntilBulletPayment" as "PeriodUntilBulletPayment" ,
            "OLD_PeriodUntilBulletPaymentTimeUnit" as "PeriodUntilBulletPaymentTimeUnit" ,
            "OLD_PeriodUntilFirstPayment" as "PeriodUntilFirstPayment" ,
            "OLD_PeriodUntilFirstRegularInstallment" as "PeriodUntilFirstRegularInstallment" ,
            "OLD_PeriodUntilLastPayment" as "PeriodUntilLastPayment" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD_StandardPaymentScheduleCategory" as "StandardPaymentScheduleCategory" ,
            "OLD_StandardPaymentScheduleType" as "StandardPaymentScheduleType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialStandardProduct.IDSystem",
                        "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_FinancialStandardProduct.PricingScheme",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_FinancialStandardProduct.IDSystem" AS "OLD_ASSOC_FinancialStandardProduct.IDSystem" ,
                "OLD"."ASSOC_FinancialStandardProduct.PricingScheme" AS "OLD_ASSOC_FinancialStandardProduct.PricingScheme" ,
                "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."DayOfMonthOfPayment" AS "OLD_DayOfMonthOfPayment" ,
                "OLD"."InstallmentAmount" AS "OLD_InstallmentAmount" ,
                "OLD"."InstallmentCurrency" AS "OLD_InstallmentCurrency" ,
                "OLD"."InstallmentLagPeriodLength" AS "OLD_InstallmentLagPeriodLength" ,
                "OLD"."InstallmentLagTimeUnit" AS "OLD_InstallmentLagTimeUnit" ,
                "OLD"."InstallmentPeriodLength" AS "OLD_InstallmentPeriodLength" ,
                "OLD"."InstallmentPeriodTimeUnit" AS "OLD_InstallmentPeriodTimeUnit" ,
                "OLD"."InstallmentsRelativeToInterestPaymentDates" AS "OLD_InstallmentsRelativeToInterestPaymentDates" ,
                "OLD"."PartialPaymentRequestsPermitted" AS "OLD_PartialPaymentRequestsPermitted" ,
                "OLD"."PaymentToBeRequested" AS "OLD_PaymentToBeRequested" ,
                "OLD"."PeriodUntilBulletPayment" AS "OLD_PeriodUntilBulletPayment" ,
                "OLD"."PeriodUntilBulletPaymentTimeUnit" AS "OLD_PeriodUntilBulletPaymentTimeUnit" ,
                "OLD"."PeriodUntilFirstPayment" AS "OLD_PeriodUntilFirstPayment" ,
                "OLD"."PeriodUntilFirstRegularInstallment" AS "OLD_PeriodUntilFirstRegularInstallment" ,
                "OLD"."PeriodUntilLastPayment" AS "OLD_PeriodUntilLastPayment" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."StandardPaymentScheduleCategory" AS "OLD_StandardPaymentScheduleCategory" ,
                "OLD"."StandardPaymentScheduleType" AS "OLD_StandardPaymentScheduleType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardPaymentSchedule" as "OLD"
            on
                      "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" and
                      "IN"."ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_FinancialStandardProduct.IDSystem" and
                      "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                      "IN"."ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_FinancialStandardProduct.PricingScheme" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::StandardPaymentSchedule" (
        "SequenceNumber",
        "ASSOC_FinancialStandardProduct.FinancialStandardProductID",
        "ASSOC_FinancialStandardProduct.IDSystem",
        "ASSOC_FinancialStandardProduct.PricingScheme",
        "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "DayOfMonthOfPayment",
        "InstallmentAmount",
        "InstallmentCurrency",
        "InstallmentLagPeriodLength",
        "InstallmentLagTimeUnit",
        "InstallmentPeriodLength",
        "InstallmentPeriodTimeUnit",
        "InstallmentsRelativeToInterestPaymentDates",
        "PartialPaymentRequestsPermitted",
        "PaymentToBeRequested",
        "PeriodUntilBulletPayment",
        "PeriodUntilBulletPaymentTimeUnit",
        "PeriodUntilFirstPayment",
        "PeriodUntilFirstRegularInstallment",
        "PeriodUntilLastPayment",
        "RoleOfPayer",
        "StandardPaymentScheduleCategory",
        "StandardPaymentScheduleType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_FinancialStandardProduct.FinancialStandardProductID" as "ASSOC_FinancialStandardProduct.FinancialStandardProductID",
            "OLD_ASSOC_FinancialStandardProduct.IDSystem" as "ASSOC_FinancialStandardProduct.IDSystem",
            "OLD_ASSOC_FinancialStandardProduct.PricingScheme" as "ASSOC_FinancialStandardProduct.PricingScheme",
            "OLD_ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" as "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_DayOfMonthOfPayment" as "DayOfMonthOfPayment",
            "OLD_InstallmentAmount" as "InstallmentAmount",
            "OLD_InstallmentCurrency" as "InstallmentCurrency",
            "OLD_InstallmentLagPeriodLength" as "InstallmentLagPeriodLength",
            "OLD_InstallmentLagTimeUnit" as "InstallmentLagTimeUnit",
            "OLD_InstallmentPeriodLength" as "InstallmentPeriodLength",
            "OLD_InstallmentPeriodTimeUnit" as "InstallmentPeriodTimeUnit",
            "OLD_InstallmentsRelativeToInterestPaymentDates" as "InstallmentsRelativeToInterestPaymentDates",
            "OLD_PartialPaymentRequestsPermitted" as "PartialPaymentRequestsPermitted",
            "OLD_PaymentToBeRequested" as "PaymentToBeRequested",
            "OLD_PeriodUntilBulletPayment" as "PeriodUntilBulletPayment",
            "OLD_PeriodUntilBulletPaymentTimeUnit" as "PeriodUntilBulletPaymentTimeUnit",
            "OLD_PeriodUntilFirstPayment" as "PeriodUntilFirstPayment",
            "OLD_PeriodUntilFirstRegularInstallment" as "PeriodUntilFirstRegularInstallment",
            "OLD_PeriodUntilLastPayment" as "PeriodUntilLastPayment",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD_StandardPaymentScheduleCategory" as "StandardPaymentScheduleCategory",
            "OLD_StandardPaymentScheduleType" as "StandardPaymentScheduleType",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialStandardProduct.IDSystem",
                        "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
                        "OLD"."ASSOC_FinancialStandardProduct.PricingScheme",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" AS "OLD_ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_FinancialStandardProduct.IDSystem" AS "OLD_ASSOC_FinancialStandardProduct.IDSystem" ,
                "OLD"."ASSOC_FinancialStandardProduct.PricingScheme" AS "OLD_ASSOC_FinancialStandardProduct.PricingScheme" ,
                "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" AS "OLD_ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."DayOfMonthOfPayment" AS "OLD_DayOfMonthOfPayment" ,
                "OLD"."InstallmentAmount" AS "OLD_InstallmentAmount" ,
                "OLD"."InstallmentCurrency" AS "OLD_InstallmentCurrency" ,
                "OLD"."InstallmentLagPeriodLength" AS "OLD_InstallmentLagPeriodLength" ,
                "OLD"."InstallmentLagTimeUnit" AS "OLD_InstallmentLagTimeUnit" ,
                "OLD"."InstallmentPeriodLength" AS "OLD_InstallmentPeriodLength" ,
                "OLD"."InstallmentPeriodTimeUnit" AS "OLD_InstallmentPeriodTimeUnit" ,
                "OLD"."InstallmentsRelativeToInterestPaymentDates" AS "OLD_InstallmentsRelativeToInterestPaymentDates" ,
                "OLD"."PartialPaymentRequestsPermitted" AS "OLD_PartialPaymentRequestsPermitted" ,
                "OLD"."PaymentToBeRequested" AS "OLD_PaymentToBeRequested" ,
                "OLD"."PeriodUntilBulletPayment" AS "OLD_PeriodUntilBulletPayment" ,
                "OLD"."PeriodUntilBulletPaymentTimeUnit" AS "OLD_PeriodUntilBulletPaymentTimeUnit" ,
                "OLD"."PeriodUntilFirstPayment" AS "OLD_PeriodUntilFirstPayment" ,
                "OLD"."PeriodUntilFirstRegularInstallment" AS "OLD_PeriodUntilFirstRegularInstallment" ,
                "OLD"."PeriodUntilLastPayment" AS "OLD_PeriodUntilLastPayment" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."StandardPaymentScheduleCategory" AS "OLD_StandardPaymentScheduleCategory" ,
                "OLD"."StandardPaymentScheduleType" AS "OLD_StandardPaymentScheduleType" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::StandardPaymentSchedule" as "OLD"
            on
                                                "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" and
                                                "IN"."ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_FinancialStandardProduct.IDSystem" and
                                                "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                                                "IN"."ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_FinancialStandardProduct.PricingScheme" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::StandardPaymentSchedule"
    where (
        "SequenceNumber",
        "ASSOC_FinancialStandardProduct.FinancialStandardProductID",
        "ASSOC_FinancialStandardProduct.IDSystem",
        "ASSOC_FinancialStandardProduct.PricingScheme",
        "ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID",
            "OLD"."ASSOC_FinancialStandardProduct.IDSystem",
            "OLD"."ASSOC_FinancialStandardProduct.PricingScheme",
            "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::StandardPaymentSchedule" as "OLD"
        on
                                       "IN"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" and
                                       "IN"."ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_FinancialStandardProduct.IDSystem" and
                                       "IN"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                                       "IN"."ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_FinancialStandardProduct.PricingScheme" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
