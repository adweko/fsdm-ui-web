PROCEDURE "sap.fsdm.procedures::ConversionRightDelete" (IN ROW "sap.fsdm.tabletypes::ConversionRightTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                '_DebtInstrument.FinancialInstrumentID=' || TO_VARCHAR("_DebtInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_DebtInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_DebtInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "_DebtInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_DebtInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_DebtInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "_DebtInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::ConversionRight" (
        "SequenceNumber",
        "_DebtInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_ConversionTarget.FinancialInstrumentID",
        "BearerCashSettlementOption",
        "ConversionPeriodEndDate",
        "ConversionPeriodStartDate",
        "ConversionPriceFormula",
        "ConversionRightHolder",
        "ConversionTargetIsPreferredOrCommonStock",
        "ConversionTargetIssuer",
        "ConversionTargetParValueCurrency",
        "ConversionTargetParValuePerUnit",
        "FlooredConversionPrice",
        "FlooredConversionPriceCurrency",
        "IssuerCashSettlementOption",
        "PrespecifiedConversionPrice",
        "PrespecifiedConversionRatio",
        "ReferenceSharePrice",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD__DebtInstrument.FinancialInstrumentID" as "_DebtInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__ConversionTarget.FinancialInstrumentID" as "_ConversionTarget.FinancialInstrumentID" ,
            "OLD_BearerCashSettlementOption" as "BearerCashSettlementOption" ,
            "OLD_ConversionPeriodEndDate" as "ConversionPeriodEndDate" ,
            "OLD_ConversionPeriodStartDate" as "ConversionPeriodStartDate" ,
            "OLD_ConversionPriceFormula" as "ConversionPriceFormula" ,
            "OLD_ConversionRightHolder" as "ConversionRightHolder" ,
            "OLD_ConversionTargetIsPreferredOrCommonStock" as "ConversionTargetIsPreferredOrCommonStock" ,
            "OLD_ConversionTargetIssuer" as "ConversionTargetIssuer" ,
            "OLD_ConversionTargetParValueCurrency" as "ConversionTargetParValueCurrency" ,
            "OLD_ConversionTargetParValuePerUnit" as "ConversionTargetParValuePerUnit" ,
            "OLD_FlooredConversionPrice" as "FlooredConversionPrice" ,
            "OLD_FlooredConversionPriceCurrency" as "FlooredConversionPriceCurrency" ,
            "OLD_IssuerCashSettlementOption" as "IssuerCashSettlementOption" ,
            "OLD_PrespecifiedConversionPrice" as "PrespecifiedConversionPrice" ,
            "OLD_PrespecifiedConversionRatio" as "PrespecifiedConversionRatio" ,
            "OLD_ReferenceSharePrice" as "ReferenceSharePrice" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."_DebtInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."_DebtInstrument.FinancialInstrumentID" AS "OLD__DebtInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ConversionTarget.FinancialInstrumentID" AS "OLD__ConversionTarget.FinancialInstrumentID" ,
                "OLD"."BearerCashSettlementOption" AS "OLD_BearerCashSettlementOption" ,
                "OLD"."ConversionPeriodEndDate" AS "OLD_ConversionPeriodEndDate" ,
                "OLD"."ConversionPeriodStartDate" AS "OLD_ConversionPeriodStartDate" ,
                "OLD"."ConversionPriceFormula" AS "OLD_ConversionPriceFormula" ,
                "OLD"."ConversionRightHolder" AS "OLD_ConversionRightHolder" ,
                "OLD"."ConversionTargetIsPreferredOrCommonStock" AS "OLD_ConversionTargetIsPreferredOrCommonStock" ,
                "OLD"."ConversionTargetIssuer" AS "OLD_ConversionTargetIssuer" ,
                "OLD"."ConversionTargetParValueCurrency" AS "OLD_ConversionTargetParValueCurrency" ,
                "OLD"."ConversionTargetParValuePerUnit" AS "OLD_ConversionTargetParValuePerUnit" ,
                "OLD"."FlooredConversionPrice" AS "OLD_FlooredConversionPrice" ,
                "OLD"."FlooredConversionPriceCurrency" AS "OLD_FlooredConversionPriceCurrency" ,
                "OLD"."IssuerCashSettlementOption" AS "OLD_IssuerCashSettlementOption" ,
                "OLD"."PrespecifiedConversionPrice" AS "OLD_PrespecifiedConversionPrice" ,
                "OLD"."PrespecifiedConversionRatio" AS "OLD_PrespecifiedConversionRatio" ,
                "OLD"."ReferenceSharePrice" AS "OLD_ReferenceSharePrice" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ConversionRight" as "OLD"
            on
                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                      "IN"."_DebtInstrument.FinancialInstrumentID" = "OLD"."_DebtInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::ConversionRight" (
        "SequenceNumber",
        "_DebtInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_ConversionTarget.FinancialInstrumentID",
        "BearerCashSettlementOption",
        "ConversionPeriodEndDate",
        "ConversionPeriodStartDate",
        "ConversionPriceFormula",
        "ConversionRightHolder",
        "ConversionTargetIsPreferredOrCommonStock",
        "ConversionTargetIssuer",
        "ConversionTargetParValueCurrency",
        "ConversionTargetParValuePerUnit",
        "FlooredConversionPrice",
        "FlooredConversionPriceCurrency",
        "IssuerCashSettlementOption",
        "PrespecifiedConversionPrice",
        "PrespecifiedConversionRatio",
        "ReferenceSharePrice",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD__DebtInstrument.FinancialInstrumentID" as "_DebtInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__ConversionTarget.FinancialInstrumentID" as "_ConversionTarget.FinancialInstrumentID",
            "OLD_BearerCashSettlementOption" as "BearerCashSettlementOption",
            "OLD_ConversionPeriodEndDate" as "ConversionPeriodEndDate",
            "OLD_ConversionPeriodStartDate" as "ConversionPeriodStartDate",
            "OLD_ConversionPriceFormula" as "ConversionPriceFormula",
            "OLD_ConversionRightHolder" as "ConversionRightHolder",
            "OLD_ConversionTargetIsPreferredOrCommonStock" as "ConversionTargetIsPreferredOrCommonStock",
            "OLD_ConversionTargetIssuer" as "ConversionTargetIssuer",
            "OLD_ConversionTargetParValueCurrency" as "ConversionTargetParValueCurrency",
            "OLD_ConversionTargetParValuePerUnit" as "ConversionTargetParValuePerUnit",
            "OLD_FlooredConversionPrice" as "FlooredConversionPrice",
            "OLD_FlooredConversionPriceCurrency" as "FlooredConversionPriceCurrency",
            "OLD_IssuerCashSettlementOption" as "IssuerCashSettlementOption",
            "OLD_PrespecifiedConversionPrice" as "PrespecifiedConversionPrice",
            "OLD_PrespecifiedConversionRatio" as "PrespecifiedConversionRatio",
            "OLD_ReferenceSharePrice" as "ReferenceSharePrice",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."_DebtInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."_DebtInstrument.FinancialInstrumentID" AS "OLD__DebtInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_ConversionTarget.FinancialInstrumentID" AS "OLD__ConversionTarget.FinancialInstrumentID" ,
                "OLD"."BearerCashSettlementOption" AS "OLD_BearerCashSettlementOption" ,
                "OLD"."ConversionPeriodEndDate" AS "OLD_ConversionPeriodEndDate" ,
                "OLD"."ConversionPeriodStartDate" AS "OLD_ConversionPeriodStartDate" ,
                "OLD"."ConversionPriceFormula" AS "OLD_ConversionPriceFormula" ,
                "OLD"."ConversionRightHolder" AS "OLD_ConversionRightHolder" ,
                "OLD"."ConversionTargetIsPreferredOrCommonStock" AS "OLD_ConversionTargetIsPreferredOrCommonStock" ,
                "OLD"."ConversionTargetIssuer" AS "OLD_ConversionTargetIssuer" ,
                "OLD"."ConversionTargetParValueCurrency" AS "OLD_ConversionTargetParValueCurrency" ,
                "OLD"."ConversionTargetParValuePerUnit" AS "OLD_ConversionTargetParValuePerUnit" ,
                "OLD"."FlooredConversionPrice" AS "OLD_FlooredConversionPrice" ,
                "OLD"."FlooredConversionPriceCurrency" AS "OLD_FlooredConversionPriceCurrency" ,
                "OLD"."IssuerCashSettlementOption" AS "OLD_IssuerCashSettlementOption" ,
                "OLD"."PrespecifiedConversionPrice" AS "OLD_PrespecifiedConversionPrice" ,
                "OLD"."PrespecifiedConversionRatio" AS "OLD_PrespecifiedConversionRatio" ,
                "OLD"."ReferenceSharePrice" AS "OLD_ReferenceSharePrice" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ConversionRight" as "OLD"
            on
                                                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                                "IN"."_DebtInstrument.FinancialInstrumentID" = "OLD"."_DebtInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::ConversionRight"
    where (
        "SequenceNumber",
        "_DebtInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."SequenceNumber",
            "OLD"."_DebtInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ConversionRight" as "OLD"
        on
                                       "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                       "IN"."_DebtInstrument.FinancialInstrumentID" = "OLD"."_DebtInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
