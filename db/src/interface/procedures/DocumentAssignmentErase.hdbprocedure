PROCEDURE "sap.fsdm.procedures::DocumentAssignmentErase" (IN ROW "sap.fsdm.tabletypes::DocumentAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_BusinessPartner.BusinessPartnerID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_CommunicationEvent.CommunicationEventID" is null and
            "_CommunicationEvent.IDSystem" is null and
            "_Document.DocumentID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" is null and
            "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" is null and
            "_TaxExemptStatusDocument.Category" is null and
            "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::DocumentAssignment"
        WHERE
        (            "_BusinessPartner.BusinessPartnerID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_CommunicationEvent.CommunicationEventID" ,
            "_CommunicationEvent.IDSystem" ,
            "_Document.DocumentID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
            "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
            "_TaxExemptStatusDocument.Category" ,
            "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" 
        ) in
        (
            select                 "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_CommunicationEvent.CommunicationEventID" ,
                "OLD"."_CommunicationEvent.IDSystem" ,
                "OLD"."_Document.DocumentID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
                "OLD"."_TaxExemptStatusDocument.Category" ,
                "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" 
            from :ROW "IN"
            inner join "sap.fsdm::DocumentAssignment" "OLD"
            on
            "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
            "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
            "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
            "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
            "IN"."_CommunicationEvent.CommunicationEventID" = "OLD"."_CommunicationEvent.CommunicationEventID" and
            "IN"."_CommunicationEvent.IDSystem" = "OLD"."_CommunicationEvent.IDSystem" and
            "IN"."_Document.DocumentID" = "OLD"."_Document.DocumentID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" and
            "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" and
            "IN"."_TaxExemptStatusDocument.Category" = "OLD"."_TaxExemptStatusDocument.Category" and
            "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" and
            "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
            "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        );

        --delete data from history table
        delete from "sap.fsdm::DocumentAssignment_Historical"
        WHERE
        (
            "_BusinessPartner.BusinessPartnerID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_CommunicationEvent.CommunicationEventID" ,
            "_CommunicationEvent.IDSystem" ,
            "_Document.DocumentID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
            "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
            "_TaxExemptStatusDocument.Category" ,
            "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" 
        ) in
        (
            select
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_CommunicationEvent.CommunicationEventID" ,
                "OLD"."_CommunicationEvent.IDSystem" ,
                "OLD"."_Document.DocumentID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
                "OLD"."_TaxExemptStatusDocument.Category" ,
                "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" 
            from :ROW "IN"
            inner join "sap.fsdm::DocumentAssignment_Historical" "OLD"
            on
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_CommunicationEvent.CommunicationEventID" = "OLD"."_CommunicationEvent.CommunicationEventID" and
                "IN"."_CommunicationEvent.IDSystem" = "OLD"."_CommunicationEvent.IDSystem" and
                "IN"."_Document.DocumentID" = "OLD"."_Document.DocumentID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" and
                "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" and
                "IN"."_TaxExemptStatusDocument.Category" = "OLD"."_TaxExemptStatusDocument.Category" and
                "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" and
                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        );

END
