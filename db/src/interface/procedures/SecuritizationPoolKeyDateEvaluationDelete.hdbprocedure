PROCEDURE "sap.fsdm.procedures::SecuritizationPoolKeyDateEvaluationDelete" (IN ROW "sap.fsdm.tabletypes::SecuritizationPoolKeyDateEvaluationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::SecuritizationPoolKeyDateEvaluation" (
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BookValue",
        "BookValueCurrency",
        "DefaultedExposureAmount",
        "DefaultedExposureAmountCurrency",
        "EffectiveNumberOfPoolExposure",
        "ExposurAtDefaultWeightedExpectedLoss",
        "ExposureAtDefaultWeightedLGD",
        "ExposureAtDefaultWeightedUnexpectedLoss",
        "InternalRatingBasedApproachCapitalRatio",
        "InternalRatingBasedApproachCapitalRequirements",
        "InternalRatingBasedApproachCapitalRequirementsCurrency",
        "MaximumMaturityOfPoolExposurePeriodLength",
        "MaximumMaturityOfPoolExposureTimeUnit",
        "NominalAmount",
        "NominalAmountCurrency",
        "NumberOfExposure",
        "OvercollateralizationPercentage",
        "PoolFactor",
        "RatioOfExposuresInDefault",
        "RatioOfExposuresWithDelinquencyStatusUnknown",
        "RatioOfInternalRatingBasedApproachExposure",
        "RatioOfRetailExposure",
        "RegulatoryDefaultedExposureAmount",
        "RegulatoryDefaultedExposureAmountCurrency",
        "StandardizedApproachCapitalRatio",
        "StandardizedApproachCapitalRequirements",
        "StandardizedApproachCapitalRequirementsCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BookValue" as "BookValue" ,
            "OLD_BookValueCurrency" as "BookValueCurrency" ,
            "OLD_DefaultedExposureAmount" as "DefaultedExposureAmount" ,
            "OLD_DefaultedExposureAmountCurrency" as "DefaultedExposureAmountCurrency" ,
            "OLD_EffectiveNumberOfPoolExposure" as "EffectiveNumberOfPoolExposure" ,
            "OLD_ExposurAtDefaultWeightedExpectedLoss" as "ExposurAtDefaultWeightedExpectedLoss" ,
            "OLD_ExposureAtDefaultWeightedLGD" as "ExposureAtDefaultWeightedLGD" ,
            "OLD_ExposureAtDefaultWeightedUnexpectedLoss" as "ExposureAtDefaultWeightedUnexpectedLoss" ,
            "OLD_InternalRatingBasedApproachCapitalRatio" as "InternalRatingBasedApproachCapitalRatio" ,
            "OLD_InternalRatingBasedApproachCapitalRequirements" as "InternalRatingBasedApproachCapitalRequirements" ,
            "OLD_InternalRatingBasedApproachCapitalRequirementsCurrency" as "InternalRatingBasedApproachCapitalRequirementsCurrency" ,
            "OLD_MaximumMaturityOfPoolExposurePeriodLength" as "MaximumMaturityOfPoolExposurePeriodLength" ,
            "OLD_MaximumMaturityOfPoolExposureTimeUnit" as "MaximumMaturityOfPoolExposureTimeUnit" ,
            "OLD_NominalAmount" as "NominalAmount" ,
            "OLD_NominalAmountCurrency" as "NominalAmountCurrency" ,
            "OLD_NumberOfExposure" as "NumberOfExposure" ,
            "OLD_OvercollateralizationPercentage" as "OvercollateralizationPercentage" ,
            "OLD_PoolFactor" as "PoolFactor" ,
            "OLD_RatioOfExposuresInDefault" as "RatioOfExposuresInDefault" ,
            "OLD_RatioOfExposuresWithDelinquencyStatusUnknown" as "RatioOfExposuresWithDelinquencyStatusUnknown" ,
            "OLD_RatioOfInternalRatingBasedApproachExposure" as "RatioOfInternalRatingBasedApproachExposure" ,
            "OLD_RatioOfRetailExposure" as "RatioOfRetailExposure" ,
            "OLD_RegulatoryDefaultedExposureAmount" as "RegulatoryDefaultedExposureAmount" ,
            "OLD_RegulatoryDefaultedExposureAmountCurrency" as "RegulatoryDefaultedExposureAmountCurrency" ,
            "OLD_StandardizedApproachCapitalRatio" as "StandardizedApproachCapitalRatio" ,
            "OLD_StandardizedApproachCapitalRequirements" as "StandardizedApproachCapitalRequirements" ,
            "OLD_StandardizedApproachCapitalRequirementsCurrency" as "StandardizedApproachCapitalRequirementsCurrency" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BookValue" AS "OLD_BookValue" ,
                "OLD"."BookValueCurrency" AS "OLD_BookValueCurrency" ,
                "OLD"."DefaultedExposureAmount" AS "OLD_DefaultedExposureAmount" ,
                "OLD"."DefaultedExposureAmountCurrency" AS "OLD_DefaultedExposureAmountCurrency" ,
                "OLD"."EffectiveNumberOfPoolExposure" AS "OLD_EffectiveNumberOfPoolExposure" ,
                "OLD"."ExposurAtDefaultWeightedExpectedLoss" AS "OLD_ExposurAtDefaultWeightedExpectedLoss" ,
                "OLD"."ExposureAtDefaultWeightedLGD" AS "OLD_ExposureAtDefaultWeightedLGD" ,
                "OLD"."ExposureAtDefaultWeightedUnexpectedLoss" AS "OLD_ExposureAtDefaultWeightedUnexpectedLoss" ,
                "OLD"."InternalRatingBasedApproachCapitalRatio" AS "OLD_InternalRatingBasedApproachCapitalRatio" ,
                "OLD"."InternalRatingBasedApproachCapitalRequirements" AS "OLD_InternalRatingBasedApproachCapitalRequirements" ,
                "OLD"."InternalRatingBasedApproachCapitalRequirementsCurrency" AS "OLD_InternalRatingBasedApproachCapitalRequirementsCurrency" ,
                "OLD"."MaximumMaturityOfPoolExposurePeriodLength" AS "OLD_MaximumMaturityOfPoolExposurePeriodLength" ,
                "OLD"."MaximumMaturityOfPoolExposureTimeUnit" AS "OLD_MaximumMaturityOfPoolExposureTimeUnit" ,
                "OLD"."NominalAmount" AS "OLD_NominalAmount" ,
                "OLD"."NominalAmountCurrency" AS "OLD_NominalAmountCurrency" ,
                "OLD"."NumberOfExposure" AS "OLD_NumberOfExposure" ,
                "OLD"."OvercollateralizationPercentage" AS "OLD_OvercollateralizationPercentage" ,
                "OLD"."PoolFactor" AS "OLD_PoolFactor" ,
                "OLD"."RatioOfExposuresInDefault" AS "OLD_RatioOfExposuresInDefault" ,
                "OLD"."RatioOfExposuresWithDelinquencyStatusUnknown" AS "OLD_RatioOfExposuresWithDelinquencyStatusUnknown" ,
                "OLD"."RatioOfInternalRatingBasedApproachExposure" AS "OLD_RatioOfInternalRatingBasedApproachExposure" ,
                "OLD"."RatioOfRetailExposure" AS "OLD_RatioOfRetailExposure" ,
                "OLD"."RegulatoryDefaultedExposureAmount" AS "OLD_RegulatoryDefaultedExposureAmount" ,
                "OLD"."RegulatoryDefaultedExposureAmountCurrency" AS "OLD_RegulatoryDefaultedExposureAmountCurrency" ,
                "OLD"."StandardizedApproachCapitalRatio" AS "OLD_StandardizedApproachCapitalRatio" ,
                "OLD"."StandardizedApproachCapitalRequirements" AS "OLD_StandardizedApproachCapitalRequirements" ,
                "OLD"."StandardizedApproachCapitalRequirementsCurrency" AS "OLD_StandardizedApproachCapitalRequirementsCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SecuritizationPoolKeyDateEvaluation" as "OLD"
            on
                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                      "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                      "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::SecuritizationPoolKeyDateEvaluation" (
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BookValue",
        "BookValueCurrency",
        "DefaultedExposureAmount",
        "DefaultedExposureAmountCurrency",
        "EffectiveNumberOfPoolExposure",
        "ExposurAtDefaultWeightedExpectedLoss",
        "ExposureAtDefaultWeightedLGD",
        "ExposureAtDefaultWeightedUnexpectedLoss",
        "InternalRatingBasedApproachCapitalRatio",
        "InternalRatingBasedApproachCapitalRequirements",
        "InternalRatingBasedApproachCapitalRequirementsCurrency",
        "MaximumMaturityOfPoolExposurePeriodLength",
        "MaximumMaturityOfPoolExposureTimeUnit",
        "NominalAmount",
        "NominalAmountCurrency",
        "NumberOfExposure",
        "OvercollateralizationPercentage",
        "PoolFactor",
        "RatioOfExposuresInDefault",
        "RatioOfExposuresWithDelinquencyStatusUnknown",
        "RatioOfInternalRatingBasedApproachExposure",
        "RatioOfRetailExposure",
        "RegulatoryDefaultedExposureAmount",
        "RegulatoryDefaultedExposureAmountCurrency",
        "StandardizedApproachCapitalRatio",
        "StandardizedApproachCapitalRequirements",
        "StandardizedApproachCapitalRequirementsCurrency",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BookValue" as "BookValue",
            "OLD_BookValueCurrency" as "BookValueCurrency",
            "OLD_DefaultedExposureAmount" as "DefaultedExposureAmount",
            "OLD_DefaultedExposureAmountCurrency" as "DefaultedExposureAmountCurrency",
            "OLD_EffectiveNumberOfPoolExposure" as "EffectiveNumberOfPoolExposure",
            "OLD_ExposurAtDefaultWeightedExpectedLoss" as "ExposurAtDefaultWeightedExpectedLoss",
            "OLD_ExposureAtDefaultWeightedLGD" as "ExposureAtDefaultWeightedLGD",
            "OLD_ExposureAtDefaultWeightedUnexpectedLoss" as "ExposureAtDefaultWeightedUnexpectedLoss",
            "OLD_InternalRatingBasedApproachCapitalRatio" as "InternalRatingBasedApproachCapitalRatio",
            "OLD_InternalRatingBasedApproachCapitalRequirements" as "InternalRatingBasedApproachCapitalRequirements",
            "OLD_InternalRatingBasedApproachCapitalRequirementsCurrency" as "InternalRatingBasedApproachCapitalRequirementsCurrency",
            "OLD_MaximumMaturityOfPoolExposurePeriodLength" as "MaximumMaturityOfPoolExposurePeriodLength",
            "OLD_MaximumMaturityOfPoolExposureTimeUnit" as "MaximumMaturityOfPoolExposureTimeUnit",
            "OLD_NominalAmount" as "NominalAmount",
            "OLD_NominalAmountCurrency" as "NominalAmountCurrency",
            "OLD_NumberOfExposure" as "NumberOfExposure",
            "OLD_OvercollateralizationPercentage" as "OvercollateralizationPercentage",
            "OLD_PoolFactor" as "PoolFactor",
            "OLD_RatioOfExposuresInDefault" as "RatioOfExposuresInDefault",
            "OLD_RatioOfExposuresWithDelinquencyStatusUnknown" as "RatioOfExposuresWithDelinquencyStatusUnknown",
            "OLD_RatioOfInternalRatingBasedApproachExposure" as "RatioOfInternalRatingBasedApproachExposure",
            "OLD_RatioOfRetailExposure" as "RatioOfRetailExposure",
            "OLD_RegulatoryDefaultedExposureAmount" as "RegulatoryDefaultedExposureAmount",
            "OLD_RegulatoryDefaultedExposureAmountCurrency" as "RegulatoryDefaultedExposureAmountCurrency",
            "OLD_StandardizedApproachCapitalRatio" as "StandardizedApproachCapitalRatio",
            "OLD_StandardizedApproachCapitalRequirements" as "StandardizedApproachCapitalRequirements",
            "OLD_StandardizedApproachCapitalRequirementsCurrency" as "StandardizedApproachCapitalRequirementsCurrency",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_ResultGroup.ResultDataProvider" AS "OLD__ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" AS "OLD__ResultGroup.ResultGroupID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BookValue" AS "OLD_BookValue" ,
                "OLD"."BookValueCurrency" AS "OLD_BookValueCurrency" ,
                "OLD"."DefaultedExposureAmount" AS "OLD_DefaultedExposureAmount" ,
                "OLD"."DefaultedExposureAmountCurrency" AS "OLD_DefaultedExposureAmountCurrency" ,
                "OLD"."EffectiveNumberOfPoolExposure" AS "OLD_EffectiveNumberOfPoolExposure" ,
                "OLD"."ExposurAtDefaultWeightedExpectedLoss" AS "OLD_ExposurAtDefaultWeightedExpectedLoss" ,
                "OLD"."ExposureAtDefaultWeightedLGD" AS "OLD_ExposureAtDefaultWeightedLGD" ,
                "OLD"."ExposureAtDefaultWeightedUnexpectedLoss" AS "OLD_ExposureAtDefaultWeightedUnexpectedLoss" ,
                "OLD"."InternalRatingBasedApproachCapitalRatio" AS "OLD_InternalRatingBasedApproachCapitalRatio" ,
                "OLD"."InternalRatingBasedApproachCapitalRequirements" AS "OLD_InternalRatingBasedApproachCapitalRequirements" ,
                "OLD"."InternalRatingBasedApproachCapitalRequirementsCurrency" AS "OLD_InternalRatingBasedApproachCapitalRequirementsCurrency" ,
                "OLD"."MaximumMaturityOfPoolExposurePeriodLength" AS "OLD_MaximumMaturityOfPoolExposurePeriodLength" ,
                "OLD"."MaximumMaturityOfPoolExposureTimeUnit" AS "OLD_MaximumMaturityOfPoolExposureTimeUnit" ,
                "OLD"."NominalAmount" AS "OLD_NominalAmount" ,
                "OLD"."NominalAmountCurrency" AS "OLD_NominalAmountCurrency" ,
                "OLD"."NumberOfExposure" AS "OLD_NumberOfExposure" ,
                "OLD"."OvercollateralizationPercentage" AS "OLD_OvercollateralizationPercentage" ,
                "OLD"."PoolFactor" AS "OLD_PoolFactor" ,
                "OLD"."RatioOfExposuresInDefault" AS "OLD_RatioOfExposuresInDefault" ,
                "OLD"."RatioOfExposuresWithDelinquencyStatusUnknown" AS "OLD_RatioOfExposuresWithDelinquencyStatusUnknown" ,
                "OLD"."RatioOfInternalRatingBasedApproachExposure" AS "OLD_RatioOfInternalRatingBasedApproachExposure" ,
                "OLD"."RatioOfRetailExposure" AS "OLD_RatioOfRetailExposure" ,
                "OLD"."RegulatoryDefaultedExposureAmount" AS "OLD_RegulatoryDefaultedExposureAmount" ,
                "OLD"."RegulatoryDefaultedExposureAmountCurrency" AS "OLD_RegulatoryDefaultedExposureAmountCurrency" ,
                "OLD"."StandardizedApproachCapitalRatio" AS "OLD_StandardizedApproachCapitalRatio" ,
                "OLD"."StandardizedApproachCapitalRequirements" AS "OLD_StandardizedApproachCapitalRequirements" ,
                "OLD"."StandardizedApproachCapitalRequirementsCurrency" AS "OLD_StandardizedApproachCapitalRequirementsCurrency" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SecuritizationPoolKeyDateEvaluation" as "OLD"
            on
                                                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::SecuritizationPoolKeyDateEvaluation"
    where (
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SecuritizationPoolKeyDateEvaluation" as "OLD"
        on
                                       "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                       "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                       "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                                       "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
