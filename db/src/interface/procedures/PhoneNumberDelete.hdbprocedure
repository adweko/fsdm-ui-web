PROCEDURE "sap.fsdm.procedures::PhoneNumberDelete" (IN ROW "sap.fsdm.tabletypes::PhoneNumberTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PhoneNumberType=' || TO_VARCHAR("PhoneNumberType") || ' ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                'ASSOC_OrganizationalUnit.IDSystem=' || TO_VARCHAR("ASSOC_OrganizationalUnit.IDSystem") || ' ' ||
                'ASSOC_OrganizationalUnit.OrganizationalUnitID=' || TO_VARCHAR("ASSOC_OrganizationalUnit.OrganizationalUnitID") || ' ' ||
                'ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                'ASSOC_Partner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_Partner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PhoneNumberType",
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_OrganizationalUnit.IDSystem",
                        "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_Partner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PhoneNumberType",
                        "IN"."SequenceNumber",
                        "IN"."ASSOC_OrganizationalUnit.IDSystem",
                        "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_Partner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PhoneNumberType",
                        "SequenceNumber",
                        "ASSOC_OrganizationalUnit.IDSystem",
                        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "ASSOC_Partner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PhoneNumberType",
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_OrganizationalUnit.IDSystem",
                                    "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_Partner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PhoneNumberType",
                                    "IN"."SequenceNumber",
                                    "IN"."ASSOC_OrganizationalUnit.IDSystem",
                                    "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_Partner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PhoneNumberType" is null and
            "SequenceNumber" is null and
            "ASSOC_OrganizationalUnit.IDSystem" is null and
            "ASSOC_OrganizationalUnit.OrganizationalUnitID" is null and
            "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_Partner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::PhoneNumber" (
        "PhoneNumberType",
        "SequenceNumber",
        "ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Partner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AreaCode",
        "ContactPersonName",
        "CountryCallingNumber",
        "ExtensionNumber",
        "IsPreferred",
        "PhoneNumber",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PhoneNumberType" as "PhoneNumberType" ,
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD_ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_OrganizationalUnit.IDSystem" ,
            "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_ASSOC_Partner.BusinessPartnerID" as "ASSOC_Partner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AreaCode" as "AreaCode" ,
            "OLD_ContactPersonName" as "ContactPersonName" ,
            "OLD_CountryCallingNumber" as "CountryCallingNumber" ,
            "OLD_ExtensionNumber" as "ExtensionNumber" ,
            "OLD_IsPreferred" as "IsPreferred" ,
            "OLD_PhoneNumber" as "PhoneNumber" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PhoneNumberType",
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_OrganizationalUnit.IDSystem",
                        "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_Partner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PhoneNumberType" AS "OLD_PhoneNumberType" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Partner.BusinessPartnerID" AS "OLD_ASSOC_Partner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AreaCode" AS "OLD_AreaCode" ,
                "OLD"."ContactPersonName" AS "OLD_ContactPersonName" ,
                "OLD"."CountryCallingNumber" AS "OLD_CountryCallingNumber" ,
                "OLD"."ExtensionNumber" AS "OLD_ExtensionNumber" ,
                "OLD"."IsPreferred" AS "OLD_IsPreferred" ,
                "OLD"."PhoneNumber" AS "OLD_PhoneNumber" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PhoneNumber" as "OLD"
            on
                      "IN"."PhoneNumberType" = "OLD"."PhoneNumberType" and
                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                      "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                      "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                      "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                      "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PhoneNumber" (
        "PhoneNumberType",
        "SequenceNumber",
        "ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Partner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AreaCode",
        "ContactPersonName",
        "CountryCallingNumber",
        "ExtensionNumber",
        "IsPreferred",
        "PhoneNumber",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PhoneNumberType" as "PhoneNumberType",
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD_ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_OrganizationalUnit.IDSystem",
            "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_ASSOC_Partner.BusinessPartnerID" as "ASSOC_Partner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AreaCode" as "AreaCode",
            "OLD_ContactPersonName" as "ContactPersonName",
            "OLD_CountryCallingNumber" as "CountryCallingNumber",
            "OLD_ExtensionNumber" as "ExtensionNumber",
            "OLD_IsPreferred" as "IsPreferred",
            "OLD_PhoneNumber" as "PhoneNumber",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PhoneNumberType",
                        "OLD"."SequenceNumber",
                        "OLD"."ASSOC_OrganizationalUnit.IDSystem",
                        "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_Partner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PhoneNumberType" AS "OLD_PhoneNumberType" ,
                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Partner.BusinessPartnerID" AS "OLD_ASSOC_Partner.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AreaCode" AS "OLD_AreaCode" ,
                "OLD"."ContactPersonName" AS "OLD_ContactPersonName" ,
                "OLD"."CountryCallingNumber" AS "OLD_CountryCallingNumber" ,
                "OLD"."ExtensionNumber" AS "OLD_ExtensionNumber" ,
                "OLD"."IsPreferred" AS "OLD_IsPreferred" ,
                "OLD"."PhoneNumber" AS "OLD_PhoneNumber" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PhoneNumber" as "OLD"
            on
                                                "IN"."PhoneNumberType" = "OLD"."PhoneNumberType" and
                                                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                                "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                                                "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                                                "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                                "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::PhoneNumber"
    where (
        "PhoneNumberType",
        "SequenceNumber",
        "ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Partner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PhoneNumberType",
            "OLD"."SequenceNumber",
            "OLD"."ASSOC_OrganizationalUnit.IDSystem",
            "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."ASSOC_Partner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PhoneNumber" as "OLD"
        on
                                       "IN"."PhoneNumberType" = "OLD"."PhoneNumberType" and
                                       "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                       "IN"."ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_OrganizationalUnit.IDSystem" and
                                       "IN"."ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                                       "IN"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                       "IN"."ASSOC_Partner.BusinessPartnerID" = "OLD"."ASSOC_Partner.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
