PROCEDURE "sap.fsdm.procedures::PlanBudgetForecastLoad" (IN ROW "sap.fsdm.tabletypes::PlanBudgetForecastTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ID=' || TO_VARCHAR("ID") || ' ' ||
                'VersionID=' || TO_VARCHAR("VersionID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."VersionID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."VersionID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ID",
                        "VersionID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."VersionID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."VersionID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::PlanBudgetForecast" (
        "ID",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Plan.ID",
        "_Plan.VersionID",
        "Category",
        "EndDate",
        "StartDate",
        "Status",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID" ,
            "OLD_VersionID" as "VersionID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__Plan.ID" as "_Plan.ID" ,
            "OLD__Plan.VersionID" as "_Plan.VersionID" ,
            "OLD_Category" as "Category" ,
            "OLD_EndDate" as "EndDate" ,
            "OLD_StartDate" as "StartDate" ,
            "OLD_Status" as "Status" ,
            "OLD_Type" as "Type" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ID",
                        "IN"."VersionID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ID" as "OLD_ID",
                                "OLD"."VersionID" as "OLD_VersionID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_Plan.ID" as "OLD__Plan.ID",
                                "OLD"."_Plan.VersionID" as "OLD__Plan.VersionID",
                                "OLD"."Category" as "OLD_Category",
                                "OLD"."EndDate" as "OLD_EndDate",
                                "OLD"."StartDate" as "OLD_StartDate",
                                "OLD"."Status" as "OLD_Status",
                                "OLD"."Type" as "OLD_Type",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PlanBudgetForecast" as "OLD"
            on
                ifnull( "IN"."ID", '') = "OLD"."ID" and
                ifnull( "IN"."VersionID", '') = "OLD"."VersionID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PlanBudgetForecast" (
        "ID",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Plan.ID",
        "_Plan.VersionID",
        "Category",
        "EndDate",
        "StartDate",
        "Status",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID",
            "OLD_VersionID" as "VersionID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Plan.ID" as "_Plan.ID",
            "OLD__Plan.VersionID" as "_Plan.VersionID",
            "OLD_Category" as "Category",
            "OLD_EndDate" as "EndDate",
            "OLD_StartDate" as "StartDate",
            "OLD_Status" as "Status",
            "OLD_Type" as "Type",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ID",
                        "IN"."VersionID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."ID" as "OLD_ID",
                        "OLD"."VersionID" as "OLD_VersionID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."_Plan.ID" as "OLD__Plan.ID",
                        "OLD"."_Plan.VersionID" as "OLD__Plan.VersionID",
                        "OLD"."Category" as "OLD_Category",
                        "OLD"."EndDate" as "OLD_EndDate",
                        "OLD"."StartDate" as "OLD_StartDate",
                        "OLD"."Status" as "OLD_Status",
                        "OLD"."Type" as "OLD_Type",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PlanBudgetForecast" as "OLD"
            on
                ifnull( "IN"."ID", '' ) = "OLD"."ID" and
                ifnull( "IN"."VersionID", '' ) = "OLD"."VersionID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::PlanBudgetForecast"
    where (
        "ID",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ID",
            "OLD"."VersionID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PlanBudgetForecast" as "OLD"
        on
           ifnull( "IN"."ID", '' ) = "OLD"."ID" and
           ifnull( "IN"."VersionID", '' ) = "OLD"."VersionID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::PlanBudgetForecast" (
        "ID",
        "VersionID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Plan.ID",
        "_Plan.VersionID",
        "Category",
        "EndDate",
        "StartDate",
        "Status",
        "Type",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "ID", '' ) as "ID",
            ifnull( "VersionID", '' ) as "VersionID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "_Plan.ID"  ,
            "_Plan.VersionID"  ,
            "Category"  ,
            "EndDate"  ,
            "StartDate"  ,
            "Status"  ,
            "Type"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END