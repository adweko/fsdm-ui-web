PROCEDURE "sap.fsdm.procedures::StandardPaymentIntervalListEntryErase" (IN ROW "sap.fsdm.tabletypes::StandardPaymentIntervalListEntryTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PeriodUntilPaymentIntervalEnd" is null and
            "PeriodUntilPaymentIntervalStart" is null and
            "ASSOC_StandardPaymentIntervalList.SequenceNumber" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" is null and
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::StandardPaymentIntervalListEntry"
        WHERE
        (            "PeriodUntilPaymentIntervalEnd" ,
            "PeriodUntilPaymentIntervalStart" ,
            "ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."PeriodUntilPaymentIntervalEnd" ,
                "OLD"."PeriodUntilPaymentIntervalStart" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::StandardPaymentIntervalListEntry" "OLD"
            on
            "IN"."PeriodUntilPaymentIntervalEnd" = "OLD"."PeriodUntilPaymentIntervalEnd" and
            "IN"."PeriodUntilPaymentIntervalStart" = "OLD"."PeriodUntilPaymentIntervalStart" and
            "IN"."ASSOC_StandardPaymentIntervalList.SequenceNumber" = "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber" and
            "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
            "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" and
            "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" and
            "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" 
        );

        --delete data from history table
        delete from "sap.fsdm::StandardPaymentIntervalListEntry_Historical"
        WHERE
        (
            "PeriodUntilPaymentIntervalEnd" ,
            "PeriodUntilPaymentIntervalStart" ,
            "ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
            "ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."PeriodUntilPaymentIntervalEnd" ,
                "OLD"."PeriodUntilPaymentIntervalStart" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" ,
                "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::StandardPaymentIntervalListEntry_Historical" "OLD"
            on
                "IN"."PeriodUntilPaymentIntervalEnd" = "OLD"."PeriodUntilPaymentIntervalEnd" and
                "IN"."PeriodUntilPaymentIntervalStart" = "OLD"."PeriodUntilPaymentIntervalStart" and
                "IN"."ASSOC_StandardPaymentIntervalList.SequenceNumber" = "OLD"."ASSOC_StandardPaymentIntervalList.SequenceNumber" and
                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.FinancialStandardProductID" and
                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.IDSystem" and
                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.PricingScheme" and
                "IN"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" = "OLD"."ASSOC_StandardPaymentIntervalList.ASSOC_FinancialStandardProduct.ASSOC_Company.BusinessPartnerID" 
        );

END
