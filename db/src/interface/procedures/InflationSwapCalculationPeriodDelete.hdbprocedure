PROCEDURE "sap.fsdm.procedures::InflationSwapCalculationPeriodDelete" (IN ROW "sap.fsdm.tabletypes::InflationSwapCalculationPeriodTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IntervalEndDate=' || TO_VARCHAR("IntervalEndDate") || ' ' ||
                'IntervalStartDate=' || TO_VARCHAR("IntervalStartDate") || ' ' ||
                'RoleOfPayer=' || TO_VARCHAR("RoleOfPayer") || ' ' ||
                '_Swap.FinancialContractID=' || TO_VARCHAR("_Swap.FinancialContractID") || ' ' ||
                '_Swap.IDSystem=' || TO_VARCHAR("_Swap.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IntervalEndDate",
                        "IN"."IntervalStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_Swap.FinancialContractID",
                        "IN"."_Swap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IntervalEndDate",
                        "IN"."IntervalStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_Swap.FinancialContractID",
                        "IN"."_Swap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IntervalEndDate",
                        "IntervalStartDate",
                        "RoleOfPayer",
                        "_Swap.FinancialContractID",
                        "_Swap.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IntervalEndDate",
                                    "IN"."IntervalStartDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_Swap.FinancialContractID",
                                    "IN"."_Swap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IntervalEndDate",
                                    "IN"."IntervalStartDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_Swap.FinancialContractID",
                                    "IN"."_Swap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "IntervalEndDate" is null and
            "IntervalStartDate" is null and
            "RoleOfPayer" is null and
            "_Swap.FinancialContractID" is null and
            "_Swap.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::InflationSwapCalculationPeriod" (
        "IntervalEndDate",
        "IntervalStartDate",
        "RoleOfPayer",
        "_Swap.FinancialContractID",
        "_Swap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "CappedPercentage",
        "DayCountConvention",
        "FlooredPercentage",
        "NotionalAmount",
        "NotionalAmountCurrency",
        "Spread",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IntervalEndDate" as "IntervalEndDate" ,
            "OLD_IntervalStartDate" as "IntervalStartDate" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD__Swap.FinancialContractID" as "_Swap.FinancialContractID" ,
            "OLD__Swap.IDSystem" as "_Swap.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_CappedPercentage" as "CappedPercentage" ,
            "OLD_DayCountConvention" as "DayCountConvention" ,
            "OLD_FlooredPercentage" as "FlooredPercentage" ,
            "OLD_NotionalAmount" as "NotionalAmount" ,
            "OLD_NotionalAmountCurrency" as "NotionalAmountCurrency" ,
            "OLD_Spread" as "Spread" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."IntervalEndDate",
                        "OLD"."IntervalStartDate",
                        "OLD"."RoleOfPayer",
                        "OLD"."_Swap.FinancialContractID",
                        "OLD"."_Swap.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."IntervalEndDate" AS "OLD_IntervalEndDate" ,
                "OLD"."IntervalStartDate" AS "OLD_IntervalStartDate" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."_Swap.FinancialContractID" AS "OLD__Swap.FinancialContractID" ,
                "OLD"."_Swap.IDSystem" AS "OLD__Swap.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CappedPercentage" AS "OLD_CappedPercentage" ,
                "OLD"."DayCountConvention" AS "OLD_DayCountConvention" ,
                "OLD"."FlooredPercentage" AS "OLD_FlooredPercentage" ,
                "OLD"."NotionalAmount" AS "OLD_NotionalAmount" ,
                "OLD"."NotionalAmountCurrency" AS "OLD_NotionalAmountCurrency" ,
                "OLD"."Spread" AS "OLD_Spread" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InflationSwapCalculationPeriod" as "OLD"
            on
                      "IN"."IntervalEndDate" = "OLD"."IntervalEndDate" and
                      "IN"."IntervalStartDate" = "OLD"."IntervalStartDate" and
                      "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                      "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                      "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::InflationSwapCalculationPeriod" (
        "IntervalEndDate",
        "IntervalStartDate",
        "RoleOfPayer",
        "_Swap.FinancialContractID",
        "_Swap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "CappedPercentage",
        "DayCountConvention",
        "FlooredPercentage",
        "NotionalAmount",
        "NotionalAmountCurrency",
        "Spread",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IntervalEndDate" as "IntervalEndDate",
            "OLD_IntervalStartDate" as "IntervalStartDate",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD__Swap.FinancialContractID" as "_Swap.FinancialContractID",
            "OLD__Swap.IDSystem" as "_Swap.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_CappedPercentage" as "CappedPercentage",
            "OLD_DayCountConvention" as "DayCountConvention",
            "OLD_FlooredPercentage" as "FlooredPercentage",
            "OLD_NotionalAmount" as "NotionalAmount",
            "OLD_NotionalAmountCurrency" as "NotionalAmountCurrency",
            "OLD_Spread" as "Spread",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."IntervalEndDate",
                        "OLD"."IntervalStartDate",
                        "OLD"."RoleOfPayer",
                        "OLD"."_Swap.FinancialContractID",
                        "OLD"."_Swap.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."IntervalEndDate" AS "OLD_IntervalEndDate" ,
                "OLD"."IntervalStartDate" AS "OLD_IntervalStartDate" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."_Swap.FinancialContractID" AS "OLD__Swap.FinancialContractID" ,
                "OLD"."_Swap.IDSystem" AS "OLD__Swap.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."CappedPercentage" AS "OLD_CappedPercentage" ,
                "OLD"."DayCountConvention" AS "OLD_DayCountConvention" ,
                "OLD"."FlooredPercentage" AS "OLD_FlooredPercentage" ,
                "OLD"."NotionalAmount" AS "OLD_NotionalAmount" ,
                "OLD"."NotionalAmountCurrency" AS "OLD_NotionalAmountCurrency" ,
                "OLD"."Spread" AS "OLD_Spread" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InflationSwapCalculationPeriod" as "OLD"
            on
                                                "IN"."IntervalEndDate" = "OLD"."IntervalEndDate" and
                                                "IN"."IntervalStartDate" = "OLD"."IntervalStartDate" and
                                                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                                                "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                                                "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::InflationSwapCalculationPeriod"
    where (
        "IntervalEndDate",
        "IntervalStartDate",
        "RoleOfPayer",
        "_Swap.FinancialContractID",
        "_Swap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."IntervalEndDate",
            "OLD"."IntervalStartDate",
            "OLD"."RoleOfPayer",
            "OLD"."_Swap.FinancialContractID",
            "OLD"."_Swap.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InflationSwapCalculationPeriod" as "OLD"
        on
                                       "IN"."IntervalEndDate" = "OLD"."IntervalEndDate" and
                                       "IN"."IntervalStartDate" = "OLD"."IntervalStartDate" and
                                       "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                                       "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                                       "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
