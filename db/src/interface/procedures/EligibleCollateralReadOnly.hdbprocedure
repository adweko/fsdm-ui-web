PROCEDURE "sap.fsdm.procedures::EligibleCollateralReadOnly" (IN ROW "sap.fsdm.tabletypes::EligibleCollateralTT", OUT CURR_DEL "sap.fsdm.tabletypes::EligibleCollateralTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EligibleCollateralTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Scheme=' || TO_VARCHAR("Scheme") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_InstrumentClass.InstrumentClass=' || TO_VARCHAR("_InstrumentClass.InstrumentClass") || ' ' ||
                '_InstrumentClass.InstrumentClassificationSystem=' || TO_VARCHAR("_InstrumentClass.InstrumentClassificationSystem") || ' ' ||
                '_MasterAgreement.FinancialContractID=' || TO_VARCHAR("_MasterAgreement.FinancialContractID") || ' ' ||
                '_MasterAgreement.IDSystem=' || TO_VARCHAR("_MasterAgreement.IDSystem") || ' ' ||
                '_Organization.BusinessPartnerID=' || TO_VARCHAR("_Organization.BusinessPartnerID") || ' ' ||
                '_ProductCatalogItem.ProductCatalogItem=' || TO_VARCHAR("_ProductCatalogItem.ProductCatalogItem") || ' ' ||
                '_ProductCatalogItem._ProductCatalog.CatalogID=' || TO_VARCHAR("_ProductCatalogItem._ProductCatalog.CatalogID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Scheme",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InstrumentClass.InstrumentClass",
                        "IN"."_InstrumentClass.InstrumentClassificationSystem",
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_Organization.BusinessPartnerID",
                        "IN"."_ProductCatalogItem.ProductCatalogItem",
                        "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Scheme",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InstrumentClass.InstrumentClass",
                        "IN"."_InstrumentClass.InstrumentClassificationSystem",
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_Organization.BusinessPartnerID",
                        "IN"."_ProductCatalogItem.ProductCatalogItem",
                        "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Scheme",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_InstrumentClass.InstrumentClass",
                        "_InstrumentClass.InstrumentClassificationSystem",
                        "_MasterAgreement.FinancialContractID",
                        "_MasterAgreement.IDSystem",
                        "_Organization.BusinessPartnerID",
                        "_ProductCatalogItem.ProductCatalogItem",
                        "_ProductCatalogItem._ProductCatalog.CatalogID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Scheme",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InstrumentClass.InstrumentClass",
                                    "IN"."_InstrumentClass.InstrumentClassificationSystem",
                                    "IN"."_MasterAgreement.FinancialContractID",
                                    "IN"."_MasterAgreement.IDSystem",
                                    "IN"."_Organization.BusinessPartnerID",
                                    "IN"."_ProductCatalogItem.ProductCatalogItem",
                                    "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Scheme",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InstrumentClass.InstrumentClass",
                                    "IN"."_InstrumentClass.InstrumentClassificationSystem",
                                    "IN"."_MasterAgreement.FinancialContractID",
                                    "IN"."_MasterAgreement.IDSystem",
                                    "IN"."_Organization.BusinessPartnerID",
                                    "IN"."_ProductCatalogItem.ProductCatalogItem",
                                    "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "Scheme",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InstrumentClass.InstrumentClass",
        "_InstrumentClass.InstrumentClassificationSystem",
        "_MasterAgreement.FinancialContractID",
        "_MasterAgreement.IDSystem",
        "_Organization.BusinessPartnerID",
        "_ProductCatalogItem.ProductCatalogItem",
        "_ProductCatalogItem._ProductCatalog.CatalogID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::EligibleCollateral" WHERE
        (            "Scheme" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_InstrumentClass.InstrumentClass" ,
            "_InstrumentClass.InstrumentClassificationSystem" ,
            "_MasterAgreement.FinancialContractID" ,
            "_MasterAgreement.IDSystem" ,
            "_Organization.BusinessPartnerID" ,
            "_ProductCatalogItem.ProductCatalogItem" ,
            "_ProductCatalogItem._ProductCatalog.CatalogID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."Scheme",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_InstrumentClass.InstrumentClass",
            "OLD"."_InstrumentClass.InstrumentClassificationSystem",
            "OLD"."_MasterAgreement.FinancialContractID",
            "OLD"."_MasterAgreement.IDSystem",
            "OLD"."_Organization.BusinessPartnerID",
            "OLD"."_ProductCatalogItem.ProductCatalogItem",
            "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::EligibleCollateral" as "OLD"
            on
               ifnull( "IN"."Scheme",'' ) = "OLD"."Scheme" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" and
               ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID",'' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               ifnull( "IN"."_InstrumentClass.InstrumentClass",'' ) = "OLD"."_InstrumentClass.InstrumentClass" and
               ifnull( "IN"."_InstrumentClass.InstrumentClassificationSystem",'' ) = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
               ifnull( "IN"."_MasterAgreement.FinancialContractID",'' ) = "OLD"."_MasterAgreement.FinancialContractID" and
               ifnull( "IN"."_MasterAgreement.IDSystem",'' ) = "OLD"."_MasterAgreement.IDSystem" and
               ifnull( "IN"."_Organization.BusinessPartnerID",'' ) = "OLD"."_Organization.BusinessPartnerID" and
               ifnull( "IN"."_ProductCatalogItem.ProductCatalogItem",'' ) = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
               ifnull( "IN"."_ProductCatalogItem._ProductCatalog.CatalogID",'' ) = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "Scheme",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InstrumentClass.InstrumentClass",
        "_InstrumentClass.InstrumentClassificationSystem",
        "_MasterAgreement.FinancialContractID",
        "_MasterAgreement.IDSystem",
        "_Organization.BusinessPartnerID",
        "_ProductCatalogItem.ProductCatalogItem",
        "_ProductCatalogItem._ProductCatalog.CatalogID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "EligibilityStatus",
        "Haircut",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "Scheme", '' ) as "Scheme",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
                    ifnull( "_InstrumentClass.InstrumentClass", '' ) as "_InstrumentClass.InstrumentClass",
                    ifnull( "_InstrumentClass.InstrumentClassificationSystem", '' ) as "_InstrumentClass.InstrumentClassificationSystem",
                    ifnull( "_MasterAgreement.FinancialContractID", '' ) as "_MasterAgreement.FinancialContractID",
                    ifnull( "_MasterAgreement.IDSystem", '' ) as "_MasterAgreement.IDSystem",
                    ifnull( "_Organization.BusinessPartnerID", '' ) as "_Organization.BusinessPartnerID",
                    ifnull( "_ProductCatalogItem.ProductCatalogItem", '' ) as "_ProductCatalogItem.ProductCatalogItem",
                    ifnull( "_ProductCatalogItem._ProductCatalog.CatalogID", '' ) as "_ProductCatalogItem._ProductCatalog.CatalogID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "EligibilityStatus"  ,
                    "Haircut"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_Scheme" as "Scheme" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
                    "OLD__InstrumentClass.InstrumentClass" as "_InstrumentClass.InstrumentClass" ,
                    "OLD__InstrumentClass.InstrumentClassificationSystem" as "_InstrumentClass.InstrumentClassificationSystem" ,
                    "OLD__MasterAgreement.FinancialContractID" as "_MasterAgreement.FinancialContractID" ,
                    "OLD__MasterAgreement.IDSystem" as "_MasterAgreement.IDSystem" ,
                    "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID" ,
                    "OLD__ProductCatalogItem.ProductCatalogItem" as "_ProductCatalogItem.ProductCatalogItem" ,
                    "OLD__ProductCatalogItem._ProductCatalog.CatalogID" as "_ProductCatalogItem._ProductCatalog.CatalogID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_EligibilityStatus" as "EligibilityStatus" ,
                    "OLD_Haircut" as "Haircut" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."Scheme",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InstrumentClass.InstrumentClass",
                        "IN"."_InstrumentClass.InstrumentClassificationSystem",
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_Organization.BusinessPartnerID",
                        "IN"."_ProductCatalogItem.ProductCatalogItem",
                        "IN"."_ProductCatalogItem._ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."Scheme" as "OLD_Scheme",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_InstrumentClass.InstrumentClass" as "OLD__InstrumentClass.InstrumentClass",
                                "OLD"."_InstrumentClass.InstrumentClassificationSystem" as "OLD__InstrumentClass.InstrumentClassificationSystem",
                                "OLD"."_MasterAgreement.FinancialContractID" as "OLD__MasterAgreement.FinancialContractID",
                                "OLD"."_MasterAgreement.IDSystem" as "OLD__MasterAgreement.IDSystem",
                                "OLD"."_Organization.BusinessPartnerID" as "OLD__Organization.BusinessPartnerID",
                                "OLD"."_ProductCatalogItem.ProductCatalogItem" as "OLD__ProductCatalogItem.ProductCatalogItem",
                                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" as "OLD__ProductCatalogItem._ProductCatalog.CatalogID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."EligibilityStatus" as "OLD_EligibilityStatus",
                                "OLD"."Haircut" as "OLD_Haircut",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EligibleCollateral" as "OLD"
            on
                ifnull( "IN"."Scheme", '') = "OLD"."Scheme" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_InstrumentClass.InstrumentClass", '') = "OLD"."_InstrumentClass.InstrumentClass" and
                ifnull( "IN"."_InstrumentClass.InstrumentClassificationSystem", '') = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                ifnull( "IN"."_MasterAgreement.FinancialContractID", '') = "OLD"."_MasterAgreement.FinancialContractID" and
                ifnull( "IN"."_MasterAgreement.IDSystem", '') = "OLD"."_MasterAgreement.IDSystem" and
                ifnull( "IN"."_Organization.BusinessPartnerID", '') = "OLD"."_Organization.BusinessPartnerID" and
                ifnull( "IN"."_ProductCatalogItem.ProductCatalogItem", '') = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                ifnull( "IN"."_ProductCatalogItem._ProductCatalog.CatalogID", '') = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_Scheme" as "Scheme",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__InstrumentClass.InstrumentClass" as "_InstrumentClass.InstrumentClass",
            "OLD__InstrumentClass.InstrumentClassificationSystem" as "_InstrumentClass.InstrumentClassificationSystem",
            "OLD__MasterAgreement.FinancialContractID" as "_MasterAgreement.FinancialContractID",
            "OLD__MasterAgreement.IDSystem" as "_MasterAgreement.IDSystem",
            "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID",
            "OLD__ProductCatalogItem.ProductCatalogItem" as "_ProductCatalogItem.ProductCatalogItem",
            "OLD__ProductCatalogItem._ProductCatalog.CatalogID" as "_ProductCatalogItem._ProductCatalog.CatalogID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_EligibilityStatus" as "EligibilityStatus",
            "OLD_Haircut" as "Haircut",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."Scheme",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InstrumentClass.InstrumentClass",
                        "IN"."_InstrumentClass.InstrumentClassificationSystem",
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_Organization.BusinessPartnerID",
                        "IN"."_ProductCatalogItem.ProductCatalogItem",
                        "IN"."_ProductCatalogItem._ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."Scheme" as "OLD_Scheme",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InstrumentClass.InstrumentClass" as "OLD__InstrumentClass.InstrumentClass",
                        "OLD"."_InstrumentClass.InstrumentClassificationSystem" as "OLD__InstrumentClass.InstrumentClassificationSystem",
                        "OLD"."_MasterAgreement.FinancialContractID" as "OLD__MasterAgreement.FinancialContractID",
                        "OLD"."_MasterAgreement.IDSystem" as "OLD__MasterAgreement.IDSystem",
                        "OLD"."_Organization.BusinessPartnerID" as "OLD__Organization.BusinessPartnerID",
                        "OLD"."_ProductCatalogItem.ProductCatalogItem" as "OLD__ProductCatalogItem.ProductCatalogItem",
                        "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" as "OLD__ProductCatalogItem._ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."EligibilityStatus" as "OLD_EligibilityStatus",
                        "OLD"."Haircut" as "OLD_Haircut",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::EligibleCollateral" as "OLD"
            on
                ifnull("IN"."Scheme", '') = "OLD"."Scheme" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull("IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull("IN"."_InstrumentClass.InstrumentClass", '') = "OLD"."_InstrumentClass.InstrumentClass" and
                ifnull("IN"."_InstrumentClass.InstrumentClassificationSystem", '') = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                ifnull("IN"."_MasterAgreement.FinancialContractID", '') = "OLD"."_MasterAgreement.FinancialContractID" and
                ifnull("IN"."_MasterAgreement.IDSystem", '') = "OLD"."_MasterAgreement.IDSystem" and
                ifnull("IN"."_Organization.BusinessPartnerID", '') = "OLD"."_Organization.BusinessPartnerID" and
                ifnull("IN"."_ProductCatalogItem.ProductCatalogItem", '') = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                ifnull("IN"."_ProductCatalogItem._ProductCatalog.CatalogID", '') = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
