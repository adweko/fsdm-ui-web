PROCEDURE "sap.fsdm.procedures::OrganizationalUnitTradeAssignmentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::OrganizationalUnitTradeAssignmentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::OrganizationalUnitTradeAssignmentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::OrganizationalUnitTradeAssignmentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "RoleOfOrganizationalUnit" is null and
            "_OrgUnit.IDSystem" is null and
            "_OrgUnit.OrganizationalUnitID" is null and
            "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "RoleOfOrganizationalUnit" ,
                "_OrgUnit.IDSystem" ,
                "_OrgUnit.OrganizationalUnitID" ,
                "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "_Trade.IDSystem" ,
                "_Trade.TradeID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."RoleOfOrganizationalUnit" ,
                "OLD"."_OrgUnit.IDSystem" ,
                "OLD"."_OrgUnit.OrganizationalUnitID" ,
                "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::OrganizationalUnitTradeAssignment" "OLD"
            on
                "IN"."RoleOfOrganizationalUnit" = "OLD"."RoleOfOrganizationalUnit" and
                "IN"."_OrgUnit.IDSystem" = "OLD"."_OrgUnit.IDSystem" and
                "IN"."_OrgUnit.OrganizationalUnitID" = "OLD"."_OrgUnit.OrganizationalUnitID" and
                "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "RoleOfOrganizationalUnit" ,
            "_OrgUnit.IDSystem" ,
            "_OrgUnit.OrganizationalUnitID" ,
            "_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_Trade.IDSystem" ,
            "_Trade.TradeID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."RoleOfOrganizationalUnit" ,
                "OLD"."_OrgUnit.IDSystem" ,
                "OLD"."_OrgUnit.OrganizationalUnitID" ,
                "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::OrganizationalUnitTradeAssignment_Historical" "OLD"
            on
                "IN"."RoleOfOrganizationalUnit" = "OLD"."RoleOfOrganizationalUnit" and
                "IN"."_OrgUnit.IDSystem" = "OLD"."_OrgUnit.IDSystem" and
                "IN"."_OrgUnit.OrganizationalUnitID" = "OLD"."_OrgUnit.OrganizationalUnitID" and
                "IN"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_OrgUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        );

END
