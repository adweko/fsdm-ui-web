PROCEDURE "sap.fsdm.procedures::IndustryClassHierarchyRelationDelete" (IN ROW "sap.fsdm.tabletypes::IndustryClassHierarchyRelationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IndustryHierarchy=' || TO_VARCHAR("IndustryHierarchy") || ' ' ||
                'ASSOC_Child.Industry=' || TO_VARCHAR("ASSOC_Child.Industry") || ' ' ||
                'ASSOC_Child.IndustryClassificationSystem=' || TO_VARCHAR("ASSOC_Child.IndustryClassificationSystem") || ' ' ||
                'ASSOC_Parent.Industry=' || TO_VARCHAR("ASSOC_Parent.Industry") || ' ' ||
                'ASSOC_Parent.IndustryClassificationSystem=' || TO_VARCHAR("ASSOC_Parent.IndustryClassificationSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IndustryHierarchy",
                        "IN"."ASSOC_Child.Industry",
                        "IN"."ASSOC_Child.IndustryClassificationSystem",
                        "IN"."ASSOC_Parent.Industry",
                        "IN"."ASSOC_Parent.IndustryClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IndustryHierarchy",
                        "IN"."ASSOC_Child.Industry",
                        "IN"."ASSOC_Child.IndustryClassificationSystem",
                        "IN"."ASSOC_Parent.Industry",
                        "IN"."ASSOC_Parent.IndustryClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IndustryHierarchy",
                        "ASSOC_Child.Industry",
                        "ASSOC_Child.IndustryClassificationSystem",
                        "ASSOC_Parent.Industry",
                        "ASSOC_Parent.IndustryClassificationSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IndustryHierarchy",
                                    "IN"."ASSOC_Child.Industry",
                                    "IN"."ASSOC_Child.IndustryClassificationSystem",
                                    "IN"."ASSOC_Parent.Industry",
                                    "IN"."ASSOC_Parent.IndustryClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IndustryHierarchy",
                                    "IN"."ASSOC_Child.Industry",
                                    "IN"."ASSOC_Child.IndustryClassificationSystem",
                                    "IN"."ASSOC_Parent.Industry",
                                    "IN"."ASSOC_Parent.IndustryClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "IndustryHierarchy" is null and
            "ASSOC_Child.Industry" is null and
            "ASSOC_Child.IndustryClassificationSystem" is null and
            "ASSOC_Parent.Industry" is null and
            "ASSOC_Parent.IndustryClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::IndustryClassHierarchyRelation" (
        "IndustryHierarchy",
        "ASSOC_Child.Industry",
        "ASSOC_Child.IndustryClassificationSystem",
        "ASSOC_Parent.Industry",
        "ASSOC_Parent.IndustryClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "IndustryHierarchyName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IndustryHierarchy" as "IndustryHierarchy" ,
            "OLD_ASSOC_Child.Industry" as "ASSOC_Child.Industry" ,
            "OLD_ASSOC_Child.IndustryClassificationSystem" as "ASSOC_Child.IndustryClassificationSystem" ,
            "OLD_ASSOC_Parent.Industry" as "ASSOC_Parent.Industry" ,
            "OLD_ASSOC_Parent.IndustryClassificationSystem" as "ASSOC_Parent.IndustryClassificationSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_IndustryHierarchyName" as "IndustryHierarchyName" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."IndustryHierarchy",
                        "OLD"."ASSOC_Child.Industry",
                        "OLD"."ASSOC_Child.IndustryClassificationSystem",
                        "OLD"."ASSOC_Parent.Industry",
                        "OLD"."ASSOC_Parent.IndustryClassificationSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."IndustryHierarchy" AS "OLD_IndustryHierarchy" ,
                "OLD"."ASSOC_Child.Industry" AS "OLD_ASSOC_Child.Industry" ,
                "OLD"."ASSOC_Child.IndustryClassificationSystem" AS "OLD_ASSOC_Child.IndustryClassificationSystem" ,
                "OLD"."ASSOC_Parent.Industry" AS "OLD_ASSOC_Parent.Industry" ,
                "OLD"."ASSOC_Parent.IndustryClassificationSystem" AS "OLD_ASSOC_Parent.IndustryClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."IndustryHierarchyName" AS "OLD_IndustryHierarchyName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::IndustryClassHierarchyRelation" as "OLD"
            on
                      "IN"."IndustryHierarchy" = "OLD"."IndustryHierarchy" and
                      "IN"."ASSOC_Child.Industry" = "OLD"."ASSOC_Child.Industry" and
                      "IN"."ASSOC_Child.IndustryClassificationSystem" = "OLD"."ASSOC_Child.IndustryClassificationSystem" and
                      "IN"."ASSOC_Parent.Industry" = "OLD"."ASSOC_Parent.Industry" and
                      "IN"."ASSOC_Parent.IndustryClassificationSystem" = "OLD"."ASSOC_Parent.IndustryClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::IndustryClassHierarchyRelation" (
        "IndustryHierarchy",
        "ASSOC_Child.Industry",
        "ASSOC_Child.IndustryClassificationSystem",
        "ASSOC_Parent.Industry",
        "ASSOC_Parent.IndustryClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "IndustryHierarchyName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_IndustryHierarchy" as "IndustryHierarchy",
            "OLD_ASSOC_Child.Industry" as "ASSOC_Child.Industry",
            "OLD_ASSOC_Child.IndustryClassificationSystem" as "ASSOC_Child.IndustryClassificationSystem",
            "OLD_ASSOC_Parent.Industry" as "ASSOC_Parent.Industry",
            "OLD_ASSOC_Parent.IndustryClassificationSystem" as "ASSOC_Parent.IndustryClassificationSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_IndustryHierarchyName" as "IndustryHierarchyName",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."IndustryHierarchy",
                        "OLD"."ASSOC_Child.Industry",
                        "OLD"."ASSOC_Child.IndustryClassificationSystem",
                        "OLD"."ASSOC_Parent.Industry",
                        "OLD"."ASSOC_Parent.IndustryClassificationSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."IndustryHierarchy" AS "OLD_IndustryHierarchy" ,
                "OLD"."ASSOC_Child.Industry" AS "OLD_ASSOC_Child.Industry" ,
                "OLD"."ASSOC_Child.IndustryClassificationSystem" AS "OLD_ASSOC_Child.IndustryClassificationSystem" ,
                "OLD"."ASSOC_Parent.Industry" AS "OLD_ASSOC_Parent.Industry" ,
                "OLD"."ASSOC_Parent.IndustryClassificationSystem" AS "OLD_ASSOC_Parent.IndustryClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."IndustryHierarchyName" AS "OLD_IndustryHierarchyName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::IndustryClassHierarchyRelation" as "OLD"
            on
                                                "IN"."IndustryHierarchy" = "OLD"."IndustryHierarchy" and
                                                "IN"."ASSOC_Child.Industry" = "OLD"."ASSOC_Child.Industry" and
                                                "IN"."ASSOC_Child.IndustryClassificationSystem" = "OLD"."ASSOC_Child.IndustryClassificationSystem" and
                                                "IN"."ASSOC_Parent.Industry" = "OLD"."ASSOC_Parent.Industry" and
                                                "IN"."ASSOC_Parent.IndustryClassificationSystem" = "OLD"."ASSOC_Parent.IndustryClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::IndustryClassHierarchyRelation"
    where (
        "IndustryHierarchy",
        "ASSOC_Child.Industry",
        "ASSOC_Child.IndustryClassificationSystem",
        "ASSOC_Parent.Industry",
        "ASSOC_Parent.IndustryClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."IndustryHierarchy",
            "OLD"."ASSOC_Child.Industry",
            "OLD"."ASSOC_Child.IndustryClassificationSystem",
            "OLD"."ASSOC_Parent.Industry",
            "OLD"."ASSOC_Parent.IndustryClassificationSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::IndustryClassHierarchyRelation" as "OLD"
        on
                                       "IN"."IndustryHierarchy" = "OLD"."IndustryHierarchy" and
                                       "IN"."ASSOC_Child.Industry" = "OLD"."ASSOC_Child.Industry" and
                                       "IN"."ASSOC_Child.IndustryClassificationSystem" = "OLD"."ASSOC_Child.IndustryClassificationSystem" and
                                       "IN"."ASSOC_Parent.Industry" = "OLD"."ASSOC_Parent.Industry" and
                                       "IN"."ASSOC_Parent.IndustryClassificationSystem" = "OLD"."ASSOC_Parent.IndustryClassificationSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
