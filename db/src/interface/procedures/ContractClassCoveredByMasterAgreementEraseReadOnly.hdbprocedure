PROCEDURE "sap.fsdm.procedures::ContractClassCoveredByMasterAgreementEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ContractClassCoveredByMasterAgreementTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ContractClassCoveredByMasterAgreementTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ContractClassCoveredByMasterAgreementTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_MasterAgreement.FinancialContractID" is null and
            "_MasterAgreement.IDSystem" is null and
            "_ProductClass.ProductClass" is null and
            "_ProductClass.ProductClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_MasterAgreement.FinancialContractID" ,
                "_MasterAgreement.IDSystem" ,
                "_ProductClass.ProductClass" ,
                "_ProductClass.ProductClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" ,
                "OLD"."_ProductClass.ProductClass" ,
                "OLD"."_ProductClass.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ContractClassCoveredByMasterAgreement" "OLD"
            on
                "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
                "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_MasterAgreement.FinancialContractID" ,
            "_MasterAgreement.IDSystem" ,
            "_ProductClass.ProductClass" ,
            "_ProductClass.ProductClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" ,
                "OLD"."_ProductClass.ProductClass" ,
                "OLD"."_ProductClass.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ContractClassCoveredByMasterAgreement_Historical" "OLD"
            on
                "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
                "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" 
        );

END
