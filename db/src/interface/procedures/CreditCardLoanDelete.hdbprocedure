PROCEDURE "sap.fsdm.procedures::CreditCardLoanDelete" (IN ROW "sap.fsdm.tabletypes::CreditCardLoanTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ID=' || TO_VARCHAR("ID") || ' ' ||
                '_CreditCardAgreement.FinancialContractID=' || TO_VARCHAR("_CreditCardAgreement.FinancialContractID") || ' ' ||
                '_CreditCardAgreement.IDSystem=' || TO_VARCHAR("_CreditCardAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."_CreditCardAgreement.FinancialContractID",
                        "IN"."_CreditCardAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ID",
                        "IN"."_CreditCardAgreement.FinancialContractID",
                        "IN"."_CreditCardAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ID",
                        "_CreditCardAgreement.FinancialContractID",
                        "_CreditCardAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."_CreditCardAgreement.FinancialContractID",
                                    "IN"."_CreditCardAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ID",
                                    "IN"."_CreditCardAgreement.FinancialContractID",
                                    "IN"."_CreditCardAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_CreditCardAgreement.FinancialContractID" is null and
            "_CreditCardAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CreditCardLoan" (
        "ID",
        "_CreditCardAgreement.FinancialContractID",
        "_CreditCardAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CreationDate",
        "Description",
        "LoanAmount",
        "LoanAmountCurrency",
        "NumberOfInstallments",
        "Status",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID" ,
            "OLD__CreditCardAgreement.FinancialContractID" as "_CreditCardAgreement.FinancialContractID" ,
            "OLD__CreditCardAgreement.IDSystem" as "_CreditCardAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CreationDate" as "CreationDate" ,
            "OLD_Description" as "Description" ,
            "OLD_LoanAmount" as "LoanAmount" ,
            "OLD_LoanAmountCurrency" as "LoanAmountCurrency" ,
            "OLD_NumberOfInstallments" as "NumberOfInstallments" ,
            "OLD_Status" as "Status" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."_CreditCardAgreement.FinancialContractID",
                        "OLD"."_CreditCardAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."_CreditCardAgreement.FinancialContractID" AS "OLD__CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardAgreement.IDSystem" AS "OLD__CreditCardAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CreationDate" AS "OLD_CreationDate" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."LoanAmount" AS "OLD_LoanAmount" ,
                "OLD"."LoanAmountCurrency" AS "OLD_LoanAmountCurrency" ,
                "OLD"."NumberOfInstallments" AS "OLD_NumberOfInstallments" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditCardLoan" as "OLD"
            on
                      "IN"."ID" = "OLD"."ID" and
                      "IN"."_CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardAgreement.FinancialContractID" and
                      "IN"."_CreditCardAgreement.IDSystem" = "OLD"."_CreditCardAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CreditCardLoan" (
        "ID",
        "_CreditCardAgreement.FinancialContractID",
        "_CreditCardAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CreationDate",
        "Description",
        "LoanAmount",
        "LoanAmountCurrency",
        "NumberOfInstallments",
        "Status",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ID" as "ID",
            "OLD__CreditCardAgreement.FinancialContractID" as "_CreditCardAgreement.FinancialContractID",
            "OLD__CreditCardAgreement.IDSystem" as "_CreditCardAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CreationDate" as "CreationDate",
            "OLD_Description" as "Description",
            "OLD_LoanAmount" as "LoanAmount",
            "OLD_LoanAmountCurrency" as "LoanAmountCurrency",
            "OLD_NumberOfInstallments" as "NumberOfInstallments",
            "OLD_Status" as "Status",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ID",
                        "OLD"."_CreditCardAgreement.FinancialContractID",
                        "OLD"."_CreditCardAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ID" AS "OLD_ID" ,
                "OLD"."_CreditCardAgreement.FinancialContractID" AS "OLD__CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardAgreement.IDSystem" AS "OLD__CreditCardAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CreationDate" AS "OLD_CreationDate" ,
                "OLD"."Description" AS "OLD_Description" ,
                "OLD"."LoanAmount" AS "OLD_LoanAmount" ,
                "OLD"."LoanAmountCurrency" AS "OLD_LoanAmountCurrency" ,
                "OLD"."NumberOfInstallments" AS "OLD_NumberOfInstallments" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditCardLoan" as "OLD"
            on
                                                "IN"."ID" = "OLD"."ID" and
                                                "IN"."_CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardAgreement.FinancialContractID" and
                                                "IN"."_CreditCardAgreement.IDSystem" = "OLD"."_CreditCardAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CreditCardLoan"
    where (
        "ID",
        "_CreditCardAgreement.FinancialContractID",
        "_CreditCardAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ID",
            "OLD"."_CreditCardAgreement.FinancialContractID",
            "OLD"."_CreditCardAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditCardLoan" as "OLD"
        on
                                       "IN"."ID" = "OLD"."ID" and
                                       "IN"."_CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardAgreement.FinancialContractID" and
                                       "IN"."_CreditCardAgreement.IDSystem" = "OLD"."_CreditCardAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
