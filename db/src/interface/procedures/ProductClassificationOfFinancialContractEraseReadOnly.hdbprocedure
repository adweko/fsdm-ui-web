PROCEDURE "sap.fsdm.procedures::ProductClassificationOfFinancialContractEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ProductClassificationOfFinancialContractTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ProductClassificationOfFinancialContractTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ProductClassificationOfFinancialContractTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_ProductClass.ProductClass" is null and
            "ASSOC_ProductClass.ProductClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_ProductClass.ProductClass" ,
                "ASSOC_ProductClass.ProductClassificationSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_ProductClass.ProductClass" ,
                "OLD"."ASSOC_ProductClass.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ProductClassificationOfFinancialContract" "OLD"
            on
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_ProductClass.ProductClass" = "OLD"."ASSOC_ProductClass.ProductClass" and
                "IN"."ASSOC_ProductClass.ProductClassificationSystem" = "OLD"."ASSOC_ProductClass.ProductClassificationSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_ProductClass.ProductClass" ,
            "ASSOC_ProductClass.ProductClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_ProductClass.ProductClass" ,
                "OLD"."ASSOC_ProductClass.ProductClassificationSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ProductClassificationOfFinancialContract_Historical" "OLD"
            on
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_ProductClass.ProductClass" = "OLD"."ASSOC_ProductClass.ProductClass" and
                "IN"."ASSOC_ProductClass.ProductClassificationSystem" = "OLD"."ASSOC_ProductClass.ProductClassificationSystem" 
        );

END
