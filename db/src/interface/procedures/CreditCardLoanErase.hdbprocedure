PROCEDURE "sap.fsdm.procedures::CreditCardLoanErase" (IN ROW "sap.fsdm.tabletypes::CreditCardLoanTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_CreditCardAgreement.FinancialContractID" is null and
            "_CreditCardAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::CreditCardLoan"
        WHERE
        (            "ID" ,
            "_CreditCardAgreement.FinancialContractID" ,
            "_CreditCardAgreement.IDSystem" 
        ) in
        (
            select                 "OLD"."ID" ,
                "OLD"."_CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditCardLoan" "OLD"
            on
            "IN"."ID" = "OLD"."ID" and
            "IN"."_CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardAgreement.FinancialContractID" and
            "IN"."_CreditCardAgreement.IDSystem" = "OLD"."_CreditCardAgreement.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::CreditCardLoan_Historical"
        WHERE
        (
            "ID" ,
            "_CreditCardAgreement.FinancialContractID" ,
            "_CreditCardAgreement.IDSystem" 
        ) in
        (
            select
                "OLD"."ID" ,
                "OLD"."_CreditCardAgreement.FinancialContractID" ,
                "OLD"."_CreditCardAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::CreditCardLoan_Historical" "OLD"
            on
                "IN"."ID" = "OLD"."ID" and
                "IN"."_CreditCardAgreement.FinancialContractID" = "OLD"."_CreditCardAgreement.FinancialContractID" and
                "IN"."_CreditCardAgreement.IDSystem" = "OLD"."_CreditCardAgreement.IDSystem" 
        );

END
