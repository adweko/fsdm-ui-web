PROCEDURE "sap.fsdm.procedures::OrganizationalUnitHierarchyRelationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::OrganizationalUnitHierarchyRelationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::OrganizationalUnitHierarchyRelationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::OrganizationalUnitHierarchyRelationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'OrganizationalUnitHierarchyID=' || TO_VARCHAR("OrganizationalUnitHierarchyID") || ' ' ||
                'ASSOC_Child.IDSystem=' || TO_VARCHAR("ASSOC_Child.IDSystem") || ' ' ||
                'ASSOC_Child.OrganizationalUnitID=' || TO_VARCHAR("ASSOC_Child.OrganizationalUnitID") || ' ' ||
                'ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                'ASSOC_Parent.IDSystem=' || TO_VARCHAR("ASSOC_Parent.IDSystem") || ' ' ||
                'ASSOC_Parent.OrganizationalUnitID=' || TO_VARCHAR("ASSOC_Parent.OrganizationalUnitID") || ' ' ||
                'ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."OrganizationalUnitHierarchyID",
                        "IN"."ASSOC_Child.IDSystem",
                        "IN"."ASSOC_Child.OrganizationalUnitID",
                        "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_Parent.IDSystem",
                        "IN"."ASSOC_Parent.OrganizationalUnitID",
                        "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."OrganizationalUnitHierarchyID",
                        "IN"."ASSOC_Child.IDSystem",
                        "IN"."ASSOC_Child.OrganizationalUnitID",
                        "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_Parent.IDSystem",
                        "IN"."ASSOC_Parent.OrganizationalUnitID",
                        "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "OrganizationalUnitHierarchyID",
                        "ASSOC_Child.IDSystem",
                        "ASSOC_Child.OrganizationalUnitID",
                        "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "ASSOC_Parent.IDSystem",
                        "ASSOC_Parent.OrganizationalUnitID",
                        "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."OrganizationalUnitHierarchyID",
                                    "IN"."ASSOC_Child.IDSystem",
                                    "IN"."ASSOC_Child.OrganizationalUnitID",
                                    "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_Parent.IDSystem",
                                    "IN"."ASSOC_Parent.OrganizationalUnitID",
                                    "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."OrganizationalUnitHierarchyID",
                                    "IN"."ASSOC_Child.IDSystem",
                                    "IN"."ASSOC_Child.OrganizationalUnitID",
                                    "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_Parent.IDSystem",
                                    "IN"."ASSOC_Parent.OrganizationalUnitID",
                                    "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "OrganizationalUnitHierarchyID" is null and
            "ASSOC_Child.IDSystem" is null and
            "ASSOC_Child.OrganizationalUnitID" is null and
            "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_Parent.IDSystem" is null and
            "ASSOC_Parent.OrganizationalUnitID" is null and
            "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "OrganizationalUnitHierarchyID",
            "ASSOC_Child.IDSystem",
            "ASSOC_Child.OrganizationalUnitID",
            "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "ASSOC_Parent.IDSystem",
            "ASSOC_Parent.OrganizationalUnitID",
            "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::OrganizationalUnitHierarchyRelation" WHERE
            (
            "OrganizationalUnitHierarchyID" ,
            "ASSOC_Child.IDSystem" ,
            "ASSOC_Child.OrganizationalUnitID" ,
            "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "ASSOC_Parent.IDSystem" ,
            "ASSOC_Parent.OrganizationalUnitID" ,
            "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."OrganizationalUnitHierarchyID",
            "OLD"."ASSOC_Child.IDSystem",
            "OLD"."ASSOC_Child.OrganizationalUnitID",
            "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."ASSOC_Parent.IDSystem",
            "OLD"."ASSOC_Parent.OrganizationalUnitID",
            "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::OrganizationalUnitHierarchyRelation" as "OLD"
        on
                              "IN"."OrganizationalUnitHierarchyID" = "OLD"."OrganizationalUnitHierarchyID" and
                              "IN"."ASSOC_Child.IDSystem" = "OLD"."ASSOC_Child.IDSystem" and
                              "IN"."ASSOC_Child.OrganizationalUnitID" = "OLD"."ASSOC_Child.OrganizationalUnitID" and
                              "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                              "IN"."ASSOC_Parent.IDSystem" = "OLD"."ASSOC_Parent.IDSystem" and
                              "IN"."ASSOC_Parent.OrganizationalUnitID" = "OLD"."ASSOC_Parent.OrganizationalUnitID" and
                              "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "OrganizationalUnitHierarchyID",
        "ASSOC_Child.IDSystem",
        "ASSOC_Child.OrganizationalUnitID",
        "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Parent.IDSystem",
        "ASSOC_Parent.OrganizationalUnitID",
        "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "OrganizationalUnitHierarchyName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_OrganizationalUnitHierarchyID" as "OrganizationalUnitHierarchyID" ,
            "OLD_ASSOC_Child.IDSystem" as "ASSOC_Child.IDSystem" ,
            "OLD_ASSOC_Child.OrganizationalUnitID" as "ASSOC_Child.OrganizationalUnitID" ,
            "OLD_ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_ASSOC_Parent.IDSystem" as "ASSOC_Parent.IDSystem" ,
            "OLD_ASSOC_Parent.OrganizationalUnitID" as "ASSOC_Parent.OrganizationalUnitID" ,
            "OLD_ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_OrganizationalUnitHierarchyName" as "OrganizationalUnitHierarchyName" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."OrganizationalUnitHierarchyID",
                        "OLD"."ASSOC_Child.IDSystem",
                        "OLD"."ASSOC_Child.OrganizationalUnitID",
                        "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_Parent.IDSystem",
                        "OLD"."ASSOC_Parent.OrganizationalUnitID",
                        "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."OrganizationalUnitHierarchyID" AS "OLD_OrganizationalUnitHierarchyID" ,
                "OLD"."ASSOC_Child.IDSystem" AS "OLD_ASSOC_Child.IDSystem" ,
                "OLD"."ASSOC_Child.OrganizationalUnitID" AS "OLD_ASSOC_Child.OrganizationalUnitID" ,
                "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Parent.IDSystem" AS "OLD_ASSOC_Parent.IDSystem" ,
                "OLD"."ASSOC_Parent.OrganizationalUnitID" AS "OLD_ASSOC_Parent.OrganizationalUnitID" ,
                "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."OrganizationalUnitHierarchyName" AS "OLD_OrganizationalUnitHierarchyName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnitHierarchyRelation" as "OLD"
            on
                                      "IN"."OrganizationalUnitHierarchyID" = "OLD"."OrganizationalUnitHierarchyID" and
                                      "IN"."ASSOC_Child.IDSystem" = "OLD"."ASSOC_Child.IDSystem" and
                                      "IN"."ASSOC_Child.OrganizationalUnitID" = "OLD"."ASSOC_Child.OrganizationalUnitID" and
                                      "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                      "IN"."ASSOC_Parent.IDSystem" = "OLD"."ASSOC_Parent.IDSystem" and
                                      "IN"."ASSOC_Parent.OrganizationalUnitID" = "OLD"."ASSOC_Parent.OrganizationalUnitID" and
                                      "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_OrganizationalUnitHierarchyID" as "OrganizationalUnitHierarchyID",
            "OLD_ASSOC_Child.IDSystem" as "ASSOC_Child.IDSystem",
            "OLD_ASSOC_Child.OrganizationalUnitID" as "ASSOC_Child.OrganizationalUnitID",
            "OLD_ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_ASSOC_Parent.IDSystem" as "ASSOC_Parent.IDSystem",
            "OLD_ASSOC_Parent.OrganizationalUnitID" as "ASSOC_Parent.OrganizationalUnitID",
            "OLD_ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_OrganizationalUnitHierarchyName" as "OrganizationalUnitHierarchyName",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."OrganizationalUnitHierarchyID",
                        "OLD"."ASSOC_Child.IDSystem",
                        "OLD"."ASSOC_Child.OrganizationalUnitID",
                        "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_Parent.IDSystem",
                        "OLD"."ASSOC_Parent.OrganizationalUnitID",
                        "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."OrganizationalUnitHierarchyID" AS "OLD_OrganizationalUnitHierarchyID" ,
                "OLD"."ASSOC_Child.IDSystem" AS "OLD_ASSOC_Child.IDSystem" ,
                "OLD"."ASSOC_Child.OrganizationalUnitID" AS "OLD_ASSOC_Child.OrganizationalUnitID" ,
                "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Parent.IDSystem" AS "OLD_ASSOC_Parent.IDSystem" ,
                "OLD"."ASSOC_Parent.OrganizationalUnitID" AS "OLD_ASSOC_Parent.OrganizationalUnitID" ,
                "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."OrganizationalUnitHierarchyName" AS "OLD_OrganizationalUnitHierarchyName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnitHierarchyRelation" as "OLD"
            on
               "IN"."OrganizationalUnitHierarchyID" = "OLD"."OrganizationalUnitHierarchyID" and
               "IN"."ASSOC_Child.IDSystem" = "OLD"."ASSOC_Child.IDSystem" and
               "IN"."ASSOC_Child.OrganizationalUnitID" = "OLD"."ASSOC_Child.OrganizationalUnitID" and
               "IN"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Child.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
               "IN"."ASSOC_Parent.IDSystem" = "OLD"."ASSOC_Parent.IDSystem" and
               "IN"."ASSOC_Parent.OrganizationalUnitID" = "OLD"."ASSOC_Parent.OrganizationalUnitID" and
               "IN"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Parent.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
