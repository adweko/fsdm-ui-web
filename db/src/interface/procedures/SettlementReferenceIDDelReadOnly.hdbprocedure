PROCEDURE "sap.fsdm.procedures::SettlementReferenceIDDelReadOnly" (IN ROW "sap.fsdm.tabletypes::SettlementReferenceIDTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::SettlementReferenceIDTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SettlementReferenceIDTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ReferenceIDType=' || TO_VARCHAR("ReferenceIDType") || ' ' ||
                'SystemID=' || TO_VARCHAR("SystemID") || ' ' ||
                '_Settlement.IDSystem=' || TO_VARCHAR("_Settlement.IDSystem") || ' ' ||
                '_Settlement.SettlementID=' || TO_VARCHAR("_Settlement.SettlementID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ReferenceIDType",
                        "IN"."SystemID",
                        "IN"."_Settlement.IDSystem",
                        "IN"."_Settlement.SettlementID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ReferenceIDType",
                        "IN"."SystemID",
                        "IN"."_Settlement.IDSystem",
                        "IN"."_Settlement.SettlementID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ReferenceIDType",
                        "SystemID",
                        "_Settlement.IDSystem",
                        "_Settlement.SettlementID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ReferenceIDType",
                                    "IN"."SystemID",
                                    "IN"."_Settlement.IDSystem",
                                    "IN"."_Settlement.SettlementID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ReferenceIDType",
                                    "IN"."SystemID",
                                    "IN"."_Settlement.IDSystem",
                                    "IN"."_Settlement.SettlementID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ReferenceIDType" is null and
            "SystemID" is null and
            "_Settlement.IDSystem" is null and
            "_Settlement.SettlementID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ReferenceIDType",
            "SystemID",
            "_Settlement.IDSystem",
            "_Settlement.SettlementID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::SettlementReferenceID" WHERE
            (
            "ReferenceIDType" ,
            "SystemID" ,
            "_Settlement.IDSystem" ,
            "_Settlement.SettlementID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ReferenceIDType",
            "OLD"."SystemID",
            "OLD"."_Settlement.IDSystem",
            "OLD"."_Settlement.SettlementID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SettlementReferenceID" as "OLD"
        on
                              "IN"."ReferenceIDType" = "OLD"."ReferenceIDType" and
                              "IN"."SystemID" = "OLD"."SystemID" and
                              "IN"."_Settlement.IDSystem" = "OLD"."_Settlement.IDSystem" and
                              "IN"."_Settlement.SettlementID" = "OLD"."_Settlement.SettlementID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ReferenceIDType",
        "SystemID",
        "_Settlement.IDSystem",
        "_Settlement.SettlementID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ReferenceID",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ReferenceIDType" as "ReferenceIDType" ,
            "OLD_SystemID" as "SystemID" ,
            "OLD__Settlement.IDSystem" as "_Settlement.IDSystem" ,
            "OLD__Settlement.SettlementID" as "_Settlement.SettlementID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ReferenceID" as "ReferenceID" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ReferenceIDType",
                        "OLD"."SystemID",
                        "OLD"."_Settlement.IDSystem",
                        "OLD"."_Settlement.SettlementID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ReferenceIDType" AS "OLD_ReferenceIDType" ,
                "OLD"."SystemID" AS "OLD_SystemID" ,
                "OLD"."_Settlement.IDSystem" AS "OLD__Settlement.IDSystem" ,
                "OLD"."_Settlement.SettlementID" AS "OLD__Settlement.SettlementID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ReferenceID" AS "OLD_ReferenceID" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SettlementReferenceID" as "OLD"
            on
                                      "IN"."ReferenceIDType" = "OLD"."ReferenceIDType" and
                                      "IN"."SystemID" = "OLD"."SystemID" and
                                      "IN"."_Settlement.IDSystem" = "OLD"."_Settlement.IDSystem" and
                                      "IN"."_Settlement.SettlementID" = "OLD"."_Settlement.SettlementID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ReferenceIDType" as "ReferenceIDType",
            "OLD_SystemID" as "SystemID",
            "OLD__Settlement.IDSystem" as "_Settlement.IDSystem",
            "OLD__Settlement.SettlementID" as "_Settlement.SettlementID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ReferenceID" as "ReferenceID",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ReferenceIDType",
                        "OLD"."SystemID",
                        "OLD"."_Settlement.IDSystem",
                        "OLD"."_Settlement.SettlementID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ReferenceIDType" AS "OLD_ReferenceIDType" ,
                "OLD"."SystemID" AS "OLD_SystemID" ,
                "OLD"."_Settlement.IDSystem" AS "OLD__Settlement.IDSystem" ,
                "OLD"."_Settlement.SettlementID" AS "OLD__Settlement.SettlementID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ReferenceID" AS "OLD_ReferenceID" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SettlementReferenceID" as "OLD"
            on
               "IN"."ReferenceIDType" = "OLD"."ReferenceIDType" and
               "IN"."SystemID" = "OLD"."SystemID" and
               "IN"."_Settlement.IDSystem" = "OLD"."_Settlement.IDSystem" and
               "IN"."_Settlement.SettlementID" = "OLD"."_Settlement.SettlementID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
