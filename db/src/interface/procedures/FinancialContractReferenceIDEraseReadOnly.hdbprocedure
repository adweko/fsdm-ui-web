PROCEDURE "sap.fsdm.procedures::FinancialContractReferenceIDEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FinancialContractReferenceIDTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FinancialContractReferenceIDTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FinancialContractReferenceIDTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ReferenceIDType" is null and
            "SystemID" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ReferenceIDType" ,
                "SystemID" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ReferenceIDType" ,
                "OLD"."SystemID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialContractReferenceID" "OLD"
            on
                "IN"."ReferenceIDType" = "OLD"."ReferenceIDType" and
                "IN"."SystemID" = "OLD"."SystemID" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ReferenceIDType" ,
            "SystemID" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ReferenceIDType" ,
                "OLD"."SystemID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialContractReferenceID_Historical" "OLD"
            on
                "IN"."ReferenceIDType" = "OLD"."ReferenceIDType" and
                "IN"."SystemID" = "OLD"."SystemID" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" 
        );

END
