PROCEDURE "sap.fsdm.procedures::SettlementEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::SettlementTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::SettlementTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::SettlementTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "IDSystem" is null and
            "ItemNumber" is null and
            "SettlementID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "IDSystem" ,
                "ItemNumber" ,
                "SettlementID" 
        from
        (
            select
                "OLD"."IDSystem" ,
                "OLD"."ItemNumber" ,
                "OLD"."SettlementID" 
            from :ROW "IN"
            inner join "sap.fsdm::Settlement" "OLD"
            on
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."ItemNumber" = "OLD"."ItemNumber" and
                "IN"."SettlementID" = "OLD"."SettlementID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "IDSystem" ,
            "ItemNumber" ,
            "SettlementID" 
        from
        (
            select
                "OLD"."IDSystem" ,
                "OLD"."ItemNumber" ,
                "OLD"."SettlementID" 
            from :ROW "IN"
            inner join "sap.fsdm::Settlement_Historical" "OLD"
            on
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."ItemNumber" = "OLD"."ItemNumber" and
                "IN"."SettlementID" = "OLD"."SettlementID" 
        );

END
