sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/routing/History"

], function (Controller, Filter, FilterOperator, JSONModel, History) {
    "use strict";
    return Controller.extend("sap.ui.hana.controller.TableColumns", {
        onInit: function () {
            var oRouter = this.getOwnerComponent().getRouter();
            oRouter.getRoute("tableColumns").attachPatternMatched(this._onObjectMatched, this);
        },
        _onObjectMatched: function (oEvent) {
            var oModel = new JSONModel();

            this.getView().setModel(oModel);

            var tableName = oEvent.getParameter("arguments").parameter;
            oModel.loadData("/selectData.xsjs", {name: "tableColumns", data: tableName});
            var oLabel = this.byId("pageHeader");
            oLabel.setTitle(tableName);
            sessionStorage.setItem("tableName", tableName);
        },
        onSearch: function (oEvent) {
            var aFilters = [];
            var sQuery = oEvent.getSource().getValue();
            if (sQuery && sQuery.length > 0) {
                var filter = new Filter("COLUMN_NAME", FilterOperator.Contains, sQuery);
                aFilters.push(filter);
            }
            // update list binding
            var oList = this.byId("idList");
            var oBinding = oList.getBinding("items");

            oBinding.filter(aFilters, "Application");
        },
        onSelectionChange: function (oEvent) {

            var oList = oEvent.getSource();

            var oLabel = this.byId("idFilterLabel");
            var oLabel2 = this.byId("idFilterLabel2");
            var oInfoToolbar = this.byId("idInfoToolbar");

            // With the 'getSelectedContexts' function you can access the context paths
            // of all list items that have been selected, regardless of any current
            // filter on the aggregation binding.
            var aContexts = oList.getSelectedContexts(true);

            // update UI
            var bSelected = (aContexts && aContexts.length > 0);
            var sText = (bSelected) ? aContexts.length + " selected" : null;

            console.log(aContexts.map(function (oContext) {
                return oContext.getObject().COLUMN_NAME;
            }));

            var sText2 = aContexts.map(function (oContext) {
                return oContext.getObject().COLUMN_NAME;
            }).join(", ");
            oInfoToolbar.setVisible(bSelected);
            oLabel.setText(sText);
            oLabel2.setText(sText2);
        },
        onPressLoadData: function () {
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            var tableFields = this.byId("idFilterLabel2").getText();
            oRouter.navTo("tableElements", {parameter: tableFields});
            this.byId("idFilterLabel2").setText("");
            this.byId("idFilterLabel").setText("");
        },
        onNavBack: function () {

            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = this.getOwnerComponent().getRouter();
                oRouter.navTo("tableNames", {}, true);
            }
            this.byId("idFilterLabel2").setText("");
            this.byId("idFilterLabel").setText("");
        }

    });
});