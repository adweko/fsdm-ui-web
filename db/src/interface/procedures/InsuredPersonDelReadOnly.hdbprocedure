PROCEDURE "sap.fsdm.procedures::InsuredPersonDelReadOnly" (IN ROW "sap.fsdm.tabletypes::InsuredPersonTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::InsuredPersonTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::InsuredPersonTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceOrdinalNumber=' || TO_VARCHAR("SequenceOrdinalNumber") || ' ' ||
                '_IndividualPerson.BusinessPartnerID=' || TO_VARCHAR("_IndividualPerson.BusinessPartnerID") || ' ' ||
                '_InsuranceCoverage.ID=' || TO_VARCHAR("_InsuranceCoverage.ID") || ' ' ||
                '_InsuranceCoverage._InsuranceContract.FinancialContractID=' || TO_VARCHAR("_InsuranceCoverage._InsuranceContract.FinancialContractID") || ' ' ||
                '_InsuranceCoverage._InsuranceContract.IDSystem=' || TO_VARCHAR("_InsuranceCoverage._InsuranceContract.IDSystem") || ' ' ||
                '_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID=' || TO_VARCHAR("_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID") || ' ' ||
                '_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem=' || TO_VARCHAR("_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem") || ' ' ||
                '_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceOrdinalNumber",
                        "IN"."_IndividualPerson.BusinessPartnerID",
                        "IN"."_InsuranceCoverage.ID",
                        "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "IN"."_InsuranceCoverage._InsuranceContract.IDSystem",
                        "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceOrdinalNumber",
                        "IN"."_IndividualPerson.BusinessPartnerID",
                        "IN"."_InsuranceCoverage.ID",
                        "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "IN"."_InsuranceCoverage._InsuranceContract.IDSystem",
                        "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceOrdinalNumber",
                        "_IndividualPerson.BusinessPartnerID",
                        "_InsuranceCoverage.ID",
                        "_InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "_InsuranceCoverage._InsuranceContract.IDSystem",
                        "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceOrdinalNumber",
                                    "IN"."_IndividualPerson.BusinessPartnerID",
                                    "IN"."_InsuranceCoverage.ID",
                                    "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
                                    "IN"."_InsuranceCoverage._InsuranceContract.IDSystem",
                                    "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                                    "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                                    "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceOrdinalNumber",
                                    "IN"."_IndividualPerson.BusinessPartnerID",
                                    "IN"."_InsuranceCoverage.ID",
                                    "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
                                    "IN"."_InsuranceCoverage._InsuranceContract.IDSystem",
                                    "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                                    "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                                    "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceOrdinalNumber" is null and
            "_IndividualPerson.BusinessPartnerID" is null and
            "_InsuranceCoverage.ID" is null and
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_InsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceOrdinalNumber",
            "_IndividualPerson.BusinessPartnerID",
            "_InsuranceCoverage.ID",
            "_InsuranceCoverage._InsuranceContract.FinancialContractID",
            "_InsuranceCoverage._InsuranceContract.IDSystem",
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::InsuredPerson" WHERE
            (
            "SequenceOrdinalNumber" ,
            "_IndividualPerson.BusinessPartnerID" ,
            "_InsuranceCoverage.ID" ,
            "_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuranceContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceOrdinalNumber",
            "OLD"."_IndividualPerson.BusinessPartnerID",
            "OLD"."_InsuranceCoverage.ID",
            "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem",
            "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InsuredPerson" as "OLD"
        on
                              "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
                              "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                              "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
                              "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
                              "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
                              "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                              "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                              "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceOrdinalNumber",
        "_IndividualPerson.BusinessPartnerID",
        "_InsuranceCoverage.ID",
        "_InsuranceCoverage._InsuranceContract.FinancialContractID",
        "_InsuranceCoverage._InsuranceContract.IDSystem",
        "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
        "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
        "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Address.AddressType",
        "_Address.SequenceNumber",
        "_Address.ASSOC_BankingChannel.BankingChannelID",
        "_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
        "_Address.ASSOC_OrganizationalUnit.IDSystem",
        "_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
        "_Address._Claim.ID",
        "_Address._Claim.IDSystem",
        "ActuarialAge",
        "HeightInCentimetersAtInception",
        "HeightInFeetAtInception",
        "HeightInInchesAtInception",
        "RiskGroup",
        "WeightInKilogramsAtInception",
        "WeightInPoundsAtInception",
        "WeightInStonesAtInception",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceOrdinalNumber" as "SequenceOrdinalNumber" ,
            "OLD__IndividualPerson.BusinessPartnerID" as "_IndividualPerson.BusinessPartnerID" ,
            "OLD__InsuranceCoverage.ID" as "_InsuranceCoverage.ID" ,
            "OLD__InsuranceCoverage._InsuranceContract.FinancialContractID" as "_InsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "OLD__InsuranceCoverage._InsuranceContract.IDSystem" as "_InsuranceCoverage._InsuranceContract.IDSystem" ,
            "OLD__InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "OLD__InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "OLD__InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__Address.AddressType" as "_Address.AddressType" ,
            "OLD__Address.SequenceNumber" as "_Address.SequenceNumber" ,
            "OLD__Address.ASSOC_BankingChannel.BankingChannelID" as "_Address.ASSOC_BankingChannel.BankingChannelID" ,
            "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" as "_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" as "_Address.ASSOC_OrganizationalUnit.IDSystem" ,
            "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" as "_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" as "_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "OLD__Address._Claim.ID" as "_Address._Claim.ID" ,
            "OLD__Address._Claim.IDSystem" as "_Address._Claim.IDSystem" ,
            "OLD_ActuarialAge" as "ActuarialAge" ,
            "OLD_HeightInCentimetersAtInception" as "HeightInCentimetersAtInception" ,
            "OLD_HeightInFeetAtInception" as "HeightInFeetAtInception" ,
            "OLD_HeightInInchesAtInception" as "HeightInInchesAtInception" ,
            "OLD_RiskGroup" as "RiskGroup" ,
            "OLD_WeightInKilogramsAtInception" as "WeightInKilogramsAtInception" ,
            "OLD_WeightInPoundsAtInception" as "WeightInPoundsAtInception" ,
            "OLD_WeightInStonesAtInception" as "WeightInStonesAtInception" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceOrdinalNumber",
                        "OLD"."_IndividualPerson.BusinessPartnerID",
                        "OLD"."_InsuranceCoverage.ID",
                        "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem",
                        "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceOrdinalNumber" AS "OLD_SequenceOrdinalNumber" ,
                "OLD"."_IndividualPerson.BusinessPartnerID" AS "OLD__IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InsuranceCoverage.ID" AS "OLD__InsuranceCoverage.ID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Address.AddressType" AS "OLD__Address.AddressType" ,
                "OLD"."_Address.SequenceNumber" AS "OLD__Address.SequenceNumber" ,
                "OLD"."_Address.ASSOC_BankingChannel.BankingChannelID" AS "OLD__Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.IDSystem" AS "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Address._Claim.ID" AS "OLD__Address._Claim.ID" ,
                "OLD"."_Address._Claim.IDSystem" AS "OLD__Address._Claim.IDSystem" ,
                "OLD"."ActuarialAge" AS "OLD_ActuarialAge" ,
                "OLD"."HeightInCentimetersAtInception" AS "OLD_HeightInCentimetersAtInception" ,
                "OLD"."HeightInFeetAtInception" AS "OLD_HeightInFeetAtInception" ,
                "OLD"."HeightInInchesAtInception" AS "OLD_HeightInInchesAtInception" ,
                "OLD"."RiskGroup" AS "OLD_RiskGroup" ,
                "OLD"."WeightInKilogramsAtInception" AS "OLD_WeightInKilogramsAtInception" ,
                "OLD"."WeightInPoundsAtInception" AS "OLD_WeightInPoundsAtInception" ,
                "OLD"."WeightInStonesAtInception" AS "OLD_WeightInStonesAtInception" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InsuredPerson" as "OLD"
            on
                                      "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
                                      "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                                      "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
                                      "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
                                      "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
                                      "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                                      "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                                      "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceOrdinalNumber" as "SequenceOrdinalNumber",
            "OLD__IndividualPerson.BusinessPartnerID" as "_IndividualPerson.BusinessPartnerID",
            "OLD__InsuranceCoverage.ID" as "_InsuranceCoverage.ID",
            "OLD__InsuranceCoverage._InsuranceContract.FinancialContractID" as "_InsuranceCoverage._InsuranceContract.FinancialContractID",
            "OLD__InsuranceCoverage._InsuranceContract.IDSystem" as "_InsuranceCoverage._InsuranceContract.IDSystem",
            "OLD__InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" as "_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
            "OLD__InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" as "_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
            "OLD__InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" as "_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Address.AddressType" as "_Address.AddressType",
            "OLD__Address.SequenceNumber" as "_Address.SequenceNumber",
            "OLD__Address.ASSOC_BankingChannel.BankingChannelID" as "_Address.ASSOC_BankingChannel.BankingChannelID",
            "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" as "_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" as "_Address.ASSOC_OrganizationalUnit.IDSystem",
            "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" as "_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" as "_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD__Address._Claim.ID" as "_Address._Claim.ID",
            "OLD__Address._Claim.IDSystem" as "_Address._Claim.IDSystem",
            "OLD_ActuarialAge" as "ActuarialAge",
            "OLD_HeightInCentimetersAtInception" as "HeightInCentimetersAtInception",
            "OLD_HeightInFeetAtInception" as "HeightInFeetAtInception",
            "OLD_HeightInInchesAtInception" as "HeightInInchesAtInception",
            "OLD_RiskGroup" as "RiskGroup",
            "OLD_WeightInKilogramsAtInception" as "WeightInKilogramsAtInception",
            "OLD_WeightInPoundsAtInception" as "WeightInPoundsAtInception",
            "OLD_WeightInStonesAtInception" as "WeightInStonesAtInception",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceOrdinalNumber",
                        "OLD"."_IndividualPerson.BusinessPartnerID",
                        "OLD"."_InsuranceCoverage.ID",
                        "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID",
                        "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem",
                        "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID",
                        "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem",
                        "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceOrdinalNumber" AS "OLD_SequenceOrdinalNumber" ,
                "OLD"."_IndividualPerson.BusinessPartnerID" AS "OLD__IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InsuranceCoverage.ID" AS "OLD__InsuranceCoverage.ID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" AS "OLD__InsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" AS "OLD__InsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" AS "OLD__InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" AS "OLD__InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" AS "OLD__InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_Address.AddressType" AS "OLD__Address.AddressType" ,
                "OLD"."_Address.SequenceNumber" AS "OLD__Address.SequenceNumber" ,
                "OLD"."_Address.ASSOC_BankingChannel.BankingChannelID" AS "OLD__Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD__Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.IDSystem" AS "OLD__Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD__Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD__Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_Address.ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD__Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Address._Claim.ID" AS "OLD__Address._Claim.ID" ,
                "OLD"."_Address._Claim.IDSystem" AS "OLD__Address._Claim.IDSystem" ,
                "OLD"."ActuarialAge" AS "OLD_ActuarialAge" ,
                "OLD"."HeightInCentimetersAtInception" AS "OLD_HeightInCentimetersAtInception" ,
                "OLD"."HeightInFeetAtInception" AS "OLD_HeightInFeetAtInception" ,
                "OLD"."HeightInInchesAtInception" AS "OLD_HeightInInchesAtInception" ,
                "OLD"."RiskGroup" AS "OLD_RiskGroup" ,
                "OLD"."WeightInKilogramsAtInception" AS "OLD_WeightInKilogramsAtInception" ,
                "OLD"."WeightInPoundsAtInception" AS "OLD_WeightInPoundsAtInception" ,
                "OLD"."WeightInStonesAtInception" AS "OLD_WeightInStonesAtInception" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InsuredPerson" as "OLD"
            on
               "IN"."SequenceOrdinalNumber" = "OLD"."SequenceOrdinalNumber" and
               "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
               "IN"."_InsuranceCoverage.ID" = "OLD"."_InsuranceCoverage.ID" and
               "IN"."_InsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuranceContract.FinancialContractID" and
               "IN"."_InsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuranceContract.IDSystem" and
               "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
               "IN"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
               "IN"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
