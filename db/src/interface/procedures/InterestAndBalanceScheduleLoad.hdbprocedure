PROCEDURE "sap.fsdm.procedures::InterestAndBalanceScheduleLoad" (IN ROW "sap.fsdm.tabletypes::InterestAndBalanceScheduleTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AssetOrLiability=' || TO_VARCHAR("AssetOrLiability") || ' ' ||
                'KeyDate=' || TO_VARCHAR("KeyDate") || ' ' ||
                'Scenario=' || TO_VARCHAR("Scenario") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_TimeBucket.MaturityBandID=' || TO_VARCHAR("_TimeBucket.MaturityBandID") || ' ' ||
                '_TimeBucket.TimeBucketID=' || TO_VARCHAR("_TimeBucket.TimeBucketID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."KeyDate",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."KeyDate",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AssetOrLiability",
                        "KeyDate",
                        "Scenario",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_TimeBucket.MaturityBandID",
                        "_TimeBucket.TimeBucketID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AssetOrLiability",
                                    "IN"."KeyDate",
                                    "IN"."Scenario",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AssetOrLiability",
                                    "IN"."KeyDate",
                                    "IN"."Scenario",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_TimeBucket.MaturityBandID",
                                    "IN"."_TimeBucket.TimeBucketID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::InterestAndBalanceSchedule" (
        "AssetOrLiability",
        "KeyDate",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmortizationAmount",
        "CumulatedInterestAmount",
        "CumulatedPresentValueOfAmortizationAmount",
        "CumulatedPresentValueOfInterestAmount",
        "Currency",
        "DiscountInterestRate",
        "InterestAmount",
        "NominalInterestRate",
        "PresentValueOfAmortizationAmount",
        "PresentValueOfInterestAmount",
        "PrincipalAmount",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AssetOrLiability" as "AssetOrLiability" ,
            "OLD_KeyDate" as "KeyDate" ,
            "OLD_Scenario" as "Scenario" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID" ,
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AmortizationAmount" as "AmortizationAmount" ,
            "OLD_CumulatedInterestAmount" as "CumulatedInterestAmount" ,
            "OLD_CumulatedPresentValueOfAmortizationAmount" as "CumulatedPresentValueOfAmortizationAmount" ,
            "OLD_CumulatedPresentValueOfInterestAmount" as "CumulatedPresentValueOfInterestAmount" ,
            "OLD_Currency" as "Currency" ,
            "OLD_DiscountInterestRate" as "DiscountInterestRate" ,
            "OLD_InterestAmount" as "InterestAmount" ,
            "OLD_NominalInterestRate" as "NominalInterestRate" ,
            "OLD_PresentValueOfAmortizationAmount" as "PresentValueOfAmortizationAmount" ,
            "OLD_PresentValueOfInterestAmount" as "PresentValueOfInterestAmount" ,
            "OLD_PrincipalAmount" as "PrincipalAmount" ,
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate" ,
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."KeyDate",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."AssetOrLiability" as "OLD_AssetOrLiability",
                                "OLD"."KeyDate" as "OLD_KeyDate",
                                "OLD"."Scenario" as "OLD_Scenario",
                                "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                                "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                                "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AmortizationAmount" as "OLD_AmortizationAmount",
                                "OLD"."CumulatedInterestAmount" as "OLD_CumulatedInterestAmount",
                                "OLD"."CumulatedPresentValueOfAmortizationAmount" as "OLD_CumulatedPresentValueOfAmortizationAmount",
                                "OLD"."CumulatedPresentValueOfInterestAmount" as "OLD_CumulatedPresentValueOfInterestAmount",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."DiscountInterestRate" as "OLD_DiscountInterestRate",
                                "OLD"."InterestAmount" as "OLD_InterestAmount",
                                "OLD"."NominalInterestRate" as "OLD_NominalInterestRate",
                                "OLD"."PresentValueOfAmortizationAmount" as "OLD_PresentValueOfAmortizationAmount",
                                "OLD"."PresentValueOfInterestAmount" as "OLD_PresentValueOfInterestAmount",
                                "OLD"."PrincipalAmount" as "OLD_PrincipalAmount",
                                "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                                "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::InterestAndBalanceSchedule" as "OLD"
            on
                ifnull( "IN"."AssetOrLiability", '') = "OLD"."AssetOrLiability" and
                ifnull( "IN"."KeyDate", '0001-01-01') = "OLD"."KeyDate" and
                ifnull( "IN"."Scenario", '') = "OLD"."Scenario" and
                ifnull( "IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '') = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '') = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::InterestAndBalanceSchedule" (
        "AssetOrLiability",
        "KeyDate",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmortizationAmount",
        "CumulatedInterestAmount",
        "CumulatedPresentValueOfAmortizationAmount",
        "CumulatedPresentValueOfInterestAmount",
        "Currency",
        "DiscountInterestRate",
        "InterestAmount",
        "NominalInterestRate",
        "PresentValueOfAmortizationAmount",
        "PresentValueOfInterestAmount",
        "PrincipalAmount",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AssetOrLiability" as "AssetOrLiability",
            "OLD_KeyDate" as "KeyDate",
            "OLD_Scenario" as "Scenario",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__TimeBucket.MaturityBandID" as "_TimeBucket.MaturityBandID",
            "OLD__TimeBucket.TimeBucketID" as "_TimeBucket.TimeBucketID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AmortizationAmount" as "AmortizationAmount",
            "OLD_CumulatedInterestAmount" as "CumulatedInterestAmount",
            "OLD_CumulatedPresentValueOfAmortizationAmount" as "CumulatedPresentValueOfAmortizationAmount",
            "OLD_CumulatedPresentValueOfInterestAmount" as "CumulatedPresentValueOfInterestAmount",
            "OLD_Currency" as "Currency",
            "OLD_DiscountInterestRate" as "DiscountInterestRate",
            "OLD_InterestAmount" as "InterestAmount",
            "OLD_NominalInterestRate" as "NominalInterestRate",
            "OLD_PresentValueOfAmortizationAmount" as "PresentValueOfAmortizationAmount",
            "OLD_PresentValueOfInterestAmount" as "PresentValueOfInterestAmount",
            "OLD_PrincipalAmount" as "PrincipalAmount",
            "OLD_TimeBucketEndDate" as "TimeBucketEndDate",
            "OLD_TimeBucketStartDate" as "TimeBucketStartDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."AssetOrLiability",
                        "IN"."KeyDate",
                        "IN"."Scenario",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_TimeBucket.MaturityBandID",
                        "IN"."_TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."AssetOrLiability" as "OLD_AssetOrLiability",
                        "OLD"."KeyDate" as "OLD_KeyDate",
                        "OLD"."Scenario" as "OLD_Scenario",
                        "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                        "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_TimeBucket.MaturityBandID" as "OLD__TimeBucket.MaturityBandID",
                        "OLD"."_TimeBucket.TimeBucketID" as "OLD__TimeBucket.TimeBucketID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."AmortizationAmount" as "OLD_AmortizationAmount",
                        "OLD"."CumulatedInterestAmount" as "OLD_CumulatedInterestAmount",
                        "OLD"."CumulatedPresentValueOfAmortizationAmount" as "OLD_CumulatedPresentValueOfAmortizationAmount",
                        "OLD"."CumulatedPresentValueOfInterestAmount" as "OLD_CumulatedPresentValueOfInterestAmount",
                        "OLD"."Currency" as "OLD_Currency",
                        "OLD"."DiscountInterestRate" as "OLD_DiscountInterestRate",
                        "OLD"."InterestAmount" as "OLD_InterestAmount",
                        "OLD"."NominalInterestRate" as "OLD_NominalInterestRate",
                        "OLD"."PresentValueOfAmortizationAmount" as "OLD_PresentValueOfAmortizationAmount",
                        "OLD"."PresentValueOfInterestAmount" as "OLD_PresentValueOfInterestAmount",
                        "OLD"."PrincipalAmount" as "OLD_PrincipalAmount",
                        "OLD"."TimeBucketEndDate" as "OLD_TimeBucketEndDate",
                        "OLD"."TimeBucketStartDate" as "OLD_TimeBucketStartDate",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::InterestAndBalanceSchedule" as "OLD"
            on
                ifnull( "IN"."AssetOrLiability", '' ) = "OLD"."AssetOrLiability" and
                ifnull( "IN"."KeyDate", '0001-01-01' ) = "OLD"."KeyDate" and
                ifnull( "IN"."Scenario", '' ) = "OLD"."Scenario" and
                ifnull( "IN"."_Collection.CollectionID", '' ) = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '' ) = "OLD"."_Collection.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_TimeBucket.MaturityBandID", '' ) = "OLD"."_TimeBucket.MaturityBandID" and
                ifnull( "IN"."_TimeBucket.TimeBucketID", '' ) = "OLD"."_TimeBucket.TimeBucketID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::InterestAndBalanceSchedule"
    where (
        "AssetOrLiability",
        "KeyDate",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."AssetOrLiability",
            "OLD"."KeyDate",
            "OLD"."Scenario",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_TimeBucket.MaturityBandID",
            "OLD"."_TimeBucket.TimeBucketID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InterestAndBalanceSchedule" as "OLD"
        on
           ifnull( "IN"."AssetOrLiability", '' ) = "OLD"."AssetOrLiability" and
           ifnull( "IN"."KeyDate", '0001-01-01' ) = "OLD"."KeyDate" and
           ifnull( "IN"."Scenario", '' ) = "OLD"."Scenario" and
           ifnull( "IN"."_Collection.CollectionID", '' ) = "OLD"."_Collection.CollectionID" and
           ifnull( "IN"."_Collection.IDSystem", '' ) = "OLD"."_Collection.IDSystem" and
           ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
           ifnull( "IN"."_TimeBucket.MaturityBandID", '' ) = "OLD"."_TimeBucket.MaturityBandID" and
           ifnull( "IN"."_TimeBucket.TimeBucketID", '' ) = "OLD"."_TimeBucket.TimeBucketID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::InterestAndBalanceSchedule" (
        "AssetOrLiability",
        "KeyDate",
        "Scenario",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_TimeBucket.MaturityBandID",
        "_TimeBucket.TimeBucketID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmortizationAmount",
        "CumulatedInterestAmount",
        "CumulatedPresentValueOfAmortizationAmount",
        "CumulatedPresentValueOfInterestAmount",
        "Currency",
        "DiscountInterestRate",
        "InterestAmount",
        "NominalInterestRate",
        "PresentValueOfAmortizationAmount",
        "PresentValueOfInterestAmount",
        "PrincipalAmount",
        "TimeBucketEndDate",
        "TimeBucketStartDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "AssetOrLiability", '' ) as "AssetOrLiability",
            ifnull( "KeyDate", '0001-01-01' ) as "KeyDate",
            ifnull( "Scenario", '' ) as "Scenario",
            ifnull( "_Collection.CollectionID", '' ) as "_Collection.CollectionID",
            ifnull( "_Collection.IDSystem", '' ) as "_Collection.IDSystem",
            ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
            ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
            ifnull( "_TimeBucket.MaturityBandID", '' ) as "_TimeBucket.MaturityBandID",
            ifnull( "_TimeBucket.TimeBucketID", '' ) as "_TimeBucket.TimeBucketID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "AmortizationAmount"  ,
            "CumulatedInterestAmount"  ,
            "CumulatedPresentValueOfAmortizationAmount"  ,
            "CumulatedPresentValueOfInterestAmount"  ,
            "Currency"  ,
            "DiscountInterestRate"  ,
            "InterestAmount"  ,
            "NominalInterestRate"  ,
            "PresentValueOfAmortizationAmount"  ,
            "PresentValueOfInterestAmount"  ,
            "PrincipalAmount"  ,
            "TimeBucketEndDate"  ,
            "TimeBucketStartDate"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END