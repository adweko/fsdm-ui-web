namespace "sap"."fsdm";

using "sap"."fsdm"::"BusinessPartner";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"OrganizationalUnit";
using "sap"."fsdm"::"Settlement";

entity "ContributionMargin" {
    key "Category"                                                                             : String(40)                                                   default '';
    key "PeriodFrequency"                                                                      : String(40)                                                   default '';
    key "PeriodStartDate"                                                                      : LocalDate                                                    default '0001-01-01';
    key "RoleOfCurrency"                                                                       : String(40)                                                   default '';
    key "_BusinessPartner"                                                                     : association to BusinessPartner { BusinessPartnerID }         not null;
    key "_FinancialContract"                                                                   : association to FinancialContract {
                                                                                                                                    FinancialContractID,
                                                                                                                                    IDSystem
                                                                                                                                  }                           not null;
    key "_FinancialInstrument"                                                                 : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_OrganizationalUnit"                                                                  : association to OrganizationalUnit {
                                                                                                                                     IDSystem,
                                                                                                                                     OrganizationalUnitID,
                                                                                                                                     ASSOC_OrganizationHostingOrganizationalUnit
                                                                                                                                   }                          not null;
    key "_SecuritiesAccount"                                                                   : association to FinancialContract {
                                                                                                                                    FinancialContractID,
                                                                                                                                    IDSystem
                                                                                                                                  }                           not null;
    key "_SettlementItem"                                                                      : association to Settlement {
                                                                                                                             IDSystem,
                                                                                                                             ItemNumber,
                                                                                                                             SettlementID
                                                                                                                           }                                  not null;
    key "BusinessValidFrom"                                                                    : LocalDate;
    key "BusinessValidTo"                                                                      : LocalDate;
        "SystemValidFrom"                                                                      : UTCTimestamp                                                 not null;
        "SystemValidTo"                                                                        : UTCTimestamp                                                 not null;
        "AmortizedCostRealizedResults"                                                         : Decimal(34, 6);
        "BankGuaranteeStandardRiskCosts"                                                       : Decimal(34, 6);
        "BankLevy"                                                                             : Decimal(34, 6);
        "CapitalCosts"                                                                         : Decimal(34, 6);
        "ChangeInEquityResults"                                                                : Decimal(34, 6);
        "CommissionExpense"                                                                    : Decimal(34, 6);
        "CommissionIncome"                                                                     : Decimal(34, 6);
        "CoveredLiquidityCosts"                                                                : Decimal(34, 6);
        "CreditCommitmentFeeIncome"                                                            : Decimal(34, 6);
        "CreditCommitmentLiquidityCosts"                                                       : Decimal(34, 6);
        "CreditCommitmentStandardRiskCosts"                                                    : Decimal(34, 6);
        "CurrencyCode"                                                                         : String(3);
        "DebtInstrumentProductClearingResult"                                                  : Decimal(34, 6);
        "DelayedCoverageCosts"                                                                 : Decimal(34, 6);
        "DerivativesSalesMargin"                                                               : Decimal(34, 6);
        "DiscountedFundingLoss"                                                                : Decimal(34, 6);
        "EarlyRepaymentChargesFromUnscheduledRepayment"                                        : Decimal(34, 6);
        "FairValueRealizedResults"                                                             : Decimal(34, 6);
        "FairValueUnrealizedResults"                                                           : Decimal(34, 6);
        "FeeExpense"                                                                           : Decimal(34, 6);
        "FeeIncome"                                                                            : Decimal(34, 6);
        "FundingAdvantage"                                                                     : Decimal(34, 6);
        "FundingLossFromUnscheduledRepayment"                                                  : Decimal(34, 6);
        "InterestExpense"                                                                      : Decimal(34, 6);
        "InterestIncome"                                                                       : Decimal(34, 6);
        "InterestLikeIncome"                                                                   : Decimal(34, 6);
        "LiabilityLiquidityCosts"                                                              : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Fair Value Unrealized Results
    *@ReplacedBy: Fair Value Unrealized Results
    */
    "LoanFairValueThroughProfitLossChange"                                                     : Decimal(34, 6);
        "LoanStandardRiskCosts"                                                                : Decimal(34, 6);
        "OpportunityExpense"                                                                   : Decimal(34, 6);
        "OpportunityIncome"                                                                    : Decimal(34, 6);
        "OtherStandardRiskCosts"                                                               : Decimal(34, 6);
        "ParticipationInvestmentLiquidityCosts"                                                : Decimal(34, 6);
        "ParticipationInvestmentMarketInterest"                                                : Decimal(34, 6);
        "PeriodEndDate"                                                                        : LocalDate;
        "ProfitDistributionIncome"                                                             : Decimal(34, 6);
        "ProvisionOfCollateralCosts"                                                           : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Fair Value Realized Results
    *@ReplacedBy: Fair Value Realized Results
    */
    "RealizedFinancialAssetResults"                                                            : Decimal(34, 6);
        "ReallocatedBankLevy"                                                                  : Decimal(34, 6);
        "ReallocatedCapitalCosts"                                                              : Decimal(34, 6);
        "ReallocatedChangeInEquityResults"                                                     : Decimal(34, 6);
        "ReallocatedComissionSurplusResults"                                                   : Decimal(34, 6);
        "ReallocatedDisposalOrReclassificationOfAssets"                                        : Decimal(34, 6);
        "ReallocatedFairValueRealizedResults"                                                  : Decimal(34, 6);
        "ReallocatedFairValueUnrealizedResults"                                                : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Unrealized or Realized Results
    *@ReplacedBy: Reallocated Fair Value Unrealized or Realized Results
    */
    "ReallocatedFinancialAssetResults"                                                         : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Unrealized or Realized Results
    *@ReplacedBy: Reallocated Fair Value Unrealized or Realized Results
    */
    "ReallocatedFinancialInstrumentsAndFinancialContractsFairValueThroughProfitAndLossResults" : Decimal(34, 6);
        "ReallocatedIFRSStage3LoanLossProvisionResults"                                        : Decimal(34, 6);
        "ReallocatedInterestSurplusCoreBusinessResults"                                        : Decimal(34, 6);
        "ReallocatedInterestSurplusNonCoreBusinessResults"                                     : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Unrealized or Realized Results
    *@ReplacedBy: Reallocated Fair Value Unrealized or Realized Results
    */
    "ReallocatedLoansFairValueChangeFromFairValueThroughProfitLoss"                            : Decimal(34, 6);
        "ReallocatedPurchasedOrOriginatedCreditImpairedLoanLossProvisionResults"               : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Realized or Unrealized Results
    *@ReplacedBy: Reallocated Fair Value Realized or Unrealized Results
    */
    "ReallocatedSecuritiesFairValueProfitLoss"                                                 : Decimal(34, 6);
        "ReallocatedSecuritiesRiskProvisioning"                                                : Decimal(34, 6);
        "ReallocatedStandardProcessingCosts"                                                   : Decimal(34, 6);
        "ReallocatedStandardRiskCosts"                                                         : Decimal(34, 6);
        "ReallocatedTax"                                                                       : Decimal(34, 6);
        "ReclassificationFromAmortizedCostResults"                                             : Decimal(34, 6);
        "ReclassificationFromFairValueOtherComprehensiveIncomeResults"                         : Decimal(34, 6);
        "SalesMarginDerivatives"                                                               : Decimal(34, 6);
        "SecuritiesLiquidityCosts"                                                             : Decimal(34, 6);
        "SecuritiesMarketRateContribution"                                                     : Decimal(34, 6);
        "SecuritiesRiskProvisioning"                                                           : Decimal(34, 6);
        "StandardProcessingCosts"                                                              : Decimal(34, 6);
        "Tax"                                                                                  : Decimal(34, 6);
        "UncoveredLiquidityCosts"                                                              : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Fair Value Unrealized Results
    *@ReplacedBy: Fair Value Unrealized Results
    */
    "UnrealizedFinancialAssetResults"                                                          : Decimal(34, 6);
        "SourceSystemID"                                                                       : String(128);
        "ChangeTimestampInSourceSystem"                                                        : UTCTimestamp;
        "ChangingUserInSourceSystem"                                                           : String(128);
        "ChangingProcessType"                                                                  : String(40);
        "ChangingProcessID"                                                                    : String(128);
}
technical configuration {
    column store;
};

entity "ContributionMargin_Historical" {
    "Category"                                                                                 : String(40)                                                   default '' not null;
    "PeriodFrequency"                                                                          : String(40)                                                   default '' not null;
    "PeriodStartDate"                                                                          : LocalDate                                                    default '0001-01-01' not null;
    "RoleOfCurrency"                                                                           : String(40)                                                   default '' not null;
    "_BusinessPartner"                                                                         : association to BusinessPartner { BusinessPartnerID }         not null;
    "_FinancialContract"                                                                       : association to FinancialContract {
                                                                                                                                    FinancialContractID,
                                                                                                                                    IDSystem
                                                                                                                                  }                           not null;
    "_FinancialInstrument"                                                                     : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_OrganizationalUnit"                                                                      : association to OrganizationalUnit {
                                                                                                                                     IDSystem,
                                                                                                                                     OrganizationalUnitID,
                                                                                                                                     ASSOC_OrganizationHostingOrganizationalUnit
                                                                                                                                   }                          not null;
    "_SecuritiesAccount"                                                                       : association to FinancialContract {
                                                                                                                                    FinancialContractID,
                                                                                                                                    IDSystem
                                                                                                                                  }                           not null;
    "_SettlementItem"                                                                          : association to Settlement {
                                                                                                                             IDSystem,
                                                                                                                             ItemNumber,
                                                                                                                             SettlementID
                                                                                                                           }                                  not null;
    "BusinessValidFrom"                                                                        : LocalDate                                                    not null;
    "BusinessValidTo"                                                                          : LocalDate                                                    not null;
    "SystemValidFrom"                                                                          : UTCTimestamp                                                 not null;
    "SystemValidTo"                                                                            : UTCTimestamp                                                 not null;
    "AmortizedCostRealizedResults"                                                             : Decimal(34, 6);
    "BankGuaranteeStandardRiskCosts"                                                           : Decimal(34, 6);
    "BankLevy"                                                                                 : Decimal(34, 6);
    "CapitalCosts"                                                                             : Decimal(34, 6);
    "ChangeInEquityResults"                                                                    : Decimal(34, 6);
    "CommissionExpense"                                                                        : Decimal(34, 6);
    "CommissionIncome"                                                                         : Decimal(34, 6);
    "CoveredLiquidityCosts"                                                                    : Decimal(34, 6);
    "CreditCommitmentFeeIncome"                                                                : Decimal(34, 6);
    "CreditCommitmentLiquidityCosts"                                                           : Decimal(34, 6);
    "CreditCommitmentStandardRiskCosts"                                                        : Decimal(34, 6);
    "CurrencyCode"                                                                             : String(3);
    "DebtInstrumentProductClearingResult"                                                      : Decimal(34, 6);
    "DelayedCoverageCosts"                                                                     : Decimal(34, 6);
    "DerivativesSalesMargin"                                                                   : Decimal(34, 6);
    "DiscountedFundingLoss"                                                                    : Decimal(34, 6);
    "EarlyRepaymentChargesFromUnscheduledRepayment"                                            : Decimal(34, 6);
    "FairValueRealizedResults"                                                                 : Decimal(34, 6);
    "FairValueUnrealizedResults"                                                               : Decimal(34, 6);
    "FeeExpense"                                                                               : Decimal(34, 6);
    "FeeIncome"                                                                                : Decimal(34, 6);
    "FundingAdvantage"                                                                         : Decimal(34, 6);
    "FundingLossFromUnscheduledRepayment"                                                      : Decimal(34, 6);
    "InterestExpense"                                                                          : Decimal(34, 6);
    "InterestIncome"                                                                           : Decimal(34, 6);
    "InterestLikeIncome"                                                                       : Decimal(34, 6);
    "LiabilityLiquidityCosts"                                                                  : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Fair Value Unrealized Results
    *@ReplacedBy: Fair Value Unrealized Results
    */
    "LoanFairValueThroughProfitLossChange"                                                     : Decimal(34, 6);
    "LoanStandardRiskCosts"                                                                    : Decimal(34, 6);
    "OpportunityExpense"                                                                       : Decimal(34, 6);
    "OpportunityIncome"                                                                        : Decimal(34, 6);
    "OtherStandardRiskCosts"                                                                   : Decimal(34, 6);
    "ParticipationInvestmentLiquidityCosts"                                                    : Decimal(34, 6);
    "ParticipationInvestmentMarketInterest"                                                    : Decimal(34, 6);
    "PeriodEndDate"                                                                            : LocalDate;
    "ProfitDistributionIncome"                                                                 : Decimal(34, 6);
    "ProvisionOfCollateralCosts"                                                               : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Fair Value Realized Results
    *@ReplacedBy: Fair Value Realized Results
    */
    "RealizedFinancialAssetResults"                                                            : Decimal(34, 6);
    "ReallocatedBankLevy"                                                                      : Decimal(34, 6);
    "ReallocatedCapitalCosts"                                                                  : Decimal(34, 6);
    "ReallocatedChangeInEquityResults"                                                         : Decimal(34, 6);
    "ReallocatedComissionSurplusResults"                                                       : Decimal(34, 6);
    "ReallocatedDisposalOrReclassificationOfAssets"                                            : Decimal(34, 6);
    "ReallocatedFairValueRealizedResults"                                                      : Decimal(34, 6);
    "ReallocatedFairValueUnrealizedResults"                                                    : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Unrealized or Realized Results
    *@ReplacedBy: Reallocated Fair Value Unrealized or Realized Results
    */
    "ReallocatedFinancialAssetResults"                                                         : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Unrealized or Realized Results
    *@ReplacedBy: Reallocated Fair Value Unrealized or Realized Results
    */
    "ReallocatedFinancialInstrumentsAndFinancialContractsFairValueThroughProfitAndLossResults" : Decimal(34, 6);
    "ReallocatedIFRSStage3LoanLossProvisionResults"                                            : Decimal(34, 6);
    "ReallocatedInterestSurplusCoreBusinessResults"                                            : Decimal(34, 6);
    "ReallocatedInterestSurplusNonCoreBusinessResults"                                         : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Unrealized or Realized Results
    *@ReplacedBy: Reallocated Fair Value Unrealized or Realized Results
    */
    "ReallocatedLoansFairValueChangeFromFairValueThroughProfitLoss"                            : Decimal(34, 6);
    "ReallocatedPurchasedOrOriginatedCreditImpairedLoanLossProvisionResults"                   : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Reallocated Fair Value Realized or Unrealized Results
    *@ReplacedBy: Reallocated Fair Value Realized or Unrealized Results
    */
    "ReallocatedSecuritiesFairValueProfitLoss"                                                 : Decimal(34, 6);
    "ReallocatedSecuritiesRiskProvisioning"                                                    : Decimal(34, 6);
    "ReallocatedStandardProcessingCosts"                                                       : Decimal(34, 6);
    "ReallocatedStandardRiskCosts"                                                             : Decimal(34, 6);
    "ReallocatedTax"                                                                           : Decimal(34, 6);
    "ReclassificationFromAmortizedCostResults"                                                 : Decimal(34, 6);
    "ReclassificationFromFairValueOtherComprehensiveIncomeResults"                             : Decimal(34, 6);
    "SalesMarginDerivatives"                                                                   : Decimal(34, 6);
    "SecuritiesLiquidityCosts"                                                                 : Decimal(34, 6);
    "SecuritiesMarketRateContribution"                                                         : Decimal(34, 6);
    "SecuritiesRiskProvisioning"                                                               : Decimal(34, 6);
    "StandardProcessingCosts"                                                                  : Decimal(34, 6);
    "Tax"                                                                                      : Decimal(34, 6);
    "UncoveredLiquidityCosts"                                                                  : Decimal(34, 6);
    /**
    *@Deprecated
    *@Reason: Replaced by Fair Value Unrealized Results
    *@ReplacedBy: Fair Value Unrealized Results
    */
    "UnrealizedFinancialAssetResults"                                                          : Decimal(34, 6);
    "SourceSystemID"                                                                           : String(128);
    "ChangeTimestampInSourceSystem"                                                            : UTCTimestamp;
    "ChangingUserInSourceSystem"                                                               : String(128);
    "ChangingProcessType"                                                                      : String(40);
    "ChangingProcessID"                                                                        : String(128);
}
technical configuration {
    column store;
};