PROCEDURE "sap.fsdm.procedures::AccountingClassificationStatusErase" (IN ROW "sap.fsdm.tabletypes::AccountingClassificationStatusTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountingClassificationCategory" is null and
            "LotID" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::AccountingClassificationStatus"
        WHERE
        (            "AccountingClassificationCategory" ,
            "LotID" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" 
        ) in
        (
            select                 "OLD"."AccountingClassificationCategory" ,
                "OLD"."LotID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::AccountingClassificationStatus" "OLD"
            on
            "IN"."AccountingClassificationCategory" = "OLD"."AccountingClassificationCategory" and
            "IN"."LotID" = "OLD"."LotID" and
            "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
            "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
            "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
            "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
            "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
            "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::AccountingClassificationStatus_Historical"
        WHERE
        (
            "AccountingClassificationCategory" ,
            "LotID" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" 
        ) in
        (
            select
                "OLD"."AccountingClassificationCategory" ,
                "OLD"."LotID" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::AccountingClassificationStatus_Historical" "OLD"
            on
                "IN"."AccountingClassificationCategory" = "OLD"."AccountingClassificationCategory" and
                "IN"."LotID" = "OLD"."LotID" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

END
