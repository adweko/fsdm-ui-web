PROCEDURE "sap.fsdm.procedures::ValueAtRiskBacktestingInterimResultErase" (IN ROW "sap.fsdm.tabletypes::ValueAtRiskBacktestingInterimResultTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ProfitAndLossValueDate" is null and
            "ProfitAndLossValueType" is null and
            "RoleOfCurrency" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ValueAtRiskBacktestingInterimResult"
        WHERE
        (            "ProfitAndLossValueDate" ,
            "ProfitAndLossValueType" ,
            "RoleOfCurrency" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" 
        ) in
        (
            select                 "OLD"."ProfitAndLossValueDate" ,
                "OLD"."ProfitAndLossValueType" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" 
            from :ROW "IN"
            inner join "sap.fsdm::ValueAtRiskBacktestingInterimResult" "OLD"
            on
            "IN"."ProfitAndLossValueDate" = "OLD"."ProfitAndLossValueDate" and
            "IN"."ProfitAndLossValueType" = "OLD"."ProfitAndLossValueType" and
            "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
            "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
            "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ValueAtRiskBacktestingInterimResult_Historical"
        WHERE
        (
            "ProfitAndLossValueDate" ,
            "ProfitAndLossValueType" ,
            "RoleOfCurrency" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" 
        ) in
        (
            select
                "OLD"."ProfitAndLossValueDate" ,
                "OLD"."ProfitAndLossValueType" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" 
            from :ROW "IN"
            inner join "sap.fsdm::ValueAtRiskBacktestingInterimResult_Historical" "OLD"
            on
                "IN"."ProfitAndLossValueDate" = "OLD"."ProfitAndLossValueDate" and
                "IN"."ProfitAndLossValueType" = "OLD"."ProfitAndLossValueType" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" 
        );

END
