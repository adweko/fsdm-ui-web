PROCEDURE "sap.fsdm.procedures::RatingEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RatingTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RatingTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RatingTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "IsFxRating" is null and
            "RatingAgency" is null and
            "RatingCategory" is null and
            "RatingMethod" is null and
            "RatingStatus" is null and
            "TimeHorizon" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_GeographicalRegion.GeographicalStructureID" is null and
            "ASSOC_GeographicalRegion.GeographicalUnitID" is null and
            "_PhysicalAsset.PhysicalAssetID" is null and
            "_Security.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "IsFxRating" ,
                "RatingAgency" ,
                "RatingCategory" ,
                "RatingMethod" ,
                "RatingStatus" ,
                "TimeHorizon" ,
                "ASSOC_BusinessPartner.BusinessPartnerID" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "_PhysicalAsset.PhysicalAssetID" ,
                "_Security.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."IsFxRating" ,
                "OLD"."RatingAgency" ,
                "OLD"."RatingCategory" ,
                "OLD"."RatingMethod" ,
                "OLD"."RatingStatus" ,
                "OLD"."TimeHorizon" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Security.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Rating" "OLD"
            on
                "IN"."IsFxRating" = "OLD"."IsFxRating" and
                "IN"."RatingAgency" = "OLD"."RatingAgency" and
                "IN"."RatingCategory" = "OLD"."RatingCategory" and
                "IN"."RatingMethod" = "OLD"."RatingMethod" and
                "IN"."RatingStatus" = "OLD"."RatingStatus" and
                "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" and
                "IN"."ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" and
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_Security.FinancialInstrumentID" = "OLD"."_Security.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "IsFxRating" ,
            "RatingAgency" ,
            "RatingCategory" ,
            "RatingMethod" ,
            "RatingStatus" ,
            "TimeHorizon" ,
            "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_GeographicalRegion.GeographicalStructureID" ,
            "ASSOC_GeographicalRegion.GeographicalUnitID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "_Security.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."IsFxRating" ,
                "OLD"."RatingAgency" ,
                "OLD"."RatingCategory" ,
                "OLD"."RatingMethod" ,
                "OLD"."RatingStatus" ,
                "OLD"."TimeHorizon" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_Security.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Rating_Historical" "OLD"
            on
                "IN"."IsFxRating" = "OLD"."IsFxRating" and
                "IN"."RatingAgency" = "OLD"."RatingAgency" and
                "IN"."RatingCategory" = "OLD"."RatingCategory" and
                "IN"."RatingMethod" = "OLD"."RatingMethod" and
                "IN"."RatingStatus" = "OLD"."RatingStatus" and
                "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_GeographicalRegion.GeographicalStructureID" = "OLD"."ASSOC_GeographicalRegion.GeographicalStructureID" and
                "IN"."ASSOC_GeographicalRegion.GeographicalUnitID" = "OLD"."ASSOC_GeographicalRegion.GeographicalUnitID" and
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_Security.FinancialInstrumentID" = "OLD"."_Security.FinancialInstrumentID" 
        );

END
