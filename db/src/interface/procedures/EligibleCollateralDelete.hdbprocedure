PROCEDURE "sap.fsdm.procedures::EligibleCollateralDelete" (IN ROW "sap.fsdm.tabletypes::EligibleCollateralTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'Scheme=' || TO_VARCHAR("Scheme") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_InstrumentClass.InstrumentClass=' || TO_VARCHAR("_InstrumentClass.InstrumentClass") || ' ' ||
                '_InstrumentClass.InstrumentClassificationSystem=' || TO_VARCHAR("_InstrumentClass.InstrumentClassificationSystem") || ' ' ||
                '_MasterAgreement.FinancialContractID=' || TO_VARCHAR("_MasterAgreement.FinancialContractID") || ' ' ||
                '_MasterAgreement.IDSystem=' || TO_VARCHAR("_MasterAgreement.IDSystem") || ' ' ||
                '_Organization.BusinessPartnerID=' || TO_VARCHAR("_Organization.BusinessPartnerID") || ' ' ||
                '_ProductCatalogItem.ProductCatalogItem=' || TO_VARCHAR("_ProductCatalogItem.ProductCatalogItem") || ' ' ||
                '_ProductCatalogItem._ProductCatalog.CatalogID=' || TO_VARCHAR("_ProductCatalogItem._ProductCatalog.CatalogID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."Scheme",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InstrumentClass.InstrumentClass",
                        "IN"."_InstrumentClass.InstrumentClassificationSystem",
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_Organization.BusinessPartnerID",
                        "IN"."_ProductCatalogItem.ProductCatalogItem",
                        "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."Scheme",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_InstrumentClass.InstrumentClass",
                        "IN"."_InstrumentClass.InstrumentClassificationSystem",
                        "IN"."_MasterAgreement.FinancialContractID",
                        "IN"."_MasterAgreement.IDSystem",
                        "IN"."_Organization.BusinessPartnerID",
                        "IN"."_ProductCatalogItem.ProductCatalogItem",
                        "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "Scheme",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_InstrumentClass.InstrumentClass",
                        "_InstrumentClass.InstrumentClassificationSystem",
                        "_MasterAgreement.FinancialContractID",
                        "_MasterAgreement.IDSystem",
                        "_Organization.BusinessPartnerID",
                        "_ProductCatalogItem.ProductCatalogItem",
                        "_ProductCatalogItem._ProductCatalog.CatalogID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."Scheme",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InstrumentClass.InstrumentClass",
                                    "IN"."_InstrumentClass.InstrumentClassificationSystem",
                                    "IN"."_MasterAgreement.FinancialContractID",
                                    "IN"."_MasterAgreement.IDSystem",
                                    "IN"."_Organization.BusinessPartnerID",
                                    "IN"."_ProductCatalogItem.ProductCatalogItem",
                                    "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."Scheme",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_InstrumentClass.InstrumentClass",
                                    "IN"."_InstrumentClass.InstrumentClassificationSystem",
                                    "IN"."_MasterAgreement.FinancialContractID",
                                    "IN"."_MasterAgreement.IDSystem",
                                    "IN"."_Organization.BusinessPartnerID",
                                    "IN"."_ProductCatalogItem.ProductCatalogItem",
                                    "IN"."_ProductCatalogItem._ProductCatalog.CatalogID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "Scheme" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_InstrumentClass.InstrumentClass" is null and
            "_InstrumentClass.InstrumentClassificationSystem" is null and
            "_MasterAgreement.FinancialContractID" is null and
            "_MasterAgreement.IDSystem" is null and
            "_Organization.BusinessPartnerID" is null and
            "_ProductCatalogItem.ProductCatalogItem" is null and
            "_ProductCatalogItem._ProductCatalog.CatalogID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::EligibleCollateral" (
        "Scheme",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InstrumentClass.InstrumentClass",
        "_InstrumentClass.InstrumentClassificationSystem",
        "_MasterAgreement.FinancialContractID",
        "_MasterAgreement.IDSystem",
        "_Organization.BusinessPartnerID",
        "_ProductCatalogItem.ProductCatalogItem",
        "_ProductCatalogItem._ProductCatalog.CatalogID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "EligibilityStatus",
        "Haircut",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Scheme" as "Scheme" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__InstrumentClass.InstrumentClass" as "_InstrumentClass.InstrumentClass" ,
            "OLD__InstrumentClass.InstrumentClassificationSystem" as "_InstrumentClass.InstrumentClassificationSystem" ,
            "OLD__MasterAgreement.FinancialContractID" as "_MasterAgreement.FinancialContractID" ,
            "OLD__MasterAgreement.IDSystem" as "_MasterAgreement.IDSystem" ,
            "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID" ,
            "OLD__ProductCatalogItem.ProductCatalogItem" as "_ProductCatalogItem.ProductCatalogItem" ,
            "OLD__ProductCatalogItem._ProductCatalog.CatalogID" as "_ProductCatalogItem._ProductCatalog.CatalogID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_EligibilityStatus" as "EligibilityStatus" ,
            "OLD_Haircut" as "Haircut" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."Scheme",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InstrumentClass.InstrumentClass",
                        "OLD"."_InstrumentClass.InstrumentClassificationSystem",
                        "OLD"."_MasterAgreement.FinancialContractID",
                        "OLD"."_MasterAgreement.IDSystem",
                        "OLD"."_Organization.BusinessPartnerID",
                        "OLD"."_ProductCatalogItem.ProductCatalogItem",
                        "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."Scheme" AS "OLD_Scheme" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InstrumentClass.InstrumentClass" AS "OLD__InstrumentClass.InstrumentClass" ,
                "OLD"."_InstrumentClass.InstrumentClassificationSystem" AS "OLD__InstrumentClass.InstrumentClassificationSystem" ,
                "OLD"."_MasterAgreement.FinancialContractID" AS "OLD__MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" AS "OLD__MasterAgreement.IDSystem" ,
                "OLD"."_Organization.BusinessPartnerID" AS "OLD__Organization.BusinessPartnerID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" AS "OLD__ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" AS "OLD__ProductCatalogItem._ProductCatalog.CatalogID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."EligibilityStatus" AS "OLD_EligibilityStatus" ,
                "OLD"."Haircut" AS "OLD_Haircut" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EligibleCollateral" as "OLD"
            on
                      "IN"."Scheme" = "OLD"."Scheme" and
                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                      "IN"."_InstrumentClass.InstrumentClass" = "OLD"."_InstrumentClass.InstrumentClass" and
                      "IN"."_InstrumentClass.InstrumentClassificationSystem" = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                      "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                      "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                      "IN"."_Organization.BusinessPartnerID" = "OLD"."_Organization.BusinessPartnerID" and
                      "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                      "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::EligibleCollateral" (
        "Scheme",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InstrumentClass.InstrumentClass",
        "_InstrumentClass.InstrumentClassificationSystem",
        "_MasterAgreement.FinancialContractID",
        "_MasterAgreement.IDSystem",
        "_Organization.BusinessPartnerID",
        "_ProductCatalogItem.ProductCatalogItem",
        "_ProductCatalogItem._ProductCatalog.CatalogID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "EligibilityStatus",
        "Haircut",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_Scheme" as "Scheme",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__InstrumentClass.InstrumentClass" as "_InstrumentClass.InstrumentClass",
            "OLD__InstrumentClass.InstrumentClassificationSystem" as "_InstrumentClass.InstrumentClassificationSystem",
            "OLD__MasterAgreement.FinancialContractID" as "_MasterAgreement.FinancialContractID",
            "OLD__MasterAgreement.IDSystem" as "_MasterAgreement.IDSystem",
            "OLD__Organization.BusinessPartnerID" as "_Organization.BusinessPartnerID",
            "OLD__ProductCatalogItem.ProductCatalogItem" as "_ProductCatalogItem.ProductCatalogItem",
            "OLD__ProductCatalogItem._ProductCatalog.CatalogID" as "_ProductCatalogItem._ProductCatalog.CatalogID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_EligibilityStatus" as "EligibilityStatus",
            "OLD_Haircut" as "Haircut",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."Scheme",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_InstrumentClass.InstrumentClass",
                        "OLD"."_InstrumentClass.InstrumentClassificationSystem",
                        "OLD"."_MasterAgreement.FinancialContractID",
                        "OLD"."_MasterAgreement.IDSystem",
                        "OLD"."_Organization.BusinessPartnerID",
                        "OLD"."_ProductCatalogItem.ProductCatalogItem",
                        "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."Scheme" AS "OLD_Scheme" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_InstrumentClass.InstrumentClass" AS "OLD__InstrumentClass.InstrumentClass" ,
                "OLD"."_InstrumentClass.InstrumentClassificationSystem" AS "OLD__InstrumentClass.InstrumentClassificationSystem" ,
                "OLD"."_MasterAgreement.FinancialContractID" AS "OLD__MasterAgreement.FinancialContractID" ,
                "OLD"."_MasterAgreement.IDSystem" AS "OLD__MasterAgreement.IDSystem" ,
                "OLD"."_Organization.BusinessPartnerID" AS "OLD__Organization.BusinessPartnerID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" AS "OLD__ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" AS "OLD__ProductCatalogItem._ProductCatalog.CatalogID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."EligibilityStatus" AS "OLD_EligibilityStatus" ,
                "OLD"."Haircut" AS "OLD_Haircut" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EligibleCollateral" as "OLD"
            on
                                                "IN"."Scheme" = "OLD"."Scheme" and
                                                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                                "IN"."_InstrumentClass.InstrumentClass" = "OLD"."_InstrumentClass.InstrumentClass" and
                                                "IN"."_InstrumentClass.InstrumentClassificationSystem" = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                                                "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                                                "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                                                "IN"."_Organization.BusinessPartnerID" = "OLD"."_Organization.BusinessPartnerID" and
                                                "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                                                "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::EligibleCollateral"
    where (
        "Scheme",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_InstrumentClass.InstrumentClass",
        "_InstrumentClass.InstrumentClassificationSystem",
        "_MasterAgreement.FinancialContractID",
        "_MasterAgreement.IDSystem",
        "_Organization.BusinessPartnerID",
        "_ProductCatalogItem.ProductCatalogItem",
        "_ProductCatalogItem._ProductCatalog.CatalogID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."Scheme",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_InstrumentClass.InstrumentClass",
            "OLD"."_InstrumentClass.InstrumentClassificationSystem",
            "OLD"."_MasterAgreement.FinancialContractID",
            "OLD"."_MasterAgreement.IDSystem",
            "OLD"."_Organization.BusinessPartnerID",
            "OLD"."_ProductCatalogItem.ProductCatalogItem",
            "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::EligibleCollateral" as "OLD"
        on
                                       "IN"."Scheme" = "OLD"."Scheme" and
                                       "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                       "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                       "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                       "IN"."_InstrumentClass.InstrumentClass" = "OLD"."_InstrumentClass.InstrumentClass" and
                                       "IN"."_InstrumentClass.InstrumentClassificationSystem" = "OLD"."_InstrumentClass.InstrumentClassificationSystem" and
                                       "IN"."_MasterAgreement.FinancialContractID" = "OLD"."_MasterAgreement.FinancialContractID" and
                                       "IN"."_MasterAgreement.IDSystem" = "OLD"."_MasterAgreement.IDSystem" and
                                       "IN"."_Organization.BusinessPartnerID" = "OLD"."_Organization.BusinessPartnerID" and
                                       "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                                       "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
