PROCEDURE "sap.fsdm.procedures::BarrierErase" (IN ROW "sap.fsdm.tabletypes::BarrierTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BarrierType" is null and
            "BarrierValidityStartDate" is null and
            "_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" is null and
            "_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" is null and
            "_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::Barrier"
        WHERE
        (            "BarrierType" ,
            "BarrierValidityStartDate" ,
            "_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" ,
            "_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" ,
            "_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" 
        ) in
        (
            select                 "OLD"."BarrierType" ,
                "OLD"."BarrierValidityStartDate" ,
                "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" ,
                "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" ,
                "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::Barrier" "OLD"
            on
            "IN"."BarrierType" = "OLD"."BarrierType" and
            "IN"."BarrierValidityStartDate" = "OLD"."BarrierValidityStartDate" and
            "IN"."_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" and
            "IN"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" = "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" and
            "IN"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
            "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

        --delete data from history table
        delete from "sap.fsdm::Barrier_Historical"
        WHERE
        (
            "BarrierType" ,
            "BarrierValidityStartDate" ,
            "_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" ,
            "_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" ,
            "_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" 
        ) in
        (
            select
                "OLD"."BarrierType" ,
                "OLD"."BarrierValidityStartDate" ,
                "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" ,
                "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" ,
                "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            from :ROW "IN"
            inner join "sap.fsdm::Barrier_Historical" "OLD"
            on
                "IN"."BarrierType" = "OLD"."BarrierType" and
                "IN"."BarrierValidityStartDate" = "OLD"."BarrierValidityStartDate" and
                "IN"."_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._EquityLinkedInstrument.FinancialInstrumentID" and
                "IN"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" = "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingIndex.IndexID" and
                "IN"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" = "OLD"."_EquityLinkedInstrumentUnderlyingAssignment._UnderlyingInstrument.FinancialInstrumentID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        );

END
