PROCEDURE "sap.fsdm.procedures::PartnerRelationClassificationDelete" (IN ROW "sap.fsdm.tabletypes::PartnerRelationClassificationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PartnerRelationClass=' || TO_VARCHAR("PartnerRelationClass") || ' ' ||
                'PartnerRelationClassificationSystem=' || TO_VARCHAR("PartnerRelationClassificationSystem") || ' ' ||
                'ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType=' || TO_VARCHAR("ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType") || ' ' ||
                'ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID") || ' ' ||
                'ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PartnerRelationClass",
                        "IN"."PartnerRelationClassificationSystem",
                        "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                        "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                        "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PartnerRelationClass",
                        "IN"."PartnerRelationClassificationSystem",
                        "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                        "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                        "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PartnerRelationClass",
                        "PartnerRelationClassificationSystem",
                        "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                        "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                        "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PartnerRelationClass",
                                    "IN"."PartnerRelationClassificationSystem",
                                    "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                                    "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                                    "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PartnerRelationClass",
                                    "IN"."PartnerRelationClassificationSystem",
                                    "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                                    "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                                    "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PartnerRelationClass" is null and
            "PartnerRelationClassificationSystem" is null and
            "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" is null and
            "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" is null and
            "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::PartnerRelationClassification" (
        "PartnerRelationClass",
        "PartnerRelationClassificationSystem",
        "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
        "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PartnerRelationClassDescription",
        "PartnerRelationClassificationSystemDescription",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PartnerRelationClass" as "PartnerRelationClass" ,
            "OLD_PartnerRelationClassificationSystem" as "PartnerRelationClassificationSystem" ,
            "OLD_ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" as "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" ,
            "OLD_ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
            "OLD_ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_PartnerRelationClassDescription" as "PartnerRelationClassDescription" ,
            "OLD_PartnerRelationClassificationSystemDescription" as "PartnerRelationClassificationSystemDescription" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PartnerRelationClass",
                        "OLD"."PartnerRelationClassificationSystem",
                        "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                        "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                        "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PartnerRelationClass" AS "OLD_PartnerRelationClass" ,
                "OLD"."PartnerRelationClassificationSystem" AS "OLD_PartnerRelationClassificationSystem" ,
                "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" AS "OLD_ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" ,
                "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PartnerRelationClassDescription" AS "OLD_PartnerRelationClassDescription" ,
                "OLD"."PartnerRelationClassificationSystemDescription" AS "OLD_PartnerRelationClassificationSystemDescription" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PartnerRelationClassification" as "OLD"
            on
                      "IN"."PartnerRelationClass" = "OLD"."PartnerRelationClass" and
                      "IN"."PartnerRelationClassificationSystem" = "OLD"."PartnerRelationClassificationSystem" and
                      "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" = "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" and
                      "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" and
                      "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PartnerRelationClassification" (
        "PartnerRelationClass",
        "PartnerRelationClassificationSystem",
        "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
        "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PartnerRelationClassDescription",
        "PartnerRelationClassificationSystemDescription",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PartnerRelationClass" as "PartnerRelationClass",
            "OLD_PartnerRelationClassificationSystem" as "PartnerRelationClassificationSystem",
            "OLD_ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" as "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
            "OLD_ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
            "OLD_ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" as "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PartnerRelationClassDescription" as "PartnerRelationClassDescription",
            "OLD_PartnerRelationClassificationSystemDescription" as "PartnerRelationClassificationSystemDescription",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PartnerRelationClass",
                        "OLD"."PartnerRelationClassificationSystem",
                        "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
                        "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
                        "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PartnerRelationClass" AS "OLD_PartnerRelationClass" ,
                "OLD"."PartnerRelationClassificationSystem" AS "OLD_PartnerRelationClassificationSystem" ,
                "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" AS "OLD_ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" ,
                "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PartnerRelationClassDescription" AS "OLD_PartnerRelationClassDescription" ,
                "OLD"."PartnerRelationClassificationSystemDescription" AS "OLD_PartnerRelationClassificationSystemDescription" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PartnerRelationClassification" as "OLD"
            on
                                                "IN"."PartnerRelationClass" = "OLD"."PartnerRelationClass" and
                                                "IN"."PartnerRelationClassificationSystem" = "OLD"."PartnerRelationClassificationSystem" and
                                                "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" = "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" and
                                                "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" and
                                                "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::PartnerRelationClassification"
    where (
        "PartnerRelationClass",
        "PartnerRelationClassificationSystem",
        "ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
        "ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
        "ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PartnerRelationClass",
            "OLD"."PartnerRelationClassificationSystem",
            "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType",
            "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID",
            "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PartnerRelationClassification" as "OLD"
        on
                                       "IN"."PartnerRelationClass" = "OLD"."PartnerRelationClass" and
                                       "IN"."PartnerRelationClassificationSystem" = "OLD"."PartnerRelationClassificationSystem" and
                                       "IN"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" = "OLD"."ASSOC_BusinessPartnerRelation.BusinessPartnerRelationType" and
                                       "IN"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_SourceInBusinessPartnerRelation.BusinessPartnerID" and
                                       "IN"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartnerRelation.ASSOC_TargetInBusinessPartnerRelation.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
