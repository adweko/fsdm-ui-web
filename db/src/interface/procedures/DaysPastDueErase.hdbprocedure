PROCEDURE "sap.fsdm.procedures::DaysPastDueErase" (IN ROW "sap.fsdm.tabletypes::DaysPastDueTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PastDueType" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" is null and
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" is null and
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_FinancialInstrumentDaysPastDue.FinancialInstrumentID" is null and
            "_TimeBucket.MaturityBandID" is null and
            "_TimeBucket.TimeBucketID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::DaysPastDue"
        WHERE
        (            "PastDueType" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_FinancialInstrumentDaysPastDue.FinancialInstrumentID" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" 
        ) in
        (
            select                 "OLD"."PastDueType" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" ,
                "OLD"."_TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" 
            from :ROW "IN"
            inner join "sap.fsdm::DaysPastDue" "OLD"
            on
            "IN"."PastDueType" = "OLD"."PastDueType" and
            "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
            "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
            "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
            "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
            "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
            "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
            "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" = "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" and
            "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
            "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        );

        --delete data from history table
        delete from "sap.fsdm::DaysPastDue_Historical"
        WHERE
        (
            "PastDueType" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
            "ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_FinancialInstrumentDaysPastDue.FinancialInstrumentID" ,
            "_TimeBucket.MaturityBandID" ,
            "_TimeBucket.TimeBucketID" 
        ) in
        (
            select
                "OLD"."PastDueType" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" ,
                "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" ,
                "OLD"."_TimeBucket.MaturityBandID" ,
                "OLD"."_TimeBucket.TimeBucketID" 
            from :ROW "IN"
            inner join "sap.fsdm::DaysPastDue_Historical" "OLD"
            on
                "IN"."PastDueType" = "OLD"."PastDueType" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.PositionCurrency" and
                "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.FinancialContractID" and
                "IN"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" = "OLD"."ASSOC_PositionCurrencyOfMultiCurrencyContract.ASSOC_MultiCcyAccnt.IDSystem" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" = "OLD"."_FinancialInstrumentDaysPastDue.FinancialInstrumentID" and
                "IN"."_TimeBucket.MaturityBandID" = "OLD"."_TimeBucket.MaturityBandID" and
                "IN"."_TimeBucket.TimeBucketID" = "OLD"."_TimeBucket.TimeBucketID" 
        );

END
