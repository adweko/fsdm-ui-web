PROCEDURE "sap.fsdm.procedures::TotalReturnSwapReturnLegDelete" (IN ROW "sap.fsdm.tabletypes::TotalReturnSwapReturnLegTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Swap.FinancialContractID=' || TO_VARCHAR("_Swap.FinancialContractID") || ' ' ||
                '_Swap.IDSystem=' || TO_VARCHAR("_Swap.IDSystem") || ' ' ||
                '_Underlying.FinancialInstrumentID=' || TO_VARCHAR("_Underlying.FinancialInstrumentID") || ' ' ||
                '_UnderlyingIndex.IndexID=' || TO_VARCHAR("_UnderlyingIndex.IndexID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Swap.FinancialContractID",
                        "IN"."_Swap.IDSystem",
                        "IN"."_Underlying.FinancialInstrumentID",
                        "IN"."_UnderlyingIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Swap.FinancialContractID",
                        "IN"."_Swap.IDSystem",
                        "IN"."_Underlying.FinancialInstrumentID",
                        "IN"."_UnderlyingIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Swap.FinancialContractID",
                        "_Swap.IDSystem",
                        "_Underlying.FinancialInstrumentID",
                        "_UnderlyingIndex.IndexID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Swap.FinancialContractID",
                                    "IN"."_Swap.IDSystem",
                                    "IN"."_Underlying.FinancialInstrumentID",
                                    "IN"."_UnderlyingIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Swap.FinancialContractID",
                                    "IN"."_Swap.IDSystem",
                                    "IN"."_Underlying.FinancialInstrumentID",
                                    "IN"."_UnderlyingIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Swap.FinancialContractID" is null and
            "_Swap.IDSystem" is null and
            "_Underlying.FinancialInstrumentID" is null and
            "_UnderlyingIndex.IndexID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::TotalReturnSwapReturnLeg" (
        "_Swap.FinancialContractID",
        "_Swap.IDSystem",
        "_Underlying.FinancialInstrumentID",
        "_UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Swap.FinancialContractID" as "_Swap.FinancialContractID" ,
            "OLD__Swap.IDSystem" as "_Swap.IDSystem" ,
            "OLD__Underlying.FinancialInstrumentID" as "_Underlying.FinancialInstrumentID" ,
            "OLD__UnderlyingIndex.IndexID" as "_UnderlyingIndex.IndexID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Swap.FinancialContractID",
                        "OLD"."_Swap.IDSystem",
                        "OLD"."_Underlying.FinancialInstrumentID",
                        "OLD"."_UnderlyingIndex.IndexID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_Swap.FinancialContractID" AS "OLD__Swap.FinancialContractID" ,
                "OLD"."_Swap.IDSystem" AS "OLD__Swap.IDSystem" ,
                "OLD"."_Underlying.FinancialInstrumentID" AS "OLD__Underlying.FinancialInstrumentID" ,
                "OLD"."_UnderlyingIndex.IndexID" AS "OLD__UnderlyingIndex.IndexID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TotalReturnSwapReturnLeg" as "OLD"
            on
                      "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                      "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" and
                      "IN"."_Underlying.FinancialInstrumentID" = "OLD"."_Underlying.FinancialInstrumentID" and
                      "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::TotalReturnSwapReturnLeg" (
        "_Swap.FinancialContractID",
        "_Swap.IDSystem",
        "_Underlying.FinancialInstrumentID",
        "_UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__Swap.FinancialContractID" as "_Swap.FinancialContractID",
            "OLD__Swap.IDSystem" as "_Swap.IDSystem",
            "OLD__Underlying.FinancialInstrumentID" as "_Underlying.FinancialInstrumentID",
            "OLD__UnderlyingIndex.IndexID" as "_UnderlyingIndex.IndexID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Swap.FinancialContractID",
                        "OLD"."_Swap.IDSystem",
                        "OLD"."_Underlying.FinancialInstrumentID",
                        "OLD"."_UnderlyingIndex.IndexID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Swap.FinancialContractID" AS "OLD__Swap.FinancialContractID" ,
                "OLD"."_Swap.IDSystem" AS "OLD__Swap.IDSystem" ,
                "OLD"."_Underlying.FinancialInstrumentID" AS "OLD__Underlying.FinancialInstrumentID" ,
                "OLD"."_UnderlyingIndex.IndexID" AS "OLD__UnderlyingIndex.IndexID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TotalReturnSwapReturnLeg" as "OLD"
            on
                                                "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                                                "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" and
                                                "IN"."_Underlying.FinancialInstrumentID" = "OLD"."_Underlying.FinancialInstrumentID" and
                                                "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::TotalReturnSwapReturnLeg"
    where (
        "_Swap.FinancialContractID",
        "_Swap.IDSystem",
        "_Underlying.FinancialInstrumentID",
        "_UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_Swap.FinancialContractID",
            "OLD"."_Swap.IDSystem",
            "OLD"."_Underlying.FinancialInstrumentID",
            "OLD"."_UnderlyingIndex.IndexID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TotalReturnSwapReturnLeg" as "OLD"
        on
                                       "IN"."_Swap.FinancialContractID" = "OLD"."_Swap.FinancialContractID" and
                                       "IN"."_Swap.IDSystem" = "OLD"."_Swap.IDSystem" and
                                       "IN"."_Underlying.FinancialInstrumentID" = "OLD"."_Underlying.FinancialInstrumentID" and
                                       "IN"."_UnderlyingIndex.IndexID" = "OLD"."_UnderlyingIndex.IndexID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
