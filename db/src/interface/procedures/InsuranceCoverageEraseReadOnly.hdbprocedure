PROCEDURE "sap.fsdm.procedures::InsuranceCoverageEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InsuranceCoverageTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InsuranceCoverageTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InsuranceCoverageTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_InsuranceContract.FinancialContractID" is null and
            "_InsuranceContract.IDSystem" is null and
            "_InsuredObject._FinancialContract.FinancialContractID" is null and
            "_InsuredObject._FinancialContract.IDSystem" is null and
            "_InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "ID" ,
                "_InsuranceContract.FinancialContractID" ,
                "_InsuranceContract.IDSystem" ,
                "_InsuredObject._FinancialContract.FinancialContractID" ,
                "_InsuredObject._FinancialContract.IDSystem" ,
                "_InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."ID" ,
                "OLD"."_InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceContract.IDSystem" ,
                "OLD"."_InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InsuranceCoverage" "OLD"
            on
                "IN"."ID" = "OLD"."ID" and
                "IN"."_InsuranceContract.FinancialContractID" = "OLD"."_InsuranceContract.FinancialContractID" and
                "IN"."_InsuranceContract.IDSystem" = "OLD"."_InsuranceContract.IDSystem" and
                "IN"."_InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuredObject._FinancialContract.IDSystem" and
                "IN"."_InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "ID" ,
            "_InsuranceContract.FinancialContractID" ,
            "_InsuranceContract.IDSystem" ,
            "_InsuredObject._FinancialContract.FinancialContractID" ,
            "_InsuredObject._FinancialContract.IDSystem" ,
            "_InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."ID" ,
                "OLD"."_InsuranceContract.FinancialContractID" ,
                "OLD"."_InsuranceContract.IDSystem" ,
                "OLD"."_InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InsuranceCoverage_Historical" "OLD"
            on
                "IN"."ID" = "OLD"."ID" and
                "IN"."_InsuranceContract.FinancialContractID" = "OLD"."_InsuranceContract.FinancialContractID" and
                "IN"."_InsuranceContract.IDSystem" = "OLD"."_InsuranceContract.IDSystem" and
                "IN"."_InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_InsuredObject._FinancialContract.IDSystem" = "OLD"."_InsuredObject._FinancialContract.IDSystem" and
                "IN"."_InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
