PROCEDURE "sap.fsdm.procedures::RepoAnalysisEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RepoAnalysisTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RepoAnalysisTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RepoAnalysisTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "LiquidityRiskAnalysisType" is null and
            "LiquidityRiskResultType" is null and
            "LiquidityRiskSplitPartType" is null and
            "RiskProvisionScenario" is null and
            "RoleOfCurrency" is null and
            "_DynamicTimeBucket.MaturityBandID" is null and
            "_DynamicTimeBucket.TimeBucketID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_ResultGroup.ResultDataProvider" is null and
            "_ResultGroup.ResultGroupID" is null and
            "_RiskReportingNode.RiskReportingNodeID" is null and
            "_SecuritiesAccount.FinancialContractID" is null and
            "_SecuritiesAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "LiquidityRiskAnalysisType" ,
                "LiquidityRiskResultType" ,
                "LiquidityRiskSplitPartType" ,
                "RiskProvisionScenario" ,
                "RoleOfCurrency" ,
                "_DynamicTimeBucket.MaturityBandID" ,
                "_DynamicTimeBucket.TimeBucketID" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_FinancialInstrument.FinancialInstrumentID" ,
                "_ResultGroup.ResultDataProvider" ,
                "_ResultGroup.ResultGroupID" ,
                "_RiskReportingNode.RiskReportingNodeID" ,
                "_SecuritiesAccount.FinancialContractID" ,
                "_SecuritiesAccount.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."LiquidityRiskAnalysisType" ,
                "OLD"."LiquidityRiskResultType" ,
                "OLD"."LiquidityRiskSplitPartType" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_DynamicTimeBucket.MaturityBandID" ,
                "OLD"."_DynamicTimeBucket.TimeBucketID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RepoAnalysis" "OLD"
            on
                "IN"."LiquidityRiskAnalysisType" = "OLD"."LiquidityRiskAnalysisType" and
                "IN"."LiquidityRiskResultType" = "OLD"."LiquidityRiskResultType" and
                "IN"."LiquidityRiskSplitPartType" = "OLD"."LiquidityRiskSplitPartType" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_DynamicTimeBucket.MaturityBandID" = "OLD"."_DynamicTimeBucket.MaturityBandID" and
                "IN"."_DynamicTimeBucket.TimeBucketID" = "OLD"."_DynamicTimeBucket.TimeBucketID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "LiquidityRiskAnalysisType" ,
            "LiquidityRiskResultType" ,
            "LiquidityRiskSplitPartType" ,
            "RiskProvisionScenario" ,
            "RoleOfCurrency" ,
            "_DynamicTimeBucket.MaturityBandID" ,
            "_DynamicTimeBucket.TimeBucketID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_RiskReportingNode.RiskReportingNodeID" ,
            "_SecuritiesAccount.FinancialContractID" ,
            "_SecuritiesAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."LiquidityRiskAnalysisType" ,
                "OLD"."LiquidityRiskResultType" ,
                "OLD"."LiquidityRiskSplitPartType" ,
                "OLD"."RiskProvisionScenario" ,
                "OLD"."RoleOfCurrency" ,
                "OLD"."_DynamicTimeBucket.MaturityBandID" ,
                "OLD"."_DynamicTimeBucket.TimeBucketID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_ResultGroup.ResultDataProvider" ,
                "OLD"."_ResultGroup.ResultGroupID" ,
                "OLD"."_RiskReportingNode.RiskReportingNodeID" ,
                "OLD"."_SecuritiesAccount.FinancialContractID" ,
                "OLD"."_SecuritiesAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RepoAnalysis_Historical" "OLD"
            on
                "IN"."LiquidityRiskAnalysisType" = "OLD"."LiquidityRiskAnalysisType" and
                "IN"."LiquidityRiskResultType" = "OLD"."LiquidityRiskResultType" and
                "IN"."LiquidityRiskSplitPartType" = "OLD"."LiquidityRiskSplitPartType" and
                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                "IN"."RoleOfCurrency" = "OLD"."RoleOfCurrency" and
                "IN"."_DynamicTimeBucket.MaturityBandID" = "OLD"."_DynamicTimeBucket.MaturityBandID" and
                "IN"."_DynamicTimeBucket.TimeBucketID" = "OLD"."_DynamicTimeBucket.TimeBucketID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                "IN"."_ResultGroup.ResultDataProvider" = "OLD"."_ResultGroup.ResultDataProvider" and
                "IN"."_ResultGroup.ResultGroupID" = "OLD"."_ResultGroup.ResultGroupID" and
                "IN"."_RiskReportingNode.RiskReportingNodeID" = "OLD"."_RiskReportingNode.RiskReportingNodeID" and
                "IN"."_SecuritiesAccount.FinancialContractID" = "OLD"."_SecuritiesAccount.FinancialContractID" and
                "IN"."_SecuritiesAccount.IDSystem" = "OLD"."_SecuritiesAccount.IDSystem" 
        );

END
