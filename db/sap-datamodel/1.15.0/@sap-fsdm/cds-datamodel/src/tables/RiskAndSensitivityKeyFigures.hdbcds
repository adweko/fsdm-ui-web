namespace "sap"."fsdm";

using "sap"."fsdm"::"Collection";

entity "RiskAndSensitivityKeyFigures" {
    key "AssetOrLiability"              : String(10);
    key "Category"                      : String(40);
    key "KeyDate"                       : LocalDate;
    key "Provider"                      : String(256);
    key "Scenario"                      : String(100)                 default '';
    key "_Collection"                   : association to Collection {
                                                                      CollectionID,
                                                                      IDSystem
                                                                    } not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                not null;
        "SystemValidTo"                 : UTCTimestamp                not null;
        "CalculationMethod"             : String(20);
        "Description"                   : String(256);
        "MacaulayDuration"              : Decimal(34, 6);
        "MacaulayDurationTimeUnit"      : String(128);
        "ModifiedDuration"              : Decimal(34, 6);
        "ModifiedDurationTimeUnit"      : String(128);
        "Name"                          : String(256);
        "WeightedAverageLife"           : Decimal(34, 6);
        "WeightedAverageLifeTimeUnit"   : String(128);
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "RiskAndSensitivityKeyFigures_Historical" {
    "AssetOrLiability"              : String(10)                  not null;
    "Category"                      : String(40)                  not null;
    "KeyDate"                       : LocalDate                   not null;
    "Provider"                      : String(256)                 not null;
    "Scenario"                      : String(100)                 default '' not null;
    "_Collection"                   : association to Collection {
                                                                  CollectionID,
                                                                  IDSystem
                                                                } not null;
    "BusinessValidFrom"             : LocalDate                   not null;
    "BusinessValidTo"               : LocalDate                   not null;
    "SystemValidFrom"               : UTCTimestamp                not null;
    "SystemValidTo"                 : UTCTimestamp                not null;
    "CalculationMethod"             : String(20);
    "Description"                   : String(256);
    "MacaulayDuration"              : Decimal(34, 6);
    "MacaulayDurationTimeUnit"      : String(128);
    "ModifiedDuration"              : Decimal(34, 6);
    "ModifiedDurationTimeUnit"      : String(128);
    "Name"                          : String(256);
    "WeightedAverageLife"           : Decimal(34, 6);
    "WeightedAverageLifeTimeUnit"   : String(128);
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};