PROCEDURE "sap.fsdm.procedures::RiskFactorInterdependenceMatrixLoad" (IN ROW "sap.fsdm.tabletypes::RiskFactorInterdependenceMatrixTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ColumnRiskFactorType=' || TO_VARCHAR("ColumnRiskFactorType") || ' ' ||
                'ColumnRiskFactorTypeValue=' || TO_VARCHAR("ColumnRiskFactorTypeValue") || ' ' ||
                'MatrixCategory=' || TO_VARCHAR("MatrixCategory") || ' ' ||
                'MatrixID=' || TO_VARCHAR("MatrixID") || ' ' ||
                'RowRiskFactorType=' || TO_VARCHAR("RowRiskFactorType") || ' ' ||
                'RowRiskFactorTypeValue=' || TO_VARCHAR("RowRiskFactorTypeValue") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ColumnRiskFactorType",
                        "IN"."ColumnRiskFactorTypeValue",
                        "IN"."MatrixCategory",
                        "IN"."MatrixID",
                        "IN"."RowRiskFactorType",
                        "IN"."RowRiskFactorTypeValue"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ColumnRiskFactorType",
                        "IN"."ColumnRiskFactorTypeValue",
                        "IN"."MatrixCategory",
                        "IN"."MatrixID",
                        "IN"."RowRiskFactorType",
                        "IN"."RowRiskFactorTypeValue"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ColumnRiskFactorType",
                        "ColumnRiskFactorTypeValue",
                        "MatrixCategory",
                        "MatrixID",
                        "RowRiskFactorType",
                        "RowRiskFactorTypeValue"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ColumnRiskFactorType",
                                    "IN"."ColumnRiskFactorTypeValue",
                                    "IN"."MatrixCategory",
                                    "IN"."MatrixID",
                                    "IN"."RowRiskFactorType",
                                    "IN"."RowRiskFactorTypeValue"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ColumnRiskFactorType",
                                    "IN"."ColumnRiskFactorTypeValue",
                                    "IN"."MatrixCategory",
                                    "IN"."MatrixID",
                                    "IN"."RowRiskFactorType",
                                    "IN"."RowRiskFactorTypeValue"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::RiskFactorInterdependenceMatrix" (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationMethod",
        "DataProvider",
        "Description",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ColumnRiskFactorType" as "ColumnRiskFactorType" ,
            "OLD_ColumnRiskFactorTypeValue" as "ColumnRiskFactorTypeValue" ,
            "OLD_MatrixCategory" as "MatrixCategory" ,
            "OLD_MatrixID" as "MatrixID" ,
            "OLD_RowRiskFactorType" as "RowRiskFactorType" ,
            "OLD_RowRiskFactorTypeValue" as "RowRiskFactorTypeValue" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_Amount" as "Amount" ,
            "OLD_CalculationMethod" as "CalculationMethod" ,
            "OLD_DataProvider" as "DataProvider" ,
            "OLD_Description" as "Description" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ColumnRiskFactorType",
                        "IN"."ColumnRiskFactorTypeValue",
                        "IN"."MatrixCategory",
                        "IN"."MatrixID",
                        "IN"."RowRiskFactorType",
                        "IN"."RowRiskFactorTypeValue",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ColumnRiskFactorType" as "OLD_ColumnRiskFactorType",
                                "OLD"."ColumnRiskFactorTypeValue" as "OLD_ColumnRiskFactorTypeValue",
                                "OLD"."MatrixCategory" as "OLD_MatrixCategory",
                                "OLD"."MatrixID" as "OLD_MatrixID",
                                "OLD"."RowRiskFactorType" as "OLD_RowRiskFactorType",
                                "OLD"."RowRiskFactorTypeValue" as "OLD_RowRiskFactorTypeValue",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Amount" as "OLD_Amount",
                                "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                                "OLD"."DataProvider" as "OLD_DataProvider",
                                "OLD"."Description" as "OLD_Description",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RiskFactorInterdependenceMatrix" as "OLD"
            on
                ifnull( "IN"."ColumnRiskFactorType", '') = "OLD"."ColumnRiskFactorType" and
                ifnull( "IN"."ColumnRiskFactorTypeValue", '') = "OLD"."ColumnRiskFactorTypeValue" and
                ifnull( "IN"."MatrixCategory", '') = "OLD"."MatrixCategory" and
                ifnull( "IN"."MatrixID", '') = "OLD"."MatrixID" and
                ifnull( "IN"."RowRiskFactorType", '') = "OLD"."RowRiskFactorType" and
                ifnull( "IN"."RowRiskFactorTypeValue", '') = "OLD"."RowRiskFactorTypeValue" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RiskFactorInterdependenceMatrix" (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationMethod",
        "DataProvider",
        "Description",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ColumnRiskFactorType" as "ColumnRiskFactorType",
            "OLD_ColumnRiskFactorTypeValue" as "ColumnRiskFactorTypeValue",
            "OLD_MatrixCategory" as "MatrixCategory",
            "OLD_MatrixID" as "MatrixID",
            "OLD_RowRiskFactorType" as "RowRiskFactorType",
            "OLD_RowRiskFactorTypeValue" as "RowRiskFactorTypeValue",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Amount" as "Amount",
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_DataProvider" as "DataProvider",
            "OLD_Description" as "Description",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ColumnRiskFactorType",
                        "IN"."ColumnRiskFactorTypeValue",
                        "IN"."MatrixCategory",
                        "IN"."MatrixID",
                        "IN"."RowRiskFactorType",
                        "IN"."RowRiskFactorTypeValue",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."ColumnRiskFactorType" as "OLD_ColumnRiskFactorType",
                        "OLD"."ColumnRiskFactorTypeValue" as "OLD_ColumnRiskFactorTypeValue",
                        "OLD"."MatrixCategory" as "OLD_MatrixCategory",
                        "OLD"."MatrixID" as "OLD_MatrixID",
                        "OLD"."RowRiskFactorType" as "OLD_RowRiskFactorType",
                        "OLD"."RowRiskFactorTypeValue" as "OLD_RowRiskFactorTypeValue",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."Amount" as "OLD_Amount",
                        "OLD"."CalculationMethod" as "OLD_CalculationMethod",
                        "OLD"."DataProvider" as "OLD_DataProvider",
                        "OLD"."Description" as "OLD_Description",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RiskFactorInterdependenceMatrix" as "OLD"
            on
                ifnull( "IN"."ColumnRiskFactorType", '' ) = "OLD"."ColumnRiskFactorType" and
                ifnull( "IN"."ColumnRiskFactorTypeValue", '' ) = "OLD"."ColumnRiskFactorTypeValue" and
                ifnull( "IN"."MatrixCategory", '' ) = "OLD"."MatrixCategory" and
                ifnull( "IN"."MatrixID", '' ) = "OLD"."MatrixID" and
                ifnull( "IN"."RowRiskFactorType", '' ) = "OLD"."RowRiskFactorType" and
                ifnull( "IN"."RowRiskFactorTypeValue", '' ) = "OLD"."RowRiskFactorTypeValue" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::RiskFactorInterdependenceMatrix"
    where (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ColumnRiskFactorType",
            "OLD"."ColumnRiskFactorTypeValue",
            "OLD"."MatrixCategory",
            "OLD"."MatrixID",
            "OLD"."RowRiskFactorType",
            "OLD"."RowRiskFactorTypeValue",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RiskFactorInterdependenceMatrix" as "OLD"
        on
           ifnull( "IN"."ColumnRiskFactorType", '' ) = "OLD"."ColumnRiskFactorType" and
           ifnull( "IN"."ColumnRiskFactorTypeValue", '' ) = "OLD"."ColumnRiskFactorTypeValue" and
           ifnull( "IN"."MatrixCategory", '' ) = "OLD"."MatrixCategory" and
           ifnull( "IN"."MatrixID", '' ) = "OLD"."MatrixID" and
           ifnull( "IN"."RowRiskFactorType", '' ) = "OLD"."RowRiskFactorType" and
           ifnull( "IN"."RowRiskFactorTypeValue", '' ) = "OLD"."RowRiskFactorTypeValue" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::RiskFactorInterdependenceMatrix" (
        "ColumnRiskFactorType",
        "ColumnRiskFactorTypeValue",
        "MatrixCategory",
        "MatrixID",
        "RowRiskFactorType",
        "RowRiskFactorTypeValue",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Amount",
        "CalculationMethod",
        "DataProvider",
        "Description",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "ColumnRiskFactorType", '' ) as "ColumnRiskFactorType",
            ifnull( "ColumnRiskFactorTypeValue", '' ) as "ColumnRiskFactorTypeValue",
            ifnull( "MatrixCategory", '' ) as "MatrixCategory",
            ifnull( "MatrixID", '' ) as "MatrixID",
            ifnull( "RowRiskFactorType", '' ) as "RowRiskFactorType",
            ifnull( "RowRiskFactorTypeValue", '' ) as "RowRiskFactorTypeValue",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "Amount"  ,
            "CalculationMethod"  ,
            "DataProvider"  ,
            "Description"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END