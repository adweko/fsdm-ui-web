PROCEDURE "sap.fsdm.procedures::IdentifyingDocumentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::IdentifyingDocumentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::IdentifyingDocumentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::IdentifyingDocumentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "IdentifyingDocumentID" is null and
            "IdentifyingDocumentType" is null and
            "IssuingAuthority" is null and
            "IssuingCountry" is null and
            "ASSOC_IndividualPerson.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "IdentifyingDocumentID" ,
                "IdentifyingDocumentType" ,
                "IssuingAuthority" ,
                "IssuingCountry" ,
                "ASSOC_IndividualPerson.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."IdentifyingDocumentID" ,
                "OLD"."IdentifyingDocumentType" ,
                "OLD"."IssuingAuthority" ,
                "OLD"."IssuingCountry" ,
                "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::IdentifyingDocument" "OLD"
            on
                "IN"."IdentifyingDocumentID" = "OLD"."IdentifyingDocumentID" and
                "IN"."IdentifyingDocumentType" = "OLD"."IdentifyingDocumentType" and
                "IN"."IssuingAuthority" = "OLD"."IssuingAuthority" and
                "IN"."IssuingCountry" = "OLD"."IssuingCountry" and
                "IN"."ASSOC_IndividualPerson.BusinessPartnerID" = "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "IdentifyingDocumentID" ,
            "IdentifyingDocumentType" ,
            "IssuingAuthority" ,
            "IssuingCountry" ,
            "ASSOC_IndividualPerson.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."IdentifyingDocumentID" ,
                "OLD"."IdentifyingDocumentType" ,
                "OLD"."IssuingAuthority" ,
                "OLD"."IssuingCountry" ,
                "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::IdentifyingDocument_Historical" "OLD"
            on
                "IN"."IdentifyingDocumentID" = "OLD"."IdentifyingDocumentID" and
                "IN"."IdentifyingDocumentType" = "OLD"."IdentifyingDocumentType" and
                "IN"."IssuingAuthority" = "OLD"."IssuingAuthority" and
                "IN"."IssuingCountry" = "OLD"."IssuingCountry" and
                "IN"."ASSOC_IndividualPerson.BusinessPartnerID" = "OLD"."ASSOC_IndividualPerson.BusinessPartnerID" 
        );

END
