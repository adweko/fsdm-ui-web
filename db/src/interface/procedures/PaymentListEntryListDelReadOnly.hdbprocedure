PROCEDURE "sap.fsdm.procedures::PaymentListEntryListDelReadOnly" (IN ROW "sap.fsdm.tabletypes::PaymentListEntryTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::PaymentListEntryTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::PaymentListEntryTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PaymentDate=' || TO_VARCHAR("PaymentDate") || ' ' ||
                'ASSOC_PaymentList.SequenceNumber=' || TO_VARCHAR("ASSOC_PaymentList.SequenceNumber") || ' ' ||
                'ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem") || ' ' ||
                'ASSOC_PaymentList.ASSOC_Receivable.ReceivableID=' || TO_VARCHAR("ASSOC_PaymentList.ASSOC_Receivable.ReceivableID") || ' ' ||
                'ASSOC_PaymentList._CreditCardLoan.ID=' || TO_VARCHAR("ASSOC_PaymentList._CreditCardLoan.ID") || ' ' ||
                'ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID") || ' ' ||
                'ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem=' || TO_VARCHAR("ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem") || ' ' ||
                'ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID=' || TO_VARCHAR("ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID") || ' ' ||
                'ASSOC_PaymentList._InflationOptionComponent.ComponentNumber=' || TO_VARCHAR("ASSOC_PaymentList._InflationOptionComponent.ComponentNumber") || ' ' ||
                'ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID=' || TO_VARCHAR("ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID") || ' ' ||
                'ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem=' || TO_VARCHAR("ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem") || ' ' ||
                'ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber=' || TO_VARCHAR("ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber") || ' ' ||
                'ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID=' || TO_VARCHAR("ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID") || ' ' ||
                'ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem=' || TO_VARCHAR("ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PaymentDate",
                        "IN"."ASSOC_PaymentList.SequenceNumber",
                        "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                        "IN"."ASSOC_PaymentList._CreditCardLoan.ID",
                        "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                        "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                        "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                        "IN"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
                        "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
                        "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
                        "IN"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
                        "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                        "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PaymentDate",
                        "IN"."ASSOC_PaymentList.SequenceNumber",
                        "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                        "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                        "IN"."ASSOC_PaymentList._CreditCardLoan.ID",
                        "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                        "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                        "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                        "IN"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
                        "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
                        "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
                        "IN"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
                        "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                        "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PaymentDate",
                        "ASSOC_PaymentList.SequenceNumber",
                        "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                        "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                        "ASSOC_PaymentList._CreditCardLoan.ID",
                        "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                        "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                        "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                        "ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
                        "ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
                        "ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
                        "ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
                        "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                        "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PaymentDate",
                                    "IN"."ASSOC_PaymentList.SequenceNumber",
                                    "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                                    "IN"."ASSOC_PaymentList._CreditCardLoan.ID",
                                    "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                                    "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                                    "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                                    "IN"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
                                    "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
                                    "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
                                    "IN"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
                                    "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                                    "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PaymentDate",
                                    "IN"."ASSOC_PaymentList.SequenceNumber",
                                    "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                                    "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                                    "IN"."ASSOC_PaymentList._CreditCardLoan.ID",
                                    "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                                    "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                                    "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                                    "IN"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
                                    "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
                                    "IN"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
                                    "IN"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
                                    "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                                    "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" is null and
            "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" is null and
            "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" is null and
            "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" is null and
            "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" is null and
            "ASSOC_PaymentList._CreditCardLoan.ID" is null and
            "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" is null and
            "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" is null and
            "PaymentDate" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "PaymentDate",
            "ASSOC_PaymentList.SequenceNumber",
            "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
            "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
            "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
            "ASSOC_PaymentList._CreditCardLoan.ID",
            "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
            "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
            "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
            "ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
            "ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
            "ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
            "ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
            "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
            "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::PaymentListEntry" WHERE
            (
            "PaymentDate" ,
            "ASSOC_PaymentList.SequenceNumber" ,
            "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" ,
            "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" ,
            "ASSOC_PaymentList._CreditCardLoan.ID" ,
            "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" ,
            "ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" ,
            "ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" ,
            "ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" ,
            "ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" ,
            "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
            "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."PaymentDate",
            "OLD"."ASSOC_PaymentList.SequenceNumber",
            "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
            "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
            "OLD"."ASSOC_PaymentList._CreditCardLoan.ID",
            "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
            "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
            "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
            "OLD"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
            "OLD"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
            "OLD"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
            "OLD"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
            "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
            "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PaymentListEntry" as "OLD"
        on
                              "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" and
                              "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" and
                              "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" and
                              "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" and
                              "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                              "IN"."ASSOC_PaymentList._CreditCardLoan.ID" = "OLD"."ASSOC_PaymentList._CreditCardLoan.ID" and
                              "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                              "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" and
                              "IN"."PaymentDate" = "OLD"."PaymentDate" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "PaymentDate",
        "ASSOC_PaymentList.SequenceNumber",
        "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
        "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
        "ASSOC_PaymentList._CreditCardLoan.ID",
        "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
        "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
        "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
        "ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
        "ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
        "ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
        "ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
        "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
        "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "PaymentAmount",
        "PaymentCurrency",
        "PaymentPercentage",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_PaymentDate" as "PaymentDate" ,
            "OLD_ASSOC_PaymentList.SequenceNumber" as "ASSOC_PaymentList.SequenceNumber" ,
            "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" as "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" as "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" ,
            "OLD_ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" as "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" ,
            "OLD_ASSOC_PaymentList._CreditCardLoan.ID" as "ASSOC_PaymentList._CreditCardLoan.ID" ,
            "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" as "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
            "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" as "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
            "OLD_ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" as "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" ,
            "OLD_ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" as "ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" ,
            "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" as "ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" ,
            "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" as "ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" ,
            "OLD_ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" as "ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" ,
            "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" as "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
            "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" as "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_PaymentAmount" as "PaymentAmount" ,
            "OLD_PaymentCurrency" as "PaymentCurrency" ,
            "OLD_PaymentPercentage" as "PaymentPercentage" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                        "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                        "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem",
                        "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                        "OLD"."ASSOC_PaymentList._CreditCardLoan.ID",
                        "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                        "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                        "OLD"."PaymentDate",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."PaymentDate" AS "OLD_PaymentDate" ,
                "OLD"."ASSOC_PaymentList.SequenceNumber" AS "OLD_ASSOC_PaymentList.SequenceNumber" ,
                "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" AS "OLD_ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" ,
                "OLD"."ASSOC_PaymentList._CreditCardLoan.ID" AS "OLD_ASSOC_PaymentList._CreditCardLoan.ID" ,
                "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" AS "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" AS "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" AS "OLD_ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" AS "OLD_ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" AS "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" AS "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" AS "OLD_ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" AS "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" AS "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PaymentAmount" AS "OLD_PaymentAmount" ,
                "OLD"."PaymentCurrency" AS "OLD_PaymentCurrency" ,
                "OLD"."PaymentPercentage" AS "OLD_PaymentPercentage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PaymentListEntry" as "OLD"
            on
                                      "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" and
                                      "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" and
                                      "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" and
                                      "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" and
                                      "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
                                      "IN"."ASSOC_PaymentList._CreditCardLoan.ID" = "OLD"."ASSOC_PaymentList._CreditCardLoan.ID" and
                                      "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
                                      "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" and
                                      "IN"."PaymentDate" = "OLD"."PaymentDate" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_PaymentDate" as "PaymentDate",
            "OLD_ASSOC_PaymentList.SequenceNumber" as "ASSOC_PaymentList.SequenceNumber",
            "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" as "ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" as "ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
            "OLD_ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" as "ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
            "OLD_ASSOC_PaymentList._CreditCardLoan.ID" as "ASSOC_PaymentList._CreditCardLoan.ID",
            "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" as "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
            "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" as "ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
            "OLD_ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" as "ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
            "OLD_ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" as "ASSOC_PaymentList._InflationOptionComponent.ComponentNumber",
            "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" as "ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID",
            "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" as "ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem",
            "OLD_ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" as "ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber",
            "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" as "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
            "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" as "ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_PaymentAmount" as "PaymentAmount",
            "OLD_PaymentCurrency" as "PaymentCurrency",
            "OLD_PaymentPercentage" as "PaymentPercentage",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID",
                        "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem",
                        "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID",
                        "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem",
                        "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID",
                        "OLD"."ASSOC_PaymentList._CreditCardLoan.ID",
                        "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID",
                        "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem",
                        "OLD"."PaymentDate",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PaymentDate" AS "OLD_PaymentDate" ,
                "OLD"."ASSOC_PaymentList.SequenceNumber" AS "OLD_ASSOC_PaymentList.SequenceNumber" ,
                "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" AS "OLD_ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" ,
                "OLD"."ASSOC_PaymentList._CreditCardLoan.ID" AS "OLD_ASSOC_PaymentList._CreditCardLoan.ID" ,
                "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" AS "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" AS "OLD_ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" ,
                "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" AS "OLD_ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" AS "OLD_ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" AS "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" AS "OLD_ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" ,
                "OLD"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" AS "OLD_ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" ,
                "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" AS "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
                "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" AS "OLD_ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."PaymentAmount" AS "OLD_PaymentAmount" ,
                "OLD"."PaymentCurrency" AS "OLD_PaymentCurrency" ,
                "OLD"."PaymentPercentage" AS "OLD_PaymentPercentage" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::PaymentListEntry" as "OLD"
            on
               "IN"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" = "OLD"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" and
               "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" and
               "IN"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" and
               "IN"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" = "OLD"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" and
               "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" = "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" and
               "IN"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "OLD"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" and
               "IN"."ASSOC_PaymentList._CreditCardLoan.ID" = "OLD"."ASSOC_PaymentList._CreditCardLoan.ID" and
               "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" = "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID" and
               "IN"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" = "OLD"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" and
               "IN"."PaymentDate" = "OLD"."PaymentDate" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
