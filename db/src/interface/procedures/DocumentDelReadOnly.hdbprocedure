PROCEDURE "sap.fsdm.procedures::DocumentDelReadOnly" (IN ROW "sap.fsdm.tabletypes::DocumentTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::DocumentTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::DocumentTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DocumentID=' || TO_VARCHAR("DocumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DocumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DocumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DocumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DocumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DocumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "DocumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "DocumentID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::Document" WHERE
            (
            "DocumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."DocumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::Document" as "OLD"
        on
                              "IN"."DocumentID" = "OLD"."DocumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "DocumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_CommunicationEvent.CommunicationEventID",
        "ASSOC_CommunicationEvent.IDSystem",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
        "_TaxExemptStatusDocument.Category",
        "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "DocumentComment",
        "DocumentIssuingAuthority",
        "DocumentIssuingCountry",
        "DocumentLabel",
        "DocumentLink",
        "DocumentLocation",
        "DocumentSystem",
        "DocumentType",
        "DocumentValidFrom",
        "ExpiryDate",
        "Status",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_DocumentID" as "DocumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_CommunicationEvent.CommunicationEventID" as "ASSOC_CommunicationEvent.CommunicationEventID" ,
            "OLD_ASSOC_CommunicationEvent.IDSystem" as "ASSOC_CommunicationEvent.IDSystem" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" as "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" as "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
            "OLD__TaxExemptStatusDocument.Category" as "_TaxExemptStatusDocument.Category" ,
            "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" as "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "OLD_DocumentComment" as "DocumentComment" ,
            "OLD_DocumentIssuingAuthority" as "DocumentIssuingAuthority" ,
            "OLD_DocumentIssuingCountry" as "DocumentIssuingCountry" ,
            "OLD_DocumentLabel" as "DocumentLabel" ,
            "OLD_DocumentLink" as "DocumentLink" ,
            "OLD_DocumentLocation" as "DocumentLocation" ,
            "OLD_DocumentSystem" as "DocumentSystem" ,
            "OLD_DocumentType" as "DocumentType" ,
            "OLD_DocumentValidFrom" as "DocumentValidFrom" ,
            "OLD_ExpiryDate" as "ExpiryDate" ,
            "OLD_Status" as "Status" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."DocumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."DocumentID" AS "OLD_DocumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" AS "OLD_ASSOC_CommunicationEvent.CommunicationEventID" ,
                "OLD"."ASSOC_CommunicationEvent.IDSystem" AS "OLD_ASSOC_CommunicationEvent.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
                "OLD"."_TaxExemptStatusDocument.Category" AS "OLD__TaxExemptStatusDocument.Category" ,
                "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" AS "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."DocumentComment" AS "OLD_DocumentComment" ,
                "OLD"."DocumentIssuingAuthority" AS "OLD_DocumentIssuingAuthority" ,
                "OLD"."DocumentIssuingCountry" AS "OLD_DocumentIssuingCountry" ,
                "OLD"."DocumentLabel" AS "OLD_DocumentLabel" ,
                "OLD"."DocumentLink" AS "OLD_DocumentLink" ,
                "OLD"."DocumentLocation" AS "OLD_DocumentLocation" ,
                "OLD"."DocumentSystem" AS "OLD_DocumentSystem" ,
                "OLD"."DocumentType" AS "OLD_DocumentType" ,
                "OLD"."DocumentValidFrom" AS "OLD_DocumentValidFrom" ,
                "OLD"."ExpiryDate" AS "OLD_ExpiryDate" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Document" as "OLD"
            on
                                      "IN"."DocumentID" = "OLD"."DocumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_DocumentID" as "DocumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_CommunicationEvent.CommunicationEventID" as "ASSOC_CommunicationEvent.CommunicationEventID",
            "OLD_ASSOC_CommunicationEvent.IDSystem" as "ASSOC_CommunicationEvent.IDSystem",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" as "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" as "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
            "OLD__TaxExemptStatusDocument.Category" as "_TaxExemptStatusDocument.Category",
            "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" as "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "OLD_DocumentComment" as "DocumentComment",
            "OLD_DocumentIssuingAuthority" as "DocumentIssuingAuthority",
            "OLD_DocumentIssuingCountry" as "DocumentIssuingCountry",
            "OLD_DocumentLabel" as "DocumentLabel",
            "OLD_DocumentLink" as "DocumentLink",
            "OLD_DocumentLocation" as "DocumentLocation",
            "OLD_DocumentSystem" as "DocumentSystem",
            "OLD_DocumentType" as "DocumentType",
            "OLD_DocumentValidFrom" as "DocumentValidFrom",
            "OLD_ExpiryDate" as "ExpiryDate",
            "OLD_Status" as "Status",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."DocumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."DocumentID" AS "OLD_DocumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_CommunicationEvent.CommunicationEventID" AS "OLD_ASSOC_CommunicationEvent.CommunicationEventID" ,
                "OLD"."ASSOC_CommunicationEvent.IDSystem" AS "OLD_ASSOC_CommunicationEvent.IDSystem" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
                "OLD"."_TaxExemptStatusDocument.Category" AS "OLD__TaxExemptStatusDocument.Category" ,
                "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" AS "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."DocumentComment" AS "OLD_DocumentComment" ,
                "OLD"."DocumentIssuingAuthority" AS "OLD_DocumentIssuingAuthority" ,
                "OLD"."DocumentIssuingCountry" AS "OLD_DocumentIssuingCountry" ,
                "OLD"."DocumentLabel" AS "OLD_DocumentLabel" ,
                "OLD"."DocumentLink" AS "OLD_DocumentLink" ,
                "OLD"."DocumentLocation" AS "OLD_DocumentLocation" ,
                "OLD"."DocumentSystem" AS "OLD_DocumentSystem" ,
                "OLD"."DocumentType" AS "OLD_DocumentType" ,
                "OLD"."DocumentValidFrom" AS "OLD_DocumentValidFrom" ,
                "OLD"."ExpiryDate" AS "OLD_ExpiryDate" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::Document" as "OLD"
            on
               "IN"."DocumentID" = "OLD"."DocumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
