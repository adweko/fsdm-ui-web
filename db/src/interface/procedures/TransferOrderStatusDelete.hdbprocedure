PROCEDURE "sap.fsdm.procedures::TransferOrderStatusDelete" (IN ROW "sap.fsdm.tabletypes::TransferOrderStatusTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'TransferOrderStatusCategory=' || TO_VARCHAR("TransferOrderStatusCategory") || ' ' ||
                'ASSOC_TransferOrder.IDSystem=' || TO_VARCHAR("ASSOC_TransferOrder.IDSystem") || ' ' ||
                'ASSOC_TransferOrder.TransferOrderID=' || TO_VARCHAR("ASSOC_TransferOrder.TransferOrderID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."TransferOrderStatusCategory",
                        "IN"."ASSOC_TransferOrder.IDSystem",
                        "IN"."ASSOC_TransferOrder.TransferOrderID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."TransferOrderStatusCategory",
                        "IN"."ASSOC_TransferOrder.IDSystem",
                        "IN"."ASSOC_TransferOrder.TransferOrderID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "TransferOrderStatusCategory",
                        "ASSOC_TransferOrder.IDSystem",
                        "ASSOC_TransferOrder.TransferOrderID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."TransferOrderStatusCategory",
                                    "IN"."ASSOC_TransferOrder.IDSystem",
                                    "IN"."ASSOC_TransferOrder.TransferOrderID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."TransferOrderStatusCategory",
                                    "IN"."ASSOC_TransferOrder.IDSystem",
                                    "IN"."ASSOC_TransferOrder.TransferOrderID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "TransferOrderStatusCategory" is null and
            "ASSOC_TransferOrder.IDSystem" is null and
            "ASSOC_TransferOrder.TransferOrderID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::TransferOrderStatus" (
        "TransferOrderStatusCategory",
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID",
        "Status",
        "StatusChangeReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_TransferOrderStatusCategory" as "TransferOrderStatusCategory" ,
            "OLD_ASSOC_TransferOrder.IDSystem" as "ASSOC_TransferOrder.IDSystem" ,
            "OLD_ASSOC_TransferOrder.TransferOrderID" as "ASSOC_TransferOrder.TransferOrderID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" as "ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeReason" as "StatusChangeReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."TransferOrderStatusCategory",
                        "OLD"."ASSOC_TransferOrder.IDSystem",
                        "OLD"."ASSOC_TransferOrder.TransferOrderID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."TransferOrderStatusCategory" AS "OLD_TransferOrderStatusCategory" ,
                "OLD"."ASSOC_TransferOrder.IDSystem" AS "OLD_ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.TransferOrderID" AS "OLD_ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" AS "OLD_ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TransferOrderStatus" as "OLD"
            on
                      "IN"."TransferOrderStatusCategory" = "OLD"."TransferOrderStatusCategory" and
                      "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                      "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::TransferOrderStatus" (
        "TransferOrderStatusCategory",
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID",
        "Status",
        "StatusChangeReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_TransferOrderStatusCategory" as "TransferOrderStatusCategory",
            "OLD_ASSOC_TransferOrder.IDSystem" as "ASSOC_TransferOrder.IDSystem",
            "OLD_ASSOC_TransferOrder.TransferOrderID" as "ASSOC_TransferOrder.TransferOrderID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" as "ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID",
            "OLD_Status" as "Status",
            "OLD_StatusChangeReason" as "StatusChangeReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."TransferOrderStatusCategory",
                        "OLD"."ASSOC_TransferOrder.IDSystem",
                        "OLD"."ASSOC_TransferOrder.TransferOrderID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."TransferOrderStatusCategory" AS "OLD_TransferOrderStatusCategory" ,
                "OLD"."ASSOC_TransferOrder.IDSystem" AS "OLD_ASSOC_TransferOrder.IDSystem" ,
                "OLD"."ASSOC_TransferOrder.TransferOrderID" AS "OLD_ASSOC_TransferOrder.TransferOrderID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" AS "OLD_ASSOC_EmployeeResponsibleForTransferOrderStatusChange.BusinessPartnerID" ,
                "OLD"."Status" AS "OLD_Status" ,
                "OLD"."StatusChangeReason" AS "OLD_StatusChangeReason" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::TransferOrderStatus" as "OLD"
            on
                                                "IN"."TransferOrderStatusCategory" = "OLD"."TransferOrderStatusCategory" and
                                                "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                                                "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::TransferOrderStatus"
    where (
        "TransferOrderStatusCategory",
        "ASSOC_TransferOrder.IDSystem",
        "ASSOC_TransferOrder.TransferOrderID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."TransferOrderStatusCategory",
            "OLD"."ASSOC_TransferOrder.IDSystem",
            "OLD"."ASSOC_TransferOrder.TransferOrderID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::TransferOrderStatus" as "OLD"
        on
                                       "IN"."TransferOrderStatusCategory" = "OLD"."TransferOrderStatusCategory" and
                                       "IN"."ASSOC_TransferOrder.IDSystem" = "OLD"."ASSOC_TransferOrder.IDSystem" and
                                       "IN"."ASSOC_TransferOrder.TransferOrderID" = "OLD"."ASSOC_TransferOrder.TransferOrderID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
