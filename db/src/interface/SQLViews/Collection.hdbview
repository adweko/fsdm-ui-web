view "sap.fsdm.SQLViews::Collection_View" 
as select
      "CollectionID" ,
      "IDSystem" ,
      "_Client.BusinessPartnerID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "_AccountingSystem.AccountingSystemID" ,
      "_EmployeeResponsibleForPortfolio.BusinessPartnerID" ,
      "_OrganizationalUnit.IDSystem" ,
      "_OrganizationalUnit.OrganizationalUnitID" ,
      "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "CollateralPoolCategory" ,
      "CollateralPoolCoveringType" ,
      "CollateralPoolType" ,
      "CollectionCategory" ,
      "CollectionName" ,
      "CountryOfCollateralPoolCovering" ,
      "DerivativeStrategyType" ,
      "Distributed" ,
      "FactorLagLength" ,
      "FactorLagTimeUnit" ,
      "FinancialInstrumentPortfolioType" ,
      "HedgeRelationshipStatus" ,
      "HedgeStrategyType" ,
      "InvestmentObjective" ,
      "ManagingUnit" ,
      "OriginalExposureAtDefault" ,
      "OriginalExposureAtDefaultCurrency" ,
      "OriginalRegulatoryCapitalRequirementRate" ,
      "PoolAdministrationType" ,
      "PortfolioCategory" ,
      "SizeOfTradingBucket" ,
      "TradeBookType" ,
      "TradingDeskType" ,
      "UnderlyingExposureType" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::Collection"
with associations
(
	join "sap.fsdm.SQLViews::AccountingClassificationStatus_View" as "_AccountingClassificationStatus__Collection"
	  on "_Client.BusinessPartnerID" = "_AccountingClassificationStatus__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_AccountingClassificationStatus__Collection"."_Collection.IDSystem" AND 
	     "CollectionID" = "_AccountingClassificationStatus__Collection"."_Collection.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::AssignedAssetDistribution_View" as "_AssignedAssetDistribution__Collection"
	  on "_Client.BusinessPartnerID" = "_AssignedAssetDistribution__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_AssignedAssetDistribution__Collection"."_Collection.IDSystem" AND 
	     "CollectionID" = "_AssignedAssetDistribution__Collection"."_Collection.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::BookDesignation_View" as "_BookDesignation__Collection"
	  on "IDSystem" = "_BookDesignation__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_BookDesignation__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_BookDesignation__Collection"."_Collection.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::CashFlowStream_View" as "_CashFlowStream__CollectionOfCashFlow"
	  on "IDSystem" = "_CashFlowStream__CollectionOfCashFlow"."_CollectionOfCashFlow.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_CashFlowStream__CollectionOfCashFlow"."_CollectionOfCashFlow._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_CashFlowStream__CollectionOfCashFlow"."_CollectionOfCashFlow.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::CollateralPoolAssetAssignment_View" as "_CollateralPoolAssetAssignment__CollateralPool"
	  on "IDSystem" = "_CollateralPoolAssetAssignment__CollateralPool"."_CollateralPool.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_CollateralPoolAssetAssignment__CollateralPool"."_CollateralPool._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_CollateralPoolAssetAssignment__CollateralPool"."_CollateralPool.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::CollateralPoolKeyFigures_View" as "_CollateralPoolKeyFigures__CollateralPool"
	  on "CollectionID" = "_CollateralPoolKeyFigures__CollateralPool"."_CollateralPool.CollectionID" AND 
	     "IDSystem" = "_CollateralPoolKeyFigures__CollateralPool"."_CollateralPool.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::AccountingSystem_View" as "_AccountingSystem"
	  on "_AccountingSystem"."AccountingSystemID" = "_AccountingSystem.AccountingSystemID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "_Client"
	  on "_Client"."BusinessPartnerID" = "_Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "_EmployeeResponsibleForPortfolio"
	  on "_EmployeeResponsibleForPortfolio"."BusinessPartnerID" = "_EmployeeResponsibleForPortfolio.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_OrganizationalUnit"
	  on "_OrganizationalUnit"."IDSystem" = "_OrganizationalUnit.IDSystem" AND 
	     "_OrganizationalUnit"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_OrganizationalUnit"."OrganizationalUnitID" = "_OrganizationalUnit.OrganizationalUnitID"
	     ,
	join "sap.fsdm.SQLViews::CollectionReferenceID_View" as "_CollectionReferenceID__Collection"
	  on "CollectionID" = "_CollectionReferenceID__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_CollectionReferenceID__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CollectionRelation_View" as "_CollectionRelation__SourceInCollectionRelation"
	  on "_Client.BusinessPartnerID" = "_CollectionRelation__SourceInCollectionRelation"."_SourceInCollectionRelation._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_CollectionRelation__SourceInCollectionRelation"."_SourceInCollectionRelation.IDSystem" AND 
	     "CollectionID" = "_CollectionRelation__SourceInCollectionRelation"."_SourceInCollectionRelation.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::CollectionRelation_View" as "_CollectionRelation__TargetInCollectionRelation"
	  on "CollectionID" = "_CollectionRelation__TargetInCollectionRelation"."_TargetInCollectionRelation.CollectionID" AND 
	     "IDSystem" = "_CollectionRelation__TargetInCollectionRelation"."_TargetInCollectionRelation.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_CollectionRelation__TargetInCollectionRelation"."_TargetInCollectionRelation._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::CoveredFinancialContractAssignment_View" as "_CoveredFinancialContractAssignment__CollateralPool"
	  on "CollectionID" = "_CoveredFinancialContractAssignment__CollateralPool"."_CollateralPool.CollectionID" AND 
	     "IDSystem" = "_CoveredFinancialContractAssignment__CollateralPool"."_CollateralPool.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_CoveredFinancialContractAssignment__CollateralPool"."_CollateralPool._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::CoveredFinancialInstrumentAssignment_View" as "_CoveredFinancialInstrumentAssignment__CollateralPool"
	  on "_Client.BusinessPartnerID" = "_CoveredFinancialInstrumentAssignment__CollateralPool"."_CollateralPool._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_CoveredFinancialInstrumentAssignment__CollateralPool"."_CollateralPool.IDSystem" AND 
	     "CollectionID" = "_CoveredFinancialInstrumentAssignment__CollateralPool"."_CollateralPool.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::CreditDefaultSwapProtectionLeg_View" as "_CreditDefaultSwapProtectionLeg__SecuritizationPool"
	  on "CollectionID" = "_CreditDefaultSwapProtectionLeg__SecuritizationPool"."_SecuritizationPool.CollectionID" AND 
	     "IDSystem" = "_CreditDefaultSwapProtectionLeg__SecuritizationPool"."_SecuritizationPool.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExpectedLoss_View" as "_CreditRiskExpectedLoss__Collection"
	  on "IDSystem" = "_CreditRiskExpectedLoss__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_CreditRiskExpectedLoss__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_CreditRiskExpectedLoss__Collection"."_Collection.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExposure_View" as "_CreditRiskExposure__CollectionOfInstruments"
	  on "CollectionID" = "_CreditRiskExposure__CollectionOfInstruments"."_CollectionOfInstruments.CollectionID" AND 
	     "_Client.BusinessPartnerID" = "_CreditRiskExposure__CollectionOfInstruments"."_CollectionOfInstruments._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_CreditRiskExposure__CollectionOfInstruments"."_CollectionOfInstruments.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExposureAtDefault_View" as "_CreditRiskExposureAtDefault__Collection"
	  on "CollectionID" = "_CreditRiskExposureAtDefault__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_CreditRiskExposureAtDefault__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_CreditRiskExposureAtDefault__Collection"."_Collection._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::Document_View" as "_Document__Collection"
	  on "CollectionID" = "_Document__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_Document__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_Document__Collection"."_Collection._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::DocumentAssignment_View" as "_DocumentAssignment__Collection"
	  on "_Client.BusinessPartnerID" = "_DocumentAssignment__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_DocumentAssignment__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_DocumentAssignment__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::DueSettlement_View" as "_DueSettlement__Collection"
	  on "IDSystem" = "_DueSettlement__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_DueSettlement__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_DueSettlement__Collection"."_Collection.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::EffectiveInterestRate_View" as "_EffectiveInterestRate__Collection"
	  on "IDSystem" = "_EffectiveInterestRate__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_EffectiveInterestRate__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_EffectiveInterestRate__Collection"."_Collection.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::EndOfDayVolatilityObservation_View" as "_EndOfDayVolatilityObservation__Collection"
	  on "CollectionID" = "_EndOfDayVolatilityObservation__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_EndOfDayVolatilityObservation__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_EndOfDayVolatilityObservation__Collection"."_Collection._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::EndofDayBetaObservation_View" as "_EndofDayBetaObservation__Collection"
	  on "CollectionID" = "_EndofDayBetaObservation__Collection"."_Collection.CollectionID" AND 
	     "_Client.BusinessPartnerID" = "_EndofDayBetaObservation__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_EndofDayBetaObservation__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::EuropeanRegulatoryReportingForContract_View" as "_EuropeanRegulatoryReportingForContract__Collection"
	  on "CollectionID" = "_EuropeanRegulatoryReportingForContract__Collection"."_Collection.CollectionID" AND 
	     "_Client.BusinessPartnerID" = "_EuropeanRegulatoryReportingForContract__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_EuropeanRegulatoryReportingForContract__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FSSubledgerDocument_View" as "_FSSubledgerDocument__FinancialInstrumentPortfolio"
	  on "_Client.BusinessPartnerID" = "_FSSubledgerDocument__FinancialInstrumentPortfolio"."_FinancialInstrumentPortfolio._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_FSSubledgerDocument__FinancialInstrumentPortfolio"."_FinancialInstrumentPortfolio.CollectionID" AND 
	     "IDSystem" = "_FSSubledgerDocument__FinancialInstrumentPortfolio"."_FinancialInstrumentPortfolio.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FairValue_View" as "_FairValue__Collection"
	  on "CollectionID" = "_FairValue__Collection"."_Collection.CollectionID" AND 
	     "_Client.BusinessPartnerID" = "_FairValue__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "IDSystem" = "_FairValue__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "_FinancialContract__SecuritizationPool"
	  on "IDSystem" = "_FinancialContract__SecuritizationPool"."_SecuritizationPool.IDSystem" AND 
	     "CollectionID" = "_FinancialContract__SecuritizationPool"."_SecuritizationPool.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_FinancialInstrument__SecuritizationPool"
	  on "IDSystem" = "_FinancialInstrument__SecuritizationPool"."_SecuritizationPool.IDSystem" AND 
	     "CollectionID" = "_FinancialInstrument__SecuritizationPool"."_SecuritizationPool.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::FundsTransferPricingRate_View" as "_FundsTransferPricingRate__Collection"
	  on "_Client.BusinessPartnerID" = "_FundsTransferPricingRate__Collection"."_Collection._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_FundsTransferPricingRate__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_FundsTransferPricingRate__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::InterestAndBalanceSchedule_View" as "_InterestAndBalanceSchedule__Collection"
	  on "CollectionID" = "_InterestAndBalanceSchedule__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_InterestAndBalanceSchedule__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Position_View" as "_Position__Collection"
	  on "IDSystem" = "_Position__Collection"."_Collection.IDSystem" AND 
	     "CollectionID" = "_Position__Collection"."_Collection.CollectionID" AND 
	     "_Client.BusinessPartnerID" = "_Position__Collection"."_Collection._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::RefinancingRelation_View" as "_RefinancingRelation__CollectionOfRefinancedSecuritiesHolding"
	  on "IDSystem" = "_RefinancingRelation__CollectionOfRefinancedSecuritiesHolding"."_CollectionOfRefinancedSecuritiesHolding.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_RefinancingRelation__CollectionOfRefinancedSecuritiesHolding"."_CollectionOfRefinancedSecuritiesHolding._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_RefinancingRelation__CollectionOfRefinancedSecuritiesHolding"."_CollectionOfRefinancedSecuritiesHolding.CollectionID"
	     ,
	join "sap.fsdm.SQLViews::RegulatoryIndicatorForCollection_View" as "_RegulatoryIndicatorForCollection__Collection"
	  on "CollectionID" = "_RegulatoryIndicatorForCollection__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_RegulatoryIndicatorForCollection__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_RegulatoryIndicatorForCollection__Collection"."_Collection._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::RiskAndSensitivityKeyFigures_View" as "_RiskAndSensitivityKeyFigures__Collection"
	  on "CollectionID" = "_RiskAndSensitivityKeyFigures__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_RiskAndSensitivityKeyFigures__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::SecuritizationPoolKeyDateEvaluation_View" as "_SecuritizationPoolKeyDateEvaluation__Collection"
	  on "CollectionID" = "_SecuritizationPoolKeyDateEvaluation__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_SecuritizationPoolKeyDateEvaluation__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::SecuritizationPoolPeriodEvaluation_View" as "_SecuritizationPoolPeriodEvaluation__Collection"
	  on "CollectionID" = "_SecuritizationPoolPeriodEvaluation__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_SecuritizationPoolPeriodEvaluation__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Trade_View" as "_Trade__Collection"
	  on "CollectionID" = "_Trade__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_Trade__Collection"."_Collection.IDSystem" AND 
	     "_Client.BusinessPartnerID" = "_Trade__Collection"."_Collection._Client.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::Trade_View" as "_Trade__CollectionAtCounterparty"
	  on "_Client.BusinessPartnerID" = "_Trade__CollectionAtCounterparty"."_CollectionAtCounterparty._Client.BusinessPartnerID" AND 
	     "CollectionID" = "_Trade__CollectionAtCounterparty"."_CollectionAtCounterparty.CollectionID" AND 
	     "IDSystem" = "_Trade__CollectionAtCounterparty"."_CollectionAtCounterparty.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::TradingPeriod_View" as "_TradingPeriod__Collection"
	  on "CollectionID" = "_TradingPeriod__Collection"."_Collection.CollectionID" AND 
	     "IDSystem" = "_TradingPeriod__Collection"."_Collection.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::Valuation_View" as "_Valuation__Collection"
	  on "IDSystem" = "_Valuation__Collection"."_Collection.IDSystem" AND 
	     "CollectionID" = "_Valuation__Collection"."_Collection.CollectionID"
	     
);