PROCEDURE "sap.fsdm.procedures::OrganizationalUnitDelReadOnly" (IN ROW "sap.fsdm.tabletypes::OrganizationalUnitTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::OrganizationalUnitTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::OrganizationalUnitTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'IDSystem=' || TO_VARCHAR("IDSystem") || ' ' ||
                'OrganizationalUnitID=' || TO_VARCHAR("OrganizationalUnitID") || ' ' ||
                'ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."IDSystem",
                        "IN"."OrganizationalUnitID",
                        "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "IDSystem",
                        "OrganizationalUnitID",
                        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."IDSystem",
                                    "IN"."OrganizationalUnitID",
                                    "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "IDSystem" is null and
            "OrganizationalUnitID" is null and
            "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "IDSystem",
            "OrganizationalUnitID",
            "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::OrganizationalUnit" WHERE
            (
            "IDSystem" ,
            "OrganizationalUnitID" ,
            "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."IDSystem",
            "OLD"."OrganizationalUnitID",
            "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::OrganizationalUnit" as "OLD"
        on
                              "IN"."IDSystem" = "OLD"."IDSystem" and
                              "IN"."OrganizationalUnitID" = "OLD"."OrganizationalUnitID" and
                              "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "IDSystem",
        "OrganizationalUnitID",
        "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_CompanyCode.CompanyCode",
        "_CompanyCode.ASSOC_Company.BusinessPartnerID",
        "OrganisationalUnitType",
        "OrganizationalUnitCategory",
        "OrganizationalUnitName",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_IDSystem" as "IDSystem" ,
            "OLD_OrganizationalUnitID" as "OrganizationalUnitID" ,
            "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__CompanyCode.CompanyCode" as "_CompanyCode.CompanyCode" ,
            "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID" as "_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
            "OLD_OrganisationalUnitType" as "OrganisationalUnitType" ,
            "OLD_OrganizationalUnitCategory" as "OrganizationalUnitCategory" ,
            "OLD_OrganizationalUnitName" as "OrganizationalUnitName" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."IDSystem",
                        "OLD"."OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."OrganizationalUnitID" AS "OLD_OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_CompanyCode.CompanyCode" AS "OLD__CompanyCode.CompanyCode" ,
                "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" AS "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."OrganisationalUnitType" AS "OLD_OrganisationalUnitType" ,
                "OLD"."OrganizationalUnitCategory" AS "OLD_OrganizationalUnitCategory" ,
                "OLD"."OrganizationalUnitName" AS "OLD_OrganizationalUnitName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnit" as "OLD"
            on
                                      "IN"."IDSystem" = "OLD"."IDSystem" and
                                      "IN"."OrganizationalUnitID" = "OLD"."OrganizationalUnitID" and
                                      "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_IDSystem" as "IDSystem",
            "OLD_OrganizationalUnitID" as "OrganizationalUnitID",
            "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__CompanyCode.CompanyCode" as "_CompanyCode.CompanyCode",
            "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID" as "_CompanyCode.ASSOC_Company.BusinessPartnerID",
            "OLD_OrganisationalUnitType" as "OrganisationalUnitType",
            "OLD_OrganizationalUnitCategory" as "OrganizationalUnitCategory",
            "OLD_OrganizationalUnitName" as "OrganizationalUnitName",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."IDSystem",
                        "OLD"."OrganizationalUnitID",
                        "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."IDSystem" AS "OLD_IDSystem" ,
                "OLD"."OrganizationalUnitID" AS "OLD_OrganizationalUnitID" ,
                "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_CompanyCode.CompanyCode" AS "OLD__CompanyCode.CompanyCode" ,
                "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" AS "OLD__CompanyCode.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."OrganisationalUnitType" AS "OLD_OrganisationalUnitType" ,
                "OLD"."OrganizationalUnitCategory" AS "OLD_OrganizationalUnitCategory" ,
                "OLD"."OrganizationalUnitName" AS "OLD_OrganizationalUnitName" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::OrganizationalUnit" as "OLD"
            on
               "IN"."IDSystem" = "OLD"."IDSystem" and
               "IN"."OrganizationalUnitID" = "OLD"."OrganizationalUnitID" and
               "IN"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
