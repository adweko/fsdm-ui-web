PROCEDURE "sap.fsdm.procedures::CreditRiskAssessmentAdmonitionLoad" (IN ROW "sap.fsdm.tabletypes::CreditRiskAssessmentAdmonitionTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AdmonitionDate=' || TO_VARCHAR("AdmonitionDate") || ' ' ||
                '_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason=' || TO_VARCHAR("_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason") || ' ' ||
                '_DataRequestInformation.CreditRiskAssessmentDate=' || TO_VARCHAR("_DataRequestInformation.CreditRiskAssessmentDate") || ' ' ||
                '_DataRequestInformation._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_DataRequestInformation._BusinessPartner.BusinessPartnerID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AdmonitionDate",
                        "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."_DataRequestInformation.CreditRiskAssessmentDate",
                        "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AdmonitionDate",
                        "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."_DataRequestInformation.CreditRiskAssessmentDate",
                        "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AdmonitionDate",
                        "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                        "_DataRequestInformation.CreditRiskAssessmentDate",
                        "_DataRequestInformation._BusinessPartner.BusinessPartnerID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AdmonitionDate",
                                    "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                                    "IN"."_DataRequestInformation.CreditRiskAssessmentDate",
                                    "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AdmonitionDate",
                                    "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                                    "IN"."_DataRequestInformation.CreditRiskAssessmentDate",
                                    "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::CreditRiskAssessmentAdmonition" (
        "AdmonitionDate",
        "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
        "_DataRequestInformation.CreditRiskAssessmentDate",
        "_DataRequestInformation._BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AdmonitionDate" as "AdmonitionDate" ,
            "OLD__DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" as "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" ,
            "OLD__DataRequestInformation.CreditRiskAssessmentDate" as "_DataRequestInformation.CreditRiskAssessmentDate" ,
            "OLD__DataRequestInformation._BusinessPartner.BusinessPartnerID" as "_DataRequestInformation._BusinessPartner.BusinessPartnerID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."AdmonitionDate",
                        "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."_DataRequestInformation.CreditRiskAssessmentDate",
                        "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."AdmonitionDate" as "OLD_AdmonitionDate",
                                "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" as "OLD__DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                                "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" as "OLD__DataRequestInformation.CreditRiskAssessmentDate",
                                "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" as "OLD__DataRequestInformation._BusinessPartner.BusinessPartnerID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskAssessmentAdmonition" as "OLD"
            on
                ifnull( "IN"."AdmonitionDate", '0001-01-01') = "OLD"."AdmonitionDate" and
                ifnull( "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason", '') = "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" and
                ifnull( "IN"."_DataRequestInformation.CreditRiskAssessmentDate", '0001-01-01') = "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" and
                ifnull( "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID", '') = "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CreditRiskAssessmentAdmonition" (
        "AdmonitionDate",
        "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
        "_DataRequestInformation.CreditRiskAssessmentDate",
        "_DataRequestInformation._BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AdmonitionDate" as "AdmonitionDate",
            "OLD__DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" as "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
            "OLD__DataRequestInformation.CreditRiskAssessmentDate" as "_DataRequestInformation.CreditRiskAssessmentDate",
            "OLD__DataRequestInformation._BusinessPartner.BusinessPartnerID" as "_DataRequestInformation._BusinessPartner.BusinessPartnerID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."AdmonitionDate",
                        "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                        "IN"."_DataRequestInformation.CreditRiskAssessmentDate",
                        "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."AdmonitionDate" as "OLD_AdmonitionDate",
                        "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" as "OLD__DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
                        "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" as "OLD__DataRequestInformation.CreditRiskAssessmentDate",
                        "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" as "OLD__DataRequestInformation._BusinessPartner.BusinessPartnerID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskAssessmentAdmonition" as "OLD"
            on
                ifnull( "IN"."AdmonitionDate", '0001-01-01' ) = "OLD"."AdmonitionDate" and
                ifnull( "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason", '' ) = "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" and
                ifnull( "IN"."_DataRequestInformation.CreditRiskAssessmentDate", '0001-01-01' ) = "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" and
                ifnull( "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::CreditRiskAssessmentAdmonition"
    where (
        "AdmonitionDate",
        "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
        "_DataRequestInformation.CreditRiskAssessmentDate",
        "_DataRequestInformation._BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."AdmonitionDate",
            "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
            "OLD"."_DataRequestInformation.CreditRiskAssessmentDate",
            "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditRiskAssessmentAdmonition" as "OLD"
        on
           ifnull( "IN"."AdmonitionDate", '0001-01-01' ) = "OLD"."AdmonitionDate" and
           ifnull( "IN"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason", '' ) = "OLD"."_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason" and
           ifnull( "IN"."_DataRequestInformation.CreditRiskAssessmentDate", '0001-01-01' ) = "OLD"."_DataRequestInformation.CreditRiskAssessmentDate" and
           ifnull( "IN"."_DataRequestInformation._BusinessPartner.BusinessPartnerID", '' ) = "OLD"."_DataRequestInformation._BusinessPartner.BusinessPartnerID" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::CreditRiskAssessmentAdmonition" (
        "AdmonitionDate",
        "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
        "_DataRequestInformation.CreditRiskAssessmentDate",
        "_DataRequestInformation._BusinessPartner.BusinessPartnerID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "AdmonitionDate", '0001-01-01' ) as "AdmonitionDate",
            ifnull( "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason", '' ) as "_DataRequestInformation.BusinessPartnerCreditRiskAssessmentReason",
            ifnull( "_DataRequestInformation.CreditRiskAssessmentDate", '0001-01-01' ) as "_DataRequestInformation.CreditRiskAssessmentDate",
            ifnull( "_DataRequestInformation._BusinessPartner.BusinessPartnerID", '' ) as "_DataRequestInformation._BusinessPartner.BusinessPartnerID",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END