Financial Services Data Management - Reusable Datamodel
=======================================================

This is the Financial Services Data Management (FSDM) data model as a reusable DB module. Technically, this is a NPM module.

For more information, check the documentation for [Financial Services Data Management](https://help.sap.com/viewer/product/FS_DATA_MANAGEMENT).