namespace "sap"."fsdm";

using "sap"."fsdm"::"MaturityBand";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";
using "sap"."fsdm"::"ResultGroup";
using "sap"."fsdm"::"RiskReportingNode";

entity "StockFlowAnalysis" {
    key "LiquidityRiskAnalysisType"     : String(100)                                                  default '';
    key "LiquidityRiskResultType"       : String(100)                                                  default '';
    key "LiquidityRiskSplitPartType"    : String(100)                                                  default '';
    key "RiskProvisionScenario"         : String(100)                                                  default '';
    key "RoleOfCurrency"                : String(40)                                                   default '';
    key "_DynamicTimeBucket"            : association to MaturityBand {
                                                                        MaturityBandID,
                                                                        TimeBucketID
                                                                      }                                not null;
    key "_FinancialContract"            : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    key "_FinancialInstrument"          : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "_ResultGroup"                  : association to ResultGroup {
                                                                       ResultDataProvider,
                                                                       ResultGroupID
                                                                     }                                 not null;
    key "_RiskReportingNode"            : association to RiskReportingNode { RiskReportingNodeID }     not null;
    key "_SecuritiesAccount"            : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }                           not null;
    key "_TimeBucket"                   : association to MaturityBand {
                                                                        MaturityBandID,
                                                                        TimeBucketID
                                                                      }                                not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                                                 not null;
        "SystemValidTo"                 : UTCTimestamp                                                 not null;
        "Currency"                      : String(3);
        "DynamicTimeBucketEndDate"      : LocalDate;
        "DynamicTimeBucketStartDate"    : LocalDate;
        "InitialStockFlow"              : Decimal(34, 6);
        "StockFlow"                     : Decimal(34, 6);
        "StockFlowLiquidityLevel"       : String(100);
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "StockFlowAnalysis_Historical" {
    "LiquidityRiskAnalysisType"     : String(100)                                                  default '' not null;
    "LiquidityRiskResultType"       : String(100)                                                  default '' not null;
    "LiquidityRiskSplitPartType"    : String(100)                                                  default '' not null;
    "RiskProvisionScenario"         : String(100)                                                  default '' not null;
    "RoleOfCurrency"                : String(40)                                                   default '' not null;
    "_DynamicTimeBucket"            : association to MaturityBand {
                                                                    MaturityBandID,
                                                                    TimeBucketID
                                                                  }                                not null;
    "_FinancialContract"            : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }                           not null;
    "_FinancialInstrument"          : association to FinancialInstrument { FinancialInstrumentID } not null;
    "_ResultGroup"                  : association to ResultGroup {
                                                                   ResultDataProvider,
                                                                   ResultGroupID
                                                                 }                                 not null;
    "_RiskReportingNode"            : association to RiskReportingNode { RiskReportingNodeID }     not null;
    "_SecuritiesAccount"            : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }                           not null;
    "_TimeBucket"                   : association to MaturityBand {
                                                                    MaturityBandID,
                                                                    TimeBucketID
                                                                  }                                not null;
    "BusinessValidFrom"             : LocalDate                                                    not null;
    "BusinessValidTo"               : LocalDate                                                    not null;
    "SystemValidFrom"               : UTCTimestamp                                                 not null;
    "SystemValidTo"                 : UTCTimestamp                                                 not null;
    "Currency"                      : String(3);
    "DynamicTimeBucketEndDate"      : LocalDate;
    "DynamicTimeBucketStartDate"    : LocalDate;
    "InitialStockFlow"              : Decimal(34, 6);
    "StockFlow"                     : Decimal(34, 6);
    "StockFlowLiquidityLevel"       : String(100);
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};