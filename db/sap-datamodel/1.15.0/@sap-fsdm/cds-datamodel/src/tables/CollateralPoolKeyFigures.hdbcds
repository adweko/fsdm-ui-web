namespace "sap"."fsdm";

using "sap"."fsdm"::"Collection";
using "sap"."fsdm"::"MaturityBand";

entity "CollateralPoolKeyFigures" {
    key "Category"                            : String(40);
    key "KeyDate"                             : LocalDate;
    key "Provider"                            : String(256);
    key "Scenario"                            : String(100)                   default '';
    key "_CollateralPool"                     : association to Collection {
                                                                            CollectionID,
                                                                            IDSystem
                                                                          }   not null;
    key "_TimeBucket"                         : association to MaturityBand {
                                                                              MaturityBandID,
                                                                              TimeBucketID
                                                                            } not null;
    key "BusinessValidFrom"                   : LocalDate;
    key "BusinessValidTo"                     : LocalDate;
        "SystemValidFrom"                     : UTCTimestamp                  not null;
        "SystemValidTo"                       : UTCTimestamp                  not null;
        "CalculationMethod"                   : String(20);
        "DepositAtBankInPercent"              : Decimal(15, 11);
        "Description"                         : String(256);
        "ExcessCollateralInPercent"           : Decimal(15, 11);
        "LiquidCollateralInPercent"           : Decimal(15, 11);
        "Name"                                : String(256);
        "ProlongedAmortizationAmount"         : Decimal(34, 6);
        "ProlongedAmortizationAmountCurrency" : String(3);
        "RegularAmortizationAmount"           : Decimal(34, 6);
        "RegularAmortizationAmountCurrency"   : String(3);
        "TimeBucketEndDate"                   : LocalDate;
        "TimeBucketStartDate"                 : LocalDate;
        "SourceSystemID"                      : String(128);
        "ChangeTimestampInSourceSystem"       : UTCTimestamp;
        "ChangingUserInSourceSystem"          : String(128);
        "ChangingProcessType"                 : String(40);
        "ChangingProcessID"                   : String(128);
}
technical configuration {
    column store;
};

entity "CollateralPoolKeyFigures_Historical" {
    "Category"                            : String(40)                    not null;
    "KeyDate"                             : LocalDate                     not null;
    "Provider"                            : String(256)                   not null;
    "Scenario"                            : String(100)                   default '' not null;
    "_CollateralPool"                     : association to Collection {
                                                                        CollectionID,
                                                                        IDSystem
                                                                      }   not null;
    "_TimeBucket"                         : association to MaturityBand {
                                                                          MaturityBandID,
                                                                          TimeBucketID
                                                                        } not null;
    "BusinessValidFrom"                   : LocalDate                     not null;
    "BusinessValidTo"                     : LocalDate                     not null;
    "SystemValidFrom"                     : UTCTimestamp                  not null;
    "SystemValidTo"                       : UTCTimestamp                  not null;
    "CalculationMethod"                   : String(20);
    "DepositAtBankInPercent"              : Decimal(15, 11);
    "Description"                         : String(256);
    "ExcessCollateralInPercent"           : Decimal(15, 11);
    "LiquidCollateralInPercent"           : Decimal(15, 11);
    "Name"                                : String(256);
    "ProlongedAmortizationAmount"         : Decimal(34, 6);
    "ProlongedAmortizationAmountCurrency" : String(3);
    "RegularAmortizationAmount"           : Decimal(34, 6);
    "RegularAmortizationAmountCurrency"   : String(3);
    "TimeBucketEndDate"                   : LocalDate;
    "TimeBucketStartDate"                 : LocalDate;
    "SourceSystemID"                      : String(128);
    "ChangeTimestampInSourceSystem"       : UTCTimestamp;
    "ChangingUserInSourceSystem"          : String(128);
    "ChangingProcessType"                 : String(40);
    "ChangingProcessID"                   : String(128);
}
technical configuration {
    column store;
};