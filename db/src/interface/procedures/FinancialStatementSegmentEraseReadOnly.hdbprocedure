PROCEDURE "sap.fsdm.procedures::FinancialStatementSegmentEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FinancialStatementSegmentTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FinancialStatementSegmentTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FinancialStatementSegmentTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "FinancialStatementSegmentID" is null and
            "_FinancialStatementStructure.FinancialStatementStructureID" is null and
            "_GLAccount.GLAccount" is null and
            "_GLAccount._ChartOfAccounts.ChartOfAccountId" is null and
            "_SubledgerAccount.SubledgerAccount" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "FinancialStatementSegmentID" ,
                "_FinancialStatementStructure.FinancialStatementStructureID" ,
                "_GLAccount.GLAccount" ,
                "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "_SubledgerAccount.SubledgerAccount" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."FinancialStatementSegmentID" ,
                "OLD"."_FinancialStatementStructure.FinancialStatementStructureID" ,
                "OLD"."_GLAccount.GLAccount" ,
                "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_SubledgerAccount.SubledgerAccount" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialStatementSegment" "OLD"
            on
                "IN"."FinancialStatementSegmentID" = "OLD"."FinancialStatementSegmentID" and
                "IN"."_FinancialStatementStructure.FinancialStatementStructureID" = "OLD"."_FinancialStatementStructure.FinancialStatementStructureID" and
                "IN"."_GLAccount.GLAccount" = "OLD"."_GLAccount.GLAccount" and
                "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_SubledgerAccount.SubledgerAccount" = "OLD"."_SubledgerAccount.SubledgerAccount" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "FinancialStatementSegmentID" ,
            "_FinancialStatementStructure.FinancialStatementStructureID" ,
            "_GLAccount.GLAccount" ,
            "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_SubledgerAccount.SubledgerAccount" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."FinancialStatementSegmentID" ,
                "OLD"."_FinancialStatementStructure.FinancialStatementStructureID" ,
                "OLD"."_GLAccount.GLAccount" ,
                "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_SubledgerAccount.SubledgerAccount" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialStatementSegment_Historical" "OLD"
            on
                "IN"."FinancialStatementSegmentID" = "OLD"."FinancialStatementSegmentID" and
                "IN"."_FinancialStatementStructure.FinancialStatementStructureID" = "OLD"."_FinancialStatementStructure.FinancialStatementStructureID" and
                "IN"."_GLAccount.GLAccount" = "OLD"."_GLAccount.GLAccount" and
                "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_SubledgerAccount.SubledgerAccount" = "OLD"."_SubledgerAccount.SubledgerAccount" 
        );

END
