var result;

function manipulateData(body) {

    try {
        var conn = $.hdb.getConnection(); //init connection to hana
        if (body.name === "insertNewField") {
            var values = body.values;
            var fields = body.fields;
            var tableName = body.tableName;
            var valuesString = '';
            var columnString = '';
            for (var i = 0; i < values.length; i++) {
                    valuesString += ("'" + values[i] + "'");
                    columnString += ('"' + fields[i] + '"');
                    if (!(i === values.length - 1)){
                    	valuesString +=  ",";
                		columnString += ",";
                    }
            }
            conn.executeUpdate('INSERT INTO ' + '"' + tableName + '"' + ' (' + columnString + ') VALUES(' + valuesString + ')');
            
            result = {statusText: "Data successfully inserted", status: $.net.http.CREATED};


        } else if (body.name === "deleteChosenRow") {
            var values = body.values;
            var keyValues = body.keyValues;
            var tableName = body.tableName;
            var query = "";
            for (var i = 0; i < keyValues.length; i++) {
                query += '"' + keyValues[i] + '"' + " = " + "'" + values[i] + "'";
                if (i < keyValues.length - 1) {
                    query += " AND ";
                }
            }
            conn.executeUpdate('DELETE FROM  ' + '"' + tableName + '"' + ' WHERE' + query + '');
            result = {statusText: "Row successfully deleted", status: $.net.http.CREATED};
        }
        conn.commit();//confirm update(must have element)
        conn.close();//also must-have

    } catch (errorDB) {

        result = {statusText: errorDB.message};
    }

}

var body = JSON.parse($.request.body.asString());
manipulateData(body);

$.response.contentType = "application/json";
//$.response.status = result.status;
$.response.setBody({statusText: result.statusText, test: ["2", "2"]});


