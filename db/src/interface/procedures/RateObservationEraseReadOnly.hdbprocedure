PROCEDURE "sap.fsdm.procedures::RateObservationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RateObservationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RateObservationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RateObservationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "RateDataProvider" is null and
            "Timestamp" is null and
            "ASSOC_ReferenceRateID.ReferenceRateID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "RateDataProvider" ,
                "Timestamp" ,
                "ASSOC_ReferenceRateID.ReferenceRateID" 
        from
        (
            select
                "OLD"."RateDataProvider" ,
                "OLD"."Timestamp" ,
                "OLD"."ASSOC_ReferenceRateID.ReferenceRateID" 
            from :ROW "IN"
            inner join "sap.fsdm::RateObservation" "OLD"
            on
                "IN"."RateDataProvider" = "OLD"."RateDataProvider" and
                "IN"."Timestamp" = "OLD"."Timestamp" and
                "IN"."ASSOC_ReferenceRateID.ReferenceRateID" = "OLD"."ASSOC_ReferenceRateID.ReferenceRateID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "RateDataProvider" ,
            "Timestamp" ,
            "ASSOC_ReferenceRateID.ReferenceRateID" 
        from
        (
            select
                "OLD"."RateDataProvider" ,
                "OLD"."Timestamp" ,
                "OLD"."ASSOC_ReferenceRateID.ReferenceRateID" 
            from :ROW "IN"
            inner join "sap.fsdm::RateObservation_Historical" "OLD"
            on
                "IN"."RateDataProvider" = "OLD"."RateDataProvider" and
                "IN"."Timestamp" = "OLD"."Timestamp" and
                "IN"."ASSOC_ReferenceRateID.ReferenceRateID" = "OLD"."ASSOC_ReferenceRateID.ReferenceRateID" 
        );

END
