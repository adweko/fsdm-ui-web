PROCEDURE "sap.fsdm.procedures::SwapAmortizationSpecificationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::SwapAmortizationSpecificationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::SwapAmortizationSpecificationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::SwapAmortizationSpecificationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AmortizationEndDate=' || TO_VARCHAR("AmortizationEndDate") || ' ' ||
                'AmortizationStartDate=' || TO_VARCHAR("AmortizationStartDate") || ' ' ||
                'RoleOfPayer=' || TO_VARCHAR("RoleOfPayer") || ' ' ||
                '_InterestBearingSwap.FinancialContractID=' || TO_VARCHAR("_InterestBearingSwap.FinancialContractID") || ' ' ||
                '_InterestBearingSwap.IDSystem=' || TO_VARCHAR("_InterestBearingSwap.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AmortizationEndDate",
                        "IN"."AmortizationStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_InterestBearingSwap.FinancialContractID",
                        "IN"."_InterestBearingSwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AmortizationEndDate",
                        "IN"."AmortizationStartDate",
                        "IN"."RoleOfPayer",
                        "IN"."_InterestBearingSwap.FinancialContractID",
                        "IN"."_InterestBearingSwap.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AmortizationEndDate",
                        "AmortizationStartDate",
                        "RoleOfPayer",
                        "_InterestBearingSwap.FinancialContractID",
                        "_InterestBearingSwap.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AmortizationEndDate",
                                    "IN"."AmortizationStartDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_InterestBearingSwap.FinancialContractID",
                                    "IN"."_InterestBearingSwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AmortizationEndDate",
                                    "IN"."AmortizationStartDate",
                                    "IN"."RoleOfPayer",
                                    "IN"."_InterestBearingSwap.FinancialContractID",
                                    "IN"."_InterestBearingSwap.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "AmortizationEndDate" is null and
            "AmortizationStartDate" is null and
            "RoleOfPayer" is null and
            "_InterestBearingSwap.FinancialContractID" is null and
            "_InterestBearingSwap.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "AmortizationEndDate",
            "AmortizationStartDate",
            "RoleOfPayer",
            "_InterestBearingSwap.FinancialContractID",
            "_InterestBearingSwap.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::SwapAmortizationSpecification" WHERE
            (
            "AmortizationEndDate" ,
            "AmortizationStartDate" ,
            "RoleOfPayer" ,
            "_InterestBearingSwap.FinancialContractID" ,
            "_InterestBearingSwap.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."AmortizationEndDate",
            "OLD"."AmortizationStartDate",
            "OLD"."RoleOfPayer",
            "OLD"."_InterestBearingSwap.FinancialContractID",
            "OLD"."_InterestBearingSwap.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::SwapAmortizationSpecification" as "OLD"
        on
                              "IN"."AmortizationEndDate" = "OLD"."AmortizationEndDate" and
                              "IN"."AmortizationStartDate" = "OLD"."AmortizationStartDate" and
                              "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                              "IN"."_InterestBearingSwap.FinancialContractID" = "OLD"."_InterestBearingSwap.FinancialContractID" and
                              "IN"."_InterestBearingSwap.IDSystem" = "OLD"."_InterestBearingSwap.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "AmortizationEndDate",
        "AmortizationStartDate",
        "RoleOfPayer",
        "_InterestBearingSwap.FinancialContractID",
        "_InterestBearingSwap.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AmortizationAmount",
        "AmortizationFrequency",
        "AmortizationFrequencyTimeUnit",
        "AmortizationRate",
        "AmortizationRule",
        "AnnuityAmount",
        "CouponReinvestmentPercentage",
        "PayFullCoupon",
        "TargetNotionalAmount",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_AmortizationEndDate" as "AmortizationEndDate" ,
            "OLD_AmortizationStartDate" as "AmortizationStartDate" ,
            "OLD_RoleOfPayer" as "RoleOfPayer" ,
            "OLD__InterestBearingSwap.FinancialContractID" as "_InterestBearingSwap.FinancialContractID" ,
            "OLD__InterestBearingSwap.IDSystem" as "_InterestBearingSwap.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AmortizationAmount" as "AmortizationAmount" ,
            "OLD_AmortizationFrequency" as "AmortizationFrequency" ,
            "OLD_AmortizationFrequencyTimeUnit" as "AmortizationFrequencyTimeUnit" ,
            "OLD_AmortizationRate" as "AmortizationRate" ,
            "OLD_AmortizationRule" as "AmortizationRule" ,
            "OLD_AnnuityAmount" as "AnnuityAmount" ,
            "OLD_CouponReinvestmentPercentage" as "CouponReinvestmentPercentage" ,
            "OLD_PayFullCoupon" as "PayFullCoupon" ,
            "OLD_TargetNotionalAmount" as "TargetNotionalAmount" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."AmortizationEndDate",
                        "OLD"."AmortizationStartDate",
                        "OLD"."RoleOfPayer",
                        "OLD"."_InterestBearingSwap.FinancialContractID",
                        "OLD"."_InterestBearingSwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."AmortizationEndDate" AS "OLD_AmortizationEndDate" ,
                "OLD"."AmortizationStartDate" AS "OLD_AmortizationStartDate" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."_InterestBearingSwap.FinancialContractID" AS "OLD__InterestBearingSwap.FinancialContractID" ,
                "OLD"."_InterestBearingSwap.IDSystem" AS "OLD__InterestBearingSwap.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AmortizationAmount" AS "OLD_AmortizationAmount" ,
                "OLD"."AmortizationFrequency" AS "OLD_AmortizationFrequency" ,
                "OLD"."AmortizationFrequencyTimeUnit" AS "OLD_AmortizationFrequencyTimeUnit" ,
                "OLD"."AmortizationRate" AS "OLD_AmortizationRate" ,
                "OLD"."AmortizationRule" AS "OLD_AmortizationRule" ,
                "OLD"."AnnuityAmount" AS "OLD_AnnuityAmount" ,
                "OLD"."CouponReinvestmentPercentage" AS "OLD_CouponReinvestmentPercentage" ,
                "OLD"."PayFullCoupon" AS "OLD_PayFullCoupon" ,
                "OLD"."TargetNotionalAmount" AS "OLD_TargetNotionalAmount" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SwapAmortizationSpecification" as "OLD"
            on
                                      "IN"."AmortizationEndDate" = "OLD"."AmortizationEndDate" and
                                      "IN"."AmortizationStartDate" = "OLD"."AmortizationStartDate" and
                                      "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                                      "IN"."_InterestBearingSwap.FinancialContractID" = "OLD"."_InterestBearingSwap.FinancialContractID" and
                                      "IN"."_InterestBearingSwap.IDSystem" = "OLD"."_InterestBearingSwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_AmortizationEndDate" as "AmortizationEndDate",
            "OLD_AmortizationStartDate" as "AmortizationStartDate",
            "OLD_RoleOfPayer" as "RoleOfPayer",
            "OLD__InterestBearingSwap.FinancialContractID" as "_InterestBearingSwap.FinancialContractID",
            "OLD__InterestBearingSwap.IDSystem" as "_InterestBearingSwap.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AmortizationAmount" as "AmortizationAmount",
            "OLD_AmortizationFrequency" as "AmortizationFrequency",
            "OLD_AmortizationFrequencyTimeUnit" as "AmortizationFrequencyTimeUnit",
            "OLD_AmortizationRate" as "AmortizationRate",
            "OLD_AmortizationRule" as "AmortizationRule",
            "OLD_AnnuityAmount" as "AnnuityAmount",
            "OLD_CouponReinvestmentPercentage" as "CouponReinvestmentPercentage",
            "OLD_PayFullCoupon" as "PayFullCoupon",
            "OLD_TargetNotionalAmount" as "TargetNotionalAmount",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."AmortizationEndDate",
                        "OLD"."AmortizationStartDate",
                        "OLD"."RoleOfPayer",
                        "OLD"."_InterestBearingSwap.FinancialContractID",
                        "OLD"."_InterestBearingSwap.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."AmortizationEndDate" AS "OLD_AmortizationEndDate" ,
                "OLD"."AmortizationStartDate" AS "OLD_AmortizationStartDate" ,
                "OLD"."RoleOfPayer" AS "OLD_RoleOfPayer" ,
                "OLD"."_InterestBearingSwap.FinancialContractID" AS "OLD__InterestBearingSwap.FinancialContractID" ,
                "OLD"."_InterestBearingSwap.IDSystem" AS "OLD__InterestBearingSwap.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AmortizationAmount" AS "OLD_AmortizationAmount" ,
                "OLD"."AmortizationFrequency" AS "OLD_AmortizationFrequency" ,
                "OLD"."AmortizationFrequencyTimeUnit" AS "OLD_AmortizationFrequencyTimeUnit" ,
                "OLD"."AmortizationRate" AS "OLD_AmortizationRate" ,
                "OLD"."AmortizationRule" AS "OLD_AmortizationRule" ,
                "OLD"."AnnuityAmount" AS "OLD_AnnuityAmount" ,
                "OLD"."CouponReinvestmentPercentage" AS "OLD_CouponReinvestmentPercentage" ,
                "OLD"."PayFullCoupon" AS "OLD_PayFullCoupon" ,
                "OLD"."TargetNotionalAmount" AS "OLD_TargetNotionalAmount" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::SwapAmortizationSpecification" as "OLD"
            on
               "IN"."AmortizationEndDate" = "OLD"."AmortizationEndDate" and
               "IN"."AmortizationStartDate" = "OLD"."AmortizationStartDate" and
               "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
               "IN"."_InterestBearingSwap.FinancialContractID" = "OLD"."_InterestBearingSwap.FinancialContractID" and
               "IN"."_InterestBearingSwap.IDSystem" = "OLD"."_InterestBearingSwap.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
