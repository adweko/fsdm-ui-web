PROCEDURE "sap.fsdm.procedures::DayOfWeekReadOnly" (IN ROW "sap.fsdm.tabletypes::DayOfWeekTT", OUT CURR_DEL "sap.fsdm.tabletypes::DayOfWeekTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::DayOfWeekTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DayOfWeek=' || TO_VARCHAR("DayOfWeek") || ' ' ||
                '_BusinessCalendar.BusinessCalendar=' || TO_VARCHAR("_BusinessCalendar.BusinessCalendar") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DayOfWeek",
                        "IN"."_BusinessCalendar.BusinessCalendar"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DayOfWeek",
                        "IN"."_BusinessCalendar.BusinessCalendar"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DayOfWeek",
                        "_BusinessCalendar.BusinessCalendar"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DayOfWeek",
                                    "IN"."_BusinessCalendar.BusinessCalendar"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DayOfWeek",
                                    "IN"."_BusinessCalendar.BusinessCalendar"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "DayOfWeek",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::DayOfWeek" WHERE
        (            "DayOfWeek" ,
            "_BusinessCalendar.BusinessCalendar" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."DayOfWeek",
            "OLD"."_BusinessCalendar.BusinessCalendar",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::DayOfWeek" as "OLD"
            on
               ifnull( "IN"."DayOfWeek",-1 ) = "OLD"."DayOfWeek" and
               ifnull( "IN"."_BusinessCalendar.BusinessCalendar",'' ) = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "DayOfWeek",
        "_BusinessCalendar.BusinessCalendar",
        "BusinessValidFrom",
        "BusinessValidTo",
        "IsWorkingDay",
        "NameOfDay",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "DayOfWeek", -1 ) as "DayOfWeek",
                    ifnull( "_BusinessCalendar.BusinessCalendar", '' ) as "_BusinessCalendar.BusinessCalendar",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "IsWorkingDay"  ,
                    "NameOfDay"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_DayOfWeek" as "DayOfWeek" ,
                    "OLD__BusinessCalendar.BusinessCalendar" as "_BusinessCalendar.BusinessCalendar" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_IsWorkingDay" as "IsWorkingDay" ,
                    "OLD_NameOfDay" as "NameOfDay" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."DayOfWeek",
                        "IN"."_BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."DayOfWeek" as "OLD_DayOfWeek",
                                "OLD"."_BusinessCalendar.BusinessCalendar" as "OLD__BusinessCalendar.BusinessCalendar",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."IsWorkingDay" as "OLD_IsWorkingDay",
                                "OLD"."NameOfDay" as "OLD_NameOfDay",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::DayOfWeek" as "OLD"
            on
                ifnull( "IN"."DayOfWeek", -1) = "OLD"."DayOfWeek" and
                ifnull( "IN"."_BusinessCalendar.BusinessCalendar", '') = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_DayOfWeek" as "DayOfWeek",
            "OLD__BusinessCalendar.BusinessCalendar" as "_BusinessCalendar.BusinessCalendar",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_IsWorkingDay" as "IsWorkingDay",
            "OLD_NameOfDay" as "NameOfDay",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."DayOfWeek",
                        "IN"."_BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."DayOfWeek" as "OLD_DayOfWeek",
                        "OLD"."_BusinessCalendar.BusinessCalendar" as "OLD__BusinessCalendar.BusinessCalendar",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."IsWorkingDay" as "OLD_IsWorkingDay",
                        "OLD"."NameOfDay" as "OLD_NameOfDay",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::DayOfWeek" as "OLD"
            on
                ifnull("IN"."DayOfWeek", -1) = "OLD"."DayOfWeek" and
                ifnull("IN"."_BusinessCalendar.BusinessCalendar", '') = "OLD"."_BusinessCalendar.BusinessCalendar" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
