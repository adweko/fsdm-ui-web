namespace "sap"."fsdm";

using "sap"."fsdm"::"AccountingEntityGLAccount";
using "sap"."fsdm"::"Fee";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"GLAccount";
using "sap"."fsdm"::"Interest";
using "sap"."fsdm"::"PaymentSchedule";

entity "Locking" {
    key "LockingType"                   : String(100);
    key "_AccountingEntityGLAccount"    : association to AccountingEntityGLAccount {
                                                                                     CompanyCode,
                                                                                     _GLAccount
                                                                                   } not null;
    key "_LockingOfFee"                 : association to Fee {
                                                               SequenceNumber,
                                                               ASSOC_FinancialContract,
                                                               ASSOC_TransferOrder,
                                                               _LeaseService,
                                                               _Trade,
                                                               _TrancheInSyndication
                                                             }                       not null;
    key "_LockingOfFinancialContract"   : association to FinancialContract {
                                                                             FinancialContractID,
                                                                             IDSystem
                                                                           }         not null;
    key "_LockingOfGLAccount"           : association to GLAccount {
                                                                     GLAccount,
                                                                     _ChartOfAccounts
                                                                   }                 not null;
    key "_LockingOfInterest"            : association to Interest {
                                                                    SequenceNumber,
                                                                    ASSOC_CcyOfMultiCcyAccnt,
                                                                    ASSOC_FinancialContract,
                                                                    _DebtInstrument,
                                                                    _OptionOfReferenceRateSpecification,
                                                                    _OptionOfStrikeSpecification,
                                                                    _Trade
                                                                  }                  not null;
    key "_LockingOfPaymentSchedule"     : association to PaymentSchedule {
                                                                           SequenceNumber,
                                                                           ASSOC_FinancialContract,
                                                                           ASSOC_Receivable,
                                                                           _CreditCardLoan,
                                                                           _DebtInstrument,
                                                                           _InflationOptionComponent,
                                                                           _InterestRateOptionComponent
                                                                         }           not null;
    key "BusinessValidFrom"             : LocalDate;
    key "BusinessValidTo"               : LocalDate;
        "SystemValidFrom"               : UTCTimestamp                               not null;
        "SystemValidTo"                 : UTCTimestamp                               not null;
        "LockingCategory"               : String(100);
        "ReasonForLocking"              : String(200);
        "SourceSystemID"                : String(128);
        "ChangeTimestampInSourceSystem" : UTCTimestamp;
        "ChangingUserInSourceSystem"    : String(128);
        "ChangingProcessType"           : String(40);
        "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};

entity "Locking_Historical" {
    "LockingType"                   : String(100)                                not null;
    "_AccountingEntityGLAccount"    : association to AccountingEntityGLAccount {
                                                                                 CompanyCode,
                                                                                 _GLAccount
                                                                               } not null;
    "_LockingOfFee"                 : association to Fee {
                                                           SequenceNumber,
                                                           ASSOC_FinancialContract,
                                                           ASSOC_TransferOrder,
                                                           _LeaseService,
                                                           _Trade,
                                                           _TrancheInSyndication
                                                         }                       not null;
    "_LockingOfFinancialContract"   : association to FinancialContract {
                                                                         FinancialContractID,
                                                                         IDSystem
                                                                       }         not null;
    "_LockingOfGLAccount"           : association to GLAccount {
                                                                 GLAccount,
                                                                 _ChartOfAccounts
                                                               }                 not null;
    "_LockingOfInterest"            : association to Interest {
                                                                SequenceNumber,
                                                                ASSOC_CcyOfMultiCcyAccnt,
                                                                ASSOC_FinancialContract,
                                                                _DebtInstrument,
                                                                _OptionOfReferenceRateSpecification,
                                                                _OptionOfStrikeSpecification,
                                                                _Trade
                                                              }                  not null;
    "_LockingOfPaymentSchedule"     : association to PaymentSchedule {
                                                                       SequenceNumber,
                                                                       ASSOC_FinancialContract,
                                                                       ASSOC_Receivable,
                                                                       _CreditCardLoan,
                                                                       _DebtInstrument,
                                                                       _InflationOptionComponent,
                                                                       _InterestRateOptionComponent
                                                                     }           not null;
    "BusinessValidFrom"             : LocalDate                                  not null;
    "BusinessValidTo"               : LocalDate                                  not null;
    "SystemValidFrom"               : UTCTimestamp                               not null;
    "SystemValidTo"                 : UTCTimestamp                               not null;
    "LockingCategory"               : String(100);
    "ReasonForLocking"              : String(200);
    "SourceSystemID"                : String(128);
    "ChangeTimestampInSourceSystem" : UTCTimestamp;
    "ChangingUserInSourceSystem"    : String(128);
    "ChangingProcessType"           : String(40);
    "ChangingProcessID"             : String(128);
}
technical configuration {
    column store;
};