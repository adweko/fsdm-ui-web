PROCEDURE "sap.fsdm.procedures::EndOfDayVolatilityObservationDelReadOnly" (IN ROW "sap.fsdm.tabletypes::EndOfDayVolatilityObservationTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::EndOfDayVolatilityObservationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::EndOfDayVolatilityObservationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BaseCurrency=' || TO_VARCHAR("BaseCurrency") || ' ' ||
                'CalculationMethod=' || TO_VARCHAR("CalculationMethod") || ' ' ||
                'CurrencyOfUnderlying=' || TO_VARCHAR("CurrencyOfUnderlying") || ' ' ||
                'DataProvider=' || TO_VARCHAR("DataProvider") || ' ' ||
                'ExpirationDateOfOption=' || TO_VARCHAR("ExpirationDateOfOption") || ' ' ||
                'MaturityDateOfUnderlying=' || TO_VARCHAR("MaturityDateOfUnderlying") || ' ' ||
                'MoneynessFactor=' || TO_VARCHAR("MoneynessFactor") || ' ' ||
                'PutorCall=' || TO_VARCHAR("PutorCall") || ' ' ||
                'QuoteCurrency=' || TO_VARCHAR("QuoteCurrency") || ' ' ||
                'StrikePriceOfOption=' || TO_VARCHAR("StrikePriceOfOption") || ' ' ||
                'TimeHorizon=' || TO_VARCHAR("TimeHorizon") || ' ' ||
                'TimeUnit=' || TO_VARCHAR("TimeUnit") || ' ' ||
                'VolatilityCategory=' || TO_VARCHAR("VolatilityCategory") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_Commodity.CommodityID=' || TO_VARCHAR("_Commodity.CommodityID") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_Index.IndexID=' || TO_VARCHAR("_Index.IndexID") || ' ' ||
                '_ReferenceRate.ReferenceRateID=' || TO_VARCHAR("_ReferenceRate.ReferenceRateID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BaseCurrency",
                        "IN"."CalculationMethod",
                        "IN"."CurrencyOfUnderlying",
                        "IN"."DataProvider",
                        "IN"."ExpirationDateOfOption",
                        "IN"."MaturityDateOfUnderlying",
                        "IN"."MoneynessFactor",
                        "IN"."PutorCall",
                        "IN"."QuoteCurrency",
                        "IN"."StrikePriceOfOption",
                        "IN"."TimeHorizon",
                        "IN"."TimeUnit",
                        "IN"."VolatilityCategory",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_Commodity.CommodityID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID",
                        "IN"."_ReferenceRate.ReferenceRateID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BaseCurrency",
                        "IN"."CalculationMethod",
                        "IN"."CurrencyOfUnderlying",
                        "IN"."DataProvider",
                        "IN"."ExpirationDateOfOption",
                        "IN"."MaturityDateOfUnderlying",
                        "IN"."MoneynessFactor",
                        "IN"."PutorCall",
                        "IN"."QuoteCurrency",
                        "IN"."StrikePriceOfOption",
                        "IN"."TimeHorizon",
                        "IN"."TimeUnit",
                        "IN"."VolatilityCategory",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_Commodity.CommodityID",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_Index.IndexID",
                        "IN"."_ReferenceRate.ReferenceRateID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BaseCurrency",
                        "CalculationMethod",
                        "CurrencyOfUnderlying",
                        "DataProvider",
                        "ExpirationDateOfOption",
                        "MaturityDateOfUnderlying",
                        "MoneynessFactor",
                        "PutorCall",
                        "QuoteCurrency",
                        "StrikePriceOfOption",
                        "TimeHorizon",
                        "TimeUnit",
                        "VolatilityCategory",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_Commodity.CommodityID",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_Index.IndexID",
                        "_ReferenceRate.ReferenceRateID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BaseCurrency",
                                    "IN"."CalculationMethod",
                                    "IN"."CurrencyOfUnderlying",
                                    "IN"."DataProvider",
                                    "IN"."ExpirationDateOfOption",
                                    "IN"."MaturityDateOfUnderlying",
                                    "IN"."MoneynessFactor",
                                    "IN"."PutorCall",
                                    "IN"."QuoteCurrency",
                                    "IN"."StrikePriceOfOption",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeUnit",
                                    "IN"."VolatilityCategory",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_Commodity.CommodityID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_Index.IndexID",
                                    "IN"."_ReferenceRate.ReferenceRateID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BaseCurrency",
                                    "IN"."CalculationMethod",
                                    "IN"."CurrencyOfUnderlying",
                                    "IN"."DataProvider",
                                    "IN"."ExpirationDateOfOption",
                                    "IN"."MaturityDateOfUnderlying",
                                    "IN"."MoneynessFactor",
                                    "IN"."PutorCall",
                                    "IN"."QuoteCurrency",
                                    "IN"."StrikePriceOfOption",
                                    "IN"."TimeHorizon",
                                    "IN"."TimeUnit",
                                    "IN"."VolatilityCategory",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_Commodity.CommodityID",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_Index.IndexID",
                                    "IN"."_ReferenceRate.ReferenceRateID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "BaseCurrency" is null and
            "CalculationMethod" is null and
            "CurrencyOfUnderlying" is null and
            "DataProvider" is null and
            "ExpirationDateOfOption" is null and
            "MaturityDateOfUnderlying" is null and
            "MoneynessFactor" is null and
            "PutorCall" is null and
            "QuoteCurrency" is null and
            "StrikePriceOfOption" is null and
            "TimeHorizon" is null and
            "TimeUnit" is null and
            "VolatilityCategory" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_Commodity.CommodityID" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null and
            "_Index.IndexID" is null and
            "_ReferenceRate.ReferenceRateID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "BaseCurrency",
            "CalculationMethod",
            "CurrencyOfUnderlying",
            "DataProvider",
            "ExpirationDateOfOption",
            "MaturityDateOfUnderlying",
            "MoneynessFactor",
            "PutorCall",
            "QuoteCurrency",
            "StrikePriceOfOption",
            "TimeHorizon",
            "TimeUnit",
            "VolatilityCategory",
            "_Collection.CollectionID",
            "_Collection.IDSystem",
            "_Collection._Client.BusinessPartnerID",
            "_Commodity.CommodityID",
            "_FinancialInstrument.FinancialInstrumentID",
            "_Index.IndexID",
            "_ReferenceRate.ReferenceRateID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::EndOfDayVolatilityObservation" WHERE
            (
            "BaseCurrency" ,
            "CalculationMethod" ,
            "CurrencyOfUnderlying" ,
            "DataProvider" ,
            "ExpirationDateOfOption" ,
            "MaturityDateOfUnderlying" ,
            "MoneynessFactor" ,
            "PutorCall" ,
            "QuoteCurrency" ,
            "StrikePriceOfOption" ,
            "TimeHorizon" ,
            "TimeUnit" ,
            "VolatilityCategory" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_Commodity.CommodityID" ,
            "_FinancialInstrument.FinancialInstrumentID" ,
            "_Index.IndexID" ,
            "_ReferenceRate.ReferenceRateID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."BaseCurrency",
            "OLD"."CalculationMethod",
            "OLD"."CurrencyOfUnderlying",
            "OLD"."DataProvider",
            "OLD"."ExpirationDateOfOption",
            "OLD"."MaturityDateOfUnderlying",
            "OLD"."MoneynessFactor",
            "OLD"."PutorCall",
            "OLD"."QuoteCurrency",
            "OLD"."StrikePriceOfOption",
            "OLD"."TimeHorizon",
            "OLD"."TimeUnit",
            "OLD"."VolatilityCategory",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_Commodity.CommodityID",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_Index.IndexID",
            "OLD"."_ReferenceRate.ReferenceRateID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::EndOfDayVolatilityObservation" as "OLD"
        on
                              "IN"."BaseCurrency" = "OLD"."BaseCurrency" and
                              "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
                              "IN"."CurrencyOfUnderlying" = "OLD"."CurrencyOfUnderlying" and
                              "IN"."DataProvider" = "OLD"."DataProvider" and
                              "IN"."ExpirationDateOfOption" = "OLD"."ExpirationDateOfOption" and
                              "IN"."MaturityDateOfUnderlying" = "OLD"."MaturityDateOfUnderlying" and
                              "IN"."MoneynessFactor" = "OLD"."MoneynessFactor" and
                              "IN"."PutorCall" = "OLD"."PutorCall" and
                              "IN"."QuoteCurrency" = "OLD"."QuoteCurrency" and
                              "IN"."StrikePriceOfOption" = "OLD"."StrikePriceOfOption" and
                              "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                              "IN"."TimeUnit" = "OLD"."TimeUnit" and
                              "IN"."VolatilityCategory" = "OLD"."VolatilityCategory" and
                              "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                              "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                              "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                              "IN"."_Commodity.CommodityID" = "OLD"."_Commodity.CommodityID" and
                              "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                              "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                              "IN"."_ReferenceRate.ReferenceRateID" = "OLD"."_ReferenceRate.ReferenceRateID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "BaseCurrency",
        "CalculationMethod",
        "CurrencyOfUnderlying",
        "DataProvider",
        "ExpirationDateOfOption",
        "MaturityDateOfUnderlying",
        "MoneynessFactor",
        "PutorCall",
        "QuoteCurrency",
        "StrikePriceOfOption",
        "TimeHorizon",
        "TimeUnit",
        "VolatilityCategory",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_Commodity.CommodityID",
        "_FinancialInstrument.FinancialInstrumentID",
        "_Index.IndexID",
        "_ReferenceRate.ReferenceRateID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AbsoluteVolatilityAmount",
        "AbsoluteVolatilityAmountCurrency",
        "AbsoluteVolatilityPercentageValue",
        "AbsoluteVolatilityPointValue",
        "ReturnsEndDate",
        "ReturnsPeriodLength",
        "ReturnsPeriodLengthTimeUnit",
        "ReturnsStartDate",
        "Volatility",
        "VolatilityObservationCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_BaseCurrency" as "BaseCurrency" ,
            "OLD_CalculationMethod" as "CalculationMethod" ,
            "OLD_CurrencyOfUnderlying" as "CurrencyOfUnderlying" ,
            "OLD_DataProvider" as "DataProvider" ,
            "OLD_ExpirationDateOfOption" as "ExpirationDateOfOption" ,
            "OLD_MaturityDateOfUnderlying" as "MaturityDateOfUnderlying" ,
            "OLD_MoneynessFactor" as "MoneynessFactor" ,
            "OLD_PutorCall" as "PutorCall" ,
            "OLD_QuoteCurrency" as "QuoteCurrency" ,
            "OLD_StrikePriceOfOption" as "StrikePriceOfOption" ,
            "OLD_TimeHorizon" as "TimeHorizon" ,
            "OLD_TimeUnit" as "TimeUnit" ,
            "OLD_VolatilityCategory" as "VolatilityCategory" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
            "OLD__Commodity.CommodityID" as "_Commodity.CommodityID" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__Index.IndexID" as "_Index.IndexID" ,
            "OLD__ReferenceRate.ReferenceRateID" as "_ReferenceRate.ReferenceRateID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AbsoluteVolatilityAmount" as "AbsoluteVolatilityAmount" ,
            "OLD_AbsoluteVolatilityAmountCurrency" as "AbsoluteVolatilityAmountCurrency" ,
            "OLD_AbsoluteVolatilityPercentageValue" as "AbsoluteVolatilityPercentageValue" ,
            "OLD_AbsoluteVolatilityPointValue" as "AbsoluteVolatilityPointValue" ,
            "OLD_ReturnsEndDate" as "ReturnsEndDate" ,
            "OLD_ReturnsPeriodLength" as "ReturnsPeriodLength" ,
            "OLD_ReturnsPeriodLengthTimeUnit" as "ReturnsPeriodLengthTimeUnit" ,
            "OLD_ReturnsStartDate" as "ReturnsStartDate" ,
            "OLD_Volatility" as "Volatility" ,
            "OLD_VolatilityObservationCategory" as "VolatilityObservationCategory" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."BaseCurrency",
                        "OLD"."CalculationMethod",
                        "OLD"."CurrencyOfUnderlying",
                        "OLD"."DataProvider",
                        "OLD"."ExpirationDateOfOption",
                        "OLD"."MaturityDateOfUnderlying",
                        "OLD"."MoneynessFactor",
                        "OLD"."PutorCall",
                        "OLD"."QuoteCurrency",
                        "OLD"."StrikePriceOfOption",
                        "OLD"."TimeHorizon",
                        "OLD"."TimeUnit",
                        "OLD"."VolatilityCategory",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_Commodity.CommodityID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_Index.IndexID",
                        "OLD"."_ReferenceRate.ReferenceRateID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."BaseCurrency" AS "OLD_BaseCurrency" ,
                "OLD"."CalculationMethod" AS "OLD_CalculationMethod" ,
                "OLD"."CurrencyOfUnderlying" AS "OLD_CurrencyOfUnderlying" ,
                "OLD"."DataProvider" AS "OLD_DataProvider" ,
                "OLD"."ExpirationDateOfOption" AS "OLD_ExpirationDateOfOption" ,
                "OLD"."MaturityDateOfUnderlying" AS "OLD_MaturityDateOfUnderlying" ,
                "OLD"."MoneynessFactor" AS "OLD_MoneynessFactor" ,
                "OLD"."PutorCall" AS "OLD_PutorCall" ,
                "OLD"."QuoteCurrency" AS "OLD_QuoteCurrency" ,
                "OLD"."StrikePriceOfOption" AS "OLD_StrikePriceOfOption" ,
                "OLD"."TimeHorizon" AS "OLD_TimeHorizon" ,
                "OLD"."TimeUnit" AS "OLD_TimeUnit" ,
                "OLD"."VolatilityCategory" AS "OLD_VolatilityCategory" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_Commodity.CommodityID" AS "OLD__Commodity.CommodityID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_Index.IndexID" AS "OLD__Index.IndexID" ,
                "OLD"."_ReferenceRate.ReferenceRateID" AS "OLD__ReferenceRate.ReferenceRateID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AbsoluteVolatilityAmount" AS "OLD_AbsoluteVolatilityAmount" ,
                "OLD"."AbsoluteVolatilityAmountCurrency" AS "OLD_AbsoluteVolatilityAmountCurrency" ,
                "OLD"."AbsoluteVolatilityPercentageValue" AS "OLD_AbsoluteVolatilityPercentageValue" ,
                "OLD"."AbsoluteVolatilityPointValue" AS "OLD_AbsoluteVolatilityPointValue" ,
                "OLD"."ReturnsEndDate" AS "OLD_ReturnsEndDate" ,
                "OLD"."ReturnsPeriodLength" AS "OLD_ReturnsPeriodLength" ,
                "OLD"."ReturnsPeriodLengthTimeUnit" AS "OLD_ReturnsPeriodLengthTimeUnit" ,
                "OLD"."ReturnsStartDate" AS "OLD_ReturnsStartDate" ,
                "OLD"."Volatility" AS "OLD_Volatility" ,
                "OLD"."VolatilityObservationCategory" AS "OLD_VolatilityObservationCategory" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EndOfDayVolatilityObservation" as "OLD"
            on
                                      "IN"."BaseCurrency" = "OLD"."BaseCurrency" and
                                      "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
                                      "IN"."CurrencyOfUnderlying" = "OLD"."CurrencyOfUnderlying" and
                                      "IN"."DataProvider" = "OLD"."DataProvider" and
                                      "IN"."ExpirationDateOfOption" = "OLD"."ExpirationDateOfOption" and
                                      "IN"."MaturityDateOfUnderlying" = "OLD"."MaturityDateOfUnderlying" and
                                      "IN"."MoneynessFactor" = "OLD"."MoneynessFactor" and
                                      "IN"."PutorCall" = "OLD"."PutorCall" and
                                      "IN"."QuoteCurrency" = "OLD"."QuoteCurrency" and
                                      "IN"."StrikePriceOfOption" = "OLD"."StrikePriceOfOption" and
                                      "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
                                      "IN"."TimeUnit" = "OLD"."TimeUnit" and
                                      "IN"."VolatilityCategory" = "OLD"."VolatilityCategory" and
                                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                      "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                                      "IN"."_Commodity.CommodityID" = "OLD"."_Commodity.CommodityID" and
                                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                                      "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
                                      "IN"."_ReferenceRate.ReferenceRateID" = "OLD"."_ReferenceRate.ReferenceRateID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_BaseCurrency" as "BaseCurrency",
            "OLD_CalculationMethod" as "CalculationMethod",
            "OLD_CurrencyOfUnderlying" as "CurrencyOfUnderlying",
            "OLD_DataProvider" as "DataProvider",
            "OLD_ExpirationDateOfOption" as "ExpirationDateOfOption",
            "OLD_MaturityDateOfUnderlying" as "MaturityDateOfUnderlying",
            "OLD_MoneynessFactor" as "MoneynessFactor",
            "OLD_PutorCall" as "PutorCall",
            "OLD_QuoteCurrency" as "QuoteCurrency",
            "OLD_StrikePriceOfOption" as "StrikePriceOfOption",
            "OLD_TimeHorizon" as "TimeHorizon",
            "OLD_TimeUnit" as "TimeUnit",
            "OLD_VolatilityCategory" as "VolatilityCategory",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__Commodity.CommodityID" as "_Commodity.CommodityID",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__Index.IndexID" as "_Index.IndexID",
            "OLD__ReferenceRate.ReferenceRateID" as "_ReferenceRate.ReferenceRateID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AbsoluteVolatilityAmount" as "AbsoluteVolatilityAmount",
            "OLD_AbsoluteVolatilityAmountCurrency" as "AbsoluteVolatilityAmountCurrency",
            "OLD_AbsoluteVolatilityPercentageValue" as "AbsoluteVolatilityPercentageValue",
            "OLD_AbsoluteVolatilityPointValue" as "AbsoluteVolatilityPointValue",
            "OLD_ReturnsEndDate" as "ReturnsEndDate",
            "OLD_ReturnsPeriodLength" as "ReturnsPeriodLength",
            "OLD_ReturnsPeriodLengthTimeUnit" as "ReturnsPeriodLengthTimeUnit",
            "OLD_ReturnsStartDate" as "ReturnsStartDate",
            "OLD_Volatility" as "Volatility",
            "OLD_VolatilityObservationCategory" as "VolatilityObservationCategory",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."BaseCurrency",
                        "OLD"."CalculationMethod",
                        "OLD"."CurrencyOfUnderlying",
                        "OLD"."DataProvider",
                        "OLD"."ExpirationDateOfOption",
                        "OLD"."MaturityDateOfUnderlying",
                        "OLD"."MoneynessFactor",
                        "OLD"."PutorCall",
                        "OLD"."QuoteCurrency",
                        "OLD"."StrikePriceOfOption",
                        "OLD"."TimeHorizon",
                        "OLD"."TimeUnit",
                        "OLD"."VolatilityCategory",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_Commodity.CommodityID",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_Index.IndexID",
                        "OLD"."_ReferenceRate.ReferenceRateID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."BaseCurrency" AS "OLD_BaseCurrency" ,
                "OLD"."CalculationMethod" AS "OLD_CalculationMethod" ,
                "OLD"."CurrencyOfUnderlying" AS "OLD_CurrencyOfUnderlying" ,
                "OLD"."DataProvider" AS "OLD_DataProvider" ,
                "OLD"."ExpirationDateOfOption" AS "OLD_ExpirationDateOfOption" ,
                "OLD"."MaturityDateOfUnderlying" AS "OLD_MaturityDateOfUnderlying" ,
                "OLD"."MoneynessFactor" AS "OLD_MoneynessFactor" ,
                "OLD"."PutorCall" AS "OLD_PutorCall" ,
                "OLD"."QuoteCurrency" AS "OLD_QuoteCurrency" ,
                "OLD"."StrikePriceOfOption" AS "OLD_StrikePriceOfOption" ,
                "OLD"."TimeHorizon" AS "OLD_TimeHorizon" ,
                "OLD"."TimeUnit" AS "OLD_TimeUnit" ,
                "OLD"."VolatilityCategory" AS "OLD_VolatilityCategory" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_Commodity.CommodityID" AS "OLD__Commodity.CommodityID" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."_Index.IndexID" AS "OLD__Index.IndexID" ,
                "OLD"."_ReferenceRate.ReferenceRateID" AS "OLD__ReferenceRate.ReferenceRateID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."AbsoluteVolatilityAmount" AS "OLD_AbsoluteVolatilityAmount" ,
                "OLD"."AbsoluteVolatilityAmountCurrency" AS "OLD_AbsoluteVolatilityAmountCurrency" ,
                "OLD"."AbsoluteVolatilityPercentageValue" AS "OLD_AbsoluteVolatilityPercentageValue" ,
                "OLD"."AbsoluteVolatilityPointValue" AS "OLD_AbsoluteVolatilityPointValue" ,
                "OLD"."ReturnsEndDate" AS "OLD_ReturnsEndDate" ,
                "OLD"."ReturnsPeriodLength" AS "OLD_ReturnsPeriodLength" ,
                "OLD"."ReturnsPeriodLengthTimeUnit" AS "OLD_ReturnsPeriodLengthTimeUnit" ,
                "OLD"."ReturnsStartDate" AS "OLD_ReturnsStartDate" ,
                "OLD"."Volatility" AS "OLD_Volatility" ,
                "OLD"."VolatilityObservationCategory" AS "OLD_VolatilityObservationCategory" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::EndOfDayVolatilityObservation" as "OLD"
            on
               "IN"."BaseCurrency" = "OLD"."BaseCurrency" and
               "IN"."CalculationMethod" = "OLD"."CalculationMethod" and
               "IN"."CurrencyOfUnderlying" = "OLD"."CurrencyOfUnderlying" and
               "IN"."DataProvider" = "OLD"."DataProvider" and
               "IN"."ExpirationDateOfOption" = "OLD"."ExpirationDateOfOption" and
               "IN"."MaturityDateOfUnderlying" = "OLD"."MaturityDateOfUnderlying" and
               "IN"."MoneynessFactor" = "OLD"."MoneynessFactor" and
               "IN"."PutorCall" = "OLD"."PutorCall" and
               "IN"."QuoteCurrency" = "OLD"."QuoteCurrency" and
               "IN"."StrikePriceOfOption" = "OLD"."StrikePriceOfOption" and
               "IN"."TimeHorizon" = "OLD"."TimeHorizon" and
               "IN"."TimeUnit" = "OLD"."TimeUnit" and
               "IN"."VolatilityCategory" = "OLD"."VolatilityCategory" and
               "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
               "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
               "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
               "IN"."_Commodity.CommodityID" = "OLD"."_Commodity.CommodityID" and
               "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
               "IN"."_Index.IndexID" = "OLD"."_Index.IndexID" and
               "IN"."_ReferenceRate.ReferenceRateID" = "OLD"."_ReferenceRate.ReferenceRateID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
