PROCEDURE "sap.fsdm.procedures::EuropeanRegulatoryReportingForCountryEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::EuropeanRegulatoryReportingForCountryTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::EuropeanRegulatoryReportingForCountryTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::EuropeanRegulatoryReportingForCountryTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Country.GeographicalStructureID" is null and
            "_Country.GeographicalUnitID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_Country.GeographicalStructureID" ,
                "_Country.GeographicalUnitID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Country.GeographicalStructureID" ,
                "OLD"."_Country.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EuropeanRegulatoryReportingForCountry" "OLD"
            on
                "IN"."_Country.GeographicalStructureID" = "OLD"."_Country.GeographicalStructureID" and
                "IN"."_Country.GeographicalUnitID" = "OLD"."_Country.GeographicalUnitID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_Country.GeographicalStructureID" ,
            "_Country.GeographicalUnitID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_Country.GeographicalStructureID" ,
                "OLD"."_Country.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::EuropeanRegulatoryReportingForCountry_Historical" "OLD"
            on
                "IN"."_Country.GeographicalStructureID" = "OLD"."_Country.GeographicalStructureID" and
                "IN"."_Country.GeographicalUnitID" = "OLD"."_Country.GeographicalUnitID" 
        );

END
