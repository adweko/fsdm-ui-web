namespace "sap"."fsdm";

using "sap"."fsdm"::"BusinessPartner";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"FinancialInstrument";

entity "ProbationPhase" {
    key "ProbationRelatedTo"              : String(100)                                                  default '';
    key "_BusinessPartner"                : association to BusinessPartner { BusinessPartnerID }         not null;
    key "_FinancialContract"              : association to FinancialContract {
                                                                               FinancialContractID,
                                                                               IDSystem
                                                                             }                           not null;
    key "_FinancialInstrument"            : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "BusinessValidFrom"               : LocalDate;
    key "BusinessValidTo"                 : LocalDate;
        "SystemValidFrom"                 : UTCTimestamp                                                 not null;
        "SystemValidTo"                   : UTCTimestamp                                                 not null;
        "EndDateOfActiveProbationPhase"   : LocalDate;
        "ExpectedEndDate"                 : LocalDate;
        "ProbationPhaseLength"            : Decimal(34, 6);
        "ProbationPhaseTimeUnit"          : String(128);
        "StartDateOfActiveProbationPhase" : LocalDate;
        "SourceSystemID"                  : String(128);
        "ChangeTimestampInSourceSystem"   : UTCTimestamp;
        "ChangingUserInSourceSystem"      : String(128);
        "ChangingProcessType"             : String(40);
        "ChangingProcessID"               : String(128);
}
technical configuration {
    column store;
};

entity "ProbationPhase_Historical" {
    "ProbationRelatedTo"              : String(100)                                                  default '' not null;
    "_BusinessPartner"                : association to BusinessPartner { BusinessPartnerID }         not null;
    "_FinancialContract"              : association to FinancialContract {
                                                                           FinancialContractID,
                                                                           IDSystem
                                                                         }                           not null;
    "_FinancialInstrument"            : association to FinancialInstrument { FinancialInstrumentID } not null;
    "BusinessValidFrom"               : LocalDate                                                    not null;
    "BusinessValidTo"                 : LocalDate                                                    not null;
    "SystemValidFrom"                 : UTCTimestamp                                                 not null;
    "SystemValidTo"                   : UTCTimestamp                                                 not null;
    "EndDateOfActiveProbationPhase"   : LocalDate;
    "ExpectedEndDate"                 : LocalDate;
    "ProbationPhaseLength"            : Decimal(34, 6);
    "ProbationPhaseTimeUnit"          : String(128);
    "StartDateOfActiveProbationPhase" : LocalDate;
    "SourceSystemID"                  : String(128);
    "ChangeTimestampInSourceSystem"   : UTCTimestamp;
    "ChangingUserInSourceSystem"      : String(128);
    "ChangingProcessType"             : String(40);
    "ChangingProcessID"               : String(128);
}
technical configuration {
    column store;
};