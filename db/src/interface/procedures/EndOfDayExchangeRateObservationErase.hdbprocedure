PROCEDURE "sap.fsdm.procedures::EndOfDayExchangeRateObservationErase" (IN ROW "sap.fsdm.tabletypes::EndOfDayExchangeRateObservationTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "BaseCurrency" is null and
            "ExchangeRateType" is null and
            "PriceDataProvider" is null and
            "QuotationType" is null and
            "QuoteCurrency" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::EndOfDayExchangeRateObservation"
        WHERE
        (            "BaseCurrency" ,
            "ExchangeRateType" ,
            "PriceDataProvider" ,
            "QuotationType" ,
            "QuoteCurrency" 
        ) in
        (
            select                 "OLD"."BaseCurrency" ,
                "OLD"."ExchangeRateType" ,
                "OLD"."PriceDataProvider" ,
                "OLD"."QuotationType" ,
                "OLD"."QuoteCurrency" 
            from :ROW "IN"
            inner join "sap.fsdm::EndOfDayExchangeRateObservation" "OLD"
            on
            "IN"."BaseCurrency" = "OLD"."BaseCurrency" and
            "IN"."ExchangeRateType" = "OLD"."ExchangeRateType" and
            "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
            "IN"."QuotationType" = "OLD"."QuotationType" and
            "IN"."QuoteCurrency" = "OLD"."QuoteCurrency" 
        );

        --delete data from history table
        delete from "sap.fsdm::EndOfDayExchangeRateObservation_Historical"
        WHERE
        (
            "BaseCurrency" ,
            "ExchangeRateType" ,
            "PriceDataProvider" ,
            "QuotationType" ,
            "QuoteCurrency" 
        ) in
        (
            select
                "OLD"."BaseCurrency" ,
                "OLD"."ExchangeRateType" ,
                "OLD"."PriceDataProvider" ,
                "OLD"."QuotationType" ,
                "OLD"."QuoteCurrency" 
            from :ROW "IN"
            inner join "sap.fsdm::EndOfDayExchangeRateObservation_Historical" "OLD"
            on
                "IN"."BaseCurrency" = "OLD"."BaseCurrency" and
                "IN"."ExchangeRateType" = "OLD"."ExchangeRateType" and
                "IN"."PriceDataProvider" = "OLD"."PriceDataProvider" and
                "IN"."QuotationType" = "OLD"."QuotationType" and
                "IN"."QuoteCurrency" = "OLD"."QuoteCurrency" 
        );

END
