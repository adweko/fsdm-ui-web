PROCEDURE "sap.fsdm.procedures::AccruedPerformanceThresholdEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::AccruedPerformanceThresholdTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::AccruedPerformanceThresholdTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::AccruedPerformanceThresholdTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_IndexSpecification._Index.IndexID" is null and
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" is null and
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" is null and
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" is null and
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" is null and
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" is null and
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "_IndexSpecification._Index.IndexID" ,
                "_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" ,
                "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
                "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."_IndexSpecification._Index.IndexID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::AccruedPerformanceThreshold" "OLD"
            on
                "IN"."_IndexSpecification._Index.IndexID" = "OLD"."_IndexSpecification._Index.IndexID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "_IndexSpecification._Index.IndexID" ,
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" ,
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
            "_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."_IndexSpecification._Index.IndexID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" ,
                "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::AccruedPerformanceThreshold_Historical" "OLD"
            on
                "IN"."_IndexSpecification._Index.IndexID" = "OLD"."_IndexSpecification._Index.IndexID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage.ID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.FinancialContractID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuranceContract.IDSystem" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.FinancialContractID" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._FinancialContract.IDSystem" and
                "IN"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" = "OLD"."_IndexSpecification._LifeAndAnnuityInsuranceCoverage._InsuredObject._PhysicalAsset.PhysicalAssetID" 
        );

END
