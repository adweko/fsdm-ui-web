PROCEDURE "sap.fsdm.procedures::ClassificationOfFundDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ClassificationOfFundTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ClassificationOfFundTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ClassificationOfFundTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_Fund.FundID=' || TO_VARCHAR("_Fund.FundID") || ' ' ||
                '_Fund._InvestmentCorporation.BusinessPartnerID=' || TO_VARCHAR("_Fund._InvestmentCorporation.BusinessPartnerID") || ' ' ||
                '_FundClass.FundClass=' || TO_VARCHAR("_FundClass.FundClass") || ' ' ||
                '_FundClass.FundClassificationSystem=' || TO_VARCHAR("_FundClass.FundClassificationSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_Fund.FundID",
                        "IN"."_Fund._InvestmentCorporation.BusinessPartnerID",
                        "IN"."_FundClass.FundClass",
                        "IN"."_FundClass.FundClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_Fund.FundID",
                        "IN"."_Fund._InvestmentCorporation.BusinessPartnerID",
                        "IN"."_FundClass.FundClass",
                        "IN"."_FundClass.FundClassificationSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_Fund.FundID",
                        "_Fund._InvestmentCorporation.BusinessPartnerID",
                        "_FundClass.FundClass",
                        "_FundClass.FundClassificationSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_Fund.FundID",
                                    "IN"."_Fund._InvestmentCorporation.BusinessPartnerID",
                                    "IN"."_FundClass.FundClass",
                                    "IN"."_FundClass.FundClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_Fund.FundID",
                                    "IN"."_Fund._InvestmentCorporation.BusinessPartnerID",
                                    "IN"."_FundClass.FundClass",
                                    "IN"."_FundClass.FundClassificationSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_Fund.FundID" is null and
            "_Fund._InvestmentCorporation.BusinessPartnerID" is null and
            "_FundClass.FundClass" is null and
            "_FundClass.FundClassificationSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_Fund.FundID",
            "_Fund._InvestmentCorporation.BusinessPartnerID",
            "_FundClass.FundClass",
            "_FundClass.FundClassificationSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::ClassificationOfFund" WHERE
            (
            "_Fund.FundID" ,
            "_Fund._InvestmentCorporation.BusinessPartnerID" ,
            "_FundClass.FundClass" ,
            "_FundClass.FundClassificationSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_Fund.FundID",
            "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID",
            "OLD"."_FundClass.FundClass",
            "OLD"."_FundClass.FundClassificationSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ClassificationOfFund" as "OLD"
        on
                              "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                              "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                              "IN"."_FundClass.FundClass" = "OLD"."_FundClass.FundClass" and
                              "IN"."_FundClass.FundClassificationSystem" = "OLD"."_FundClass.FundClassificationSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_Fund.FundID",
        "_Fund._InvestmentCorporation.BusinessPartnerID",
        "_FundClass.FundClass",
        "_FundClass.FundClassificationSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__Fund.FundID" as "_Fund.FundID" ,
            "OLD__Fund._InvestmentCorporation.BusinessPartnerID" as "_Fund._InvestmentCorporation.BusinessPartnerID" ,
            "OLD__FundClass.FundClass" as "_FundClass.FundClass" ,
            "OLD__FundClass.FundClassificationSystem" as "_FundClass.FundClassificationSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_Fund.FundID",
                        "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID",
                        "OLD"."_FundClass.FundClass",
                        "OLD"."_FundClass.FundClassificationSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_Fund.FundID" AS "OLD__Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" AS "OLD__Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_FundClass.FundClass" AS "OLD__FundClass.FundClass" ,
                "OLD"."_FundClass.FundClassificationSystem" AS "OLD__FundClass.FundClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClassificationOfFund" as "OLD"
            on
                                      "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
                                      "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
                                      "IN"."_FundClass.FundClass" = "OLD"."_FundClass.FundClass" and
                                      "IN"."_FundClass.FundClassificationSystem" = "OLD"."_FundClass.FundClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__Fund.FundID" as "_Fund.FundID",
            "OLD__Fund._InvestmentCorporation.BusinessPartnerID" as "_Fund._InvestmentCorporation.BusinessPartnerID",
            "OLD__FundClass.FundClass" as "_FundClass.FundClass",
            "OLD__FundClass.FundClassificationSystem" as "_FundClass.FundClassificationSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_Fund.FundID",
                        "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID",
                        "OLD"."_FundClass.FundClass",
                        "OLD"."_FundClass.FundClassificationSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_Fund.FundID" AS "OLD__Fund.FundID" ,
                "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" AS "OLD__Fund._InvestmentCorporation.BusinessPartnerID" ,
                "OLD"."_FundClass.FundClass" AS "OLD__FundClass.FundClass" ,
                "OLD"."_FundClass.FundClassificationSystem" AS "OLD__FundClass.FundClassificationSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ClassificationOfFund" as "OLD"
            on
               "IN"."_Fund.FundID" = "OLD"."_Fund.FundID" and
               "IN"."_Fund._InvestmentCorporation.BusinessPartnerID" = "OLD"."_Fund._InvestmentCorporation.BusinessPartnerID" and
               "IN"."_FundClass.FundClass" = "OLD"."_FundClass.FundClass" and
               "IN"."_FundClass.FundClassificationSystem" = "OLD"."_FundClass.FundClassificationSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
