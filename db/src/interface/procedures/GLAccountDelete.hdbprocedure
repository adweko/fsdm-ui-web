PROCEDURE "sap.fsdm.procedures::GLAccountDelete" (IN ROW "sap.fsdm.tabletypes::GLAccountTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'GLAccount=' || TO_VARCHAR("GLAccount") || ' ' ||
                '_ChartOfAccounts.ChartOfAccountId=' || TO_VARCHAR("_ChartOfAccounts.ChartOfAccountId") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."GLAccount",
                        "IN"."_ChartOfAccounts.ChartOfAccountId"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."GLAccount",
                        "IN"."_ChartOfAccounts.ChartOfAccountId"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "GLAccount",
                        "_ChartOfAccounts.ChartOfAccountId"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."GLAccount",
                                    "IN"."_ChartOfAccounts.ChartOfAccountId"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."GLAccount",
                                    "IN"."_ChartOfAccounts.ChartOfAccountId"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "GLAccount" is null and
            "_ChartOfAccounts.ChartOfAccountId" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::GLAccount" (
        "GLAccount",
        "_ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_GLAccountGroup.GLAccountGroupID",
        "AssetLiabilityAssignment",
        "GLAccountCurrency",
        "GLAccountDescription",
        "GLAccountType",
        "OpeningDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_GLAccount" as "GLAccount" ,
            "OLD__ChartOfAccounts.ChartOfAccountId" as "_ChartOfAccounts.ChartOfAccountId" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__GLAccountGroup.GLAccountGroupID" as "_GLAccountGroup.GLAccountGroupID" ,
            "OLD_AssetLiabilityAssignment" as "AssetLiabilityAssignment" ,
            "OLD_GLAccountCurrency" as "GLAccountCurrency" ,
            "OLD_GLAccountDescription" as "GLAccountDescription" ,
            "OLD_GLAccountType" as "GLAccountType" ,
            "OLD_OpeningDate" as "OpeningDate" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."GLAccount",
                        "OLD"."_ChartOfAccounts.ChartOfAccountId",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."GLAccount" AS "OLD_GLAccount" ,
                "OLD"."_ChartOfAccounts.ChartOfAccountId" AS "OLD__ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_GLAccountGroup.GLAccountGroupID" AS "OLD__GLAccountGroup.GLAccountGroupID" ,
                "OLD"."AssetLiabilityAssignment" AS "OLD_AssetLiabilityAssignment" ,
                "OLD"."GLAccountCurrency" AS "OLD_GLAccountCurrency" ,
                "OLD"."GLAccountDescription" AS "OLD_GLAccountDescription" ,
                "OLD"."GLAccountType" AS "OLD_GLAccountType" ,
                "OLD"."OpeningDate" AS "OLD_OpeningDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::GLAccount" as "OLD"
            on
                      "IN"."GLAccount" = "OLD"."GLAccount" and
                      "IN"."_ChartOfAccounts.ChartOfAccountId" = "OLD"."_ChartOfAccounts.ChartOfAccountId" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::GLAccount" (
        "GLAccount",
        "_ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_GLAccountGroup.GLAccountGroupID",
        "AssetLiabilityAssignment",
        "GLAccountCurrency",
        "GLAccountDescription",
        "GLAccountType",
        "OpeningDate",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_GLAccount" as "GLAccount",
            "OLD__ChartOfAccounts.ChartOfAccountId" as "_ChartOfAccounts.ChartOfAccountId",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__GLAccountGroup.GLAccountGroupID" as "_GLAccountGroup.GLAccountGroupID",
            "OLD_AssetLiabilityAssignment" as "AssetLiabilityAssignment",
            "OLD_GLAccountCurrency" as "GLAccountCurrency",
            "OLD_GLAccountDescription" as "GLAccountDescription",
            "OLD_GLAccountType" as "GLAccountType",
            "OLD_OpeningDate" as "OpeningDate",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."GLAccount",
                        "OLD"."_ChartOfAccounts.ChartOfAccountId",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."GLAccount" AS "OLD_GLAccount" ,
                "OLD"."_ChartOfAccounts.ChartOfAccountId" AS "OLD__ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_GLAccountGroup.GLAccountGroupID" AS "OLD__GLAccountGroup.GLAccountGroupID" ,
                "OLD"."AssetLiabilityAssignment" AS "OLD_AssetLiabilityAssignment" ,
                "OLD"."GLAccountCurrency" AS "OLD_GLAccountCurrency" ,
                "OLD"."GLAccountDescription" AS "OLD_GLAccountDescription" ,
                "OLD"."GLAccountType" AS "OLD_GLAccountType" ,
                "OLD"."OpeningDate" AS "OLD_OpeningDate" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::GLAccount" as "OLD"
            on
                                                "IN"."GLAccount" = "OLD"."GLAccount" and
                                                "IN"."_ChartOfAccounts.ChartOfAccountId" = "OLD"."_ChartOfAccounts.ChartOfAccountId" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::GLAccount"
    where (
        "GLAccount",
        "_ChartOfAccounts.ChartOfAccountId",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."GLAccount",
            "OLD"."_ChartOfAccounts.ChartOfAccountId",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::GLAccount" as "OLD"
        on
                                       "IN"."GLAccount" = "OLD"."GLAccount" and
                                       "IN"."_ChartOfAccounts.ChartOfAccountId" = "OLD"."_ChartOfAccounts.ChartOfAccountId" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
