PROCEDURE "sap.fsdm.procedures::LeaseServiceErase" (IN ROW "sap.fsdm.tabletypes::LeaseServiceTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "ID" is null and
            "_LeaseServiceAgreement.FinancialContractID" is null and
            "_LeaseServiceAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::LeaseService"
        WHERE
        (            "ID" ,
            "_LeaseServiceAgreement.FinancialContractID" ,
            "_LeaseServiceAgreement.IDSystem" 
        ) in
        (
            select                 "OLD"."ID" ,
                "OLD"."_LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LeaseServiceAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::LeaseService" "OLD"
            on
            "IN"."ID" = "OLD"."ID" and
            "IN"."_LeaseServiceAgreement.FinancialContractID" = "OLD"."_LeaseServiceAgreement.FinancialContractID" and
            "IN"."_LeaseServiceAgreement.IDSystem" = "OLD"."_LeaseServiceAgreement.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::LeaseService_Historical"
        WHERE
        (
            "ID" ,
            "_LeaseServiceAgreement.FinancialContractID" ,
            "_LeaseServiceAgreement.IDSystem" 
        ) in
        (
            select
                "OLD"."ID" ,
                "OLD"."_LeaseServiceAgreement.FinancialContractID" ,
                "OLD"."_LeaseServiceAgreement.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::LeaseService_Historical" "OLD"
            on
                "IN"."ID" = "OLD"."ID" and
                "IN"."_LeaseServiceAgreement.FinancialContractID" = "OLD"."_LeaseServiceAgreement.FinancialContractID" and
                "IN"."_LeaseServiceAgreement.IDSystem" = "OLD"."_LeaseServiceAgreement.IDSystem" 
        );

END
