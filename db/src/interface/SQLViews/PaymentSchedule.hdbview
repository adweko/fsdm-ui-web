view "sap.fsdm.SQLViews::PaymentSchedule_View" 
as select
      "SequenceNumber" ,
      "ASSOC_FinancialContract.FinancialContractID" ,
      "ASSOC_FinancialContract.IDSystem" ,
      "ASSOC_Receivable.ReceivableID" ,
      "_CreditCardLoan.ID" ,
      "_CreditCardLoan._CreditCardAgreement.FinancialContractID" ,
      "_CreditCardLoan._CreditCardAgreement.IDSystem" ,
      "_DebtInstrument.FinancialInstrumentID" ,
      "_InflationOptionComponent.ComponentNumber" ,
      "_InflationOptionComponent._InflationOption.FinancialContractID" ,
      "_InflationOptionComponent._InflationOption.IDSystem" ,
      "_InterestRateOptionComponent.ComponentNumber" ,
      "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" ,
      "_InterestRateOptionComponent._InterestRateOption.IDSystem" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "BulletPaymentAmount" ,
      "BulletPaymentCurrency" ,
      "BulletPaymentDate" ,
      "BusinessCalendar" ,
      "BusinessDayConvention" ,
      "ConditionAcceptanceDate" ,
      "ConditionFixedPeriodEndDate" ,
      "ConditionFixedPeriodStartDate" ,
      "DayOfMonthOfPayment" ,
      "FirstPaymentDate" ,
      "InstallmentAmount" ,
      "InstallmentCurrency" ,
      "InstallmentLagPeriodLength" ,
      "InstallmentLagTimeUnit" ,
      "InstallmentPeriodLength" ,
      "InstallmentPeriodTimeUnit" ,
      "InstallmentsRelativeToInterestPaymentDates" ,
      "LastPaymentDate" ,
      "ManualBusinessDayOffset" ,
      "PartialPaymentRequestsPermitted" ,
      "PaymentScheduleCategory" ,
      "PaymentScheduleRole" ,
      "PaymentScheduleStartDate" ,
      "PaymentScheduleType" ,
      "PaymentToBeRequested" ,
      "ProRataSinking" ,
      "RepaymentBySinking" ,
      "RoleOfPayer" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::PaymentSchedule"
with associations
(
	join "sap.fsdm.SQLViews::Locking_View" as "_Locking__LockingOfPaymentSchedule"
	  on "_DebtInstrument.FinancialInstrumentID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._DebtInstrument.FinancialInstrumentID" AND 
	     "_CreditCardLoan._CreditCardAgreement.IDSystem" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" AND 
	     "SequenceNumber" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule.SequenceNumber" AND 
	     "ASSOC_Receivable.ReceivableID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule.ASSOC_Receivable.ReceivableID" AND 
	     "_CreditCardLoan.ID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._CreditCardLoan.ID" AND 
	     "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" AND 
	     "_InterestRateOptionComponent._InterestRateOption.IDSystem" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" AND 
	     "_InflationOptionComponent.ComponentNumber" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._InflationOptionComponent.ComponentNumber" AND 
	     "_InterestRateOptionComponent.ComponentNumber" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._InterestRateOptionComponent.ComponentNumber" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.FinancialContractID" AND 
	     "_InflationOptionComponent._InflationOption.IDSystem" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.IDSystem" AND 
	     "_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule.ASSOC_FinancialContract.IDSystem" AND 
	     "_InflationOptionComponent._InflationOption.FinancialContractID" = "_Locking__LockingOfPaymentSchedule"."_LockingOfPaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID"
	     ,
	join "sap.fsdm.SQLViews::PaymentIntervalListEntry_View" as "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"
	  on "_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.FinancialContractID" AND 
	     "SequenceNumber" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList.SequenceNumber" AND 
	     "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._DebtInstrument.FinancialInstrumentID" AND 
	     "_CreditCardLoan._CreditCardAgreement.IDSystem" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._CreditCardLoan._CreditCardAgreement.IDSystem" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.IDSystem" AND 
	     "_InterestRateOptionComponent._InterestRateOption.IDSystem" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._InterestRateOptionComponent._InterestRateOption.IDSystem" AND 
	     "_InterestRateOptionComponent.ComponentNumber" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._InterestRateOptionComponent.ComponentNumber" AND 
	     "_InflationOptionComponent._InflationOption.FinancialContractID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.FinancialContractID" AND 
	     "ASSOC_Receivable.ReceivableID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList.ASSOC_Receivable.ReceivableID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList.ASSOC_FinancialContract.FinancialContractID" AND 
	     "_InflationOptionComponent.ComponentNumber" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._InflationOptionComponent.ComponentNumber" AND 
	     "_InflationOptionComponent._InflationOption.IDSystem" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._InflationOptionComponent._InflationOption.IDSystem" AND 
	     "_CreditCardLoan.ID" = "_PaymentIntervalListEntry_ASSOC_PaymentIntervalList"."ASSOC_PaymentIntervalList._CreditCardLoan.ID"
	     ,
	join "sap.fsdm.SQLViews::PaymentListEntry_View" as "_PaymentListEntry_ASSOC_PaymentList"
	  on "_CreditCardLoan._CreditCardAgreement.IDSystem" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.IDSystem" AND 
	     "ASSOC_Receivable.ReceivableID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList.ASSOC_Receivable.ReceivableID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList.ASSOC_FinancialContract.FinancialContractID" AND 
	     "_InflationOptionComponent._InflationOption.FinancialContractID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.FinancialContractID" AND 
	     "_InterestRateOptionComponent.ComponentNumber" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._InterestRateOptionComponent.ComponentNumber" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList.ASSOC_FinancialContract.IDSystem" AND 
	     "SequenceNumber" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList.SequenceNumber" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._DebtInstrument.FinancialInstrumentID" AND 
	     "_CreditCardLoan.ID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._CreditCardLoan.ID" AND 
	     "_InflationOptionComponent._InflationOption.IDSystem" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._InflationOptionComponent._InflationOption.IDSystem" AND 
	     "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.FinancialContractID" AND 
	     "_InterestRateOptionComponent._InterestRateOption.IDSystem" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._InterestRateOptionComponent._InterestRateOption.IDSystem" AND 
	     "_InflationOptionComponent.ComponentNumber" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._InflationOptionComponent.ComponentNumber" AND 
	     "_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "_PaymentListEntry_ASSOC_PaymentList"."ASSOC_PaymentList._CreditCardLoan._CreditCardAgreement.FinancialContractID"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "ASSOC_FinancialContract"
	  on "ASSOC_FinancialContract"."IDSystem" = "ASSOC_FinancialContract.IDSystem" AND 
	     "ASSOC_FinancialContract"."FinancialContractID" = "ASSOC_FinancialContract.FinancialContractID"
	     ,
	join "sap.fsdm.SQLViews::Receivable_View" as "ASSOC_Receivable"
	  on "ASSOC_Receivable"."ReceivableID" = "ASSOC_Receivable.ReceivableID"
	     ,
	join "sap.fsdm.SQLViews::CreditCardLoan_View" as "_CreditCardLoan"
	  on "_CreditCardLoan"."_CreditCardAgreement.FinancialContractID" = "_CreditCardLoan._CreditCardAgreement.FinancialContractID" AND 
	     "_CreditCardLoan"."ID" = "_CreditCardLoan.ID" AND 
	     "_CreditCardLoan"."_CreditCardAgreement.IDSystem" = "_CreditCardLoan._CreditCardAgreement.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_DebtInstrument"
	  on "_DebtInstrument"."FinancialInstrumentID" = "_DebtInstrument.FinancialInstrumentID"
	     ,
	join "sap.fsdm.SQLViews::InflationOptionComponent_View" as "_InflationOptionComponent"
	  on "_InflationOptionComponent"."ComponentNumber" = "_InflationOptionComponent.ComponentNumber" AND 
	     "_InflationOptionComponent"."_InflationOption.FinancialContractID" = "_InflationOptionComponent._InflationOption.FinancialContractID" AND 
	     "_InflationOptionComponent"."_InflationOption.IDSystem" = "_InflationOptionComponent._InflationOption.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::InterestRateOptionComponent_View" as "_InterestRateOptionComponent"
	  on "_InterestRateOptionComponent"."_InterestRateOption.IDSystem" = "_InterestRateOptionComponent._InterestRateOption.IDSystem" AND 
	     "_InterestRateOptionComponent"."_InterestRateOption.FinancialContractID" = "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" AND 
	     "_InterestRateOptionComponent"."ComponentNumber" = "_InterestRateOptionComponent.ComponentNumber"
	     ,
	join "sap.fsdm.SQLViews::WaterfallAssignment_View" as "_WaterfallAssignment__PaymentSchedule"
	  on "_CreditCardLoan._CreditCardAgreement.FinancialContractID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._CreditCardLoan._CreditCardAgreement.FinancialContractID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule.ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule.ASSOC_FinancialContract.IDSystem" AND 
	     "_InflationOptionComponent.ComponentNumber" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._InflationOptionComponent.ComponentNumber" AND 
	     "_InterestRateOptionComponent._InterestRateOption.IDSystem" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._InterestRateOptionComponent._InterestRateOption.IDSystem" AND 
	     "_InterestRateOptionComponent.ComponentNumber" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._InterestRateOptionComponent.ComponentNumber" AND 
	     "_CreditCardLoan.ID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._CreditCardLoan.ID" AND 
	     "ASSOC_Receivable.ReceivableID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule.ASSOC_Receivable.ReceivableID" AND 
	     "_InflationOptionComponent._InflationOption.FinancialContractID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._InflationOptionComponent._InflationOption.FinancialContractID" AND 
	     "_CreditCardLoan._CreditCardAgreement.IDSystem" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._CreditCardLoan._CreditCardAgreement.IDSystem" AND 
	     "_DebtInstrument.FinancialInstrumentID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._DebtInstrument.FinancialInstrumentID" AND 
	     "SequenceNumber" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule.SequenceNumber" AND 
	     "_InterestRateOptionComponent._InterestRateOption.FinancialContractID" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._InterestRateOptionComponent._InterestRateOption.FinancialContractID" AND 
	     "_InflationOptionComponent._InflationOption.IDSystem" = "_WaterfallAssignment__PaymentSchedule"."_PaymentSchedule._InflationOptionComponent._InflationOption.IDSystem"
	     
);