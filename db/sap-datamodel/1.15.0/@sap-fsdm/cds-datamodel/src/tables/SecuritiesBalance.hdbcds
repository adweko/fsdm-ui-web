namespace "sap"."fsdm";

using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"AccountingSystem";
using "sap"."fsdm"::"FinancialInstrument";

entity "SecuritiesBalance" {
    key "LotID"                                   : String(128)                                                  default '';
    key "_Account"                                : association to FinancialContract {
                                                                                       FinancialContractID,
                                                                                       IDSystem
                                                                                     }                           not null;
    key "_AccountingSystemOfSecuritiesLotBalance" : association to AccountingSystem { AccountingSystemID }       not null;
    key "_FinancialInstrument"                    : association to FinancialInstrument { FinancialInstrumentID } not null;
    key "BusinessValidFrom"                       : LocalDate;
    key "BusinessValidTo"                         : LocalDate;
        "SystemValidFrom"                         : UTCTimestamp                                                 not null;
        "SystemValidTo"                           : UTCTimestamp                                                 not null;
        "CapitalizedFees"                         : Decimal(34, 6);
        "CapitalizedFeesCurrency"                 : String(3);
        "CapitalizedInterest"                     : Decimal(34, 6);
        "CapitalizedInterestCurrency"             : String(3);
        "FeesPastDue"                             : Decimal(34, 6);
        "FeesPastDueCurrency"                     : String(3);
        "InterestPastDue"                         : Decimal(34, 6);
        "InterestPastDueCurrency"                 : String(3);
        "LendingValue"                            : Decimal(34, 6);
        "LendingValueCurrency"                    : String(3);
        "MarketValue"                             : Decimal(34, 6);
        "MarketValueCurrency"                     : String(3);
        "NominalAmount"                           : Decimal(34, 6);
        "NominalAmountCurrency"                   : String(3);
        "OutstandingPrincipal"                    : Decimal(34, 6);
        "OutstandingPrincipalCurrency"            : String(3);
        "PaidFeesSinceInception"                  : Decimal(34, 6);
        "PaidFeesSinceInceptionCurrency"          : String(3);
        "PaidInterestSinceInception"              : Decimal(34, 6);
        "PaidInterestSinceInceptionCurrency"      : String(3);
        "PrincipalPastDue"                        : Decimal(34, 6);
        "PrincipalPastDueCurrency"                : String(3);
        "PurchasePrice"                           : Decimal(34, 6);
        "PurchasePriceCurrency"                   : String(3);
        "Quantity"                                : Decimal(34, 6);
        "RepaidPrincipalSinceInception"           : Decimal(34, 6);
        "RepaidPrincipalSinceInceptionCurrency"   : String(3);
        "SecuritiesAccountBalanceCategory"        : String(100);
        "SecuritiesBalanceCategory"               : String(40);
        "TotalPastDue"                            : Decimal(34, 6);
        "TotalPastDueCurrency"                    : String(3);
        "Unit"                                    : String(10);
        "UnpaidDueFees"                           : Decimal(34, 6);
        "UnpaidDueFeesCurrency"                   : String(3);
        "UnpaidDueInterest"                       : Decimal(34, 6);
        "UnpaidDueInterestCurrency"               : String(3);
        "UnpaidDuePrincipal"                      : Decimal(34, 6);
        "UnpaidDuePrincipalCurrency"              : String(3);
        "SourceSystemID"                          : String(128);
        "ChangeTimestampInSourceSystem"           : UTCTimestamp;
        "ChangingUserInSourceSystem"              : String(128);
        "ChangingProcessType"                     : String(40);
        "ChangingProcessID"                       : String(128);
}
technical configuration {
    column store;
};

entity "SecuritiesBalance_Historical" {
    "LotID"                                   : String(128)                                                  default '' not null;
    "_Account"                                : association to FinancialContract {
                                                                                   FinancialContractID,
                                                                                   IDSystem
                                                                                 }                           not null;
    "_AccountingSystemOfSecuritiesLotBalance" : association to AccountingSystem { AccountingSystemID }       not null;
    "_FinancialInstrument"                    : association to FinancialInstrument { FinancialInstrumentID } not null;
    "BusinessValidFrom"                       : LocalDate                                                    not null;
    "BusinessValidTo"                         : LocalDate                                                    not null;
    "SystemValidFrom"                         : UTCTimestamp                                                 not null;
    "SystemValidTo"                           : UTCTimestamp                                                 not null;
    "CapitalizedFees"                         : Decimal(34, 6);
    "CapitalizedFeesCurrency"                 : String(3);
    "CapitalizedInterest"                     : Decimal(34, 6);
    "CapitalizedInterestCurrency"             : String(3);
    "FeesPastDue"                             : Decimal(34, 6);
    "FeesPastDueCurrency"                     : String(3);
    "InterestPastDue"                         : Decimal(34, 6);
    "InterestPastDueCurrency"                 : String(3);
    "LendingValue"                            : Decimal(34, 6);
    "LendingValueCurrency"                    : String(3);
    "MarketValue"                             : Decimal(34, 6);
    "MarketValueCurrency"                     : String(3);
    "NominalAmount"                           : Decimal(34, 6);
    "NominalAmountCurrency"                   : String(3);
    "OutstandingPrincipal"                    : Decimal(34, 6);
    "OutstandingPrincipalCurrency"            : String(3);
    "PaidFeesSinceInception"                  : Decimal(34, 6);
    "PaidFeesSinceInceptionCurrency"          : String(3);
    "PaidInterestSinceInception"              : Decimal(34, 6);
    "PaidInterestSinceInceptionCurrency"      : String(3);
    "PrincipalPastDue"                        : Decimal(34, 6);
    "PrincipalPastDueCurrency"                : String(3);
    "PurchasePrice"                           : Decimal(34, 6);
    "PurchasePriceCurrency"                   : String(3);
    "Quantity"                                : Decimal(34, 6);
    "RepaidPrincipalSinceInception"           : Decimal(34, 6);
    "RepaidPrincipalSinceInceptionCurrency"   : String(3);
    "SecuritiesAccountBalanceCategory"        : String(100);
    "SecuritiesBalanceCategory"               : String(40);
    "TotalPastDue"                            : Decimal(34, 6);
    "TotalPastDueCurrency"                    : String(3);
    "Unit"                                    : String(10);
    "UnpaidDueFees"                           : Decimal(34, 6);
    "UnpaidDueFeesCurrency"                   : String(3);
    "UnpaidDueInterest"                       : Decimal(34, 6);
    "UnpaidDueInterestCurrency"               : String(3);
    "UnpaidDuePrincipal"                      : Decimal(34, 6);
    "UnpaidDuePrincipalCurrency"              : String(3);
    "SourceSystemID"                          : String(128);
    "ChangeTimestampInSourceSystem"           : UTCTimestamp;
    "ChangingUserInSourceSystem"              : String(128);
    "ChangingProcessType"                     : String(40);
    "ChangingProcessID"                       : String(128);
}
technical configuration {
    column store;
};