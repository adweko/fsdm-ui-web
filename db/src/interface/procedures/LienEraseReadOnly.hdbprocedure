PROCEDURE "sap.fsdm.procedures::LienEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::LienTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::LienTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::LienTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "_LienAccount.FinancialContractID" is null and
            "_LienAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "_LienAccount.FinancialContractID" ,
                "_LienAccount.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_LienAccount.FinancialContractID" ,
                "OLD"."_LienAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Lien" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."_LienAccount.FinancialContractID" = "OLD"."_LienAccount.FinancialContractID" and
                "IN"."_LienAccount.IDSystem" = "OLD"."_LienAccount.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "_LienAccount.FinancialContractID" ,
            "_LienAccount.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."_LienAccount.FinancialContractID" ,
                "OLD"."_LienAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Lien_Historical" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."_LienAccount.FinancialContractID" = "OLD"."_LienAccount.FinancialContractID" and
                "IN"."_LienAccount.IDSystem" = "OLD"."_LienAccount.IDSystem" 
        );

END
