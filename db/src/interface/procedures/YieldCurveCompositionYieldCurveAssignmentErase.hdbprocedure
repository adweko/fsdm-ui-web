PROCEDURE "sap.fsdm.procedures::YieldCurveCompositionYieldCurveAssignmentErase" (IN ROW "sap.fsdm.tabletypes::YieldCurveCompositionYieldCurveAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_YieldCurve.ProviderOfYieldCurve" is null and
            "_YieldCurve.YieldCurveID" is null and
            "_YieldCurveComposition.YieldCurveCompositionID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::YieldCurveCompositionYieldCurveAssignment"
        WHERE
        (            "_YieldCurve.ProviderOfYieldCurve" ,
            "_YieldCurve.YieldCurveID" ,
            "_YieldCurveComposition.YieldCurveCompositionID" 
        ) in
        (
            select                 "OLD"."_YieldCurve.ProviderOfYieldCurve" ,
                "OLD"."_YieldCurve.YieldCurveID" ,
                "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
            from :ROW "IN"
            inner join "sap.fsdm::YieldCurveCompositionYieldCurveAssignment" "OLD"
            on
            "IN"."_YieldCurve.ProviderOfYieldCurve" = "OLD"."_YieldCurve.ProviderOfYieldCurve" and
            "IN"."_YieldCurve.YieldCurveID" = "OLD"."_YieldCurve.YieldCurveID" and
            "IN"."_YieldCurveComposition.YieldCurveCompositionID" = "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
        );

        --delete data from history table
        delete from "sap.fsdm::YieldCurveCompositionYieldCurveAssignment_Historical"
        WHERE
        (
            "_YieldCurve.ProviderOfYieldCurve" ,
            "_YieldCurve.YieldCurveID" ,
            "_YieldCurveComposition.YieldCurveCompositionID" 
        ) in
        (
            select
                "OLD"."_YieldCurve.ProviderOfYieldCurve" ,
                "OLD"."_YieldCurve.YieldCurveID" ,
                "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
            from :ROW "IN"
            inner join "sap.fsdm::YieldCurveCompositionYieldCurveAssignment_Historical" "OLD"
            on
                "IN"."_YieldCurve.ProviderOfYieldCurve" = "OLD"."_YieldCurve.ProviderOfYieldCurve" and
                "IN"."_YieldCurve.YieldCurveID" = "OLD"."_YieldCurve.YieldCurveID" and
                "IN"."_YieldCurveComposition.YieldCurveCompositionID" = "OLD"."_YieldCurveComposition.YieldCurveCompositionID" 
        );

END
