PROCEDURE "sap.fsdm.procedures::ExemptionOrderSnapshotErase" (IN ROW "sap.fsdm.tabletypes::ExemptionOrderSnapshotTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_ExemptionOrder.Category" is null and
            "_ExemptionOrder._BusinessPartner.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ExemptionOrderSnapshot"
        WHERE
        (            "_ExemptionOrder.Category" ,
            "_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
        ) in
        (
            select                 "OLD"."_ExemptionOrder.Category" ,
                "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::ExemptionOrderSnapshot" "OLD"
            on
            "IN"."_ExemptionOrder.Category" = "OLD"."_ExemptionOrder.Category" and
            "IN"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" = "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
        );

        --delete data from history table
        delete from "sap.fsdm::ExemptionOrderSnapshot_Historical"
        WHERE
        (
            "_ExemptionOrder.Category" ,
            "_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
        ) in
        (
            select
                "OLD"."_ExemptionOrder.Category" ,
                "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
            from :ROW "IN"
            inner join "sap.fsdm::ExemptionOrderSnapshot_Historical" "OLD"
            on
                "IN"."_ExemptionOrder.Category" = "OLD"."_ExemptionOrder.Category" and
                "IN"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" = "OLD"."_ExemptionOrder._BusinessPartner.BusinessPartnerID" 
        );

END
