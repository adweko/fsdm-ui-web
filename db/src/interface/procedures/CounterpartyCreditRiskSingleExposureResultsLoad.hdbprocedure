PROCEDURE "sap.fsdm.procedures::CounterpartyCreditRiskSingleExposureResultsLoad" (IN ROW "sap.fsdm.tabletypes::CounterpartyCreditRiskSingleExposureResultsTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AssetSubclass=' || TO_VARCHAR("AssetSubclass") || ' ' ||
                'MarginedNettingSet=' || TO_VARCHAR("MarginedNettingSet") || ' ' ||
                '_ExchangeTradedNettingSet.FinancialInstrumentID=' || TO_VARCHAR("_ExchangeTradedNettingSet.FinancialInstrumentID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_FinancialContractNettingSet.FinancialContractID=' || TO_VARCHAR("_FinancialContractNettingSet.FinancialContractID") || ' ' ||
                '_FinancialContractNettingSet.IDSystem=' || TO_VARCHAR("_FinancialContractNettingSet.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_SecuritiesAccount.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccount.FinancialContractID") || ' ' ||
                '_SecuritiesAccount.IDSystem=' || TO_VARCHAR("_SecuritiesAccount.IDSystem") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AssetSubclass",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AssetSubclass",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AssetSubclass",
                        "MarginedNettingSet",
                        "_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_FinancialContractNettingSet.FinancialContractID",
                        "_FinancialContractNettingSet.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_SecuritiesAccount.FinancialContractID",
                        "_SecuritiesAccount.IDSystem",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AssetSubclass",
                                    "IN"."MarginedNettingSet",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AssetSubclass",
                                    "IN"."MarginedNettingSet",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccount.FinancialContractID",
                                    "IN"."_SecuritiesAccount.IDSystem",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::CounterpartyCreditRiskSingleExposureResults" (
        "AssetSubclass",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdjustedNotionalAmount",
        "AdjustedNotionalAmountCurrency",
        "LongSettlementTransaction",
        "LongShort",
        "MarketValue",
        "MarketValueCurrency",
        "NotionalAmount",
        "NotionalAmountCurrency",
        "ReferenceEntityType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AssetSubclass" as "AssetSubclass" ,
            "OLD_MarginedNettingSet" as "MarginedNettingSet" ,
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID" ,
            "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID" ,
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem" ,
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_AdjustedNotionalAmount" as "AdjustedNotionalAmount" ,
            "OLD_AdjustedNotionalAmountCurrency" as "AdjustedNotionalAmountCurrency" ,
            "OLD_LongSettlementTransaction" as "LongSettlementTransaction" ,
            "OLD_LongShort" as "LongShort" ,
            "OLD_MarketValue" as "MarketValue" ,
            "OLD_MarketValueCurrency" as "MarketValueCurrency" ,
            "OLD_NotionalAmount" as "NotionalAmount" ,
            "OLD_NotionalAmountCurrency" as "NotionalAmountCurrency" ,
            "OLD_ReferenceEntityType" as "ReferenceEntityType" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."AssetSubclass",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."AssetSubclass" as "OLD_AssetSubclass",
                                "OLD"."MarginedNettingSet" as "OLD_MarginedNettingSet",
                                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" as "OLD__ExchangeTradedNettingSet.FinancialInstrumentID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."_FinancialContractNettingSet.FinancialContractID" as "OLD__FinancialContractNettingSet.FinancialContractID",
                                "OLD"."_FinancialContractNettingSet.IDSystem" as "OLD__FinancialContractNettingSet.IDSystem",
                                "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                                "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AdjustedNotionalAmount" as "OLD_AdjustedNotionalAmount",
                                "OLD"."AdjustedNotionalAmountCurrency" as "OLD_AdjustedNotionalAmountCurrency",
                                "OLD"."LongSettlementTransaction" as "OLD_LongSettlementTransaction",
                                "OLD"."LongShort" as "OLD_LongShort",
                                "OLD"."MarketValue" as "OLD_MarketValue",
                                "OLD"."MarketValueCurrency" as "OLD_MarketValueCurrency",
                                "OLD"."NotionalAmount" as "OLD_NotionalAmount",
                                "OLD"."NotionalAmountCurrency" as "OLD_NotionalAmountCurrency",
                                "OLD"."ReferenceEntityType" as "OLD_ReferenceEntityType",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskSingleExposureResults" as "OLD"
            on
                ifnull( "IN"."AssetSubclass", '') = "OLD"."AssetSubclass" and
                ifnull( "IN"."MarginedNettingSet", false) = "OLD"."MarginedNettingSet" and
                ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '') = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '') = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '') = "OLD"."_FinancialContractNettingSet.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '') = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '') = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '') = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CounterpartyCreditRiskSingleExposureResults" (
        "AssetSubclass",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdjustedNotionalAmount",
        "AdjustedNotionalAmountCurrency",
        "LongSettlementTransaction",
        "LongShort",
        "MarketValue",
        "MarketValueCurrency",
        "NotionalAmount",
        "NotionalAmountCurrency",
        "ReferenceEntityType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_AssetSubclass" as "AssetSubclass",
            "OLD_MarginedNettingSet" as "MarginedNettingSet",
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID",
            "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__SecuritiesAccount.FinancialContractID" as "_SecuritiesAccount.FinancialContractID",
            "OLD__SecuritiesAccount.IDSystem" as "_SecuritiesAccount.IDSystem",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AdjustedNotionalAmount" as "AdjustedNotionalAmount",
            "OLD_AdjustedNotionalAmountCurrency" as "AdjustedNotionalAmountCurrency",
            "OLD_LongSettlementTransaction" as "LongSettlementTransaction",
            "OLD_LongShort" as "LongShort",
            "OLD_MarketValue" as "MarketValue",
            "OLD_MarketValueCurrency" as "MarketValueCurrency",
            "OLD_NotionalAmount" as "NotionalAmount",
            "OLD_NotionalAmountCurrency" as "NotionalAmountCurrency",
            "OLD_ReferenceEntityType" as "ReferenceEntityType",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."AssetSubclass",
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccount.FinancialContractID",
                        "IN"."_SecuritiesAccount.IDSystem",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."AssetSubclass" as "OLD_AssetSubclass",
                        "OLD"."MarginedNettingSet" as "OLD_MarginedNettingSet",
                        "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" as "OLD__ExchangeTradedNettingSet.FinancialInstrumentID",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."_FinancialContractNettingSet.FinancialContractID" as "OLD__FinancialContractNettingSet.FinancialContractID",
                        "OLD"."_FinancialContractNettingSet.IDSystem" as "OLD__FinancialContractNettingSet.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID" as "OLD__FinancialInstrument.FinancialInstrumentID",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_SecuritiesAccount.FinancialContractID" as "OLD__SecuritiesAccount.FinancialContractID",
                        "OLD"."_SecuritiesAccount.IDSystem" as "OLD__SecuritiesAccount.IDSystem",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."AdjustedNotionalAmount" as "OLD_AdjustedNotionalAmount",
                        "OLD"."AdjustedNotionalAmountCurrency" as "OLD_AdjustedNotionalAmountCurrency",
                        "OLD"."LongSettlementTransaction" as "OLD_LongSettlementTransaction",
                        "OLD"."LongShort" as "OLD_LongShort",
                        "OLD"."MarketValue" as "OLD_MarketValue",
                        "OLD"."MarketValueCurrency" as "OLD_MarketValueCurrency",
                        "OLD"."NotionalAmount" as "OLD_NotionalAmount",
                        "OLD"."NotionalAmountCurrency" as "OLD_NotionalAmountCurrency",
                        "OLD"."ReferenceEntityType" as "OLD_ReferenceEntityType",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskSingleExposureResults" as "OLD"
            on
                ifnull( "IN"."AssetSubclass", '' ) = "OLD"."AssetSubclass" and
                ifnull( "IN"."MarginedNettingSet", false ) = "OLD"."MarginedNettingSet" and
                ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
                ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '' ) = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '' ) = "OLD"."_FinancialContractNettingSet.IDSystem" and
                ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccount.IDSystem", '' ) = "OLD"."_SecuritiesAccount.IDSystem" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::CounterpartyCreditRiskSingleExposureResults"
    where (
        "AssetSubclass",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."AssetSubclass",
            "OLD"."MarginedNettingSet",
            "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_FinancialContractNettingSet.FinancialContractID",
            "OLD"."_FinancialContractNettingSet.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_SecuritiesAccount.FinancialContractID",
            "OLD"."_SecuritiesAccount.IDSystem",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CounterpartyCreditRiskSingleExposureResults" as "OLD"
        on
           ifnull( "IN"."AssetSubclass", '' ) = "OLD"."AssetSubclass" and
           ifnull( "IN"."MarginedNettingSet", false ) = "OLD"."MarginedNettingSet" and
           ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
           ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" and
           ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '' ) = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
           ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '' ) = "OLD"."_FinancialContractNettingSet.IDSystem" and
           ifnull( "IN"."_FinancialInstrument.FinancialInstrumentID", '' ) = "OLD"."_FinancialInstrument.FinancialInstrumentID" and
           ifnull( "IN"."_ResultGroup.ResultDataProvider", '' ) = "OLD"."_ResultGroup.ResultDataProvider" and
           ifnull( "IN"."_ResultGroup.ResultGroupID", '' ) = "OLD"."_ResultGroup.ResultGroupID" and
           ifnull( "IN"."_SecuritiesAccount.FinancialContractID", '' ) = "OLD"."_SecuritiesAccount.FinancialContractID" and
           ifnull( "IN"."_SecuritiesAccount.IDSystem", '' ) = "OLD"."_SecuritiesAccount.IDSystem" and
           ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
           ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::CounterpartyCreditRiskSingleExposureResults" (
        "AssetSubclass",
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccount.FinancialContractID",
        "_SecuritiesAccount.IDSystem",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AdjustedNotionalAmount",
        "AdjustedNotionalAmountCurrency",
        "LongSettlementTransaction",
        "LongShort",
        "MarketValue",
        "MarketValueCurrency",
        "NotionalAmount",
        "NotionalAmountCurrency",
        "ReferenceEntityType",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "AssetSubclass", '' ) as "AssetSubclass",
            ifnull( "MarginedNettingSet", false ) as "MarginedNettingSet",
            ifnull( "_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) as "_ExchangeTradedNettingSet.FinancialInstrumentID",
            ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
            ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
            ifnull( "_FinancialContractNettingSet.FinancialContractID", '' ) as "_FinancialContractNettingSet.FinancialContractID",
            ifnull( "_FinancialContractNettingSet.IDSystem", '' ) as "_FinancialContractNettingSet.IDSystem",
            ifnull( "_FinancialInstrument.FinancialInstrumentID", '' ) as "_FinancialInstrument.FinancialInstrumentID",
            ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
            ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
            ifnull( "_SecuritiesAccount.FinancialContractID", '' ) as "_SecuritiesAccount.FinancialContractID",
            ifnull( "_SecuritiesAccount.IDSystem", '' ) as "_SecuritiesAccount.IDSystem",
            ifnull( "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            ifnull( "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "AdjustedNotionalAmount"  ,
            "AdjustedNotionalAmountCurrency"  ,
            "LongSettlementTransaction"  ,
            "LongShort"  ,
            "MarketValue"  ,
            "MarketValueCurrency"  ,
            "NotionalAmount"  ,
            "NotionalAmountCurrency"  ,
            "ReferenceEntityType"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END