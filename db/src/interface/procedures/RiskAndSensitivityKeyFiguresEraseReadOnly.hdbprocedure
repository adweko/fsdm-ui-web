PROCEDURE "sap.fsdm.procedures::RiskAndSensitivityKeyFiguresEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::RiskAndSensitivityKeyFiguresTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::RiskAndSensitivityKeyFiguresTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::RiskAndSensitivityKeyFiguresTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AssetOrLiability" is null and
            "Category" is null and
            "KeyDate" is null and
            "Provider" is null and
            "Scenario" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "AssetOrLiability" ,
                "Category" ,
                "KeyDate" ,
                "Provider" ,
                "Scenario" ,
                "_Collection.CollectionID" ,
                "_Collection.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."AssetOrLiability" ,
                "OLD"."Category" ,
                "OLD"."KeyDate" ,
                "OLD"."Provider" ,
                "OLD"."Scenario" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RiskAndSensitivityKeyFigures" "OLD"
            on
                "IN"."AssetOrLiability" = "OLD"."AssetOrLiability" and
                "IN"."Category" = "OLD"."Category" and
                "IN"."KeyDate" = "OLD"."KeyDate" and
                "IN"."Provider" = "OLD"."Provider" and
                "IN"."Scenario" = "OLD"."Scenario" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "AssetOrLiability" ,
            "Category" ,
            "KeyDate" ,
            "Provider" ,
            "Scenario" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."AssetOrLiability" ,
                "OLD"."Category" ,
                "OLD"."KeyDate" ,
                "OLD"."Provider" ,
                "OLD"."Scenario" ,
                "OLD"."_Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::RiskAndSensitivityKeyFigures_Historical" "OLD"
            on
                "IN"."AssetOrLiability" = "OLD"."AssetOrLiability" and
                "IN"."Category" = "OLD"."Category" and
                "IN"."KeyDate" = "OLD"."KeyDate" and
                "IN"."Provider" = "OLD"."Provider" and
                "IN"."Scenario" = "OLD"."Scenario" and
                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" 
        );

END
