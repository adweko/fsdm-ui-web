PROCEDURE "sap.fsdm.procedures::DocumentAssignmentDelete" (IN ROW "sap.fsdm.tabletypes::DocumentAssignmentTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_BusinessPartner.BusinessPartnerID") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_CommunicationEvent.CommunicationEventID=' || TO_VARCHAR("_CommunicationEvent.CommunicationEventID") || ' ' ||
                '_CommunicationEvent.IDSystem=' || TO_VARCHAR("_CommunicationEvent.IDSystem") || ' ' ||
                '_Document.DocumentID=' || TO_VARCHAR("_Document.DocumentID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                '_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID=' || TO_VARCHAR("_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID") || ' ' ||
                '_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem=' || TO_VARCHAR("_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem") || ' ' ||
                '_TaxExemptStatusDocument.Category=' || TO_VARCHAR("_TaxExemptStatusDocument.Category") || ' ' ||
                '_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID") || ' ' ||
                '_Trade.IDSystem=' || TO_VARCHAR("_Trade.IDSystem") || ' ' ||
                '_Trade.TradeID=' || TO_VARCHAR("_Trade.TradeID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_CommunicationEvent.CommunicationEventID",
                        "IN"."_CommunicationEvent.IDSystem",
                        "IN"."_Document.DocumentID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                        "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                        "IN"."_TaxExemptStatusDocument.Category",
                        "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_BusinessPartner.BusinessPartnerID",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_CommunicationEvent.CommunicationEventID",
                        "IN"."_CommunicationEvent.IDSystem",
                        "IN"."_Document.DocumentID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                        "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                        "IN"."_TaxExemptStatusDocument.Category",
                        "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                        "IN"."_Trade.IDSystem",
                        "IN"."_Trade.TradeID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_BusinessPartner.BusinessPartnerID",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_CommunicationEvent.CommunicationEventID",
                        "_CommunicationEvent.IDSystem",
                        "_Document.DocumentID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem",
                        "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                        "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                        "_TaxExemptStatusDocument.Category",
                        "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                        "_Trade.IDSystem",
                        "_Trade.TradeID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_CommunicationEvent.CommunicationEventID",
                                    "IN"."_CommunicationEvent.IDSystem",
                                    "IN"."_Document.DocumentID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                                    "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                                    "IN"."_TaxExemptStatusDocument.Category",
                                    "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_BusinessPartner.BusinessPartnerID",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_CommunicationEvent.CommunicationEventID",
                                    "IN"."_CommunicationEvent.IDSystem",
                                    "IN"."_Document.DocumentID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem",
                                    "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                                    "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                                    "IN"."_TaxExemptStatusDocument.Category",
                                    "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                                    "IN"."_Trade.IDSystem",
                                    "IN"."_Trade.TradeID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_BusinessPartner.BusinessPartnerID" is null and
            "_Collection.CollectionID" is null and
            "_Collection.IDSystem" is null and
            "_Collection._Client.BusinessPartnerID" is null and
            "_CommunicationEvent.CommunicationEventID" is null and
            "_CommunicationEvent.IDSystem" is null and
            "_Document.DocumentID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" is null and
            "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" is null and
            "_TaxExemptStatusDocument.Category" is null and
            "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" is null and
            "_Trade.IDSystem" is null and
            "_Trade.TradeID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::DocumentAssignment" (
        "_BusinessPartner.BusinessPartnerID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_CommunicationEvent.CommunicationEventID",
        "_CommunicationEvent.IDSystem",
        "_Document.DocumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
        "_TaxExemptStatusDocument.Category",
        "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DocumentAssignmentCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID" ,
            "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
            "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
            "OLD__CommunicationEvent.CommunicationEventID" as "_CommunicationEvent.CommunicationEventID" ,
            "OLD__CommunicationEvent.IDSystem" as "_CommunicationEvent.IDSystem" ,
            "OLD__Document.DocumentID" as "_Document.DocumentID" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" as "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" as "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
            "OLD__TaxExemptStatusDocument.Category" as "_TaxExemptStatusDocument.Category" ,
            "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" as "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
            "OLD__Trade.IDSystem" as "_Trade.IDSystem" ,
            "OLD__Trade.TradeID" as "_Trade.TradeID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_DocumentAssignmentCategory" as "DocumentAssignmentCategory" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_CommunicationEvent.CommunicationEventID",
                        "OLD"."_CommunicationEvent.IDSystem",
                        "OLD"."_Document.DocumentID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                        "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                        "OLD"."_TaxExemptStatusDocument.Category",
                        "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_CommunicationEvent.CommunicationEventID" AS "OLD__CommunicationEvent.CommunicationEventID" ,
                "OLD"."_CommunicationEvent.IDSystem" AS "OLD__CommunicationEvent.IDSystem" ,
                "OLD"."_Document.DocumentID" AS "OLD__Document.DocumentID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
                "OLD"."_TaxExemptStatusDocument.Category" AS "OLD__TaxExemptStatusDocument.Category" ,
                "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" AS "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DocumentAssignmentCategory" AS "OLD_DocumentAssignmentCategory" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::DocumentAssignment" as "OLD"
            on
                      "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                      "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                      "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                      "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                      "IN"."_CommunicationEvent.CommunicationEventID" = "OLD"."_CommunicationEvent.CommunicationEventID" and
                      "IN"."_CommunicationEvent.IDSystem" = "OLD"."_CommunicationEvent.IDSystem" and
                      "IN"."_Document.DocumentID" = "OLD"."_Document.DocumentID" and
                      "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                      "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                      "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" and
                      "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" and
                      "IN"."_TaxExemptStatusDocument.Category" = "OLD"."_TaxExemptStatusDocument.Category" and
                      "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" and
                      "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                      "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::DocumentAssignment" (
        "_BusinessPartner.BusinessPartnerID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_CommunicationEvent.CommunicationEventID",
        "_CommunicationEvent.IDSystem",
        "_Document.DocumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
        "_TaxExemptStatusDocument.Category",
        "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DocumentAssignmentCategory",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD__BusinessPartner.BusinessPartnerID" as "_BusinessPartner.BusinessPartnerID",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__CommunicationEvent.CommunicationEventID" as "_CommunicationEvent.CommunicationEventID",
            "OLD__CommunicationEvent.IDSystem" as "_CommunicationEvent.IDSystem",
            "OLD__Document.DocumentID" as "_Document.DocumentID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" as "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
            "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" as "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
            "OLD__TaxExemptStatusDocument.Category" as "_TaxExemptStatusDocument.Category",
            "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" as "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
            "OLD__Trade.IDSystem" as "_Trade.IDSystem",
            "OLD__Trade.TradeID" as "_Trade.TradeID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DocumentAssignmentCategory" as "DocumentAssignmentCategory",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_BusinessPartner.BusinessPartnerID",
                        "OLD"."_Collection.CollectionID",
                        "OLD"."_Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID",
                        "OLD"."_CommunicationEvent.CommunicationEventID",
                        "OLD"."_CommunicationEvent.IDSystem",
                        "OLD"."_Document.DocumentID",
                        "OLD"."_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem",
                        "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
                        "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
                        "OLD"."_TaxExemptStatusDocument.Category",
                        "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
                        "OLD"."_Trade.IDSystem",
                        "OLD"."_Trade.TradeID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_BusinessPartner.BusinessPartnerID" AS "OLD__BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Collection.CollectionID" AS "OLD__Collection.CollectionID" ,
                "OLD"."_Collection.IDSystem" AS "OLD__Collection.IDSystem" ,
                "OLD"."_Collection._Client.BusinessPartnerID" AS "OLD__Collection._Client.BusinessPartnerID" ,
                "OLD"."_CommunicationEvent.CommunicationEventID" AS "OLD__CommunicationEvent.CommunicationEventID" ,
                "OLD"."_CommunicationEvent.IDSystem" AS "OLD__CommunicationEvent.IDSystem" ,
                "OLD"."_Document.DocumentID" AS "OLD__Document.DocumentID" ,
                "OLD"."_FinancialContract.FinancialContractID" AS "OLD__FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" AS "OLD__FinancialContract.IDSystem" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" ,
                "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" AS "OLD__PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" ,
                "OLD"."_TaxExemptStatusDocument.Category" AS "OLD__TaxExemptStatusDocument.Category" ,
                "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" AS "OLD__TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_Trade.IDSystem" AS "OLD__Trade.IDSystem" ,
                "OLD"."_Trade.TradeID" AS "OLD__Trade.TradeID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."DocumentAssignmentCategory" AS "OLD_DocumentAssignmentCategory" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::DocumentAssignment" as "OLD"
            on
                                                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                                                "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                                "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                                "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                                                "IN"."_CommunicationEvent.CommunicationEventID" = "OLD"."_CommunicationEvent.CommunicationEventID" and
                                                "IN"."_CommunicationEvent.IDSystem" = "OLD"."_CommunicationEvent.IDSystem" and
                                                "IN"."_Document.DocumentID" = "OLD"."_Document.DocumentID" and
                                                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                                "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" and
                                                "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" and
                                                "IN"."_TaxExemptStatusDocument.Category" = "OLD"."_TaxExemptStatusDocument.Category" and
                                                "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" and
                                                "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                                "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::DocumentAssignment"
    where (
        "_BusinessPartner.BusinessPartnerID",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_CommunicationEvent.CommunicationEventID",
        "_CommunicationEvent.IDSystem",
        "_Document.DocumentID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
        "_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
        "_TaxExemptStatusDocument.Category",
        "_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
        "_Trade.IDSystem",
        "_Trade.TradeID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."_BusinessPartner.BusinessPartnerID",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_CommunicationEvent.CommunicationEventID",
            "OLD"."_CommunicationEvent.IDSystem",
            "OLD"."_Document.DocumentID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID",
            "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem",
            "OLD"."_TaxExemptStatusDocument.Category",
            "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID",
            "OLD"."_Trade.IDSystem",
            "OLD"."_Trade.TradeID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::DocumentAssignment" as "OLD"
        on
                                       "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                                       "IN"."_Collection.CollectionID" = "OLD"."_Collection.CollectionID" and
                                       "IN"."_Collection.IDSystem" = "OLD"."_Collection.IDSystem" and
                                       "IN"."_Collection._Client.BusinessPartnerID" = "OLD"."_Collection._Client.BusinessPartnerID" and
                                       "IN"."_CommunicationEvent.CommunicationEventID" = "OLD"."_CommunicationEvent.CommunicationEventID" and
                                       "IN"."_CommunicationEvent.IDSystem" = "OLD"."_CommunicationEvent.IDSystem" and
                                       "IN"."_Document.DocumentID" = "OLD"."_Document.DocumentID" and
                                       "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                                       "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                                       "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.FinancialContractID" and
                                       "IN"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" = "OLD"."_PurchasedReceivable._ReceivablePurchaseAgreement.IDSystem" and
                                       "IN"."_TaxExemptStatusDocument.Category" = "OLD"."_TaxExemptStatusDocument.Category" and
                                       "IN"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" = "OLD"."_TaxExemptStatusDocument._BusinessPartner.BusinessPartnerID" and
                                       "IN"."_Trade.IDSystem" = "OLD"."_Trade.IDSystem" and
                                       "IN"."_Trade.TradeID" = "OLD"."_Trade.TradeID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
