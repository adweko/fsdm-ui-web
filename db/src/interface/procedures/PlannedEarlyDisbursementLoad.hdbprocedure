PROCEDURE "sap.fsdm.procedures::PlannedEarlyDisbursementLoad" (IN ROW "sap.fsdm.tabletypes::PlannedEarlyDisbursementTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DisbursementDifferentiationCriterium=' || TO_VARCHAR("DisbursementDifferentiationCriterium") || ' ' ||
                'PlannedDisbursementDate=' || TO_VARCHAR("PlannedDisbursementDate") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DisbursementDifferentiationCriterium",
                        "IN"."PlannedDisbursementDate",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DisbursementDifferentiationCriterium",
                        "IN"."PlannedDisbursementDate",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DisbursementDifferentiationCriterium",
                        "PlannedDisbursementDate",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DisbursementDifferentiationCriterium",
                                    "IN"."PlannedDisbursementDate",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DisbursementDifferentiationCriterium",
                                    "IN"."PlannedDisbursementDate",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::PlannedEarlyDisbursement" (
        "DisbursementDifferentiationCriterium",
        "PlannedDisbursementDate",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DisbursementProbability",
        "PlannedDisbursementAmount",
        "PlannedDisbursementAmountCurrency",
        "Status",
        "StatusChangeDate",
        "StatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_DisbursementDifferentiationCriterium" as "DisbursementDifferentiationCriterium" ,
            "OLD_PlannedDisbursementDate" as "PlannedDisbursementDate" ,
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_DisbursementProbability" as "DisbursementProbability" ,
            "OLD_PlannedDisbursementAmount" as "PlannedDisbursementAmount" ,
            "OLD_PlannedDisbursementAmountCurrency" as "PlannedDisbursementAmountCurrency" ,
            "OLD_Status" as "Status" ,
            "OLD_StatusChangeDate" as "StatusChangeDate" ,
            "OLD_StatusReason" as "StatusReason" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."DisbursementDifferentiationCriterium",
                        "IN"."PlannedDisbursementDate",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."DisbursementDifferentiationCriterium" as "OLD_DisbursementDifferentiationCriterium",
                                "OLD"."PlannedDisbursementDate" as "OLD_PlannedDisbursementDate",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."DisbursementProbability" as "OLD_DisbursementProbability",
                                "OLD"."PlannedDisbursementAmount" as "OLD_PlannedDisbursementAmount",
                                "OLD"."PlannedDisbursementAmountCurrency" as "OLD_PlannedDisbursementAmountCurrency",
                                "OLD"."Status" as "OLD_Status",
                                "OLD"."StatusChangeDate" as "OLD_StatusChangeDate",
                                "OLD"."StatusReason" as "OLD_StatusReason",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PlannedEarlyDisbursement" as "OLD"
            on
                ifnull( "IN"."DisbursementDifferentiationCriterium", '') = "OLD"."DisbursementDifferentiationCriterium" and
                ifnull( "IN"."PlannedDisbursementDate", '0001-01-01') = "OLD"."PlannedDisbursementDate" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::PlannedEarlyDisbursement" (
        "DisbursementDifferentiationCriterium",
        "PlannedDisbursementDate",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DisbursementProbability",
        "PlannedDisbursementAmount",
        "PlannedDisbursementAmountCurrency",
        "Status",
        "StatusChangeDate",
        "StatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_DisbursementDifferentiationCriterium" as "DisbursementDifferentiationCriterium",
            "OLD_PlannedDisbursementDate" as "PlannedDisbursementDate",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DisbursementProbability" as "DisbursementProbability",
            "OLD_PlannedDisbursementAmount" as "PlannedDisbursementAmount",
            "OLD_PlannedDisbursementAmountCurrency" as "PlannedDisbursementAmountCurrency",
            "OLD_Status" as "Status",
            "OLD_StatusChangeDate" as "StatusChangeDate",
            "OLD_StatusReason" as "StatusReason",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."DisbursementDifferentiationCriterium",
                        "IN"."PlannedDisbursementDate",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."DisbursementDifferentiationCriterium" as "OLD_DisbursementDifferentiationCriterium",
                        "OLD"."PlannedDisbursementDate" as "OLD_PlannedDisbursementDate",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."DisbursementProbability" as "OLD_DisbursementProbability",
                        "OLD"."PlannedDisbursementAmount" as "OLD_PlannedDisbursementAmount",
                        "OLD"."PlannedDisbursementAmountCurrency" as "OLD_PlannedDisbursementAmountCurrency",
                        "OLD"."Status" as "OLD_Status",
                        "OLD"."StatusChangeDate" as "OLD_StatusChangeDate",
                        "OLD"."StatusReason" as "OLD_StatusReason",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::PlannedEarlyDisbursement" as "OLD"
            on
                ifnull( "IN"."DisbursementDifferentiationCriterium", '' ) = "OLD"."DisbursementDifferentiationCriterium" and
                ifnull( "IN"."PlannedDisbursementDate", '0001-01-01' ) = "OLD"."PlannedDisbursementDate" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::PlannedEarlyDisbursement"
    where (
        "DisbursementDifferentiationCriterium",
        "PlannedDisbursementDate",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."DisbursementDifferentiationCriterium",
            "OLD"."PlannedDisbursementDate",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::PlannedEarlyDisbursement" as "OLD"
        on
           ifnull( "IN"."DisbursementDifferentiationCriterium", '' ) = "OLD"."DisbursementDifferentiationCriterium" and
           ifnull( "IN"."PlannedDisbursementDate", '0001-01-01' ) = "OLD"."PlannedDisbursementDate" and
           ifnull( "IN"."_FinancialContract.FinancialContractID", '' ) = "OLD"."_FinancialContract.FinancialContractID" and
           ifnull( "IN"."_FinancialContract.IDSystem", '' ) = "OLD"."_FinancialContract.IDSystem" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::PlannedEarlyDisbursement" (
        "DisbursementDifferentiationCriterium",
        "PlannedDisbursementDate",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DisbursementProbability",
        "PlannedDisbursementAmount",
        "PlannedDisbursementAmountCurrency",
        "Status",
        "StatusChangeDate",
        "StatusReason",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "DisbursementDifferentiationCriterium", '' ) as "DisbursementDifferentiationCriterium",
            ifnull( "PlannedDisbursementDate", '0001-01-01' ) as "PlannedDisbursementDate",
            ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
            ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "DisbursementProbability"  ,
            "PlannedDisbursementAmount"  ,
            "PlannedDisbursementAmountCurrency"  ,
            "Status"  ,
            "StatusChangeDate"  ,
            "StatusReason"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END