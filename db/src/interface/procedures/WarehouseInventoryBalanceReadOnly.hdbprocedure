PROCEDURE "sap.fsdm.procedures::WarehouseInventoryBalanceReadOnly" (IN ROW "sap.fsdm.tabletypes::WarehouseInventoryBalanceTT", OUT CURR_DEL "sap.fsdm.tabletypes::WarehouseInventoryBalanceTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::WarehouseInventoryBalanceTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ProductID=' || TO_VARCHAR("ProductID") || ' ' ||
                '_WarehouseInventory.PhysicalAssetID=' || TO_VARCHAR("_WarehouseInventory.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ProductID",
                        "IN"."_WarehouseInventory.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ProductID",
                        "IN"."_WarehouseInventory.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ProductID",
                        "_WarehouseInventory.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ProductID",
                                    "IN"."_WarehouseInventory.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ProductID",
                                    "IN"."_WarehouseInventory.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "ProductID",
        "_WarehouseInventory.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::WarehouseInventoryBalance" WHERE
        (            "ProductID" ,
            "_WarehouseInventory.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."ProductID",
            "OLD"."_WarehouseInventory.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::WarehouseInventoryBalance" as "OLD"
            on
               ifnull( "IN"."ProductID",'' ) = "OLD"."ProductID" and
               ifnull( "IN"."_WarehouseInventory.PhysicalAssetID",'' ) = "OLD"."_WarehouseInventory.PhysicalAssetID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "ProductID",
        "_WarehouseInventory.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "Quantity",
        "QuantityUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "ProductID", '' ) as "ProductID",
                    ifnull( "_WarehouseInventory.PhysicalAssetID", '' ) as "_WarehouseInventory.PhysicalAssetID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "Quantity"  ,
                    "QuantityUnit"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_ProductID" as "ProductID" ,
                    "OLD__WarehouseInventory.PhysicalAssetID" as "_WarehouseInventory.PhysicalAssetID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_Quantity" as "Quantity" ,
                    "OLD_QuantityUnit" as "QuantityUnit" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."ProductID",
                        "IN"."_WarehouseInventory.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."ProductID" as "OLD_ProductID",
                                "OLD"."_WarehouseInventory.PhysicalAssetID" as "OLD__WarehouseInventory.PhysicalAssetID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."Quantity" as "OLD_Quantity",
                                "OLD"."QuantityUnit" as "OLD_QuantityUnit",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::WarehouseInventoryBalance" as "OLD"
            on
                ifnull( "IN"."ProductID", '') = "OLD"."ProductID" and
                ifnull( "IN"."_WarehouseInventory.PhysicalAssetID", '') = "OLD"."_WarehouseInventory.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_ProductID" as "ProductID",
            "OLD__WarehouseInventory.PhysicalAssetID" as "_WarehouseInventory.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_Quantity" as "Quantity",
            "OLD_QuantityUnit" as "QuantityUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."ProductID",
                        "IN"."_WarehouseInventory.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."ProductID" as "OLD_ProductID",
                        "OLD"."_WarehouseInventory.PhysicalAssetID" as "OLD__WarehouseInventory.PhysicalAssetID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."Quantity" as "OLD_Quantity",
                        "OLD"."QuantityUnit" as "OLD_QuantityUnit",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::WarehouseInventoryBalance" as "OLD"
            on
                ifnull("IN"."ProductID", '') = "OLD"."ProductID" and
                ifnull("IN"."_WarehouseInventory.PhysicalAssetID", '') = "OLD"."_WarehouseInventory.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
