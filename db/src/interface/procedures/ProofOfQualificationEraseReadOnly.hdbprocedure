PROCEDURE "sap.fsdm.procedures::ProofOfQualificationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::ProofOfQualificationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::ProofOfQualificationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::ProofOfQualificationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "IDSystem" is null and
            "IssuingOrganization" is null and
            "QualificationIdentifier" is null and
            "_IndividualPerson.BusinessPartnerID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "IDSystem" ,
                "IssuingOrganization" ,
                "QualificationIdentifier" ,
                "_IndividualPerson.BusinessPartnerID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."IDSystem" ,
                "OLD"."IssuingOrganization" ,
                "OLD"."QualificationIdentifier" ,
                "OLD"."_IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ProofOfQualification" "OLD"
            on
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."IssuingOrganization" = "OLD"."IssuingOrganization" and
                "IN"."QualificationIdentifier" = "OLD"."QualificationIdentifier" and
                "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "IDSystem" ,
            "IssuingOrganization" ,
            "QualificationIdentifier" ,
            "_IndividualPerson.BusinessPartnerID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."IDSystem" ,
                "OLD"."IssuingOrganization" ,
                "OLD"."QualificationIdentifier" ,
                "OLD"."_IndividualPerson.BusinessPartnerID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::ProofOfQualification_Historical" "OLD"
            on
                "IN"."IDSystem" = "OLD"."IDSystem" and
                "IN"."IssuingOrganization" = "OLD"."IssuingOrganization" and
                "IN"."QualificationIdentifier" = "OLD"."QualificationIdentifier" and
                "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" 
        );

END
