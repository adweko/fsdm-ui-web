PROCEDURE "sap.fsdm.procedures::CommodityEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::CommodityTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::CommodityTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::CommodityTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "CommodityID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "CommodityID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."CommodityID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Commodity" "OLD"
            on
                "IN"."CommodityID" = "OLD"."CommodityID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "CommodityID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."CommodityID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::Commodity_Historical" "OLD"
            on
                "IN"."CommodityID" = "OLD"."CommodityID" 
        );

END
