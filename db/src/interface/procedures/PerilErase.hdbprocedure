PROCEDURE "sap.fsdm.procedures::PerilErase" (IN ROW "sap.fsdm.tabletypes::PerilTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "Peril" is null and
            "_ReinsuranceCoverage.CoverageID" is null and
            "_ReinsuranceCoverage.SectionID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" is null and
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::Peril"
        WHERE
        (            "Peril" ,
            "_ReinsuranceCoverage.CoverageID" ,
            "_ReinsuranceCoverage.SectionID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        ) in
        (
            select                 "OLD"."Peril" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::Peril" "OLD"
            on
            "IN"."Peril" = "OLD"."Peril" and
            "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
            "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
            "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
            "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::Peril_Historical"
        WHERE
        (
            "Peril" ,
            "_ReinsuranceCoverage.CoverageID" ,
            "_ReinsuranceCoverage.SectionID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
            "_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        ) in
        (
            select
                "OLD"."Peril" ,
                "OLD"."_ReinsuranceCoverage.CoverageID" ,
                "OLD"."_ReinsuranceCoverage.SectionID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" ,
                "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::Peril_Historical" "OLD"
            on
                "IN"."Peril" = "OLD"."Peril" and
                "IN"."_ReinsuranceCoverage.CoverageID" = "OLD"."_ReinsuranceCoverage.CoverageID" and
                "IN"."_ReinsuranceCoverage.SectionID" = "OLD"."_ReinsuranceCoverage.SectionID" and
                "IN"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.FinancialContractID" and
                "IN"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" = "OLD"."_ReinsuranceCoverage._ReinsuranceContract.IDSystem" 
        );

END
