PROCEDURE "sap.fsdm.procedures::VoiceAccessDelReadOnly" (IN ROW "sap.fsdm.tabletypes::VoiceAccessTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::VoiceAccessTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::VoiceAccessTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'AccountName=' || TO_VARCHAR("AccountName") || ' ' ||
                'ASSOC_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartner.BusinessPartnerID") || ' ' ||
                'ASSOC_VoiceAccessSystem.BankingChannelID=' || TO_VARCHAR("ASSOC_VoiceAccessSystem.BankingChannelID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."AccountName",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_VoiceAccessSystem.BankingChannelID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."AccountName",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_VoiceAccessSystem.BankingChannelID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "AccountName",
                        "ASSOC_BusinessPartner.BusinessPartnerID",
                        "ASSOC_VoiceAccessSystem.BankingChannelID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."AccountName",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_VoiceAccessSystem.BankingChannelID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."AccountName",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_VoiceAccessSystem.BankingChannelID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountName" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_VoiceAccessSystem.BankingChannelID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "AccountName",
            "ASSOC_BusinessPartner.BusinessPartnerID",
            "ASSOC_VoiceAccessSystem.BankingChannelID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::VoiceAccess" WHERE
            (
            "AccountName" ,
            "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "ASSOC_VoiceAccessSystem.BankingChannelID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."AccountName",
            "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::VoiceAccess" as "OLD"
        on
                              "IN"."AccountName" = "OLD"."AccountName" and
                              "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                              "IN"."ASSOC_VoiceAccessSystem.BankingChannelID" = "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "AccountName",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_VoiceAccessSystem.BankingChannelID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "VoiceAccessStatus",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_AccountName" as "AccountName" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_VoiceAccessSystem.BankingChannelID" as "ASSOC_VoiceAccessSystem.BankingChannelID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_VoiceAccessStatus" as "VoiceAccessStatus" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."AccountName",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."AccountName" AS "OLD_AccountName" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" AS "OLD_ASSOC_VoiceAccessSystem.BankingChannelID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."VoiceAccessStatus" AS "OLD_VoiceAccessStatus" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::VoiceAccess" as "OLD"
            on
                                      "IN"."AccountName" = "OLD"."AccountName" and
                                      "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                                      "IN"."ASSOC_VoiceAccessSystem.BankingChannelID" = "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_AccountName" as "AccountName",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_VoiceAccessSystem.BankingChannelID" as "ASSOC_VoiceAccessSystem.BankingChannelID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_VoiceAccessStatus" as "VoiceAccessStatus",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."AccountName",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."AccountName" AS "OLD_AccountName" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" AS "OLD_ASSOC_VoiceAccessSystem.BankingChannelID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."VoiceAccessStatus" AS "OLD_VoiceAccessStatus" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::VoiceAccess" as "OLD"
            on
               "IN"."AccountName" = "OLD"."AccountName" and
               "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
               "IN"."ASSOC_VoiceAccessSystem.BankingChannelID" = "OLD"."ASSOC_VoiceAccessSystem.BankingChannelID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
