PROCEDURE "sap.fsdm.procedures::AssignedAssetDistributionReadOnly" (IN ROW "sap.fsdm.tabletypes::AssignedAssetDistributionTT", OUT CURR_DEL "sap.fsdm.tabletypes::AssignedAssetDistributionTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::AssignedAssetDistributionTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'DistributionType=' || TO_VARCHAR("DistributionType") || ' ' ||
                '_Collection.CollectionID=' || TO_VARCHAR("_Collection.CollectionID") || ' ' ||
                '_Collection.IDSystem=' || TO_VARCHAR("_Collection.IDSystem") || ' ' ||
                '_Collection._Client.BusinessPartnerID=' || TO_VARCHAR("_Collection._Client.BusinessPartnerID") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_Segment.SegmentID=' || TO_VARCHAR("_Segment.SegmentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."DistributionType",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."DistributionType",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_Segment.SegmentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "DistributionType",
                        "_Collection.CollectionID",
                        "_Collection.IDSystem",
                        "_Collection._Client.BusinessPartnerID",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_Segment.SegmentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."DistributionType",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."DistributionType",
                                    "IN"."_Collection.CollectionID",
                                    "IN"."_Collection.IDSystem",
                                    "IN"."_Collection._Client.BusinessPartnerID",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_Segment.SegmentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "DistributionType",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::AssignedAssetDistribution" WHERE
        (            "DistributionType" ,
            "_Collection.CollectionID" ,
            "_Collection.IDSystem" ,
            "_Collection._Client.BusinessPartnerID" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_Segment.SegmentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."DistributionType",
            "OLD"."_Collection.CollectionID",
            "OLD"."_Collection.IDSystem",
            "OLD"."_Collection._Client.BusinessPartnerID",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_Segment.SegmentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::AssignedAssetDistribution" as "OLD"
            on
               ifnull( "IN"."DistributionType",'' ) = "OLD"."DistributionType" and
               ifnull( "IN"."_Collection.CollectionID",'' ) = "OLD"."_Collection.CollectionID" and
               ifnull( "IN"."_Collection.IDSystem",'' ) = "OLD"."_Collection.IDSystem" and
               ifnull( "IN"."_Collection._Client.BusinessPartnerID",'' ) = "OLD"."_Collection._Client.BusinessPartnerID" and
               ifnull( "IN"."_ResultGroup.ResultDataProvider",'' ) = "OLD"."_ResultGroup.ResultDataProvider" and
               ifnull( "IN"."_ResultGroup.ResultGroupID",'' ) = "OLD"."_ResultGroup.ResultGroupID" and
               ifnull( "IN"."_Segment.SegmentID",'' ) = "OLD"."_Segment.SegmentID" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "DistributionType",
        "_Collection.CollectionID",
        "_Collection.IDSystem",
        "_Collection._Client.BusinessPartnerID",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_Segment.SegmentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "DistributionAmount",
        "DistributionAmountCurrency",
        "DistributionRatio",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "DistributionType", '' ) as "DistributionType",
                    ifnull( "_Collection.CollectionID", '' ) as "_Collection.CollectionID",
                    ifnull( "_Collection.IDSystem", '' ) as "_Collection.IDSystem",
                    ifnull( "_Collection._Client.BusinessPartnerID", '' ) as "_Collection._Client.BusinessPartnerID",
                    ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
                    ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
                    ifnull( "_Segment.SegmentID", '' ) as "_Segment.SegmentID",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "DistributionAmount"  ,
                    "DistributionAmountCurrency"  ,
                    "DistributionRatio"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_DistributionType" as "DistributionType" ,
                    "OLD__Collection.CollectionID" as "_Collection.CollectionID" ,
                    "OLD__Collection.IDSystem" as "_Collection.IDSystem" ,
                    "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID" ,
                    "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
                    "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
                    "OLD__Segment.SegmentID" as "_Segment.SegmentID" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_DistributionAmount" as "DistributionAmount" ,
                    "OLD_DistributionAmountCurrency" as "DistributionAmountCurrency" ,
                    "OLD_DistributionRatio" as "DistributionRatio" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."DistributionType",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."DistributionType" as "OLD_DistributionType",
                                "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                                "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                                "OLD"."_Collection._Client.BusinessPartnerID" as "OLD__Collection._Client.BusinessPartnerID",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_Segment.SegmentID" as "OLD__Segment.SegmentID",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."DistributionAmount" as "OLD_DistributionAmount",
                                "OLD"."DistributionAmountCurrency" as "OLD_DistributionAmountCurrency",
                                "OLD"."DistributionRatio" as "OLD_DistributionRatio",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::AssignedAssetDistribution" as "OLD"
            on
                ifnull( "IN"."DistributionType", '') = "OLD"."DistributionType" and
                ifnull( "IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull( "IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" and
                ifnull( "IN"."_Collection._Client.BusinessPartnerID", '') = "OLD"."_Collection._Client.BusinessPartnerID" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_Segment.SegmentID", '') = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_DistributionType" as "DistributionType",
            "OLD__Collection.CollectionID" as "_Collection.CollectionID",
            "OLD__Collection.IDSystem" as "_Collection.IDSystem",
            "OLD__Collection._Client.BusinessPartnerID" as "_Collection._Client.BusinessPartnerID",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__Segment.SegmentID" as "_Segment.SegmentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_DistributionAmount" as "DistributionAmount",
            "OLD_DistributionAmountCurrency" as "DistributionAmountCurrency",
            "OLD_DistributionRatio" as "DistributionRatio",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."DistributionType",
                        "IN"."_Collection.CollectionID",
                        "IN"."_Collection.IDSystem",
                        "IN"."_Collection._Client.BusinessPartnerID",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_Segment.SegmentID",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."DistributionType" as "OLD_DistributionType",
                        "OLD"."_Collection.CollectionID" as "OLD__Collection.CollectionID",
                        "OLD"."_Collection.IDSystem" as "OLD__Collection.IDSystem",
                        "OLD"."_Collection._Client.BusinessPartnerID" as "OLD__Collection._Client.BusinessPartnerID",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_Segment.SegmentID" as "OLD__Segment.SegmentID",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."DistributionAmount" as "OLD_DistributionAmount",
                        "OLD"."DistributionAmountCurrency" as "OLD_DistributionAmountCurrency",
                        "OLD"."DistributionRatio" as "OLD_DistributionRatio",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::AssignedAssetDistribution" as "OLD"
            on
                ifnull("IN"."DistributionType", '') = "OLD"."DistributionType" and
                ifnull("IN"."_Collection.CollectionID", '') = "OLD"."_Collection.CollectionID" and
                ifnull("IN"."_Collection.IDSystem", '') = "OLD"."_Collection.IDSystem" and
                ifnull("IN"."_Collection._Client.BusinessPartnerID", '') = "OLD"."_Collection._Client.BusinessPartnerID" and
                ifnull("IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull("IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull("IN"."_Segment.SegmentID", '') = "OLD"."_Segment.SegmentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
