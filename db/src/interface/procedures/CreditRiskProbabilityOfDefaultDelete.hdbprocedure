PROCEDURE "sap.fsdm.procedures::CreditRiskProbabilityOfDefaultDelete" (IN ROW "sap.fsdm.tabletypes::CreditRiskProbabilityOfDefaultTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'PDEstimationMethod=' || TO_VARCHAR("PDEstimationMethod") || ' ' ||
                'PDTimeHorizon=' || TO_VARCHAR("PDTimeHorizon") || ' ' ||
                'PDTimeHorizonUnit=' || TO_VARCHAR("PDTimeHorizonUnit") || ' ' ||
                'RiskProvisionScenario=' || TO_VARCHAR("RiskProvisionScenario") || ' ' ||
                'ASSOC_BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("ASSOC_BusinessPartner.BusinessPartnerID") || ' ' ||
                'ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                'ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_FinancialInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialInstrument.FinancialInstrumentID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."PDEstimationMethod",
                        "IN"."PDTimeHorizon",
                        "IN"."PDTimeHorizonUnit",
                        "IN"."RiskProvisionScenario",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."PDEstimationMethod",
                        "IN"."PDTimeHorizon",
                        "IN"."PDTimeHorizonUnit",
                        "IN"."RiskProvisionScenario",
                        "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "IN"."ASSOC_FinancialContract.FinancialContractID",
                        "IN"."ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialInstrument.FinancialInstrumentID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "PDEstimationMethod",
                        "PDTimeHorizon",
                        "PDTimeHorizonUnit",
                        "RiskProvisionScenario",
                        "ASSOC_BusinessPartner.BusinessPartnerID",
                        "ASSOC_FinancialContract.FinancialContractID",
                        "ASSOC_FinancialContract.IDSystem",
                        "_FinancialInstrument.FinancialInstrumentID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."PDEstimationMethod",
                                    "IN"."PDTimeHorizon",
                                    "IN"."PDTimeHorizonUnit",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."PDEstimationMethod",
                                    "IN"."PDTimeHorizon",
                                    "IN"."PDTimeHorizonUnit",
                                    "IN"."RiskProvisionScenario",
                                    "IN"."ASSOC_BusinessPartner.BusinessPartnerID",
                                    "IN"."ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialInstrument.FinancialInstrumentID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "PDEstimationMethod" is null and
            "PDTimeHorizon" is null and
            "PDTimeHorizonUnit" is null and
            "RiskProvisionScenario" is null and
            "ASSOC_BusinessPartner.BusinessPartnerID" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_FinancialInstrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CreditRiskProbabilityOfDefault" (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "PDBasedOnMarginsOfConservatism",
        "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
        "ProbabilityOfDefault",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PDEstimationMethod" as "PDEstimationMethod" ,
            "OLD_PDTimeHorizon" as "PDTimeHorizon" ,
            "OLD_PDTimeHorizonUnit" as "PDTimeHorizonUnit" ,
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario" ,
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID" ,
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID" ,
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem" ,
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA" ,
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB" ,
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC" ,
            "OLD_PDBasedOnMarginsOfConservatism" as "PDBasedOnMarginsOfConservatism" ,
            "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" as "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" ,
            "OLD_ProbabilityOfDefault" as "ProbabilityOfDefault" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."PDEstimationMethod",
                        "OLD"."PDTimeHorizon",
                        "OLD"."PDTimeHorizonUnit",
                        "OLD"."RiskProvisionScenario",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."PDEstimationMethod" AS "OLD_PDEstimationMethod" ,
                "OLD"."PDTimeHorizon" AS "OLD_PDTimeHorizon" ,
                "OLD"."PDTimeHorizonUnit" AS "OLD_PDTimeHorizonUnit" ,
                "OLD"."RiskProvisionScenario" AS "OLD_RiskProvisionScenario" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."MarginOfConservatismCategoryA" AS "OLD_MarginOfConservatismCategoryA" ,
                "OLD"."MarginOfConservatismCategoryB" AS "OLD_MarginOfConservatismCategoryB" ,
                "OLD"."MarginOfConservatismCategoryC" AS "OLD_MarginOfConservatismCategoryC" ,
                "OLD"."PDBasedOnMarginsOfConservatism" AS "OLD_PDBasedOnMarginsOfConservatism" ,
                "OLD"."PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" AS "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" ,
                "OLD"."ProbabilityOfDefault" AS "OLD_ProbabilityOfDefault" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskProbabilityOfDefault" as "OLD"
            on
                      "IN"."PDEstimationMethod" = "OLD"."PDEstimationMethod" and
                      "IN"."PDTimeHorizon" = "OLD"."PDTimeHorizon" and
                      "IN"."PDTimeHorizonUnit" = "OLD"."PDTimeHorizonUnit" and
                      "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                      "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                      "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                      "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                      "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CreditRiskProbabilityOfDefault" (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MarginOfConservatismCategoryA",
        "MarginOfConservatismCategoryB",
        "MarginOfConservatismCategoryC",
        "PDBasedOnMarginsOfConservatism",
        "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
        "ProbabilityOfDefault",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_PDEstimationMethod" as "PDEstimationMethod",
            "OLD_PDTimeHorizon" as "PDTimeHorizon",
            "OLD_PDTimeHorizonUnit" as "PDTimeHorizonUnit",
            "OLD_RiskProvisionScenario" as "RiskProvisionScenario",
            "OLD_ASSOC_BusinessPartner.BusinessPartnerID" as "ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD_ASSOC_FinancialContract.FinancialContractID" as "ASSOC_FinancialContract.FinancialContractID",
            "OLD_ASSOC_FinancialContract.IDSystem" as "ASSOC_FinancialContract.IDSystem",
            "OLD__FinancialInstrument.FinancialInstrumentID" as "_FinancialInstrument.FinancialInstrumentID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_MarginOfConservatismCategoryA" as "MarginOfConservatismCategoryA",
            "OLD_MarginOfConservatismCategoryB" as "MarginOfConservatismCategoryB",
            "OLD_MarginOfConservatismCategoryC" as "MarginOfConservatismCategoryC",
            "OLD_PDBasedOnMarginsOfConservatism" as "PDBasedOnMarginsOfConservatism",
            "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" as "PDBasedOnMarginsOfConservatismAndSupervisoryMeasures",
            "OLD_ProbabilityOfDefault" as "ProbabilityOfDefault",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."PDEstimationMethod",
                        "OLD"."PDTimeHorizon",
                        "OLD"."PDTimeHorizonUnit",
                        "OLD"."RiskProvisionScenario",
                        "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
                        "OLD"."ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialInstrument.FinancialInstrumentID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."PDEstimationMethod" AS "OLD_PDEstimationMethod" ,
                "OLD"."PDTimeHorizon" AS "OLD_PDTimeHorizon" ,
                "OLD"."PDTimeHorizonUnit" AS "OLD_PDTimeHorizonUnit" ,
                "OLD"."RiskProvisionScenario" AS "OLD_RiskProvisionScenario" ,
                "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" AS "OLD_ASSOC_BusinessPartner.BusinessPartnerID" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" AS "OLD_ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" AS "OLD_ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_FinancialInstrument.FinancialInstrumentID" AS "OLD__FinancialInstrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."MarginOfConservatismCategoryA" AS "OLD_MarginOfConservatismCategoryA" ,
                "OLD"."MarginOfConservatismCategoryB" AS "OLD_MarginOfConservatismCategoryB" ,
                "OLD"."MarginOfConservatismCategoryC" AS "OLD_MarginOfConservatismCategoryC" ,
                "OLD"."PDBasedOnMarginsOfConservatism" AS "OLD_PDBasedOnMarginsOfConservatism" ,
                "OLD"."PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" AS "OLD_PDBasedOnMarginsOfConservatismAndSupervisoryMeasures" ,
                "OLD"."ProbabilityOfDefault" AS "OLD_ProbabilityOfDefault" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CreditRiskProbabilityOfDefault" as "OLD"
            on
                                                "IN"."PDEstimationMethod" = "OLD"."PDEstimationMethod" and
                                                "IN"."PDTimeHorizon" = "OLD"."PDTimeHorizon" and
                                                "IN"."PDTimeHorizonUnit" = "OLD"."PDTimeHorizonUnit" and
                                                "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                                                "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                                                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                                "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CreditRiskProbabilityOfDefault"
    where (
        "PDEstimationMethod",
        "PDTimeHorizon",
        "PDTimeHorizonUnit",
        "RiskProvisionScenario",
        "ASSOC_BusinessPartner.BusinessPartnerID",
        "ASSOC_FinancialContract.FinancialContractID",
        "ASSOC_FinancialContract.IDSystem",
        "_FinancialInstrument.FinancialInstrumentID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."PDEstimationMethod",
            "OLD"."PDTimeHorizon",
            "OLD"."PDTimeHorizonUnit",
            "OLD"."RiskProvisionScenario",
            "OLD"."ASSOC_BusinessPartner.BusinessPartnerID",
            "OLD"."ASSOC_FinancialContract.FinancialContractID",
            "OLD"."ASSOC_FinancialContract.IDSystem",
            "OLD"."_FinancialInstrument.FinancialInstrumentID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CreditRiskProbabilityOfDefault" as "OLD"
        on
                                       "IN"."PDEstimationMethod" = "OLD"."PDEstimationMethod" and
                                       "IN"."PDTimeHorizon" = "OLD"."PDTimeHorizon" and
                                       "IN"."PDTimeHorizonUnit" = "OLD"."PDTimeHorizonUnit" and
                                       "IN"."RiskProvisionScenario" = "OLD"."RiskProvisionScenario" and
                                       "IN"."ASSOC_BusinessPartner.BusinessPartnerID" = "OLD"."ASSOC_BusinessPartner.BusinessPartnerID" and
                                       "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                                       "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                                       "IN"."_FinancialInstrument.FinancialInstrumentID" = "OLD"."_FinancialInstrument.FinancialInstrumentID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
