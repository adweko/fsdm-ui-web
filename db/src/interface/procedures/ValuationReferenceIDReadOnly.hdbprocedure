PROCEDURE "sap.fsdm.procedures::ValuationReferenceIDReadOnly" (IN ROW "sap.fsdm.tabletypes::ValuationReferenceIDTT", OUT CURR_DEL "sap.fsdm.tabletypes::ValuationReferenceIDTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ValuationReferenceIDTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SystemID=' || TO_VARCHAR("SystemID") || ' ' ||
                '_Valuation.Scenario=' || TO_VARCHAR("_Valuation.Scenario") || ' ' ||
                '_Valuation.ValuationMethod=' || TO_VARCHAR("_Valuation.ValuationMethod") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SystemID",
                        "IN"."_Valuation.Scenario",
                        "IN"."_Valuation.ValuationMethod"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SystemID",
                        "IN"."_Valuation.Scenario",
                        "IN"."_Valuation.ValuationMethod"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SystemID",
                        "_Valuation.Scenario",
                        "_Valuation.ValuationMethod"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SystemID",
                                    "IN"."_Valuation.Scenario",
                                    "IN"."_Valuation.ValuationMethod"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SystemID",
                                    "IN"."_Valuation.Scenario",
                                    "IN"."_Valuation.ValuationMethod"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "SystemID",
        "_Valuation.Scenario",
        "_Valuation.ValuationMethod",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::ValuationReferenceID" WHERE
        (            "SystemID" ,
            "_Valuation.Scenario" ,
            "_Valuation.ValuationMethod" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."SystemID",
            "OLD"."_Valuation.Scenario",
            "OLD"."_Valuation.ValuationMethod",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::ValuationReferenceID" as "OLD"
            on
               ifnull( "IN"."SystemID",'' ) = "OLD"."SystemID" and
               ifnull( "IN"."_Valuation.Scenario",'' ) = "OLD"."_Valuation.Scenario" and
               ifnull( "IN"."_Valuation.ValuationMethod",'' ) = "OLD"."_Valuation.ValuationMethod" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "SystemID",
        "_Valuation.Scenario",
        "_Valuation.ValuationMethod",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ReferenceID",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "SystemID", '' ) as "SystemID",
                    ifnull( "_Valuation.Scenario", '' ) as "_Valuation.Scenario",
                    ifnull( "_Valuation.ValuationMethod", '' ) as "_Valuation.ValuationMethod",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "ReferenceID"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_SystemID" as "SystemID" ,
                    "OLD__Valuation.Scenario" as "_Valuation.Scenario" ,
                    "OLD__Valuation.ValuationMethod" as "_Valuation.ValuationMethod" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_ReferenceID" as "ReferenceID" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."SystemID",
                        "IN"."_Valuation.Scenario",
                        "IN"."_Valuation.ValuationMethod",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."SystemID" as "OLD_SystemID",
                                "OLD"."_Valuation.Scenario" as "OLD__Valuation.Scenario",
                                "OLD"."_Valuation.ValuationMethod" as "OLD__Valuation.ValuationMethod",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."ReferenceID" as "OLD_ReferenceID",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ValuationReferenceID" as "OLD"
            on
                ifnull( "IN"."SystemID", '') = "OLD"."SystemID" and
                ifnull( "IN"."_Valuation.Scenario", '') = "OLD"."_Valuation.Scenario" and
                ifnull( "IN"."_Valuation.ValuationMethod", '') = "OLD"."_Valuation.ValuationMethod" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_SystemID" as "SystemID",
            "OLD__Valuation.Scenario" as "_Valuation.Scenario",
            "OLD__Valuation.ValuationMethod" as "_Valuation.ValuationMethod",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ReferenceID" as "ReferenceID",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."SystemID",
                        "IN"."_Valuation.Scenario",
                        "IN"."_Valuation.ValuationMethod",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."SystemID" as "OLD_SystemID",
                        "OLD"."_Valuation.Scenario" as "OLD__Valuation.Scenario",
                        "OLD"."_Valuation.ValuationMethod" as "OLD__Valuation.ValuationMethod",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."ReferenceID" as "OLD_ReferenceID",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::ValuationReferenceID" as "OLD"
            on
                ifnull("IN"."SystemID", '') = "OLD"."SystemID" and
                ifnull("IN"."_Valuation.Scenario", '') = "OLD"."_Valuation.Scenario" and
                ifnull("IN"."_Valuation.ValuationMethod", '') = "OLD"."_Valuation.ValuationMethod" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
