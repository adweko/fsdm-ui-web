PROCEDURE "sap.fsdm.procedures::ValuationScheduleListDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ValuationScheduleTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ValuationScheduleTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ValuationScheduleTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                '_TotalReturnSwapReturnLeg._Swap.FinancialContractID=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._Swap.FinancialContractID") || ' ' ||
                '_TotalReturnSwapReturnLeg._Swap.IDSystem=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._Swap.IDSystem") || ' ' ||
                '_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID") || ' ' ||
                '_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID=' || TO_VARCHAR("_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                                    "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                                    "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                                    "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                                    "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                                    "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" is null and
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" is null and
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" is null and
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceNumber",
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
            "_TotalReturnSwapReturnLeg._Swap.IDSystem",
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::ValuationSchedule" WHERE
            (
            "SequenceNumber" ,
            "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
            "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
            "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
            "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceNumber",
            "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
            "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
            "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
            "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ValuationSchedule" as "OLD"
        on
                              "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                              "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" and
                              "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                              "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceNumber",
        "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
        "_TotalReturnSwapReturnLeg._Swap.IDSystem",
        "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
        "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "BusinessCalendar",
        "BusinessDayConvention",
        "FirstValuationDate",
        "LastValuationDate",
        "ValuationDate",
        "ValuationPeriodLength",
        "ValuationPeriodUnit",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID" as "_TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
            "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem" as "_TotalReturnSwapReturnLeg._Swap.IDSystem" ,
            "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" as "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
            "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" as "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_BusinessCalendar" as "BusinessCalendar" ,
            "OLD_BusinessDayConvention" as "BusinessDayConvention" ,
            "OLD_FirstValuationDate" as "FirstValuationDate" ,
            "OLD_LastValuationDate" as "LastValuationDate" ,
            "OLD_ValuationDate" as "ValuationDate" ,
            "OLD_ValuationPeriodLength" as "ValuationPeriodLength" ,
            "OLD_ValuationPeriodUnit" as "ValuationPeriodUnit" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
                        "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" AS "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" AS "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" AS "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" AS "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."FirstValuationDate" AS "OLD_FirstValuationDate" ,
                "OLD"."LastValuationDate" AS "OLD_LastValuationDate" ,
                "OLD"."ValuationDate" AS "OLD_ValuationDate" ,
                "OLD"."ValuationPeriodLength" AS "OLD_ValuationPeriodLength" ,
                "OLD"."ValuationPeriodUnit" AS "OLD_ValuationPeriodUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ValuationSchedule" as "OLD"
            on
                                      "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
                                      "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" and
                                      "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
                                      "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID" as "_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
            "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem" as "_TotalReturnSwapReturnLeg._Swap.IDSystem",
            "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" as "_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
            "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" as "_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_BusinessCalendar" as "BusinessCalendar",
            "OLD_BusinessDayConvention" as "BusinessDayConvention",
            "OLD_FirstValuationDate" as "FirstValuationDate",
            "OLD_LastValuationDate" as "LastValuationDate",
            "OLD_ValuationDate" as "ValuationDate",
            "OLD_ValuationPeriodLength" as "ValuationPeriodLength",
            "OLD_ValuationPeriodUnit" as "ValuationPeriodUnit",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID",
                        "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID",
                        "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem",
                        "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" AS "OLD__TotalReturnSwapReturnLeg._Swap.FinancialContractID" ,
                "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" AS "OLD__TotalReturnSwapReturnLeg._Swap.IDSystem" ,
                "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" AS "OLD__TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" ,
                "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" AS "OLD__TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."BusinessCalendar" AS "OLD_BusinessCalendar" ,
                "OLD"."BusinessDayConvention" AS "OLD_BusinessDayConvention" ,
                "OLD"."FirstValuationDate" AS "OLD_FirstValuationDate" ,
                "OLD"."LastValuationDate" AS "OLD_LastValuationDate" ,
                "OLD"."ValuationDate" AS "OLD_ValuationDate" ,
                "OLD"."ValuationPeriodLength" AS "OLD_ValuationPeriodLength" ,
                "OLD"."ValuationPeriodUnit" AS "OLD_ValuationPeriodUnit" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ValuationSchedule" as "OLD"
            on
               "IN"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" = "OLD"."_TotalReturnSwapReturnLeg._Underlying.FinancialInstrumentID" and
               "IN"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" = "OLD"."_TotalReturnSwapReturnLeg._UnderlyingIndex.IndexID" and
               "IN"."_TotalReturnSwapReturnLeg._Swap.IDSystem" = "OLD"."_TotalReturnSwapReturnLeg._Swap.IDSystem" and
               "IN"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" = "OLD"."_TotalReturnSwapReturnLeg._Swap.FinancialContractID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
