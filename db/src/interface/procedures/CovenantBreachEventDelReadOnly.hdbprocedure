PROCEDURE "sap.fsdm.procedures::CovenantBreachEventDelReadOnly" (IN ROW "sap.fsdm.tabletypes::CovenantBreachEventTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::CovenantBreachEventTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CovenantBreachEventTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CovenantBreachEventDate=' || TO_VARCHAR("CovenantBreachEventDate") || ' ' ||
                '_FinancialCovenant.SequenceNumber=' || TO_VARCHAR("_FinancialCovenant.SequenceNumber") || ' ' ||
                '_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialCovenant.ASSOC_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialCovenant.ASSOC_FinancialContract.IDSystem") || ' ' ||
                '_FinancialCovenant._BusinessPartner.BusinessPartnerID=' || TO_VARCHAR("_FinancialCovenant._BusinessPartner.BusinessPartnerID") || ' ' ||
                '_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID=' || TO_VARCHAR("_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID") || ' ' ||
                '_FinancialCovenant._ContractBundle.ContractBundleID=' || TO_VARCHAR("_FinancialCovenant._ContractBundle.ContractBundleID") || ' ' ||
                '_FinancialCovenant._PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_FinancialCovenant._PhysicalAsset.PhysicalAssetID") || ' ' ||
                '_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber=' || TO_VARCHAR("_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber") || ' ' ||
                '_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID=' || TO_VARCHAR("_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID") || ' ' ||
                '_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem=' || TO_VARCHAR("_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CovenantBreachEventDate",
                        "IN"."_FinancialCovenant.SequenceNumber",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CovenantBreachEventDate",
                        "IN"."_FinancialCovenant.SequenceNumber",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CovenantBreachEventDate",
                        "_FinancialCovenant.SequenceNumber",
                        "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "_FinancialCovenant._ContractBundle.ContractBundleID",
                        "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CovenantBreachEventDate",
                                    "IN"."_FinancialCovenant.SequenceNumber",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                                    "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                                    "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CovenantBreachEventDate",
                                    "IN"."_FinancialCovenant.SequenceNumber",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                                    "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                                    "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                                    "IN"."_FinancialCovenant._ContractBundle.ContractBundleID",
                                    "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                                    "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CovenantBreachEventDate" is null and
            "_FinancialCovenant.SequenceNumber" is null and
            "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" is null and
            "_FinancialCovenant.ASSOC_FinancialContract.IDSystem" is null and
            "_FinancialCovenant._BusinessPartner.BusinessPartnerID" is null and
            "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" is null and
            "_FinancialCovenant._ContractBundle.ContractBundleID" is null and
            "_FinancialCovenant._PhysicalAsset.PhysicalAssetID" is null and
            "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" is null and
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "CovenantBreachEventDate",
            "_FinancialCovenant.SequenceNumber",
            "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
            "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
            "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
            "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
            "_FinancialCovenant._ContractBundle.ContractBundleID",
            "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
            "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::CovenantBreachEvent" WHERE
            (
            "CovenantBreachEventDate" ,
            "_FinancialCovenant.SequenceNumber" ,
            "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
            "_FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
            "_FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
            "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "_FinancialCovenant._ContractBundle.ContractBundleID" ,
            "_FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
            "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."CovenantBreachEventDate",
            "OLD"."_FinancialCovenant.SequenceNumber",
            "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
            "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
            "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
            "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID",
            "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
            "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
            "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CovenantBreachEvent" as "OLD"
        on
                              "IN"."CovenantBreachEventDate" = "OLD"."CovenantBreachEventDate" and
                              "IN"."_FinancialCovenant.SequenceNumber" = "OLD"."_FinancialCovenant.SequenceNumber" and
                              "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" and
                              "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" and
                              "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" = "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" and
                              "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" and
                              "IN"."_FinancialCovenant._ContractBundle.ContractBundleID" = "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" and
                              "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" = "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" and
                              "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" and
                              "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                              "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "CovenantBreachEventDate",
        "_FinancialCovenant.SequenceNumber",
        "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
        "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
        "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
        "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
        "_FinancialCovenant._ContractBundle.ContractBundleID",
        "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
        "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
        "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_BankingChannel.BankingChannelID",
        "_BusinessEventDataOwner.BusinessPartnerID",
        "_CancellationOption.SequenceNumber",
        "_CancellationOption.ASSOC_FinancialContract.FinancialContractID",
        "_CancellationOption.ASSOC_FinancialContract.IDSystem",
        "_CancellationOption._RentalContractInformation.RentalContractInformationReferenceID",
        "_CancellationOption._Trade.IDSystem",
        "_CancellationOption._Trade.TradeID",
        "_ConversionRight.SequenceNumber",
        "_ConversionRight._DebtInstrument.FinancialInstrumentID",
        "_WriteDownMechanism.SequenceNumber",
        "_WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID",
        "CovenantBreachEventOutcome",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_CovenantBreachEventDate" as "CovenantBreachEventDate" ,
            "OLD__FinancialCovenant.SequenceNumber" as "_FinancialCovenant.SequenceNumber" ,
            "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" as "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
            "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" as "_FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
            "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" as "_FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
            "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" as "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "OLD__FinancialCovenant._ContractBundle.ContractBundleID" as "_FinancialCovenant._ContractBundle.ContractBundleID" ,
            "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" as "_FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
            "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" as "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID" ,
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID" ,
            "OLD__CancellationOption.SequenceNumber" as "_CancellationOption.SequenceNumber" ,
            "OLD__CancellationOption.ASSOC_FinancialContract.FinancialContractID" as "_CancellationOption.ASSOC_FinancialContract.FinancialContractID" ,
            "OLD__CancellationOption.ASSOC_FinancialContract.IDSystem" as "_CancellationOption.ASSOC_FinancialContract.IDSystem" ,
            "OLD__CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" as "_CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
            "OLD__CancellationOption._Trade.IDSystem" as "_CancellationOption._Trade.IDSystem" ,
            "OLD__CancellationOption._Trade.TradeID" as "_CancellationOption._Trade.TradeID" ,
            "OLD__ConversionRight.SequenceNumber" as "_ConversionRight.SequenceNumber" ,
            "OLD__ConversionRight._DebtInstrument.FinancialInstrumentID" as "_ConversionRight._DebtInstrument.FinancialInstrumentID" ,
            "OLD__WriteDownMechanism.SequenceNumber" as "_WriteDownMechanism.SequenceNumber" ,
            "OLD__WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" as "_WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "OLD_CovenantBreachEventOutcome" as "CovenantBreachEventOutcome" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CovenantBreachEventDate",
                        "OLD"."_FinancialCovenant.SequenceNumber",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."CovenantBreachEventDate" AS "OLD_CovenantBreachEventDate" ,
                "OLD"."_FinancialCovenant.SequenceNumber" AS "OLD__FinancialCovenant.SequenceNumber" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" AS "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" AS "OLD__FinancialCovenant._ContractBundle.ContractBundleID" ,
                "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" AS "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" AS "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_BankingChannel.BankingChannelID" AS "OLD__BankingChannel.BankingChannelID" ,
                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" AS "OLD__BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."_CancellationOption.SequenceNumber" AS "OLD__CancellationOption.SequenceNumber" ,
                "OLD"."_CancellationOption.ASSOC_FinancialContract.FinancialContractID" AS "OLD__CancellationOption.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_CancellationOption.ASSOC_FinancialContract.IDSystem" AS "OLD__CancellationOption.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."_CancellationOption._Trade.IDSystem" AS "OLD__CancellationOption._Trade.IDSystem" ,
                "OLD"."_CancellationOption._Trade.TradeID" AS "OLD__CancellationOption._Trade.TradeID" ,
                "OLD"."_ConversionRight.SequenceNumber" AS "OLD__ConversionRight.SequenceNumber" ,
                "OLD"."_ConversionRight._DebtInstrument.FinancialInstrumentID" AS "OLD__ConversionRight._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_WriteDownMechanism.SequenceNumber" AS "OLD__WriteDownMechanism.SequenceNumber" ,
                "OLD"."_WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."CovenantBreachEventOutcome" AS "OLD_CovenantBreachEventOutcome" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CovenantBreachEvent" as "OLD"
            on
                                      "IN"."CovenantBreachEventDate" = "OLD"."CovenantBreachEventDate" and
                                      "IN"."_FinancialCovenant.SequenceNumber" = "OLD"."_FinancialCovenant.SequenceNumber" and
                                      "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" and
                                      "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" and
                                      "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" = "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" and
                                      "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" and
                                      "IN"."_FinancialCovenant._ContractBundle.ContractBundleID" = "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" and
                                      "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" = "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" and
                                      "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" and
                                      "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                                      "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_CovenantBreachEventDate" as "CovenantBreachEventDate",
            "OLD__FinancialCovenant.SequenceNumber" as "_FinancialCovenant.SequenceNumber",
            "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" as "_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
            "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" as "_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
            "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" as "_FinancialCovenant._BusinessPartner.BusinessPartnerID",
            "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" as "_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD__FinancialCovenant._ContractBundle.ContractBundleID" as "_FinancialCovenant._ContractBundle.ContractBundleID",
            "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" as "_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
            "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" as "_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
            "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" as "_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__BankingChannel.BankingChannelID" as "_BankingChannel.BankingChannelID",
            "OLD__BusinessEventDataOwner.BusinessPartnerID" as "_BusinessEventDataOwner.BusinessPartnerID",
            "OLD__CancellationOption.SequenceNumber" as "_CancellationOption.SequenceNumber",
            "OLD__CancellationOption.ASSOC_FinancialContract.FinancialContractID" as "_CancellationOption.ASSOC_FinancialContract.FinancialContractID",
            "OLD__CancellationOption.ASSOC_FinancialContract.IDSystem" as "_CancellationOption.ASSOC_FinancialContract.IDSystem",
            "OLD__CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" as "_CancellationOption._RentalContractInformation.RentalContractInformationReferenceID",
            "OLD__CancellationOption._Trade.IDSystem" as "_CancellationOption._Trade.IDSystem",
            "OLD__CancellationOption._Trade.TradeID" as "_CancellationOption._Trade.TradeID",
            "OLD__ConversionRight.SequenceNumber" as "_ConversionRight.SequenceNumber",
            "OLD__ConversionRight._DebtInstrument.FinancialInstrumentID" as "_ConversionRight._DebtInstrument.FinancialInstrumentID",
            "OLD__WriteDownMechanism.SequenceNumber" as "_WriteDownMechanism.SequenceNumber",
            "OLD__WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" as "_WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID",
            "OLD_CovenantBreachEventOutcome" as "CovenantBreachEventOutcome",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CovenantBreachEventDate",
                        "OLD"."_FinancialCovenant.SequenceNumber",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID",
                        "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem",
                        "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID",
                        "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID",
                        "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID",
                        "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID",
                        "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CovenantBreachEventDate" AS "OLD_CovenantBreachEventDate" ,
                "OLD"."_FinancialCovenant.SequenceNumber" AS "OLD__FinancialCovenant.SequenceNumber" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" AS "OLD__FinancialCovenant.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" AS "OLD__FinancialCovenant._BusinessPartner.BusinessPartnerID" ,
                "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" AS "OLD__FinancialCovenant._ContractBundle.ContractBundleID" ,
                "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" AS "OLD__FinancialCovenant._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" AS "OLD__FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" AS "OLD__FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."_BankingChannel.BankingChannelID" AS "OLD__BankingChannel.BankingChannelID" ,
                "OLD"."_BusinessEventDataOwner.BusinessPartnerID" AS "OLD__BusinessEventDataOwner.BusinessPartnerID" ,
                "OLD"."_CancellationOption.SequenceNumber" AS "OLD__CancellationOption.SequenceNumber" ,
                "OLD"."_CancellationOption.ASSOC_FinancialContract.FinancialContractID" AS "OLD__CancellationOption.ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."_CancellationOption.ASSOC_FinancialContract.IDSystem" AS "OLD__CancellationOption.ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" AS "OLD__CancellationOption._RentalContractInformation.RentalContractInformationReferenceID" ,
                "OLD"."_CancellationOption._Trade.IDSystem" AS "OLD__CancellationOption._Trade.IDSystem" ,
                "OLD"."_CancellationOption._Trade.TradeID" AS "OLD__CancellationOption._Trade.TradeID" ,
                "OLD"."_ConversionRight.SequenceNumber" AS "OLD__ConversionRight.SequenceNumber" ,
                "OLD"."_ConversionRight._DebtInstrument.FinancialInstrumentID" AS "OLD__ConversionRight._DebtInstrument.FinancialInstrumentID" ,
                "OLD"."_WriteDownMechanism.SequenceNumber" AS "OLD__WriteDownMechanism.SequenceNumber" ,
                "OLD"."_WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" AS "OLD__WriteDownMechanism._ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."CovenantBreachEventOutcome" AS "OLD_CovenantBreachEventOutcome" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CovenantBreachEvent" as "OLD"
            on
               "IN"."CovenantBreachEventDate" = "OLD"."CovenantBreachEventDate" and
               "IN"."_FinancialCovenant.SequenceNumber" = "OLD"."_FinancialCovenant.SequenceNumber" and
               "IN"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.FinancialContractID" and
               "IN"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" = "OLD"."_FinancialCovenant.ASSOC_FinancialContract.IDSystem" and
               "IN"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" = "OLD"."_FinancialCovenant._BusinessPartner.BusinessPartnerID" and
               "IN"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_FinancialCovenant._ContingentConvertibleInstrument.FinancialInstrumentID" and
               "IN"."_FinancialCovenant._ContractBundle.ContractBundleID" = "OLD"."_FinancialCovenant._ContractBundle.ContractBundleID" and
               "IN"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" = "OLD"."_FinancialCovenant._PhysicalAsset.PhysicalAssetID" and
               "IN"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_FinancialCovenant._TrancheInSyndication.TrancheSequenceNumber" and
               "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
               "IN"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_FinancialCovenant._TrancheInSyndication._SyndicationAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
