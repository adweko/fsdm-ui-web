PROCEDURE "sap.fsdm.procedures::CardProgramParticipationDelete" (IN ROW "sap.fsdm.tabletypes::CardProgramParticipationTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'CardProgramID=' || TO_VARCHAR("CardProgramID") || ' ' ||
                'ASSOC_CardIssue.CardIssueID=' || TO_VARCHAR("ASSOC_CardIssue.CardIssueID") || ' ' ||
                'ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID=' || TO_VARCHAR("ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID") || ' ' ||
                'ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem=' || TO_VARCHAR("ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem") || ' ' ||
                'ASSOC_CardIssue._BankAccount.FinancialContractID=' || TO_VARCHAR("ASSOC_CardIssue._BankAccount.FinancialContractID") || ' ' ||
                'ASSOC_CardIssue._BankAccount.IDSystem=' || TO_VARCHAR("ASSOC_CardIssue._BankAccount.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."CardProgramID",
                        "IN"."ASSOC_CardIssue.CardIssueID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."CardProgramID",
                        "IN"."ASSOC_CardIssue.CardIssueID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "CardProgramID",
                        "ASSOC_CardIssue.CardIssueID",
                        "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "ASSOC_CardIssue._BankAccount.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."CardProgramID",
                                    "IN"."ASSOC_CardIssue.CardIssueID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                                    "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                                    "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."CardProgramID",
                                    "IN"."ASSOC_CardIssue.CardIssueID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                                    "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                                    "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                                    "IN"."ASSOC_CardIssue._BankAccount.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "CardProgramID" is null and
            "ASSOC_CardIssue.CardIssueID" is null and
            "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" is null and
            "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" is null and
            "ASSOC_CardIssue._BankAccount.FinancialContractID" is null and
            "ASSOC_CardIssue._BankAccount.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::CardProgramParticipation" (
        "CardProgramID",
        "ASSOC_CardIssue.CardIssueID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
        "ASSOC_CardIssue._BankAccount.FinancialContractID",
        "ASSOC_CardIssue._BankAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CardProgramParticipationNumber",
        "CardProgramStatus",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CardProgramID" as "CardProgramID" ,
            "OLD_ASSOC_CardIssue.CardIssueID" as "ASSOC_CardIssue.CardIssueID" ,
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" as "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" as "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
            "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" as "ASSOC_CardIssue._BankAccount.FinancialContractID" ,
            "OLD_ASSOC_CardIssue._BankAccount.IDSystem" as "ASSOC_CardIssue._BankAccount.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_CardProgramParticipationNumber" as "CardProgramParticipationNumber" ,
            "OLD_CardProgramStatus" as "CardProgramStatus" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."CardProgramID",
                        "OLD"."ASSOC_CardIssue.CardIssueID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "OLD"."ASSOC_CardIssue._BankAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."CardProgramID" AS "OLD_CardProgramID" ,
                "OLD"."ASSOC_CardIssue.CardIssueID" AS "OLD_ASSOC_CardIssue.CardIssueID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" AS "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" AS "OLD_ASSOC_CardIssue._BankAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CardProgramParticipationNumber" AS "OLD_CardProgramParticipationNumber" ,
                "OLD"."CardProgramStatus" AS "OLD_CardProgramStatus" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CardProgramParticipation" as "OLD"
            on
                      "IN"."CardProgramID" = "OLD"."CardProgramID" and
                      "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                      "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                      "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                      "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                      "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::CardProgramParticipation" (
        "CardProgramID",
        "ASSOC_CardIssue.CardIssueID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
        "ASSOC_CardIssue._BankAccount.FinancialContractID",
        "ASSOC_CardIssue._BankAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "CardProgramParticipationNumber",
        "CardProgramStatus",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_CardProgramID" as "CardProgramID",
            "OLD_ASSOC_CardIssue.CardIssueID" as "ASSOC_CardIssue.CardIssueID",
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" as "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
            "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" as "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
            "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" as "ASSOC_CardIssue._BankAccount.FinancialContractID",
            "OLD_ASSOC_CardIssue._BankAccount.IDSystem" as "ASSOC_CardIssue._BankAccount.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_CardProgramParticipationNumber" as "CardProgramParticipationNumber",
            "OLD_CardProgramStatus" as "CardProgramStatus",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."CardProgramID",
                        "OLD"."ASSOC_CardIssue.CardIssueID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
                        "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
                        "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID",
                        "OLD"."ASSOC_CardIssue._BankAccount.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."CardProgramID" AS "OLD_CardProgramID" ,
                "OLD"."ASSOC_CardIssue.CardIssueID" AS "OLD_ASSOC_CardIssue.CardIssueID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" AS "OLD_ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" ,
                "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" AS "OLD_ASSOC_CardIssue._BankAccount.FinancialContractID" ,
                "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" AS "OLD_ASSOC_CardIssue._BankAccount.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."CardProgramParticipationNumber" AS "OLD_CardProgramParticipationNumber" ,
                "OLD"."CardProgramStatus" AS "OLD_CardProgramStatus" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::CardProgramParticipation" as "OLD"
            on
                                                "IN"."CardProgramID" = "OLD"."CardProgramID" and
                                                "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                                                "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                                                "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                                                "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                                                "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::CardProgramParticipation"
    where (
        "CardProgramID",
        "ASSOC_CardIssue.CardIssueID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
        "ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
        "ASSOC_CardIssue._BankAccount.FinancialContractID",
        "ASSOC_CardIssue._BankAccount.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."CardProgramID",
            "OLD"."ASSOC_CardIssue.CardIssueID",
            "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID",
            "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem",
            "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID",
            "OLD"."ASSOC_CardIssue._BankAccount.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::CardProgramParticipation" as "OLD"
        on
                                       "IN"."CardProgramID" = "OLD"."CardProgramID" and
                                       "IN"."ASSOC_CardIssue.CardIssueID" = "OLD"."ASSOC_CardIssue.CardIssueID" and
                                       "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.FinancialContractID" and
                                       "IN"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" = "OLD"."ASSOC_CardIssue.ASSOC_CardAgreement.IDSystem" and
                                       "IN"."ASSOC_CardIssue._BankAccount.FinancialContractID" = "OLD"."ASSOC_CardIssue._BankAccount.FinancialContractID" and
                                       "IN"."ASSOC_CardIssue._BankAccount.IDSystem" = "OLD"."ASSOC_CardIssue._BankAccount.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
