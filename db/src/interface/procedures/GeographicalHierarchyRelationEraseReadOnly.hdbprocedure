PROCEDURE "sap.fsdm.procedures::GeographicalHierarchyRelationEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::GeographicalHierarchyRelationTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::GeographicalHierarchyRelationTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::GeographicalHierarchyRelationTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "GeographicalHierarchyID" is null and
            "ASSOC_Child.GeographicalStructureID" is null and
            "ASSOC_Child.GeographicalUnitID" is null and
            "ASSOC_Parent.GeographicalStructureID" is null and
            "ASSOC_Parent.GeographicalUnitID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "GeographicalHierarchyID" ,
                "ASSOC_Child.GeographicalStructureID" ,
                "ASSOC_Child.GeographicalUnitID" ,
                "ASSOC_Parent.GeographicalStructureID" ,
                "ASSOC_Parent.GeographicalUnitID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."GeographicalHierarchyID" ,
                "OLD"."ASSOC_Child.GeographicalStructureID" ,
                "OLD"."ASSOC_Child.GeographicalUnitID" ,
                "OLD"."ASSOC_Parent.GeographicalStructureID" ,
                "OLD"."ASSOC_Parent.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::GeographicalHierarchyRelation" "OLD"
            on
                "IN"."GeographicalHierarchyID" = "OLD"."GeographicalHierarchyID" and
                "IN"."ASSOC_Child.GeographicalStructureID" = "OLD"."ASSOC_Child.GeographicalStructureID" and
                "IN"."ASSOC_Child.GeographicalUnitID" = "OLD"."ASSOC_Child.GeographicalUnitID" and
                "IN"."ASSOC_Parent.GeographicalStructureID" = "OLD"."ASSOC_Parent.GeographicalStructureID" and
                "IN"."ASSOC_Parent.GeographicalUnitID" = "OLD"."ASSOC_Parent.GeographicalUnitID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "GeographicalHierarchyID" ,
            "ASSOC_Child.GeographicalStructureID" ,
            "ASSOC_Child.GeographicalUnitID" ,
            "ASSOC_Parent.GeographicalStructureID" ,
            "ASSOC_Parent.GeographicalUnitID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."GeographicalHierarchyID" ,
                "OLD"."ASSOC_Child.GeographicalStructureID" ,
                "OLD"."ASSOC_Child.GeographicalUnitID" ,
                "OLD"."ASSOC_Parent.GeographicalStructureID" ,
                "OLD"."ASSOC_Parent.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::GeographicalHierarchyRelation_Historical" "OLD"
            on
                "IN"."GeographicalHierarchyID" = "OLD"."GeographicalHierarchyID" and
                "IN"."ASSOC_Child.GeographicalStructureID" = "OLD"."ASSOC_Child.GeographicalStructureID" and
                "IN"."ASSOC_Child.GeographicalUnitID" = "OLD"."ASSOC_Child.GeographicalUnitID" and
                "IN"."ASSOC_Parent.GeographicalStructureID" = "OLD"."ASSOC_Parent.GeographicalStructureID" and
                "IN"."ASSOC_Parent.GeographicalUnitID" = "OLD"."ASSOC_Parent.GeographicalUnitID" 
        );

END
