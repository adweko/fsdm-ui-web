view "sap.fsdm.SQLViews::Rating_View" 
as select
      "IsFxRating" ,
      "RatingAgency" ,
      "RatingCategory" ,
      "RatingMethod" ,
      "RatingStatus" ,
      "TimeHorizon" ,
      "ASSOC_BusinessPartner.BusinessPartnerID" ,
      "ASSOC_FinancialContract.FinancialContractID" ,
      "ASSOC_FinancialContract.IDSystem" ,
      "ASSOC_GeographicalRegion.GeographicalStructureID" ,
      "ASSOC_GeographicalRegion.GeographicalUnitID" ,
      "_PhysicalAsset.PhysicalAssetID" ,
      "_Security.FinancialInstrumentID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "ASSOC_EmployeeResponsibleForRating.BusinessPartnerID" ,
      "_EmployeeApprovingTheRating.BusinessPartnerID" ,
      "_OverridingRating.IsFxRating" ,
      "_OverridingRating.RatingAgency" ,
      "_OverridingRating.RatingCategory" ,
      "_OverridingRating.RatingMethod" ,
      "_OverridingRating.RatingStatus" ,
      "_OverridingRating.TimeHorizon" ,
      "_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" ,
      "_OverridingRating.ASSOC_FinancialContract.FinancialContractID" ,
      "_OverridingRating.ASSOC_FinancialContract.IDSystem" ,
      "_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" ,
      "_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" ,
      "_OverridingRating._PhysicalAsset.PhysicalAssetID" ,
      "_OverridingRating._Security.FinancialInstrumentID" ,
      "AcquisitionDate" ,
      "ApprovalDate" ,
      "Commissioned" ,
      "CreationDate" ,
      "Endorsed" ,
      "MarginOfConservatismCategoryA" ,
      "MarginOfConservatismCategoryB" ,
      "MarginOfConservatismCategoryC" ,
      "PublicationDate" ,
      "Rating" ,
      "RatingBasedOnMarginsOfConservatism" ,
      "RatingComment" ,
      "RatingDescription" ,
      "RatingMethodVersion" ,
      "RatingOutlook" ,
      "RatingRank" ,
      "RatingStatusChangeDate" ,
      "RatingStatusReason" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::Rating"
with associations
(
	join "sap.fsdm.SQLViews::IndividualRatingsUsedForTotalESGRatingAssignment_View" as "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"
	  on "IsFxRating" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.IsFxRating" AND 
	     "RatingCategory" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.RatingCategory" AND 
	     "RatingStatus" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.RatingStatus" AND 
	     "RatingMethod" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.RatingMethod" AND 
	     "_PhysicalAsset.PhysicalAssetID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating._PhysicalAsset.PhysicalAssetID" AND 
	     "_Security.FinancialInstrumentID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating._Security.FinancialInstrumentID" AND 
	     "ASSOC_BusinessPartner.BusinessPartnerID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.ASSOC_BusinessPartner.BusinessPartnerID" AND 
	     "ASSOC_GeographicalRegion.GeographicalStructureID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.ASSOC_GeographicalRegion.GeographicalStructureID" AND 
	     "RatingAgency" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.RatingAgency" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.ASSOC_FinancialContract.FinancialContractID" AND 
	     "TimeHorizon" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.TimeHorizon" AND 
	     "ASSOC_GeographicalRegion.GeographicalUnitID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.ASSOC_GeographicalRegion.GeographicalUnitID" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_IndividualRatingsUsedForTotalESGRatingAssignment__Rating"."_Rating.ASSOC_FinancialContract.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::IndividualRatingsUsedForTotalESGRatingAssignment_View" as "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"
	  on "_Security.FinancialInstrumentID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating._Security.FinancialInstrumentID" AND 
	     "TimeHorizon" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.TimeHorizon" AND 
	     "RatingAgency" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.RatingAgency" AND 
	     "_PhysicalAsset.PhysicalAssetID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating._PhysicalAsset.PhysicalAssetID" AND 
	     "RatingCategory" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.RatingCategory" AND 
	     "IsFxRating" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.IsFxRating" AND 
	     "ASSOC_GeographicalRegion.GeographicalUnitID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalUnitID" AND 
	     "ASSOC_BusinessPartner.BusinessPartnerID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.ASSOC_BusinessPartner.BusinessPartnerID" AND 
	     "ASSOC_FinancialContract.FinancialContractID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.ASSOC_FinancialContract.FinancialContractID" AND 
	     "RatingStatus" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.RatingStatus" AND 
	     "ASSOC_FinancialContract.IDSystem" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.ASSOC_FinancialContract.IDSystem" AND 
	     "RatingMethod" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.RatingMethod" AND 
	     "ASSOC_GeographicalRegion.GeographicalStructureID" = "_IndividualRatingsUsedForTotalESGRatingAssignment__TotalESGRating"."_TotalESGRating.ASSOC_GeographicalRegion.GeographicalStructureID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "ASSOC_BusinessPartner"
	  on "ASSOC_BusinessPartner"."BusinessPartnerID" = "ASSOC_BusinessPartner.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "ASSOC_EmployeeResponsibleForRating"
	  on "ASSOC_EmployeeResponsibleForRating"."BusinessPartnerID" = "ASSOC_EmployeeResponsibleForRating.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::FinancialContract_View" as "ASSOC_FinancialContract"
	  on "ASSOC_FinancialContract"."FinancialContractID" = "ASSOC_FinancialContract.FinancialContractID" AND 
	     "ASSOC_FinancialContract"."IDSystem" = "ASSOC_FinancialContract.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::GeographicalUnit_View" as "ASSOC_GeographicalRegion"
	  on "ASSOC_GeographicalRegion"."GeographicalUnitID" = "ASSOC_GeographicalRegion.GeographicalUnitID" AND 
	     "ASSOC_GeographicalRegion"."GeographicalStructureID" = "ASSOC_GeographicalRegion.GeographicalStructureID"
	     ,
	join "sap.fsdm.SQLViews::BusinessPartner_View" as "_EmployeeApprovingTheRating"
	  on "_EmployeeApprovingTheRating"."BusinessPartnerID" = "_EmployeeApprovingTheRating.BusinessPartnerID"
	     ,
	join "sap.fsdm.SQLViews::Rating_View" as "_OverridingRating"
	  on "_OverridingRating"."ASSOC_GeographicalRegion.GeographicalStructureID" = "_OverridingRating.ASSOC_GeographicalRegion.GeographicalStructureID" AND 
	     "_OverridingRating"."RatingAgency" = "_OverridingRating.RatingAgency" AND 
	     "_OverridingRating"."_Security.FinancialInstrumentID" = "_OverridingRating._Security.FinancialInstrumentID" AND 
	     "_OverridingRating"."IsFxRating" = "_OverridingRating.IsFxRating" AND 
	     "_OverridingRating"."ASSOC_GeographicalRegion.GeographicalUnitID" = "_OverridingRating.ASSOC_GeographicalRegion.GeographicalUnitID" AND 
	     "_OverridingRating"."ASSOC_FinancialContract.FinancialContractID" = "_OverridingRating.ASSOC_FinancialContract.FinancialContractID" AND 
	     "_OverridingRating"."ASSOC_BusinessPartner.BusinessPartnerID" = "_OverridingRating.ASSOC_BusinessPartner.BusinessPartnerID" AND 
	     "_OverridingRating"."RatingCategory" = "_OverridingRating.RatingCategory" AND 
	     "_OverridingRating"."RatingStatus" = "_OverridingRating.RatingStatus" AND 
	     "_OverridingRating"."RatingMethod" = "_OverridingRating.RatingMethod" AND 
	     "_OverridingRating"."TimeHorizon" = "_OverridingRating.TimeHorizon" AND 
	     "_OverridingRating"."_PhysicalAsset.PhysicalAssetID" = "_OverridingRating._PhysicalAsset.PhysicalAssetID" AND 
	     "_OverridingRating"."ASSOC_FinancialContract.IDSystem" = "_OverridingRating.ASSOC_FinancialContract.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::PhysicalAsset_View" as "_PhysicalAsset"
	  on "_PhysicalAsset"."PhysicalAssetID" = "_PhysicalAsset.PhysicalAssetID"
	     ,
	join "sap.fsdm.SQLViews::FinancialInstrument_View" as "_Security"
	  on "_Security"."FinancialInstrumentID" = "_Security.FinancialInstrumentID"
	     
);