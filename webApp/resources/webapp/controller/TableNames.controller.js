sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/json/JSONModel"
], function (Controller, Filter, FilterOperator, JSONModel) {
    "use strict";

    return Controller.extend("sap.ui.hana.controller.TableNames", {

        onInit: function () {
            var oModel = new JSONModel();
            oModel.loadData("/selectData.xsjs", {name: "tableNames"});
            this.getView().setModel(oModel);
        },

        onSearch: function (oEvent) {
            var aFilters = [];
            var sQuery = oEvent.getSource().getValue();
            if (sQuery && sQuery.length > 0) {
                var filter = new Filter("TABLE_NAME", FilterOperator.Contains, sQuery);
                aFilters.push(filter);
            }
            // update list binding
            var oList = this.byId("idList");
            var oBinding = oList.getBinding("items");

            oBinding.filter(aFilters, "Application");
        },
        onPress: function (oEvent) {
            var oItem = oEvent.getSource();
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.navTo("tableColumns", {parameter: oItem.getBindingContext().getProperty("TABLE_NAME")});
        }

    });
});