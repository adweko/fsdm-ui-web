PROCEDURE "sap.fsdm.procedures::CounterpartyCreditRiskNettingSetResultsReadOnly" (IN ROW "sap.fsdm.tabletypes::CounterpartyCreditRiskNettingSetResultsTT", OUT CURR_DEL "sap.fsdm.tabletypes::CounterpartyCreditRiskNettingSetResultsTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::CounterpartyCreditRiskNettingSetResultsTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'MarginedNettingSet=' || TO_VARCHAR("MarginedNettingSet") || ' ' ||
                '_ExchangeTradedNettingSet.FinancialInstrumentID=' || TO_VARCHAR("_ExchangeTradedNettingSet.FinancialInstrumentID") || ' ' ||
                '_FinancialContractNettingSet.FinancialContractID=' || TO_VARCHAR("_FinancialContractNettingSet.FinancialContractID") || ' ' ||
                '_FinancialContractNettingSet.IDSystem=' || TO_VARCHAR("_FinancialContractNettingSet.IDSystem") || ' ' ||
                '_ResultGroup.ResultDataProvider=' || TO_VARCHAR("_ResultGroup.ResultDataProvider") || ' ' ||
                '_ResultGroup.ResultGroupID=' || TO_VARCHAR("_ResultGroup.ResultGroupID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID") || ' ' ||
                '_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem=' || TO_VARCHAR("_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "MarginedNettingSet",
                        "_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "_FinancialContractNettingSet.FinancialContractID",
                        "_FinancialContractNettingSet.IDSystem",
                        "_ResultGroup.ResultDataProvider",
                        "_ResultGroup.ResultGroupID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."MarginedNettingSet",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."MarginedNettingSet",
                                    "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                                    "IN"."_FinancialContractNettingSet.FinancialContractID",
                                    "IN"."_FinancialContractNettingSet.IDSystem",
                                    "IN"."_ResultGroup.ResultDataProvider",
                                    "IN"."_ResultGroup.ResultGroupID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                    "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::CounterpartyCreditRiskNettingSetResults" WHERE
        (            "MarginedNettingSet" ,
            "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
            "_FinancialContractNettingSet.FinancialContractID" ,
            "_FinancialContractNettingSet.IDSystem" ,
            "_ResultGroup.ResultDataProvider" ,
            "_ResultGroup.ResultGroupID" ,
            "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
            "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."MarginedNettingSet",
            "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD"."_FinancialContractNettingSet.FinancialContractID",
            "OLD"."_FinancialContractNettingSet.IDSystem",
            "OLD"."_ResultGroup.ResultDataProvider",
            "OLD"."_ResultGroup.ResultGroupID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskNettingSetResults" as "OLD"
            on
               ifnull( "IN"."MarginedNettingSet",false ) = "OLD"."MarginedNettingSet" and
               ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",'' ) = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
               ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID",'' ) = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
               ifnull( "IN"."_FinancialContractNettingSet.IDSystem",'' ) = "OLD"."_FinancialContractNettingSet.IDSystem" and
               ifnull( "IN"."_ResultGroup.ResultDataProvider",'' ) = "OLD"."_ResultGroup.ResultDataProvider" and
               ifnull( "IN"."_ResultGroup.ResultGroupID",'' ) = "OLD"."_ResultGroup.ResultGroupID" and
               ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",'' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
               ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",'' ) = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "MarginedNettingSet",
        "_ExchangeTradedNettingSet.FinancialInstrumentID",
        "_FinancialContractNettingSet.FinancialContractID",
        "_FinancialContractNettingSet.IDSystem",
        "_ResultGroup.ResultDataProvider",
        "_ResultGroup.ResultGroupID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
        "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "AddOnAmountForCommodityRisk",
        "AddOnAmountForCreditRisk",
        "AddOnAmountForEquityRisk",
        "AddOnAmountForFXRisk",
        "AddOnAmountForInterestRateRisk",
        "AddOnAmountForOtherRisk",
        "AggregatedAddOnAmount",
        "Currency",
        "ExposureAtDefault",
        "MarginThresholdAmount",
        "MinimumTransferAmount",
        "MultiplierForAggregatedAddOnAmount",
        "NetHaircutValueOfNetCollateralHeld",
        "NetIndependentCollateralAmount",
        "NetInitialMarginAmount",
        "NetMarketValue",
        "NetVariationMarginAmount",
        "NumberOfContractsInNettingSet",
        "PotentialFutureExposure",
        "ReplacementCosts",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "MarginedNettingSet", false ) as "MarginedNettingSet",
                    ifnull( "_ExchangeTradedNettingSet.FinancialInstrumentID", '' ) as "_ExchangeTradedNettingSet.FinancialInstrumentID",
                    ifnull( "_FinancialContractNettingSet.FinancialContractID", '' ) as "_FinancialContractNettingSet.FinancialContractID",
                    ifnull( "_FinancialContractNettingSet.IDSystem", '' ) as "_FinancialContractNettingSet.IDSystem",
                    ifnull( "_ResultGroup.ResultDataProvider", '' ) as "_ResultGroup.ResultDataProvider",
                    ifnull( "_ResultGroup.ResultGroupID", '' ) as "_ResultGroup.ResultGroupID",
                    ifnull( "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '' ) as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                    ifnull( "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '' ) as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "AddOnAmountForCommodityRisk"  ,
                    "AddOnAmountForCreditRisk"  ,
                    "AddOnAmountForEquityRisk"  ,
                    "AddOnAmountForFXRisk"  ,
                    "AddOnAmountForInterestRateRisk"  ,
                    "AddOnAmountForOtherRisk"  ,
                    "AggregatedAddOnAmount"  ,
                    "Currency"  ,
                    "ExposureAtDefault"  ,
                    "MarginThresholdAmount"  ,
                    "MinimumTransferAmount"  ,
                    "MultiplierForAggregatedAddOnAmount"  ,
                    "NetHaircutValueOfNetCollateralHeld"  ,
                    "NetIndependentCollateralAmount"  ,
                    "NetInitialMarginAmount"  ,
                    "NetMarketValue"  ,
                    "NetVariationMarginAmount"  ,
                    "NumberOfContractsInNettingSet"  ,
                    "PotentialFutureExposure"  ,
                    "ReplacementCosts"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_MarginedNettingSet" as "MarginedNettingSet" ,
                    "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID" ,
                    "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID" ,
                    "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem" ,
                    "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider" ,
                    "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID" ,
                    "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" ,
                    "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD_AddOnAmountForCommodityRisk" as "AddOnAmountForCommodityRisk" ,
                    "OLD_AddOnAmountForCreditRisk" as "AddOnAmountForCreditRisk" ,
                    "OLD_AddOnAmountForEquityRisk" as "AddOnAmountForEquityRisk" ,
                    "OLD_AddOnAmountForFXRisk" as "AddOnAmountForFXRisk" ,
                    "OLD_AddOnAmountForInterestRateRisk" as "AddOnAmountForInterestRateRisk" ,
                    "OLD_AddOnAmountForOtherRisk" as "AddOnAmountForOtherRisk" ,
                    "OLD_AggregatedAddOnAmount" as "AggregatedAddOnAmount" ,
                    "OLD_Currency" as "Currency" ,
                    "OLD_ExposureAtDefault" as "ExposureAtDefault" ,
                    "OLD_MarginThresholdAmount" as "MarginThresholdAmount" ,
                    "OLD_MinimumTransferAmount" as "MinimumTransferAmount" ,
                    "OLD_MultiplierForAggregatedAddOnAmount" as "MultiplierForAggregatedAddOnAmount" ,
                    "OLD_NetHaircutValueOfNetCollateralHeld" as "NetHaircutValueOfNetCollateralHeld" ,
                    "OLD_NetIndependentCollateralAmount" as "NetIndependentCollateralAmount" ,
                    "OLD_NetInitialMarginAmount" as "NetInitialMarginAmount" ,
                    "OLD_NetMarketValue" as "NetMarketValue" ,
                    "OLD_NetVariationMarginAmount" as "NetVariationMarginAmount" ,
                    "OLD_NumberOfContractsInNettingSet" as "NumberOfContractsInNettingSet" ,
                    "OLD_PotentialFutureExposure" as "PotentialFutureExposure" ,
                    "OLD_ReplacementCosts" as "ReplacementCosts" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."MarginedNettingSet" as "OLD_MarginedNettingSet",
                                "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" as "OLD__ExchangeTradedNettingSet.FinancialInstrumentID",
                                "OLD"."_FinancialContractNettingSet.FinancialContractID" as "OLD__FinancialContractNettingSet.FinancialContractID",
                                "OLD"."_FinancialContractNettingSet.IDSystem" as "OLD__FinancialContractNettingSet.IDSystem",
                                "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                                "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                                "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."AddOnAmountForCommodityRisk" as "OLD_AddOnAmountForCommodityRisk",
                                "OLD"."AddOnAmountForCreditRisk" as "OLD_AddOnAmountForCreditRisk",
                                "OLD"."AddOnAmountForEquityRisk" as "OLD_AddOnAmountForEquityRisk",
                                "OLD"."AddOnAmountForFXRisk" as "OLD_AddOnAmountForFXRisk",
                                "OLD"."AddOnAmountForInterestRateRisk" as "OLD_AddOnAmountForInterestRateRisk",
                                "OLD"."AddOnAmountForOtherRisk" as "OLD_AddOnAmountForOtherRisk",
                                "OLD"."AggregatedAddOnAmount" as "OLD_AggregatedAddOnAmount",
                                "OLD"."Currency" as "OLD_Currency",
                                "OLD"."ExposureAtDefault" as "OLD_ExposureAtDefault",
                                "OLD"."MarginThresholdAmount" as "OLD_MarginThresholdAmount",
                                "OLD"."MinimumTransferAmount" as "OLD_MinimumTransferAmount",
                                "OLD"."MultiplierForAggregatedAddOnAmount" as "OLD_MultiplierForAggregatedAddOnAmount",
                                "OLD"."NetHaircutValueOfNetCollateralHeld" as "OLD_NetHaircutValueOfNetCollateralHeld",
                                "OLD"."NetIndependentCollateralAmount" as "OLD_NetIndependentCollateralAmount",
                                "OLD"."NetInitialMarginAmount" as "OLD_NetInitialMarginAmount",
                                "OLD"."NetMarketValue" as "OLD_NetMarketValue",
                                "OLD"."NetVariationMarginAmount" as "OLD_NetVariationMarginAmount",
                                "OLD"."NumberOfContractsInNettingSet" as "OLD_NumberOfContractsInNettingSet",
                                "OLD"."PotentialFutureExposure" as "OLD_PotentialFutureExposure",
                                "OLD"."ReplacementCosts" as "OLD_ReplacementCosts",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskNettingSetResults" as "OLD"
            on
                ifnull( "IN"."MarginedNettingSet", false) = "OLD"."MarginedNettingSet" and
                ifnull( "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '') = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                ifnull( "IN"."_FinancialContractNettingSet.FinancialContractID", '') = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                ifnull( "IN"."_FinancialContractNettingSet.IDSystem", '') = "OLD"."_FinancialContractNettingSet.IDSystem" and
                ifnull( "IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull( "IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                ifnull( "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_MarginedNettingSet" as "MarginedNettingSet",
            "OLD__ExchangeTradedNettingSet.FinancialInstrumentID" as "_ExchangeTradedNettingSet.FinancialInstrumentID",
            "OLD__FinancialContractNettingSet.FinancialContractID" as "_FinancialContractNettingSet.FinancialContractID",
            "OLD__FinancialContractNettingSet.IDSystem" as "_FinancialContractNettingSet.IDSystem",
            "OLD__ResultGroup.ResultDataProvider" as "_ResultGroup.ResultDataProvider",
            "OLD__ResultGroup.ResultGroupID" as "_ResultGroup.ResultGroupID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
            "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_AddOnAmountForCommodityRisk" as "AddOnAmountForCommodityRisk",
            "OLD_AddOnAmountForCreditRisk" as "AddOnAmountForCreditRisk",
            "OLD_AddOnAmountForEquityRisk" as "AddOnAmountForEquityRisk",
            "OLD_AddOnAmountForFXRisk" as "AddOnAmountForFXRisk",
            "OLD_AddOnAmountForInterestRateRisk" as "AddOnAmountForInterestRateRisk",
            "OLD_AddOnAmountForOtherRisk" as "AddOnAmountForOtherRisk",
            "OLD_AggregatedAddOnAmount" as "AggregatedAddOnAmount",
            "OLD_Currency" as "Currency",
            "OLD_ExposureAtDefault" as "ExposureAtDefault",
            "OLD_MarginThresholdAmount" as "MarginThresholdAmount",
            "OLD_MinimumTransferAmount" as "MinimumTransferAmount",
            "OLD_MultiplierForAggregatedAddOnAmount" as "MultiplierForAggregatedAddOnAmount",
            "OLD_NetHaircutValueOfNetCollateralHeld" as "NetHaircutValueOfNetCollateralHeld",
            "OLD_NetIndependentCollateralAmount" as "NetIndependentCollateralAmount",
            "OLD_NetInitialMarginAmount" as "NetInitialMarginAmount",
            "OLD_NetMarketValue" as "NetMarketValue",
            "OLD_NetVariationMarginAmount" as "NetVariationMarginAmount",
            "OLD_NumberOfContractsInNettingSet" as "NumberOfContractsInNettingSet",
            "OLD_PotentialFutureExposure" as "PotentialFutureExposure",
            "OLD_ReplacementCosts" as "ReplacementCosts",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."MarginedNettingSet",
                        "IN"."_ExchangeTradedNettingSet.FinancialInstrumentID",
                        "IN"."_FinancialContractNettingSet.FinancialContractID",
                        "IN"."_FinancialContractNettingSet.IDSystem",
                        "IN"."_ResultGroup.ResultDataProvider",
                        "IN"."_ResultGroup.ResultGroupID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."MarginedNettingSet" as "OLD_MarginedNettingSet",
                        "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" as "OLD__ExchangeTradedNettingSet.FinancialInstrumentID",
                        "OLD"."_FinancialContractNettingSet.FinancialContractID" as "OLD__FinancialContractNettingSet.FinancialContractID",
                        "OLD"."_FinancialContractNettingSet.IDSystem" as "OLD__FinancialContractNettingSet.IDSystem",
                        "OLD"."_ResultGroup.ResultDataProvider" as "OLD__ResultGroup.ResultDataProvider",
                        "OLD"."_ResultGroup.ResultGroupID" as "OLD__ResultGroup.ResultGroupID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID",
                        "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" as "OLD__SecuritiesAccountOfExchangeTradedNettingSet.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."AddOnAmountForCommodityRisk" as "OLD_AddOnAmountForCommodityRisk",
                        "OLD"."AddOnAmountForCreditRisk" as "OLD_AddOnAmountForCreditRisk",
                        "OLD"."AddOnAmountForEquityRisk" as "OLD_AddOnAmountForEquityRisk",
                        "OLD"."AddOnAmountForFXRisk" as "OLD_AddOnAmountForFXRisk",
                        "OLD"."AddOnAmountForInterestRateRisk" as "OLD_AddOnAmountForInterestRateRisk",
                        "OLD"."AddOnAmountForOtherRisk" as "OLD_AddOnAmountForOtherRisk",
                        "OLD"."AggregatedAddOnAmount" as "OLD_AggregatedAddOnAmount",
                        "OLD"."Currency" as "OLD_Currency",
                        "OLD"."ExposureAtDefault" as "OLD_ExposureAtDefault",
                        "OLD"."MarginThresholdAmount" as "OLD_MarginThresholdAmount",
                        "OLD"."MinimumTransferAmount" as "OLD_MinimumTransferAmount",
                        "OLD"."MultiplierForAggregatedAddOnAmount" as "OLD_MultiplierForAggregatedAddOnAmount",
                        "OLD"."NetHaircutValueOfNetCollateralHeld" as "OLD_NetHaircutValueOfNetCollateralHeld",
                        "OLD"."NetIndependentCollateralAmount" as "OLD_NetIndependentCollateralAmount",
                        "OLD"."NetInitialMarginAmount" as "OLD_NetInitialMarginAmount",
                        "OLD"."NetMarketValue" as "OLD_NetMarketValue",
                        "OLD"."NetVariationMarginAmount" as "OLD_NetVariationMarginAmount",
                        "OLD"."NumberOfContractsInNettingSet" as "OLD_NumberOfContractsInNettingSet",
                        "OLD"."PotentialFutureExposure" as "OLD_PotentialFutureExposure",
                        "OLD"."ReplacementCosts" as "OLD_ReplacementCosts",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::CounterpartyCreditRiskNettingSetResults" as "OLD"
            on
                ifnull("IN"."MarginedNettingSet", false) = "OLD"."MarginedNettingSet" and
                ifnull("IN"."_ExchangeTradedNettingSet.FinancialInstrumentID", '') = "OLD"."_ExchangeTradedNettingSet.FinancialInstrumentID" and
                ifnull("IN"."_FinancialContractNettingSet.FinancialContractID", '') = "OLD"."_FinancialContractNettingSet.FinancialContractID" and
                ifnull("IN"."_FinancialContractNettingSet.IDSystem", '') = "OLD"."_FinancialContractNettingSet.IDSystem" and
                ifnull("IN"."_ResultGroup.ResultDataProvider", '') = "OLD"."_ResultGroup.ResultDataProvider" and
                ifnull("IN"."_ResultGroup.ResultGroupID", '') = "OLD"."_ResultGroup.ResultGroupID" and
                ifnull("IN"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.FinancialContractID" and
                ifnull("IN"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem", '') = "OLD"."_SecuritiesAccountOfExchangeTradedNettingSet.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
