PROCEDURE "sap.fsdm.procedures::PrincipalExchangeScheduleErase" (IN ROW "sap.fsdm.tabletypes::PrincipalExchangeScheduleTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "PrincipalExchangeDate" is null and
            "RoleOfPayer" is null and
            "_SwapForSchedule.FinancialContractID" is null and
            "_SwapForSchedule.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::PrincipalExchangeSchedule"
        WHERE
        (            "PrincipalExchangeDate" ,
            "RoleOfPayer" ,
            "_SwapForSchedule.FinancialContractID" ,
            "_SwapForSchedule.IDSystem" 
        ) in
        (
            select                 "OLD"."PrincipalExchangeDate" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_SwapForSchedule.FinancialContractID" ,
                "OLD"."_SwapForSchedule.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::PrincipalExchangeSchedule" "OLD"
            on
            "IN"."PrincipalExchangeDate" = "OLD"."PrincipalExchangeDate" and
            "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
            "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
            "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::PrincipalExchangeSchedule_Historical"
        WHERE
        (
            "PrincipalExchangeDate" ,
            "RoleOfPayer" ,
            "_SwapForSchedule.FinancialContractID" ,
            "_SwapForSchedule.IDSystem" 
        ) in
        (
            select
                "OLD"."PrincipalExchangeDate" ,
                "OLD"."RoleOfPayer" ,
                "OLD"."_SwapForSchedule.FinancialContractID" ,
                "OLD"."_SwapForSchedule.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::PrincipalExchangeSchedule_Historical" "OLD"
            on
                "IN"."PrincipalExchangeDate" = "OLD"."PrincipalExchangeDate" and
                "IN"."RoleOfPayer" = "OLD"."RoleOfPayer" and
                "IN"."_SwapForSchedule.FinancialContractID" = "OLD"."_SwapForSchedule.FinancialContractID" and
                "IN"."_SwapForSchedule.IDSystem" = "OLD"."_SwapForSchedule.IDSystem" 
        );

END
