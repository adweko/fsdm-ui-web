view "sap.fsdm.SQLViews::ResultGroup_View" 
as select
      "ResultDataProvider" ,
      "ResultGroupID" ,
      "BusinessValidFrom" ,
      "BusinessValidTo" ,
      "SystemValidFrom" ,
      "SystemValidTo" ,
      "_OrganizationalUnit.IDSystem" ,
      "_OrganizationalUnit.OrganizationalUnitID" ,
      "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
      "ResultGroupCreationTimestamp" ,
      "ResultGroupDescription" ,
      "ResultGroupType" ,
      "SelectionBusinessDate" ,
      "SelectionSystemTimestamp" ,
      "Status" ,
      "SourceSystemID" ,
      "ChangeTimestampInSourceSystem" ,
      "ChangingUserInSourceSystem" ,
      "ChangingProcessType" ,
      "ChangingProcessID" 
  
from "sap.fsdm::ResultGroup"
with associations
(
	join "sap.fsdm.SQLViews::AssignedAssetDistribution_View" as "_AssignedAssetDistribution__ResultGroup"
	  on "ResultGroupID" = "_AssignedAssetDistribution__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_AssignedAssetDistribution__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::CashFlowStream_View" as "_CashFlowStream__ResultGroup"
	  on "ResultGroupID" = "_CashFlowStream__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_CashFlowStream__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::CommodityExposure_View" as "_CommodityExposure__ResultGroup"
	  on "ResultDataProvider" = "_CommodityExposure__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_CommodityExposure__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::CounterpartyCreditRiskCollateralResults_View" as "_CounterpartyCreditRiskCollateralResults__ResultGroup"
	  on "ResultGroupID" = "_CounterpartyCreditRiskCollateralResults__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_CounterpartyCreditRiskCollateralResults__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::CounterpartyCreditRiskHedingSetResults_View" as "_CounterpartyCreditRiskHedingSetResults__ResultGroup"
	  on "ResultGroupID" = "_CounterpartyCreditRiskHedingSetResults__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_CounterpartyCreditRiskHedingSetResults__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::CounterpartyCreditRiskNettingSetResults_View" as "_CounterpartyCreditRiskNettingSetResults__ResultGroup"
	  on "ResultGroupID" = "_CounterpartyCreditRiskNettingSetResults__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_CounterpartyCreditRiskNettingSetResults__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::CounterpartyCreditRiskSingleExposureResults_View" as "_CounterpartyCreditRiskSingleExposureResults__ResultGroup"
	  on "ResultGroupID" = "_CounterpartyCreditRiskSingleExposureResults__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_CounterpartyCreditRiskSingleExposureResults__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::CreditRiskExposure_View" as "_CreditRiskExposure__ResultGroup"
	  on "ResultGroupID" = "_CreditRiskExposure__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_CreditRiskExposure__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::DefaultRiskResultOfStandardizedApproachForMarketRisk_View" as "_DefaultRiskResultOfStandardizedApproachForMarketRisk__ResultGroup"
	  on "ResultGroupID" = "_DefaultRiskResultOfStandardizedApproachForMarketRisk__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_DefaultRiskResultOfStandardizedApproachForMarketRisk__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::ExpectedCreditLoss_View" as "_ExpectedCreditLoss__ResultGroup"
	  on "ResultDataProvider" = "_ExpectedCreditLoss__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_ExpectedCreditLoss__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::FXExposure_View" as "_FXExposure__ResultGroup"
	  on "ResultGroupID" = "_FXExposure__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_FXExposure__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::IndexExposure_View" as "_IndexExposure__ResultGroup"
	  on "ResultDataProvider" = "_IndexExposure__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_IndexExposure__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::InterestAndBalanceSchedule_View" as "_InterestAndBalanceSchedule__ResultGroup"
	  on "ResultGroupID" = "_InterestAndBalanceSchedule__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_InterestAndBalanceSchedule__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::KeyRateAnalysis_View" as "_KeyRateAnalysis__ResultGroup"
	  on "ResultDataProvider" = "_KeyRateAnalysis__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_KeyRateAnalysis__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::LiquidityCoverageRatio_View" as "_LiquidityCoverageRatio__ResultGroup"
	  on "ResultGroupID" = "_LiquidityCoverageRatio__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_LiquidityCoverageRatio__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::LiquidityGap_View" as "_LiquidityGap__ResultGroup"
	  on "ResultGroupID" = "_LiquidityGap__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_LiquidityGap__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::NetPresentValueAnalysis_View" as "_NetPresentValueAnalysis__ResultGroup"
	  on "ResultDataProvider" = "_NetPresentValueAnalysis__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_NetPresentValueAnalysis__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::NetStableFundingRatio_View" as "_NetStableFundingRatio__ResultGroup"
	  on "ResultDataProvider" = "_NetStableFundingRatio__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_NetStableFundingRatio__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::RepoAnalysis_View" as "_RepoAnalysis__ResultGroup"
	  on "ResultDataProvider" = "_RepoAnalysis__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_RepoAnalysis__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::RepricingGap_View" as "_RepricingGap__ResultGroup"
	  on "ResultGroupID" = "_RepricingGap__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_RepricingGap__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::ResidualRiskResultOfStandardizedApproachForMarketRisk_View" as "_ResidualRiskResultOfStandardizedApproachForMarketRisk__ResultGroup"
	  on "ResultDataProvider" = "_ResidualRiskResultOfStandardizedApproachForMarketRisk__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_ResidualRiskResultOfStandardizedApproachForMarketRisk__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::OrganizationalUnit_View" as "_OrganizationalUnit"
	  on "_OrganizationalUnit"."OrganizationalUnitID" = "_OrganizationalUnit.OrganizationalUnitID" AND 
	     "_OrganizationalUnit"."ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AND 
	     "_OrganizationalUnit"."IDSystem" = "_OrganizationalUnit.IDSystem"
	     ,
	join "sap.fsdm.SQLViews::SecuritizationPoolKeyDateEvaluation_View" as "_SecuritizationPoolKeyDateEvaluation__ResultGroup"
	  on "ResultDataProvider" = "_SecuritizationPoolKeyDateEvaluation__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_SecuritizationPoolKeyDateEvaluation__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::SecuritizationPoolPeriodEvaluation_View" as "_SecuritizationPoolPeriodEvaluation__ResultGroup"
	  on "ResultDataProvider" = "_SecuritizationPoolPeriodEvaluation__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_SecuritizationPoolPeriodEvaluation__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::SensitivityBasedResultOfStandardizedApproachForMarketRisk_View" as "_SensitivityBasedResultOfStandardizedApproachForMarketRisk__ResultGroup"
	  on "ResultDataProvider" = "_SensitivityBasedResultOfStandardizedApproachForMarketRisk__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_SensitivityBasedResultOfStandardizedApproachForMarketRisk__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::SensitivityGap_View" as "_SensitivityGap__ResultGroup"
	  on "ResultDataProvider" = "_SensitivityGap__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_SensitivityGap__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::StockFlowAnalysis_View" as "_StockFlowAnalysis__ResultGroup"
	  on "ResultDataProvider" = "_StockFlowAnalysis__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_StockFlowAnalysis__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::TARGETTransactionReportResult_View" as "_TARGETTransactionReportResult__ResultGroup"
	  on "ResultGroupID" = "_TARGETTransactionReportResult__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_TARGETTransactionReportResult__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::ValueAtRiskBacktestingInterimResult_View" as "_ValueAtRiskBacktestingInterimResult__ResultGroup"
	  on "ResultDataProvider" = "_ValueAtRiskBacktestingInterimResult__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_ValueAtRiskBacktestingInterimResult__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::ValueAtRiskBacktestingResult_View" as "_ValueAtRiskBacktestingResult__ResultGroup"
	  on "ResultDataProvider" = "_ValueAtRiskBacktestingResult__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_ValueAtRiskBacktestingResult__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::ValueAtRiskInterimResult_View" as "_ValueAtRiskInterimResult__ResultGroup"
	  on "ResultGroupID" = "_ValueAtRiskInterimResult__ResultGroup"."_ResultGroup.ResultGroupID" AND 
	     "ResultDataProvider" = "_ValueAtRiskInterimResult__ResultGroup"."_ResultGroup.ResultDataProvider"
	     ,
	join "sap.fsdm.SQLViews::ValueAtRiskResult_View" as "_ValueAtRiskResult__ResultGroup"
	  on "ResultDataProvider" = "_ValueAtRiskResult__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_ValueAtRiskResult__ResultGroup"."_ResultGroup.ResultGroupID"
	     ,
	join "sap.fsdm.SQLViews::VolatilityExposure_View" as "_VolatilityExposure__ResultGroup"
	  on "ResultDataProvider" = "_VolatilityExposure__ResultGroup"."_ResultGroup.ResultDataProvider" AND 
	     "ResultGroupID" = "_VolatilityExposure__ResultGroup"."_ResultGroup.ResultGroupID"
	     
);