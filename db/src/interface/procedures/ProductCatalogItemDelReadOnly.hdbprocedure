PROCEDURE "sap.fsdm.procedures::ProductCatalogItemDelReadOnly" (IN ROW "sap.fsdm.tabletypes::ProductCatalogItemTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::ProductCatalogItemTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::ProductCatalogItemTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ProductCatalogItem=' || TO_VARCHAR("ProductCatalogItem") || ' ' ||
                '_ProductCatalog.CatalogID=' || TO_VARCHAR("_ProductCatalog.CatalogID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ProductCatalogItem",
                        "IN"."_ProductCatalog.CatalogID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ProductCatalogItem",
                        "IN"."_ProductCatalog.CatalogID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ProductCatalogItem",
                        "_ProductCatalog.CatalogID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ProductCatalogItem",
                                    "IN"."_ProductCatalog.CatalogID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ProductCatalogItem",
                                    "IN"."_ProductCatalog.CatalogID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ProductCatalogItem" is null and
            "_ProductCatalog.CatalogID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "ProductCatalogItem",
            "_ProductCatalog.CatalogID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::ProductCatalogItem" WHERE
            (
            "ProductCatalogItem" ,
            "_ProductCatalog.CatalogID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."ProductCatalogItem",
            "OLD"."_ProductCatalog.CatalogID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::ProductCatalogItem" as "OLD"
        on
                              "IN"."ProductCatalogItem" = "OLD"."ProductCatalogItem" and
                              "IN"."_ProductCatalog.CatalogID" = "OLD"."_ProductCatalog.CatalogID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "ProductCatalogItem",
        "_ProductCatalog.CatalogID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "ProductCatalogItemCategory",
        "ProductCatalogItemDescription",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_ProductCatalogItem" as "ProductCatalogItem" ,
            "OLD__ProductCatalog.CatalogID" as "_ProductCatalog.CatalogID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_ProductCatalogItemCategory" as "ProductCatalogItemCategory" ,
            "OLD_ProductCatalogItemDescription" as "ProductCatalogItemDescription" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ProductCatalogItem",
                        "OLD"."_ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."ProductCatalogItem" AS "OLD_ProductCatalogItem" ,
                "OLD"."_ProductCatalog.CatalogID" AS "OLD__ProductCatalog.CatalogID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ProductCatalogItemCategory" AS "OLD_ProductCatalogItemCategory" ,
                "OLD"."ProductCatalogItemDescription" AS "OLD_ProductCatalogItemDescription" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ProductCatalogItem" as "OLD"
            on
                                      "IN"."ProductCatalogItem" = "OLD"."ProductCatalogItem" and
                                      "IN"."_ProductCatalog.CatalogID" = "OLD"."_ProductCatalog.CatalogID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_ProductCatalogItem" as "ProductCatalogItem",
            "OLD__ProductCatalog.CatalogID" as "_ProductCatalog.CatalogID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_ProductCatalogItemCategory" as "ProductCatalogItemCategory",
            "OLD_ProductCatalogItemDescription" as "ProductCatalogItemDescription",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ProductCatalogItem",
                        "OLD"."_ProductCatalog.CatalogID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ProductCatalogItem" AS "OLD_ProductCatalogItem" ,
                "OLD"."_ProductCatalog.CatalogID" AS "OLD__ProductCatalog.CatalogID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."ProductCatalogItemCategory" AS "OLD_ProductCatalogItemCategory" ,
                "OLD"."ProductCatalogItemDescription" AS "OLD_ProductCatalogItemDescription" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::ProductCatalogItem" as "OLD"
            on
               "IN"."ProductCatalogItem" = "OLD"."ProductCatalogItem" and
               "IN"."_ProductCatalog.CatalogID" = "OLD"."_ProductCatalog.CatalogID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
