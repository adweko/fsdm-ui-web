namespace "sap"."fsdm";

using "sap"."fsdm"::"AccountingSystem";
using "sap"."fsdm"::"BusinessPartner";
using "sap"."fsdm"::"OrganizationalUnit";
using "sap"."fsdm"::"CompanyCode";
using "sap"."fsdm"::"FinancialContract";
using "sap"."fsdm"::"GLAccount";
using "sap"."fsdm"::"PlanBudgetForecast";
using "sap"."fsdm"::"ProductCatalogItem";
using "sap"."fsdm"::"ProductClass";
using "sap"."fsdm"::"SubledgerAccount";

entity "SubledgerAccountbalance" {
    key "AccountingBalanceType"          : String(10);
    key "FiscalYear"                     : String(10)                                             default '';
    key "PostingDate"                    : LocalDate;
    key "TransactionCurrency"            : String(3)                                              default '';
    key "_AccountingSystem"              : association to AccountingSystem { AccountingSystemID } not null;
    key "_BusinessPartner"               : association to BusinessPartner { BusinessPartnerID }   not null;
    key "_BusinessSegment"               : association to OrganizationalUnit {
                                                                               IDSystem,
                                                                               OrganizationalUnitID,
                                                                               ASSOC_OrganizationHostingOrganizationalUnit
                                                                             }                    not null;
    key "_BusinessSegmentAtCounterparty" : association to OrganizationalUnit {
                                                                               IDSystem,
                                                                               OrganizationalUnitID,
                                                                               ASSOC_OrganizationHostingOrganizationalUnit
                                                                             }                    not null;
    key "_CompanyCode"                   : association to CompanyCode {
                                                                        CompanyCode,
                                                                        ASSOC_Company
                                                                      }                           not null;
    key "_FinancialContract"             : association to FinancialContract {
                                                                              FinancialContractID,
                                                                              IDSystem
                                                                            }                     not null;
    key "_GLAccount"                     : association to GLAccount {
                                                                      GLAccount,
                                                                      _ChartOfAccounts
                                                                    }                             not null;
    key "_PlanBudgetForecast"            : association to PlanBudgetForecast {
                                                                               ID,
                                                                               VersionID
                                                                             }                    not null;
    key "_ProductCatalogItem"            : association to ProductCatalogItem {
                                                                               ProductCatalogItem,
                                                                               _ProductCatalog
                                                                             }                    not null;
    /**
    *@Deprecated
    *@Reason: Due to deprecation of the Product Class entity
    *@ReplacedBy: No known use case
    */
    key "_ProductClass"                  : association to ProductClass {
                                                                         ProductClass,
                                                                         ProductClassificationSystem
                                                                       }                          not null;
    key "_ProfitCenter"                  : association to OrganizationalUnit {
                                                                               IDSystem,
                                                                               OrganizationalUnitID,
                                                                               ASSOC_OrganizationHostingOrganizationalUnit
                                                                             }                    not null;
    key "_ProftCenterAtCouterparty"      : association to OrganizationalUnit {
                                                                               IDSystem,
                                                                               OrganizationalUnitID,
                                                                               ASSOC_OrganizationHostingOrganizationalUnit
                                                                             }                    not null;
    key "_SubledgerAccount"              : association to SubledgerAccount { SubledgerAccount }   not null;
    key "BusinessValidFrom"              : LocalDate                                              default '0001-01-01';
    key "BusinessValidTo"                : LocalDate                                              default '9999-12-31';
        "SystemValidFrom"                : UTCTimestamp                                           default timestamp'0001-01-01 00:00:00.0000000' not null;
        "SystemValidTo"                  : UTCTimestamp                                           default timestamp'9999-12-31 23:59:59.9999999' not null;
        "AmountInFunctionalCurrency"     : Decimal(34, 6);
        "AmountInGroupCurrency"          : Decimal(34, 6);
        "AmountInHardCurrency"           : Decimal(34, 6);
        "AmountInIndexCurrency"          : Decimal(34, 6);
        "AmountInLocalCurrency"          : Decimal(34, 6);
        "AmountInTransactionCurrency"    : Decimal(34, 6);
        "FiscalPeriodEnd"                : String(10);
        "FiscalPeriodStart"              : String(10);
        "FunctionalCurrency"             : String(3);
        "GroupCurrency"                  : String(3);
        "HardCurrency"                   : String(3);
        "IndexCurrency"                  : String(3);
        "LocalCurrency"                  : String(3);
        "SourceSystemID"                 : String(128);
        "ChangeTimestampInSourceSystem"  : UTCTimestamp;
        "ChangingUserInSourceSystem"     : String(128);
        "ChangingProcessType"            : String(40);
        "ChangingProcessID"              : String(128);
}
technical configuration {
    column store;
};

entity "SubledgerAccountbalance_Historical" {
    "AccountingBalanceType"          : String(10)                                             not null;
    "FiscalYear"                     : String(10)                                             default '' not null;
    "PostingDate"                    : LocalDate                                              not null;
    "TransactionCurrency"            : String(3)                                              default '' not null;
    "_AccountingSystem"              : association to AccountingSystem { AccountingSystemID } not null;
    "_BusinessPartner"               : association to BusinessPartner { BusinessPartnerID }   not null;
    "_BusinessSegment"               : association to OrganizationalUnit {
                                                                           IDSystem,
                                                                           OrganizationalUnitID,
                                                                           ASSOC_OrganizationHostingOrganizationalUnit
                                                                         }                    not null;
    "_BusinessSegmentAtCounterparty" : association to OrganizationalUnit {
                                                                           IDSystem,
                                                                           OrganizationalUnitID,
                                                                           ASSOC_OrganizationHostingOrganizationalUnit
                                                                         }                    not null;
    "_CompanyCode"                   : association to CompanyCode {
                                                                    CompanyCode,
                                                                    ASSOC_Company
                                                                  }                           not null;
    "_FinancialContract"             : association to FinancialContract {
                                                                          FinancialContractID,
                                                                          IDSystem
                                                                        }                     not null;
    "_GLAccount"                     : association to GLAccount {
                                                                  GLAccount,
                                                                  _ChartOfAccounts
                                                                }                             not null;
    "_PlanBudgetForecast"            : association to PlanBudgetForecast {
                                                                           ID,
                                                                           VersionID
                                                                         }                    not null;
    "_ProductCatalogItem"            : association to ProductCatalogItem {
                                                                           ProductCatalogItem,
                                                                           _ProductCatalog
                                                                         }                    not null;
    /**
    *@Deprecated
    *@Reason: Due to deprecation of the Product Class entity
    *@ReplacedBy: No known use case
    */
    "_ProductClass"                  : association to ProductClass {
                                                                     ProductClass,
                                                                     ProductClassificationSystem
                                                                   }                          not null;
    "_ProfitCenter"                  : association to OrganizationalUnit {
                                                                           IDSystem,
                                                                           OrganizationalUnitID,
                                                                           ASSOC_OrganizationHostingOrganizationalUnit
                                                                         }                    not null;
    "_ProftCenterAtCouterparty"      : association to OrganizationalUnit {
                                                                           IDSystem,
                                                                           OrganizationalUnitID,
                                                                           ASSOC_OrganizationHostingOrganizationalUnit
                                                                         }                    not null;
    "_SubledgerAccount"              : association to SubledgerAccount { SubledgerAccount }   not null;
    "BusinessValidFrom"              : LocalDate                                              default '0001-01-01' not null;
    "BusinessValidTo"                : LocalDate                                              default '9999-12-31' not null;
    "SystemValidFrom"                : UTCTimestamp                                           default timestamp'0001-01-01 00:00:00.0000000' not null;
    "SystemValidTo"                  : UTCTimestamp                                           default timestamp'9999-12-31 23:59:59.9999999' not null;
    "AmountInFunctionalCurrency"     : Decimal(34, 6);
    "AmountInGroupCurrency"          : Decimal(34, 6);
    "AmountInHardCurrency"           : Decimal(34, 6);
    "AmountInIndexCurrency"          : Decimal(34, 6);
    "AmountInLocalCurrency"          : Decimal(34, 6);
    "AmountInTransactionCurrency"    : Decimal(34, 6);
    "FiscalPeriodEnd"                : String(10);
    "FiscalPeriodStart"              : String(10);
    "FunctionalCurrency"             : String(3);
    "GroupCurrency"                  : String(3);
    "HardCurrency"                   : String(3);
    "IndexCurrency"                  : String(3);
    "LocalCurrency"                  : String(3);
    "SourceSystemID"                 : String(128);
    "ChangeTimestampInSourceSystem"  : UTCTimestamp;
    "ChangingUserInSourceSystem"     : String(128);
    "ChangingProcessType"            : String(40);
    "ChangingProcessID"              : String(128);
}
technical configuration {
    column store;
};