PROCEDURE "sap.fsdm.procedures::SubledgerAccountbalanceEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::SubledgerAccountbalanceTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::SubledgerAccountbalanceTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::SubledgerAccountbalanceTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "AccountingBalanceType" is null and
            "FiscalYear" is null and
            "PostingDate" is null and
            "TransactionCurrency" is null and
            "_AccountingSystem.AccountingSystemID" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_BusinessSegment.IDSystem" is null and
            "_BusinessSegment.OrganizationalUnitID" is null and
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_BusinessSegmentAtCounterparty.IDSystem" is null and
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" is null and
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_CompanyCode.CompanyCode" is null and
            "_CompanyCode.ASSOC_Company.BusinessPartnerID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null and
            "_GLAccount.GLAccount" is null and
            "_GLAccount._ChartOfAccounts.ChartOfAccountId" is null and
            "_PlanBudgetForecast.ID" is null and
            "_PlanBudgetForecast.VersionID" is null and
            "_ProductCatalogItem.ProductCatalogItem" is null and
            "_ProductCatalogItem._ProductCatalog.CatalogID" is null and
            "_ProductClass.ProductClass" is null and
            "_ProductClass.ProductClassificationSystem" is null and
            "_ProfitCenter.IDSystem" is null and
            "_ProfitCenter.OrganizationalUnitID" is null and
            "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_ProftCenterAtCouterparty.IDSystem" is null and
            "_ProftCenterAtCouterparty.OrganizationalUnitID" is null and
            "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "_SubledgerAccount.SubledgerAccount" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "AccountingBalanceType" ,
                "FiscalYear" ,
                "PostingDate" ,
                "TransactionCurrency" ,
                "_AccountingSystem.AccountingSystemID" ,
                "_BusinessPartner.BusinessPartnerID" ,
                "_BusinessSegment.IDSystem" ,
                "_BusinessSegment.OrganizationalUnitID" ,
                "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "_BusinessSegmentAtCounterparty.IDSystem" ,
                "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "_CompanyCode.CompanyCode" ,
                "_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
                "_FinancialContract.FinancialContractID" ,
                "_FinancialContract.IDSystem" ,
                "_GLAccount.GLAccount" ,
                "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "_PlanBudgetForecast.ID" ,
                "_PlanBudgetForecast.VersionID" ,
                "_ProductCatalogItem.ProductCatalogItem" ,
                "_ProductCatalogItem._ProductCatalog.CatalogID" ,
                "_ProductClass.ProductClass" ,
                "_ProductClass.ProductClassificationSystem" ,
                "_ProfitCenter.IDSystem" ,
                "_ProfitCenter.OrganizationalUnitID" ,
                "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "_ProftCenterAtCouterparty.IDSystem" ,
                "_ProftCenterAtCouterparty.OrganizationalUnitID" ,
                "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "_SubledgerAccount.SubledgerAccount" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."AccountingBalanceType" ,
                "OLD"."FiscalYear" ,
                "OLD"."PostingDate" ,
                "OLD"."TransactionCurrency" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_BusinessSegment.IDSystem" ,
                "OLD"."_BusinessSegment.OrganizationalUnitID" ,
                "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_BusinessSegmentAtCounterparty.IDSystem" ,
                "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_CompanyCode.CompanyCode" ,
                "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_GLAccount.GLAccount" ,
                "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_PlanBudgetForecast.ID" ,
                "OLD"."_PlanBudgetForecast.VersionID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" ,
                "OLD"."_ProductClass.ProductClass" ,
                "OLD"."_ProductClass.ProductClassificationSystem" ,
                "OLD"."_ProfitCenter.IDSystem" ,
                "OLD"."_ProfitCenter.OrganizationalUnitID" ,
                "OLD"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_ProftCenterAtCouterparty.IDSystem" ,
                "OLD"."_ProftCenterAtCouterparty.OrganizationalUnitID" ,
                "OLD"."_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_SubledgerAccount.SubledgerAccount" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::SubledgerAccountbalance" "OLD"
            on
                "IN"."AccountingBalanceType" = "OLD"."AccountingBalanceType" and
                "IN"."FiscalYear" = "OLD"."FiscalYear" and
                "IN"."PostingDate" = "OLD"."PostingDate" and
                "IN"."TransactionCurrency" = "OLD"."TransactionCurrency" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_BusinessSegment.IDSystem" = "OLD"."_BusinessSegment.IDSystem" and
                "IN"."_BusinessSegment.OrganizationalUnitID" = "OLD"."_BusinessSegment.OrganizationalUnitID" and
                "IN"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_BusinessSegmentAtCounterparty.IDSystem" = "OLD"."_BusinessSegmentAtCounterparty.IDSystem" and
                "IN"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" = "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" and
                "IN"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_CompanyCode.CompanyCode" = "OLD"."_CompanyCode.CompanyCode" and
                "IN"."_CompanyCode.ASSOC_Company.BusinessPartnerID" = "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_GLAccount.GLAccount" = "OLD"."_GLAccount.GLAccount" and
                "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_PlanBudgetForecast.ID" = "OLD"."_PlanBudgetForecast.ID" and
                "IN"."_PlanBudgetForecast.VersionID" = "OLD"."_PlanBudgetForecast.VersionID" and
                "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" and
                "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
                "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" and
                "IN"."_ProfitCenter.IDSystem" = "OLD"."_ProfitCenter.IDSystem" and
                "IN"."_ProfitCenter.OrganizationalUnitID" = "OLD"."_ProfitCenter.OrganizationalUnitID" and
                "IN"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_ProftCenterAtCouterparty.IDSystem" = "OLD"."_ProftCenterAtCouterparty.IDSystem" and
                "IN"."_ProftCenterAtCouterparty.OrganizationalUnitID" = "OLD"."_ProftCenterAtCouterparty.OrganizationalUnitID" and
                "IN"."_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_SubledgerAccount.SubledgerAccount" = "OLD"."_SubledgerAccount.SubledgerAccount" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "AccountingBalanceType" ,
            "FiscalYear" ,
            "PostingDate" ,
            "TransactionCurrency" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_BusinessSegment.IDSystem" ,
            "_BusinessSegment.OrganizationalUnitID" ,
            "_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_BusinessSegmentAtCounterparty.IDSystem" ,
            "_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
            "_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_CompanyCode.CompanyCode" ,
            "_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "_GLAccount.GLAccount" ,
            "_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
            "_PlanBudgetForecast.ID" ,
            "_PlanBudgetForecast.VersionID" ,
            "_ProductCatalogItem.ProductCatalogItem" ,
            "_ProductCatalogItem._ProductCatalog.CatalogID" ,
            "_ProductClass.ProductClass" ,
            "_ProductClass.ProductClassificationSystem" ,
            "_ProfitCenter.IDSystem" ,
            "_ProfitCenter.OrganizationalUnitID" ,
            "_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_ProftCenterAtCouterparty.IDSystem" ,
            "_ProftCenterAtCouterparty.OrganizationalUnitID" ,
            "_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "_SubledgerAccount.SubledgerAccount" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."AccountingBalanceType" ,
                "OLD"."FiscalYear" ,
                "OLD"."PostingDate" ,
                "OLD"."TransactionCurrency" ,
                "OLD"."_AccountingSystem.AccountingSystemID" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_BusinessSegment.IDSystem" ,
                "OLD"."_BusinessSegment.OrganizationalUnitID" ,
                "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_BusinessSegmentAtCounterparty.IDSystem" ,
                "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" ,
                "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_CompanyCode.CompanyCode" ,
                "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" ,
                "OLD"."_GLAccount.GLAccount" ,
                "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" ,
                "OLD"."_PlanBudgetForecast.ID" ,
                "OLD"."_PlanBudgetForecast.VersionID" ,
                "OLD"."_ProductCatalogItem.ProductCatalogItem" ,
                "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" ,
                "OLD"."_ProductClass.ProductClass" ,
                "OLD"."_ProductClass.ProductClassificationSystem" ,
                "OLD"."_ProfitCenter.IDSystem" ,
                "OLD"."_ProfitCenter.OrganizationalUnitID" ,
                "OLD"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_ProftCenterAtCouterparty.IDSystem" ,
                "OLD"."_ProftCenterAtCouterparty.OrganizationalUnitID" ,
                "OLD"."_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."_SubledgerAccount.SubledgerAccount" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::SubledgerAccountbalance_Historical" "OLD"
            on
                "IN"."AccountingBalanceType" = "OLD"."AccountingBalanceType" and
                "IN"."FiscalYear" = "OLD"."FiscalYear" and
                "IN"."PostingDate" = "OLD"."PostingDate" and
                "IN"."TransactionCurrency" = "OLD"."TransactionCurrency" and
                "IN"."_AccountingSystem.AccountingSystemID" = "OLD"."_AccountingSystem.AccountingSystemID" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_BusinessSegment.IDSystem" = "OLD"."_BusinessSegment.IDSystem" and
                "IN"."_BusinessSegment.OrganizationalUnitID" = "OLD"."_BusinessSegment.OrganizationalUnitID" and
                "IN"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_BusinessSegment.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_BusinessSegmentAtCounterparty.IDSystem" = "OLD"."_BusinessSegmentAtCounterparty.IDSystem" and
                "IN"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" = "OLD"."_BusinessSegmentAtCounterparty.OrganizationalUnitID" and
                "IN"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_BusinessSegmentAtCounterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_CompanyCode.CompanyCode" = "OLD"."_CompanyCode.CompanyCode" and
                "IN"."_CompanyCode.ASSOC_Company.BusinessPartnerID" = "OLD"."_CompanyCode.ASSOC_Company.BusinessPartnerID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" and
                "IN"."_GLAccount.GLAccount" = "OLD"."_GLAccount.GLAccount" and
                "IN"."_GLAccount._ChartOfAccounts.ChartOfAccountId" = "OLD"."_GLAccount._ChartOfAccounts.ChartOfAccountId" and
                "IN"."_PlanBudgetForecast.ID" = "OLD"."_PlanBudgetForecast.ID" and
                "IN"."_PlanBudgetForecast.VersionID" = "OLD"."_PlanBudgetForecast.VersionID" and
                "IN"."_ProductCatalogItem.ProductCatalogItem" = "OLD"."_ProductCatalogItem.ProductCatalogItem" and
                "IN"."_ProductCatalogItem._ProductCatalog.CatalogID" = "OLD"."_ProductCatalogItem._ProductCatalog.CatalogID" and
                "IN"."_ProductClass.ProductClass" = "OLD"."_ProductClass.ProductClass" and
                "IN"."_ProductClass.ProductClassificationSystem" = "OLD"."_ProductClass.ProductClassificationSystem" and
                "IN"."_ProfitCenter.IDSystem" = "OLD"."_ProfitCenter.IDSystem" and
                "IN"."_ProfitCenter.OrganizationalUnitID" = "OLD"."_ProfitCenter.OrganizationalUnitID" and
                "IN"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_ProfitCenter.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_ProftCenterAtCouterparty.IDSystem" = "OLD"."_ProftCenterAtCouterparty.IDSystem" and
                "IN"."_ProftCenterAtCouterparty.OrganizationalUnitID" = "OLD"."_ProftCenterAtCouterparty.OrganizationalUnitID" and
                "IN"."_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."_ProftCenterAtCouterparty.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                "IN"."_SubledgerAccount.SubledgerAccount" = "OLD"."_SubledgerAccount.SubledgerAccount" 
        );

END
