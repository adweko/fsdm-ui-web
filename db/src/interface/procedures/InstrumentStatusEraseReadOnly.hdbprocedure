PROCEDURE "sap.fsdm.procedures::InstrumentStatusEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::InstrumentStatusTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::InstrumentStatusTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::InstrumentStatusTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "InstrumentStatusCategory" is null and
            "InstrumentStatusType" is null and
            "_Instrument.FinancialInstrumentID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "InstrumentStatusCategory" ,
                "InstrumentStatusType" ,
                "_Instrument.FinancialInstrumentID" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."InstrumentStatusCategory" ,
                "OLD"."InstrumentStatusType" ,
                "OLD"."_Instrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InstrumentStatus" "OLD"
            on
                "IN"."InstrumentStatusCategory" = "OLD"."InstrumentStatusCategory" and
                "IN"."InstrumentStatusType" = "OLD"."InstrumentStatusType" and
                "IN"."_Instrument.FinancialInstrumentID" = "OLD"."_Instrument.FinancialInstrumentID" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "InstrumentStatusCategory" ,
            "InstrumentStatusType" ,
            "_Instrument.FinancialInstrumentID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."InstrumentStatusCategory" ,
                "OLD"."InstrumentStatusType" ,
                "OLD"."_Instrument.FinancialInstrumentID" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::InstrumentStatus_Historical" "OLD"
            on
                "IN"."InstrumentStatusCategory" = "OLD"."InstrumentStatusCategory" and
                "IN"."InstrumentStatusType" = "OLD"."InstrumentStatusType" and
                "IN"."_Instrument.FinancialInstrumentID" = "OLD"."_Instrument.FinancialInstrumentID" 
        );

END
