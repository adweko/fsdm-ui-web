sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/routing/History",
    "sap/ui/commons/form/SimpleForm",
    "sap/ui/commons/Label",
    "sap/ui/commons/TextField",
    "sap/ui/core/Title",
    "sap/m/Dialog",
    "sap/m/Button"

], function (Controller, Filter, FilterOperator, JSONModel, History, SimpleForm, Label, TextField, Title, Dialog, Button) {
    "use strict";
    return Controller.extend("sap.ui.hana.controller.TableNewRow", {
        onInit: function () {
            var oRouter = this.getOwnerComponent().getRouter();
            oRouter.getRoute("tableNewRow").attachPatternMatched(this._onObjectMatched, this);
        },
        _onObjectMatched: function (oEvent) {


            var oModel = new JSONModel();
            var oModelKey = new JSONModel();


            this.getView().setModel(oModel, "main");
            this.getView().setModel(oModelKey, "keyValues");


            var tableName = oEvent.getParameter("arguments").parameter;
            oModel.loadData("/selectData.xsjs", {name: "tableColumns", data: tableName});
            oModelKey.loadData("/selectData.xsjs", {name: "keyValues", data: tableName});
            var oLabel = this.byId("pageHeader");
            oLabel.setTitle("New Record || Choose fields you want to add");


        },
        onSearch: function (oEvent) {

            var aFilters = [];
            var sQuery = oEvent.getSource().getValue();
            if (sQuery && sQuery.length > 0) {
                var filter = new Filter("COLUMN_NAME", FilterOperator.Contains, sQuery);
                aFilters.push(filter);
            }
            // update list binding
            var oList = this.byId("idList");
            var oBinding = oList.getBinding("items");

            oBinding.filter(aFilters, "Application");
        },
        onSelectionChange: function (oEvent) {

            var oList = oEvent.getSource();

            var oLabel = this.byId("idFilterLabel");
            var oLabel2 = this.byId("idFilterLabel2");
            var oInfoToolbar = this.byId("idInfoToolbar");


            var aContexts = oList.getSelectedContexts(true);
            // update UI
            var bSelected = (aContexts && aContexts.length > 0);
            var sText = (bSelected) ? aContexts.length + " selected" : null;
            (aContexts.map(function (oContext) {
                return oContext.getObject().COLUMN_NAME;
            }));
            var sText2 = aContexts.map(function (oContext) {
                return oContext.getObject().COLUMN_NAME;
            }).join(", ");
            oInfoToolbar.setVisible(bSelected);
            oLabel.setText(sText);
            oLabel2.setText(sText2);


        },
        onPressLoadData: function (oEvent) {
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            var chosenAttributes = this.byId("idFilterLabel2").getText();

            oRouter.navTo("tableElements", {parameter: chosenAttributes});
            this.byId("idFilterLabel2").setText("");
            this.byId("idFilterLabel").setText("");
        },
        onPressAddRow: function (oEvent) {

            var keyValues = this.getView().getModel("keyValues").getData();
            var chosenValues = this.byId("idFilterLabel2").getText().split(", ");

            var chosenValuesArray;
            if (!chosenValues[0] == "") {

                chosenValuesArray = [];
                chosenValues.forEach(function (v) {
                    chosenValuesArray.push({COLUMN_NAME: v});
                });
            }


            function intercept(arr1, arr2, prop) {
                var out = {};
                if (arr1) {
                    arr1.forEach(function (v) {
                        if (!out[v[prop]]) {
                            out[v[prop]] = v;
                        }
                    });
                }
                if (arr2) {
                    arr2.forEach(function (v) {
                        if (!out[v[prop]]) {
                            out[v[prop]] = v;
                        }
                    });
                }
                return Object.values(out);
            }

            var listForSimpleForm = intercept(keyValues, chosenValuesArray, "COLUMN_NAME");

            var simpleForm = new SimpleForm();

            // for (var i = 0; i < listForSimpleForm.length; i++) {
            //     simpleForm.addContent(new Label({text: listForSimpleForm[i].COLUMN_NAME}));


            //     if (listForSimpleForm[i].COLUMN_NAME === "BusinessValidTo") {
            //         simpleForm.addContent(new TextField({value: "9999-12-31", id: listForSimpleForm[i].COLUMN_NAME}));
            //     } else if (listForSimpleForm[i].COLUMN_NAME === "BusinessValidFrom") {
            //         simpleForm.addContent(new TextField({value: "2022-01-01", id: listForSimpleForm[i].COLUMN_NAME}));
            //     } else {
            //         simpleForm.addContent(new TextField({value: "", id: listForSimpleForm[i].COLUMN_NAME}));
            //     }
            // }
            listForSimpleForm.forEach(function(v){ 
                simpleForm.addContent(new Label({text: v.COLUMN_NAME}));

                if (v.COLUMN_NAME === "BusinessValidTo") {
                    simpleForm.addContent(new TextField({value: "9999-12-31", id: v.COLUMN_NAME}));
                } else if (v.COLUMN_NAME === "BusinessValidFrom") {
                    simpleForm.addContent(new TextField({value: "2022-01-01", id: v.COLUMN_NAME}));
                } else {
                    simpleForm.addContent(new TextField({value: "", id: v.COLUMN_NAME}));
                }
            });
            var oFirstDialog = new Dialog({
                width: "400px", // sap.ui.core.CSSSize
                height: "550px", // sap.ui.core.CSSSize
                title: "New Record", // string
                applyContentPadding: true, // boolean
                modal: true, // boolean
                beforeClose: function () {
                    oFirstDialog.destroy();
                },
                content: [simpleForm]
            });

            var oFirstDialogLocal = oFirstDialog;

            oFirstDialog.addButton(new Button({
                text: "Close",
                press: function () {
                    oFirstDialogLocal.close();
                    simpleForm.destroyContent();
                }
            }));
            oFirstDialog.addButton(new Button({
                text: "ADD",
                press: function () {
                    var fieldsAsArray = [];

                    function getValuesFromView() {
                        var values = [];
                         listForSimpleForm.forEach(function(v){ 
                            fieldsAsArray.push(v.COLUMN_NAME);
                            var value = sap.ui.getCore().byId(v.COLUMN_NAME).getValue();
                            values.push(value);
                        });
                        return values;
                    }

                    var payload = {};
                    payload.name = "insertNewField";
                    payload.values = getValuesFromView();
                    payload.fields = fieldsAsArray;
                    payload.tableName = sessionStorage.getItem("tableName");
                    var insertData = JSON.stringify(payload);

                    $.ajax({
                        type: "POST",
                        url: "/manipulateData.xsjs",
                        contentType: "application/json",
                        data: insertData,
                        dataType: "json",
                        crossDomain: true,
                        statusCode: {
                            200: function (data) {
                                sap.m.MessageToast.show(data.statusText, {my: "center top"});
                            }
                        }

                    });
                }
            }));

            oFirstDialog.open();

        },

        onNavBack: function () {

            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = this.getOwnerComponent().getRouter();
                oRouter.navTo("tableNames", {}, true);
            }
            this.byId("idFilterLabel2").setText("");
            this.byId("idFilterLabel").setText("");
        }

    });
});
