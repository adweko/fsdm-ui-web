PROCEDURE "sap.fsdm.procedures::StatementAboutContractModificationReadOnly" (IN ROW "sap.fsdm.tabletypes::StatementAboutContractModificationTT", OUT CURR_DEL "sap.fsdm.tabletypes::StatementAboutContractModificationTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::StatementAboutContractModificationTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'BookValueType=' || TO_VARCHAR("BookValueType") || ' ' ||
                'ModificationCategory=' || TO_VARCHAR("ModificationCategory") || ' ' ||
                'ModificationDate=' || TO_VARCHAR("ModificationDate") || ' ' ||
                '_AccountingSystem.AccountingSystemID=' || TO_VARCHAR("_AccountingSystem.AccountingSystemID") || ' ' ||
                '_FinancialContract.FinancialContractID=' || TO_VARCHAR("_FinancialContract.FinancialContractID") || ' ' ||
                '_FinancialContract.IDSystem=' || TO_VARCHAR("_FinancialContract.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."BookValueType",
                        "IN"."ModificationCategory",
                        "IN"."ModificationDate",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."BookValueType",
                        "IN"."ModificationCategory",
                        "IN"."ModificationDate",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "BookValueType",
                        "ModificationCategory",
                        "ModificationDate",
                        "_AccountingSystem.AccountingSystemID",
                        "_FinancialContract.FinancialContractID",
                        "_FinancialContract.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."BookValueType",
                                    "IN"."ModificationCategory",
                                    "IN"."ModificationDate",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."BookValueType",
                                    "IN"."ModificationCategory",
                                    "IN"."ModificationDate",
                                    "IN"."_AccountingSystem.AccountingSystemID",
                                    "IN"."_FinancialContract.FinancialContractID",
                                    "IN"."_FinancialContract.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();




     CURR_DEL = select 
        "BookValueType",
        "ModificationCategory",
        "ModificationDate",
        "_AccountingSystem.AccountingSystemID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo"
        from "sap.fsdm::StatementAboutContractModification" WHERE
        (            "BookValueType" ,
            "ModificationCategory" ,
            "ModificationDate" ,
            "_AccountingSystem.AccountingSystemID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 

        )        
in ( select
            "OLD"."BookValueType",
            "OLD"."ModificationCategory",
            "OLD"."ModificationDate",
            "OLD"."_AccountingSystem.AccountingSystemID",
            "OLD"."_FinancialContract.FinancialContractID",
            "OLD"."_FinancialContract.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
            from :row as "IN"
            inner join "sap.fsdm::StatementAboutContractModification" as "OLD"
            on
               ifnull( "IN"."BookValueType",'' ) = "OLD"."BookValueType" and
               ifnull( "IN"."ModificationCategory",'' ) = "OLD"."ModificationCategory" and
               ifnull( "IN"."ModificationDate",'0001-01-01' ) = "OLD"."ModificationDate" and
               ifnull( "IN"."_AccountingSystem.AccountingSystemID",'' ) = "OLD"."_AccountingSystem.AccountingSystemID" and
               ifnull( "IN"."_FinancialContract.FinancialContractID",'' ) = "OLD"."_FinancialContract.FinancialContractID" and
               ifnull( "IN"."_FinancialContract.IDSystem",'' ) = "OLD"."_FinancialContract.IDSystem" 
            where
               (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
               ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))           );


--Insert ALL the input data 

    CURR_INS = select 
        "BookValueType",
        "ModificationCategory",
        "ModificationDate",
        "_AccountingSystem.AccountingSystemID",
        "_FinancialContract.FinancialContractID",
        "_FinancialContract.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "_Reverses.BookValueType",
        "_Reverses.ModificationCategory",
        "_Reverses.ModificationDate",
        "_Reverses._AccountingSystem.AccountingSystemID",
        "_Reverses._FinancialContract.FinancialContractID",
        "_Reverses._FinancialContract.IDSystem",
        "BookValueModificationAmount",
        "BookValueModificationAmountCurrency",
        "Reversal",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
        from (
            (
                select
                    ifnull( "BookValueType", '' ) as "BookValueType",
                    ifnull( "ModificationCategory", '' ) as "ModificationCategory",
                    ifnull( "ModificationDate", '0001-01-01' ) as "ModificationDate",
                    ifnull( "_AccountingSystem.AccountingSystemID", '' ) as "_AccountingSystem.AccountingSystemID",
                    ifnull( "_FinancialContract.FinancialContractID", '' ) as "_FinancialContract.FinancialContractID",
                    ifnull( "_FinancialContract.IDSystem", '' ) as "_FinancialContract.IDSystem",
                    "BusinessValidFrom"  ,
                    "BusinessValidTo"  ,
                    "_Reverses.BookValueType"  ,
                    "_Reverses.ModificationCategory"  ,
                    "_Reverses.ModificationDate"  ,
                    "_Reverses._AccountingSystem.AccountingSystemID"  ,
                    "_Reverses._FinancialContract.FinancialContractID"  ,
                    "_Reverses._FinancialContract.IDSystem"  ,
                    "BookValueModificationAmount"  ,
                    "BookValueModificationAmountCurrency"  ,
                    "Reversal"  ,
                    "SourceSystemID"  ,
                    "ChangeTimestampInSourceSystem"  ,
                    "ChangingUserInSourceSystem"  ,
                    "ChangingProcessType"  ,
                    "ChangingProcessID"  
                from :row             )
            UNION ALL
            (
                select
                    "OLD_BookValueType" as "BookValueType" ,
                    "OLD_ModificationCategory" as "ModificationCategory" ,
                    "OLD_ModificationDate" as "ModificationDate" ,
                    "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID" ,
                    "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID" ,
                    "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem" ,
                    "NX_" as "BusinessValidFrom" ,
                    "BusinessValidFrom" as "BusinessValidTo" ,
                    "OLD__Reverses.BookValueType" as "_Reverses.BookValueType" ,
                    "OLD__Reverses.ModificationCategory" as "_Reverses.ModificationCategory" ,
                    "OLD__Reverses.ModificationDate" as "_Reverses.ModificationDate" ,
                    "OLD__Reverses._AccountingSystem.AccountingSystemID" as "_Reverses._AccountingSystem.AccountingSystemID" ,
                    "OLD__Reverses._FinancialContract.FinancialContractID" as "_Reverses._FinancialContract.FinancialContractID" ,
                    "OLD__Reverses._FinancialContract.IDSystem" as "_Reverses._FinancialContract.IDSystem" ,
                    "OLD_BookValueModificationAmount" as "BookValueModificationAmount" ,
                    "OLD_BookValueModificationAmountCurrency" as "BookValueModificationAmountCurrency" ,
                    "OLD_Reversal" as "Reversal" ,
                    "OLD_SourceSystemID" as "SourceSystemID" ,
                    "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
                    "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
                    "OLD_ChangingProcessType" as "ChangingProcessType" ,
                    "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."BookValueType",
                        "IN"."ModificationCategory",
                        "IN"."ModificationDate",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."BookValueType" as "OLD_BookValueType",
                                "OLD"."ModificationCategory" as "OLD_ModificationCategory",
                                "OLD"."ModificationDate" as "OLD_ModificationDate",
                                "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                                "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                                "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."_Reverses.BookValueType" as "OLD__Reverses.BookValueType",
                                "OLD"."_Reverses.ModificationCategory" as "OLD__Reverses.ModificationCategory",
                                "OLD"."_Reverses.ModificationDate" as "OLD__Reverses.ModificationDate",
                                "OLD"."_Reverses._AccountingSystem.AccountingSystemID" as "OLD__Reverses._AccountingSystem.AccountingSystemID",
                                "OLD"."_Reverses._FinancialContract.FinancialContractID" as "OLD__Reverses._FinancialContract.FinancialContractID",
                                "OLD"."_Reverses._FinancialContract.IDSystem" as "OLD__Reverses._FinancialContract.IDSystem",
                                "OLD"."BookValueModificationAmount" as "OLD_BookValueModificationAmount",
                                "OLD"."BookValueModificationAmountCurrency" as "OLD_BookValueModificationAmountCurrency",
                                "OLD"."Reversal" as "OLD_Reversal",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::StatementAboutContractModification" as "OLD"
            on
                ifnull( "IN"."BookValueType", '') = "OLD"."BookValueType" and
                ifnull( "IN"."ModificationCategory", '') = "OLD"."ModificationCategory" and
                ifnull( "IN"."ModificationDate", '0001-01-01') = "OLD"."ModificationDate" and
                ifnull( "IN"."_AccountingSystem.AccountingSystemID", '') = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull( "IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull( "IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                            )
        where "BusinessValidFrom" > "NX_" )
            UNION ALL
        (
            select
            "OLD_BookValueType" as "BookValueType",
            "OLD_ModificationCategory" as "ModificationCategory",
            "OLD_ModificationDate" as "ModificationDate",
            "OLD__AccountingSystem.AccountingSystemID" as "_AccountingSystem.AccountingSystemID",
            "OLD__FinancialContract.FinancialContractID" as "_FinancialContract.FinancialContractID",
            "OLD__FinancialContract.IDSystem" as "_FinancialContract.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD__Reverses.BookValueType" as "_Reverses.BookValueType",
            "OLD__Reverses.ModificationCategory" as "_Reverses.ModificationCategory",
            "OLD__Reverses.ModificationDate" as "_Reverses.ModificationDate",
            "OLD__Reverses._AccountingSystem.AccountingSystemID" as "_Reverses._AccountingSystem.AccountingSystemID",
            "OLD__Reverses._FinancialContract.FinancialContractID" as "_Reverses._FinancialContract.FinancialContractID",
            "OLD__Reverses._FinancialContract.IDSystem" as "_Reverses._FinancialContract.IDSystem",
            "OLD_BookValueModificationAmount" as "BookValueModificationAmount",
            "OLD_BookValueModificationAmountCurrency" as "BookValueModificationAmountCurrency",
            "OLD_Reversal" as "Reversal",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
            from
            (
                select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."BookValueType",
                        "IN"."ModificationCategory",
                        "IN"."ModificationDate",
                        "IN"."_AccountingSystem.AccountingSystemID",
                        "IN"."_FinancialContract.FinancialContractID",
                        "IN"."_FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."BookValueType" as "OLD_BookValueType",
                        "OLD"."ModificationCategory" as "OLD_ModificationCategory",
                        "OLD"."ModificationDate" as "OLD_ModificationDate",
                        "OLD"."_AccountingSystem.AccountingSystemID" as "OLD__AccountingSystem.AccountingSystemID",
                        "OLD"."_FinancialContract.FinancialContractID" as "OLD__FinancialContract.FinancialContractID",
                        "OLD"."_FinancialContract.IDSystem" as "OLD__FinancialContract.IDSystem",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."_Reverses.BookValueType" as "OLD__Reverses.BookValueType",
                        "OLD"."_Reverses.ModificationCategory" as "OLD__Reverses.ModificationCategory",
                        "OLD"."_Reverses.ModificationDate" as "OLD__Reverses.ModificationDate",
                        "OLD"."_Reverses._AccountingSystem.AccountingSystemID" as "OLD__Reverses._AccountingSystem.AccountingSystemID",
                        "OLD"."_Reverses._FinancialContract.FinancialContractID" as "OLD__Reverses._FinancialContract.FinancialContractID",
                        "OLD"."_Reverses._FinancialContract.IDSystem" as "OLD__Reverses._FinancialContract.IDSystem",
                        "OLD"."BookValueModificationAmount" as "OLD_BookValueModificationAmount",
                        "OLD"."BookValueModificationAmountCurrency" as "OLD_BookValueModificationAmountCurrency",
                        "OLD"."Reversal" as "OLD_Reversal",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::StatementAboutContractModification" as "OLD"
            on
                ifnull("IN"."BookValueType", '') = "OLD"."BookValueType" and
                ifnull("IN"."ModificationCategory", '') = "OLD"."ModificationCategory" and
                ifnull("IN"."ModificationDate", '0001-01-01') = "OLD"."ModificationDate" and
                ifnull("IN"."_AccountingSystem.AccountingSystemID", '') = "OLD"."_AccountingSystem.AccountingSystemID" and
                ifnull("IN"."_FinancialContract.FinancialContractID", '') = "OLD"."_FinancialContract.FinancialContractID" and
                ifnull("IN"."_FinancialContract.IDSystem", '') = "OLD"."_FinancialContract.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));



END
