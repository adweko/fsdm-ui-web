PROCEDURE "sap.fsdm.procedures::FinancialCovenantEraseReadOnly" (IN ROW "sap.fsdm.tabletypes::FinancialCovenantTT_Erase", OUT CURR_DEL "sap.fsdm.tabletypes::FinancialCovenantTT_Del" , OUT HIST_DEL "sap.fsdm.tabletypes::FinancialCovenantTT_Del" )
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "ASSOC_FinancialContract.FinancialContractID" is null and
            "ASSOC_FinancialContract.IDSystem" is null and
            "_BusinessPartner.BusinessPartnerID" is null and
            "_ContingentConvertibleInstrument.FinancialInstrumentID" is null and
            "_ContractBundle.ContractBundleID" is null and
            "_PhysicalAsset.PhysicalAssetID" is null and
            "_TrancheInSyndication.TrancheSequenceNumber" is null and
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" is null and
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        CURR_DEL =
            select
                "SequenceNumber" ,
                "ASSOC_FinancialContract.FinancialContractID" ,
                "ASSOC_FinancialContract.IDSystem" ,
                "_BusinessPartner.BusinessPartnerID" ,
                "_ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "_ContractBundle.ContractBundleID" ,
                "_PhysicalAsset.PhysicalAssetID" ,
                "_TrancheInSyndication.TrancheSequenceNumber" ,
                "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "BusinessValidFrom" ,
                "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialCovenant" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" and
                "IN"."_ContractBundle.ContractBundleID" = "OLD"."_ContractBundle.ContractBundleID" and
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" 
        );

        --delete data from history table
        HIST_DEL =
            select
            "SequenceNumber" ,
            "ASSOC_FinancialContract.FinancialContractID" ,
            "ASSOC_FinancialContract.IDSystem" ,
            "_BusinessPartner.BusinessPartnerID" ,
            "_ContingentConvertibleInstrument.FinancialInstrumentID" ,
            "_ContractBundle.ContractBundleID" ,
            "_PhysicalAsset.PhysicalAssetID" ,
            "_TrancheInSyndication.TrancheSequenceNumber" ,
            "_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
            "_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
        from
        (
            select
                "OLD"."SequenceNumber" ,
                "OLD"."ASSOC_FinancialContract.FinancialContractID" ,
                "OLD"."ASSOC_FinancialContract.IDSystem" ,
                "OLD"."_BusinessPartner.BusinessPartnerID" ,
                "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" ,
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."_TrancheInSyndication.TrancheSequenceNumber" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" ,
                "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" ,
                "OLD"."BusinessValidTo" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialCovenant_Historical" "OLD"
            on
                "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                "IN"."ASSOC_FinancialContract.FinancialContractID" = "OLD"."ASSOC_FinancialContract.FinancialContractID" and
                "IN"."ASSOC_FinancialContract.IDSystem" = "OLD"."ASSOC_FinancialContract.IDSystem" and
                "IN"."_BusinessPartner.BusinessPartnerID" = "OLD"."_BusinessPartner.BusinessPartnerID" and
                "IN"."_ContingentConvertibleInstrument.FinancialInstrumentID" = "OLD"."_ContingentConvertibleInstrument.FinancialInstrumentID" and
                "IN"."_ContractBundle.ContractBundleID" = "OLD"."_ContractBundle.ContractBundleID" and
                "IN"."_PhysicalAsset.PhysicalAssetID" = "OLD"."_PhysicalAsset.PhysicalAssetID" and
                "IN"."_TrancheInSyndication.TrancheSequenceNumber" = "OLD"."_TrancheInSyndication.TrancheSequenceNumber" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" = "OLD"."_TrancheInSyndication._SyndicationAgreement.FinancialContractID" and
                "IN"."_TrancheInSyndication._SyndicationAgreement.IDSystem" = "OLD"."_TrancheInSyndication._SyndicationAgreement.IDSystem" 
        );

END
