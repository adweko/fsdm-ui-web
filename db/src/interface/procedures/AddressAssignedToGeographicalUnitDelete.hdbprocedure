PROCEDURE "sap.fsdm.procedures::AddressAssignedToGeographicalUnitDelete" (IN ROW "sap.fsdm.tabletypes::AddressAssignedToGeographicalUnitTT_Del")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'ASSOC_Address.AddressType=' || TO_VARCHAR("ASSOC_Address.AddressType") || ' ' ||
                'ASSOC_Address.SequenceNumber=' || TO_VARCHAR("ASSOC_Address.SequenceNumber") || ' ' ||
                'ASSOC_Address.ASSOC_BankingChannel.BankingChannelID=' || TO_VARCHAR("ASSOC_Address.ASSOC_BankingChannel.BankingChannelID") || ' ' ||
                'ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID=' || TO_VARCHAR("ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID") || ' ' ||
                'ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem=' || TO_VARCHAR("ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem") || ' ' ||
                'ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID=' || TO_VARCHAR("ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID") || ' ' ||
                'ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID=' || TO_VARCHAR("ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID") || ' ' ||
                'ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID") || ' ' ||
                'ASSOC_Address._Claim.ID=' || TO_VARCHAR("ASSOC_Address._Claim.ID") || ' ' ||
                'ASSOC_Address._Claim.IDSystem=' || TO_VARCHAR("ASSOC_Address._Claim.IDSystem") || ' ' ||
                'ASSOC_GeographicalUnit.GeographicalStructureID=' || TO_VARCHAR("ASSOC_GeographicalUnit.GeographicalStructureID") || ' ' ||
                'ASSOC_GeographicalUnit.GeographicalUnitID=' || TO_VARCHAR("ASSOC_GeographicalUnit.GeographicalUnitID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."ASSOC_Address.AddressType",
                        "IN"."ASSOC_Address.SequenceNumber",
                        "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                        "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                        "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                        "IN"."ASSOC_Address._Claim.ID",
                        "IN"."ASSOC_Address._Claim.IDSystem",
                        "IN"."ASSOC_GeographicalUnit.GeographicalStructureID",
                        "IN"."ASSOC_GeographicalUnit.GeographicalUnitID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."ASSOC_Address.AddressType",
                        "IN"."ASSOC_Address.SequenceNumber",
                        "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                        "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                        "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                        "IN"."ASSOC_Address._Claim.ID",
                        "IN"."ASSOC_Address._Claim.IDSystem",
                        "IN"."ASSOC_GeographicalUnit.GeographicalStructureID",
                        "IN"."ASSOC_GeographicalUnit.GeographicalUnitID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "ASSOC_Address.AddressType",
                        "ASSOC_Address.SequenceNumber",
                        "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                        "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                        "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                        "ASSOC_Address._Claim.ID",
                        "ASSOC_Address._Claim.IDSystem",
                        "ASSOC_GeographicalUnit.GeographicalStructureID",
                        "ASSOC_GeographicalUnit.GeographicalUnitID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."ASSOC_Address.AddressType",
                                    "IN"."ASSOC_Address.SequenceNumber",
                                    "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                                    "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                                    "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                                    "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                                    "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                                    "IN"."ASSOC_Address._Claim.ID",
                                    "IN"."ASSOC_Address._Claim.IDSystem",
                                    "IN"."ASSOC_GeographicalUnit.GeographicalStructureID",
                                    "IN"."ASSOC_GeographicalUnit.GeographicalUnitID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."ASSOC_Address.AddressType",
                                    "IN"."ASSOC_Address.SequenceNumber",
                                    "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                                    "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                                    "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                                    "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                                    "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                                    "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                                    "IN"."ASSOC_Address._Claim.ID",
                                    "IN"."ASSOC_Address._Claim.IDSystem",
                                    "IN"."ASSOC_GeographicalUnit.GeographicalStructureID",
                                    "IN"."ASSOC_GeographicalUnit.GeographicalUnitID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "ASSOC_Address.AddressType" is null and
            "ASSOC_Address.SequenceNumber" is null and
            "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" is null and
            "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" is null and
            "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" is null and
            "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" is null and
            "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" is null and
            "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" is null and
            "ASSOC_Address._Claim.ID" is null and
            "ASSOC_Address._Claim.IDSystem" is null and
            "ASSOC_GeographicalUnit.GeographicalStructureID" is null and
            "ASSOC_GeographicalUnit.GeographicalUnitID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    --Insert chunked versions of object
    insert into "sap.fsdm::AddressAssignedToGeographicalUnit" (
        "ASSOC_Address.AddressType",
        "ASSOC_Address.SequenceNumber",
        "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
        "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
        "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
        "ASSOC_Address._Claim.ID",
        "ASSOC_Address._Claim.IDSystem",
        "ASSOC_GeographicalUnit.GeographicalStructureID",
        "ASSOC_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ASSOC_Address.AddressType" as "ASSOC_Address.AddressType" ,
            "OLD_ASSOC_Address.SequenceNumber" as "ASSOC_Address.SequenceNumber" ,
            "OLD_ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" as "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
            "OLD_ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" as "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
            "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
            "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
            "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
            "OLD_ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
            "OLD_ASSOC_Address._Claim.ID" as "ASSOC_Address._Claim.ID" ,
            "OLD_ASSOC_Address._Claim.IDSystem" as "ASSOC_Address._Claim.IDSystem" ,
            "OLD_ASSOC_GeographicalUnit.GeographicalStructureID" as "ASSOC_GeographicalUnit.GeographicalStructureID" ,
            "OLD_ASSOC_GeographicalUnit.GeographicalUnitID" as "ASSOC_GeographicalUnit.GeographicalUnitID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."ASSOC_Address.AddressType",
                        "OLD"."ASSOC_Address.SequenceNumber",
                        "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                        "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                        "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."ASSOC_Address._Claim.ID",
                        "OLD"."ASSOC_Address._Claim.IDSystem",
                        "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID",
                        "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                "OLD"."ASSOC_Address.AddressType" AS "OLD_ASSOC_Address.AddressType" ,
                "OLD"."ASSOC_Address.SequenceNumber" AS "OLD_ASSOC_Address.SequenceNumber" ,
                "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" AS "OLD_ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD_ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."ASSOC_Address._Claim.ID" AS "OLD_ASSOC_Address._Claim.ID" ,
                "OLD"."ASSOC_Address._Claim.IDSystem" AS "OLD_ASSOC_Address._Claim.IDSystem" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" AS "OLD_ASSOC_GeographicalUnit.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" AS "OLD_ASSOC_GeographicalUnit.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AddressAssignedToGeographicalUnit" as "OLD"
            on
                      "IN"."ASSOC_Address.AddressType" = "OLD"."ASSOC_Address.AddressType" and
                      "IN"."ASSOC_Address.SequenceNumber" = "OLD"."ASSOC_Address.SequenceNumber" and
                      "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" and
                      "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" and
                      "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" and
                      "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                      "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                      "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" and
                      "IN"."ASSOC_Address._Claim.ID" = "OLD"."ASSOC_Address._Claim.ID" and
                      "IN"."ASSOC_Address._Claim.IDSystem" = "OLD"."ASSOC_Address._Claim.IDSystem" and
                      "IN"."ASSOC_GeographicalUnit.GeographicalStructureID" = "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" and
                      "IN"."ASSOC_GeographicalUnit.GeographicalUnitID" = "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::AddressAssignedToGeographicalUnit" (
        "ASSOC_Address.AddressType",
        "ASSOC_Address.SequenceNumber",
        "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
        "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
        "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
        "ASSOC_Address._Claim.ID",
        "ASSOC_Address._Claim.IDSystem",
        "ASSOC_GeographicalUnit.GeographicalStructureID",
        "ASSOC_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_ASSOC_Address.AddressType" as "ASSOC_Address.AddressType",
            "OLD_ASSOC_Address.SequenceNumber" as "ASSOC_Address.SequenceNumber",
            "OLD_ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" as "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
            "OLD_ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" as "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" as "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
            "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" as "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" as "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD_ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" as "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD_ASSOC_Address._Claim.ID" as "ASSOC_Address._Claim.ID",
            "OLD_ASSOC_Address._Claim.IDSystem" as "ASSOC_Address._Claim.IDSystem",
            "OLD_ASSOC_GeographicalUnit.GeographicalStructureID" as "ASSOC_GeographicalUnit.GeographicalStructureID",
            "OLD_ASSOC_GeographicalUnit.GeographicalUnitID" as "ASSOC_GeographicalUnit.GeographicalUnitID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."ASSOC_Address.AddressType",
                        "OLD"."ASSOC_Address.SequenceNumber",
                        "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
                        "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
                        "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
                        "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
                        "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
                        "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
                        "OLD"."ASSOC_Address._Claim.ID",
                        "OLD"."ASSOC_Address._Claim.IDSystem",
                        "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID",
                        "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."ASSOC_Address.AddressType" AS "OLD_ASSOC_Address.AddressType" ,
                "OLD"."ASSOC_Address.SequenceNumber" AS "OLD_ASSOC_Address.SequenceNumber" ,
                "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" AS "OLD_ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" ,
                "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" AS "OLD_ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" AS "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" AS "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" ,
                "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" AS "OLD_ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" ,
                "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" AS "OLD_ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" ,
                "OLD"."ASSOC_Address._Claim.ID" AS "OLD_ASSOC_Address._Claim.ID" ,
                "OLD"."ASSOC_Address._Claim.IDSystem" AS "OLD_ASSOC_Address._Claim.IDSystem" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" AS "OLD_ASSOC_GeographicalUnit.GeographicalStructureID" ,
                "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" AS "OLD_ASSOC_GeographicalUnit.GeographicalUnitID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::AddressAssignedToGeographicalUnit" as "OLD"
            on
                                                "IN"."ASSOC_Address.AddressType" = "OLD"."ASSOC_Address.AddressType" and
                                                "IN"."ASSOC_Address.SequenceNumber" = "OLD"."ASSOC_Address.SequenceNumber" and
                                                "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" and
                                                "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" and
                                                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" and
                                                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                                                "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                                "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" and
                                                "IN"."ASSOC_Address._Claim.ID" = "OLD"."ASSOC_Address._Claim.ID" and
                                                "IN"."ASSOC_Address._Claim.IDSystem" = "OLD"."ASSOC_Address._Claim.IDSystem" and
                                                "IN"."ASSOC_GeographicalUnit.GeographicalStructureID" = "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" and
                                                "IN"."ASSOC_GeographicalUnit.GeographicalUnitID" = "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");
---delete all matching data from current table



    delete from "sap.fsdm::AddressAssignedToGeographicalUnit"
    where (
        "ASSOC_Address.AddressType",
        "ASSOC_Address.SequenceNumber",
        "ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
        "ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
        "ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
        "ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
        "ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
        "ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
        "ASSOC_Address._Claim.ID",
        "ASSOC_Address._Claim.IDSystem",
        "ASSOC_GeographicalUnit.GeographicalStructureID",
        "ASSOC_GeographicalUnit.GeographicalUnitID",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."ASSOC_Address.AddressType",
            "OLD"."ASSOC_Address.SequenceNumber",
            "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID",
            "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID",
            "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem",
            "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID",
            "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID",
            "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID",
            "OLD"."ASSOC_Address._Claim.ID",
            "OLD"."ASSOC_Address._Claim.IDSystem",
            "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID",
            "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::AddressAssignedToGeographicalUnit" as "OLD"
        on
                                       "IN"."ASSOC_Address.AddressType" = "OLD"."ASSOC_Address.AddressType" and
                                       "IN"."ASSOC_Address.SequenceNumber" = "OLD"."ASSOC_Address.SequenceNumber" and
                                       "IN"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" = "OLD"."ASSOC_Address.ASSOC_BankingChannel.BankingChannelID" and
                                       "IN"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_BusinessPartnerID.BusinessPartnerID" and
                                       "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.IDSystem" and
                                       "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.OrganizationalUnitID" and
                                       "IN"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" = "OLD"."ASSOC_Address.ASSOC_OrganizationalUnit.ASSOC_OrganizationHostingOrganizationalUnit.BusinessPartnerID" and
                                       "IN"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" = "OLD"."ASSOC_Address.ASSOC_PhysicalAsset.PhysicalAssetID" and
                                       "IN"."ASSOC_Address._Claim.ID" = "OLD"."ASSOC_Address._Claim.ID" and
                                       "IN"."ASSOC_Address._Claim.IDSystem" = "OLD"."ASSOC_Address._Claim.IDSystem" and
                                       "IN"."ASSOC_GeographicalUnit.GeographicalStructureID" = "OLD"."ASSOC_GeographicalUnit.GeographicalStructureID" and
                                       "IN"."ASSOC_GeographicalUnit.GeographicalUnitID" = "OLD"."ASSOC_GeographicalUnit.GeographicalUnitID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" )) 
)
;



END
