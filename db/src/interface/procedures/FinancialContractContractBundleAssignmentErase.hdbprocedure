PROCEDURE "sap.fsdm.procedures::FinancialContractContractBundleAssignmentErase" (IN ROW "sap.fsdm.tabletypes::FinancialContractContractBundleAssignmentTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "_ContractBundle.ContractBundleID" is null and
            "_FinancialContract.FinancialContractID" is null and
            "_FinancialContract.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::FinancialContractContractBundleAssignment"
        WHERE
        (            "_ContractBundle.ContractBundleID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select                 "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialContractContractBundleAssignment" "OLD"
            on
            "IN"."_ContractBundle.ContractBundleID" = "OLD"."_ContractBundle.ContractBundleID" and
            "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
            "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

        --delete data from history table
        delete from "sap.fsdm::FinancialContractContractBundleAssignment_Historical"
        WHERE
        (
            "_ContractBundle.ContractBundleID" ,
            "_FinancialContract.FinancialContractID" ,
            "_FinancialContract.IDSystem" 
        ) in
        (
            select
                "OLD"."_ContractBundle.ContractBundleID" ,
                "OLD"."_FinancialContract.FinancialContractID" ,
                "OLD"."_FinancialContract.IDSystem" 
            from :ROW "IN"
            inner join "sap.fsdm::FinancialContractContractBundleAssignment_Historical" "OLD"
            on
                "IN"."_ContractBundle.ContractBundleID" = "OLD"."_ContractBundle.ContractBundleID" and
                "IN"."_FinancialContract.FinancialContractID" = "OLD"."_FinancialContract.FinancialContractID" and
                "IN"."_FinancialContract.IDSystem" = "OLD"."_FinancialContract.IDSystem" 
        );

END
