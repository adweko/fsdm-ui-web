PROCEDURE "sap.fsdm.procedures::LeaseRateDelReadOnly" (IN ROW "sap.fsdm.tabletypes::LeaseRateTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::LeaseRateTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::LeaseRateTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'SequenceNumber=' || TO_VARCHAR("SequenceNumber") || ' ' ||
                '_LeaseAgreement.FinancialContractID=' || TO_VARCHAR("_LeaseAgreement.FinancialContractID") || ' ' ||
                '_LeaseAgreement.IDSystem=' || TO_VARCHAR("_LeaseAgreement.IDSystem") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_LeaseAgreement.FinancialContractID",
                        "IN"."_LeaseAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."SequenceNumber",
                        "IN"."_LeaseAgreement.FinancialContractID",
                        "IN"."_LeaseAgreement.IDSystem"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "SequenceNumber",
                        "_LeaseAgreement.FinancialContractID",
                        "_LeaseAgreement.IDSystem"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_LeaseAgreement.FinancialContractID",
                                    "IN"."_LeaseAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."SequenceNumber",
                                    "IN"."_LeaseAgreement.FinancialContractID",
                                    "IN"."_LeaseAgreement.IDSystem"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "SequenceNumber" is null and
            "_LeaseAgreement.FinancialContractID" is null and
            "_LeaseAgreement.IDSystem" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "SequenceNumber",
            "_LeaseAgreement.FinancialContractID",
            "_LeaseAgreement.IDSystem",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::LeaseRate" WHERE
            (
            "SequenceNumber" ,
            "_LeaseAgreement.FinancialContractID" ,
            "_LeaseAgreement.IDSystem" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."SequenceNumber",
            "OLD"."_LeaseAgreement.FinancialContractID",
            "OLD"."_LeaseAgreement.IDSystem",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::LeaseRate" as "OLD"
        on
                              "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                              "IN"."_LeaseAgreement.FinancialContractID" = "OLD"."_LeaseAgreement.FinancialContractID" and
                              "IN"."_LeaseAgreement.IDSystem" = "OLD"."_LeaseAgreement.IDSystem" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "SequenceNumber",
        "_LeaseAgreement.FinancialContractID",
        "_LeaseAgreement.IDSystem",
        "BusinessValidFrom",
        "BusinessValidTo",
        "LeaseRateCurrency",
        "LeaseRatePerTimeUnit",
        "LeaseRatePerUnit",
        "LeaseRateTimeUnit",
        "LeaseRateType",
        "LeaseRateUnit",
        "PreconditionApplies",
        "ScaleApplies",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD_SequenceNumber" as "SequenceNumber" ,
            "OLD__LeaseAgreement.FinancialContractID" as "_LeaseAgreement.FinancialContractID" ,
            "OLD__LeaseAgreement.IDSystem" as "_LeaseAgreement.IDSystem" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_LeaseRateCurrency" as "LeaseRateCurrency" ,
            "OLD_LeaseRatePerTimeUnit" as "LeaseRatePerTimeUnit" ,
            "OLD_LeaseRatePerUnit" as "LeaseRatePerUnit" ,
            "OLD_LeaseRateTimeUnit" as "LeaseRateTimeUnit" ,
            "OLD_LeaseRateType" as "LeaseRateType" ,
            "OLD_LeaseRateUnit" as "LeaseRateUnit" ,
            "OLD_PreconditionApplies" as "PreconditionApplies" ,
            "OLD_ScaleApplies" as "ScaleApplies" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."_LeaseAgreement.FinancialContractID",
                        "OLD"."_LeaseAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."_LeaseAgreement.FinancialContractID" AS "OLD__LeaseAgreement.FinancialContractID" ,
                "OLD"."_LeaseAgreement.IDSystem" AS "OLD__LeaseAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."LeaseRateCurrency" AS "OLD_LeaseRateCurrency" ,
                "OLD"."LeaseRatePerTimeUnit" AS "OLD_LeaseRatePerTimeUnit" ,
                "OLD"."LeaseRatePerUnit" AS "OLD_LeaseRatePerUnit" ,
                "OLD"."LeaseRateTimeUnit" AS "OLD_LeaseRateTimeUnit" ,
                "OLD"."LeaseRateType" AS "OLD_LeaseRateType" ,
                "OLD"."LeaseRateUnit" AS "OLD_LeaseRateUnit" ,
                "OLD"."PreconditionApplies" AS "OLD_PreconditionApplies" ,
                "OLD"."ScaleApplies" AS "OLD_ScaleApplies" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::LeaseRate" as "OLD"
            on
                                      "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
                                      "IN"."_LeaseAgreement.FinancialContractID" = "OLD"."_LeaseAgreement.FinancialContractID" and
                                      "IN"."_LeaseAgreement.IDSystem" = "OLD"."_LeaseAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD_SequenceNumber" as "SequenceNumber",
            "OLD__LeaseAgreement.FinancialContractID" as "_LeaseAgreement.FinancialContractID",
            "OLD__LeaseAgreement.IDSystem" as "_LeaseAgreement.IDSystem",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_LeaseRateCurrency" as "LeaseRateCurrency",
            "OLD_LeaseRatePerTimeUnit" as "LeaseRatePerTimeUnit",
            "OLD_LeaseRatePerUnit" as "LeaseRatePerUnit",
            "OLD_LeaseRateTimeUnit" as "LeaseRateTimeUnit",
            "OLD_LeaseRateType" as "LeaseRateType",
            "OLD_LeaseRateUnit" as "LeaseRateUnit",
            "OLD_PreconditionApplies" as "PreconditionApplies",
            "OLD_ScaleApplies" as "ScaleApplies",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."SequenceNumber",
                        "OLD"."_LeaseAgreement.FinancialContractID",
                        "OLD"."_LeaseAgreement.IDSystem",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."SequenceNumber" AS "OLD_SequenceNumber" ,
                "OLD"."_LeaseAgreement.FinancialContractID" AS "OLD__LeaseAgreement.FinancialContractID" ,
                "OLD"."_LeaseAgreement.IDSystem" AS "OLD__LeaseAgreement.IDSystem" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."LeaseRateCurrency" AS "OLD_LeaseRateCurrency" ,
                "OLD"."LeaseRatePerTimeUnit" AS "OLD_LeaseRatePerTimeUnit" ,
                "OLD"."LeaseRatePerUnit" AS "OLD_LeaseRatePerUnit" ,
                "OLD"."LeaseRateTimeUnit" AS "OLD_LeaseRateTimeUnit" ,
                "OLD"."LeaseRateType" AS "OLD_LeaseRateType" ,
                "OLD"."LeaseRateUnit" AS "OLD_LeaseRateUnit" ,
                "OLD"."PreconditionApplies" AS "OLD_PreconditionApplies" ,
                "OLD"."ScaleApplies" AS "OLD_ScaleApplies" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::LeaseRate" as "OLD"
            on
               "IN"."SequenceNumber" = "OLD"."SequenceNumber" and
               "IN"."_LeaseAgreement.FinancialContractID" = "OLD"."_LeaseAgreement.FinancialContractID" and
               "IN"."_LeaseAgreement.IDSystem" = "OLD"."_LeaseAgreement.IDSystem" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
