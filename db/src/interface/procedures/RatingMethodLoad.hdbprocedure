PROCEDURE "sap.fsdm.procedures::RatingMethodLoad" (IN ROW "sap.fsdm.tabletypes::RatingMethodTT")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                'RatingMethod=' || TO_VARCHAR("RatingMethod") || ' ' ||
                'RatingMethodComponentType=' || TO_VARCHAR("RatingMethodComponentType") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."RatingMethod",
                        "IN"."RatingMethodComponentType"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."RatingMethod",
                        "IN"."RatingMethodComponentType"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "RatingMethod",
                        "RatingMethodComponentType"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."RatingMethod",
                                    "IN"."RatingMethodComponentType"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."RatingMethod",
                                    "IN"."RatingMethodComponentType"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;

    :var_overlap.delete();





    --Insert chunked versions of object
    insert into "sap.fsdm::RatingMethod" (
        "RatingMethod",
        "RatingMethodComponentType",
        "BusinessValidFrom",
        "BusinessValidTo",
        "RatingMethodComponent",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RatingMethod" as "RatingMethod" ,
            "OLD_RatingMethodComponentType" as "RatingMethodComponentType" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_RatingMethodComponent" as "RatingMethodComponent" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "IN"."RatingMethod",
                        "IN"."RatingMethodComponentType",
                        "OLD"."BusinessValidFrom"
                       order by "IN"."BusinessValidFrom") as "NX_",
                "IN"."BusinessValidFrom",
                                "OLD"."RatingMethod" as "OLD_RatingMethod",
                                "OLD"."RatingMethodComponentType" as "OLD_RatingMethodComponentType",
                                "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                                "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
                "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
                "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                                "OLD"."RatingMethodComponent" as "OLD_RatingMethodComponent",
                                "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                                "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                                "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                                "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                                "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RatingMethod" as "OLD"
            on
                ifnull( "IN"."RatingMethod", '') = "OLD"."RatingMethod" and
                ifnull( "IN"."RatingMethodComponentType", '') = "OLD"."RatingMethodComponentType" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "BusinessValidFrom" > "NX_" );

    --Insert rightmost chunk
    insert into "sap.fsdm::RatingMethod" (
        "RatingMethod",
        "RatingMethodComponentType",
        "BusinessValidFrom",
        "BusinessValidTo",
        "RatingMethodComponent",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
    )
    (
        select
            "OLD_RatingMethod" as "RatingMethod",
            "OLD_RatingMethodComponentType" as "RatingMethodComponentType",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_RatingMethodComponent" as "RatingMethodComponent",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "IN"."RatingMethod",
                        "IN"."RatingMethodComponentType",
                        "OLD"."BusinessValidFrom"
                order by "IN"."BusinessValidFrom") AS "NY_",
                "IN"."BusinessValidTo",
                        "OLD"."RatingMethod" as "OLD_RatingMethod",
                        "OLD"."RatingMethodComponentType" as "OLD_RatingMethodComponentType",
                        "OLD"."BusinessValidFrom" as "OLD_BusinessValidFrom",
                        "OLD"."BusinessValidTo" as "OLD_BusinessValidTo",
            "OLD"."SystemValidFrom" as "OLD_SystemValidFrom",
            "OLD"."SystemValidTo" as "OLD_SystemValidTo",
                        "OLD"."RatingMethodComponent" as "OLD_RatingMethodComponent",
                        "OLD"."SourceSystemID" as "OLD_SourceSystemID",
                        "OLD"."ChangeTimestampInSourceSystem" as "OLD_ChangeTimestampInSourceSystem",
                        "OLD"."ChangingUserInSourceSystem" as "OLD_ChangingUserInSourceSystem",
                        "OLD"."ChangingProcessType" as "OLD_ChangingProcessType",
                        "OLD"."ChangingProcessID" as "OLD_ChangingProcessID"
            from :row as "IN"
            inner join "sap.fsdm::RatingMethod" as "OLD"
            on
                ifnull( "IN"."RatingMethod", '' ) = "OLD"."RatingMethod" and
                ifnull( "IN"."RatingMethodComponentType", '' ) = "OLD"."RatingMethodComponentType" 
            where
                         ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
)
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo");

    delete from "sap.fsdm::RatingMethod"
    where (
        "RatingMethod",
        "RatingMethodComponentType",
        "BusinessValidFrom",
        "BusinessValidTo"
    )
    in
    (
        select
            "OLD"."RatingMethod",
            "OLD"."RatingMethodComponentType",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::RatingMethod" as "OLD"
        on
           ifnull( "IN"."RatingMethod", '' ) = "OLD"."RatingMethod" and
           ifnull( "IN"."RatingMethodComponentType", '' ) = "OLD"."RatingMethodComponentType" 
        where
           ( ( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ) )
);

    --Insert ALL the input data 
    insert into "sap.fsdm::RatingMethod" (
        "RatingMethod",
        "RatingMethodComponentType",
        "BusinessValidFrom",
        "BusinessValidTo",
        "RatingMethodComponent",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
)    (
        select
            ifnull( "RatingMethod", '' ) as "RatingMethod",
            ifnull( "RatingMethodComponentType", '' ) as "RatingMethodComponentType",
            "BusinessValidFrom"  ,
            "BusinessValidTo"  ,
            "RatingMethodComponent"  ,
            "SourceSystemID"  ,
            "ChangeTimestampInSourceSystem"  ,
            "ChangingUserInSourceSystem"  ,
            "ChangingProcessType"  ,
            "ChangingProcessID"  
        from :row );

END