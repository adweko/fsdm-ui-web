PROCEDURE "sap.fsdm.procedures::ValuationReferenceIDErase" (IN ROW "sap.fsdm.tabletypes::ValuationReferenceIDTT_Erase")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  AS
BEGIN

    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10001;
    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;
    var_find_null =
        select 1 as find_null
        from :row
        where
            "SystemID" is null and
            "_Valuation.Scenario" is null and
            "_Valuation.ValuationMethod" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

--delete data from current table
        delete from "sap.fsdm::ValuationReferenceID"
        WHERE
        (            "SystemID" ,
            "_Valuation.Scenario" ,
            "_Valuation.ValuationMethod" 
        ) in
        (
            select                 "OLD"."SystemID" ,
                "OLD"."_Valuation.Scenario" ,
                "OLD"."_Valuation.ValuationMethod" 
            from :ROW "IN"
            inner join "sap.fsdm::ValuationReferenceID" "OLD"
            on
            "IN"."SystemID" = "OLD"."SystemID" and
            "IN"."_Valuation.Scenario" = "OLD"."_Valuation.Scenario" and
            "IN"."_Valuation.ValuationMethod" = "OLD"."_Valuation.ValuationMethod" 
        );

        --delete data from history table
        delete from "sap.fsdm::ValuationReferenceID_Historical"
        WHERE
        (
            "SystemID" ,
            "_Valuation.Scenario" ,
            "_Valuation.ValuationMethod" 
        ) in
        (
            select
                "OLD"."SystemID" ,
                "OLD"."_Valuation.Scenario" ,
                "OLD"."_Valuation.ValuationMethod" 
            from :ROW "IN"
            inner join "sap.fsdm::ValuationReferenceID_Historical" "OLD"
            on
                "IN"."SystemID" = "OLD"."SystemID" and
                "IN"."_Valuation.Scenario" = "OLD"."_Valuation.Scenario" and
                "IN"."_Valuation.ValuationMethod" = "OLD"."_Valuation.ValuationMethod" 
        );

END
