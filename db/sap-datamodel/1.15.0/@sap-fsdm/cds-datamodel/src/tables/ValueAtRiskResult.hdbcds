namespace "sap"."fsdm";

using "sap"."fsdm"::"ResultGroup";
using "sap"."fsdm"::"RiskReportingNode";

entity "ValueAtRiskResult" {
    key "RiskProvisionScenario"              : String(100)                                              default '';
    key "RoleOfCurrency"                     : String(40)                                               default '';
    key "_ResultGroup"                       : association to ResultGroup {
                                                                            ResultDataProvider,
                                                                            ResultGroupID
                                                                          }                             not null;
    key "_RiskReportingNode"                 : association to RiskReportingNode { RiskReportingNodeID } not null;
    key "BusinessValidFrom"                  : LocalDate;
    key "BusinessValidTo"                    : LocalDate;
        "SystemValidFrom"                    : UTCTimestamp                                             not null;
        "SystemValidTo"                      : UTCTimestamp                                             not null;
        "ConfidenceLevel"                    : Decimal(15, 11);
        "Currency"                           : String(3);
        "DecayFactor"                        : Decimal(15, 11);
        "DeltaVariance"                      : Decimal(15, 11);
        "ExpectedShortfall"                  : Decimal(34, 6);
        "GammaVariance"                      : Decimal(15, 11);
        "HistoricalPeriodEndDate"            : LocalDate;
        "HistoricalPeriodStartDate"          : LocalDate;
        "HoldingPeriod"                      : Decimal(34, 6);
        "HoldingPeriodTimeUnit"              : String(128);
        "NumberOfSimulationRuns"             : Integer;
        "OverlapReturnHorizonPeriod"         : Decimal(34, 6);
        "OverlapReturnHorizonPeriodTimeUnit" : String(128);
        "ReturnHorizonPeriod"                : Decimal(34, 6);
        "ReturnHorizonTimeUnit"              : String(128);
        "RiskGroup"                          : String(100);
        "SimulatedMarketDataCreationMethod"  : String(100);
        "ThirdMomentDelta"                   : Decimal(15, 11);
        "ThirdMomentGamma"                   : Decimal(15, 11);
        "ValueAtRisk"                        : Decimal(34, 6);
        "ValueAtRiskCalculationMethod"       : String(100);
        "ValueAtRiskResultCategory"          : String(40);
        "SourceSystemID"                     : String(128);
        "ChangeTimestampInSourceSystem"      : UTCTimestamp;
        "ChangingUserInSourceSystem"         : String(128);
        "ChangingProcessType"                : String(40);
        "ChangingProcessID"                  : String(128);
}
technical configuration {
    column store;
};

entity "ValueAtRiskResult_Historical" {
    "RiskProvisionScenario"              : String(100)                                              default '' not null;
    "RoleOfCurrency"                     : String(40)                                               default '' not null;
    "_ResultGroup"                       : association to ResultGroup {
                                                                        ResultDataProvider,
                                                                        ResultGroupID
                                                                      }                             not null;
    "_RiskReportingNode"                 : association to RiskReportingNode { RiskReportingNodeID } not null;
    "BusinessValidFrom"                  : LocalDate                                                not null;
    "BusinessValidTo"                    : LocalDate                                                not null;
    "SystemValidFrom"                    : UTCTimestamp                                             not null;
    "SystemValidTo"                      : UTCTimestamp                                             not null;
    "ConfidenceLevel"                    : Decimal(15, 11);
    "Currency"                           : String(3);
    "DecayFactor"                        : Decimal(15, 11);
    "DeltaVariance"                      : Decimal(15, 11);
    "ExpectedShortfall"                  : Decimal(34, 6);
    "GammaVariance"                      : Decimal(15, 11);
    "HistoricalPeriodEndDate"            : LocalDate;
    "HistoricalPeriodStartDate"          : LocalDate;
    "HoldingPeriod"                      : Decimal(34, 6);
    "HoldingPeriodTimeUnit"              : String(128);
    "NumberOfSimulationRuns"             : Integer;
    "OverlapReturnHorizonPeriod"         : Decimal(34, 6);
    "OverlapReturnHorizonPeriodTimeUnit" : String(128);
    "ReturnHorizonPeriod"                : Decimal(34, 6);
    "ReturnHorizonTimeUnit"              : String(128);
    "RiskGroup"                          : String(100);
    "SimulatedMarketDataCreationMethod"  : String(100);
    "ThirdMomentDelta"                   : Decimal(15, 11);
    "ThirdMomentGamma"                   : Decimal(15, 11);
    "ValueAtRisk"                        : Decimal(34, 6);
    "ValueAtRiskCalculationMethod"       : String(100);
    "ValueAtRiskResultCategory"          : String(40);
    "SourceSystemID"                     : String(128);
    "ChangeTimestampInSourceSystem"      : UTCTimestamp;
    "ChangingUserInSourceSystem"         : String(128);
    "ChangingProcessType"                : String(40);
    "ChangingProcessID"                  : String(128);
}
technical configuration {
    column store;
};