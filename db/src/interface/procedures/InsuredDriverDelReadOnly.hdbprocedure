PROCEDURE "sap.fsdm.procedures::InsuredDriverDelReadOnly" (IN ROW "sap.fsdm.tabletypes::InsuredDriverTT_Del", OUT CURR_DEL "sap.fsdm.tabletypes::InsuredDriverTT_Del", OUT CURR_INS "sap.fsdm.tabletypes::InsuredDriverTT_Out")
  LANGUAGE SQLSCRIPT
  SQL SECURITY DEFINER
  READS SQL DATA
  AS
BEGIN

    --Check for period overlap
    declare period_overlap condition for sql_error_code 10001;
    declare semantic_keys_null_in_one_or_more_rows condition for sql_error_code 10003;
    declare exit handler for period_overlap
        begin
            declare err_msg clob;
            select TOP 1
                'Business Period Overlap: Key ' ||
                '_IndividualPerson.BusinessPartnerID=' || TO_VARCHAR("_IndividualPerson.BusinessPartnerID") || ' ' ||
                '_InsuredVehicle._FinancialContract.FinancialContractID=' || TO_VARCHAR("_InsuredVehicle._FinancialContract.FinancialContractID") || ' ' ||
                '_InsuredVehicle._FinancialContract.IDSystem=' || TO_VARCHAR("_InsuredVehicle._FinancialContract.IDSystem") || ' ' ||
                '_InsuredVehicle._PhysicalAsset.PhysicalAssetID=' || TO_VARCHAR("_InsuredVehicle._PhysicalAsset.PhysicalAssetID") || ' ' ||
                ':Business Period Overlap Error'
            into err_msg
            from
            (
                select
                    "IN"."BusinessValidFrom" as "X",
                    "IN"."BusinessValidTo" as "Y",
                    lag ( "IN"."BusinessValidFrom", 1)
                    over ( partition by
                        "IN"."_IndividualPerson.BusinessPartnerID",
                        "IN"."_InsuredVehicle._FinancialContract.FinancialContractID",
                        "IN"."_InsuredVehicle._FinancialContract.IDSystem",
                        "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NX_",
                    lag("IN"."BusinessValidTo", 1)
                    over ( partition by
                        "IN"."_IndividualPerson.BusinessPartnerID",
                        "IN"."_InsuredVehicle._FinancialContract.FinancialContractID",
                        "IN"."_InsuredVehicle._FinancialContract.IDSystem",
                        "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID"
                    order by "IN"."BusinessValidFrom") as "NY_",
                        "_IndividualPerson.BusinessPartnerID",
                        "_InsuredVehicle._FinancialContract.FinancialContractID",
                        "_InsuredVehicle._FinancialContract.IDSystem",
                        "_InsuredVehicle._PhysicalAsset.PhysicalAssetID"
                from :row as "IN"
            )
            where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");
            resignal set message_text = :err_msg;
        end;

    declare exit handler for semantic_keys_null_in_one_or_more_rows
        BEGIN
            declare err_msg clob;
            select 'All the fields in the input are null' into err_msg
            from "sap.fsdm.synonyms::DUMMY";
            resignal set message_text = :err_msg;
        END;

    var_overlap = select *
                    from
                    (
                        select
                            "IN"."BusinessValidFrom" as "X",
                            "IN"."BusinessValidTo" as "Y",
                            lag ( "IN"."BusinessValidFrom", 1)
                            over ( partition by
                                    "IN"."_IndividualPerson.BusinessPartnerID",
                                    "IN"."_InsuredVehicle._FinancialContract.FinancialContractID",
                                    "IN"."_InsuredVehicle._FinancialContract.IDSystem",
                                    "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NX_",
                            lag ( "IN"."BusinessValidTo", 1)
                            over ( partition by
                                    "IN"."_IndividualPerson.BusinessPartnerID",
                                    "IN"."_InsuredVehicle._FinancialContract.FinancialContractID",
                                    "IN"."_InsuredVehicle._FinancialContract.IDSystem",
                                    "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID"
                            order by "IN"."BusinessValidFrom") as "NY_"
                from :row as "IN"
                )
                where ("X" >= "NX_" and "X" < "NY_") or ("Y" > "NX_" and "Y" <= "NY_");

    if not is_empty(:var_overlap) then
      signal period_overlap;
    end if;


    :var_overlap.delete();

    var_find_null =
        select 1 as find_null
        from :row
        where
            "_IndividualPerson.BusinessPartnerID" is null and
            "_InsuredVehicle._FinancialContract.FinancialContractID" is null and
            "_InsuredVehicle._FinancialContract.IDSystem" is null and
            "_InsuredVehicle._PhysicalAsset.PhysicalAssetID" is null 
;

    IF NOT IS_EMPTY(:var_find_null) THEN
        SIGNAL semantic_keys_null_in_one_or_more_rows;
    END IF;

    CURR_DEL = select 
            "_IndividualPerson.BusinessPartnerID",
            "_InsuredVehicle._FinancialContract.FinancialContractID",
            "_InsuredVehicle._FinancialContract.IDSystem",
            "_InsuredVehicle._PhysicalAsset.PhysicalAssetID",
            "BusinessValidFrom",
            "BusinessValidTo"
        from "sap.fsdm::InsuredDriver" WHERE
            (
            "_IndividualPerson.BusinessPartnerID" ,
            "_InsuredVehicle._FinancialContract.FinancialContractID" ,
            "_InsuredVehicle._FinancialContract.IDSystem" ,
            "_InsuredVehicle._PhysicalAsset.PhysicalAssetID" ,
            "BusinessValidFrom" ,
            "BusinessValidTo" 
            
)
        in ( select
            "OLD"."_IndividualPerson.BusinessPartnerID",
            "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID",
            "OLD"."_InsuredVehicle._FinancialContract.IDSystem",
            "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID",
            "OLD"."BusinessValidFrom",
            "OLD"."BusinessValidTo"
        from :row as "IN"
        inner join "sap.fsdm::InsuredDriver" as "OLD"
        on
                              "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                              "IN"."_InsuredVehicle._FinancialContract.FinancialContractID" = "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID" and
                              "IN"."_InsuredVehicle._FinancialContract.IDSystem" = "OLD"."_InsuredVehicle._FinancialContract.IDSystem" and
                              "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" 
        where
           (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
           ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))               )
;


--Insert ALL the input data 

     CURR_INS = select 
        "_IndividualPerson.BusinessPartnerID",
        "_InsuredVehicle._FinancialContract.FinancialContractID",
        "_InsuredVehicle._FinancialContract.IDSystem",
        "_InsuredVehicle._PhysicalAsset.PhysicalAssetID",
        "BusinessValidFrom",
        "BusinessValidTo",
        "MainIndicator",
        "NoClaimsBonusIndicator",
        "SourceSystemID",
        "ChangeTimestampInSourceSystem",
        "ChangingUserInSourceSystem",
        "ChangingProcessType",
        "ChangingProcessID"
          from (
          (
          select
            "OLD__IndividualPerson.BusinessPartnerID" as "_IndividualPerson.BusinessPartnerID" ,
            "OLD__InsuredVehicle._FinancialContract.FinancialContractID" as "_InsuredVehicle._FinancialContract.FinancialContractID" ,
            "OLD__InsuredVehicle._FinancialContract.IDSystem" as "_InsuredVehicle._FinancialContract.IDSystem" ,
            "OLD__InsuredVehicle._PhysicalAsset.PhysicalAssetID" as "_InsuredVehicle._PhysicalAsset.PhysicalAssetID" ,
            "NX_" as "BusinessValidFrom" ,
            "BusinessValidFrom" as "BusinessValidTo" ,
            "OLD_MainIndicator" as "MainIndicator" ,
            "OLD_NoClaimsBonusIndicator" as "NoClaimsBonusIndicator" ,
            "OLD_SourceSystemID" as "SourceSystemID" ,
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem" ,
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem" ,
            "OLD_ChangingProcessType" as "ChangingProcessType" ,
            "OLD_ChangingProcessID" as "ChangingProcessID" 
        from
        (
            select
                lag("IN"."BusinessValidTo", 1, "OLD"."BusinessValidFrom")
                over ( partition by
                        "OLD"."_IndividualPerson.BusinessPartnerID",
                        "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID",
                        "OLD"."_InsuredVehicle._FinancialContract.IDSystem",
                        "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                       order by "OLD"."BusinessValidFrom") as "NX_",
                                "OLD"."_IndividualPerson.BusinessPartnerID" AS "OLD__IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID" AS "OLD__InsuredVehicle._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuredVehicle._FinancialContract.IDSystem" AS "OLD__InsuredVehicle._FinancialContract.IDSystem" ,
                "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" AS "OLD__InsuredVehicle._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."MainIndicator" AS "OLD_MainIndicator" ,
                "OLD"."NoClaimsBonusIndicator" AS "OLD_NoClaimsBonusIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InsuredDriver" as "OLD"
            on
                                      "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
                                      "IN"."_InsuredVehicle._FinancialContract.FinancialContractID" = "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID" and
                                      "IN"."_InsuredVehicle._FinancialContract.IDSystem" = "OLD"."_InsuredVehicle._FinancialContract.IDSystem" and
                                      "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                            )
        where "BusinessValidFrom" > "NX_"         )
        UNION ALL
        (
        select
            "OLD__IndividualPerson.BusinessPartnerID" as "_IndividualPerson.BusinessPartnerID",
            "OLD__InsuredVehicle._FinancialContract.FinancialContractID" as "_InsuredVehicle._FinancialContract.FinancialContractID",
            "OLD__InsuredVehicle._FinancialContract.IDSystem" as "_InsuredVehicle._FinancialContract.IDSystem",
            "OLD__InsuredVehicle._PhysicalAsset.PhysicalAssetID" as "_InsuredVehicle._PhysicalAsset.PhysicalAssetID",
            "BusinessValidTo" as "BusinessValidFrom",
            "OLD_BusinessValidTo" as "BusinessValidTo",
            "OLD_MainIndicator" as "MainIndicator",
            "OLD_NoClaimsBonusIndicator" as "NoClaimsBonusIndicator",
            "OLD_SourceSystemID" as "SourceSystemID",
            "OLD_ChangeTimestampInSourceSystem" as "ChangeTimestampInSourceSystem",
            "OLD_ChangingUserInSourceSystem" as "ChangingUserInSourceSystem",
            "OLD_ChangingProcessType" as "ChangingProcessType",
            "OLD_ChangingProcessID" as "ChangingProcessID"
        from
        (
            select
                lead("IN"."BusinessValidFrom", 1, "OLD"."BusinessValidTo")
                over ( partition by
                        "OLD"."_IndividualPerson.BusinessPartnerID",
                        "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID",
                        "OLD"."_InsuredVehicle._FinancialContract.IDSystem",
                        "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID",
                        "OLD"."BusinessValidFrom"
                order by "OLD"."BusinessValidFrom") AS "NY_",
                                "OLD"."_IndividualPerson.BusinessPartnerID" AS "OLD__IndividualPerson.BusinessPartnerID" ,
                "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID" AS "OLD__InsuredVehicle._FinancialContract.FinancialContractID" ,
                "OLD"."_InsuredVehicle._FinancialContract.IDSystem" AS "OLD__InsuredVehicle._FinancialContract.IDSystem" ,
                "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" AS "OLD__InsuredVehicle._PhysicalAsset.PhysicalAssetID" ,
                "OLD"."BusinessValidFrom" AS "OLD_BusinessValidFrom" ,
                "IN"."BusinessValidFrom"  ,
                "OLD"."BusinessValidTo" AS "OLD_BusinessValidTo" ,
                "IN"."BusinessValidTo"  ,
                "OLD"."SystemValidFrom" AS "OLD_SystemValidFrom" ,
                "OLD"."SystemValidTo" AS "OLD_SystemValidTo" ,
                "OLD"."MainIndicator" AS "OLD_MainIndicator" ,
                "OLD"."NoClaimsBonusIndicator" AS "OLD_NoClaimsBonusIndicator" ,
                "OLD"."SourceSystemID" AS "OLD_SourceSystemID" ,
                "OLD"."ChangeTimestampInSourceSystem" AS "OLD_ChangeTimestampInSourceSystem" ,
                "OLD"."ChangingUserInSourceSystem" AS "OLD_ChangingUserInSourceSystem" ,
                "OLD"."ChangingProcessType" AS "OLD_ChangingProcessType" ,
                "OLD"."ChangingProcessID" AS "OLD_ChangingProcessID" 
            from :row as "IN"
            inner join "sap.fsdm::InsuredDriver" as "OLD"
            on
               "IN"."_IndividualPerson.BusinessPartnerID" = "OLD"."_IndividualPerson.BusinessPartnerID" and
               "IN"."_InsuredVehicle._FinancialContract.FinancialContractID" = "OLD"."_InsuredVehicle._FinancialContract.FinancialContractID" and
               "IN"."_InsuredVehicle._FinancialContract.IDSystem" = "OLD"."_InsuredVehicle._FinancialContract.IDSystem" and
               "IN"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" = "OLD"."_InsuredVehicle._PhysicalAsset.PhysicalAssetID" 
            where
                         (( "IN"."BusinessValidFrom" < "OLD"."BusinessValidTo" and "IN"."BusinessValidTo" > "OLD"."BusinessValidFrom" ) or
                         ( "IN"."BusinessValidFrom" = "OLD"."BusinessValidFrom" and "IN"."BusinessValidTo" = "OLD"."BusinessValidTo" ))
                                    )
        where "NY_" = "OLD_BusinessValidTo" and "OLD_BusinessValidTo" > "BusinessValidTo"));


END
